<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/record_decisions_info.php');
if ($_GET['meeting_notice_id'] != NULL) {
    $result = $record_decisions_info->rpt_record_of_decision_getone(array($_GET['meeting_notice_id']));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title>Briefing Note</title>
    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Record of Decisions</title>
    <?php else: ?>
        <title>সিদ্ধান্তসমূহ</title>
    <?php endif; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
</head>

<body>
<?php if ($result->is_issued === '1') : ?>
<div style="margin:10px; width:80%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:80%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat;backgroun-size: 200px 180px"
        id="message_area">
        <?php endif; ?>

        <div>
            <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            <div style="float:left; height:80px; padding-top:25px; padding-left:30px;font-weight: bold;">
                <div style="font-size:18px;"><?php echo $result->committee_name; ?></div>
                <?php if (isset($language_id) && $language_id === '1'): ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">Parliament of Bangladesh</div>
                <?php else: ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">বাংলাদেশ জাতীয় সংসদ</div>
                <?php endif; ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br/>
        <?php if (isset($language_id) && $language_id === '1'): ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">RECORD OF DECISION</div>
        <?php else: ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">সিদ্ধান্তসমূহ</div>
        <?php endif; ?>

        <div style="margin-top:10px;">
            <?php

            if (isset($language_id) && $language_id === '1') {
                echo '<b>Date: </b>' . htmlspecialchars_decode($result->en_date) . ' and ' . htmlspecialchars_decode($result->bn_date) . '<br>';
                echo '<b>Time: </b>' . htmlspecialchars_decode($result->time) . '<br>';
                echo '<b>Location: </b>' . htmlspecialchars_decode($result->venue_name) . '<br>';
                echo '<b>Chair: </b>' . htmlspecialchars_decode($result->chairman_name) . '<br>';
                echo '<b>Members Present: </b>' . htmlspecialchars_decode($result->member_name) . '<br>';
                echo '<b>Officials Presents: </b>' . htmlspecialchars_decode($result->logistic_member_name) . '<br>';
                echo '<b>Witnessess: </b>' . htmlspecialchars_decode($result->witness_member_name) . '<br><br>';
                // echo '<b>PRIVATE BUSINESS</b><br>'.htmlspecialchars_decode($result->private_business).'<br>';
                echo '<b>PUBLIC BUSINESS</b><br>' . htmlspecialchars_decode($result->public_business) . '<br>';
                echo '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($result->result_of_deliberation) . '<br>';
            } else {
                echo '<b>তারিখ: </b>' . htmlspecialchars_decode($result->en_date) . ' and ' . htmlspecialchars_decode($result->bn_date) . '<br>';
                echo '<b>সময়: </b>' . htmlspecialchars_decode($result->time) . '<br>';
                echo '<b>স্থান: </b>' . htmlspecialchars_decode($result->venue_name) . '<br>';
                echo '<b>চেয়ার: </b>' . htmlspecialchars_decode($result->chairman_name) . '<br>';
                echo '<b>উপস্থিত সদস্যবৃন্দ: </b>' . htmlspecialchars_decode($result->member_name) . '<br>';
                echo '<b>উপস্থিত কর্মকর্তাবৃন্দ: </b>' . htmlspecialchars_decode($result->logistic_member_name) . '<br>';
                echo '<b>সাক্ষীগন: </b>' . htmlspecialchars_decode($result->witness_member_name) . '<br><br>';
                // echo '<b>PRIVATE BUSINESS</b><br>'.htmlspecialchars_decode($result->private_business).'<br>';
                echo '<b>সাধারণ আলোচ্য বিষয়</b><br>' . htmlspecialchars_decode($result->public_business) . '<br>';
                echo '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($result->result_of_deliberation) . '<br>';
            }
            ?>
        </div>
    </div>
    <?php if ($result->is_issued !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
