<?php
require_once('header.php');
require_once('aside_left.php');
require_once('menu.php');
?>


<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("parliament_popup.php?parliament_id=" + p, "", "dialogTop:325px;dialogLeft:400px;dialogWidth:470px;dialogHeight:190px;status: No; ");
    }

    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
    }
</script>

<?php
require_once('../model/parliament_info.php');

$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}
?>
<div id="head_info" style="margin-top: 10px; margin-bottom: 10px;">
    <?php
    echo $parliament_information;
    ?>
</div>
<div style="padding-bottom:10px;">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
    <br/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($id, $parliament);
    echo $parliament_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php
require_once('aside_right.php');
require_once('footer.php');
?>	 