<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
</head>
<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/document_registration_info.php');
?>
<?php
$xml = simplexml_load_file("xml/document_key_areas_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date_start = $information->doc_reg_date_start;
        $doc_title = $information->doc_title;
        $recommendations = $information->recommendations;
        $result_forcasting = $information->result_forcasting;
        $to = $information->to;
        $key_area = $information->key_area;
    }
}
?>
<?php
$user_id = 1;
//echo $_GET['ministry_member_id'];
if ($_GET['doc_reg_id'] != NULL) {
    $result = $document_registration_info->editrow(array($_GET['doc_reg_id'], $user_id));
}
?>
<?php
require_once('../model/document_registration_info_key.php');
?>
<?php
$user_id = 1;
//echo $_GET['ministry_member_id'];
if ($_GET['doc_reg_id'] != NULL) {
    $result2 = $document_registration_info_key->editrow(array($_GET['doc_reg_id'], $user_id));
}
?>
<body>
<form id="formID" class="formular" name="overlay_form" method="post">
    <h2><?php echo $headingpopup; ?></h2>
    <input name="doc_reg_id" type="hidden"
           value="<?php echo isset($result->doc_reg_id) ? $result->doc_reg_id : ''; ?>"/>

    <div style="width:820px;">
        <div>
            <div class="columnA"><label><?php echo $doc_reg_no; ?> * </label></div>
            <div class="columnB"><input id="name" type="text" name="doc_reg_no"
                                        value="<?php echo isset($result->doc_reg_no) ? $result->doc_reg_no : ''; ?>"
                                        readonly="true"/></div>
            <div class="columnA" style="text-align:right;"><label><?php echo $doc_reg_date_start; ?> * </label></div>
            <div class="columnB"><input id="name" type="text" name="doc_reg_date"
                                        value="<?php echo isset($result->doc_reg_date) ? $result->doc_reg_date : ''; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $doc_title; ?> * </label></div>
            <div class="columnB"><textarea name='doc_title' rows="15" cols="120"
                                           style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"
                                           readonly="readonly"><?php echo isset($result->doc_title) ? trim($result->doc_title) : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $key_area; ?> * </label></div>
            <div class="columnB">
                <textarea name='key_area' rows="15" cols="120" class='validate[required] text-input bangla'
                          style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"><?php echo isset($result2->key_area) ? $result2->key_area : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $recommendations; ?> * </label></div>
            <div class="columnB">
                <textarea name='recommendations' rows="15" cols="120" class='validate[required] text-input bangla'
                          style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"><?php echo isset($result2->recommendations) ? $result2->recommendations : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $result_forcasting; ?> * </label></div>
            <div class="columnB">
                <textarea name='result_forcasting' rows="15" cols="120" class='validate[required] text-input bangla'
                          style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"><?php echo isset($result2->result_forcasting) ? $result2->result_forcasting : ''; ?>
                </textarea>

            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div class="divclear"></div>
    </div>
    <div style="text-align:center; float:left; width:65%">
        <input type="submit" value="Close" name="btn_close" class="popupbutton simplemodal-wrap"/>
        &nbsp;&nbsp;<input type="button" onclick='CloseIt();' value="Refresh" class="popupbutton simplemodal-close"/>
        &nbsp;&nbsp;<input id="send" name="btn_save_key" type="submit" value="Save" class="popupbutton"/>


    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
