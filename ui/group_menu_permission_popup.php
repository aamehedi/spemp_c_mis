<?php
session_start();
require_once('../model/group_menu_permission.php');
require_once('../model/group.php');
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link type='text/css' href='css/grid.css' rel='stylesheet' media='screen'/>
    <!-- Contact Form CSS files -->
    <script type="text/javascript" language="javascript">
        function popupwinid(p, q, r, s) {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("divgroup").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "group_menu_permission_data.php?group_id=" + p + "&menu_id=" + q + "&type=" + r + "&status=" + s, true);
            xmlhttp.send();
        }
    </script>
    <style>
        tr:nth-child(odd) {
            background-color: #F2F1F0;
            color: #ffffff;
        }
    </style>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<?php
if ($_GET['group_id'] != NULL) {
    $result = $group->editrow(array($_GET['group_id']));
}
?>
<body>
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo 'Group Menu Permission'; ?></h2>
    </br>
    <div>
        <div>
            <div class="columnA"><label><?php echo 'Group ID'; ?> </label></div>
            <div class="columnB"><input id="name" type="text" name="group_id" value="<?php echo $result->group_id; ?>"
                                        readonly="true"/></div>
            <div class="columnA"><label><?php echo '&nbsp;&nbsp;&nbsp;Group Name' ?></label></div>
            <div class="columnB">
                <input id="name" type="text" name="group_name" value="<?php echo $result->group_name; ?>"
                       readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div id="divgroup">
            <?php
            echo $group_menu_permission->getgrid($_GET['group_id']);
            ?>
        </div>
</form>
</body>
</html>
