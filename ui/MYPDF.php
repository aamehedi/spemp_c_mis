<?php

require_once 'tcpdf/tcpdf.php';

class MYPDF extends TCPDF
{
    protected $confData;
    public $fontName;

    //Page header
    public function Header()
    {


        // Set font
        $this->setFontSubsetting(true);
        if ($this->confData['isDefault']) {
            // Logo
            $image_file = 'img/logo.png';
            $this->Image($image_file, 10, 10, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

            $this->SetFont($this->fontName, 'B', 20, 'B', 'false');
            // $this->SetFont($this->fontName, 'BI', 20, '', 'false');
            // $this->SetFont('Nikosh', '', 20, '', 'false');
            // $this->setFontSpacing(1.7);

            // $this->SetTextColor(0, 0, 0);
            // $this->SetFont('times', 'B', 20);
            // $this->SetFont('times', 'B', 20);

            // Committee Name
            // $this->Cell(78, 30, $this->confData['committeeName'], 'TBRL', 1, 'L', false, '', 0, false, 'C', 'B');
            $this->MultiCell(90, 10, $this->confData['committeeName'] . ' ' . $this->confData['parliamentName'], false, 'L', false, 0, 43, 13);

            // $this->Cell(0, 30, 'পাব্লিজ', 0, 1, 'C', false, '', 0, false, 'C', 'B');
            // $this->Cell(0, 30, 'পাবলিক আ্যকাউন্ট কমিটি', 0, 1, 'C', false, '', 0, false, 'C', 'B');


            // $this->SetFont($this->fontName, 'B', 18);
            // Report Name
            // $this->Cell(0, 7, , 0, 1, 'C', false, '', 0, false, 'C', 'T');
            // $this->Ln();
            // $this->Ln();
            $this->MultiCell(0, 25, '');
            $this->MultiCell(0, 10, $this->confData['reportName'], false, 'C', false, 1);
        }
        $this->SetFont($this->fontName, '');
    }

    function __construct($PDF_PAGE_ORIENTATION, $PDF_UNIT, $PDF_PAGE_FORMAT, $true, $charSet, $condition, $confData)
    {
        parent::__construct($PDF_PAGE_ORIENTATION, $PDF_UNIT, $PDF_PAGE_FORMAT, $true, $charSet, $condition);
        $this->confData = $confData;
        // create new PDF document

        // set document information
        $this->SetCreator(PDF_CREATOR);
        // $this->SetAuthor('Nicola Asuni');
        $this->SetTitle($this->confData['pageTitle']);
        $this->setCellHeightRatio(0.8);
        // $this->SetSubject('TCPDF Tutorial');
        // $this->SetKeywords('TCPDF, PDF, example, test, guide');

        // set default header data
        // $this->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
        // $this->SetHeaderData('img/logo.png', 100, ''.'', PDF_HEADER_STRING, array(255,255,255), array(255,255,255));
        // $this->setFooterData(array(0,64,0), array(0,64,128));

        // set header and footer fonts
        // $this->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        // $this->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        // set default monospaced font
        // $this->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        // set margins
        if ($this->confData['isDefault']) {
            $this->SetMargins(PDF_MARGIN_LEFT, 60, PDF_MARGIN_RIGHT);
        } else {
            $this->SetMargins(PDF_MARGIN_LEFT, 20, PDF_MARGIN_RIGHT);
        }


        // $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $this->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        // var_dump($this->confData);
        if ($this->confData['language'] === 'english') {
            $this->fontName = 'cambria';

            // $this->fontName = $this->addTTFfont(K_PATH_FONTS.'georgiab.ttf', 'OpenType', 'B', 32);
        } else {
            // require_once 'fonts/sutonnymj.php';
            // $this->AddFont('SolaimanLipi', '', 'fonts/solaimanlipi.php');

            // $this->fontName = 'solaimanlipi';
            $this->fontName = $this->addTTFfont(K_PATH_FONTS . 'kalpurush.ttf', '', '', 32);


        }
        // $this->SetFont($this->fontName, '', 14, '', true);
        // $this->SetFont($this->fontName);
        // $this->SetFont('solaimanlipi', '', 20, '', 'false');


    }
}
