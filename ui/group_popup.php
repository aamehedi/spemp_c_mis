<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/group.php');
if ($_GET['group_id'] != NULL) {
    $result = $group->editrow(array($_GET['group_id']));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body>

<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2>
        <?php if ($_SESSION['language_id'] === '1') : ?>
            Create New Group
        <?php else : ?>
            নতুন গ্রুপ তৈরী করুন
        <?php endif; ?>
    </h2>
    </br>
    <div>
        <div>
            <div class="columnA"><label>
                    <?php if ($_SESSION['language_id'] === '1') : ?>
                        Group ID
                    <?php else : ?>
                        গ্রুপ আইডি
                    <?php endif; ?>
                </label></div>
            <div class="columnB"><input id="name" type="text" name="group_id"
                                        value="<?php echo isset($result->group_id) ? $result->group_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label>
                    <?php if ($_SESSION['language_id'] === '1') : ?>
                        Group Name
                    <?php else : ?>
                        গ্রুপের নাম
                    <?php endif; ?>
                </label></div>
            <div class="columnB">
                <input id="name" type="text" name="group_name" class='validate[required,maxSize[80]] text-input bangla'
                       value="<?php echo isset($result->group_name) ? $result->group_name : ''; ?>"/></div>
            <div class="divclear"></div>
        </div>
        <br/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>

                &nbsp;&nbsp;&nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
