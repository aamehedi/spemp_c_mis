<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
$xml = simplexml_load_file("xml/parliament_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $id = $information->id;
        $parliament = $information->parliament;
        //echo $heading;
    }
}
?>
<?php
require_once('../model/parliament_info.php');
//echo  $_GET['parliament_id'];
$user_id = 1;
if ($_GET['id'] != NULL) {
    $result = $parliament_info->editrow(array($_GET['id']));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php
    if ($_SESSION['language_id'] == 2) {
        echo '<script src="js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
					<script src="js/avro-1.1-beta.min.js" type="text/javascript" charset="utf-8"></script>	
					<script type="text/javascript" charset="utf-8">
						$(function(){
							$(".bangla").avro({"bn":true}, function(isBangla) {
								if(isBangla) {
									$(".mode").text("English");
								} else {
									$(".mode").text("Bangla");
								}
							});
						});
					</script>';
    }
    ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<body>
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo "Insert/ Update Other Ministry & Organization" //echo  $heading ?></h2>
    </br>
    <div>
        <div>
            <div class="columnA"><label></label></div>
            <div class="columnB"><input id="name" type="hidden" name="parliament_id"
                                        value="<?php echo isset($result->parliament_id) ? $result->parliament_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo 'Ministry / Organization'; //echo  $parliament;?></label></div>
            <div class="columnB"><input id="name" type="text" class="bangla" name="parliament"
                                        value="<?php echo isset($result->parliament) ? $result->parliament : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                &nbsp;&nbsp;<input id="send" name="btn_close" type="submit" value="Close" class="popupbutton"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
