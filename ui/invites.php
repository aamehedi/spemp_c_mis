<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("invites_popup.php?invitees_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:730px;dialogHeight:400px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
        else {

        }
    }
</script>

<?php
require_once('../model/invitees_info.php');
?>
<?php
$xml = simplexml_load_file("xml/invitees_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $organization_member_name = $information->organization_member_name;
        $organization_name = $information->organization_name;
        $comments = $information->comment;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
    }
}
?>


<div id="head_info">
    <?php
    echo $heading;
    ?>
</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($organization_member_name, $organization_name, $designation);
    echo $invitees_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>