<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/committee_secretariat_member_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['member_id'] != NULL) {
    $result = $committee_secretariat_member_info->editrow(array($_GET['member_id'], $user_id));
    $resultUser = $committee_secretariat_member_info->getNotAddedUser(empty($result->user_id) ? '0' : $result->user_id);
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/committee_secretariat_member.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $user_name = $information->user_name;
            $member_name = $information->member_name;
            $contact_person = $information->contact_person;
            $designation = $information->designation;
            $committee_secretariat = $information->committee_secretariat;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $comments = $information->comments;
            $active = $information->active;
        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF; width: 850px !important;">
<form id="formID" class="formular" name="overlay_form" method="post" action="#" style="width: 800px !important">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?> </label></div>
            <div class="columnB"><input id="name" type="text" name="member_id"
                                        value="<?php echo isset($result->member_id) ? $result->member_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $member_name; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[150]] text-input bangla'
                                        name='member_name'
                                        value="<?php echo isset($result->member_name) ? $result->member_name : ''; ?>"
                                        style="font-size: 11px;width: 370%;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $designation; ?> *</label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='designation' class='validate[required,maxSize[80]] text-input bangla'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                </div>
                <div style="float:left;width:150px;padding-left:30px;">
                    <?php echo $committee_secretariat; ?> *
                </div>
                <div style="float:left;">
                    <?php
                    $language_id = $_SESSION['language_id'];
                    $id = isset($result->committee_secretariat_id) ? $result->committee_secretariat_id : '';
                    echo $committee_secretariat_member_info->comboview($language_id, $id);
                    ?>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='phone' class='validate[required] text-input bangla'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:150px;padding-left:30px;">
                    <?php echo $cell_phone; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' name='cell_phone' class='text-input bangla'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='validate[maxSize[20]] text-input bangla' name='fax'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:150px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[maxSize[150],custom[email]] text-input bangla' name='email'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?></label></div>
            <div class="columnB">
                <textarea class='validate[maxSize[250]] text-input bangla' name="address"
                          style="font-size: 12px;width: 450px;"><?php echo isset($result->address) ? $result->address : ''; ?></textarea>
            </div>
            <div class="divclear"></div>

        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB"><textarea type='text' class='validate[maxSize[250]] text-input bangla' name='comments'
                                           style="font-size: 12px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $active; ?> *</label></div>
            <div class="columnB">
                <?php
                $id = isset($result->is_active) ? $result->is_active : '1';
                if ($id == 1) {
                    echo '<input type="radio" name="active" value="1" style="display:inline;" checked>Active';
                    echo '<input type="radio" name="active" value="0" style="display:inline;" > Inactive';
                } elseif ($id == 0) {
                    echo '<input type="radio" name="active" value="1" style="display:inline;">Active';
                    echo '<input type="radio" name="active" value="0" style="display:inline;" checked> Inactive';
                }
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>

        <input name="user_id" type="hidden" value="' '"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
