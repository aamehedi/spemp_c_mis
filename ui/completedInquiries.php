<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<?php require_once('../model/inquiry_report_info.php'); ?>
<?php require_once('../dal/data_access.php'); ?>


<?php
if (!empty($_GET['inquiry_id'])) {

    // var_dump($meeting_notice_info->editrow(array($_GET['sittingNo'],'1')));
    $resultInquiry = $create_inquiry_info->editrow(array($_GET['inquiry_id'], '1'));
    $data_access = new data_access();
    try {
        $result = $data_access->data_reader("tbl_inquiry_report_master_search", array('', '', "''", '', "'" . $resultInquiry['inquiry_no'] . "'", "'" . $_SESSION['language_id'] . "'", "'" . $_SESSION['committee_id'] . "'", 1, ''));
        $result = mysqli_fetch($result);
    } catch (Exception $e) {
        return "Erorr: " . $e->getMessage();
    }
    try {
        $resultFollowUp = $data_access->data_reader("tbl_follow_up_master_search", array('', '', '', '', "'" . $resultInquiry['inquiry_no'] . "'", "'" . $_SESSION['language_id'] . "'", $_SESSION['committee_id'], 1, ''));
        $resultFollowUp = mysqli_fetch($resultFollowUp);
    } catch (Exception $e) {
        return "Erorr: " . $e->getMessage();
    }
    // $resultParliament = mysqli_fetch_object($data_access->data_reader('tbl_parliament_gid', array($result->parliament_id)));
    // $resultSession = mysqli_fetch_object($data_access->data_reader('tbl_session_gid', array($result->session_id)));
    $resultSittings = $data_access->data_reader("tbl_meeting_notice_master_search_dashboard", array("''", "''", "''", 0, "''", "'" . $_SESSION['committee_id'] . "'", "'" . $_SESSION['language_id'] . "'", "'" . $_SESSION['user_id'] . "'", "'" . $_GET['inquiry_id'] . "'", "''"));
    // var_dump($result);
    // $result1 = $meeting_notice_info->gridview_token_pree_popolated_inqu1iry($_GET['sittingNo']);
    // $committeeReference = substr($result1, strpos($result1, 'name: '') + 7, strlen($result1) - (strpos($result1, 'name: '') + 10));
    // $resultCommittee = $meeting_notice_info->raw_token_pree_popolated_committee($_GET['sittingNo'],$result->sub_committee_id,$_SESSION['committee_id'],$_SESSION['language_id']);
    // $resultWitness = $meeting_notice_info->raw_token_pree_popolated_witness($_GET['sittingNo']);
    // $resultLogistic = $meeting_notice_info->raw_token_pree_popolated_logistic($_GET['sittingNo']);
    // $resultNotification = $meeting_notice_info->raw_token_pree_popolated_notification($_GET['sittingNo']);
    // $resultAdviser = $meeting_notice_info->raw_token_pree_popolated_adviser($_GET['sittingNo']);
    // $resultOfficer = $meeting_notice_info->raw_token_pree_popolated_officer($_GET['sittingNo']);

    $attributes = array();
    if ($_SESSION['language_id'] === '2') {
        $attributes = array(
            'inquiry_report' => 'তদন্ত প্রতিবেদন',
            'follow_up_report' => 'তদন্ত অগ্রগতি প্রতিবেদন',
            'created_on' => 'তদন্ত তারিখ',
            'approved_by_committee_on' => 'অনুমোদন তারিখ',
            'tabled_on' => 'টেবিল তারিখ');
        ?>
        <style type='text/css'>
            table.mGrid tr td {
                font-size: 20px !important;
            }
        </style>
    <?php


    } else {
        $attributes = array(
            'inquiry_report' => 'Inquiry Report',
            'follow_up_report' => 'Follow up Report',
            'created_on' => 'created on',
            'approved_by_committee_on' => 'Approved by Committee on',
            'tabled_on' => 'Tabled on');
    }


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

    ?>
    <style type='text/css'>
        .back {
            -moz-box-shadow: inset 0px 1px 0px 0px #d197fe;
            -webkit-box-shadow: inset 0px 1px 0px 0px #d197fe;
            box-shadow: inset 0px 1px 0px 0px #d197fe;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #a53df6), color-stop(1, #7c16cb));
            background: -moz-linear-gradient(center top, #a53df6 5%, #7c16cb 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a53df6', endColorstr='#7c16cb');
            background-color: #a53df6;
            -webkit-border-top-left-radius: 9px;
            -moz-border-radius-topleft: 9px;
            border-top-left-radius: 9px;
            -webkit-border-top-right-radius: 9px;
            -moz-border-radius-topright: 9px;
            border-top-right-radius: 9px;
            -webkit-border-bottom-right-radius: 9px;
            -moz-border-radius-bottomright: 9px;
            border-bottom-right-radius: 9px;
            -webkit-border-bottom-left-radius: 9px;
            -moz-border-radius-bottomleft: 9px;
            border-bottom-left-radius: 9px;
            text-indent: 0;
            border: 1px solid #9c33ed;
            display: inline-block;
            color: #ffffff;
            font-family: Arial;
            font-size: 19px;
            font-weight: bold;
            font-style: normal;
            height: 35px;
            line-height: 35px;
            width: 113px;
            text-decoration: none;
            text-align: center;
            text-shadow: 1px 1px 0px #7d15cd;
            margin-top: 1%;

        }

        .back:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7c16cb), color-stop(1, #a53df6));
            background: -moz-linear-gradient(center top, #7c16cb 5%, #a53df6 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7c16cb', endColorstr='#a53df6');
            background-color: #7c16cb;
        }

        .back:active {
            position: relative;
            top: 1px;
        }

        .sittingNo {
            width: 40%;
            background-color: #7d15cd;
            color: #FFFFFF;
            height: 35px;
            float: left;
            margin-right: 48.9%;
            text-align: center;
            font-size: 1.5em;
            font-weight: bold;
            display: table;
            overflow: hidden;
            margin-top: 1%;
        }

        table {
            margin-top: 0px !important;
        }

        a {
            color: blue;
        }

        .header_contain > a {
            color: #FFFFFF;
        }

        td {
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function viewnoticeid(p) {
            //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
            window.showModalDialog("meeting_notice_popup_view_isssued.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
        function viewBriefingNote(p) {
            //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
            window.showModalDialog("briefing_note_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
        function viewRecordOfDecision(p) {
            window.showModalDialog("record_decisions_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        }

    </script>
    <div class='sittingNo'>
        <div style='display: table-cell; vertical-align: middle;'>
            Inquiry No. <?php echo $result->inquiry_no; ?>
        </div>
    </div>
    <a href='index.php' class='back'>BACK</a>
    <table class='mGrid'>

        <tr>
            <td><?php echo $attributes['inquiry_report']; ?></td>
            <td><?php echo $attributes['created_on']; ?></td>
            <td><?php echo htmlspecialchars_decode($result->report_date); ?></td>
            <td><?php echo $attributes['approved_by_committee_on']; ?></td>
            <td><?php echo htmlspecialchars_decode($result->approved_date); ?></td>
            <td><?php echo $attributes['tabled_on']; ?></td>
            <td><?php echo htmlspecialchars_decode($result->report_date); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['follow_up_report']; ?></td>
            <td><?php echo $attributes['created_on']; ?></td>
            <td><?php echo htmlspecialchars_decode($resultFollowUp->followup_date); ?></td>
            <td><?php echo $attributes['approved_by_committee_on']; ?></td>
            <td><?php echo htmlspecialchars_decode($result->approved_date); ?></td>
            <td><?php echo $attributes['tabled_on']; ?></td>
            <td></td>
        </tr>

    </table>
    <br/>
    <table class="mGrid" cellspacing="0">
        <tbody>
        <tr>
            <th>Sitting No.</th>
            <th>Date Held</th>
            <th>Time Held</th>
            <th>Sitting Room</th>
            <th>View Notice</th>
            <th>Briefing Note</th>
            <th>View Record of Decision</th>
        </tr>
        <?php
        // var_dump($resultSittings);
        while ($rowSittings = mysqli_fetch_assoc($resultSittings)) {

            ?>
            <tr>
                <td>
                    <a href="sitting.php?sittingNo=<?php echo $rowSittings['metting_notice_id']; ?>"><?php echo $rowSittings['sitting_no']; ?></a>
                </td>
                <td><?php echo ($_SESSION['language_id'] === '1') ? $rowSittings['en_date'] : $rowSittings['bn_date']; ?></td>
                <td><?php echo $rowSittings['venue_name']; ?></td>
                <td><?php echo $rowSittings['inquiry_no']; ?></td>
                <td><?php echo $rowSittings['is_view_notice']; ?></td>
                <td><?php echo $rowSittings['is_briefing_note']; ?></td>
                <td><?php echo $rowSittings['is_record_of_dec']; ?></td>

            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php

} else {
    require_once '../dal/data_access.php';
    $data_access = new data_access();
    $result = $data_access->data_reader('tbl_inquiry_master_search_raw', array($_SESSION['language_id'], $_SESSION['committee_id'], 1));
    ?>
    <style type='text/css'>
        a {
            color: blue;
        }

        .header_contain > a {
            color: #FFFFFF;
        }
    </style>
    <table class='mGrid' cellspacing='0'>
        <tbody>
        <tr>
            <th>Inquiry No.</th>
            <th>Inquiry Details</th>
            <th>Total Sitting held</th>
            <th>Last Sitting held</th>
        </tr>
        <?php
        while ($row = mysqli_fetch_assoc($result)) {
            ?>
            <tr>
                <td>
                    <a href='<?php echo 'completedInquries.php?inquiry_id=' . $row['inquiry_id']; ?>'><?php echo $row['inquiry_no']; ?></a>
                </td>
                <td><?php echo $row['inquiry_title']; ?></td>
                <td><?php echo $row['sittingHeld']; ?></td>
                <td><?php echo $row['lastSitting']; ?></td>
            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php

}

function stripMysqliObject($mysqliObject)
{
    $first = true;
    $result = '';
    while ($row = mysqli_fetch_array($mysqliObject)) {
        if (!$first) {
            $result .= ', ';
        } else {
            $first = false;
        }
        $result .= htmlspecialchars_decode($row[1]);
    }
    return $result;
}