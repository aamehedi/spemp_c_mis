<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">
    <script type='text/javascript' src='js/jquery.js'></script>
    <script src="codebase_m/dhtmlxcalendar.js"></script>
    <style>
        #calendar,
        #calendar2,
        #calendar3,
        #calendar4 {
            border: 1px solid #909090;
            font-family: Tahoma;
            font-size: 12px;
        }
    </style>
    <script>
        var myCalendar;
        function doOnLoad() {
            myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);
        }
    </script>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>

    <?php
    session_start();
    //echo $_SESSION['language_id'];
    ?>
    <?php
    require_once('../model/inquiry_scheduling_info.php');
    ?>
    <?php
    $user_id = $_SESSION['user_id'];
    //echo $_GET['doc_reg_id'];
    if ($_GET['inquiry_id'] != NULL) {
        $result = $inquiry_scheduling_info->editrow(array($_GET['inquiry_id'], $user_id));
    }
    ?>
    <?php
    require_once('../model/inquiry_scheduling_info.php');
    ?>
    <?php
    $xml = simplexml_load_file("xml/inquiry_scheduling_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $doc_reg_no = $information->doc_reg_no;
            $id = $information->id;
            $doc_reg_date = $information->doc_reg_date;
            $doc_title = $information->doc_title;
            $designation = $information->designation;
            $doc_name = $information->doc_name;
            $inquery_no = $information->inquery_no;
            $inquery_title = $information->inquery_title;
            $proposed_date = $information->proposed_date;
            $schedule_date = $information->schedule_date;
        }
    }
    ?>
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
    <script>
        $(document).ready(function () {
            $("#btn2").click(function () {
                $("#btn1").append("<div><div style='float:left;width:30%;'><input type='hidden' name='schedule_no[]' value='0' /><input type='text' name='schedule_date[]' id='calendar2'/></div><div  style='float:left;width:45%;'></div><div class='divclear'></div></div>");
            });
        });
    </script>
</head>
<body style="background-color:#FFFFFF;" onLoad="doOnLoad();">
<form id="formID" class="formular" method="post" action="#">
    <input type="hidden" name="schedule_id" value="0">

    <h2><?php echo $headingpopup; ?></h2>
    <input name="inquiry_id" type="hidden"
           value="<?php echo isset($result->inquiry_id) ? $result->inquiry_id : ''; ?>"/>
    </br>
    <div style="width:820px;">
        <div>
            <div class="columnA"><label><?php echo $inquery_no; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="inquiry_no"
                                        value="<?php echo isset($result->inquiry_no) ? $result->inquiry_no : ''; ?>"
                                        readonly="true"/></div>
            <div class="columnA" style="text-align:right;"><label><?php echo $proposed_date; ?></label></div>
            <div class="columnB">
                <input id="doc_reg_date" type="text" name="doc_reg_date"
                       value="<?php echo isset($result->proposed_date) ? $result->proposed_date : ''; ?>"
                       readonly="true"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $inquery_title; ?></label></div>
            <div class="columnB">
                <textarea name='doc_title' rows="15" cols="120"
                          style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"
                          readonly="readonly"><?php echo isset($result->inquiry_title) ? strip_tags(htmlspecialchars_decode($result->inquiry_title)) : htmlspecialchars_decode(''); ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="float:left;width:45%;"><label><?php echo $schedule_date; ?></label></div>
            <div style="float:left;width:45%;">
                <input type="button" name="basic" id="btn2" value="+"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div style="overflow-y:scroll; height:100px;" id="btn1">
            <?php
            if ($_GET['inquiry_id'] != NULL) {
                $inquiry_id = $_GET['inquiry_id'];
                $user_id = $_SESSION['user_id'];
                echo $inquiry_scheduling_info->gridviewdetails($inquiry_id, $user_id);
            }
            ?>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
    </br>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

    <div class="divclear"></div>
    </div>
    <div style="text-align:center; float:left; width:65%">
        <input type="submit" value="Close" name="btn_close" class="popupbutton simplemodal-wrap"/>
        &nbsp;&nbsp;<input type="button" onclick='CloseIt();' value="Refresh" class="popupbutton simplemodal-close"/>
        &nbsp;&nbsp;<input id="send" name="btn_save_inquery" type="submit" value="Save" class="popupbutton"/>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
