<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/organization_member_info.php');
?>
<?php
$user_id = 1;
//echo $_GET['organization_member_id'];
if ($_GET['id'] != NULL) {
    $result = $organization_member_info->editrow(array($_GET['id'], $user_id));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <?php
    $xml = simplexml_load_file("xml/invitees_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $organization_member_name = $information->organization_member_name;
            $organization_name = $information->organization_name;
            $comments = $information->comment;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
        }
    }
    ?>
</head>
<body>

<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div>
            <div class="columnA"></div>
            <div class="columnB"><input id="name" type="hidden" name="organization_member_id"
                                        value="<?php echo isset($result->organization_member_id) ? $result->organization_member_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $organization_name; ?> *</label>
            </div>
            <div class="columnB">
                <input type='text' name='organization_member_name'
                       value="<?php echo isset($result->organization_member_name) ? $result->organization_member_name : ''; ?>">
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $organization_member_name; ?> *</label>
            </div>
            <div class="columnB">
                <input type='text' name='organization_member_name'
                       value="<?php echo isset($result->organization_member_name) ? $result->organization_member_name : ''; ?>">
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $designation; ?> *</label>
            </div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='designation'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='phone' value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' name='cell_phone'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='fax' value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' name='email'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?> *</label></div>
            <div class="columnB">
                <textarea type='text' name='address' style="font-size: 11px;width: 450px;">
                    <?php echo isset($result->address) ? $result->address : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB">
                <textarea type='text' name='comments' style="font-size: 11px;width: 450px;">
                    <?php echo isset($result->comments) ? $result->comments : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                &nbsp;&nbsp;<input id="send" name="btn_close" type="submit" value="close" class="popupbutton"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
