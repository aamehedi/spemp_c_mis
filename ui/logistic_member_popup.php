<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/logistic_member_info.php');
?>
<?php
$user_id = $_SESSION['user_id'];
if ($_GET['logistic_member_id'] != NULL) {
    $result = $logistic_member_info->editrow(array($_GET['logistic_member_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/logistic_member_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $logistic_member_name = $information->logistic_member_name;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $comments = $information->comment;
            $is_active = $information->is_active;
            $inactive = $information->inactive;

        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body style="background:#FFFFFF;">

<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2><?php echo $heading; ?></h2>
    </br>
    <div style="width:820px;">
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="logistic_member_id"
                                        value="<?php echo isset($result->logistic_member_id) ? $result->logistic_member_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $logistic_member_name; ?> *</label>
            </div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[80]] text-input bangla'
                                        name='logistic_member_name'
                                        value="<?php echo isset($result->logistic_member_name) ? $result->logistic_member_name : ''; ?>"
                                        style="font-size: 11px;width: 340%;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $designation; ?> *</label>
            </div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='validate[required,maxSize[100]] text-input bangla' name='designation'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='text-input bangla' name='phone'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[required] text-input bangla' name='cell_phone'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='validate[maxSize[20]] text-input bangla' name='fax'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[custom[email],maxSize[80]] text-input bangla' name='email'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?> *</label></div>
            <div class="columnB">
                <textarea type='text' class='validate[required,maxSize[80]] text-input bangla' name='address'
                          style="font-size: 11px;width: 450px;"><?php echo isset($result->address) ? $result->address : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB">
                <textarea type='text' class='text-input bangla' name='comments'
                          style="font-size: 11px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $is_active; ?></label></div>
            <div class="columnB">
                <?php
                $is_active_value = isset($result->is_active) ? $result->is_active : 1;
                if ($is_active_value == 0) {
                    echo '<input type="radio" name="is_active" value="1" style="display:inline;" > ' . $is_active;
                    echo '<input type="radio" name="is_active" value="0" style="display:inline;" checked> ' . $inactive . '&nbsp;&nbsp;';
                } elseif ($is_active_value == 1) {
                    echo '<input type="radio" name="is_active" value="1" style="display:inline;" checked> ' . $is_active;
                    echo '<input type="radio" name="is_active" value="0" style="display:inline;"> ' . $inactive . '&nbsp;&nbsp;';
                }
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
