<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
require_once('../model/user.php');
require_once('../model/group.php');
require_once('../dal/data_access.php');
$data_access = new data_access();
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link type='text/css' href='css/grid.css' rel='stylesheet' media='screen'/>
    <!-- Contact Form CSS files -->
    <script type="text/javascript" language="javascript">
        function popupwinid(p, q, r) {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("divgroup").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "user_group_popup_data.php?group_id=" + p + "&user_id=" + q + "&status=" + r, true);
            xmlhttp.send();
        }

        function checkParliamentMember() {
            var selectGroup = document.getElementById("group_id");
            if (selectGroup.options[selectGroup.selectedIndex].text == 'Parliament Member') {
                document.getElementById("divParliamentMember").style.display = 'block';
            } else {
                document.getElementById("divParliamentMember").style.display = 'none';
            };
        }
    </script>
    <style>
        tr:nth-child(odd) {
            background-color: #F2F1F0;
            color: #ffffff;
        }
    </style>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 250px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<?php
if ($_GET['user_id'] != NULL) {
    $result = $user->editrow(array($_GET['user_id']));
//    var_dump($result);
}
?>
<body>
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo 'User Group Permission'; ?></h2>
    </br>
    <div>


        <div>
            <div class="columnA"><label><?php echo '&nbsp;&nbsp;&nbsp;User Name' ?></label></div>
            <div class="columnB">
                <input type="hidden" name="user_id" value="<?php echo $_GET['user_id']?>"/>
                <input id="name" type="text" name="user_name" value="<?php echo $result->user_name; ?>"
                       readonly="true"/></div>
            <div class="divclear"></div>
        </div>

        <div id="divParliamentMember" style="display: none;">
            <div class="columnA"><label><?php echo '&nbsp;&nbsp;&nbsp;Parliament Member' ?> *</label></div>
            <div class="columnB">
                <select name="parliament_member_id" class='validate[required] text-input bangla'>
                    <?php
                    $resultParliamentMember = $data_access->data_reader('tbl_parliament_member_get_user', array());
                    while ($rowParliamentMember = mysqli_fetch_assoc($resultParliamentMember)) {
                        ?>
                        <option
                            value="<?php echo $rowParliamentMember['member_id']; ?>"><?php echo $rowParliamentMember['member_name']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div class="columnA"><label><?php echo '&nbsp;&nbsp;&nbsp;Group' ?></label></div>
            <div class="columnB">
                <?php
                echo '<select name="group_id" id="group_id" onchange="checkParliamentMember()" >';
                echo $group->getGroup($result->group_id);
                echo '</select>';
                ?>
            <div class="divclear"></div>
        </div>

        <div>
            <div>
                <div class="columnAbutton">
                    <input id="send" name="btn_group_change" type="submit" value="Save" class="popupbutton"/>
                </div>
                <div class="columnBbutton">
                    <script>
                        function myFunctionC() {
                            window.close();
                        }
                    </script>
                    &nbsp;&nbsp;
                    <button onClick="myFunctionC()" class="popupbutton">Close</button>
                </div>
                <div class="divclear"></div>
            </div>
            <div class="divclear"></div>
        </div>
</form>
</body>
</html>
