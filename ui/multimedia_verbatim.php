<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css"/>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
    $(document).ready(function () {
        $("#start_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
        $("#end_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
    });
    function calculateWeekend(date) {
        var weekend = date.getDay() == 5 || date.getDay() == 6;
        return [true, weekend ? 'myweekend' : ''];
    }
    ;
</script>
<script type='text/javascript'>
    function multimedia(p) {
        //$('<div></div>').load('multimedia_verbatim_popup.php?parliament_id='+p).modal();
        window.showModalDialog("multimedia_verbatim_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        return false;
    }
</script>
<?php
require_once('../model/multimedia_verbatim_info.php');
require_once('../model/meeting_notice_info.php');
?>
<?php
$xml = simplexml_load_file("xml/multimedia_verbatim_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
        $result_of_deliberation = $information->result_of_deliberation;
        $chair = $information->chair;
        $date_start = $information->date_start;
        $data_end = $information->data_end;
    }
}
?>
<link href="Style/home.css" rel="stylesheet" type="text/css"/>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php
    if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['committe_id']) || isset($_GET['ref_no']) || isset($_GET['sitting_id'])) {
        echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
    } else {
        echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
    }
    ?>
    <fieldset>
        <legend>Search Criteria</legend>
        <div>
            <div style='float:left;width:22.5%;'><?php echo $ref_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="ref_no"/></div>
            <div style='float:left;width:22.5%;'><?php echo $sitting_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="sitting_id"/></div>
            <!--<div style='float:left;width:12.5%;'><input type="button"  name="basic" value="Search" class="newbuttondiv"/></div>
            <div style='float:left;width:12.5%;'><input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbuttondiv"/> </div>-->
            <div style='float:left;width:22.5%;'><?php echo $date_start; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' id="start_date" name="start_date"/>
            </div>
            <div style='float:left;width:22.5%;'><?php echo $data_end; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' id="end_date" name="end_date"/></div>
            <div style='clear:both'></div>
        </div>
        <div style="margin-bottom:4px;">
            <div style='float:left;width:22.5%;'><?php echo $committee; ?></div>
            <div style='float:left;width:25%'>

                <?php
                $id = isset($result->sub_committee_id) ? $result->sub_committee_id : '';
                //echo $result->sub_committee_id;
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $user_id = $_SESSION['user_id'];
                echo '<select name="sub_committee_id">';
                echo $meeting_notice_info->comboview_committee($language_id, $committee_id, $user_id, $id);
                echo '</select>';
                ?>
            </div>
            <div style="float:left;width: 100%"><input type="submit" value="Search" name="search_button"
                                                       class="newbutton"/></div>
            <div style='clear:both'></div>
        </div>
    </fieldset>
    </div>
</form>
<?php
if (isset($_GET['start_date'])) {
    $start_date = $_GET['start_date'];
}
if (isset($_GET['end_date'])) {
    $end_date = $_GET['end_date'];
}
if (isset($_GET['sub_committee_id'])) {
    if (($_GET['sub_committee_id']) == NULL) {
        $sub_committee_id = 0;
    } else
        $sub_committee_id = $_GET['sub_committee_id'];
}
if (isset($_GET['ref_no'])) {
    $ref_no = $_GET['ref_no'];
}
if (isset($_GET['sitting_id'])) {
    $sitting_id = $_GET['sitting_id'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
?>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $committe_id = $_SESSION['committee_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($ref_no, $sitting_no, $date, $venue);
    if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['ref_no']) || isset($_GET['id']) || isset($_GET['sitting_no'])) {

        echo $multimedia_verbatim_info->searchgridview($start_date, $end_date, $ref_no, $sub_committee_id, $sitting_id, $committe_id, $language_id, $user_id, $param);
    } else {
        echo $multimedia_verbatim_info->searchgridview(' ', ' ', ' ', 0, ' ', $committe_id, $language_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>