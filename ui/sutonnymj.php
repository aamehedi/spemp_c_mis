<?php
$type = 'TrueType';
$name = 'SutonnyMJ';
$desc = array('Ascent' => 703, 'Descent' => 297, 'CapHeight' => 703, 'Flags' => 32, 'FontBBox' => '[-658 -315 897 863]', 'ItalicAngle' => 0, 'StemV' => 70, 'MissingWidth' => 500);
$up = -133;
$ut = 20;
$cw = array(
    chr(0) => 500, chr(1) => 500, chr(2) => 500, chr(3) => 500, chr(4) => 500, chr(5) => 500, chr(6) => 500, chr(7) => 500, chr(8) => 500, chr(9) => 500, chr(10) => 500, chr(11) => 500, chr(12) => 500, chr(13) => 500, chr(14) => 500, chr(15) => 500, chr(16) => 500, chr(17) => 500, chr(18) => 500, chr(19) => 500, chr(20) => 500, chr(21) => 500,
    chr(22) => 500, chr(23) => 500, chr(24) => 500, chr(25) => 500, chr(26) => 500, chr(27) => 500, chr(28) => 500, chr(29) => 500, chr(30) => 500, chr(31) => 500, ' ' => 269, '!' => 295, '"' => 347, '#' => 500, '$' => 384, '%' => 751, '&' => 1, '\'' => 153, '(' => 333, ')' => 333, '*' => 500, '+' => 564,
    ',' => 250, '-' => 333, '.' => 250, '/' => 278, '0' => 501, '1' => 438, '2' => 479, '3' => 508, '4' => 497, '5' => 501, '6' => 483, '7' => 504, '8' => 517, '9' => 482, ':' => 278, ';' => 278, '<' => 564, '=' => 564, '>' => 564, '?' => 413, '@' => 819, 'A' => 601,
    'B' => 469, 'C' => 513, 'D' => 521, 'E' => 577, 'F' => 605, 'G' => 580, 'H' => 619, 'I' => 540, 'J' => 635, 'K' => 571, 'L' => 486, 'M' => 495, 'N' => 471, 'O' => 457, 'P' => 408, 'Q' => 452, 'R' => 592, 'S' => 568, 'T' => 772, 'U' => 424, 'V' => 405, 'W' => 523,
    'X' => 415, 'Y' => 450, 'Z' => 570, '[' => 198, '\\' => 399, ']' => 223, '^' => 1, '_' => 477, '`' => 478, 'a' => 433, 'b' => 448, 'c' => 535, 'd' => 593, 'e' => 444, 'f' => 532, 'g' => 472, 'h' => 445, 'i' => 430, 'j' => 516, 'k' => 521, 'l' => 449, 'm' => 509,
    'n' => 441, 'o' => 524, 'p' => 421, 'q' => 450, 'r' => 357, 's' => 301, 't' => 313, 'u' => 1, 'v' => 202, 'w' => 190, 'x' => 202, 'y' => 1, 'z' => 1, '{' => 480, '|' => 511, '}' => 480, '~' => 1, chr(127) => 500, chr(128) => 500, chr(129) => 500, chr(130) => 1, chr(131) => 114,
    chr(132) => 1, chr(133) => 1, chr(134) => 298, chr(135) => 295, chr(136) => 263, chr(137) => 289, chr(138) => 219, chr(139) => 100, chr(140) => 100, chr(141) => 500, chr(142) => 500, chr(143) => 500, chr(144) => 500, chr(145) => 1, chr(146) => 1, chr(147) => 199, chr(148) => 268, chr(149) => 270, chr(150) => 1, chr(151) => 3, chr(152) => 289, chr(153) => 493,
    chr(154) => 543, chr(155) => 193, chr(156) => 1, chr(157) => 500, chr(158) => 500, chr(159) => 335, chr(160) => 384, chr(161) => 1, chr(162) => 1, chr(163) => 1, chr(164) => 456, chr(165) => 120, chr(166) => 1, chr(167) => 170, chr(168) => 241, chr(169) => 1, chr(170) => 1, chr(171) => 1, chr(172) => 1, chr(173) => 1, chr(174) => 444, chr(175) => 532,
    chr(176) => 569, chr(177) => 441, chr(178) => 664, chr(179) => 662, chr(180) => 569, chr(181) => 697, chr(182) => 665, chr(183) => 663, chr(184) => 512, chr(185) => 485, chr(186) => 765, chr(187) => 581, chr(188) => 521, chr(189) => 513, chr(190) => 768, chr(191) => 1, chr(192) => 599, chr(193) => 672, chr(194) => 626, chr(195) => 546, chr(196) => 554, chr(197) => 772,
    chr(198) => 443, chr(199) => 887, chr(200) => 600, chr(201) => 512, chr(202) => 453, chr(203) => 509, chr(204) => 528, chr(205) => 3, chr(206) => 499, chr(207) => 561, chr(208) => 333, chr(209) => 487, chr(210) => 400, chr(211) => 400, chr(212) => 220, chr(213) => 220, chr(214) => 1, chr(215) => 559, chr(216) => 392, chr(217) => 486, chr(218) => 464, chr(219) => 552,
    chr(220) => 550, chr(221) => 536, chr(222) => 700, chr(223) => 490, chr(224) => 535, chr(225) => 650, chr(226) => 619, chr(227) => 558, chr(228) => 555, chr(229) => 524, chr(230) => 199, chr(231) => 656, chr(232) => 1, chr(233) => 585, chr(234) => 497, chr(235) => 690, chr(236) => 618, chr(237) => 524, chr(238) => 680, chr(239) => 497, chr(240) => 635, chr(241) => 702,
    chr(242) => 690, chr(243) => 440, chr(244) => 479, chr(245) => 644, chr(246) => 560, chr(247) => 760, chr(248) => 1, chr(249) => 715, chr(250) => 406, chr(251) => 442, chr(252) => 522, chr(253) => 595, chr(254) => 639, chr(255) => 665);
$enc = 'cp1252';
$file = 'sutonnymj.z';
$originalsize = 64112;
?>
