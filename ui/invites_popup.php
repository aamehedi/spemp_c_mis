<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/invitees_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['invitees_id'] != NULL) {
    $result = $invitees_info->editrow(array($_GET['invitees_id'], $user_id));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
    <?php
    if ($_SESSION['language_id'] == 2) {
        echo '<script src="js/jquery-1.7.min.js" type="text/javascript" charset="utf-8"></script>
					<script src="js/avro-1.1-beta.min.js" type="text/javascript" charset="utf-8"></script>	
					<script type="text/javascript" charset="utf-8">
						$(function(){
							$(".bangla").avro({"bn":true}, function(isBangla) {
								if(isBangla) {
									$(".mode").text("English");
								} else {
									$(".mode").text("Bangla");
								}
							});
						});
					</script>';
    }
    ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <?php
    $xml = simplexml_load_file("xml/invitees_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $organization_member_name = $information->organization_member_name;
            $organization_name = $information->organization_name;
            $comments = $information->comment;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">
<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div>
        <input type="hidden" name="invitees_id"
               value="<?php echo isset($result->invitees_id) ? $result->invitees_id : ''; ?>"/>

        <div>
            <div class="columnA"><label><?php echo $organization_member_name; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[80]] text-input bangla'
                                        name='invitees_name'
                                        value="<?php echo isset($result->invitees_name) ? $result->invitees_name : ''; ?>"
                                        style="font-size: 11px;width: 370px; height:18px;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $organization_name; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[80]] text-input bangla'
                                        name='invitees_organization' style="font-size: 11px;width: 370px; height:18px;"
                                        value="<?php echo isset($result->invitees_organization) ? $result->invitees_organization : ''; ?>">
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $designation; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[80]] text-input bangla'
                                        name='designation' style="font-size: 11px;width: 370px; height:18px;"
                                        value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='validate[custom[phone],maxSize[20]] text-input bangla' name='phone'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>"
                           style="font-size: 11px; height:18px;">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[required,custom[cellphone],maxSize[20]] text-input bangla'
                           name='cell_phone'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"
                           style="font-size: 11px; height:18px;"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' class='validate[custom[fax],maxSize[20]] text-input bangla' name='fax'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>"
                           style="font-size: 11px; height:18px;">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[required,custom[email],maxSize[150]] text-input bangla'
                           name='email' value="<?php echo isset($result->email) ? $result->email : ''; ?>"
                           style="font-size: 11px; height:18px;"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?> *</label></div>
            <div class="columnB">
                <textarea type='text' class='validate[required,maxSize[250]] text-input bangla' name='address'
                          style="font-size: 12px;width: 370px; height:50px;  resize:none; overflow-y:scroll;">
                    <?php echo isset($result->address) ? $result->address : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB">
                <textarea type='text' class='bangla' name='comments'
                          style="font-size: 12px;width: 370px; height:50px;  resize:none; overflow-y:scroll;">
                    <?php echo isset($result->comments) ? $result->comments : ''; ?>
                </textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                &nbsp;&nbsp;
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
