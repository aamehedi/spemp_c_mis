<?php
session_start();
// die('It is working');

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('tcpdf/tcpdf.php');
require_once('../model/request_evidence_info.php');
require_once('MYPDF.php');

$user_id = $_SESSION['user_id'];
if ($_GET['request_evidence_id'] != NULL) {
    $result = $request_evidence_info->editrow(array($_GET['request_evidence_id'], $user_id));
}
// var_dump($result);
// die();
// var_dump($_SESSION);
$confData = array(
    'pageTitle' => 'Request Evidence',
    'committeeName' => $_SESSION['committee_name'],
    'parliamentName' => $result->parliament,
    'reportName' => 'Request Evidence',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => true
);


// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);


$pdf->AddPage();

if ($result->is_issued === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));


// if ($_SESSION['language_id'] === '1') {
// 	$pdf->SetFont('times', '', 12);
// }else{

// 	////////////////////
// }
$pdf->Cell(0, 5, '', 0, 1, 'C', false, '', 0, false, 'C', 'B');
$html = '<div style="margin:5px;">';
$html .= htmlspecialchars_decode($result->cover_letter) . '<br>';
$html .= '</div>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
// var_dump($result);
if ($result->is_issued === '0') {
    $pdf->Output('pdf_rpt_request_evidence_' . $result->request_evidence_id . '.pdf', 'I');
} else {
    $pdf->Output('issued_approved_pdf/pdf_rpt_request_evidence_' . $result->request_evidence_id . '.pdf', 'F');
    ?>
    <script language="javascript">
        opener.location.href = 'request_evidence.php';
        window.close();
    </script>
<?php
}
//============================================================+
// END OF FILE
//============================================================+
