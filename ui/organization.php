<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<?php
require_once('../model/organization_info.php');
?>
<?php
$xml = simplexml_load_file("xml/organization_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $id = $information->id;
        $organization_name = $information->organization_name;
        $short_name = $information->short_name;
        $section = $information->section;
        $branch = $information->branch;
        $contact_person = $information->contact_person;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
        $comments = $information->comments;
    }
}

?>
<script type='text/javascript'>
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "organization_delete.php?organization_id=" + p;
        }
        else {

        }
    }
    function popupwinid(p) {
        window.showModalDialog("organization_popup.php?organization_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:480px")
    }
</script>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($organization_name, $designation);
    echo $organization_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>