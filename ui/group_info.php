<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/user.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("group_popup.php?group_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:440px;dialogHeight:170px")
    }

    function groupmenuid(p) {
        window.showModalDialog("group_menu_permission_popup.php?group_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1000px;dialogHeight:700px")
    }

    function popupchangeid(p) {
        $('<div></div>').load('change_user_password_popup.php?group_id=' + p).modal();
        return false;
    }

    function activeid(p) {
        var r = confirm("Do You Want To Active and/or Inactive This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "user_activation.php?group_id=" + p;
        }
    }
</script>
<div id="head_info">
    <?php require_once('../model/group.php'); ?>
    <?php if ($_SESSION['language_id'] === '1') : ?>
        User Type Information
    <?php else : ?>
        ব্যাবহারকারীর প্রকার তথ্য
    <?php endif; ?>
</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    echo $group->gridview();
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>