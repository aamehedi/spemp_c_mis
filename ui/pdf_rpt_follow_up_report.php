<?php
session_start();

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('MYPDF.php');
require_once('../model/follow_up_info.php');

$user_id = $_SESSION['user_id'];
$introductions = '';
$issues = '';
if ($_GET['inquiry_report_followup_id'] != NULL) {
    $result1 = $follow_up_info->rpt_follow_up_getall(array($_GET['inquiry_report_followup_id']));
    $result = mysqli_fetch_object($result1);

}


// var_dump($result);
// die();

$confData = array(
    'pageTitle' => 'Follow Up Report',
    'committeeName' => '',
    'parliamentName' => '',
    'reportName' => '',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => false
);

// var_dump($confData);
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);
$pdf->SetHeaderMargin(20);

$pdf->AddPage();

// Logo
$image_file = 'img/logo.png';
$pdf->Image($image_file, 10, 10, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

if ($result->is_approved === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// var_dump($pdf->fontName);

$pdf->setCellHeightRatio(1.2);

$pdf->SetFont('cambriab', '', 36);
$pdf->MultiCell(0, 10, '');
$pdf->MultiCell(0, 0, htmlspecialchars_decode($result->committee_name) . ' of the ' . htmlspecialchars_decode($result->parliament) . ' of Bangladesh - ' . htmlspecialchars_decode($result->title), false, 'C', false, 1);

$pdf->SetFont('cambria', '', 22);
$pdf->MultiCell(0, 4, '');
// $pdf->Cell(0, 40, 'Inquiry Report No.'.$result->report_no, 'TBRL', 1, 'C', false, '', 0, false, 'C', 'B');
$pdf->MultiCell(0, 0, 'Inquiry Report No.' . htmlspecialchars_decode($result->report_no) . '/F - Follow Up', false, 'C', false, 1, '', '', true, '', false, '', '', 'B');
if (!empty($result->followup_date)) {
    $pdf->MultiCell(0, 4, '');
    $pdf->MultiCell(0, 0, htmlspecialchars_decode($result->followup_date), false, 'C', false, 1);
}

// add a page
$pdf->AddPage();


$pdf->SetFont('cambriab', '', 14);
$pdf->Cell(0, 10, 'Background', 0, 1, 'L');
$pdf->SetFont('georgia', '', 12);
$html = htmlspecialchars_decode($result->background);
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->MultiCell(0, 0, , false, 'L', false, 1,'','',true,'',false,'','','B');


$pdf->SetFont('cambriab', '', 14);
$pdf->Cell(0, 10, 'Recommendations', 0, 1, 'L');
$pdf->SetFont('georgia', '', 12);
$pdf->Ln();
// $pdf->MultiCell(0, 0, htmlspecialchars_decode($result->recommendation), false, 'L', false, 1,'','',true,'',false,'','','B');

$html = '<table width="100%" border="0" >';
$html .= '<tr style="background-color:#B7DDE7;">';
$html .= '<th style="width:25%;">Committee Recommendation</th>';
$html .= '<th style="width:25%;">Response of Organisation</th>';
$html .= '<th style="width:25%;">Action Taken</th>';
$html .= '<th style="width:25%;">Comment</th>';
$html .= '</tr>';
$html .= '<tr><td style="width:25%;">' . htmlspecialchars_decode($result->recommendation) . '</td><td style="width:25%;">' . htmlspecialchars_decode($result->organization_response) . '</td><td style="width:25%;">' . htmlspecialchars_decode($result->action_taken) . '</td><td style="width:25%;">' . htmlspecialchars_decode($result->comment) . '</td></tr>';
while ($tableRow = $result1->fetch_object()) {
    $html .= '<tr><td>' . htmlspecialchars_decode($tableRow->recommendation) . '</td><td>' . htmlspecialchars_decode($tableRow->organization_response) . '</td><td>' . htmlspecialchars_decode($tableRow->action_taken) . '</td><td>' . htmlspecialchars_decode($tableRow->comment) . '</td></tr>';
}
$html .= '</table>';


$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
// $pdf->Output('pdf_rpt_briefing_note_'.$_GET['inquiry_report_id'].'.pdf', 'I');
$pdf->Output('pdf_rpt_follow_up_report_' . $_GET['inquiry_report_followup_id'] . '.pdf', 'I');
// if ($result->is_approved === '0') {
// 	$pdf->Output('pdf_rpt_follow_up_report_'.$_GET['inquiry_report_followup_id'].'.pdf', 'I');
// }else{	
// 	$pdf->Output('issued_approved_pdf/pdf_rpt_follow_up_report_'.$_GET['inquiry_report_followup_id'].'.pdf', 'F');	
// 	echo "<script>window.close();</script>";
// }
//============================================================+
// END OF FILE
//============================================================+
