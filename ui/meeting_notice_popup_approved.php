<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/meeting_notice_info.php');
$user_id = $_SESSION['user_id'];
//echo $_GET['metting_notice_id'];
if ($_GET['metting_notice_id'] != NULL) {
    $result = $meeting_notice_info->editrow(array($_GET['metting_notice_id'], $user_id));
}
//echo $result->metting_notice_id;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">
    <script src="codebase_m/dhtmlxcalendar.js"></script>
    <style>
        #calendar,
        #calendar2,
        #calendar3,
        #calendar4 {
            border: 1px solid #909090;
            font-family: Tahoma;
            font-size: 12px;
        }
    </style>
    <script>
        var myCalendar;
        function doOnLoad() {
            myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);
        }
    </script>
    <script language="javascript" type="text/javascript" src="datepopup/datetimepicker.js">

        //Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
        //Script featured on JavaScript Kit (http://www.javascriptkit.com)
        //For this script, visit http://www.javascriptkit.com
    </script>
    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <!-- datebangla-->
    <script>
        function myFunction(str) {

            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "bndate.php?date=" + str, true);
            xmlhttp.send();
        }
    </script>

    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;

        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
</head>
<body onLoad="doOnLoad();">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<?php
$xml = simplexml_load_file("xml/meeting_notice_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
    }
}
?>
<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>

    <div>
        <input type="hidden" name="metting_notice_id"
               value="<?php echo isset($result->metting_notice_id) ? $result->metting_notice_id : ''; ?>"/>

        <div style="width:65%; float:left;">
            <div>
                <div style="width:23%; float:left;"><?php echo $ref_no; ?> *</div>
                <div style="width:37%; float:left;">
                    <table style="width:100%;" cellspacing="0" cellpadding="0" border='0'>
                        <tr>
                            <td style="width:70%;">
                                <input id="name" type="text" name="p_gov_no_pix" style="width:95%;"
                                       value="11.00.0000.731.01.021.13" disabled/>
                            </td>
                            <td style="width:30%;">
                                <input id="name" type="text" name="p_gov_no_postfix" style="width:95%;"
                                       value="<?php echo isset($result->gov_no_postfix) ? $result->gov_no_postfix : ''; ?>"
                                       disabled/>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="width:12%; float:left;"><?php echo $sitting_no; ?></div>
                <div style="width:18%; float:left;"><input id="name" disabled type="text" class='bangla'
                                                           name="sitting_no" style="width:70%;"
                                                           value="<?php echo isset($result->sitting_no) ? $result->sitting_no : '[Auto]'; ?>"/>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:23%; float:left;"><?php echo $date; ?> *</div>
                <div style="width:25%; float:left;">
                    <input style='width:70%' id="calendar2" type="text" class='validate[required] text-input bangla'
                           name="en_date" onBlur="myFunction(this.value)"
                           value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>" disabled/>
                    <!-- <a href="javascript:NewCal('demo1','ddmmyyyy')"><img src="datepopup/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>-->
                </div>
                <div style="width:12%; float:left;">
                    <div id="txtHint"><input id="name" type="text"
                                             class='validate[required,maxSize[80]] text-input bangla' name="bn_date"
                                             value="<?php echo isset($result->bn_date) ? $result->bn_date : ''; ?>"
                                             style="width:94%;" disabled/></div>
                </div>
                <div style="width:12%; float:left;"><?php echo $time; ?> *</div>
                <div style="width:22%; float:left;"><input id="name" type="text"
                                                           class='validate[required] text-input bangla' name="time"
                                                           style="width:70%;"
                                                           value="<?php echo isset($result->time) ? $result->time : ''; ?>"
                                                           disabled/></div>
                <div class="divclear"></div>
            </div>
        </div>
        <div style="width:35%; float:left; margin-top:-10px;">
            <div>
                <div style="width:25%; float:left;"><?php echo $venue; ?> *</div>
                <div style="width:75%; float:left;">
                    <?php
                    //echo $result->venue_id;

                    $id = isset($result->venue_id) ? $result->venue_id : '';
                    //echo $result->section_id;
                    $language_id = $_SESSION['language_id'];
                    $user_id = $_SESSION['user_id'];
                    echo '<select  name="venue_id" disabled>';
                    echo $meeting_notice_info->comboview_venue($language_id, $user_id, $id);
                    echo '</select>';
                    ?>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:25%; float:left;"><?php echo $committee; ?></div>
                <div style="width:75%; float:left;">
                    <?php
                    $id = isset($result->sub_committee_id) ? $result->sub_committee_id : '';
                    //echo $result->sub_committee_id;
                    $language_id = $_SESSION['language_id'];
                    $committee_id = $_SESSION['committee_id'];
                    $user_id = $_SESSION['user_id'];
                    echo '<select  name="sub_committee_id" disabled>';
                    echo $meeting_notice_info->comboview_committee($language_id, $committee_id, $user_id, $id);
                    echo '</select>';
                    ?>
                </div>
                <div class="divclear"></div>
            </div>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="margin-top:5px;">
        <div style="float:left;width:15%;">
            <?php echo $inquires; ?> *
        </div>
        <div style="float:left;width:80%;">
            <?php
            $metting_notice_id = $_GET['metting_notice_id'];
            $user_id = $_SESSION['user_id'];
            $result1 = $meeting_notice_info->gridview_token_pree_popolated_inquiry_display($metting_notice_id);
            echo $result1;
            ?>
        </div>
        <div class="divclear"></div>
    </div>

    <div>
        <div><b><?php echo $items; ?></b></div>
        <div class="divclear"></div>
    </div>

    <div style="margin-top:5px;">
        <div style="float:left;width:15%;">
            <?php echo $private_business; ?>
        </div>
        <div style="float:left;width:81%; border:solid 1px #000000; background-color:#FFFFFF; height:50px;">
            &nbsp;
            <?php
            echo html_entity_decode($result->private_business_before);
            ?>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="margin-top:10px;">
        <div style="float:left;width:15%;">
            <?php echo $public_business; ?>
        </div>
        <div style="float:left;width:81%;  border:solid 1px #000000; background-color:#FFFFFF;height:50px;">
            &nbsp;
            <?php
            echo html_entity_decode($result->public_business);
            ?>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="margin-top:5px;">
        <div style="float:left;width:15%;">
            <?php echo $private_business; ?>
        </div>
        <div style="float:left;width:81%;  border:solid 1px #000000; background-color:#FFFFFF; height:50px;">
            &nbsp;
            <?php
            echo html_entity_decode($result->private_business_after);
            ?>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="margin-top:5px;">
        <div style="float:left;width:15%;">
            <?php echo $commiitee_members; ?>
        </div>
        <div style="float:left;width:81%;">
            <?php
            $metting_notice_id = $_GET['metting_notice_id'];
            $user_id = $_SESSION['user_id'];
            echo $meeting_notice_info->gridview_token_pree_popolated_committee_display($metting_notice_id, 0, 0, 0);
            ?>
        </div>
        <div class="divclear"></div>
    </div>

    <div></div>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

    <div>
        <div style="float:left;width:25%;">
        </div>
        <div style="float:left;width:75%;">
            <script>
                function show_alert() {
                    var r = confirm("Do You Want To Approved  This Record Or Data?");
                    //alert('Delete'+p);
                    if (r == false) {
                        return false;
                    }
                }
            </script>
            &nbsp;&nbsp;<input type="submit" name="btn_close" value="Close" class="popupbutton"/>
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="send" name="btn_approved" type="submit" value="Approved" class="popupbutton"
                   onClick="return show_alert();"/>
        </div>
    </div>

</form>
</body>
</html>
