<?php
session_start();
?>
<?php
require_once('../model/group.php');
require_once('../model/user.php');
require_once '../dal/data_access.php';
$data_access = new data_access();
if ($_GET['user_id'] != NULL) {
    $result = $user->editrow(array($_GET['user_id']));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        function checkParliamentMember() {
            var selectGroup = document.getElementById("group_id");
            if (selectGroup.options[selectGroup.selectedIndex].text == 'Parliament Member') {
                document.getElementById("divParliamentMember").style.display = 'block';
            } else {
                document.getElementById("divParliamentMember").style.display = 'none';
            };
        }


    </script>
    <!--Validation End-->
</head>
<body>

<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2>
        <?php if ($_SESSION['language_id'] === '1') : ?>
            Create New User
        <?php else : ?>
            নতুন ব্যাবারকারী তৈরী করুন
        <?php endif; ?>
    </h2>
    </br>
    <div>
        <div style="display:none;">
            <div class="columnA"><label><?php echo 'User ID'; ?> </label></div>
            <div class="columnB"><input id="name" type="text" name="user_id" value="[Auto]" readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div id="divParliamentMember" style="display: none;">
            <div class="columnA"><label><?php echo 'Parliament Member' ?> *</label></div>
            <div class="columnB">
                <select name="parliamentMember" class='validate[required,maxSize[25]] text-input bangla'>
                    <option value="''">Select</option>
                    <?php
                    $resultParliamentMember = $data_access->data_reader('tbl_parliament_member_get_user', array());
                    while ($rowParliamentMember = mysqli_fetch_assoc($resultParliamentMember)) {
                        ?>
                        <option
                            value="<?php echo $rowParliamentMember['member_id']; ?>"><?php echo $rowParliamentMember['member_name']; ?></option>
                    <?php
                    }
                    ?>
                </select>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label>
                    <?php if ($_SESSION['language_id'] === '1') : ?>
                        User Name *
                    <?php else : ?>
                        ব্যাবারকারীর নাম *
                    <?php endif; ?>
                </label></div>
            <div class="columnB">
                <input id="name" type='text' class='validate[required,maxSize[25]] text-input bangla' name="user_name"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label>
                    <?php if ($_SESSION['language_id'] === '1') : ?>
                        Password *
                    <?php else : ?>
                        পাসওয়ার্ড *
                    <?php endif; ?>
                </label></div>
            <div class="columnB">
                <input id="name" type="password" class='validate[required,minSize[5],maxSize[80]] text-input bangla'
                       name="password"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label>
                    <?php if ($_SESSION['language_id'] === '1') : ?>
                        Group *
                    <?php else : ?>
                        গ্রুপ *
                    <?php endif; ?>
                </label></div>
            <div class="columnB">
                <?php
                echo '<select name="group_id" id="group_id" onchange="checkParliamentMember()">';
                echo $group->getGroup($result->group_id);
                echo '</select>';
                ?>
                </div>
            <div class="divclear"></div>
        </div>
        <br/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save_user" type="submit" value="Save" class="popupbutton"/>
            </div>
            <div class="columnBbutton">
                <script>
                    function myFunctionC() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;
                <button onClick="myFunctionC()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
