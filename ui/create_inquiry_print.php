<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/create_inquiry_info.php');

if ($_GET['inquiry_id'] != NULL) {
    $result = $create_inquiry_info->editreport(array($_GET['inquiry_id']));
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<!--
<html xmlns="http://www.w3.org/1999/xhtml">
-->

<head>
    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Manage Inquiry Report</title>
    <?php else: ?>
        <title>তদন্ত নোটিশ</title>
    <?php endif; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
</head>

<body>
<?php if ($result->is_issued === '1') : ?>
<div style="margin:10px; width:80%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:80%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat; background-size: 200px 180px;"
        id="message_area">
        <?php endif; ?>

        <div>
            <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            <div style="float:left; height:80px; padding-top:25px; padding-left:30px;font-weight:bold;">
                <div style="font-size:18px;"><?php echo $result->committee_name; ?></div>
                <?php if (isset($language_id) && $language_id === '1'): ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">Parliament of Bangladesh</div>
                <?php else: ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">বাংলাদেশ জাতীয় সংসদ</div>
                <?php endif; ?>
            </div>
            <div style="clear:both;"></div>
        </div>

        <br/>
        <head>
            <?php if (isset($language_id) && $language_id === '1'): ?>
        <div style="font-size:24px;text-align:center;font-weight:bold;">NOTICE OF INQUIRY</div>
    <?php else: ?>
        <div style="font-size:24px;text-align:center;font-weight:bold;">তদন্ত নোটিশ</div>
    <?php
    endif; ?>

        <div style="margin:5px;">
            <?php

            if (isset($language_id) && $language_id === '1') {
                echo '<b>Inquiry No.:</b>&nbsp;' . $result->inquiry_no . '<br>';
                echo '<b>Inquiry Title:</b>&nbsp;' . htmlspecialchars_decode($result->inquiry_title) . '<br>';
                echo '<b>Create Date:</b>&nbsp;' . htmlspecialchars_decode($result->create_date) . '<br>';
                echo '<b>Issued Date:</b>&nbsp;' . htmlspecialchars_decode($result->issued_date) . '<br>';
                echo '<b>Proposed Date:</b>&nbsp;' . htmlspecialchars_decode($result->proposed_date) . '<br>';
                echo '<b>Document(s):</b>&nbsp;' . htmlspecialchars_decode($result->doc_title) . '<br>';
                echo '<b>Ministry(s):</b>&nbsp;' . htmlspecialchars_decode($result->ministry_name) . '<br>';
                echo '</div>';
            } else {
                echo '<b>তদন্ত নং.:</b>&nbsp;' . $result->inquiry_no . '<br>';
                echo '<b>তদন্ত শিরোনাম:</b>&nbsp;' . htmlspecialchars_decode($result->inquiry_title) . '<br>';
                echo '<b>তদন্ত তারিখ:</b>&nbsp;' . htmlspecialchars_decode($result->create_date) . '<br>';
                echo '<b>ইস্যু তারিখ:</b>&nbsp;' . htmlspecialchars_decode($result->issued_date) . '<br>';
                echo '<b>প্রস্তাবিত তারিখ:</b>&nbsp;' . htmlspecialchars_decode($result->proposed_date) . '<br>';
                echo '<b>নথিপত্র:</b>&nbsp;' . htmlspecialchars_decode($result->doc_title) . '<br>';
                echo '<b>মন্ত্রণালয়:</b>&nbsp;' . htmlspecialchars_decode($result->ministry_name) . '<br>';
                echo '</div>';
            }



            ?>
        </div>
    </div>
    <?php if ($result->is_issued !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
