<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>
<script type="text/javascript" src="js/jquery.js"></script>

<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("logistic_member_popup.php?logistic_member_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:480px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Deletep'+p+'Deleteq'+q);
        if (r == true) {
            window.location.href = "logistic_member__delete.php?logistic_member_id=" + p;
        }
        else {

        }
    }
</script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>
<?php
require_once('../model/logistic_member_info.php');
?>
<?php
$xml = simplexml_load_file("xml/logistic_member_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $logistic_member_name = $information->logistic_member_name;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
        $comments = $information->comment;

    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right;">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<!--		  <br>-->
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php if (isset($_SESSION['logistic_member_name']) || isset($_SESSION['designation'])) : ?>
    <div style="margin-top:1px; width:100%;" id="slideOne">
        <?php else : ?>
        <div style="margin-top:1px; width:100%;" class="slider" id="slideOne">
            <?php endif; ?>
            <fieldset>
                <legend>Search Criteria</legend>
                <div style='margin:3px;'>
                    <div style='float:left;width:17.5%;'><?php echo $logistic_member_name; ?></div>
                    <div style='float:left;width:30%'><input type="text" style='width:90%' name="logistic_member_name"/>
                    </div>
                    <div style='float:left;width:17.5%;'><?php echo $designation; ?></div>
                    <div style='float:left;width:30%'><input type="text" style='width:90%' name="designation"/></div>
                    <div style='clear:both'></div>
                </div>
                <div>
                    <div style="float:left;text-align:right;width:100%;">
                        <input type="submit" name="basic" value="Search" class="newbutton"/>
                    </div>
                </div>
            </fieldset>
        </div>
</form>
<input type="button" onClick="popupwinid(0);" name="basic" value="Add Member" class="newbutton"/>
<?php
if (isset($_GET['logistic_member_name'])) {
    $logistic_member = $_GET['logistic_member_name'];
}
if (isset($_GET['designation'])) {
    $designation_m = $_GET['designation'];
}
//echo $ministry_id.'--'.$ministry_member_name.'--'.$designation;
?>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    if (isset($_GET['notification_member_name']) || isset($_GET['designation'])) {
        $param = array($logistic_member_name, $designation);
        echo $logistic_member_info->searchgridview($logistic_member, $designation_m, $language_id, $user_id, $param);
    } else {
        $param = array('', '');
        echo $logistic_member_info->searchgridview('', '', $language_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>