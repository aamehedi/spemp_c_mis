<?php
session_start();
//echo $_SESSION['user_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/commiittee_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['committee_id'] != NULL) {
    $result = $commiittee_info->editrow(array($_GET['committee_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <!--	<link type='text/css' href='css/session.css' rel='stylesheet' media='screen' />-->
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>

    <!--	<script type="text/javascript">-->
    <!--		var autoLoad = setInterval(-->
    <!--		function ()-->
    <!--		{-->
    <!--			$('#diplay_section').load('load_section.php').fadeIn("slow");-->
    <!-- 		 }, 10000); // refresh page every 1 seconds-->
    <!--	</script>-->
    <!--	<script type="text/javascript">-->
    <!--	    //$('#diplay_branch').load('load_branch.php');-->
    <!--		var autoLoad = setInterval(-->
    <!--		function ()-->
    <!--		{-->
    <!--			$('#diplay_branch').load('load_branch.php').fadeIn("slow");-->
    <!-- 		 }, 10000); // refresh page every 1 seconds-->
    <!--		//setTimeout(function() { clearInterval(autoLoad); }, 5000);-->
    <!--		-->
    <!--	</script>-->
    <?php
    $xml = simplexml_load_file("xml/committee_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {

            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $committee_name = $information->committee_name;
            $short_name = $information->short_name;
            $section = $information->section;
            $branch = $information->branch;
            $contact_person = $information->contact_person;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $comments = $information->comments;
        }
    }

    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body>
<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?> </label></div>
            <div class="columnB"><input id="name" type="text" name="committee_id"
                                        value="<?php echo isset($result->committee_id) ? $result->committee_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $committee_name; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[150]] text-input bangla'
                                        name='committee_name' style="font-size: 11px;width: 340%;"
                                        value="<?php echo isset($result->committee_name) ? $result->committee_name : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $short_name; ?> *</label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='short_name' class='validate[required,maxSize[5]] text-input bangla'
                           style="font-size: 11px;"
                           value="<?php echo isset($result->short_name) ? $result->short_name : ''; ?>"/>
                </div>
                <div style="float:left;text-align:center;margin-left: 40px;">
                    <?php echo $section; ?> *
                </div>
                <div style="float:left;">
                    <input type="text" class="validate[required]" name="section" value="<?php echo (!empty($result->section)) ? $result->section : ''; ?>"/>
                    <!--<div id="diplay_section" style="float:left;">-->
<!--                    --><?php
//                    $id = isset($result->section_id) ? $result->section_id : '';
//                    //echo $result->section_id;
//                    $language_id = $_SESSION['language_id'];
//                    $user_id = $_SESSION['user_id'];
//                    echo $commiittee_info->comboview_section($language_id, $user_id, $id);
//                    ?>
                    <!--</div>-->
<!--                    <div style="float:right;margin-left:2px;">-->
<!--                        <input type="button" name="file"-->
<!--                               onclick="javascript:void window.open('committe_section_popup.php','1376201640569','width=400,height=150,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=700,top=300');return false;"-->
<!--                               Value=" + "/>-->
<!--                    </div>-->
                    <!--<input name="section" type="hidden" value="1">-->
                </div>
                <div style="float:left;text-align:center;margin-left: 40px;">
                    <?php echo $branch; ?> *
                </div>
                <div style="float:left;">
                    <!--<div id="diplay_branch" style="float:left;">-->
                    <?php
                    $id = isset($result->branch_id) ? $result->branch_id : '';
                    $language_id = $_SESSION['language_id'];
                    $user_id = $_SESSION['user_id'];
                    echo $commiittee_info->comboview_branch($language_id, $user_id, $id);
                    ?>
                    <!--</div>-->
                    <div style="float:right;margin-left:2px;">
                        <input type="button" name="file"
                               onClick="javascript:void window.open('committe_branch_popup.php','1376201640569','width=400,height=150,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=850,top=300');return false;"
                               Value=" + "/>
                    </div>
                    <!--<input name="branch" type="hidden" value="1">-->
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $contact_person; ?> *</label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='contact_person' class='validate[required,maxSize[80]] text-input bangla'
                           value="<?php echo isset($result->contact_person) ? $result->contact_person : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $designation; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' name='designation' class='validate[required,maxSize[100]] text-input bangla'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?>
                </label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='phone' class='text-input bangla'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?>
                </div>
                <div style="float:left;">
                    <!-- <input type='text' name='cell_phone' class='validate[custom[cellphone],maxSize[20]] text-input bangla' value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"> -->
                    <input type='text' name='cell_phone' class='text-input bangla'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='fax' class='validate[maxSize[20]] text-input bangla'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' name='email' class='validate[custom[email],maxSize[150]] text-input bangla'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?></label></div>
            <div class="columnB"><textarea type='text' name='address' class='validate[maxSize[250]] text-input bangla'
                                           style="font-size: 11px;width: 450px;"><?php echo isset($result->address) ? $result->address : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB"><textarea type='text' name='comments' class='validate[maxSize[250]] text-input bangla'
                                           style="font-size: 11px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
