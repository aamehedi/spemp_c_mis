<?php

session_start();

if (isset($_GET['language_id'])) {
    $_SESSION['language_id'] = $_GET['language_id'];
    $_SESSION['committee_name'] = '&nbsp;';
    $_SESSION['menu'] = null;
}
if (isset($_SESSION['committee_id'])) {
    unset($_SESSION['committee_id']);
}
if (isset($_SESSION['committee_name'])) {
    unset($_SESSION['committee_name']);
}
?>
<script>
    function setcommittee(p) {
        window.open("committee_set.php?committee_id=" + p, "_self");
    }

</script>
<style type="text/css">
    .newbutton_l {
        float: left;
        background-color: #61399D;
        margin-bottom: 4px;
        margin-top: 4px;
        margin-right: 20px;
        color: #FFFFFF;
        width: 310px;
        height: 40px;
        border-radius: 8px;
        font: 14px Arial;
        font-weight: bold;
    }
</style>

<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>

<head>
    <title>SPO MIS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <?php if ($_SESSION["language_id"] == 1) : ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" href="css/grid.css" rel="stylesheet" media="screen"/>
    <?php else : ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" href="css/bngrid.css" rel="stylesheet" media="screen"/>
    <?php endif; ?>
    <link type='text/css' href='css/menu.css' rel='stylesheet' media='screen'/>
</head>
<body>
<div id="wrapper">
    <div class="header" onload="doonload()">
        <div
            class="header_text"><?php if (isset($_SESSION['committee_name'])) echo $_SESSION['committee_name']; else echo '&nbsp;'; ?></div>
        <div style="margin-top:-17px;margin-left:97%;"><a href="index.php"><img src="img/logo-fb_2f.gif" height="33"
                                                                                width="34"/></a></div>
        <div style="margin:-22px;">
            <div class="header_font" id="boldStuff">
                <?php
                if ($_SESSION['language_id'] == 1) {
                    echo '<a href="committee_set.php?language_id=1"><img src="img/e_white.png"></a><a href="committee_set.php?language_id=2"><img src="img/b_grey.png"></a>';
                } else
                    echo '<a href="committee_set.php?language_id=1"><img src="img/e_grey.png"></a><a href="committee_set.php?language_id=2"><img src="img/b_white.png"></a>';
                ?>
            </div>
            <div class="header_contain">&nbsp;&nbsp;<img src="img/m5.png" height="15"
                                                         width="12">&nbsp;&nbsp;<?php echo strtoupper($_SESSION["user_name"]); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Logout</a></div>
        </div>
    </div>

<?php

//require_once('header.php');
require_once('aside_left.php');

echo '<div id="content_main">';

require_once('../model/user_committee_permission.php');


?>

<?php
if (isset($_GET['committee_id'])) {
    require_once('../model/commiittee_info.php');
    $result = $commiittee_info->editrow(array($_GET['committee_id'], $_SESSION['user_id']));

    $_SESSION['committee_id'] = $result->committee_id;
    $_SESSION['committee_name'] = $result->committee_name;

    //echo $result->committee_name;
    header('Location: index.php?language_id=' . $_SESSION['language_id']);
    exit;
}

?>

<?php
if (isset($_GET['committee_id']) == FALSE) {
    $count_committee = $user_committee_permission->get_user_committee_count($_SESSION['user_id'], $_SESSION['language_id']);
    // var_dump($count_committee);
    if ($count_committee == 1) {
        $result = $user_committee_permission->get_user_committee($_SESSION['user_id'], $_SESSION['language_id']);
        $result = mysqli_fetch_object($result);

        $_SESSION['committee_id'] = $result->committee_id;
        $_SESSION['committee_name'] = $result->committee_name;
        header('Location: index.php?language_id=' . $_SESSION['language_id']);
        exit;
    }
}
//die("picasa");
?>
<div style="text-align:center;margin:0px auto;width:90%;">
    <?php
    $result = $user_committee_permission->get_user_committee($_SESSION['user_id'], $_SESSION['language_id']);
    while ($result1 = mysqli_fetch_object($result)) {
        echo "<button class='newbutton_l' onclick='setcommittee(" . $result1->committee_id . ");' >" . $result1->committee_name . "</button>";
        //echo "<br/>";
        //href='committee_set.php?committee_id=".$result1->committee_id."' target='_self'

    }
    ?>
</div>
</div>

<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>
