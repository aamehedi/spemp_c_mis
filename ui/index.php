<?php require_once('header.php'); ?>
<?php

require_once('aside_left.php');
?>

<?php require_once('menu.php'); ?>
<div>
<?php

if ($_SESSION['is_parliament_member'] !== '0') {
require_once '../dal/data_access.php';
$data_access = new data_access();

// var_dump($resultBarChart);

?>
<!--[if lt IE 9]>
<script language="javascript" type="text/javascript" src="js/excanvas.js"></script><![endif]-->
<script language="javascript" type="text/javascript" src="js/jquery-1.9.0.min.js"></script>
<script language="javascript" type="text/javascript" src="js/jquery.jqplot.min.js"></script>
<script type="text/javascript" src="js/plugins/jqplot.barRenderer.min.js"></script>
<script type="text/javascript" src="js/plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="js/plugins/jqplot.pointLabels.min.js"></script>
<script type="text/javascript" src="js/plugins/jqplot.pieRenderer.min.js"></script>
<script type="text/javascript" src="js/plugins/jqplot.donutRenderer.min.js"></script>
<link rel="stylesheet" type="text/css" href="styles/jquery.jqplot.css"/>
<!-- <script type="text/javascript" src="js/barChart.js"></script> -->
<style type="text/css">

    div.tableName {
        background-color: #61399D;
        color: #FFFFFF;
        text-align: center;
        font-weight: bold;
        font-size: 1.4em;
    }

    div.leftColumn {
        width: 40%;
        float: left;
        margin-right: .7em;
        /*min-height: 800px;*/
    }

    div.rightColumn {
        margin-left: 41%;
        margin-top: .5em;
    }

    a {
        color: blue;
    }

    .header_contain > a {
        color: #FFFFFF;
    }

    a.more {
        float: right;
    }

    .barChartHeader {
        background-color: #EF8802;
        clear: both;
        text-align: center;
        font-weight: bold;
    }

    #barChart {
        min-height: 400px;
    }

    tr:nth-child(2n+1) {
        color: #666666;
    }
</style>
<?php
$resultOngoingInquiries = $data_access->data_reader("tbl_inquiry_master_search_raw", array($_SESSION['language_id'], $_SESSION['committee_id'], 0));
$resultCompletedInquiries = $data_access->data_reader("tbl_inquiry_master_search_raw", array($_SESSION['language_id'], $_SESSION['committee_id'], 1));


$resultBarChartHeld = $data_access->data_reader("tbl_meeting_notice_master_bar_chart", array($_SESSION['user_id'], 'false'));
$resultBarChartUpcoming = $data_access->data_reader("tbl_meeting_notice_master_bar_chart", array($_SESSION['user_id'], 'true'));
$held = array();

$monthYear = array();

while ($rowHeld = mysqli_fetch_assoc($resultBarChartHeld)) {
    array_push($monthYear, $rowHeld['monthName'] . '</br>' . $rowHeld['yearName']);
    array_push($held, $rowHeld['meetingCount']);
}
$heldSize = count($held);
for (; $heldSize < 6; $heldSize++) {
    array_push($held, 0);
}
$upcoming = array(0, 0, 0, 0, 0, 0);
while ($rowUpcomming = mysqli_fetch_assoc($resultBarChartUpcoming)) {
    $lastPosition = array_search($rowUpcomming['monthName'] . '</br>' . $rowUpcomming['yearName'], $monthYear);
    if ($lastPosition) {
        $upcoming[$lastPosition] = $rowUpcomming['meetingCount'];
    } else {
        array_push($monthYear, $rowUpcomming['monthName'] . '</br>' . $rowUpcomming['yearName']);
        $lastPosition = array_search($rowUpcomming['monthName'] . '</br>' . $rowUpcomming['yearName'], $monthYear);
        $upcoming[$lastPosition] = $rowUpcomming['meetingCount'];
    }
}
?>
<script type="text/javascript">
    $(document).ready(function () {
        var held = [<?php echo implode(', ', $held); ?>];
        var upcoming = [<?php echo implode(', ', $upcoming); ?>];
        // var s3 = [14, 9, 3, 8];
        var ticks = [<?php echo "'".implode("', '", $monthYear)."'" ?>];
        plot3 = $.jqplot('barChart', [held, upcoming], {
            // Tell the plot to stack the bars.
            stackSeries: true,
            captureRightClick: true,
            seriesColors: ["#00D80E", "#FF0004"],
            seriesDefaults: {
                renderer: $.jqplot.BarRenderer,
                rendererOptions: {
                    // Put a 30 pixel margin between bars.
                    barMargin: 30,
                    // Highlight bars when mouse button pressed.
                    // Disables default highlighting on mouse over.
                    highlightMouseDown: true,
                    barWidth: 35
                },
                pointLabels: {show: true}
            },
            series: [
                {label: 'Held'},
                {label: 'Upcoming'}
            ],
            legend: {
                show: true,
                placement: 'outsideGrid',
                location: 's'
            },
            axes: {
                xaxis: {
                    renderer: $.jqplot.CategoryAxisRenderer,
                    ticks: ticks
                },
                yaxis: {
                    // Don't pad out the bottom of the data range.  By default,
                    // axes scaled as if data extended 10% above and below the
                    // actual range to prevent data points right on grid boundaries.
                    // Don't want to do that here.
                    padMin: 0
                }
            },
            // legend: {
            //   show: true,
            //   location: 'e',
            //   placement: 'outside'
            // }
        });
        // Bind a listener to the "jqplotDataClick" event.  Here, simply change
        // the text of the info3 element to show what series and ponit were
        // clicked along with the data for that point.

        // $('#chart3').bind('jqplotDataClick',
        //   function (ev, seriesIndex, pointIndex, data) {
        //     $('#info3').html('series: '+seriesIndex+', point: '+pointIndex+', data: '+data);
        //   }
        // );

        var data = [
            ['Ongoing', <?php echo $resultOngoingInquiries->num_rows; ?>],
            ['Completed', <?php echo $resultCompletedInquiries->num_rows; ?>]
        ];
        var plot1 = jQuery.jqplot('pieChart', [data],
            {
                seriesColors: ["#00D80E", "#FF0004"],
                seriesDefaults: {
                    // Make this a pie chart.
                    renderer: jQuery.jqplot.PieRenderer,
                    rendererOptions: {
                        // Put data labels on the pie slices.
                        // By default, labels show the percentage of the slice.
                        showDataLabels: true
                    }
                },
                legend: { show: true, location: 'e' }
            }
        );
    });
</script>
<div class="leftColumn">
    <div class="tableName">
        Upcoming Sittings
    </div>
    <table class="mGrid" cellspacing="0">
        <tbody>
        <tr>
            <th>Sitting No.</th>
            <th>Date</th>
            <th>Venue</th>
            <th>Inquiries</th>
        </tr>
        <?php
        // var_dump($_SESSION['user_id']);
        $resultUpcommingSittings = $data_access->data_reader("tbl_meeting_notice_master_search_dashboard", array("'" . date('Y-m-d') . "'", "''", "''", 0, "''", "'" . $_SESSION['committee_id'] . "'", "'" . $_SESSION['language_id'] . "'", "'" . $_SESSION['user_id'] . "'", "''", "''"));
        $countUpcommingSittings = 0;
        while ($rowUpcommingSittings = mysqli_fetch_assoc($resultUpcommingSittings)) {
            $countUpcommingSittings++;
            ?>
            <tr>
                <td>
                    <a href="sitting.php?sittingNo=<?php echo $rowUpcommingSittings['metting_notice_id']; ?>"><?php echo $rowUpcommingSittings['sitting_no']; ?></a>
                </td>
                <td><?php echo ($_SESSION['language_id'] === '1') ? $rowUpcommingSittings['en_date'] : $rowUpcommingSittings['bn_date']; ?></td>
                <td><?php echo $rowUpcommingSittings['venue_name']; ?></td>
                <td><?php echo $rowUpcommingSittings['inquiry_no']; ?></td>

            </tr>
            <?php
            if ($countUpcommingSittings >= 4) {
                break;
            }
        }
        ?>
        </tbody>
    </table>
    <?php
    if ($countUpcommingSittings >= 4) {
        ?>
        <a href="sitting.php" class="more">More</a>
    <?php
    }
    ?>
    <div class="barChartHeader">
        Sittings (held and upcoming)
    </div>
    <div id="barChart"></div>

</div>
<div class="rightColumn">
    <!-- Table for Ongoning Inquiries Start -->
    <div class="tableName">
        Ongoing Inquiries
    </div>
    <table class="mGrid" cellspacing="0">
        <tbody>
        <tr>
            <th>Inquiry No.</th>
            <th>Inquiry Details</th>
            <th>Total Sitting held</th>
            <th>Last Sitting held</th>
        </tr>
        <?php
        // var_dump($_SESSION['user_id']);

        $countOngoingInquiries = 0;
        while ($rowOngoingInquries = mysqli_fetch_assoc($resultOngoingInquiries)) {
            $countOngoingInquiries++;
            ?>
            <tr>
                <td>
                    <a href="<?php echo 'ongoingInquiries.php?inquiry_id=' . $rowOngoingInquries['inquiry_id']; ?>"><?php echo $rowOngoingInquries['inquiry_no']; ?></a>
                </td>
                <td><?php echo $rowOngoingInquries['inquiry_title']; ?></td>
                <td><?php echo $rowOngoingInquries['sittingHeld']; ?></td>
                <td><?php echo $rowOngoingInquries['lastSitting']; ?></td>

            </tr>
            <?php
            if ($countOngoingInquiries >= 3) {
                break;
            }
        }
        ?>
        </tbody>
    </table>
    <?php
    if ($countOngoingInquiries >= 3) {
        ?>
        <a href="ongoingInquiries.php" class="more">More</a>
    <?php
    }
    ?>
    <!-- Table for Ongoning Inquiries End -->
    <br/>
    <!-- Table for Completed Inquiries Start -->
    <div class="tableName">
        Completed Inquiries
    </div>
    <table class="mGrid" cellspacing="0">
        <tbody>
        <tr>
            <th>Inquiry No.</th>
            <th>Inquiry Details</th>
            <th>Total Sitting</th>
            <th>Status</th>
        </tr>
        <?php
        // var_dump($_SESSION['user_id']);

        $countCompletedInquiries = 0;
        while ($rowCompletedInquries = mysqli_fetch_assoc($resultCompletedInquiries)) {
            $countCompletedInquiries++;
            ?>
            <tr>
                <td><?php echo $rowCompletedInquries['inquiry_no']; ?></td>
                <td><?php echo $rowCompletedInquries['inquiry_title']; ?></td>
                <td><?php echo $rowCompletedInquries['sittingHeld']; ?></td>
                <td><?php echo $rowCompletedInquries['inquiryStatus']; ?></td>

            </tr>
            <?php
            if ($countCompletedInquiries >= 3) {
                break;
            }
        }
        ?>
        </tbody>
    </table>
    <?php
    if ($countCompletedInquiries >= 3) {
        ?>
        <a href="completedInquiries.php" class="more">More</a>
    <?php
    }
    ?>
    <!-- Table for Completed Inquiries Start -->
    <div id="pieChart"></div>
</div>
</div>


<?php
} else {
    ?>
    <style>
        #content_main div:nth-of-type(2) {
            text-align: center;
            clear: both;
        }
    </style>
    <?php
    if ($_SESSION['language_id'] == 1) {
        echo '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="700" height="500">
	  <param name="movie" value="img/mis_spemp-c_02-10-13_en_final.swf" />
	  <param name="quality" value="high" />
	  <embed src="img/mis_spemp-c_02-10-13_en_final.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="700" height="500"></embed>
	</object>';
    } else if ($_SESSION['language_id'] == 2) {
        echo '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=7,0,19,0" width="700" height="500">
	  <param name="movie" value="img/mis_spemp-c_02-10-13_bang_final.swf" />
	  <param name="quality" value="high" />
	  <embed src="img/mis_spemp-c_02-10-13_bang_final.swf" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer" type="application/x-shockwave-flash" width="700" height="500"></embed>
	</object>';
    }
}
?>
</div>
</div>
<?php
require_once('aside_right.php');
?>
<?php require_once('footer.php'); ?>