<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/briefing_note_info.php');
if ($_GET['meeting_notice_id'] != NULL) {
    $result = $briefing_note_info->rpt_brifeing_note(array($_GET['meeting_notice_id']));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>

    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Briefing Note</title>
    <?php else: ?>
        <title>ব্রিফিং নোট</title>
    <?php endif; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
</head>

<body>
<?php if ($result->is_issued === '1') : ?>
<div style="margin:10px; width:80%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:80%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat; background-width: 200px 180px;"
        id="message_area">
        <?php endif; ?>
        <div>
            <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            <div style="float:left; height:80px; padding-top:25px; padding-left:30px;font-weight: bold;">
                <div style="font-size:18px;"><?php echo $result->committee_name; ?></div>
                <?php if (isset($language_id) && $language_id === '1'): ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">Parliament of Bangladesh</div>
                <?php else: ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">বাংলাদেশ জাতীয় সংসদ</div>
                <?php endif; ?>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br/>
        <?php if (isset($language_id) && $language_id === '1'): ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">BRIEFING NOTE FOR HEARING
                ON <?php echo $result->bn_date; ?></div>
        <?php else: ?>
            <div style="font-size:18px; text-align:center;font-weight: bold;">শুনানির জন্য ব্রিফিং নোট</div>
        <?php endif; ?>

        <div style="margin-top:10px;">
            <?php
            if (isset($language_id) && $language_id === '1') {
                echo '<b>Introduction</b><br>' . htmlspecialchars_decode($result->introduction) . '<br>';
                echo '<b>Key Issues</b><br>' . htmlspecialchars_decode($result->key_issue) . '<br>';
                echo '<b>Possible Questions for Specialist Advisors</b><br>' . htmlspecialchars_decode($result->advisor_question) . '<br>';
                echo '<b>Possible Questions for Witnesses</b><br>' . htmlspecialchars_decode($result->witness_question) . '<br>';
                echo '<b>Written Evidence</b><br>' . htmlspecialchars_decode($result->written_evidence) . '<br>';
            } else {
                echo '<b>ভূমিকা</b><br>' . htmlspecialchars_decode($result->introduction) . '<br>';
                echo '<b>বিশেষ সমস্যা</b><br>' . htmlspecialchars_decode($result->key_issue) . '<br>';
                echo '<b>বিশেষজ্ঞ উপদেষ্টাদের জন্য সম্ভাব্য প্রশ্ন</b><br>' . htmlspecialchars_decode($result->advisor_question) . '<br>';
                echo '<b>সাক্ষীদের জন্য সম্ভাব্য প্রশ্ন</b><br>' . htmlspecialchars_decode($result->witness_question) . '<br>';
                echo '<b>লিখিত প্রমাণ</b><br>' . htmlspecialchars_decode($result->written_evidence) . '<br>';
            }
            ?>
        </div>
    </div>
    <?php if ($result->is_issued !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
