<?php session_start(); ?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
<?php else: ?>
    <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="styles/jquery-ui-1.10.4.custom.min.css"/>
<style>
    #overlay_form {
        position: absolute;
        /*border: 5px solid gray;*/
        padding: 0px;
    }

    .loader {
        display: block;
        border: 1px solid gray;
        width: 165px;
        text-align: center;
        padding: 6px;
        border-radius: 5px;
        text-decoration: none;
        /*margin: 0 auto;*/
    }

    .close_box#close {
        background: fuchsia;
        color: #000;
        padding: 2px 5px;
        display: inline;
        position: absolute;
        right: 15px;
        border-radius: 3px;
        cursor: pointer;
    }

    .columnA {
        float: left;
        width: 150px;
    }

    .columnB {
        float: left;
    }

    .columnAbutton {
        float: left;
        width: 100px;
    }

    .columnBbutton {
        float: left;
    }

    .divclear {
        clear: both;
    }

    textarea {
        height: 60px;
        width: 227px;
    }

    .myfileupload-buttonbar {
        float: left;
        width: 20%;
    }

    .myfileupload-buttonbar input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        border: solid transparent;
        border-width: 0 0 100px 200px;
        opacity: 0.0;
        filter: alpha(opacity=0);
        -o-transform: translate(250px, -50px) scale(1);
        -moz-transform: translate(-300px, 0) scale(4);
        direction: ltr;
        cursor: pointer;
    }

    .myui-button {
        position: relative;

        cursor: pointer;
        text-align: center !important;
        overflow: visible;
        /*background-color: red;*/
        overflow: hidden;
        /*width: 10%;*/
        max-height: 22px;
        padding-top: 5px;
        /*padding-bottom: 2px;*/

        background-color: #759ae9;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #759ae9), color-stop(50%, #376fe0), color-stop(50%, #1a5ad9), color-stop(100%, #2463de));
        background-image: -webkit-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -moz-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -ms-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -o-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        border-top: 1px solid #1f58cc;
        border-right: 1px solid #1b4db3;
        border-bottom: 1px solid #174299;
        border-left: 1px solid #1b4db3;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
        box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
        color: #fff;
        font: bold 12px/1 "helvetica neue", helvetica, arial, sans-serif;
        padding: 5px 0;
        text-shadow: 0 -1px 1px #1a5ad9;
        /*display: block !important;*/
        /*width: 150% !important;*/

    }

    }
    .myui-button:hover {
        background-color: #5d89e8;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #5d89e8), color-stop(50%, #2261e0), color-stop(50%, #044bd9), color-stop(100%, #0d53de));
        background-image: -webkit-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -moz-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -ms-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -o-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        cursor: pointer;
    }

    .myui-button:active {
        border-top: 1px solid #1b4db3;
        border-right: 1px solid #174299;
        border-bottom: 1px solid #133780;
        border-left: 1px solid #174299;
        -webkit-box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
        box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
    }

    /*.myui-button span{*/
    /*text-align: center;*/
    /*margin-top: 10px;*/
    /*min-width: 100%;*/

    /*}*/
</style>
<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">
<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script>
    function checkUpload(field, rules, i, options) {
        var uploadButtons = document.getElementsByName('doc_title[]');
        var uploadFileNames = [];
        for (i = 0; i < uploadButtons.length; ++i) {
//                    console.log(uploadButtons[i].value);
            if (field[0] === uploadButtons[i]) {
                break;
            } else {
                uploadFileNames.push(uploadButtons[i].value);
            }
        }
        if (uploadFileNames.indexOf(field[0].value) >= 0) {
            return 'Filename already exists';
        }
    }
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
        jQuery("#reg_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});
        jQuery("#ref_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});

    });

</script>
<!--Validation End-->
<script>
    function deletei(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        if (r == true) {
            var div = document.getElementById(p);
            while (div) {
                div.parentNode.removeChild(div);
                div = document.getElementById(p);
            }
            window.open('document_registration_delete.php?doc_reg_detail_id=' + p, '1376201640569', 'width=50,height=10,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=70,top=50');
            //window.location.href = "document_registration_delete.php?doc_reg_detail_id="+p;
        }

    }
</script>
<script>
    function myFunction(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        if (r == true) {
            var div = document.getElementById(p);
            while (div) {
                div.parentNode.removeChild(div);
                div = document.getElementById(p);
            }
            //alert(L);
            var k = 1;
            for (var j = 0; j <= L; j++) {
                //alert(L);
                if (document.getElementById(j)) {//alert(j);
                    document.getElementById('label' + j).innerHTML = "Recommendation " + (k++) + "";
                }
            }
            L = k;
        }
    }


</script>
<script>

    $(document).ready(function () {

        $("#btn2").click(function () {
            var j = 1, m = 1;
            for (; ;) {
                //alert(j);
                if (document.getElementById(j))
                    j++;
                m++;
                if (m > 50)break;
            }
            var i = j;
            $("#btn1").append("<div id='" + i + "'><input name='doc_reg_detail_id[]' type='hidden' value='0' /><div style='float:left;width:30%;'><input type='text' name='doc_title[]' class='validate[required,funcCall[checkUpload]]' ></div><div class='myfileupload-buttonbar'><label class='myui-button'>Add Files<input type='file' name='file_name[]' id='file' class='validate[required]' /></label></div><div style='float:left;width:15%;'>&nbsp;</div><div style='float:left;width:15%;'><input type='button' onclick='myFunction(" + (i) + ");' name='basic'  value='Delete' class='gridbutton'/></div><div style='clear:both'></div></div>");
        });

        $('#calendar2').val('<?php echo date("d-m-Y") ?>');
    });
</script>

<!--<div class='myfileupload-buttonbar'><label class='myui-button'><span >Add Files</span><input type='file' name='file_name[]' id='file' class='validate[required]' /></label></div>-->

<script>
    function download(p) {
        window.showModalDialog("download.php?doc_reg_detail_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:20px;dialogHeight:20px")
    }
</script>
</head>

<?php
require_once('../model/document_registration_info.php');
?>
<?php
$xml = simplexml_load_file("xml/document_registration_master_detail_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date_start = $information->doc_reg_date_start;
        $doc_title = $information->doc_title;
        $designation = $information->designation;
        $doc_name = $information->doc_name;
        $to = $information->to;
        $file = $information->file;
        $ref_no = $information->ref_no;
        $ref_date = $information->ref_date;
        $document_closed = $information->document_closed;
    }
}
?>
<?php
$user_id = 1;
//echo $_GET['ministry_member_id'];
if ($_GET['doc_reg_id'] != NULL) {
    $result = $document_registration_info->editrow(array($_GET['doc_reg_id'], $user_id));
}
?>
<body style="background-color:#FFFFFF;">
<form id="formID" class="formular" name="overlay_form" method="post" action="#" enctype="multipart/form-data">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <input name="doc_reg_id" type="hidden"
               value="<?php echo isset($result->doc_reg_id) ? $result->doc_reg_id : ''; ?>"/>

        <div>
            <div class="columnA"><label><?php echo $doc_reg_no; ?> </label></div>
            <div class="columnB"><input id="name" class='validate[required,maxSize[50]] text-input bangla' type="text"
                                        name="doc_reg_no"
                                        value="<?php echo isset($result->doc_reg_no) ? $result->doc_reg_no : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="columnA" style="text-align:right;"><label><?php echo $doc_reg_date_start; ?>
                    *&nbsp&nbsp;</label></div>
            <div class="columnB">
                <input style='width:90%' id="reg_date" type="text"
                       class='validate[required,maxSize[50]] text-input bangla' name="doc_reg_date"
                       value="<?php echo isset($result->doc_reg_date) ? $result->doc_reg_date : date('d-m-Y'); ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $doc_title; ?> *</label></div>
            <div class="columnB">
                <textarea name='doc_title_m' class='validate[required] text-input bangla' rows="15" cols="120"
                          style="font-size: 11px;width: 450px;resize: none;overflow-y: scroll;"><?php echo isset($result->doc_title) ? trim($result->doc_title) : ''; ?></textarea>
            </div>
            <div class="divclear">
            </div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $ref_no; ?></label></div>
            <div class="columnB"><input id="name" class='text-input bangla' type="text" name="ref_no"
                                        value="<?php echo isset($result->ref_no) ? $result->ref_no : ''; ?>"/></div>
            <div class="columnA" style="text-align:right;"><label><?php echo $ref_date; ?> &nbsp;&nbsp;</label></div>
            <div class="columnB">
                <input style='width:90%' id="ref_date" type="text" class='text-input bangla' name="ref_date"
                       value="<?php echo isset($result->ref_date) ? $result->ref_date : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $document_closed; ?></label></div>
            <div class="columnB"><input id="name" class='text-input bangla' type="checkbox"
                                        name="document_closed" <?php echo isset($result->is_delete) && $result->is_delete === '1' ? 'checked="checked"' : ''; ?>
                                        value="1"/></div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div class="divclear"></div>
        <div>
            <fieldset style="width:100%;">
                <legend>Upload Document</legend>
                <div style='margin:3px;'>
                    <input type="button" name="basic" id="btn2" value="Add" class="newbutton"/>

                    <div style="width:87%; overflow:hidden; overflow-y:scroll; height:200px;" id="btn1">
                        <div>
                            <div style="float:left;width:30%;"><?php echo $doc_name; ?></div>
                            <div style="float:left;width:30%;"><?php echo $file; ?></div>
                            <div style="float:left;width:15%;">&nbsp;</div>
                            <div style="float:left;width:15%;">&nbsp;</div>
                            <div style='clear:both'></div>
                        </div>
                        <?php
                        if ($_GET['doc_reg_id'] != NULL) {
                            $doc_reg_id = $_GET['doc_reg_id'];
                            $user_id = $_SESSION['user_id'];
                            echo $document_registration_info->gridviewDetails($doc_reg_id, $user_id);
                        }
                        ?>
                    </div>
                    <div style='clear:both'></div>
                </div>
            </fieldset>
        </div>
        <div style="text-align:center; float:left; width:65%">
            <input type="button" value="Close" name="btn_close" class="popupbutton" onclick="window.close();"/>
            &nbsp;&nbsp;<input type="button" onclick="window.location.reload(true);" value="Refresh"
                               class="popupbutton"/>
            &nbsp;&nbsp;
            <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
<script type="text/javascript">

</script>