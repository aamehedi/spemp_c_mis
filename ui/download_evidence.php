<?php
session_start();
$user_id = $_SESSION['user_id'];
require_once('../model/receive_evidance_info.php');
if ($_GET['rec_evidence_details_id'] != NULL) {
    $result = $receive_evidance_info->download(array($_GET['rec_evidence_details_id'], $user_id));
    $file = $result->file_name;
    $ext = end(explode('.', $file));
//    var_dump($ext);
//    die();
//	echo $file;
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $result->title . '.' . $ext . '"');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}
?>
