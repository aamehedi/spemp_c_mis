<?php
session_start();

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('MYPDF.php');

require_once('../model/meeting_notice_info.php');
$user_id = $_SESSION['user_id'];
//echo $_GET['meeting_notice_id'];
if ($_GET['meeting_notice_id'] != NULL) {
    $result = $meeting_notice_info->rpt_meeting_notice(array($_GET['meeting_notice_id']));
}
// var_dump($result);
// die();

$confData = array(
    'pageTitle' => 'Manage Notice Report',
    'committeeName' => $result->committee_name,
    'parliamentName' => 'Parliament of Bangladesh',
    'reportName' => 'NOTICE OF MEETING',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => true
);

// var_dump($confData);
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);


$pdf->AddPage();

if ($result->is_issued === '0' && $result->is_approved === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// var_dump($pdf->fontName);
$pdf->SetFont($pdf->fontName, '', 12);
// if ($_SESSION['language_id'] === '1') {

// }else{

// 	////////////////////
// }
$time = strtotime($result->en_date);
$newformat = date('Y-m-d', $time);

$pdf->Cell(0, 5, '', 0, 1, 'C', false, '', 0, false, 'C', 'B');
$pdf->SetFont('cambria', 'B');
$html = '<b>Date:</b>';
$pdf->writeHTMLCell(13, 0, '', '', $html, 0, 0, 0, true, '', true);
$pdf->SetFont('cambria');
$html = $newformat . '&nbsp;';
$html .= '<label style="font-size:18px;">' . $result->bn_date . '</label>';
$pdf->writeHTMLCell(80, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria', 'B');
$html = '<b>Time:</b>';
$pdf->writeHTMLCell(14, 0, '', '', $html, 0, 0, 0, true, '', true);
$pdf->SetFont('cambria');
$html = htmlspecialchars_decode($result->time);
$pdf->writeHTMLCell(35, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria', 'B');
$html = '<b>Location:</b>';
$pdf->writeHTMLCell(22, 0, '', '', $html, 0, 0, 0, true, '', true);
$pdf->SetFont('cambria');
$html = htmlspecialchars_decode($result->venue_name) . '<br>';
$pdf->writeHTMLCell(80, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria', 'B');
$html = '<br/>';
$html .= '<b>ITEMS</b>';
$html .= '<div style="margin:5px;">';
$html .= '<br><b>Private Business</b><br/>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria');
$html = htmlspecialchars_decode($result->private_business_before) . '<br>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria', 'B');
$html = '<br><b>Public Business</b><br/>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria');
$html = htmlspecialchars_decode($result->public_business) . '<br>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria', 'B');
$html = '<br><b>Private Business </b><br/>';
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
$pdf->SetFont('cambria');
$html = '<div style="padding-left:12px;">' . htmlspecialchars_decode($result->private_business_after) . '</div><br>';
$html .= '</div>';

// $html = '<div style="margin:5px;">';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times', '', 12);
// $html = '<b>Inquiry No.:</b>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont($pdf->fontName, '', 12);
// $html = $result->inquiry_no.'<br>';
// $html .= '<b>Inquiry Title:</b>'.htmlspecialchars_decode($result->inquiry_title).'<br>';
// $html .= '<b>Create Date:</b>'.htmlspecialchars_decode($result->create_date).'<br>';
// $html .= '<b>Issued Date:</b>'.htmlspecialchars_decode($result->issued_date).'<br>'; 
// $html .= '<b>Proposed Date:</b>'.htmlspecialchars_decode($result->proposed_date).'<br>'; 
// $html .= '<b>Document(s):</b>'.htmlspecialchars_decode($result->doc_title).'<br>'; 
// $html .= '<b>Ministry(s):</b>'.htmlspecialchars_decode($result->ministry_name).'<br>'; 
// $html .= '</div>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
if ($result->is_issued === '1' || $result->is_approved === '1') {
    $pdf->Output('issued_approved_pdf/pdf_rpt_meeting_notice_' . $_GET['meeting_notice_id'] . '.pdf', 'F');
    echo "<script>window.close();</script>";
} else {
    $pdf->Output('pdf_rpt_meeting_notice_' . $_GET['meeting_notice_id'] . '.pdf', 'I');
}
//============================================================+
// END OF FILE
//============================================================+
