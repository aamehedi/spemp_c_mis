<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>

<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>
<script type='text/javascript'>
    $(document).ready(function () {
        $('input[value="Print View"]').click(function () {
            if ($(this).prev().val() == '1') {
                window.showModalDialog("../pdf/briefing_note/pdf_rpt_briefing_note_" + $(this).attr('name') + ".pdf", "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px");
            } else {
                window.showModalDialog("rpt_briefing_note.php?meeting_notice_id=" + $(this).attr('name'), "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            }
            ;
        });
    });
</script>
<script type='text/javascript'>

    function popupwinid(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("briefing_note_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }

    function issuedid(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("briefing_note_issed.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }
</script>
<?php
require_once('../model/briefing_note_info.php');
require_once('../model/meeting_notice_info.php');
?>
<?php
$xml = simplexml_load_file("xml/briefing_note_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $committee = $information->committee;
        $introduction = $information->introduction;
        $key_issue = $information->key_issue;
        $advisor_question = $information->advisor_question;
        $witness_question = $information->witness_question;
        $written_evidence = $information->written_evidence;
        $date_start = $information->date_start;
        $data_end = $information->data_end;
        $sitting_no_ = $information->sitting_no;
        $inquiry_title = $information->inquiry_title;
        $sitting_date = $information->sitting_date;
        $date = $information->date;
    }
}
?>
<link href="Style/home.css" rel="stylesheet" type="text/css"/>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php
    if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['committe_id']) || isset($_GET['ref_no']) || isset($_GET['sitting_id'])) {
        echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
    } else {
        echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
    }
    ?>
    <fieldset>
        <legend>Search Criteria</legend>
        <div>
            <div style='float:left;width:22.5%;'><?php echo $ref_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="ref_no"/></div>
            <div style='float:left;width:22.5%;'><?php echo $sitting_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="sitting_id"/></div>
            <div style='clear:both'></div>
        </div>
        <div style="margin-bottom:4px;">
            <div style='float:left;width:22.5%;'><?php echo $committee; ?></div>
            <div style='float:left;width:25%'>

                <?php
                $id = isset($result->sub_committee_id) ? $result->sub_committee_id : '';
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $user_id = $_SESSION['user_id'];
                echo '<select name="sub_committee_id">';
                echo $meeting_notice_info->comboview_committee($language_id, $committee_id, $user_id, $id);
                echo '</select>';
                ?>
            </div>
            <div style="float:left;"><input type="submit" value="Search" name="search_button" class="newbutton"/></div>
            <div style='clear:both'></div>
        </div>
    </fieldset>
    </div>
</form>
<?php
if (isset($_GET['start_date'])) {
    $start_date = $_GET['start_date'];
}
if (isset($_GET['end_date'])) {
    $end_date = $_GET['end_date'];
}
if (isset($_GET['sub_committee_id'])) {
    if (($_GET['sub_committee_id']) == NULL) {
        $sub_committee_id = 0;
    } else
        $sub_committee_id = $_GET['sub_committee_id'];
}
$search_ref_no = '';
if (isset($_GET['ref_no'])) {
    $search_ref_no = $_GET['ref_no'];
}
if (isset($_GET['sitting_id'])) {
    $sitting_id = $_GET['sitting_id'];
}
?>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($ref_no, $sitting_no_, $date, $inquiry_title);
//    var_dump($param);
    if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['sub_committee_id']) || isset($_GET['ref_no']) || isset($_GET['sitting_id'])) {
        echo $briefing_note_info->searchgridview(' ', ' ', $search_ref_no, $sub_committee_id, $sitting_id, $committe_id, $language_id, $user_id, $param);
    } else {
        echo $briefing_note_info->searchgridview(' ', ' ', ' ', 0, ' ', $committe_id, $language_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>