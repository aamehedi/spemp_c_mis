<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>


<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>
<script type='text/javascript'>
    function popupwinid(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("meeting_notice_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }
    function printid(p) {

        window.showModalDialog("rpt_meeting_notice.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }
</script>
<?php
require_once('../model/meeting_notice_info.php');
require_once('../model/sub_committee_info.php');
?>
<?php
$xml = simplexml_load_file("xml/meeting_notice_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $sitting_no_ = $sitting_no;
        $venue_ = $venue;
    }
}
?>
<link href="Style/home.css" rel="stylesheet" type="text/css"/>
<div id="head_info">
</div>
<div style="background-color:#61399D;color:#FFFFFF;">
    <b><?php echo $heading; ?></b>

    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<?php
if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['ref_no']) || isset($_GET['id']) || isset($_GET['sitting_no'])) {
    echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
} else {
    echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
}
?>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <fieldset>
        <div>
            <div style='float:left;width:10.5%;'><?php echo $date; ?></div>
            <div style='float:left;width:15%'>
                <input type="text" style='width:90%' id="calendar" name="startdate"/>
                <!--<a href="javascript:NewCal('demo1','ddmmyyyy')"><img src="datepopup/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>-->
            </div>
            <div style='float:left;width:8.5%;'><?php echo $to; ?></div>
            <div style='float:left;width:15%'>
                <input type="text" style='width:90%' id="calendar2" name="enddate"/>
                <!--<a href="javascript:NewCal('demo11','ddmmyyyy')"><img src="datepopup/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>-->
            </div>
            <div style='float:left;width:8.5%;'><?php echo $ref_no; ?></div>
            <div style='float:left;width:15%'><input type="text" style='width:90%' name="ref_no"/></div>
            <div style='float:left;width:12.5%;'><input type="submit" name="basic" value="Search" class="newbuttondiv"/>
            </div>
            <div style='float:left;width:12.5%;'></div>
            <div style='clear:both'></div>
        </div>
        <div style="margin-bottom:4px;">
            <div style='float:left;width:10.5%;'><?php echo $committee; ?></div>
            <div style='float:left;width:37.5%'>
                <?php
                $id = 1;
                $language_id = $_SESSION['language_id'];
                $user_id = $_SESSION['user_id'];
                echo $sub_committee->comboview($language_id, $user_id, $id);
                ?>
            </div>
            <div style='float:left;width:10.5%;'><?php echo $sitting_no; ?></div>
            <div style='float:left;width:15%'><input type="text" style='width:90%' name="sitting_no"/></div>
            <div style='clear:both'></div>
        </div>
        <div>

        </div>
    </fieldset>
    </div>
</form>
<?php

if (isset($_GET['startdate'])) {
    $start_date = $_GET['startdate'];
}
if (isset($_GET['enddate'])) {
    $end_date = $_GET['enddate'];
}
if (isset($_GET['ref_no'])) {
    $ref_no = $_GET['ref_no'];
}

if (isset($_GET['id'])) {
    $id = $_GET['id'];
}
if (isset($_GET['sitting_no'])) {
    $sitting_no = $_GET['sitting_no'];
}
//echo $start_date.'--'.$end_date;
?>
<div class="content" id="conteudo">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
    <?php
    if (isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['ref_no']) || isset($_GET['id']) || isset($_GET['sitting_no'])) {
        $language_id = $_SESSION['language_id'];
        $user_id = $_SESSION['user_id'];
        $committe_id = $_SESSION['committee_id'];
        $param = array($sitting_no_, $date, $venue_);
        echo $meeting_notice_info->searchgridview($start_date, $end_date, $ref_no, 0, $sitting_no, $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>