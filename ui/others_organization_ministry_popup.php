<?php
session_start();
//echo $_SESSION['language_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/others_info.php');
//echo  $_GET['others_org_ministry_id'];
$user_id = $_SESSION['user_id'];
if ($_GET['others_org_ministry_id'] != NULL) {
    $result = $others_info->editrow(array($_GET['others_org_ministry_id'], $user_id));
    // var_dump($result);
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 250px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->


    <?php
    $xml = simplexml_load_file("xml/organization_popup.xml");
    $headingpopup = ($_SESSION['language_id'] === '1') ? 'Others Organization Ministry Information Entry' : 'অন্য সংস্থা মন্ত্রণালয় তথ্য ঢোকান / আপডেট করুন';
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $id = $information->id;
            $organization_name = $information->organization_name;
            $short_name = $information->short_name;
            $section = $information->section;
            $branch = $information->branch;
            $contact_person = $information->contact_person;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $comments = $information->comments;
        }
    }

    ?>
</head>
<body style="background-color:#FFFFFF">
<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="others_org_ministry_id"
                                        value="<?php echo isset($result->others_org_ministry_id) ? $result->others_org_ministry_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $organization_name; ?> *</label></div>
            <div class="columnB"><input type='text' class='validate[required,maxSize[150]] text-input bangla'
                                        name='name' value="<?php echo isset($result->name) ? $result->name : ''; ?>"
                                        style="font-size: 11px;width: 340%;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $short_name; ?></label></div>
            <div class="columnB"><input type='text' class='validate[maxSize[5]] text-input bangla' name='short_name'
                                        value="<?php echo isset($result->short_name) ? $result->short_name : ''; ?>"
                                        style="font-size: 11px;width: 340%;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $contact_person; ?> *</label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='contact_person' class='validate[required,maxSize[80]] text-input bangla'
                           value="<?php echo isset($result->contact_person) ? $result->contact_person : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $designation; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' name='designation' class='validate[required,maxSize[100]] text-input bangla'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='phone' class='text-input bangla'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?>
                </div>
                <div style="float:left;">
                    <input type='text' name='cell_phone' class='text-input bangla'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='fax' class='validate[maxSize[20]] text-input bangla'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' class='validate[custom[email],maxSize[20]] text-input bangla' name='email'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?></label></div>
            <div class="columnB"><textarea type='text' class='validate[maxSize[250]] text-input bangla' name='address'
                                           style="font-size: 12px;width: 450px;"><?php echo isset($result->address) ? $result->address : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB"><textarea type='text' name='comments' class='validate[maxSize[250]] text-input bangla'
                                           style="font-size: 12px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>


        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
        <!-- div Button-->
        <div>
            <div style="float:left;width:5%;">
                &nbsp;
            </div>
            <div style="float:left;width:60%;">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
                &nbsp;&nbsp;&nbsp;&nbsp;<input id="send" name="btn_save" type="submit" value="Save"
                                               class="popupbutton"/>
            </div>
            <div style="float:left;width:20%;">
            </div>
            <div class="divclear"></div>
        </div>
    </div>
</form>
</body>
</html>
