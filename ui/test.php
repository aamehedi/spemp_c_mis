<?php

require_once 'tcpdf/tcpdf.php';

$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->SetCreator(PDF_CREATOR);
// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, 60, PDF_MARGIN_RIGHT);
// $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// ---------------------------------------------------------

$fontName = $pdf->addTTFfont(K_PATH_FONTS . 'kalpurush.ttf', '', '', 32);
// set default font subsetting mode
$pdf->setFontSubsetting(true);
$pdf->setFont($fontName, '', 40);
$pdf->AddPage();
// set document information
$pdf->MultiCell(90, 10, 'পাবিলক', false, 'L', false, 0, 43, 13);
// $pdf->MultiCell(90, 10, 'It works', false, 'L', false, 0, 43, 13);

$pdf->Output('test.pdf', 'I');

// exec('ping 8.8.8.8', $output);
?>
<!--
<pre>
	<?php var_dump($output) ?>
</pre>
