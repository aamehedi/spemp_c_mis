<?php
if (session_id() == '') {
    session_start();
}
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
if (empty($_SESSION['committee_id'])) {
    header('Location: committee_set.php');
    exit;
}
// Why we need to store userDetails in every page
//  require_once '../biz/user_biz.php';
//  $user_biz = new user_biz();
//  $userDetails = $user_biz->getone($_SESSION['user_id']);
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>

<head>
    <title>SPO MIS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <?php if ($_SESSION["language_id"] == 1) : ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" href="css/grid.css" rel="stylesheet" media="screen"/>
    <?php else : ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" href="css/bngrid.css" rel="stylesheet" media="screen"/>
    <?php endif; ?>
    <link type='text/css' href='css/menu.css' rel='stylesheet' media='screen'/>
</head>
<body>
<div id="wrapper">
    <div class="header" onload="doonload()">
        <div
            class="header_text"><?php if (isset($_SESSION['committee_name'])) echo $_SESSION['committee_name']; else echo '&nbsp;'; ?></div>
        <div style="margin-top:-17px;margin-left:97%;"><a href="index.php"><img src="img/logo-fb_2f.gif" height="33"
                                                                                width="34"/></a></div>
        <div style="margin:-22px;">
            <div class="header_font" id="boldStuff">
                <?php
                if ($_SESSION['language_id'] == 1) {
                    echo '<a href="committee_set.php?language_id=1"><img src="img/e_white.png"></a><a href="committee_set.php?language_id=2"><img src="img/b_grey.png"></a>';
                } else
                    echo '<a href="committee_set.php?language_id=1"><img src="img/e_grey.png"></a><a href="committee_set.php?language_id=2"><img src="img/b_white.png"></a>';
                ?>
            </div>
            <div class="header_contain">&nbsp;&nbsp;<img src="img/m5.png" height="15"
                                                         width="12">&nbsp;&nbsp;<?php echo strtoupper($_SESSION["user_name"]); ?>
                &nbsp;&nbsp;&nbsp;&nbsp;<a href="logout.php">Logout</a></div>
        </div>
    </div>
