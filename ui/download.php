<?php
session_start();
$user_id = $_SESSION['user_id'];
require_once('../model/document_registration_info.php');
if ($_GET['doc_reg_detail_id'] != NULL) {
    $result = $document_registration_info->editrow_details(array($_GET['doc_reg_detail_id'], $user_id));
    $file = $result->file_name;

    $ext = end(explode('.', $file));
//    var_dump($result->doc_title);
//    var_dump($ext);
//    die();
    //echo $file;

    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename="' . $result->doc_title . '.' . $ext . '"');
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}
?>
