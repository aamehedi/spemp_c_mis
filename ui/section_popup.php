<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/venue_info.php');
//echo  $_GET['parliament_id'];
$user_id = 1;
if ($_GET['venue_id'] != NULL) {
    $result = $venue_info->editrow(array($_GET['venue_id'], $user_id));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<body style="background-color:#FFFFFF;">
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2>Insert/Update Venue Info</h2>
    </br>
    <div>

        <div>
            <div class="columnA"><label>ID</label></div>
            <div class="columnB"><input id="name" type="text" name="venue_id"
                                        value="<?php echo isset($result->venue_id) ? $result->venue_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label>Venue : </label></div>
            <div class="columnB"><input id="name" type="text" name="venue_name"
                                        value="<?php echo isset($result->venue_name) ? $result->venue_name : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="1"/>
        <input name="user_id" type="hidden" value="1"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                &nbsp;&nbsp;<input id="send" name="btn_close" type="submit" value="Close" class="popupbutton"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>

