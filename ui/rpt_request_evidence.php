<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/request_evidence_info.php');
if ($_GET['request_evidence_id'] != NULL) {
    $result = $request_evidence_info->editrow(array($_GET['request_evidence_id'], '1'));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>

    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Request Evidence</title>
    <?php else: ?>
        <title>প্রমাণ অনুরোধ</title>
    <?php endif; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
</head>
<body>
<?php if ($result->is_issued === '1') : ?>
<div style="margin:10px; width:80%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:80%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat; background-size: 200px 180px"
        id="message_area">
        <?php endif; ?>
        <div>
            <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            <div style="float:left; height:80px; padding-top:25px; padding-left:30px;">

                <?php if (isset($language_id) && $language_id === '1'): ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">Parliament of Bangladesh</div>
                <?php else: ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">বাংলাদেশ জাতীয় সংসদ</div>
                <?php endif; ?>
                <div style="font-size:18px;"></div>
            </div>
            <div style="clear:both;"></div>
        </div>
        <br/>
        <?php if (isset($language_id) && $language_id === '1'): ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">Request Evidence</div>
        <?php else: ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">প্রমাণ অনুরোধ</div>
        <?php endif; ?>

        <div style="margin:5px;">
            <?php
            echo htmlspecialchars_decode($result->cover_letter) . '<br>';
            ?>
        </div>
    </div>
    <?php if ($result->is_issued !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
