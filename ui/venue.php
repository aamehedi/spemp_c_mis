<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("venue_popup.php?venue_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:480px;dialogHeight:200px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "venue_delete.php?venue_id=" + p;
        }
        else {

        }
    }
</script>
<?php
require_once('../model/venue_info.php');
?>
<?php
$xml = simplexml_load_file("xml/venue_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $venue_name = $information->venue_name;

    }
}

?>

<div id="head_info">
    <?php echo $heading; ?>
</div>
<div style="margin-top:3px;">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($id, $venue_name);
    echo $venue_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>