<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/commiittee_branch_info.php');
//echo  $_GET['parliament_id'];
$user_id = 1;
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/committee_branch.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {

            $heading = $information->heading;
            $id = $information->id;
            $branch = $information->branch;
        }
    }

    ?>
</head>
<body style="color:#61399D;">
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo $heading; ?></h2>
    </br>
    <div>
        <input name="branch_id" type="hidden" value="0"/>

        <div>
            <div class="columnA"><label><?php echo $branch; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="branch" value=""/></div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                &nbsp;&nbsp;<input type="submit" name="btn_close" value="Close" class="popupbutton"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>

</body>
</html>
