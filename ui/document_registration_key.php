<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("document_registration_key_popup.php?doc_reg_id=" + p, "", "dialogTop:150px;dialogLeft:center;dialogWidth:850px;dialogHeight:600px")
    }
</script>
<?php
require_once('../model/document_registration_info_key.php');
?>
<?php
$xml = simplexml_load_file("xml/document_key_areas_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date_start = $information->doc_reg_date_start;
        $doc_title = $information->doc_title;
        $recommendations = $information->recommendations;
        $result_forcasting = $information->result_forcasting;
        $to = $information->to;
        $key_area = $information->key_area;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <div style="margin-top:3px;">
        <fieldset>
            <legend>Search Criateria</legend>
            <div style='margin:3px;'>
                <div style='float:left;width:21.5%;'><?php echo $doc_reg_no; ?></div>
                <div style='float:left;width:26%'><input type="text" style='width:90%' name="doc_reg_no"/></div>
                <div style='float:left;width:17%;'><?php echo $doc_reg_date_start; ?></div>
                <div style='float:left;width:15%'>
                    <input type='text' style='width:90%' id="calendar" name="doc_reg_date_start">
                </div>
                <div style='float:left;width:5%;'><?php echo $to; ?></div>
                <div style='float:left;width:15%'>
                    <input type="text" style='width:90%' id="calendar2" name="doc_reg_date_end">
                </div>
                <div style='float:left;width:21.5%;'><?php echo $doc_title; ?></div>
                <div style='float:left;width:78.5%;'><input type="text" style='width:90%' name="doc_title"/></div>
                <!--<div style='float:left;width:21.5%;'>Schedule Date From:</div>
                <div style='float:left;width:15%'><input type="text" style='width:90%' id="calendar3" name="sch_reg_date_start"/></div>
                <div style='float:left;width:5%;'>To:</div>
                <div style='float:left;width:15%'><input type="text" style='width:90%' id="calendar4" name="sch_reg_date_end"/></div>
                -->
                <div style='clear:both'></div>
            </div>
            <div>
                <div style="float:left;text-align:right;width:100%;">
                    <!--<input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>-->
                    <input type="submit" value="Search" name="search_button" class="newbutton"/>
                </div>
            </div>
        </fieldset>
    </div>
</form>
<?php
if (isset($_GET['doc_reg_no'])) {
    $doc_reg = $_GET['doc_reg_no'];
}
if (isset($_GET['doc_reg_date_start'])) {
    $doc_reg_date_s = $_GET['doc_reg_date_start'];
}
if (isset($_GET['doc_reg_date_end'])) {
    $doc_reg_date_end = $_GET['doc_reg_date_end'];
}
if (isset($_GET['doc_title'])) {
    $doc_t = $_GET['doc_title'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
?>
<div class="content" id="conteudo">
    <?php
    if (isset($_GET['doc_reg_no']) || isset($_GET['doc_reg_date_start']) || isset($_GET['doc_reg_date_end']) || isset($_GET['doc_title'])) {
        $language_id = $_SESSION['language_id'];
        $user_id = $_SESSION['user_id'];
        $committe_id = $_SESSION['committee_id'];
        $param = array($doc_reg_no, $doc_reg_date_start, $doc_title);
        echo $document_registration_info_key->searchgridview($doc_reg, $doc_reg_date_s, $doc_reg_date_end, $doc_t, $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>