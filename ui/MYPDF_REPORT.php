<?php

require_once 'tcpdf/tcpdf.php';

class MYPDF_REPORT extends TCPDF
{
    protected $pageTitle;
    public $fontName;

    //Page header
    public function Header()
    {

        // Logo
        // $image_file = 'img/logo.png';
        // $this->Image($image_file, 10, 10, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);
        // Set font
        // $this->setFontSubsetting(true);
        // if ($this->confData) {
        // 	$this->SetFont($this->fontName, 'B', 20, 'B', 'false');
        // 	$this->MultiCell(90, 10, $this->confData['committeeName'].' '.$this->confData['parliamentName'], false, 'L', false, 0, 43, 13);
        // 	$this->Ln();
        // 	$this->Ln();
        // 	$this->MultiCell(0, 10, $this->confData['reportName'], false, 'C', false, 1);
        // }


    }

    function __construct($PDF_PAGE_ORIENTATION, $PDF_UNIT, $PDF_PAGE_FORMAT, $true, $charSet, $condition, $pageTitle)
    {
        parent::__construct($PDF_PAGE_ORIENTATION, $PDF_UNIT, $PDF_PAGE_FORMAT, $true, $charSet, $condition);
        $this->pageTitle = $pageTitle;
        // create new PDF document

        // set document information
        $this->SetCreator(PDF_CREATOR);
        // $this->SetAuthor('Nicola Asuni');
        $this->SetTitle($this->pageTitle);
        $this->setCellHeightRatio(0.8);

        // set margins
        $this->SetMargins(PDF_MARGIN_LEFT, 60, PDF_MARGIN_RIGHT);

        // $this->SetHeaderMargin(PDF_MARGIN_HEADER);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        // set auto page breaks
        $this->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

        // set image scale factor
        $this->setImageScale(PDF_IMAGE_SCALE_RATIO);

        // set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $this->setLanguageArray($l);
        }

        // ---------------------------------------------------------

        // set default font subsetting mode
        $this->setFontSubsetting(true);

        // Set font
        // dejavusans is a UTF-8 Unicode font, if you only need to
        // print standard ASCII chars, you can use core fonts like
        // helvetica or times to reduce file size.
        // var_dump($this->confData);
        if ($this->confData['language'] === 'english') {
            $this->fontName = 'cambriab';

            // $this->fontName = $this->addTTFfont(K_PATH_FONTS.'Cambria.ttf', 'OpenType', 'B', 32);
        } else {
            // require_once 'fonts/sutonnymj.php';
            // $this->AddFont('SolaimanLipi', '', 'fonts/solaimanlipi.php');

            // $this->fontName = 'solaimanlipi';
            $this->fontName = $this->addTTFfont(K_PATH_FONTS . 'kalpurush.ttf', '', '', 32);


        }
    }
}
