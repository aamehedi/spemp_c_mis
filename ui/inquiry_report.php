<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>

<script type='text/javascript'>
    function popupwinid(p) {
        //$('<div></div>').load('inquiry_report_popup.php?id='+p).modal();
        window.showModalDialog("inquiry_report_popup.php?inquiry_report_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }

    // 	function deleteid(p)
    // 	{
    // 		//$('<div></div>').load('inquiry_report_popup.php?id='+p).modal();
    // // window.showModalDialog("pdf_rpt_inquiry_report.php?inquiry_report_id="+p,"","dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
    // window.showModalDialog("rpt_inquiry_report.php?inquiry_report_id="+p,"","dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
    // 		//return false;
    // 	}

    function approvedid(p) {
        //$('<div></div>').load('inquiry_report_popup.php?id='+p).modal();
        window.showModalDialog("inquiry_report_approved.php?inquiry_report_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }
    $(document).ready(function () {
        $('input[value="Print View"]').click(function () {
            if ($(this).prev().val() == '1') {
                window.showModalDialog("../pdf/inquiry_report/pdf_rpt_inquiry_report_" + $(this).attr('name') + ".pdf", "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px");
            } else {
                window.showModalDialog("rpt_inquiry_report.php?inquiry_report_id=" + $(this).attr('name'), "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            }
            ;
        });
    });
</script>
<?php
require_once('../model/inquiry_report_info.php');
?>
<?php
$xml = simplexml_load_file("xml/inquiry_report_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $report_no = $information->report_no;
        $report_date = $information->report_date;
        $title = $information->title;
        $parliament = $information->parliament;
        $session = $information->session;
        $executive_summary = $information->executive_summary;
        $introduction = $information->introduction;
        $issues = $information->issues;
        $recommendation = $information->recommendation;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $date_range = $information->date_range;
        $inquiry_no_ = $inquiry_no;
        $report_no_ = $report_no;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php
    if (isset($_GET['report_date_start']) || isset($_GET['report_date_end']) || isset($_GET['report_no']) || isset($_GET['inquiry_no']) || isset($_GET['parliament_id'])) {
        echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
    } else {
        echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
    }
    ?>
    <fieldset>
        <legend>Search Criteria</legend>
        <div style='margin:3px;'>
            <div style='float:left;width:22.5%;'><?php echo $parliament; ?></div>
            <div style='float:left;width:25%'><?php $language_id = $_SESSION['language_id'];
                $user_id = $_SESSION['user_id'];
                $committee_id = $_SESSION['committee_id'];
                echo $inquiry_report_info->comboviewparliament(0, $language_id, $committee_id, $user_id);
                ?>
            </div>
            <div style='float:left;width:22.5%;'><?php echo $report_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="report_no"/></div>
            <div style='clear:both'></div>
        </div>
        <div>
            <div style='float:left;width:22.5%;'><?php echo $inquiry_no; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="inquiry_no"/></div>
            <div style="float: left;width: 100%"><input type="submit" value="Search" name="search_button"
                                                        class="newbutton"/></div>
            <div style='clear:both'></div>
        </div>
    </fieldset>
    </div>
</form>
<?php
if (isset($_GET['report_date_start'])) {
    $report_date_start = $_GET['report_date_start'];
}
if (isset($_GET['report_date_end'])) {
    $report_date_end = $_GET['report_date_end'];
}
if (isset($_GET['report_no'])) {
    $report_no = $_GET['report_no'];
}
if (isset($_GET['inquiry_no'])) {
    $inquiry_no = $_GET['inquiry_no'];
}
if (isset($_GET['parliament_id'])) {
    $parliament_id = $_GET['parliament_id'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
?>
<div class="content" id="conteudo">
    <input type="button" name="basic" value="Add New" onclick="popupwinid(0);" class="newbutton"/>
    <?php
    $language_id = $_SESSION['language_id'];
    $committe_id = $_SESSION['committee_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($report_date, $report_no_, $inquiry_no_);
    if (isset($_GET['report_date_start']) || isset($_GET['report_date_end']) || isset($_GET['report_no']) || isset($_GET['inquiry_no']) || isset($_GET['parliament_id'])) {

        echo $inquiry_report_info->searchgridview(' ', ' ', $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param);
    } else {
        echo $inquiry_report_info->searchgridview(' ', ' ', ' ', ' ', ' ', $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>