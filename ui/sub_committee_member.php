<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("sub_committee_member_popup.php?sub_committee_member_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:560px;dialogHeight:220px")
    }

    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "session_delete.php?session_id=" + p;
        }
        else {

        }
    }
</script>
<script>
    function show(str) {
        if (str == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "session_data.php?parliament_id=" + str, true);
        xmlhttp.send();
    }
</script>
<?php
require_once('../model/sub_committee_member_info.php');
?>
<?php
$xml = simplexml_load_file("xml/sub_committee_member.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $id = $information->id;
        $session = $information->session;
        $parliament = $information->parliament;
    }
}

?>
<div id="head_info">
    <?php
    echo $heading;
    ?>
</div>
<div style="padding-bottom:10px;">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
    <br/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($id, $session);
    echo $sub_committee_member_info->gridview($language_id, $committe_id, $user_id, $param);

    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>