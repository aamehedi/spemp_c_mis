<?php
session_start();
//echo $_SESSION['language_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/committee_member_info.php');
require_once('../dal/data_access.php');
?>
<?php
$user_id = $_SESSION['user_id'];
//echo $_GET['organization_member_id'];
if ($_GET['id'] != NULL) {
    $result = $committee_member_info->editrow(array($_GET['id']));
}
$sql = "SELECT * FROM tbl_committee_member WHERE committee_id = '" . $_SESSION['committee_id'] . "' AND language_id = '" . $_SESSION['language_id'] . "' AND is_chairman = 1";
$data_access = new data_access();
$mysqli = $data_access->get_connection();
$hasChairman = 0;
if ($mysqli->query($sql)->num_rows > 0) {
    $hasChairman = 1;
}
$result2 = null;
if ($_GET['id'] != NULL) {
    $sql = "SELECT * FROM tbl_committee_member WHERE id = '" . $_GET['id'] . "'";
    $result2 = mysqli_fetch_object($mysqli->query($sql));
}

//var_dump($result2);
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/committee_member_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $organization_member_name = $information->organization_member_name;
            $organization_name = $information->organization_name;
            $comments = $information->comment;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $member_no = $information->member_no;
            $is_chairman = $information->is_chairman;
        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">

<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    </br>
    <div style="width:820px;">
        <div>
            <div class="columnA"><label><?php echo $organization_member_name; ?> *</label>
            </div>
            <div class="columnB">
                <?php
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $member_id = isset($result->committee_member_id) ? $result->committee_member_id : '';
                $cm_index_id = isset($_GET['id']) ? $_GET['id'] : '';
                echo $committee_member_info->comboview($language_id, $committee_id, $member_id, $cm_index_id);
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $member_no; ?> *</label>
            </div>
            <div class="columnB">

                <input type="text" class="validate[required] text-input bangla" name="member_no"
                       value=<?php echo (isset($result2->member_no)) ? $result2->member_no : '' ?>>

            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $is_chairman; ?></label>
            </div>
            <div class="columnB">
                <input type="checkbox" class="text-input bangla" name="is_chairman" id="is_chairman"
                       value="1" <?php echo (!empty($result2) && $result2->is_chairman === '1') ? 'checked="checked"' : '' ?> >
            </div>
            <div class="divclear"></div>
        </div>
        <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
        <input name="hasChairman" id="hasChairman" type="hidden" value="<?php echo $hasChairman ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save_n" type="submit" value="Save" class="popupbutton"
                       onclick="return showConfirmation()"/>

            </div>
            <div class="columnBbutton">

                <script type="text/javascript">
                    function showConfirmation() {
                        if (document.getElementById('hasChairman').value == '1' && document.getElementById('is_chairman').checked == true) {
                            var r = confirm("Do you want to add this member as chairman?");
                            if (r == false) {
                                return false;
                            }
                            ;
                        }
                        ;
                    }

                    function myFunction() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
