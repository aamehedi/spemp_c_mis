<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <?php
    $xml = simplexml_load_file("xml/parliament_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $id = $information->id;
            $parliament = $information->parliament;
        }
    }
    require_once('../model/parliament_info.php');

    $user_id = $_SESSION['user_id'];
    if ($_GET['parliament_id'] != NULL) {
        $result = $parliament_info->editrow(array($_GET['parliament_id']));
    }
    ?>

    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->

</head>
<body style="background-color:#FFFFFF;">
<form id="formID" class="formular" method="post" action="#">
    <div>
        <h2 style="margin-bottom: 20px;"><?php echo $heading; ?></h2>
        <!--div form value -->
        <div>
            <input type="hidden" name="parliament_id"
                   value="<?php echo isset($result->parliament_id) ? $result->parliament_id : '[Auto]'; ?>"/>
        </div>
        <div style="width:300px;">
            <div style="float:left; width:25%;"><label><?php echo $parliament; ?> *</label></div>
            <div style="float:left;width:75%;">
                <input type="text" class="validate[required,maxSize[50]] text-input bangla" maxlength="50"
                       name="parliament" style="width: 100%;"
                       value="<?php echo isset($result->parliament) ? $result->parliament : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
            <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
        </div>
        <!-- div Button-->
        <div>
            <div style="float:left; width:5%;">&nbsp;</div>
            <div style="float:left; width:60%; text-align: left;">
                <div style="text-align: left; margin-top:10px; vertical-align:middle;">
                    <script>
                        function myFunction() {
                            window.close();
                        }
                    </script>

                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <button onClick="myFunction()" class="popupbutton">Close</button>
                    <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

                    <div class="divclear"></div>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
    </div>
    </div>
</form>
</body>
</html>
