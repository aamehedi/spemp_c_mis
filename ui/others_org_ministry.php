<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        $('<div></div>').load('others_org_ministry_popup.php?id=' + p).modal();
        return false;
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
        else {

        }
    }
</script>

<?php
require_once('../model/parliament_info.php');
?>
<?php
$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}

?>


<div id="head_info">
    <?php
    //echo $parliament_information;
    echo '<h2>Other Ministry & Organization</h2>'
    ?>
</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = 1;
    $user_name;
    $committe_id;
    $committe_name;
    $param = array($id, $parliament);
    echo $parliament_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>