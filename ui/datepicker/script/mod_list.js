// Mod: Month/Year List V1.0
// Core: Xin Calendar (In-Page Core) V1.0
//       Xin Calendar (Popup Window Core) V1.0

// Author: Xin Yang
// Last Modified: 16-Mar-2003

// Default setting
var yxMonthFormat = "Month", yxYearRange = 5, yxListBG = "#ffffff";

// User interface
function setListFormat(f, yr, bg) {
    if (f.search(/^mm$/i) != -1 || f.search(/^month$/i) != -1 || f.search(/^mon$/i) != -1) {
        yxMonthFormat = f;
    }

    yxYearRange = yr;
    yxListBG = bg;
}

// Internal function
function yxScrollYear(idx, y) {
    yxMoveYear(idx, y - yxCal.CY);
}
function yxScrollMonth(idx, m) {
    yxMoveMonth(idx, m - yxCal.CM);
}

function yxList(func, para) {
    if (yxCore == 1) {
        return func + "(" + para + ")";
    }
    else if (yxCore == 2) {
        return "if (window.opener && !window.opener.closed && window.opener." + func + ") window.opener." + func + "(" + para + ")";
    }
}

// Mod function
function yxCalHeader() {
    return yxCalOpen() + "<form>" + yxCalTableOpen() + yxCalTitle() + yxCalWeekdays();
}
function yxCalFooter() {
    return yxCalFootBar() + yxCalTableEnd() + "</form>" + yxCalEnd();
}

function yxCalTitle() {
    var idx = yxCal.idx, s = "";

    s += "<tr bgcolor='" + yxColors[0] + "'><td colspan='7' align='center'><table cellpadding='0' cellspacing='0' border='0' width='100%'><tr align='center' valign='middle'>";

    s += "<td align='left'><select onchange='" + yxList("yxScrollMonth", idx + ", this.selectedIndex") + "' style='font-family:" + yxConf[5] + "; font-size:" + yxConf[6] + "px; background-color:" + yxListBG + ";'>\n";
    for (var i = 0; i < yxMonths.length; i++) {
        s += "<option value='" + i + "'";
        if (i == yxCal.CM) {
            s += " selected";
        }
        s += ">";
        if (yxMonthFormat == "Month") {
            s += yxMonths[i];
        }
        else if (yxMonthFormat == "MONTH") {
            s += yxMonths[i].toUpperCase();
        }
        else if (yxMonthFormat == "Mon") {
            s += yxMonth3(i);
        }
        else if (yxMonthFormat == "MON") {
            s += yxMonth3(i).toUpperCase();
        }
        else {
            s += yxGetDD(i + 1);
        }
        s += "</option>\n";
    }
    s += "</select></td>"

    s += "<td align='right'><select onchange='" + yxList("yxScrollYear", idx + ", this.options[this.selectedIndex].value") + "' style='font-family:" + yxConf[5] + "; font-size:" + yxConf[6] + "px; background-color:" + yxListBG + ";'>\n";
    for (var i = yxCal.CY - yxYearRange; i <= yxCal.CY + yxYearRange; i++) {
        s += "<option value='" + i + "'";
        if (i == yxCal.CY) {
            s += " selected";
        }
        s += ">";
        if (i == yxCal.CY - yxYearRange) {
            s += "...";
        }
        s += i + "";
        if (i == yxCal.CY + yxYearRange) {
            s += "...";
        }
    }
    s += "</select></td>";

    s += "</tr></table></td></tr>";

    return s;
}
