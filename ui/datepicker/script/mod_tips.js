// Mod: Date Tips V1.0
// Core: Xin Calendar (In-Page Core) V1.0

// Author: Xin Yang
// Last Modified: 23-Mar-2003

// Default settings
var yxTipsTiming = 1000;
var yxDateConf = ["#cc0000", "#f0f0f0"];
var yxTipsConf = [yxConf[12], yxColors[16], yxConf[5], yxConf[6], yxColors[1], yxColors[0], yxColors[5], yxColors[4], "left", yxConf[11]];
var yxTipsLayout = [1, 0, 4, 0];

// User interface
function setTipsTiming(t) {
    yxTipsTiming = t;
}

function setDateStyle(date_color, date_bgcolor) {
    yxDateConf = [date_color, date_bgcolor];
}

function setTipsStyle(border_size, border_color, font, font_size, title_color, title_bgcolor, tip_color, tip_bgcolor, tip_align, padding) {
    yxTipsConf = [border_size, border_color, font, font_size, title_color, title_bgcolor, tip_color, tip_bgcolor, tip_align, padding];
}

function setTipsLayout(auto_hide, sticky, pos, width) {
    yxTipsLayout = [auto_hide, sticky, pos, width];
}

function addDateTips(name, date, tips) {
    var cal = yxFindCal(name);
    if (cal != null) {
        cal.ref.tips[date] = tips;
    }
}

// Internal functions
var yxTipsTimer = 0, yxTips = null

function yxFindTips(date) {
    if (typeof(yxCal.ref.tips[date]) != "undefined") {
        return yxCal.ref.tips[date];
    }
    else {
        return "";
    }
}

function yxGetWidth(l) {
    return yxIsN4 ? l.document.width : yxIsIE4 ? l.children[0].offsetWidth : l.offsetWidth;
}
function yxGetHeight(l) {
    return yxIsN4 ? l.document.height : yxIsIE4 ? l.children[0].offsetHeight : l.offsetHeight;
}

function yxPosTips() {
    var cw = yxGetWidth(yxCal.holder), ch = yxGetHeight(yxCal.holder), pw = yxConf[13] + yxConf[14];
    var tw = yxGetWidth(yxTips), th = yxGetHeight(yxTips);

    var h = yxIsN4 ? yxCal.holder : yxCal.holder.style, cx = parseInt(h.left), cy = parseInt(h.top);

    var x = yxTipsLayout[2] == 1 ? (cx + pw) : (yxTipsLayout[2] == 0 || yxTipsLayout[2] == 2) ? cx : yxTipsLayout[2] == 5 ? (cx - tw - 1) : yxTipsLayout[2] == 6 ? (cx + cw + 1) : (yxMX < Math.floor(tw / 2) + 2) ? 2 : (yxMX - Math.floor(tw / 2));
    var y = yxTipsLayout[2] == 1 ? (cy + pw) : yxTipsLayout[2] == 0 ? (cy - th - 1) : yxTipsLayout[2] == 2 ? (cy + ch + 1) : yxTipsLayout[2] == 3 ? (yxMY - th - 16) : yxTipsLayout[2] == 4 ? (yxMY + 16) : cy;

    yxMoveLayerTo(yxTips, x, y);
}

function yxPrepareTips(date, tips) {
    var cw = yxGetWidth(yxCal.holder), w = yxTipsLayout[3] != 0 ? yxTipsLayout[3] : yxTipsLayout[2] == 1 ? (cw - (yxConf[13] + yxConf[14]) * 2) : cw;

    var header = "";
    if (yxTipsLayout[0] == 0) {
        var span = "<span style='font-family:" + yxTipsConf[2] + "; font-size:" + yxTipsConf[3] + "px; color:" + yxTipsConf[4] + "'>";
        header = "<tr bgcolor='" + yxTipsConf[5] + "'><td align='center'><table width='" + (w - (yxTipsConf[0] + yxTipsConf[9]) * 2) + "' cellpadding='0' cellspacing='0' border='0'><tr><td align='left'>" + span + date + "</span></td><td align='right'>" + span + "[<a href='javascript:yxHideTips()' style='color:" + yxTipsConf[4] + "'>X</a>]</span></td></tr></table></td></tr>";
    }

    return "<table cellspacing='0' cellpadding='0' border='0' bgcolor='" + yxTipsConf[1] + "'><tr><td><table cellspacing='" + yxTipsConf[0] + "' cellpadding='" + yxTipsConf[9] + "' border='0' width='" + w + "'" + ">" + header + "<tr bgcolor='" + yxTipsConf[7] + "'><td align='" + yxTipsConf[8] + "' valign='top'><span style='font-family:" + yxTipsConf[2] + "; font-size:" + yxTipsConf[3] + "px; color:" + yxTipsConf[6] + "'>" + tips + "</span></td></tr></table></td></tr></table>";
}

function yxShowTips(idx, day) {
    yxCal = yxCals[idx];

    var date = yxGetDate(yxCal.CY, yxCal.CM, day), tips = yxFindTips(date);
    if (tips != "") {
        if (yxIsN4 && yxTips != null && yxTips.idx < yxN4L) {
            yxTips.visibility = "hide";
            with (yxTips.document) {
                open();
                write("");
                close();
            }
            yxTips = null;
        }

        if (yxTips == null) {
            yxTips = yxMakeLayer();

            if (yxIsN4) {
                yxTips.zIndex = yxIdx + 1;
            }
            else {
                yxTips.style.zIndex = yxIdx + 1
            }
        }

        yxWriteLayer(yxTips, yxPrepareTips(date, tips));
        if (yxIsIE) {
            yxTips.style.clip = "rect(0px; " + yxTips.children[0].offsetWidth + "px; " + yxTips.children[0].offsetHeight + "px; 0px)";
        }

        yxPosTips();

        yxShowLayer(yxTips);
    }
}

function yxHideTips() {
    if (yxTips != null) {
        yxHideLayer(yxTips);
    }
}

function yxOverIt(idx, day) {
    yxTipsTimer = setTimeout("yxShowTips(" + idx + "," + day + ")", yxTipsTiming);
}

function yxOutIt() {
    clearTimeout(yxTipsTimer);
    if (yxTipsLayout[0] == 1) {
        yxHideTips();
    }
}

// Mod functions
function yxMouseInOut(idx, day) {
    return " onMouseOver='yxOverIt(" + idx + "," + day + ")' onMouseOut='yxOutIt()'";
}

function yxCalCell(y, m, d, day) {
    var wd = (new Date(y, m, d)).getDay(), dateOff = yxDateOff(y, m, d), dd = yxGetDate(y, m, d);

    var bg = (d == day) ? yxColors[8] : (typeof(yxCal.ref.tips[dd]) != "undefined" && yxDateConf[1] != "") ? yxDateConf[1] : (wd == 0 || wd == 6) ? yxColors[6] : yxColors[4];
    var fg = (d == day) ? yxColors[9] : (typeof(yxCal.ref.tips[dd]) != "undefined" && yxDateConf[0] != "") ? yxDateConf[0] : (wd == 0 || wd == 6) ? yxColors[7] : yxColors[5];

    return _yxCalCell(d, bg, fg, dateOff);
}

function yxBeforeUpdate() {
    clearTimeout(yxTipsTimer);
    if (yxTipsLayout[1] == 0) {
        yxHideTips();
    }
}

function yxBeforeScroll() {
    clearTimeout(yxTipsTimer);
    if (yxTipsLayout[1] == 0) {
        yxHideTips();
    }
}

var _tips_addCalendar = addCalendar;
function yx_tips_addCalendar(name, field, form, date, id, id2, dx, dy) {
    _tips_addCalendar(name, field, form, date, id, id2, dx, dy);

    var cal = yxFindCal(name);
    cal.ref.tips = new Array();
}
addCalendar = yx_tips_addCalendar;
