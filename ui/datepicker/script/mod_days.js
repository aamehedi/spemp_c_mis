// Mod: Special Days V1.0
// Core: Xin Calendar (In-Page Core) V1.0

// Author: Xin Yang
// Last Modified: 24-Jun-2003

// User interface
function setLoopWeek(fcolor, bgcolor, start, step) {
    var dc = 0, arg = "";

    for (var i = 4; i < setLoopWeek.arguments.length; i++) {
        if (setLoopWeek.arguments[i].search(/(sun|mon|tue|wed|thu|fri|sat)/i) != -1) {
            arg = RegExp.$1.toLowerCase();
            for (var j = 0; j < yxDays3.length; j++) {
                if (yxDays3[j] == arg) {
                    dc = j;
                    break;
                }
            }

            for (wc = start - 1; wc < 6; wc += step) {
                yxForWeek[wc][dc] = new yxCellColors(fcolor, bgcolor);
                if (dc == 0) {
                    yxForWeek[wc][7] = new yxCellColors(fcolor, bgcolor);
                }
            }
        }
    }
}

function setLoopYear(fcolor, bgcolor, format) {
    var m, d, arg;
    var mIdx = format.search(/mm/i), m3Idx = format.search(/mon/i), dIdx = format.search(/dd/i);
    var df = new RegExp('^' + _yxFormat(format) + '$');

    for (var i = 3; i < setLoopYear.arguments.length; i++) {
        arg = setLoopYear.arguments[i];

        if (df.test(arg)) {
            if (mIdx != -1) {
                m = arg.substring(mIdx, mIdx + 2) - 1;
            }
            else {
                m = yxMonthFromName(arg.substring(m3Idx, m3Idx + 3));
            }
            d = arg.substring(dIdx, dIdx + 2) - 0;

            yxForYear[m][d] = new yxCellColors(fcolor, bgcolor);
        }
    }
}

// Internal function
var yxForWeek = new Array(), yxForYear = new Array();
var yxDays3 = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"];

function yxCellColors(fg, bg) {
    this.fg = fg;
    this.bg = bg;
}

for (i = 0; i < 6; i++) {
    yxForWeek[i] = new Array();
    for (j = 0; j <= 7; j++) {
        yxForWeek[i][j] = null;
    }
}
for (var i = 0; i < 12; i++) {
    yxForYear[i] = new Array();
    for (var j = 1; j <= 31; j++) {
        yxForYear[i][j] = null;
    }
}

// Mod functions
function yxCalCell(y, m, d, day) {
    var wn = 0, fd = (new Date(y, m, 1)).getDay(), wd = (new Date(y, m, d)).getDay(), dateOff = yxDateOff(y, m, d);

    if (yxConf[1] > 0 && fd == 0) {
        fd = 7;
    }
    for (var i = 8 + yxConf[1] - fd; i <= d; i += 7) {
        wn++;
    }

    var bg = (d == day) ? yxColors[8] : (yxForYear[m][d] != null) ? (yxForYear[m][d].bg) : (yxForWeek[wn][wd] != null) ? (yxForWeek[wn][wd].bg) : (wd == 0 || wd == 6) ? yxColors[6] : yxColors[4];
    var fg = (d == day) ? yxColors[9] : (yxForYear[m][d] != null) ? (yxForYear[m][d].fg) : (yxForWeek[wn][wd] != null) ? (yxForWeek[wn][wd].fg) : (wd == 0 || wd == 6) ? yxColors[7] : yxColors[5];

    return _yxCalCell(d, bg, fg, dateOff);
}
