// Xin Calendar (In-Page Core Lite) V1.1
// Copyright 2003  Xin Yang    All Rights Reserved.

// Last Modified: 12-Jul-2004
// Web Site: yxScripts.com
// Email: m_yangxin@hotmail.com

// default settings
var yxConf = ["dd/mm/yyyy", 0, 0, 1, 1, "verdana", 12, 120, 1, 22, 1, 1, 1, 1, 1];
var yxColors = ["#cccccc", "#000000", "#999999", "#ffffff", "#ffffff", "#000000", "#ffffff", "#000000", "#999999", "#ffffff", "#ffffff", "#999999", "#ffffff", "#cccccc", "#000000", "#ffffff", "#000000", "#ffffff"];
var yxMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var yxDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];
var yxLinks = ["[Clear]", "[Close]"];
// ------

// codes
var yxCore = 1;
var yxCals = new Array();
var yxCal = null, yxDateFormat = null, yxIdx = 100;
var yxMX = 0, yxMY = 0, yxN4F = 1, yxIMW = 2, yxN4L = 0, yxIE4C = 1;

var yxTL = " style='text-decoration:none; background-color:transparent'>";
var yxTable = "<table cellpadding='0' cellspacing='0' border='0'>";

var yxIsKon = (navigator.userAgent.indexOf("Konqueror") != -1);
var yxIsOpera = (navigator.userAgent.indexOf("Opera") != -1);
var yxIsMac = (navigator.userAgent.indexOf("Mac") != -1);

var yxIsIE = (navigator.userAgent.indexOf("MSIE") != -1 && !yxIsOpera);
var yxIsIE4 = (yxIsIE && navigator.appVersion.indexOf("MSIE 4") != -1);
var yxIsIE5 = (yxIsIE && !yxIsIE4);
var yxIsN6 = (navigator.userAgent.indexOf("Gecko") != -1);
var yxIsK3 = (yxIsKon && navigator.appVersion.indexOf('Konqueror/3') != -1);
var yxIsO7 = (navigator.userAgent.indexOf("Opera 7") != -1);
var yxIsN4 = (typeof(document.layers) != "undefined");

var yxCalSafe = (yxIsN6 || yxIsN4 || yxIsIE5 || yxIsIE4 && !yxIsMac || yxIsK3 || yxIsO7);

var yxLayers = null;
if (yxIsIE) {
    yxLayers = document.all;
}
else if (yxIsN4) {
    yxLayers = document.layers;
    yxConf[6] += yxN4F;
}
else if (yxIsN6 || yxIsK3 || yxIsO7) {
    yxLayers = yxDOM;
}

if (yxIsIE && yxIsMac) {
    yxConf[9] += yxIMW;
}

function yxWriteLayer(l, content) {
    if (yxIsN4) {
        with (l.document) {
            open();
            writeln(content);
            close();
        }
    }
    else {
        l.innerHTML = content;
    }
}
function yxMoveLayerTo(l, x, y) {
    if (yxIsN4) {
        l.moveTo(x, y);
    }
    else {
        l.style.top = y + "px";
        l.style.left = x + "px";
    }
}
function yxMoveLayerBy(l, x, y) {
    if (yxIsN4) {
        l.moveBy(x, y);
    }
    else {
        l.style.top = (parseInt(l.style.top) + y) + "px";
        l.style.left = (parseInt(l.style.left) + x) + "px";
    }
}
function yxShowLayer(l) {
    if (yxIsN4) {
        l.visibility = "show";
    }
    else {
        l.style.visibility = "visible";
    }
}
function yxHideLayer(l) {
    if (yxIsN4) {
        l.visibility = "hide";
    }
    else {
        l.style.visibility = "hidden";
    }
}

function yxMakeLayer() {
    var l = null;

    if (yxIsN4) {
        l = new Layer(1000);
        l.zIndex = yxIdx;
        l.idx = ++yxN4L;
    }
    else if (yxIsIE4) {
        var id = "yx" + (yxIE4C++) + "";
        document.body.insertAdjacentHTML("AfterBegin", ('<div id="' + id + '" style="position:absolute; left:0px; top:0px; visibility:hidden; z-index:' + yxIdx + ';"></div>'));
        l = document.all(id);
    }
    else {
        l = document.createElement("DIV");
        with (l.style) {
            position = "absolute";
            visibility = "hidden";
            left = "0px";
            top = "0px";
            zIndex = yxIdx;
        }
        if (yxIsIE && !yxIsMac) {
            document.body.insertBefore(l, document.body.firstChild);
        }
        else {
            document.body.appendChild(l);
        }
    }

    return l;
}

function yxDOM(id) {
    return document.getElementById(id);
}

function yxSpan(color, content) {
    return "<span style='font-family:" + yxConf[5] + "; font-size:" + yxConf[6] + "px; color:" + color + ";'>" + content + "</span>";
}

function yxCalOBJ(idx, name, field, form, date, id, id2, dx, dy) {
    this.idx = idx;
    this.name = name;
    this.field = field;
    this.formName = form;
    this.form = null;
    this.date = date;
    this.id = id;
    this.id2 = id2;
    this.dx = dx;
    this.dy = dy;

    this.CY = 0;
    this.CM = 0;
    this.mode = 1;
    this.holder = null;
    this.checkHolder = yxCheckHolder;

    this.ref = new Function;
}

function yxRegForm(cal) {
    if (cal.form == null) {
        if (cal.formName == "") {
            if (document.forms[0]) {
                cal.form = document.forms[0];
            }
        }
        else if (document.forms[cal.formName]) {
            cal.form = document.forms[cal.formName];
        }
    }
}

function yxFindCal(name) {
    for (var i = 0; i < yxCals.length; i++) {
        if (yxCals[i].name == name) {
            return yxCals[i];
        }
    }

    return null;
}

function yxDay3(y, m, d) {
    return yxDays[(new Date(y, m, d)).getDay()].substring(0, 3);
}
function yxMonth3(i) {
    return yxMonths[i].substring(0, 3);
}

function yxMonthFromName(m3) {
    for (var i = 0; i < yxMonths.length; i++) {
        if (yxMonth3(i).toLowerCase() == m3.toLowerCase()) {
            return i;
        }
    }
    return 0;
}

function _yxFormat() {
    var calF = yxConf[0];

    calF = calF.replace(/\//g, '\\\/');
    calF = calF.replace(/dd/gi, '\\d\\d');
    calF = calF.replace(/mm/gi, '\\d\\d');
    calF = calF.replace(/yyyy/gi, '\\d\\d\\d\\d');

    return calF;
}

function yxFormat(f) {
    if (yxDateFormat == null) {
        yxDateFormat = new RegExp('^' + _yxFormat() + '$');
    }
    return yxDateFormat;
}

function yxGetNumbers(date) {
    var y, m, d;

    var yIdx = yxConf[0].search(/yyyy/i);
    var mIdx = yxConf[0].search(/mm/i);
    var dIdx = yxConf[0].search(/dd/i);

    y = date.substring(yIdx, yIdx + 4) - 0;
    m = date.substring(mIdx, mIdx + 2) - 1;
    d = date.substring(dIdx, dIdx + 2) - 0;

    return new Array(y, m, d);
}

function yxGetDate(y, m, d) {
    var date = yxConf[0];
    date = date.replace(/yyyy/i, y);
    date = date.replace(/mm/i, yxGetDD(m + 1));
    date = date.replace(/dd/i, yxGetDD(d));

    return date;
}

function yxHideCal(idx) {
    if (yxCals[idx].mode == 1 && yxCals[idx].holder != null) {
        yxHideLayer(yxCals[idx].holder)
    }
}

function yxHideAll(idx) {
    for (var i = 0; i < yxCals.length; i++) {
        if (i != idx) {
            yxHideCal(i);
        }
    }
}

function yxLeft(l) {
    return l.offsetLeft + (l.offsetParent ? yxLeft(l.offsetParent) : (yxIsIE && yxIsMac) ? parseInt(document.body.leftMargin) : 0);
}
function yxTop(l) {
    return l.offsetTop + (l.offsetParent ? yxTop(l.offsetParent) : (yxIsIE && yxIsMac) ? parseInt(document.body.topMargin) : 0);
}
function yxLeftN4(l) {
    return l.pageX;
}
function yxTopN4(l) {
    return l.pageY;
}

function yxLastDay(d) {
    var yy = d.getFullYear(), mm = d.getMonth();
    for (var i = 31; i >= 28; i--) {
        var nd = new Date(yy, mm, i);
        if (mm == nd.getMonth()) {
            return i;
        }
    }
    return 31;
}

function yxFirstDay(d) {
    return (new Date(d.getFullYear(), d.getMonth(), 1)).getDay();
}

function yxDayDisplay(i) {
    if (yxConf[10] == 0) {
        return yxDays[i];
    }
    else {
        return yxDays[i].substring(0, yxConf[10]);
    }
}

function yxCalOpen() {
    return "";
}

function yxCalTableOpen() {
    return "<table cellspacing='" + yxConf[13] + "' cellpadding='" + yxConf[14] + "' border='0' bgcolor='" + yxColors[16] + "'><tr bgcolor='" + yxColors[17] + "'><td><table cellspacing='" + yxConf[12] + "' cellpadding='" + yxConf[11] + "' border='0' bgcolor='" + yxColors[15] + "'>";
}

function yxCalTitle() {
    var yy = yxCal.CY, mm = yxMonths[yxCal.CM], idx = yxCal.idx;
    var s;

    if (yxConf[8] == 2) {
        s = "<tr bgcolor='" + yxColors[0] + "'><td colspan='7' align='center'>" + yxTable + "<tr align='center' valign='middle'><td width='" + yxConf[7] + "'>" + yxSpan(yxColors[1], mm) + "</td></tr><tr align='center' valign='middle'><td width='" + yxConf[7] + "'>" + yxSpan(yxColors[1], yy) + "</td></tr></table></td></tr>";
    }
    else {
        s = "<tr bgcolor='" + yxColors[0] + "'><td colspan='7' align='center'>" + yxTable + "<tr align='center' valign='middle'><td width='" + yxConf[7] + "'><nobr>" + yxSpan(yxColors[1], mm + " " + yy) + "</nobr></td></tr></table></td></tr>";
    }

    return s;
}

function yxCalWeekdays() {
    var s = "<tr align='center' bgcolor='" + yxColors[2] + "'>";
    for (var i = yxConf[1]; i < yxConf[1] + 7; i++) {
        s += "<td width='" + yxConf[9] + "'>" + yxSpan(yxColors[3], yxDayDisplay(i)) + "</td>";
    }
    s += "</tr>";

    return s;
}

function yxCalHeader() {
    return yxCalOpen() + yxCalTableOpen() + yxCalTitle() + yxCalWeekdays();
}

function yxMouseInOut(idx, day) {
    return "";
}

function yxDateOff(y, m, d) {
    return false;
}

function yxCalCell(y, m, d, day) {
    var wd = (new Date(y, m, d)).getDay(), dateOff = yxDateOff(y, m, d);

    var bg = (d == day) ? yxColors[8] : (wd == 0 || wd == 6) ? yxColors[6] : yxColors[4];
    var fg = (d == day) ? yxColors[9] : (wd == 0 || wd == 6) ? yxColors[7] : yxColors[5];

    return _yxCalCell(d, bg, fg, dateOff);
}

function _yxCalCell(d, bg, fg, dateOff) {
    if (dateOff) {
        return "<td bgcolor='" + yxColors[10] + "'>" + yxSpan(yxColors[11], d) + "</td>";
    }
    else {
        return "<td bgcolor='" + bg + "'><a href='javascript:yxPickDate(" + yxCal.idx + "," + d + ")'" + yxMouseInOut(yxCal.idx, d) + yxTL + yxSpan(fg, d) + "</a></td>";
    }
}

function yxCalBody(d, day) {
    var s = "", dc = 1, fd = yxFirstDay(d), ld = yxLastDay(d);

    if (yxConf[1] > 0 && fd == 0) {
        fd = 7;
    }

    for (var i = 0; i < 6; i++) {
        s += "<tr align='center' bgcolor='" + yxColors[12] + "'>";
        for (var j = yxConf[1]; j < yxConf[1] + 7; j++) {
            if (i * 7 + j < fd || dc > ld) {
                s += "<td>" + yxSpan(yxColors[12], "&nbsp;") + "</td>";
            }
            else {
                s += yxCalCell(yxCal.CY, yxCal.CM, dc++, day);
            }
        }
        s += "</tr>";
    }

    return s;
}

function yxCalFootBar() {
    var idx = yxCal.idx;
    return yxConf[3] == 1 ? ("<tr bgcolor='" + yxColors[13] + "'><td colspan='7' align='center'>" + yxTable + "<tr><td><a href='javascript:yxClearDate(" + idx + ")'" + yxTL + yxSpan(yxColors[14], yxLinks[0]) + "</a></td><td>" + yxSpan(yxColors[14], "&nbsp;&nbsp;") + "</td><td><a href='javascript:yxHideCal(" + idx + ")'" + yxTL + yxSpan(yxColors[14], yxLinks[1]) + "</a></td></tr></table></td></tr>") : "";
}

function yxCalTableEnd() {
    return "</table></td></tr></table>";
}

function yxCalEnd() {
    return "";
}

function yxCalFooter() {
    return yxCalFootBar() + yxCalTableEnd() + yxCalEnd();
}

function yxCheckRange(dir) {
}

function yxMoveYear() {
}
function yxMoveMonth() {
}

function yxCheckHolder() {
    if (yxIsN4 && this.holder != null && this.holder.idx < yxN4L) {
        this.holder.visibility = "hide";
        with (this.holder.document) {
            open();
            write("");
            close();
        }
        this.holder = null;
    }

    if (this.holder == null) {
        this.holder = yxMakeLayer();
    }
}

function yxBeforeShow() {
    yxHideAll(yxCal.idx);
}
function yxAfterShow() {
}

function yxGetForm() {
    return yxCal.form[yxCal.field].value;
}

function yxShowCal(name) {
    if (!yxCalSafe) {
        return;
    }

    yxCal = yxFindCal(name);

    if (yxCal != null) {
        yxRegForm(yxCal);

        if (yxCal.form && yxCal.form[yxCal.field]) {
            yxCal.checkHolder();

            var cx = yxCal.dx, cy = yxCal.dy;
            if (yxCal.id == "" && yxCal.id2 == "") {
                cx += yxMX;
                cy += yxMY;
            }
            else if (yxIsIE) {
                var l = yxLayers[yxCal.id];
                cx += yxLeft(l);
                cy += yxTop(l);
            }
            else if (yxIsN4) {
                var l = yxLayers[(yxCal.id2 != "") ? (yxCal.id2) : (yxCal.id)];
                cx += yxLeftN4(l);
                cy += yxTopN4(l);
            }
            else {
                var l = yxLayers((yxIsK3 && yxCal.id2 != "") ? (yxCal.id2) : (yxCal.id));
                cx += yxLeft(l);
                cy += yxTop(l);
            }

            var calRE = yxFormat(), calV = yxGetForm(), d = new Date(), dd = 0;
            if (calV != "" && calRE.test(calV) || yxCal.date != "" && calRE.test(yxCal.date)) {
                var cd = yxGetNumbers((calV != "" && calRE.test(calV)) ? calV : yxCal.date);
                d = new Date(cd[0], cd[1], cd[2]);
                dd = d.getDate();
            }

            yxCal.CY = d.getFullYear();
            yxCal.CM = d.getMonth();

            yxCheckRange(0);
            if (yxCal.CY != d.getFullYear() || yxCal.CM != d.getMonth()) {
                d = new Date(yxCal.CY, yxCal.CM, 1);
                dd = 0;
            }

            yxBeforeShow();

            var h = yxCal.holder;
            yxMoveLayerTo(h, cx, cy);
            yxWriteLayer(h, yxCalHeader() + yxCalBody(d, dd) + yxCalFooter());

            if (yxIsIE) {
                h.style.clip = "rect(0px; " + h.childNodes[0].offsetWidth + "px; " + h.childNodes[0].offsetHeight + "px; 0px)";
            }

            yxShowLayer(h);

            yxAfterShow();
        }
        else if (!yxCal.form) {
            window.status = "Form [" + yxCal.formName + "] not found.";
        }
        else if (!yxCal.form[yxCal.field]) {
            window.status = "Form Field [" + yxCal.formName + "." + yxCal.field + "] not found.";
        }
    }
    else {
        window.status = "Calendar [" + name + "] not found.";
    }
};
var showCalendar = yxShowCal;

function yxGetDD(n) {
    return ((n < 10) ? "0" : "") + n;
}

function yxClearDate(idx) {
    yxCal = yxCals[idx];

    yxBeforeUpdate("");
    yxUpdateForm("");
    yxAfterUpdate("");

    yxHideCal(idx);
}

function yxBeforeUpdate(date) {
}
function yxAfterUpdate(date) {
}

function yxUpdateForm(date) {
    yxCal.form[yxCal.field].value = date;
}

function yxPickDate(idx, day) {
    yxCal = yxCals[idx];

    var date = yxGetDate(yxCal.CY, yxCal.CM, day);
    yxBeforeUpdate(date);
    yxUpdateForm(date);
    yxAfterUpdate(date);

    yxHideCal(idx);
}

function yxTrackIt(e) {
    if (yxIsIE || yxIsK3 || yxIsOpera) {
        yxMX = event.clientX + (yxIsK3 ? 0 : window.document.body.scrollLeft);
        yxMY = event.clientY + (yxIsK3 ? 0 : window.document.body.scrollTop);
        event.cancelBubble = false;
    }
    else {
        yxMX = e.pageX;
        yxMY = e.pageY;
        document.routeEvent(e);
    }
}
// ------

// user functions
function _addCalendar(name, field, form, date, id, id2, dx, dy) {
    yxCals[yxCals.length] = new yxCalOBJ(yxCals.length, name, field, form, date, id, id2, dx, dy);
};
var addCalendar = _addCalendar;

function setFont(font, size) {
    if (font != "") {
        yxConf[5] = font;
    }
    if (size > 0) {
        yxConf[6] = size;
        if (yxIsN4) {
            yxConf[6] += yxN4F;
        }
    }
}

function setWidth(tWidth, tMode, dWidth, dDigits, cPadding, iBorder, oBorder, bSpacing) {
    if (tWidth > 0) {
        yxConf[7] = tWidth;
    }
    if (tMode == 1 || tMode == 2) {
        yxConf[8] = tMode;
    }
    if (dWidth > 0) {
        yxConf[9] = dWidth;
        if (yxIsIE && yxIsMac) {
            yxConf[9] += yxIMW;
        }
    }
    if (dDigits >= 0) {
        yxConf[10] = dDigits;
    }

    yxConf[11] = cPadding;
    yxConf[12] = iBorder;
    yxConf[13] = oBorder;
    yxConf[14] = bSpacing;
}

function setColor(tColor, wColor, dColor, weColor, cdColor, odColor, bColor, fColor, ibColor, obColor, bsColor) {
    if (tColor != "") {
        yxColors[0] = tColor;
    }
    if (wColor != "") {
        yxColors[2] = wColor;
    }
    if (dColor != "") {
        yxColors[4] = dColor;
    }
    if (weColor != "") {
        yxColors[6] = weColor;
    }
    if (cdColor != "") {
        yxColors[8] = cdColor;
    }
    if (odColor != "") {
        yxColors[10] = odColor;
    }
    if (bColor != "") {
        yxColors[12] = bColor;
    }
    if (fColor != "") {
        yxColors[13] = fColor;
    }
    if (ibColor != "") {
        yxColors[15] = ibColor;
    }
    if (obColor != "") {
        yxColors[16] = obColor;
    }
    if (bsColor != "") {
        yxColors[17] = bsColor;
    }
}

function setFontColor(tColorF, wColorF, dColorF, weColorF, cdColorF, odColorF, fColorF) {
    if (tColorF != "") {
        yxColors[1] = tColorF;
    }
    if (wColorF != "") {
        yxColors[3] = wColorF;
    }
    if (dColorF != "") {
        yxColors[5] = dColorF;
    }
    if (weColorF != "") {
        yxColors[7] = weColorF;
    }
    if (cdColorF != "") {
        yxColors[9] = cdColorF;
    }
    if (odColorF != "") {
        yxColors[11] = odColorF;
    }
    if (fColorF != "") {
        yxColors[14] = fColorF;
    }
}

function setFormat() {
}

function setWeekDay(wd) {
    if (wd == 0 || wd == 1) {
        yxConf[1] = wd;
    }
}

function setMonthTitles(jan, feb, mar, apr, may, jun, jul, aug, sep, oct, nov, dec) {
    if (jan != "") {
        yxMonths[0] = jan;
    }
    if (feb != "") {
        yxMonths[1] = feb;
    }
    if (mar != "") {
        yxMonths[2] = mar;
    }
    if (apr != "") {
        yxMonths[3] = apr;
    }
    if (may != "") {
        yxMonths[4] = may;
    }
    if (jun != "") {
        yxMonths[5] = jun;
    }
    if (jul != "") {
        yxMonths[6] = jul;
    }
    if (aug != "") {
        yxMonths[7] = aug;
    }
    if (sep != "") {
        yxMonths[8] = sep;
    }
    if (oct != "") {
        yxMonths[9] = oct;
    }
    if (nov != "") {
        yxMonths[10] = nov;
    }
    if (dec != "") {
        yxMonths[11] = dec;
    }
}

function setDayTitles(sun, mon, tue, wed, thu, fri, sat) {
    if (sun != "") {
        yxDays[0] = sun;
        yxDays[7] = sun;
    }
    if (mon != "") {
        yxDays[1] = mon;
    }
    if (tue != "") {
        yxDays[2] = tue;
    }
    if (wed != "") {
        yxDays[3] = wed;
    }
    if (thu != "") {
        yxDays[4] = thu;
    }
    if (fri != "") {
        yxDays[5] = fri;
    }
    if (sat != "") {
        yxDays[6] = sat;
    }
}

function setLinkTitles(clearL, closeL) {
    if (clearL != "") {
        yxLinks[0] = clearL;
    }
    if (closeL != "") {
        yxLinks[1] = closeL;
    }
}

function switchLinks(mode) {
    if (mode == 0 || mode == 1) {
        yxConf[3] = mode;
    }
}

function setCalendarMode() {
}
// ------

if (yxCalSafe) {
    if (yxIsN6 || yxIsN4) {
        document.captureEvents(Event.MOUSEMOVE);
    }
    document.onmousemove = yxTrackIt;
}

