<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("others_witness_popup.php?witness_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:730px;dialogHeight:400px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
        else {

        }
    }
</script>

<?php
require_once('../model/others_witness_info.php');
?>
<?php
$xml = simplexml_load_file("xml/others_witness_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $id = $information->id;
    }
}

$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $parliament = $information->parliament;
    }
}

?>


<div id="head_info">
    <?php echo $heading; ?>
</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $user_name;
    $committe_id;
    $committe_name;
    $param = array($id, $parliament);
    echo $others_witness_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>