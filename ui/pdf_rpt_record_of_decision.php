<?php
session_start();

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('MYPDF.php');

require_once('../model/record_decisions_info.php');
$user_id = $_SESSION['user_id'];
//echo $_GET['meeting_notice_id'];
if ($_GET['meeting_notice_id'] != NULL) {
    $result = $record_decisions_info->rpt_record_of_decision_getone(array($_GET['meeting_notice_id']));
}


// var_dump($_SESSION);
// var_dump($result);
// die();

$confData = array(
    'pageTitle' => 'Record of Decision',
    'committeeName' => $_SESSION['committee_name'],
    'parliamentName' => 'Parliament of Bangladesh',
    'reportName' => 'RECORD OF DECISION',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => true
);

// var_dump($confData);
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);
$pdf->SetHeaderMargin(40);
$pdf->setCellHeightRatio(1);

$pdf->AddPage();

if ($result->is_issued === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// var_dump($pdf->fontName);
// $pdf->SetFont('times', '', 12);
// $html = '<div style="margin-top:10px;">';
// $html .= '<b>Introduction</b><br>'.htmlspecialchars_decode($result->introduction).'<br>';
// $html .= '<b>Key Issues</b><br>'.htmlspecialchars_decode($result->key_issue).'<br>'; 
// $html .= '<b>Possible Questions for Specialist Advisors</b><br>'.htmlspecialchars_decode($result->advisor_question).'<br>'; 
// $html .= '<b>Possible Questions for Witnesses</b><br>'.htmlspecialchars_decode($result->witness_question).'<br>'; 
// $html .= '<b>Written Evidence</b><br>'.htmlspecialchars_decode($result->written_evidence).'<br>'; 
// $html .= '</div>';

// $pdf->SetFont('cambriab');
// $html = '<b>Introduction</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '     '.htmlspecialchars_decode($result->introduction).'<br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Key Issues</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->key_issue).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Possible Questions for Specialist Advisors</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->advisor_question).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Possible Questions for Witnesses</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->witness_question).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Written Evidence</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->written_evidence).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

$pdf->SetFont('cambria', '', 12);

$html = '<b>Date: </b>' . htmlspecialchars_decode($result->en_date) . ' and ' . htmlspecialchars_decode($result->bn_date) . '<br>';
$html .= '<b>Time: </b>' . htmlspecialchars_decode($result->time) . '<br>';
$html .= '<b>Location: </b>' . htmlspecialchars_decode($result->venue_name) . '<br>';
$html .= '<b>Chair: </b>' . htmlspecialchars_decode($result->chairman_name) . '<br>';
$html .= '<b>Members Present: </b>' . htmlspecialchars_decode($result->member_name) . '<br>';
$html .= '<b>Officials Presents: </b>' . htmlspecialchars_decode($result->logistic_member_name) . '<br>';
$html .= '<b>Witnessess: </b>' . htmlspecialchars_decode($result->witness_member_name) . '<br><br>';
$html .= '<b>PRIVATE BUSINESS</b><br>' . htmlspecialchars_decode($result->private_business) . '<br>';
$html .= '<b>PUBLIC BUSINESS</b><br>' . htmlspecialchars_decode($result->public_business) . '<br>';
$html .= '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($result->result_of_deliberation) . '<br>';


$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


// $pdf->MultiCell(0, 10, 'This is my text', false, 'C', false, 1);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
if ($result->is_issued === '0') {
    $pdf->Output('pdf_rpt_record_of_decision_' . $_GET['meeting_notice_id'] . '.pdf', 'I');
} else {
    $pdf->Output('issued_approved_pdf/pdf_rpt_record_of_decision_' . $_GET['meeting_notice_id'] . '.pdf', 'F');
    echo "<script>window.close();</script>";
}
//============================================================+
// END OF FILE
//============================================================+
