<?php
session_start();
?>
<?php
require_once('../model/officer_committee_tag.php');
$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}
?>
<?php
$officer_id = $_GET['officer_id'];
$committee_id = $_GET['committee_id'];
$type = $_GET['type'];
$user_id = $_SESSION['user_id'];
//echo $officer_id;
$officer_committee_tag->build_param($officer_id, $committee_id, $type, $user_id);
$language_id = $_SESSION['language_id'];
$committee_id = $_SESSION['committee_id'];
//echo $committee_id;
$user_id = $_SESSION['user_id'];
$param = array($id, $parliament);
echo $officer_committee_tag->gridview($committee_id, $language_id, $user_id, $param);
?>