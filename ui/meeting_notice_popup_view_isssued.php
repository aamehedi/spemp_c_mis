<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/meeting_notice_info.php');
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <?php

    //error_reporting(0);
    if (isset($_SESSION["language_id"]) == 1) {
        echo "<link href='Style/home.css' rel='stylesheet' type='text/css' />";
        echo "<link type='text/css' href='css/grid.css' rel='stylesheet' media='screen' />";
    } else {
        echo "<link href='Style/bnhome.css' rel='stylesheet' type='text/css' />";
        echo "<link type='text/css' href='css/bngrid.css' rel='stylesheet' media='screen' />";
    }
    ?>
    <link type='text/css' href='css/demo.css' rel='stylesheet' media='screen'/>
    <link type='text/css' href='css/menu.css' rel='stylesheet' media='screen'/>
    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">
    <script src="codebase_m/dhtmlxcalendar.js"></script>
    <style>
        #calendar,
        #calendar2,
        #calendar3,
        #calendar4 {
            border: 1px solid #909090;
            font-family: Tahoma;
            font-size: 12px;
        }
    </style>
    <script>
        var myCalendar;
        function doOnLoad() {
            myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);
        }
        function viewnoticeid(p) {

            window.showModalDialog("rpt_meeting_notice.php?meeting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
    </script>
    <script language="javascript" type="text/javascript" src="datepopup/datetimepicker.js">

        //Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
        //Script featured on JavaScript Kit (http://www.javascriptkit.com)
        //For this script, visit http://www.javascriptkit.com
    </script>
    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <!-- datebangla-->
    <script>
        function myFunction(str) {

            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "bndate.php?date=" + str, true);
            xmlhttp.send();
        }
    </script>

    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;

        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
</head>
<body onLoad="doOnLoad();">
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<?php
$xml = simplexml_load_file("xml/meeting_notice_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
    }
}
?>
<?php
$user_id = $_SESSION['user_id'];
$metting_notice_id = $_GET['metting_notice_id'];
//$language_id=$_SESSION['language_id'];
//$user_id=$_SESSION['user_id'];
$id = 1;
$committee_name = 1;
$short_name = 1;
$param = array($id, $committee_name, $short_name);
echo $meeting_notice_info->grid_issued_view($metting_notice_id, $user_id, $param);
?>
</body>
</html>
