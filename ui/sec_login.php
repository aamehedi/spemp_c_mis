<?php
//ob_start();
session_start();
require_once("../model/user.php");

$msg = '';
$username = '';
$password = '';
$ispost = 0;
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if (isset($_POST['login'])) {
        $ispost = 1;
        if (empty($_POST["username"])) {
            $msg = 'Username is required!<br/>';
            $username = $_POST["username"];
        } else {
            $username = $_POST["username"];
        }
        if (empty($_POST["password"])) {
            $msg = $msg . 'Password is required!<br/>';
            $password = $_POST["password"];
        } else {
            $password = $_POST["password"];
        }

        if ($msg == '') {
            $var = $user->checkuser(array("'" . $username . "'", "'" . $password . "'"));

            if ($var == true) {
                if (!isset($_SESSION['committee_id'])) {
                    require_once('../model/user_committee_permission.php');
                    // var_dump($_SESSION['language_id']);
                    $count_committee = $user_committee_permission->get_user_committee_count($_SESSION['user_id'], $_SESSION['language_id']);

                    if ($count_committee == 1) {
                        $result = $user_committee_permission->get_user_committee($_SESSION['user_id'], $_SESSION['language_id']);
                        $result = mysqli_fetch_object($result);

                        $_SESSION['committee_id'] = $result->committee_id;
                        $_SESSION['committee_name'] = $result->committee_name;
                    } else {
                        header('Location: committee_set.php');
                        exit;
                    }
                }
                header("location: index.php?language_id=2");
            } else
                $msg = 'Username and/or Password is not valid!!';
        }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>MIS for Financial Oversight Committees</title>
    <style type="text/css">
        body {
            font-size: 12px;
            background: #ffffff;
            text-align: left;
        }

        #login {
            margin: 5em auto;
            margin-top: 15%;
            background: #ffffff;
            border: 2px solid #61399D;
            width: 450px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -o-border-radius: 5px;
            border-radius: 5px;
            -moz-box-shadow: 0 0 10px #61399D;
            -webkit-box-shadow: 0 0 10px #61399D;
            box-shadow: 0 0 10px #61399D;
            text-align: left;
            position: relative;
        }

        #login h1 {
            background: #61399D;
            color: #fff;
            font-size: 16px;
            padding: 10px 20px;
            margin: 0 0 1.5em 0;
        }

        #login div {
            margin: .2em 10px;
            background: #ffffff;
            padding: 2px;
            text-align: left;
            position: relative;
        }

        .label {
            float: left;
            line-height: 30px;
            padding-left: 10px;
            font-size: 14px;
            letter-spacing: 0px;
            width: 20%;
        }

        #login .field {
            border: 1px solid #61399D;
            width: 70%;
            font-size: 12px;
            line-height: 1em;
            padding: 4px;
        }

        .failureNotification {
            font-size: 1.2em;
            color: Red;
            text-align: left;
        }

        #login input[type="submit"] {
            border: solid 1px #ccc;
            padding: 0 30px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            font-size: 12px;
            color: #fff;
            background: #61399D;
            cursor: pointer;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            -o-border-radius: 8px;
            border-radius: 8px;
        }

        legend {
            /*border:2px solid gray;
            background:#61399D;
             -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -o-border-radius: 5px;
            border-radius: 5px;*/
            font-size: 12px;
            color: #61399D;
            font-family: Arial;
            font-weight: bold;
            padding: 5px;
        }

        fieldset {
            border: 1px solid #61399D;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            -o-border-radius: 5px;
            border-radius: 5px;
        }
    </style>
</head>
<body onload="document.login.username.focus();">
<form id="login" name="login" action="<?php $_SERVER['PHP_SELF']; ?>" method="post">
    <h1 style="text-align: left;">MIS for Financial Oversight Committees</h1>

    <div>
        <fieldset>
            <legend>Login</legend>
            <span class="failureNotification"><?php echo $msg; ?></span>

            <p style="background-color: #eeeeee; padding-top: 4px; padding-bottom: 4px;">
                <label for="username" class="label">Username:&nbsp;&nbsp;</label>
                <input name="username" id="username" type="text" placeholder="Username"
                       class="field" value="<?php echo $username ?>"/>
                <span class="failureNotification"><?php if ($username == '' && $ispost == 1) echo '*'; ?></span>
            </p>

            <p style="background-color: #eeeeee; padding-top: 4px; padding-bottom: 4px;">
                <label for="password" class="label">Password:&nbsp;&nbsp;</label>
                <input name="password" id="password" type="password" placeholder="Password" class="field"
                       value="<?php echo $password ?>"/>
                <span class="failureNotification"><?php if ($password == '' && $ispost == 1) echo '*'; ?></span>
            </p>
        </fieldset>
        <p class="submitButton" style="text-align: right;">
            <input name="login" type="submit" value="Login"/>
        </p>
    </div>
</form>
</body>
</html>