<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<?php
require_once('../model/media_info.php');
?>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("media_popup.php?media_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:480px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "ministry_delete.php?ministry_id=" + p;
        }
        else {

        }
    }
</script>
<?php
$xml = simplexml_load_file("xml/media_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $ministry_name = $information->ministry_name;
        $ministry_type = $information->ministry_type;
        $contact_person = $information->contact_person;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
        $comments = $information->comments;

    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($ministry_name, $contact_person, $designation);
    echo $media_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>