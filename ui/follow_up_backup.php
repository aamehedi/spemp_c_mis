<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>


<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>

<script type='text/javascript'>
    function popupwinid(p, q) {
        //$('<div></div>').load('follow_up_popup.php?id='+p).modal();
        window.showModalDialog("follow_up_popup.php?inquiry_report_id=" + p + "&inquiry_report_followup_id=" + q, "", "dialogTop:center;dialogLeft:center;center:yes;dialogWidth:1500px;dialogHeight:650px;")
        return false;
    }

    function printid(p) {
        //$('<div></div>').load('follow_up_popup.php?id='+p).modal();
        window.showModalDialog("rpt_follow_up.php?inquiry_report_followup_id=" + p, "", "dialogTop:center;dialogLeft:center;center:yes;dialogWidth:1500px;dialogHeight:650px;")
        return false;
    }
</script>
<?php
require_once('../model/follow_up_info.php');
?>
<?php
$xml = simplexml_load_file("xml/follow_up_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $report_no = $information->report_no;
        $report_date = $information->report_date;
        $title = $information->title;
        $parliament = $information->parliament;
        $session = $information->session;
        $executive_summary = $information->executive_summary;
        $introduction = $information->introduction;
        $issues = $information->issues;
        $recommendation = $information->recommendation;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $inquiry_no_ = $inquiry_no;
        $report_no_ = $report_no;
        $fo = $information->fo;
    }
}
?>
<div id="head_info">
</div>
<div style="background-color:#61399D;color:#FFFFFF;">
    <b><?php echo $heading; ?></b>

    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php
    if (isset($_GET['report_date_start']) || isset($_GET['report_date_end']) || isset($_GET['report_no']) || isset($_GET['inquiry_no']) || isset($_GET['parliament_id'])) {
        echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
    } else {
        echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
    }
    ?>
    <fieldset>
        <div style='margin:3px;'>
            <div>
                <div style='float:left;width:50%'>
                    <div style='float:left;width:25%;'><?php echo $report_date; ?></div>
                    <div style='float:left;width:30%'><input type="text" style='width:90%' id="calendar"
                                                             name="report_date_start"/></div>
                    <div style='float:left;width:10%;'><?php echo $to; ?></div>
                    <div style='float:left;width:30%'><input type="text" style='width:90%' id="calendar2"
                                                             name="report_date_end"/></div>
                </div>
                <div style='float:left;width:50%'>
                    <div style='float:left;width:25%;'><?php echo $report_no; ?></div>
                    <div style='float:left;width:38%'><input type="text" style='width:90%' name="report_no"/></div>
                    <input type="submit" value="Search" name="search_button" class="newbutton"/>
                </div>
            </div>
            <div>
                <div style='float:left;width:50%'>
                    <div style='float:left;width:25%;'><?php echo $parliament; ?></div>
                    <div style='float:left;width:75%'><?php $language_id = $_SESSION['language_id'];
                        $user_id = $_SESSION['user_id'];
                        $committee_id = $_SESSION['committee_id'];
                        $parliament_id = 0;
                        echo $follow_up_info->comboviewparliament($parliament_id, $language_id, $committee_id, $user_id);
                        ?></div>
                </div>
                <div style='float:left;width:50%'>
                    <div style='float:left;width:25%;'><?php echo $inquiry_no; ?></div>
                    <div style='float:left;width:38%'><input type="text" style='width:90%' name="inquiry_no"/></div>
                    <!--<input type="button"  name="basic" value="Add New" onclick="popupwinid(1,1);" class="newbutton"/> -->
                </div>
            </div>
            <div style='clear:both'></div>
        </div>
    </fieldset>
    </div>
</form>
<?php
if (isset($_GET['report_date_start'])) {
    $report_date_start = $_GET['report_date_start'];
}
if (isset($_GET['report_date_end'])) {
    $report_date_end = $_GET['report_date_end'];
}
if (isset($_GET['report_no'])) {
    $report_no = $_GET['report_no'];
}
if (isset($_GET['inquiry_no'])) {
    $inquiry_no = $_GET['inquiry_no'];
}
if (isset($_GET['parliament_id'])) {
    $parliament_id = $_GET['parliament_id'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
?>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($report_no_, $report_date, $inquiry_no_, $fo);
    if (isset($_GET['report_date_start']) || isset($_GET['report_date_end']) || isset($_GET['report_no']) || isset($_GET['inquiry_no']) || isset($_GET['parliament_id'])) {

        echo $follow_up_info->searchgridview($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param);
    } else {
        echo $follow_up_info->searchgridview(' ', ' ', ' ', ' ', ' ', $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>