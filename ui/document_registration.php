<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>

<link rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css" type="text/css">
<!-- Styes for search box -->
<style type="text/css">
    .slider {
        display: none;
        cursor: move;
    }

    .collapseSlider {
        display: none;
        cursor: move;
    }

    .sliderExpanded .collapseSlider {
        display: block;
        cursor: move;
    }

    .sliderExpanded .expandSlider {
        display: none;
        cursor: move;
    }
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });

    $(document).ready(function () {
        $("#doc_reg_date_start").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
        $("#doc_reg_date_end").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
    });
    function calculateWeekend(date) {
        var weekend = date.getDay() == 5 || date.getDay() == 6;
        return [true, weekend ? 'myweekend' : ''];
    }
    ;
</script>

<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("document_registration_popup.php?doc_reg_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
    }
</script>
<?php
require_once('../model/document_registration_info.php');
?>
<?php
$xml = (array)simplexml_load_file("xml/document_registration_master_detail_popup.xml");
$information = $xml['information'][$_SESSION['language_id'] - 1];
$heading = $information->heading;
$headingpopup = $information->headingpopup;
$doc_reg_no = $information->doc_reg_no;
$id = $information->id;
$doc_reg_date_start = $information->doc_reg_date_start;
$doc_title = $information->doc_title;
$designation = $information->designation;
$doc_name = $information->doc_name;
$to = $information->to;
$ref_no = $information->ref_no;
$ref_date = $information->ref_date;

?>

<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<form id="overlay_form" name="overlay_form" method="get" action="#" onsubmit="return(validate());">
    <?php if (isset($_GET['doc_reg_no']) || isset($_GET['doc_reg_date_start']) || isset($_GET['doc_reg_date_end']) || isset($_GET['doc_title'])) : ?>
    <div style="margin-top:1px; width:100%;" id="slideOne">
        <?php else : ?>
        <div style="margin-top:1px; width:100%;" class="slider" id="slideOne">
            <?php endif; ?>
            <fieldset>
                <legend>Search Criteria</legend>
                <div>
                    <div>
                        <div style='float:left;width:22.5%;'>
                            <?php echo $doc_reg_no; ?>
                        </div>
                        <div style='float:left;width:25%;'>
                            <input type="text" style='width:90%' name="doc_reg_no"
                                   value="<?php echo isset($_GET['doc_reg_no']) ? $_GET['doc_reg_no'] : ''; ?>"/>
                        </div>
                        <div style='float:left;width:22.5%;'>
                            <?php echo $doc_title; ?>
                        </div>
                        <div style='float:left;width:25%;'>
                            <input type="text" style='width:90%' name="doc_title"
                                   value="<?php echo isset($_GET['doc_title']) ? $_GET['doc_title'] : ''; ?>"/>
                        </div>

                        <div style='clear:both'></div>
                    </div>

                    <div>
                        <div style='float:left;width:22.5%;'>
                            <?php echo $doc_reg_date_start; ?>
                        </div>
                        <div style='float:left;width:25%;'>
                            <input type="text" style='width:90%' id="doc_reg_date_start" name="doc_reg_date_start"
                                   value="<?php echo isset($_GET['doc_reg_date_start']) ? $_GET['doc_reg_date_start'] : ''; ?>"/>
                        </div>
                        <div style='float:left;width:22.5%;'>
                            <?php echo $to; ?>
                        </div>
                        <div style='float:left;width:25%;'>
                            <input type="text" style='width:90%' id="doc_reg_date_end" name="doc_reg_date_end"
                                   value="<?php echo isset($_GET['doc_reg_date_end']) ? $_GET['doc_reg_date_end'] : ''; ?>"/>
                        </div>
                        <div style='float:left;width:22.5%;'>
                            <?php echo $ref_no; ?>
                        </div>
                        <div style='float:left;width:25%;'>
                            <input type="text" style='width:90%' name="ref_no"
                                   value="<?php echo isset($_GET['ref_no']) ? $_GET['ref_no'] : ''; ?>"/>
                        </div>
                        <div style="float:left;text-align:right;width:100%;">
                            <input type="submit" value="Search" name="search_button" class="newbutton"/>
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='clear:both'></div>
                </div>
            </fieldset>
        </div>
</form>
<?php
if (isset($_GET['doc_reg_no'])) {
    $doc_reg = $_GET['doc_reg_no'];
}
if (isset($_GET['doc_reg_date_start'])) {
    $doc_reg_date_s = $_GET['doc_reg_date_start'];
}
if (isset($_GET['doc_reg_date_end'])) {
    $doc_reg_date_end = $_GET['doc_reg_date_end'];
}
if (isset($_GET['doc_title'])) {
    $doc_t = $_GET['doc_title'];
}
if (isset($_GET['ref_no'])) {
    $ref_no = $_GET['ref_no'];
}
?>
<div class="content" id="conteudo">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($doc_reg_no, $doc_reg_date_start, $doc_title, $ref_no, $ref_date);
    if (isset($_GET['doc_reg_no']) || isset($_GET['doc_reg_date_start']) || isset($_GET['doc_reg_date_end']) || isset($_GET['doc_title']) || isset($_GET['ref_no'])) {
        echo $document_registration_info->searchgridview($_GET['doc_reg_no'], $_GET['doc_reg_date_start'], $_GET['doc_reg_date_end'], $_GET['doc_title'], $_GET['ref_no'], $language_id, $committe_id, $user_id, $param);
    } else {
        echo $document_registration_info->searchgridview('', '', '', '', '', $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>