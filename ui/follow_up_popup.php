<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
<title></title>
<!--	<link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">-->
<!--	<link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">-->
<!--	<script src="codebase_m/dhtmlxcalendar.js"></script>-->
<!--	<style>-->
<!--	#calendar,-->
<!--	#calendar2,-->
<!--	#calendar3,-->
<!--	#calendar4 {-->
<!--	border: 1px solid #909090;-->
<!--	font-family: Tahoma;-->
<!--	font-size: 12px;-->
<!--	}-->
<!--	</style>-->
<!--	<script>-->
<!--	var myCalendar;-->
<!--	function doOnLoad() {-->
<!--   	 myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);-->
<!--	}-->
<!--	</script>-->

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
<?php else: ?>
    <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>
<link type="text/css" rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css"/>
<style>
    #overlay_form {
        /*position: absolute;
        /*border: 5px solid gray;*/
        padding: 0px;
    }

    .loader {
        display: block;
        border: 1px solid gray;
        width: 165px;
        text-align: center;
        padding: 6px;
        border-radius: 5px;
        text-decoration: none;
        /*margin: 0 auto;*/
    }

    .close_box#close {
        background: fuchsia;
        color: #000;
        padding: 2px 5px;
        display: inline;
        position: absolute;
        right: 15px;
        border-radius: 3px;
        cursor: pointer;
    }

    .columnA {
        float: left;
        width: 150px;
    }

    .columnB {
        float: left;
    }

    .columnAbutton {
        float: left;
        width: 100px;
    }

    .columnBbutton {
        float: left;
    }

    .divclear {
        clear: both;
    }

    textarea {
        height: 60px;
        width: 227px;
    }
</style>
<script src="js/nic/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function () {
        new nicEditor().panelInstance('area1');
        new nicEditor().panelInstance('area2');
    });
</script>
<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">
<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script>
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
        jQuery('#followup_date').datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});
    });
</script>
<!--Validation End-->
<script>
    var L = 0;
    $(document).ready(function () {

        $("#btn2").click(function () {
            var j = 1, m = 1;
            for (; ;) {
                //alert(j);
                if (document.getElementById(j))
                    j++;
                m++;
                if (m > 50)break;
            }
            var i = j;
            //alert(j);
            $("#btn1").append("<div id='" + i + "'><div class='columnA'><input name='recom_id[]' type='hidden' value='0' /><label id='label" + i + "'>Recommendation " + i + "</label></div><div class='columnB' style='width:70%'><input type='hidden' name='recom_no[]'/><textarea name='recommendation[]' class='bangla' id='txt" + i + "' style='font-size: 11px;width: 78%;'></textarea><input type='button' name='recommendation[]' value='Delete' onclick='myFunction(" + (i) + ");'  class='popupbutton simplemodal-wrap'/></div>&nbsp;&nbsp;<div class='divclear'></div><br></div>"
            );
            new nicEditor().panelInstance('txt' + i + '');
            L = i;
        });
        //alert(L);
    });
    function myFunction(p) {
        var div = document.getElementById(p);
        while (div) {
            div.parentNode.removeChild(div);
            div = document.getElementById(p);
        }
        //alert(L);
        var k = 1;
        for (var j = 0; j <= L; j++) {
            //alert(L);
            if (document.getElementById(j)) {//alert(j);
                document.getElementById('label' + j).innerHTML = "Recommendation " + (k++) + "";
            }
        }
        L = k;
    }
    function jsfunction() {
        //new nicEditor().panelInstance('txt');
        var j = 1;
        while (1) {
            if (document.getElementById('txt' + j))
                new nicEditor().panelInstance('txt' + j);
            else
                break;
            j++
        }
    }
</script>
<?php
session_start();
//echo $_SESSION['language_id'];
?>
<?php
require_once('../model/follow_up_info.php');
require_once('../model/inquiry_report_info.php');
?>
<?php
$user_id = $_SESSION['user_id'];
//echo $_GET['ministry_member_id'];
if ($_GET['inquiry_report_followup_id'] != 0) {
    $result = $follow_up_info->editrow(array($_GET['inquiry_report_followup_id'], $user_id));
}

if ($_GET['inquiry_report_id'] != 0) {
    $result1 = $inquiry_report_info->editrow(array($_GET['inquiry_report_id'], $user_id));
}

?>
<?php
$xml = simplexml_load_file("xml/follow_up_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $report_no = $information->report_no;
        $report_date = $information->report_date;
        $title = $information->title;
        $parliament = $information->parliament;
        $session = $information->session;
        $followup_date = $information->followup_date;
        $background = $information->background;
        $issues = $information->issues;
        $recommendation = $information->recommendation;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $followup_no = $information->followup_no;
        $remarks = $information->remarks;
    }
}
?>

</head>
<body>
<form id="formID" name="overlay_form" method="post">
    <h2><?php echo $headingpopup; ?></h2>
    <br>

    <div style="width:100%;">
        <div>
            <div class="columnA"><label><?php echo $report_no; ?></label></div>
            <div class="columnB">
                <input type="hidden" name="inquiry_report_followup_id"
                       value="<?php echo isset($result->inquiry_report_followup_id) ? $result->inquiry_report_followup_id : ''; ?>"/>
                <input type="hidden" name="inquiry_report_id" value="<?php echo $_GET['inquiry_report_id']; ?>"/>
                <input id="name" type="text" name="report_no"
                       value="<?php echo isset($result1->report_no) ? $result1->report_no : ''; ?>" readonly="true"/>
            </div>
            <div class="columnA" style="width:15%;"><label><?php echo $report_date; ?></label></div>
            <div class="columnB" style="text-align:right; width:10%;"><input id="name" style="width:90%;"
                                                                             value="<?php echo isset($result1->report_date) ? $result1->report_date : ''; ?>"
                                                                             readonly="true" type="text"
                                                                             name="parliament_id"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $followup_no; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="followup_no" value="[Auto]" readonly="true"/></div>
            <div class="columnA" style="width:15%;"><label><?php echo $followup_date; ?></label></div>
            <div class="columnB" style="text-align:right; width:10%;"><input id="followup_date" style="width:90%;"
                                                                             type="text" name="followup_date"
                                                                             value="<?php echo isset($result->followup_date) ? $result->followup_date : ''; ?>"
                                                                             class='validate[required,maxSize[150]] text-input bangla'/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $title; ?></label></div>
            <div class="columnB"><textarea readonly="true" name='title'
                                           style="font-size: 11px;width: 450px;"><?php echo isset($result1->title) ? $result1->title : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <b><?php echo $background; ?></b>

            <div class="divclear"></div>
        </div>
        <div>
            <div><textarea id='area1' name='background'
                           style="font-size: 11px;width: 90%;"><?php echo isset($result->background) ? $result->background : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div style="padding-top:2px;">
            <?php //echo isset($result->remarks) ? $result->remarks: ''; ?>
            <div class="columnA"><label><?php echo $remarks; ?></label></div>
            <div class="columnB">
                <textarea style="width:500px; height:50px;"
                          name="remarks"><?php echo isset($result->remarks) ? $result->remarks : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <b><?php echo $recommendation; ?></b>

            <div class="divclear"></div>
        </div>
        <div style="height:150px; width:100%;">
            <!--<div>
                <div class="columnA" style="width:25%;"><label>Committee<br style="line-height:1px;">Recommendation</label></div>
                <div class="columnA" style="width:25%;"><label>Response of Organization</label></div>
                <div class="columnA" style="width:25%;"><label>Action Taken</label></div>
                <div class="columnA" style="width:25%;"><label>Comment</label></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA" style="width:25%;"><textarea type='text' name='txtdocumentregno' style="font-size: 11px;width: 88%;"></textarea></div>
                <div class="columnA" style="width:25%;"><select><option value='Accept'>Accept</option><option value='Not Accepted'>Not Accepted</option><option value='Partially Accepted'>Partially Accepted</option></select></div>
                <div class="columnA" style="width:25%;"><textarea type='text' name='txtdocumentregno' style="font-size: 11px;width: 88%;"></textarea></div>
                <div class="columnA" style="width:25%;"><textarea type='text' name='txtdocumentregno' style="font-size: 11px;width: 88%;"></textarea></div>
                <div class="divclear"></div>
            </div>		-->
            <?php
            if ($_GET['inquiry_report_id'] != NULL) {
                $inquiry_report_id = $_GET['inquiry_report_id'];
                $inquiry_report_followup_id = $_GET['inquiry_report_followup_id'];
                $user_id = $_SESSION['user_id'];
                echo $follow_up_info->gridviewDetails($inquiry_report_followup_id, $inquiry_report_id, $user_id);
                echo '<script type="text/javascript">'
                , 'jsfunction();'
                , '</script>';
            }
            ?>
            <div class="divclear"></div>
        </div>
        <br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
        <br>

        <div style="text-align:center; margin-top:5px; float:left; width:65%">

            <input type="button" onclick='window.close();' value="Close" class="popupbutton simplemodal-wrap"/>
            &nbsp;&nbsp;<input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
