<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/sub_committee.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "sub_committee_delete.php?sub_committee_id=" + p;
        }
        else {

        }
    }
    function popupwinid(p) {
        window.showModalDialog("sub_committee_popup.php?sub_committee_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:480px;dialogHeight:300px")
    }
    function popupwinis(p, q) {
        window.showModalDialog("sub_committee_popup.php?sub_committee_id=" + p + "&committee_id=" + q, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:480px;dialogHeight:200px")

    }
</script>
<script>
    function show(str) {
        if (str == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "sub_committee_data.php?committee_id=" + str, true);
        xmlhttp.send();
    }
</script>

<?php
require_once('../model/sub_committee_info.php');
?>
<?php
$xml = simplexml_load_file("xml/sub_committee_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $id = $information->id;
        $committee = $information->committee;
        $sub_committee_name = $information->sub_committee_name;
    }
}

?>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<div style="margin-top:3px;font-size:16px;">
    <?php echo $committee; ?>
    <?php
    if (isset($_GET['committee_id'])) {
        $id = $_GET['committee_id'];
    }
    $language_id = $_SESSION['language_id'];
    $user_id = 1;
    echo $sub_committee->comboview($language_id, $user_id, $id);
    ?>
</div>

<div class="content" id="conteudo">
    <?php
    echo '<div id="txtHint">';
    if (isset($_GET["committee_id"])) {
        $committee_id = $_GET["committee_id"];
        $language_id = $_SESSION['language_id'];
        echo '<input type="submit" onClick="popupwinis(0,' . $committee_id . ');" name="basic" value="Add New" class="newbutton"/> ';
        echo '<br>';
        $user_id = $_SESSION['user_id'];
        $committe_id = $_SESSION['committe_id'];
        $param = array($id, $parliament);
        echo $sub_committee->gridview($language_id, $committee_id, $user_id, $param);
    }
    echo '</div>';
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>