<?php
session_start();
require_once('../model/user.php');
require_once('../model/user_committee_permission.php');
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link type='text/css' href='css/grid.css' rel='stylesheet' media='screen'/>
    <!-- Contact Form CSS files -->
    <script type="text/javascript" language="javascript">
        function popupwinid(p, q, r) {
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("divgroup").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "user_committee_permission_data.php?committee_id=" + p + "&user_id=" + q + "&status=" + r, true);
            xmlhttp.send();
        }
    </script>
    <style>
        tr:nth-child(odd) {
            background-color: #F2F1F0;
            color: #ffffff;
        }
    </style>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<?php
if ($_GET['user_id'] != NULL) {
    $result = $user->editrow(array($_GET['user_id']));
}
?>
<body>
<form id="overlay_form" name="overlay_form" method="post" action="#">
    <h2><?php echo 'User Committee Permission'; ?></h2>
    </br>
    <div>
        <div>
            <div class="columnA">&nbsp;</div>
            <div class="columnB">&nbsp;</div>
            <div class="columnA"><label><?php echo 'User Name' ?></label></div>
            <div class="columnB">
                <input id="name" type="text" name="user_name" value="<?php echo $result->user_name; ?>"
                       readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div id="divgroup" style="width:100%;">
            <?php
            echo $user_committee_permission->getgrid($_GET['user_id'], $_SESSION['language_id']);
            ?>
        </div>
    </div>
</form>
</body>
</html>
