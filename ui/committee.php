<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/committee.css' rel='stylesheet' media='screen'/>
<?php
require_once('../model/commiittee_info.php');
?>
<?php
$xml = simplexml_load_file("xml/committee_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $id = $information->id;
        $committee_name = $information->committee_name;
        $short_name = $information->short_name;
        $section = $information->section;
        $branch = $information->branch;
        $contact_person = $information->contact_person;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
        $comments = $information->comments;
    }
}

?>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("committee_popup.php?committee_id=" + p, "", "dialogTop:180px;dialogLeft:300px;dialogWidth:850px;dialogHeight:480px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "committee_delete.php?committee_id=" + p;
        }
        else {

        }
    }
</script>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<div style="margin-top:3px;">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($id, $committee_name, $short_name);
    echo $commiittee_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>