<?php
session_start();

require_once('../model/create_inquiry_info.php');

if (isset($_GET['q'])) {
    $keyword = $_GET['q'];
    $language_id = $_SESSION['language_id'];
    $committee_id = $_SESSION['committee_id'];
    echo $create_inquiry_info->gridview_token_document($keyword, $language_id, $committee_id);
} else {
    $name = '';
    echo '[{"name":"Foreign","id":"2"}}]';
}
?>
