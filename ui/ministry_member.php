<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<script type="text/javascript" src="js/jquery.js"></script>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>


<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>


<script type='text/javascript'>
    function popupwinid_member(p) {
        window.showModalDialog("ministry_member_popup.php?ministry_member_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:390px")

    }
    function deleteid(p, q) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Deletep'+p+'Deleteq'+q);
        if (r == true) {
            window.location.href = "ministry_member__delete.php?ministry_id=" + p + "&ministry_member_id=" + q;
        }
        else {

        }
    }
</script>
<?php
require_once('../model/ministry_member_info.php');
?>
<?php
$xml = simplexml_load_file("xml/ministry_member_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $ministry_member_name = $information->ministry_member_name;
        $ministry_name = $information->ministry_name;
        $designation = $information->designation;
        $constituency = $information->constituency;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right;">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>

<form id="overlay_form" name="overlay_form" method="get" action="#">
    <?php
    if (isset($_GET['ministry_id']) || isset($_GET['ministry_member_name']) || isset($_GET['designation'])) {
        echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
    } else {
        echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
    }
    ?>

    <fieldset>
        <legend>Search Criteria</legend>
        <div style='margin:3px;'>
            <div style='float:left;width:22.5%;'><?php echo $ministry_name; ?></div>
            <div style='float:left;width:25%'>
                <?php
                if (isset($_GET['ministry_id'])) {
                    $id = $_GET['ministry_id'];
                }
                $language_id = $_SESSION['language_id'];
                $user_id = 1;
                echo $ministry_member_info->comboview($language_id, $user_id, $id);
                ?>
            </div>
            <div style='float:left;width:22.5%;'><?php echo $ministry_member_name; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="ministry_member_name"
                                                     value="<?php echo isset($_GET['ministry_member_name']) ? $_GET['ministry_member_name'] : ''; ?>"/>
            </div>
            <div style='clear:both'></div>
        </div>
        <div style='margin:3px;'>
            <div style='float:left;width:22.5%;'><?php echo $designation; ?></div>
            <div style='float:left;width:25%'><input type="text" style='width:90%' name="designation"
                                                     value="<?php echo isset($_GET['designation']) ? $_GET['designation'] : ''; ?>"/>
            </div>
            <div style='clear:both'></div>
        </div>
        <div>
            <div style="float:left;text-align:right;width:100%;">
                <input type="submit" value="Search" class="newbutton"/>
            </div>
        </div>
    </fieldset>
    </div>
</form>
<input type="button" onClick="popupwinid_member(0);" name="basic" value="Add Member" class="newbutton"/>
<?php
if (isset($_GET['ministry_id'])) {
    if ($_GET['ministry_id'] == NULL) {
        $ministry_id = 0;
    } else {
        $ministry_id = $_GET['ministry_id'];
    }
}
if (isset($_GET['ministry_member_name'])) {
    $ministry_member_name_ = $_GET['ministry_member_name'];
}
if (isset($_GET['designation'])) {
    $designation_ = $_GET['designation'];
}
//echo $ministry_id.'--'.$ministry_member_name.'--'.$designation;
?>
<div class="content" id="conteudo">
    <?php
    if (isset($_GET['ministry_id']) || isset($_GET['ministry_member_name']) || isset($_GET['designation'])) {
        $language_id = $_SESSION['language_id'];
        $user_id = $_SESSION['user_id'];
        $param = array($ministry_member_name, $ministry_name, $designation);
        echo $ministry_member_info->searchgridview($ministry_id, $ministry_member_name_, $designation_, $language_id, $user_id, $param);
    } else {
        $param = array('', '', '');
        echo $ministry_member_info->searchgridview(0, '', '', $language_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>