<?php
session_start();
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
<title></title>
<!--	<link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">-->
<!--	<link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">-->
<!--	<script src="codebase_m/dhtmlxcalendar.js"></script>-->
<!--	<style>-->
<!--	#calendar,-->
<!--	#calendar2,-->
<!--	#calendar3,-->
<!--	#calendar4 {-->
<!--	border: 1px solid #909090;-->
<!--	font-family: Tahoma;-->
<!--	font-size: 12px;-->
<!--	}-->
<!--	</style>-->
<!--	<script>-->
<!--	var myCalendar;-->
<!--	function doOnLoad() {-->
<!--   	 myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);-->
<!--	}-->
<!--	</script>-->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
<?php else: ?>
    <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="styles/jquery-ui-1.10.4.custom.min.css"/>
<style>
    #overlay_form {
        position: absolute;
        /*border: 5px solid gray;*/
        padding: 0px;
    }

    .loader {
        display: block;
        border: 1px solid gray;
        width: 165px;
        text-align: center;
        padding: 6px;
        border-radius: 5px;
        text-decoration: none;
        /*margin: 0 auto;*/
    }

    .close_box#close {
        background: fuchsia;
        color: #000;
        padding: 2px 5px;
        display: inline;
        position: absolute;
        right: 15px;
        border-radius: 3px;
        cursor: pointer;
    }

    .columnA {
        float: left;
        width: 150px;
    }

    .columnB {
        float: left;
    }

    .columnAbutton {
        float: left;
        width: 100px;
    }

    .columnBbutton {
        float: left;
    }

    .divclear {
        clear: both;
    }

    textarea {
        height: 60px;
        width: 227px;
    }

    fieldset {
        width: 85%;
        margin: 8px;
        padding-left: 35px;
        border: 0px;
    }

    legend {
        font-weight: bold;
        color: transparent;
    }
</style>
<!--	<script type='text/javascript' src='js/jquery.js'></script>-->
<!--	<script type='text/javascript' src='js/jquery.simplemodal.js'></script>-->
<script src="js/nic/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function () {
        new nicEditor().panelInstance('area1');
        new nicEditor().panelInstance('area2');
        new nicEditor().panelInstance('area3');
        new nicEditor().panelInstance('area4');
        new nicEditor().panelInstance('area5');
        /*new nicEditor({fullPanel : true}).panelInstance('area2');
         new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
         new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
         new nicEditor({maxHeight : 100}).panelInstance('area5');*/
    });
</script>
<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">

<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script>
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
        jQuery('#report_date').datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});
    });
</script>

<script>
    var L = 0;
    $(document).ready(function () {
        $("#btn2").click(function () {
            var j = 1, m = 1;
            for (; ;) {
                if (document.getElementById(j))
                    j++;
                m++;
                if (m > 50)break;
            }
            var i = j;
            $("#btn1").append("<div id='" + i + "'><div class='columnA'><input name='recom_id[]' type='hidden' value='0' /><label id='label" + i + "'>Recommendation " + i + "</label></div><div class='columnB' style='width:70%'><input type='hidden' name='recom_no[]'/><textarea name='recommendation[]' class='bangla' id='txt" + i + "' style='font-size: 11px;width: 78%;'></textarea><input type='button' name='recommendation[]' value='Delete' onclick='myFunction(" + (i) + ");'  class='popupbutton simplemodal-wrap'/></div>&nbsp;&nbsp;<div class='divclear'></div><br></div>"
            );
            new nicEditor().panelInstance('txt' + i + '');
            L = i;
        });

        $('#inquiry_id').change(function(){
            $.post('inquiry_report_data.php',
                {inquiry_id: this.value},
                function(data){
                    data = JSON.parse(data);
                    var arrayLength = data.length;
                    $('#area1').html(data[0]);
                    $('#area1').prev().children().html(data[0]);

                    $('#btn1').html('<div></div>');
                    for (var i = 1; i < arrayLength; i++) {
                        $('#btn2').trigger('click');
                        $('#txt'+L).html(data[i]);
                        $('#txt'+L).prev().children().html(data[i]);
                    }
            });
        });
    });
    function myFunction(p) {
        var div = document.getElementById(p);
        while (div) {
            div.parentNode.removeChild(div);
            div = document.getElementById(p);
        }
        var k = 1;
        for (var j = 0; j <= L; j++) {
            if (document.getElementById(j)) {
                document.getElementById('label' + j).innerHTML = "Recommendation " + (k++) + "";
            }
        }
        L = k;
    }
    function jsfunction() {
        var j = 1;
        while (1) {
            if (document.getElementById('txt' + j))
                new nicEditor().panelInstance('txt' + (j++) + '');
            else
                break;
        }
    }
</script>

<?php
require_once('../model/inquiry_report_info.php');
?>
<?php
$xml = simplexml_load_file("xml/inquiry_report_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $report_no = $information->report_no;
        $report_date = $information->report_date;
        $title = $information->title;
        $parliament = $information->parliament;
        $session = $information->session;
        $executive_summary = $information->executive_summary;
        $introduction = $information->introduction;
        $issues = $information->issues;
        $recommendation = $information->recommendation;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $remarks = $information->remarks;
    }
}
?>
<?php
$user_id = $_SESSION['user_id'];
//echo $_GET['ministry_member_id'];
if ($_GET['inquiry_report_id'] != NULL) {
    // var_dump($user_id);
    $result = $inquiry_report_info->editrow(array($_GET['inquiry_report_id'], $user_id));
}
?>

<!--ajax p-->
<script>
    function show(str) {
        if (str == "") {
            document.getElementById("area1").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("area1").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "inquiry_report_data.php?inquiry_id=" + str, true);
        xmlhttp.send();
    }
</script>

</head>
<body onLoad="doOnLoad();">
<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>

    <!--fdfddffd-->
    <div style="width:1200px;">
        <div>
            <div style="float:left;width:16%;"><label><?php echo $report_no; ?></label></div>
            <input type="hidden" name="inquiry_report_id"
                   value="<?php echo isset($result->inquiry_report_id) ? $result->inquiry_report_id : ''; ?>"/>

            <div style="float:left;width:16%;"><input id="name" type="text" class='bangla' readonly="true"
                                                      value="<?php echo isset($result->report_no) ? $result->report_no : '[Auto]'; ?>"
                                                      name="report_no"/></div>
            <div style="float:left;width:10%;"><label><?php echo $report_date; ?> </label></div>
            <div style="float:left;width:16%;"><input style="width:90%;" id="report_date"
                                                      value="<?php echo isset($result->report_date) ? $result->report_date : ''; ?>"
                                                      type="text" class='text-input bangla' name="report_date"/></div>
            <div style="float:left;width:10%;"><label><?php echo $inquiry_no; ?> *</label></div>
            <div style="float:left;width:16%;">
                <?php
                $language_id = $_SESSION['language_id'];
                $user_id = $_SESSION['user_id'];
                $committee_id = $_SESSION['committee_id'];
                $inquiry_id = isset($result->inquiry_id) ? $result->inquiry_id : 0;
                echo $inquiry_report_info->comboviewinquiry($inquiry_id, $language_id, $committee_id, $user_id);
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="float:left;width:16%;"><label><?php echo $title; ?> *</label></div>
            <div style="float:left;width:84%;" id='txtHint'><textarea type='text'
                                                                      class='validate[required,maxSize[80]] text-input bangla'
                                                                      name='title' id="area1"
                                                                      style="font-size: 11px;width: 100%;"><?php echo isset($result->title) ? $result->title : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div style="float:left;width:16%;">
                <b><?php echo $executive_summary; ?> *</b>
            </div>
            <div style="float:left;width:84%;"><textarea type='text'
                                                         class='validate[required,maxSize[80]] text-input bangla'
                                                         name='executive_summary' id="area2"
                                                         style="font-size: 11px;width: 100%;"><?php echo isset($result->executive_summary) ? $result->executive_summary : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <fieldset>
            <legend><?php echo $recommendation; ?></legend>
            <div id="btn1">
                <?php
                if ($_GET['inquiry_report_id'] != NULL) {
                    $inquiry_report_id = $_GET['inquiry_report_id'];
                    $user_id = $_SESSION['user_id'];
                    echo $inquiry_report_info->gridviewDetails($inquiry_report_id, $user_id);
                    echo '<script type="text/javascript">'
                    , 'jsfunction();'
                    , '</script>';
                }
                ?>
            </div>
        </fieldset>
        <div>
            <div class="columnA"><label></label></div>
            <div class="columnB" style="width:70%">
                <input type="button" id="btn2" value="Add More" class="popupbutton simplemodal-wrap"/>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div style="float:left;width:16%;">
                <b><?php echo $introduction; ?> *</b>
            </div>
            <div style="float:left;width:84%;"><textarea type='text'
                                                         class='validate[required,maxSize[80]] text-input bangla'
                                                         name='introduction' id="area3"
                                                         style="font-size: 11px;width: 100%;"><?php echo isset($result->introduction) ? $result->introduction : ' '; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div style="float:left;width:16%;">
                <b><?php echo $issues; ?> *</b>
            </div>
            <div style="float:left;width:84%;"><textarea type='text'
                                                         class='validate[required,maxSize[80]] text-input bangla'
                                                         name='issues' id="area4"
                                                         style="font-size: 11px;width: 100%;"><?php echo isset($result->issues) ? $result->issues : ' '; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="float:left;width:16%;">
        <b><?php echo $remarks; ?> </b>
    </div>
    <div style="float:left;width:84%;"><textarea type='text' class='validate[maxSize[80]] text-input bangla'
                                                 name='remarks'
                                                 style="font-size: 11px;width: 100%;"><?php echo isset($result->remarks) ? $result->remarks : ' '; ?></textarea>
    </div>
    <div class="divclear"></div>
    </div>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
    <input name="parliament_id" type="hidden"
           value="<?php echo isset($result->parliament_id) ? $result->parliament_id : ''; ?>"/>
    <input name="session_id" type="hidden"
           value="<?php echo isset($result->session_id) ? $result->session_id : ''; ?>"/>

    <div class="divclear"></div>
    </div>
    <div style="text-align:center; float:left; width:65%">
        <input type="button" onclick='window.close();' value="Close" class="popupbutton simplemodal-wrap"/>
        &nbsp;&nbsp;<input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>