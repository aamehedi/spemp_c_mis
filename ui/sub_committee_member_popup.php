<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
$xml = simplexml_load_file("xml/session_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $id = $information->id;
        $parliament = $information->parliament;
        $session = $information->session;
    }
}
?>
<?php
require_once('../model/sub_committee_member_info.php');
$user_id = $_SESSION['user_id'];
if (isset($_GET['sub_committee_member_id']) != NULL) {
    $result = $sub_committee_member_info->editrow(array($_GET['sub_committee_member_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">
<?php
$xml = simplexml_load_file("xml/sub_committee_member.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $id = $information->id;
        $sub_committee_name = $information->sub_committee_name;
        $member_name = $information->member_name;
        $is_active = $information->is_active;
        $inactive = $information->inactive;
    }
}
?>

<form id="formID" class="formular" name="overlay_form" method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
    <h2>
        <?php echo $heading; ?>
    </h2>
    </br>
    <div>
        <div>
            <input type="hidden" name="sub_committee_member_id"
                   value="<?php echo isset($result->sub_committee_member_id) ? $result->sub_committee_member_id : ''; ?>"/>

            <div class="columnA"><label><?php echo $sub_committee_name; ?> * </label></div>
            <div class="columnB">
                <?php
                $id = isset($result->sub_committee_id) ? $result->sub_committee_id : '';
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $user_id = $_SESSION['user_id'];
                echo $sub_committee_member_info->comboview_sub_committee($language_id, $committee_id, $user_id, $id);
                ?>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div class="columnA"><label><?php echo $member_name; ?> *</label></div>
            <div class="columnB">
                <?php
                $id = isset($result->committee_member_id) ? $result->committee_member_id : '';
                //echo $result->parliament_id;
                //$id=1;
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $user_id = $_SESSION['user_id'];
                echo $sub_committee_member_info->comboview_committee_member($committee_id, $language_id, $user_id, $id);
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $is_active; ?></label></div>
            <div class="columnB">
                <?php
                $is_active_value = isset($result->is_active) ? $result->is_active : 1;
                if ($is_active_value == 0) {
                    echo '<input type="radio" name="is_active" value="1" style="display:inline;" > ' . $is_active;
                    echo '<input type="radio" name="is_active" value="0" style="display:inline;" checked> ' . $inactive . '&nbsp;&nbsp;';
                } elseif ($is_active_value == 1) {
                    echo '<input type="radio" name="is_active" value="1" style="display:inline;" checked> ' . $is_active;
                    echo '<input type="radio" name="is_active" value="0" style="display:inline;"> ' . $inactive . '&nbsp;&nbsp;';
                }
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
</form>
</body>
</html>
