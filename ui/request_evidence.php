<?php
require_once('header.php');
require_once('aside_left.php');
require_once('menu.php');
?>

<link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
<script type="text/javascript" src="js/jquery.js"></script>
<script type='text/javascript'>

    $(document).ready(function () {
        $('input[value="Print View"]').click(function () {
            if ($(this).prev().val() == '1') {
                window.showModalDialog("../pdf/request_evidence/pdf_rpt_request_evidence_" + $(this).attr('name') + ".pdf", "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px");
            } else {
                window.showModalDialog("rpt_request_evidence.php?request_evidence_id=" + $(this).attr('name'), "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:390px");
            }
            ;
        });
    });

    function popupwinid(p) {
        window.showModalDialog("request_evidence_popup.php?request_evidence_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:390px");
    }

    // function printid(p)
    // {
    // 	window.showModalDialog("rpt_request_evidence.php?request_evidence_id="+p,"","dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:390px");
    // }

    function issuedid(p) {
        var r = confirm("Do You Want To Issued This Record Or Data?");
        if (r == true) {
            window.location.href = "request_evidence_issued.php?request_evidence_id=" + p;
        }
    }
</script>

<?php
require_once('../model/request_evidence_info.php');

$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}
?>
<?php
$xml = simplexml_load_file("xml/request_evidance_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date = $information->doc_reg_date;
        $doc_title = $information->doc_title;
        $designation = $information->designation;
        $doc_name = $information->doc_name;
        $to = $information->to;
        $year = $information->year;
        $month = $information->month;
        $date = $information->date;
        $inquiry_title = $information->inquiry_title;
        $inquiry_no = $information->inquiry_no;
        $create_date = $information->create_date;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<div style="padding-bottom:10px;">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
    <br/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committee_id = $_SESSION['committee_id'];
    $param = array($id, $parliament);
    echo $request_evidence_info->gridview($language_id, $committee_id, $user_id, $param);
    ?>
</div>
</div>
<?php
require_once('aside_right.php');
require_once('footer.php');
?>	 