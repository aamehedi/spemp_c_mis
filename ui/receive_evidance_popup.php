<?php
session_start();
?>
<?php
require_once('../model/receive_evidance_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['doc_reg_id'] != NULL) {
    $result = $receive_evidance_info->editrow(array($_GET['doc_reg_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
<?php else: ?>
    <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>
<link type="text/css" rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css"/>
<style>
    #overlay_form {
        position: absolute;
        /*border: 5px solid gray;*/
        padding: 0px;
    }

    .loader {
        display: block;
        border: 1px solid gray;
        width: 165px;
        text-align: center;
        padding: 6px;
        border-radius: 5px;
        text-decoration: none;
        /*margin: 0 auto;*/
    }

    .close_box#close {
        background: fuchsia;
        color: #000;
        padding: 2px 5px;
        display: inline;
        position: absolute;
        right: 15px;
        border-radius: 3px;
        cursor: pointer;
    }

    .columnA {
        float: left;
        width: 150px;
    }

    .columnB {
        float: left;
    }

    .columnAbutton {
        float: left;
        width: 100px;
    }

    .columnBbutton {
        float: left;
    }

    .divclear {
        clear: both;
    }

    textarea {
        height: 60px;
        width: 227px;
    }

    .myfileupload-buttonbar {
        float: left;
        width: 10%;
    }

    .myfileupload-buttonbar input {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        border: solid transparent;
        border-width: 0 0 100px 200px;
        opacity: 0.0;
        filter: alpha(opacity=0);
        -o-transform: translate(250px, -50px) scale(1);
        -moz-transform: translate(-300px, 0) scale(4);
        direction: ltr;
        cursor: pointer;
    }

    .myui-button {
        position: relative;
        cursor: pointer;
        text-align: center !important;
        overflow: visible;
        /*background-color: red;*/
        overflow: hidden;
        /*width: 10%;*/
        max-height: 22px;
        padding-top: 5px;
        /*padding-bottom: 2px;*/

        background-color: #759ae9;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #759ae9), color-stop(50%, #376fe0), color-stop(50%, #1a5ad9), color-stop(100%, #2463de));
        background-image: -webkit-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -moz-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -ms-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: -o-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        background-image: linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
        border-top: 1px solid #1f58cc;
        border-right: 1px solid #1b4db3;
        border-bottom: 1px solid #174299;
        border-left: 1px solid #1b4db3;
        border-radius: 4px;
        -webkit-box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
        box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
        color: #fff;
        font: bold 12px/1 "helvetica neue", helvetica, arial, sans-serif;
        padding: 5px 0;
        text-shadow: 0 -1px 1px #1a5ad9;
        display: block;
        width: 100%;
    }

    }
    .myui-button:hover {
        background-color: #5d89e8;
        background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #5d89e8), color-stop(50%, #2261e0), color-stop(50%, #044bd9), color-stop(100%, #0d53de));
        background-image: -webkit-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -moz-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -ms-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: -o-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        background-image: linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
        cursor: pointer;
    }

    .myui-button:active {
        border-top: 1px solid #1b4db3;
        border-right: 1px solid #174299;
        border-bottom: 1px solid #133780;
        border-left: 1px solid #174299;
        -webkit-box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
        box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
    }
</style>
<script>
    function myFunction(p) {
        var div = document.getElementById(p);
        while (div) {
            div.parentNode.removeChild(div);
            div = document.getElementById(p);
        }
        var k = 1;
        for (var j = 0; j <= L; j++) {
            if (document.getElementById(j)) {//alert(j);
                document.getElementById('label' + j).innerHTML = "Recommendation " + (k++) + "";
            }
        }
        L = k;
    }
</script>


<!--Validation End-->
<script>
    function download(p) {
        window.showModalDialog("download_evidence.php?rec_evidence_details_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:20px;dialogHeight:20px")
    }
</script>

<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">
<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script>
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
        jQuery("#receive_deadline_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});
    });

</script>
<!--Validation End-->

<script>

    function checkTitle(field, rules, i, options) {
        var uploadTitles = document.getElementsByName('title[]');
        var uploadFileNames = [];
        for (i = 0; i < uploadTitles.length; ++i) {
//                    console.log(uploadTitles[i].value);
            if (field[0] === uploadTitles[i]) {
                break;
            } else {
                uploadFileNames.push(uploadTitles[i].value);
            }
        }
        if (uploadFileNames.indexOf(field[0].value) >= 0) {
            return 'Title already exists';
        }
    }

    $(document).ready(function () {
        var clickCount = 0;
        $(':input[id^="evidanceCalendar"]').each(function () {
            clickCount++;
            $(this).datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });
        $(':input[id^="referenceCalendar"]').each(function () {
            $(this).datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });
        $("#btn2").click(function () {
            clickCount++;
            var j = 1, m = 1;
            for (; ;) {
                //alert(j);
                if (document.getElementById(j))
                    j++;
                m++;
                if (m > 50)break;
            }
            var i = j;

            /* Edited by Mehedi */
            $("#btn1").append("<div id='" + i + "'><input name='rec_evidence_details_id[]' type='hidden' value='0' /><div style='float:left;width:11%;'><select name='evidence_type[]'><option value='WrittenEvidence'>Written Evidence</option><option value='Multimedia'>Multimedia</option></select></div><div style='float:left;width:11%;'><input style='width:90%;' type='text' name='submitted_by[]' class='validate[required] text-input bangla'/></div><div style='float:left;width:11%;'><input  type='text' name='title[]' style='width:90%;' class='validate[required,funcCall[checkTitle]] text-input bangla' /></div><div style='float:left;width:11%;'><input  type='text' id='evidanceCalendar" + clickCount + "' name='receive_date[]' style='width:90%;' class='validate[required] text-input bangla'/></div><div style='float:left;width:11%;'><input  type='text'  style='width:90%;'  name='ref_no[]' /></div><div style='float:left;width:11%;'><input  type='text' style='width:90%;' id='referenceCalendar" + clickCount + "' name='ref_date[]' /></div><div class='myfileupload-buttonbar'><label class='myui-button'>Add Files<input type='file' name='file_name[]' id='file' class='validate[required]' /></label></div><div style='float:left;width:11%;'>&nbsp;</div><div style='float:left;width:11%;'><input type='button' value='Delete' class='gridbutton' onclick='myFunction(" + (i) + ");'/></div><div class='divclear'></div></div>");
            $('#evidanceCalendar' + clickCount).datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
            $('#referenceCalendar' + clickCount).datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });
    });
</script>
<!--    <div style='float:left;width:12%;'><input  type='file' name='file_name[]' class='validate[required]'/></div>-->

</head>
<?php
$xml = simplexml_load_file("xml/receive_evidance_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date = $information->doc_reg_date;
        $doc_title = $information->doc_title;
        $receive_deadline_date = $information->receive_deadline_date;
        $submitted_by = $information->submitted_by;
        $to = $information->to;
        $year = $information->year;
        $month = $information->month;
        $receive_date = $information->receive_date;
        $inquiry_title = $information->inquiry_title;
        $inquiry_no = $information->inquiry_no;
        $create_date = $information->create_date;
        $evidence_type = $information->evidence_type;
        $title = $information->title;
        $ref_no = $information->ref_no;
        $ref_date = $information->ref_date;
        $upload = $information->upload;
    }
}
?>
<body>
<form id="formID" class="formular" method="post" action="#" enctype="multipart/form-data">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div>
            <div class="columnA"><label><?php echo $inquiry_no; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="inquiry_no"
                                        value="<?php echo isset($result->inquiry_no) ? $result->inquiry_no : ''; ?>"
                                        disabled/></div>
            <div class="columnA" style="text-align:right;"><label><?php echo $create_date; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="create_date"
                                        value="<?php echo isset($result->create_date) ? $result->create_date : ''; ?>"
                                        disabled/></div>
            <div class="divclear"></div>
        </div>
        <div style="margin-top:1px;">
            <div class="columnA">
                <input name="inquiry_id" type='hidden'
                       value="<?php echo isset($result->inquiry_id) ? $result->inquiry_id : ''; ?>"/>
                <label><?php echo $receive_deadline_date; ?></label>
            </div>
            <div class="columnB">
                <input type="text" id="receive_deadline_date" name="receive_deadline_date"
                       class='validate[required,maxSize[80]] text-input bangla '
                       value="<?php echo isset($result->receive_deadline_date) ? $result->receive_deadline_date : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <?php //echo isset($result->remarks) ? $result->remarks: ''; ?>
            <div class="columnA"><label><?php echo 'Remarks'; ?></label></div>
            <div class="columnB">
                <textarea style="width:500px; height:50px;"
                          name="remarks"><?php echo isset($result->remarks) ? $result->remarks : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div style="width:90%;">
            <div class="columnA">
            </div>
            <div class="columnA" style="width: 100px;"></div>
            <div class="columnB"></div>
            <div class="columnA" style="width: 45px;"></div>
            <div class="columnB">
            </div>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="width:1200px;"><input type="button" name="basic" id="btn2" value="Add" class="newbutton"/></div>
    <div style="width:1200px; overflow:hidden; overflow-y:scroll; height:200px;" id="btn1">
        <div>
            <div style="float:left;width:11%;">
                <?php echo $evidence_type; ?>
            </div>

            <div style="float:left;width:11%;">
                <?php echo $submitted_by; ?>*
            </div>
            <div style="float:left;width:11%;">
                <?php echo $title; ?>*
            </div>
            <div style="float:left;width:11%;">
                <?php echo $receive_date; ?>*
            </div>
            <div style="float:left;width:11%;">
                <?php echo $ref_no; ?>
            </div>
            <div style="float:left;width:11%;">
                <?php echo $ref_date; ?>
            </div>
            <div style="float:left;width:11%;">
                <?php echo $upload; ?>
            </div>
            <div style="float:left;width:11%;">

            </div>
            <div style="float:left;width:11%;">

            </div>
            <div class="divclear"></div>
        </div>
        <?php
        if ($_GET['doc_reg_id'] != NULL) {
            //$doc_reg_id=isset($result->doc_reg_id) ? $result->doc_reg_id: '';
            $doc_reg_id = $_GET['doc_reg_id'];
            $user_id = $_SESSION['user_id'];
            echo $receive_evidance_info->gridviewDetails($doc_reg_id, $user_id);
        }
        ?>

    </div>
    </br>
    <input name="language_id" type="hidden" value="1"/>
    <input name="user_id" type="hidden" value="1"/>

    <div class="divclear"></div>
    </div>
    <div style="text-align:center; float:left; width:65%">
        <script>
            function myFunctionC() {
                window.close();
            }
        </script>

        &nbsp;&nbsp;&nbsp;&nbsp;
        <button onClick="myFunctionC()" class="popupbutton">Close</button>
        &nbsp;&nbsp;<input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
