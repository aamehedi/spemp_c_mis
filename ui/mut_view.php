<?php
session_start();
$user_id = $_SESSION['user_id'];
require_once('../model/multimedia_verbatim_info.php');
if ($_GET['transcript_id'] != NULL) {
    $result = $multimedia_verbatim_info->editrow(array($_GET['transcript_id'], $user_id));
    $file = $result->file_path;
    $ext = end(explode('.', $file));
    header('Content-Description: File Transfer');
    header('Content-Type: application/octet-stream');
    header('Content-Disposition: attachment; filename=' . $result->file_title . '.' . $ext);
    header('Content-Transfer-Encoding: binary');
    header('Expires: 0');
    header('Cache-Control: must-revalidate');
    header('Pragma: public');
    header('Content-Length: ' . filesize($file));
    ob_clean();
    flush();
    readfile($file);
}
?>
