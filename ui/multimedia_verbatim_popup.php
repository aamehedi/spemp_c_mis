<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/multimedia_verbatim_info.php');
require_once('../model/meeting_notice_info.php');

$user_id = $_SESSION['user_id'];

if ($_GET['metting_notice_id'] != NULL) {
    $result = $meeting_notice_info->editrow(array($_GET['metting_notice_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link type='text/css' href='css/demo.css' rel='stylesheet' media='screen'/>
    <!--	<link type='text/css' href='css/menu.css' rel='stylesheet' media='screen' />-->
    <link type='text/css' href='css/grid.css' rel='stylesheet' media='screen'/>

    <style>
        #overlay_form {
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }

        .myfileupload-buttonbar {
            float: left;
            width: 20%;
        }

        .myfileupload-buttonbar input {
            position: absolute;
            top: 0;
            right: 0;
            margin: 0;
            border: solid transparent;
            border-width: 0 0 100px 200px;
            opacity: 0.0;
            filter: alpha(opacity=0);
            -o-transform: translate(250px, -50px) scale(1);
            -moz-transform: translate(-300px, 0) scale(4);
            direction: ltr;
            cursor: pointer;
        }

        .myui-button {
            position: relative;

            cursor: pointer;
            text-align: center !important;
            overflow: visible;
            /*background-color: red;*/
            overflow: hidden;
            /*width: 10%;*/
            max-height: 22px;
            padding-top: 5px;
            /*padding-bottom: 2px;*/

            background-color: #759ae9;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #759ae9), color-stop(50%, #376fe0), color-stop(50%, #1a5ad9), color-stop(100%, #2463de));
            background-image: -webkit-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
            background-image: -moz-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
            background-image: -ms-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
            background-image: -o-linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
            background-image: linear-gradient(top, #759ae9 0%, #376fe0 50%, #1a5ad9 50%, #2463de 100%);
            border-top: 1px solid #1f58cc;
            border-right: 1px solid #1b4db3;
            border-bottom: 1px solid #174299;
            border-left: 1px solid #1b4db3;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
            box-shadow: inset 0 0 2px 0 rgba(57, 140, 255, 0.8);
            color: #fff;
            font: bold 12px/1 "helvetica neue", helvetica, arial, sans-serif;
            padding: 5px 0;
            text-shadow: 0 -1px 1px #1a5ad9;
            /*display: block !important;*/
            /*width: 150% !important;*/
            display: block;
            width: 90%;

        }

        }
        .myui-button:hover {
            background-color: #5d89e8;
            background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #5d89e8), color-stop(50%, #2261e0), color-stop(50%, #044bd9), color-stop(100%, #0d53de));
            background-image: -webkit-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
            background-image: -moz-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
            background-image: -ms-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
            background-image: -o-linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
            background-image: linear-gradient(top, #5d89e8 0%, #2261e0 50%, #044bd9 50%, #0d53de 100%);
            cursor: pointer;
        }

        .myui-button:active {
            border-top: 1px solid #1b4db3;
            border-right: 1px solid #174299;
            border-bottom: 1px solid #133780;
            border-left: 1px solid #174299;
            -webkit-box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
            box-shadow: inset 0 0 5px 2px #1a47a0, 0 1px 0 #eeeeee;
        }

        /*.myui-button span{*/
        /*text-align: center;*/
        /*margin-top: 10px;*/
        /*min-width: 100%;*/

        /*}*/
    </style>
    <!--	<script type='text/javascript' src='js/jquery.js'></script>-->

    <!--	<script type='text/javascript' src='js/jquery.simplemodal.js'></script>-->
    <!--	<script type="text/javascript" src="js/player/flowplayer.min.js"></script>-->
    <script>
        function formSubmit() {
            document.getElementById("overlay_form").submit();
        }
    </script>
</head>
<?php
require_once("../model/multimedia_verbatim_info.php");
?>
<?php
$xml = simplexml_load_file("xml/multimedia_verbatim_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
        $result_of_deliberation = $information->result_of_deliberation;
        $chair = $information->chair;
    }
}
?>
<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">
<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script>
    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
    });

    function checkTitle(field, rules, i, options) {
        var uploadTitles = jQuery('tr > td:nth-child(1)');
        var uploadFileNames = [];
        for (i = 0; i < uploadTitles.length; ++i) {
            uploadFileNames.push(uploadTitles[i].innerHTML);
        }
        if (uploadFileNames.indexOf(field[0].value) >= 0) {
            return 'Same title already exists';
        }
    }
</script>
<script>
    function viewid(p) {
        window.showModalDialog("mut_view.php?transcript_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:20px;dialogHeight:20px")
    }
</script>
<!--Validation End-->
<body>
<form id="formID" class="formular" name="overlay_form" method="post" action="#" enctype="multipart/form-data">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div>
        <div style="width:58%; float:left;">
            <div>
                <input type="hidden" name="metting_notice_id"
                       value="<?php echo isset($result->metting_notice_id) ? $result->metting_notice_id : ''; ?>"/>

                <div style="width:15%; float:left;"><?php echo $ref_no; ?></div>
                <div style="width:40%; float:left;"><input id="name" type="text" name="p_gov_no_pix" style="width:95%;"
                                                           value="<?php echo $result->gov_no_pix . '/' . $result->gov_no_postfix ?>"
                                                           disabled="disabled"/></div>
                <div style="width:15%; float:left;"><?php echo $sitting_no; ?></div>
                <div style="width:20%; float:left;"><input id="name" type="text"
                                                           value="<?php echo isset($result->sitting_no) ? $result->sitting_no : ''; ?>"
                                                           disabled="disabled" style="width:70%;"/></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:15%; float:left;"><?php echo $date; ?></div>
                <div style="width:20%; float:left;"><input type="text" name="en_date"
                                                           value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>"
                                                           disabled="disabled" style="width:94%;"/></div>
                <div style="width:20%; float:left;"><input id="name" type="text" style="width:94%;" name="en_date"
                                                           value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>"
                                                           disabled="disabled"/></div>
                <div style="width:15%; float:left;"><?php echo $time; ?></div>
                <div style="width:20%; float:left;"><input id="name" type="text" name="time" style="width:70%;"
                                                           value="<?php echo isset($result->time) ? $result->time : ''; ?>"
                                                           disabled="disabled"/></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:15%; float:left;"><?php echo $venue; ?></div>
                <div style="width:85%; float:left;"><input id="name" type="text" name="parliament_id" style="width:94%;"
                                                           value="<?php echo isset($result->venue_name) ? $result->venue_name : ''; ?>"
                                                           disabled="disabled"/></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:15%; float:left;"><?php echo $committee; ?></div>
                <div style="width:85%; float:left;"><input id="name" type="text" name="parliament_id" style="width:94%;"
                                                           value="<?php echo $_SESSION['committee_name']; ?>"
                                                           disabled="disabled"/></div>
                <div class="divclear"></div>
            </div>
        </div>
        <div style="width:40%; float:left; ">
            <input type="hidden" name="transcript_id" value="0"/>
            File Name: *&nbsp;
            <input class="validate[required,funcCall[checkTitle]]" name="file_title" type="text" style="width:100px;"/>

            <div class="myfileupload-buttonbar">
                <label class="myui-button">Add Files<input type="file" name="file_name[]" id="file"
                                                           class="validate[required]"/></label>
            </div>
            <!--			  <input  type="file" class="validate[required]" name="file_name[]"/>-->
            <br/>
            <br/>
            <!--<input type="button" onclick="formSubmit()" value="Upload" name="upload">-->
            <input type="submit" value="Upload" name="upload" style="width: 19%"/>
        </div>
        <div class="divclear"></div>
    </div>
    <div style="margin-top:10px;">
        <div style="width:55%; float:left;">
            <div id="mediaplayer" style="width:650px;height:400px; background:#000000;">No video selected.</div>
            <script type="text/javascript" src="js/player/jwplayer.js"></script>
            <script type="text/javascript">
                function play(p) {
                    jwplayer("mediaplayer").setup({
                        flashplayer: "js/player/player.swf",
                        file: p,
                        image: "js/player/preview.jpg"
                    });

                }
                function deleteid(t, m) {
                    //alert('Are You Want To Delete');
                    var r = confirm("Are You Want To Delete!");
                    if (r == true) {
                        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                            xmlhttp = new XMLHttpRequest();
                        }
                        else {// code for IE6, IE5
                            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        xmlhttp.onreadystatechange = function () {
                            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                            }
                        }
                        xmlhttp.open("GET", "multimedia_verbatim_data.php?transcript_id=" + t + "&metting_notice_id=" + m, true);
                        xmlhttp.send();
                    }
                }
            </script>
        </div>
        <div style="width:40%; float:left; margin-left:8px;">
            <div id="txtHint">
                <?php
                $metting_notice_id = $_GET['metting_notice_id'];
                $user_id = $_SESSION['user_id'];
                $multimedia_verbatim_info->gridview($metting_notice_id, $user_id);
                ?>
            </div>
        </div>
        <div class="divclear"></div>
    </div>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

    <div>
        <div class="columnAbutton">
        </div>
        <div class="columnBbutton">

        </div>
        <div class="divclear"></div>
    </div>
    <div class="divclear"></div>
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <button onClick="window.close();" class="popupbutton">Close</button>
</form>

</body>
</html>
