<?php
session_start();

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('tcpdf/tcpdf.php');
require_once('../model/create_inquiry_info.php');
require_once('MYPDF.php');

$user_id = $_SESSION['user_id'];
if ($_GET['inquiry_id'] != NULL) {
    $result = $create_inquiry_info->editreport(array($_GET['inquiry_id']));
}
// var_dump($result);
// die();

$confData = array(
    'pageTitle' => 'Manage Inquiry Report',
    'committeeName' => $result->committee_name,
    'parliamentName' => $result->parliament,
    'reportName' => 'NOTICE OF INQUIRY',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => true
);

// var_dump($confData);
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);


$pdf->AddPage();
if ($result->is_issued === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

$pdf->SetFont($pdf->fontName, '', 12);
// if ($_SESSION['language_id'] === '1') {

// }else{

// 	////////////////////
// }

$pdf->Cell(0, 5, '', 0, 1, 'C', false, '', 0, false, 'C', 'B');
$html = '<div style="margin:5px;">';
$html .= '<b>Inquiry No.:</b>';
$html .= htmlspecialchars_decode($result->inquiry_no) . '<br>';
$html .= '<b>Inquiry Title:</b> ' . htmlspecialchars_decode($result->inquiry_title) . '<br>';
$html .= '<b>Create Date:</b> ' . htmlspecialchars_decode($result->create_date) . '<br>';
$html .= '<b>Issued Date:</b> ' . htmlspecialchars_decode($result->issued_date) . '<br>';
$html .= '<b>Proposed Date:</b> ' . htmlspecialchars_decode($result->proposed_date) . '<br>';
$html .= '<b>Document(s):</b> ' . htmlspecialchars_decode($result->doc_title) . '<br>';
$html .= '<b>Ministry(s):</b> ' . htmlspecialchars_decode($result->ministry_name) . '<br>';
$html .= '</div>';

$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// var_dump(K_PATH_IMAGES);
// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
if ($result->is_issued === '0') {
    $pdf->Output('pdf_rpt_create_inquiry_' . $result->inquiry_id . '.pdf', 'I');
} else {
    $pdf->Output('issued_approved_pdf/pdf_rpt_create_inquiry_' . $result->inquiry_id . '.pdf', 'F');
    echo "<script>window.close();</script>";
}
//============================================================+
// END OF FILE
//============================================================+
