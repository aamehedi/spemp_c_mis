<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        /*$('<div></div>').load('committee_member_popup.php?id='+p).modal();*/
        window.showModalDialog("committee_member_popup_n.php?id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:480px");
        return false;
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
    }
</script>

<?php
require_once('../model/committee_member_info.php');
?>
<?php
$xml = simplexml_load_file("xml/committee_member_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $id = $information->id;
        $organization_member_name = $information->organization_member_name;
        $organization_name = $information->organization_name;
        $comments = $information->comment;
        $designation = $information->designation;
        $phone = $information->phone;
        $cell_phone = $information->cell_phone;
        $fax = $information->fax;
        $email = $information->email;
        $address = $information->address;
    }
}
?>

<div id="head_info">
    <?php
    echo $heading; //echo $parliament_information;
    ?>
</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committee_id = $_SESSION['committee_id'];
    $param = array($organization_member_name, $designation);
    echo $committee_member_info->gridview($committee_id, $language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>