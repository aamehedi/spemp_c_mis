<?php
session_start();

//echo $_SESSION['language_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
require_once('../model/parliament_member_info.php');
//require_once('../dal/data_access.php');

//echo  $_GET['parliament_id'];
$user_id = $_SESSION['user_id'];
//echo $_GET['ministry_id'];

if ($_GET['member_id'] != NULL) {
    $result = $parliament_member_info->editrow(array($_GET['member_id'], $user_id));
    $resultUser = $parliament_member_info->getNotAddedUser(empty($result->user_id) ? '0' : $result->user_id);
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <link rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css" type="text/css">
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = (array)simplexml_load_file("xml/parliament_member_popup.xml");
    $information = $xml['information'][$_SESSION['language_id'] - 1];

    $heading = $information->heading;
    $headingpopup = $information->headingpopup;
    $id = $information->id;
    $user_name = $information->user_name;
    $ministry_name = $information->ministry_name;
    $contact_person = $information->contact_person;
    $designation = $information->designation;
    $seat_no = $information->seat_no;
    $constituency = $information->constituency;
    $phone = $information->phone;
    $cell_phone = $information->cell_phone;
    $fax = $information->fax;
    $email = $information->email;
    $current_address = $information->current_address;
    $permanent_address = $information->permanent_address;
    $comments = $information->comments;
    $is_active = $information->is_active;
    $active = $information->active;
    $inactive = $information->inactive;
    $part1 = $information->part1;
    $part2 = $information->part2;
    $url = $information->url;
    $sex = $information->sex;
    $male = $information->male;
    $female = $information->female;
    $nid = $information->nid;
    $election_date = $information->election_date;
    $party_name = $information->party_name;
    $dob = $information->dob;
    $birth_place = $information->birth_place;
    $fathers_name = $information->fathers_name;
    $mothers_name = $information->mothers_name;
    $marital_status = $information->marital_status;
    $married = $information->married;
    $unmarried = $information->unmarried;
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
            jQuery("#election_date").datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
            jQuery("#dob").datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">
<form id="formID" class="formular" name="overlay_form" method="post" action="#" style="width: 900px;">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <fieldset>
        <legend><?php echo $part1; ?></legend>


        <div style="width:820px;">
            <div style="display:none;">
                <div class="columnA"><label><?php echo $id; ?> </label></div>
                <div class="columnB"><input id="name" type="text" name="member_id"
                                            value="<?php echo isset($result->member_id) ? $result->member_id : '[Auto]'; ?>"
                                            readonly="true"/></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $ministry_name; ?> *</label></div>
                <div class="columnB"><input type='text' class='validate[maxSize[150]] text-input bangla'
                                            style="width: 264%;" name='member_name'
                                            value="<?php echo isset($result->member_name) ? $result->member_name : ''; ?>"
                                            style="font-size: 11px;width: 450px;"></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $designation; ?> *</label></div>
                <div class="columnB">
                    <div style="float:left;">
                        <input type='text' name='designation' class='validate[maxSize[80]] text-input bangla'
                               value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                    </div>
                    <div style="float:left;width:100px;padding-left:30px;">
                        <?php echo $seat_no; ?> *
                    </div>
                    <div style="float:left;">
                        <input type='text' name='seat_no' class='validate[max[330],min[1],integer] text-input bangla'
                               value="<?php echo isset($result->seat_no) ? $result->seat_no : ''; ?>">
                    </div>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $constituency; ?></label></div>
                <div class="columnB">
                    <div style="float:left;">
                        <input type='text' name='constituency' class='validate[maxSize[100]] text-input bangla'
                               value="<?php echo isset($result->constituency) ? $result->constituency : ''; ?>"></input>
                    </div>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $phone; ?></label></div>
                <div class="columnB">
                    <div style="float:left;">
                        <input type='text' name='phone' class='text-input bangla'
                               value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                    </div>
                    <div style="float:left;width:100px;padding-left:30px;">
                        <?php echo $cell_phone; ?> *
                    </div>
                    <div style="float:left;">
                        <input type='text' name='cell_phone' class='validate[required] text-input bangla'
                               value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"></input>
                    </div>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $fax; ?></label></div>
                <div class="columnB">
                    <div style="float:left;">
                        <input type='text' class='text-input bangla' name='fax'
                               value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                    </div>
                    <div style="float:left;width:100px;padding-left:30px;">
                        <?php echo $email; ?>
                    </div>
                    <div style="float:left;">
                        <input type='text' class='validate[required,maxSize[150],custom[email]] text-input bangla'
                               name='email' value="<?php echo isset($result->email) ? $result->email : ''; ?>"></input>
                    </div>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $current_address; ?></label></div>
                <div class="columnB">
                    <textarea class='validate[maxSize[250]] text-input bangla' name="current_address"
                              style="font-size: 12px;width: 450px;"><?php echo isset($result->current_address) ? $result->current_address : ''; ?></textarea>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $permanent_address; ?></label></div>
                <div class="columnB">
                    <textarea class='validate[maxSize[250]] text-input bangla' name="permanent_address"
                              style="font-size: 12px;width: 450px;"><?php echo isset($result->permanent_address) ? $result->permanent_address : ''; ?></textarea>
                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div class="columnA"><label><?php echo $comments; ?></label></div>
                <div class="columnB"><textarea type='text' class='validate[maxSize[250]] text-input bangla'
                                               name='comments'
                                               style="font-size: 12px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
                </div>
                <div class="divclear"></div>
            </div>

            <div>
                <div class="columnA"><label><?php echo $url; ?></label></div>
                <div class="columnB"><input type='text' class='text-input bangla' style="width: 264%;" name='url'
                                            value="<?php echo isset($result->url) ? $result->url : ''; ?>"
                                            style="font-size: 11px;width: 450px;"></div>
                <div class="divclear"></div>
            </div>
    </fieldset>
    <br/>
    <fieldset>
        <legend><?php echo $part2; ?></legend>

        <div>
            <div class="columnA"><label><?php echo $nid; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type="text" name="nid" class="text-input bangla"
                           value="<?php echo isset($result->nid) ? $result->nid : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $election_date; ?>
                </div>
                <div style="float:left;">
                    <input type="text" name="election_date" id="election_date" class="text-input bangla"
                           value="<?php echo isset($result->election_date) ? substr($result->election_date, 0, 10) : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $party_name; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type="text" name="party_name" class="text-input bangla"
                           value="<?php echo isset($result->party_name) ? $result->party_name : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $dob; ?>
                </div>
                <div style="float:left;">
                    <input type="text" name="dob" id="dob" class="text-input bangla"
                           value="<?php echo isset($result->dob) ? substr($result->dob, 0, 10) : ''; ?>"/>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $birth_place; ?></label></div>
            <div class="columnB"><input type='text' class='text-input bangla' style="width: 264%;" name='birth_place'
                                        value="<?php echo isset($result->birth_place) ? $result->birth_place : ''; ?>"
                                        style="font-size: 11px;width: 450px;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fathers_name; ?></label></div>
            <div class="columnB"><input type='text' class='text-input bangla' style="width: 264%;" name='fathers_name'
                                        value="<?php echo isset($result->fathers_name) ? $result->fathers_name : ''; ?>"
                                        style="font-size: 11px;width: 450px;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $mothers_name; ?></label></div>
            <div class="columnB"><input type='text' class='text-input bangla' style="width: 264%;" name='mothers_name'
                                        value="<?php echo isset($result->mothers_name) ? $result->mothers_name : ''; ?>"
                                        style="font-size: 11px;width: 450px;"></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $marital_status; ?></label></div>
            <div class="columnB">
                <?php
                $marital_status_value = isset($result->marital_status) ? $result->marital_status : 0;
                if ($marital_status_value == 0) {
                    echo '<input type="radio" name="marital_status" value="0" style="display:inline;" checked> ' . $unmarried . '&nbsp;&nbsp;';
                    echo '<input type="radio" name="marital_status" value="1" style="display:inline;" > ' . $married;
                } elseif ($marital_status_value == 1) {
                    echo '<input type="radio" name="marital_status" value="0" style="display:inline;"> ' . $unmarried . '&nbsp;&nbsp;';
                    echo '<input type="radio" name="marital_status" value="1" style="display:inline;" checked> ' . $married;
                }
                ?>
            </div>
            <div class="divclear"></div>
        </div>
    </fieldset>

    </br>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>


    <input name="user_id" type="hidden" value="' '"/>

    <div>
        <div class="columnAbutton">
            <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

        </div>
        <div class="columnBbutton">
            <script>
                function myFunction() {
                    window.close();
                }
            </script>
            &nbsp;&nbsp;
            <button onClick="myFunction()" class="popupbutton">close</button>
        </div>
        <div class="divclear"></div>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
