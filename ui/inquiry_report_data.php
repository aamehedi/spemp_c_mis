<?php
session_start();
?>
<?php
require_once('../model/create_inquiry_info.php');
require_once('../dal/data_access.php');

$user_id = $_SESSION['user_id'];;
if ($_POST['inquiry_id'] != NULL) {
    $result = $create_inquiry_info->editrow(array($_POST['inquiry_id'], $user_id));
}
$data = array(isset($result->inquiry_title) ? 'Report of ' . $result->inquiry_title : '');
$data_access = new data_access;
$deliberations = $data_access->data_reader('tbl_record_of_decision_by_inquiry', array($_POST['inquiry_id']));
while($deliberation = mysqli_fetch_object($deliberations)){
    array_push($data, $deliberation->result_of_deliberation);
}
echo json_encode($data);
?>