<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/meeting_notice_info.php');
if ($_GET['meeting_notice_id'] != NULL) {
    $result = $meeting_notice_info->rpt_meeting_notice(array($_GET['meeting_notice_id']));
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Meeting Notice Report</title>
    <?php else: ?>
        <title>সভা নোটিশ প্রতিবেদন</title>
    <?php endif; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
</head>

<body>
<?php if ($result->is_issued === '1') : ?>
<div style="margin:10px; width:80%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:80%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat; background-size: 200px 180px"
        id="message_area">
        <?php endif; ?>
        <div>
            <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            <div style="float:left; height:80px; padding-top:25px; padding-left:30px;font-weight: bold;">
                <div style="font-size:18px;"><?php echo $result->committee_name; ?></div>
                <?php if (isset($language_id) && $language_id === '1'): ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">Parliament of Bangladesh</div>
                <?php else: ?>
                    <div style="font-size:18px; text-align:center;font-weight: bold;">বাংলাদেশ জাতীয় সংসদ</div>
                <?php endif; ?>
            </div>
            <div style="clear:both;"></div>
        </div>

        <br/>
        <?php if (isset($language_id) && $language_id === '1'): ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">NOTICE OF MEETING</div>
        <?php else: ?>
            <div style="font-size:24px;text-align:center;font-weight: bold;">সভার বিজ্ঞপ্তি</div>
        <?php endif; ?>

        <div style="margin:5px;">
            <?php
            $time = strtotime($result->en_date);
            $newformat = date('Y-m-d', $time);
            if (isset($language_id) && $language_id === '1') {
                echo '<b>Date:</b>&nbsp;' . $newformat . '&nbsp;';
                echo '<label style="font-size:18px;">&nbsp;' . $result->bn_date . '</label>' . '<br>';
                echo '<b>Time:</b>&nbsp;' . htmlspecialchars_decode($result->time) . '<br>';
                echo '<b>Venue:</b>&nbsp;' . htmlspecialchars_decode($result->venue_name) . '<br>';
                echo '<b>ITEMS</b><br/>';
                echo '<div style="margin:5px;">';
                echo '<br><b>Private Business</b><br/>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;' . htmlspecialchars_decode($result->private_business_before) . '<br>';
                echo '<br><b>Public Business</b><br/>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;' . htmlspecialchars_decode($result->public_business) . '<br>';
                echo '<br><b>Private Business </b><br/>';
                echo '<div style="padding-left:12px;">' . htmlspecialchars_decode($result->private_business_after) . '</div><br>';
                echo '</div>';
            } else {
                echo '<b>তারিখ:</b>&nbsp;' . $newformat . '&nbsp;';
                echo '<label style="font-size:18px;">&nbsp;' . $result->bn_date . '</label>' . '<br>';
                echo '<b>সময়:</b>&nbsp;' . htmlspecialchars_decode($result->time) . '<br>';
                echo '<b>সভাস্থান:</b>&nbsp;' . htmlspecialchars_decode($result->venue_name) . '<br>';
                echo '<b>অনুচ্ছেদ</b><br/>';
                echo '<div style="margin:5px;">';
                echo '<br><b>সাধারণ আলোচ্য বিষয়</b><br/>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;' . htmlspecialchars_decode($result->private_business_before) . '<br>';
                echo '<br><b>প্রকাশ্য আলোচ্য বিষয়</b><br/>';
                echo '&nbsp;&nbsp;&nbsp;&nbsp;' . htmlspecialchars_decode($result->public_business) . '<br>';
                echo '<br><b>সাধারণ আলোচ্য বিষয়</b><br/>';
                echo '<div style="padding-left:12px;">' . htmlspecialchars_decode($result->private_business_after) . '</div><br>';
                echo '</div>';
            }


            ?>
        </div>
    </div>
    <?php if ($result->is_issued !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
