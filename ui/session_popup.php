<?php
session_start();
//echo $_SESSION['user_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
$xml = simplexml_load_file("xml/session_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $id = $information->id;
        $parliament = $information->parliament;
        $session = $information->session;
        //echo $heading;
    }
}
?>
<?php
require_once('../model/session_info.php');
require_once('../model/parliament_info.php');
$user_id = $_SESSION['user_id'];
if (!isset($_GET['parliament_id'])) {
    if ($_GET['session_id'] != NULL) {
        $result = $session_info->editrow(array($_GET['session_id']));
    }
} else {
    $result2 = $parliament_info->editrow(array($_GET['parliament_id']));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>

    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">
<form id="formID" class="formular" name="overlay_form" method="post" action="<?= $_SERVER['PHP_SELF'] ?>">
    <h2><?php echo $heading ?></h2>
    </br>
    <div>
        <div>
            <div class="columnA"><label><?php echo $parliament ?>  </label></div>
            <input name="parliament_id" type="hidden"
                   value="<?php echo isset($result2->parliament_id) ? $result2->parliament_id : $result->parliament_id; ?>">

            <div class="columnB"><input id="name" type="text" name="parliament_name"
                                        value="<?php echo isset($result->parliament) ? $result->parliament : $result2->parliament; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id ?> </label></div>
            <div class="columnB"><input id="name" type="text" name="session_id"
                                        value="<?php echo isset($result->session_id) ? $result->session_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $session ?> *</label></div>
            <div class="columnB"><input id="name" type="text" name="session"
                                        class='validate[required,maxSize[80]] text-input bangla'
                                        value="<?php echo isset($result->session) ? $result->session : ''; ?>"/></div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save_session" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
    </div>
</form>
</body>
</html>
