/*
Navicat MySQL Data Transfer

Source Server         : 192.168.3.20_3306
Source Server Version : 50508
Source Host           : 192.168.3.20:3306
Source Database       : dbmis

Target Server Type    : MYSQL
Target Server Version : 50508
File Encoding         : 65001

Date: 2014-02-17 20:21:30
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_briefing_note`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_briefing_note`;
CREATE TABLE `tbl_briefing_note` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `introduction` longtext,
  `key_issue` longtext,
  `advisor_question` longtext,
  `witness_question` longtext,
  `written_evidence` longtext,
  `remarks` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_briefing_note
-- ----------------------------
INSERT INTO `tbl_briefing_note` VALUES ('5', '   gfgfg   &lt;div&gt;aas&lt;/div&gt;&lt;div&gt;assa&lt;/div&gt;&lt;div&gt;saas&lt;/div&gt;&lt;div&gt;asa&lt;/div&gt;&lt;div&gt;sas&lt;/div&gt;&lt;div&gt;a&lt;/div&gt;&lt;div&gt;sas&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;sa&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;asa&lt;/div&gt;&lt;div&gt;s&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;sa&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;as&lt;/div&gt;&lt;div&gt;s&lt;/div&gt;&lt;div&gt;sasa&lt;/div&gt;', '&lt;span style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;gfgfg&lt;/span&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;aas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;assa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;saas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;asa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;a&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;asa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;s&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;s&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sasa&lt;/div&gt;', '&lt;span style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;gfgfg&lt;/span&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;aas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;assa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;saas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;asa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;a&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sas&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;asa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;s&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sa&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;as&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;s&lt;/div&gt;&lt;div style=&quot;font-family: tahoma, verdana, sans-serif; font-size: 12px;&quot;&gt;sasa&lt;/div&gt;', 'fgfgfg', 'fgfggf', '			    	fgfgfgfg		    			  			  ', '1', '1', '0', '2014-02-02 15:42:05', '0', '1');
INSERT INTO `tbl_briefing_note` VALUES ('22', '         &lt;ol&gt;&lt;li&gt;&lt;font face=&quot;tahoma, verdana, sans-serif&quot;&gt;&lt;span style=&quot;background-color: rgb(236, 236, 236);&quot;&gt;&lt;b&gt;ekjhsadkj sdkjhf dshjkds fjsdhfkjdshjk&lt;/b&gt;&lt;/span&gt;&lt;/font&gt;&lt;/li&gt;&lt;li&gt;&lt;br&gt;&lt;/li&gt;&lt;/ol&gt;', '&lt;b style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Key Issue&lt;/b&gt;&lt;br&gt;', '&lt;b style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Possible Questions for Special Advisors&lt;/b&gt; dfgdgfd&lt;br&gt;', '&lt;b style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Possible Questions for Witness&lt;/b&gt;&lt;br&gt;', '&lt;b style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Written Evidence&lt;/b&gt;&lt;br&gt;', '			    			    			    			    Remarks			    			  			  			  			  			  ', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_briefing_note` VALUES ('28', 'kdfj sdlk fsdklfjsdlkfjsdkljflksdjflksdj lk', '&lt;br&gt;', '&lt;br&gt;', '&lt;br&gt;', '&lt;br&gt;', '			    			  ', '1', '1', '0', null, '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee`;
CREATE TABLE `tbl_committee` (
  `committee_id` smallint(6) DEFAULT NULL,
  `committee_name` varchar(200) DEFAULT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `section_id` smallint(6) DEFAULT NULL,
  `branch_id` smallint(6) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee
-- ----------------------------
INSERT INTO `tbl_committee` VALUES ('1', 'Public Account Committee', 'PAC', '1', '1', 'saker', 'ED', '02558989', '018455236', '88029966325', 'saker@yahho.com', 'X', 'X', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('2', 'à¦ªà¦¾à¦¬à¦²à¦¿à¦• à¦…à§à¦¯à¦¾à¦•à¦¾à¦‰à¦¨à§à¦Ÿ à¦•à¦®à¦¿à¦Ÿà¦¿', 'à¦ªà¦¿à¦à¦šà¦¿', '2', '2', 'à§«à§¬à§¬à§«à§¬à§¬à§«à§¬', 'à§«à§¬à§«à§¬', '12121212121', '121212121212', '121212121', 'y@g.com', 'X', 'X', '2', '0', '1');
INSERT INTO `tbl_committee` VALUES ('3', 'Estimates Committee', 'EC', '1', '1', 'sdfsd', 'sdfsd', '', '', '', '', '', '', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('5', 'Public Undertakings Committee', 'PUC', '1', '1', 'X', 'X', '', '', '', '', '', '', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('6', 'fgdfg', 'cvb', '1', '1', 'gdfg', 'dfg', '5646', '456645', '7676', 'saker@yahho.com', '', '', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('7', 'My Committee', 'MC', '1', '1', 'mehedi', 'nothing', '', '', '', '', '', '', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_branch`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_branch`;
CREATE TABLE `tbl_committee_branch` (
  `branch_id` smallint(6) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_branch
-- ----------------------------
INSERT INTO `tbl_committee_branch` VALUES ('1', 'Branch 1', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('2', 'à¦¶à¦¾à¦–à¦¾ -à§§', '2', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('3', 'Branch 2', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_member`;
CREATE TABLE `tbl_committee_member` (
  `committee_member_id` int(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_member
-- ----------------------------
INSERT INTO `tbl_committee_member` VALUES ('1', '1', 'mqwerwerwerwerer', 'd', '158578774', '5445454554', '35464646', 'saker@yahho.com', '																								a																								', '																								c																								', '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('2', '1', 'X', 'X', '555555555555', '55555555555', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('3', '2', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦…à¦§à§€à¦¨à§‡ à¦•à¦®à¦¿à¦Ÿà¦¿ à¦¸à¦šà¦¿à¦¬à¦¾à¦²à¦¯à¦¼', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦…à¦§à§€à¦¨à§‡ à¦•à¦®à¦¿à¦Ÿà¦¿ à¦¸à¦šà¦¿à¦¬à¦¾à¦²à¦¯à¦¼', '012121212', '1212121', '', '', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿																', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿																', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_officer`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_officer`;
CREATE TABLE `tbl_committee_officer` (
  `officer_id` int(11) DEFAULT NULL,
  `officer_name` varchar(250) DEFAULT NULL,
  `branch_id` smallint(6) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_officer
-- ----------------------------
INSERT INTO `tbl_committee_officer` VALUES ('1', 'Xqasda asdasdasdasd', '1', 'Dqwwww', '014454544', '254132456554', '254445', 'sa@y.com', '												a												', '												c												', '1', '0', '1');
INSERT INTO `tbl_committee_officer` VALUES ('2', 'Xqasda asdasdasdasd', '1', 'd1111', '6545454545', '545445', '5544554', 'sa_sarker@yahoo.com', '							aaas									', '												asasasassa				', '1', '0', '1');
INSERT INTO `tbl_committee_officer` VALUES ('3', 'à¦¹à¦¾', '2', 'à¦¹à¦¾à¦¹à¦¾', '012121212', '', '', '', '								', '								', '2', '0', '1');
INSERT INTO `tbl_committee_officer` VALUES ('4', 'VV', '1', 'VV', '000000000000000', '00000000000000', '', '', '								', '								', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_officer_committee_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_officer_committee_tag`;
CREATE TABLE `tbl_committee_officer_committee_tag` (
  `officer_id` int(11) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_officer_committee_tag
-- ----------------------------
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('2', '5', '0', '1');
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('1', '5', '0', '1');
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('3', '2', '0', '1');
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('2', '1', '0', '1');
INSERT INTO `tbl_committee_officer_committee_tag` VALUES ('4', '3', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_section`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_section`;
CREATE TABLE `tbl_committee_section` (
  `section_id` smallint(6) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_section
-- ----------------------------
INSERT INTO `tbl_committee_section` VALUES ('1', 'PUC', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('2', 'à¦¬à¦¿à¦­à¦¾à¦— -à§§', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_document_key_areas`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_key_areas`;
CREATE TABLE `tbl_document_key_areas` (
  `doc_reg_id` int(11) DEFAULT NULL,
  `key_area` longtext,
  `recommendations` longtext,
  `result_forcasting` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_key_areas
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_document_registration_details`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_registration_details`;
CREATE TABLE `tbl_document_registration_details` (
  `doc_reg_detail_id` bigint(20) DEFAULT NULL,
  `doc_reg_id` int(11) DEFAULT NULL,
  `doc_title` varchar(200) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `doc_type` varchar(50) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_registration_details
-- ----------------------------
INSERT INTO `tbl_document_registration_details` VALUES ('1', '1', 'd1', 'document/1.docx', 'application/vnd.openxmlformats-officedocument.word', '0', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('2', '2', 'raihan', 'document/2.PDF', 'application/pdf', '1', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('3', '1', 'sdfsdf', 'document/3.docx', 'application/vnd.openxmlformats-officedocument.word', '0', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('4', '3', 'd1', 'document/4.docx', 'application/vnd.openxmlformats-officedocument.word', '0', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('5', '3', 'd2', 'document/5.jpeg', 'image/jpeg', '0', '1');

-- ----------------------------
-- Table structure for `tbl_document_registration_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_registration_master`;
CREATE TABLE `tbl_document_registration_master` (
  `doc_reg_id` int(11) DEFAULT NULL,
  `doc_reg_no` varchar(50) DEFAULT NULL,
  `doc_reg_date` datetime DEFAULT NULL,
  `doc_title` text,
  `ref_no` varchar(150) DEFAULT NULL,
  `ref_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_registration_master
-- ----------------------------
INSERT INTO `tbl_document_registration_master` VALUES ('1', '0120130001', '2013-11-12 00:00:00', 'Title', 'ref-001', '2013-11-12 00:00:00', '2013-11-13 10:32:47', '1', '1', '0', '1');
INSERT INTO `tbl_document_registration_master` VALUES ('2', '0320130001', '2013-11-13 00:00:00', 'raihan', '1', '2013-11-06 00:00:00', '2013-11-17 16:58:26', '1', '3', '0', '1');
INSERT INTO `tbl_document_registration_master` VALUES ('3', '0120140002', '2014-01-16 00:00:00', 'hfghfgh', '5645', '2014-01-16 00:00:00', '2014-01-15 18:52:24', '1', '1', '0', '1');
INSERT INTO `tbl_document_registration_master` VALUES ('4', '0120140003', '2014-02-10 00:00:00', 'Document Title 1', '', '2014-02-10 00:00:00', '2014-02-09 20:11:03', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_download_path`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_download_path`;
CREATE TABLE `tbl_download_path` (
  `download_path` varchar(250) DEFAULT NULL,
  `is_active` smallint(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_download_path
-- ----------------------------
INSERT INTO `tbl_download_path` VALUES ('http://localhost/mis/ui/', '1');

-- ----------------------------
-- Table structure for `tbl_follow_up_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_follow_up_master`;
CREATE TABLE `tbl_follow_up_master` (
  `inquiry_report_followup_id` bigint(20) DEFAULT NULL,
  `inquiry_report_id` int(11) DEFAULT NULL,
  `followup_no` varchar(50) DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `background` longtext,
  `remarks` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_approved` smallint(6) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_follow_up_master
-- ----------------------------
INSERT INTO `tbl_follow_up_master` VALUES ('1', '3', 'à§¦à§¨à§¨à§¦à§§à§©à§¦à§¦à§¦à§§', '2013-11-22 00:00:00', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶', '		à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶	    			  ', '2', '2', '0', null, '0', '1');
INSERT INTO `tbl_follow_up_master` VALUES ('2', '1', '0120130001', '2013-12-21 00:00:00', '&lt;br&gt;', '			    			  ', '1', '1', '1', '2014-01-07 16:14:26', '0', '1');
INSERT INTO `tbl_follow_up_master` VALUES ('3', '2', '[Auto]', '2014-01-08 00:00:00', '&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Backgr&lt;/b&gt;&lt;div&gt;&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/div&gt;&lt;div&gt;&lt;b&gt;ound&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Bac&lt;/b&gt;&lt;div&gt;&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/div&gt;&lt;div&gt;&lt;b&gt;kground&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Ba&lt;/b&gt;&lt;/div&gt;&lt;div&gt;&lt;b&gt;&lt;br&gt;&lt;/b&gt;&lt;/div&gt;&lt;div&gt;&lt;b&gt;ckground&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;b&gt;Background&lt;/b&gt;&lt;/div&gt;&lt;/div&gt;', '			    Something remark	  10000		  			  			  ', '1', '1', '0', null, '0', '1');

-- ----------------------------
-- Table structure for `tbl_follow_up_recommendation`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_follow_up_recommendation`;
CREATE TABLE `tbl_follow_up_recommendation` (
  `inquiry_report_followup_id` bigint(20) DEFAULT NULL,
  `recom_id` bigint(20) DEFAULT NULL,
  `recommendation` longtext,
  `organization_response` varchar(100) DEFAULT NULL,
  `action_taken` longtext,
  `comment` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_follow_up_recommendation
-- ----------------------------
INSERT INTO `tbl_follow_up_recommendation` VALUES ('1', '1', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶1', 'Accept', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('1', '2', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶2', 'Accept', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶', 'à¦¸à§à¦ªà¦¾à¦°à¦¿à¦¶', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('2', '3', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv&nbsp;', 'Accept', '<br>', '<br>', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('2', '4', '<br>', 'Accept', '<br>', '<br>', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('2', '5', '<b>hkhaakhsdklhidouututugiugauytasudtdsiahdasidsayudastdassd</b> gugihoihiods &nbsp; &nbsp;sjshkshsdhsdkshsdds&nbsp;<div><br></div>', 'Accept', '<br>', '<br>', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('2', '6', 'gdfg<br>', 'Accept', '<br>', '<br>', '0', '1');
INSERT INTO `tbl_follow_up_recommendation` VALUES ('2', '7', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh ', 'Accept', '<br>', '<br>', '0', '1');

-- ----------------------------
-- Table structure for `tbl_govt_ref_no`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_govt_ref_no`;
CREATE TABLE `tbl_govt_ref_no` (
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `govt_ref_no` varchar(128) DEFAULT NULL,
  `ref_year` varchar(10) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_govt_ref_no
-- ----------------------------
INSERT INTO `tbl_govt_ref_no` VALUES ('1', '1', 'English10001', '2013', '1');
INSERT INTO `tbl_govt_ref_no` VALUES ('2', '2', 'Bangla10001', '2013', '1');
INSERT INTO `tbl_govt_ref_no` VALUES ('3', '1', 'EnglishEC000001', '2013', '1');

-- ----------------------------
-- Table structure for `tbl_group`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group`;
CREATE TABLE `tbl_group` (
  `group_id` int(11) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_group
-- ----------------------------
INSERT INTO `tbl_group` VALUES ('1', 'SuperAdmin');
INSERT INTO `tbl_group` VALUES ('2', 'Committee1');
INSERT INTO `tbl_group` VALUES ('3', 'Member1');
INSERT INTO `tbl_group` VALUES ('4', 'Member2');
INSERT INTO `tbl_group` VALUES ('5', 'Member');

-- ----------------------------
-- Table structure for `tbl_group_menu_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group_menu_permission`;
CREATE TABLE `tbl_group_menu_permission` (
  `group_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `is_view` tinyint(4) DEFAULT NULL,
  `is_insert` tinyint(4) DEFAULT NULL,
  `is_edit` tinyint(4) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `is_print` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_group_menu_permission
-- ----------------------------
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '117', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '118', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '119', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '121', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '115', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40402', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40401', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40302', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40303', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40304', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40301', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40202', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40201', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '401', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '202', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '301', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '201', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '120', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '101', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '102', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '103', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '104', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '112', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '116', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '105', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '106', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '107', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '108', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '109', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '110', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '111', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '113', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '114', '1', '1', '1', '1', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_adviser`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_adviser`;
CREATE TABLE `tbl_inquiry_adviser` (
  `inquiry_id` int(11) DEFAULT NULL,
  `adviser_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_adviser
-- ----------------------------
INSERT INTO `tbl_inquiry_adviser` VALUES ('9', '2', '0', '1');
INSERT INTO `tbl_inquiry_adviser` VALUES ('12', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_doc_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_doc_tag`;
CREATE TABLE `tbl_inquiry_doc_tag` (
  `inquiry_id` int(11) DEFAULT NULL,
  `doc_reg_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_doc_tag
-- ----------------------------
INSERT INTO `tbl_inquiry_doc_tag` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_inquiry_doc_tag` VALUES ('12', '2', '0', '1');
INSERT INTO `tbl_inquiry_doc_tag` VALUES ('13', '4', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_master`;
CREATE TABLE `tbl_inquiry_master` (
  `inquiry_id` int(11) DEFAULT NULL,
  `inquiry_no` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `inquiry_title` text,
  `proposed_month` varchar(50) DEFAULT NULL,
  `proposed_year` varchar(4) DEFAULT NULL,
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `remarks` longtext,
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_master
-- ----------------------------
INSERT INTO `tbl_inquiry_master` VALUES ('1', 'à¦ªà¦¿à¦à¦›à¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§§', '2013-11-12 00:00:00', '&lt;font face=&quot;courier new&quot; size=&quot;4&quot;&gt;à¦¥à¦¾à¦¨à¦¾ à¦¥à¦¾à¦•à§‡ à¦¬à¦²à¦›à¦¿ &lt;/font&gt;sfsfs sdfsdfsdfsdfsdfsdf&lt;br&gt;', 'February', '2005', '2', '1', '', '2', '2', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('2', 'PAC20130001', '2013-11-12 00:00:00', 'xzcczxczcxzs&lt;br&gt;', 'June', '2016', '1', '3', '', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('3', 'PAC20130002', '2013-11-12 00:00:00', 'assasassaasa', 'February', '2002', '1', '3', '', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('4', 'PAC20130003', '2013-11-12 00:00:00', 'xddfgdfgdfg', 'January', '2003', '1', '3', 'something remark	2', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('5', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§¨', '2013-11-14 00:00:00', 'à¦ªà§à¦¬à§à¦²à¦¿à¦š à¦‡à¦¨à¦•à§à¦‡à¦°à¦¿à¦à¦¸ &lt;br&gt;', 'October', '2014', '2', '2', '', '2', '2', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('6', 'EC20130001', '2013-11-26 00:00:00', 'AS&lt;br&gt;', 'January', '2014', '1', '3', '', '3', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('7', 'PAC20130004', '2013-12-21 00:00:00', 'fsdfa&lt;br&gt;', 'January', '2014', '1', '3', '', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('8', 'PAC20140005', '2014-01-07 00:00:00', '', 'January', '2014', '1', '3', '', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('9', 'PAC20140006', '2014-01-18 00:00:00', 'Inquiry Title *', 'January', '2014', '1', '3', 'Remarks', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('10', 'PAC20140007', '2014-01-18 00:00:00', 'aaa', 'November', '2013', '1', '3', '', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('11', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§ªà§¦à§¦à§¦à§©', '2014-02-01 00:00:00', 'à¦†à¦®à¦¿ à¦ªà¦¾à¦¬à¦²à¦¿à¦•&lt;br&gt;', 'February', '2017', '2', '1', 'à¦®à¦¨à§à¦¤à¦¬à§à¦¯ à¦¸à¦®à§‚à¦¹ à¦®à¦¨à§à¦¤à¦¬à§à¦¯ à¦¸à¦®à§‚à¦¹ à¦®à¦¨à§à¦¤à¦¬à§à¦¯ à¦¸à¦®à§‚à¦¹ à¦®à¦¨à§à¦¤à¦¬à§à¦¯ à¦¸à¦®à§‚à¦¹ à¦®à¦¨à§à¦¤à¦¬à§à¦¯ à¦¸à¦®à§‚à¦¹', '2', '2', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('12', 'PAC20140008', '2014-02-09 00:00:00', '&lt;span style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Inquiry Title * 1&lt;/span&gt;', 'March', '2014', '11', '5', 'Remarks 1', '1', '1', '0', null, '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('13', 'PAC20140009', '2014-02-13 00:00:00', '&lt;span style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Inquiry Title * 12&lt;/span&gt;', 'February', '2014', '11', '5', 'RemarksRemarksRemarksRemarks', '1', '1', '0', '2014-02-17 18:41:27', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_ministry`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_ministry`;
CREATE TABLE `tbl_inquiry_ministry` (
  `inquiry_id` int(11) DEFAULT NULL,
  `ministry_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_ministry
-- ----------------------------
INSERT INTO `tbl_inquiry_ministry` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_inquiry_ministry` VALUES ('12', '3', '0', '1');
INSERT INTO `tbl_inquiry_ministry` VALUES ('13', '3', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_organization`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_organization`;
CREATE TABLE `tbl_inquiry_organization` (
  `inquiry_id` int(11) DEFAULT NULL,
  `organization_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_organization
-- ----------------------------
INSERT INTO `tbl_inquiry_organization` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_inquiry_organization` VALUES ('13', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_others`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_others`;
CREATE TABLE `tbl_inquiry_others` (
  `inquiry_id` int(11) DEFAULT NULL,
  `others_org_ministry_id` bigint(20) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_others
-- ----------------------------
INSERT INTO `tbl_inquiry_others` VALUES ('9', '1', '0', '1');
INSERT INTO `tbl_inquiry_others` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_inquiry_others` VALUES ('13', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_other_witnessess`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_other_witnessess`;
CREATE TABLE `tbl_inquiry_other_witnessess` (
  `inquiry_id` int(11) DEFAULT NULL,
  `witness_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_other_witnessess
-- ----------------------------
INSERT INTO `tbl_inquiry_other_witnessess` VALUES ('9', '1', '0', '1');
INSERT INTO `tbl_inquiry_other_witnessess` VALUES ('12', '7', '0', '1');
INSERT INTO `tbl_inquiry_other_witnessess` VALUES ('13', '9', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_report_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_report_master`;
CREATE TABLE `tbl_inquiry_report_master` (
  `inquiry_report_id` int(11) DEFAULT NULL,
  `report_no` varchar(30) DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `title` longtext,
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `executive_summary` longtext,
  `introduction` longtext,
  `issues` longtext,
  `remarks` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_approved` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_report_master
-- ----------------------------
INSERT INTO `tbl_inquiry_report_master` VALUES ('2', '\'0120130002\'', '2014-02-07 00:00:00', '3', 'Report ??????????????', '1', '3', 'Hell executive', 'new introduction', 'some issues', 'its not remarks', '1', '1', '0', '1', null, '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('3', 'à§¦à§¨à§¨à§¦à§§à§©à§¦à§¦à§¦à§§', '2014-02-27 00:00:00', '1', 'à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª', '2', '1', '&lt;span style=&quot;color: rgb(85, 85, 85); font-family: arial, verdana, tahoma, sans-serif; font-size: 17px; line-height: 19px;&quot;&gt;à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª&lt;/span&gt;', '&lt;span style=&quot;color: rgb(85, 85, 85); font-family: arial, verdana, tahoma, sans-serif; font-size: 17px; line-height: 19px;&quot;&gt;à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª&lt;/span&gt;', '&lt;span style=&quot;color: rgb(85, 85, 85); font-family: arial, verdana, tahoma, sans-serif; font-size: 17px; line-height: 19px;&quot;&gt;à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª&lt;/span&gt;', 'à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª', '2', '2', '0', '1', null, '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('1', '0120130001', '2014-02-27 00:00:00', '3', 'Report of &amp;nbsp;assasassaasa', '1', '3', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh ', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh ', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh ', ' sdf', '1', '1', '1', '1', '2013-11-19 16:00:08', '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('4', '0120140003', '0000-00-00 00:00:00', '4', 'Report of Â xddfgdfgdfg sdfsdf ds', null, null, 'sd &amp;nbsp;fds&amp;nbsp;&lt;b style=&quot;font-family: tahoma, verdana, sans-serif; background-color: rgb(236, 236, 236);&quot;&gt;Executive Summary *s dfdsfsd&lt;/b&gt;', 'dsf ds', 'sdf dsfds', ' sdfsd ', '1', '1', null, '1', null, '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('5', '0120140004', '0000-00-00 00:00:00', '9', 'Report of Â Inquiry Title *', null, null, 'cvxczvcxz xcds', 'sdfsdfsdf', 'fsdf', ' af dfs', '1', '1', null, '1', null, '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('6', '0120140005', '2014-02-15 00:00:00', '4', 'Report of Â xddfgdfgdfg', null, null, 'dsf dsf sdfdsfsd', 'fsdfsd', 'fsdfds', ' fsdfdsf', '1', '1', null, '1', null, '0');
INSERT INTO `tbl_inquiry_report_master` VALUES ('7', '0120140006', '2014-02-14 00:00:00', '4', 'Report of Â xddfgdfgdfg', null, null, 'sdfsdfsdf', 'sd fds', 'sd fds', ' sd fds', '1', '1', null, '1', null, '0');

-- ----------------------------
-- Table structure for `tbl_inquiry_report_recommendation`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_report_recommendation`;
CREATE TABLE `tbl_inquiry_report_recommendation` (
  `inquiry_report_id` int(11) DEFAULT NULL,
  `recom_id` int(11) DEFAULT NULL,
  `recom_no` varchar(50) DEFAULT NULL,
  `recommendation` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_report_recommendation
-- ----------------------------
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('3', '13', 'Recommendation 1', '<span style=\"color: rgb(85, 85, 85); font-family: arial, verdana, tahoma, sans-serif; font-size: 17px; line-height: 19px;\">à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª</span>', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('1', '8', 'Recommendation 1', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv&nbsp;', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('1', '12', 'Recommendation 5', '<br>', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('3', '14', 'Recommendation 2', '<span style=\"color: rgb(85, 85, 85); font-family: arial, verdana, tahoma, sans-serif; font-size: 17px; line-height: 19px;\">à¦¯à§‹à¦—à¦¾à¦¯à§‹à¦—à¦®à¦¨à§à¦¤à§à¦°à§€ à¦¬à¦²à§‡à¦¨, à¦ªà¦¾à¦°à§à¦¬à¦¤à§à¦¯ à¦…à¦žà§à¦šà¦²à§‡à¦° à§¨à§§à¦Ÿà¦¿ à¦¸à¦¡à¦¼à¦•à§‡à¦° à¦…à¦¬à¦¸à§à¦¥à¦¾ à¦–à¦¾à¦°à¦¾à¦ª</span>', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('1', '9', 'Recommendation 2', '<b>hkhaakhsdklhidouututugiugauytasudtdsiahdasidsayudastdassd</b> gugihoihiods &nbsp; &nbsp;sjshkshsdhsdkshsdds&nbsp;<div><br></div>', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('1', '10', 'Recommendation 3', 'gdfg<br>', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('1', '11', 'Recommendation 4', 'sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh sdfsdfsdfzxcv zxc vz xmcv nzx,mcvbkzxbncvzxc vkjzxchvzxch vhzxcvkjhzxckvhkzxchvkzjxchkvj zxcvkjzxchvkhzxckvhzxckjvh ', '0', '1');
INSERT INTO `tbl_inquiry_report_recommendation` VALUES ('7', '15', 'Recommendation 1', 'sdf sf', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_scheduling`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_scheduling`;
CREATE TABLE `tbl_inquiry_scheduling` (
  `schedule_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT '0',
  `schedule_no` smallint(6) DEFAULT '0',
  `schedule_date` datetime DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_scheduling
-- ----------------------------
INSERT INTO `tbl_inquiry_scheduling` VALUES ('1', '1', '1', '2013-10-10 00:00:00', '2013-11-12 16:02:38', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('2', '3', '1', '2012-12-12 00:00:00', '2013-11-12 16:10:42', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('3', '2', '1', '2013-11-13 00:00:00', '2013-11-13 12:52:44', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('4', '2', '2', '2013-11-07 00:00:00', '2013-11-13 12:52:44', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('5', '2', '3', '2013-11-06 00:00:00', '2013-11-13 12:52:44', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('6', '5', '1', '2013-11-14 00:00:00', '2013-11-14 16:03:49', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('7', '2', '4', '2013-11-05 00:00:00', '2013-11-28 16:16:22', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('8', '8', '1', '2014-01-09 00:00:00', '2014-01-07 16:06:04', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('9', '9', '1', '2014-01-16 00:00:00', '2014-01-18 15:15:19', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_witnesses`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_witnesses`;
CREATE TABLE `tbl_inquiry_witnesses` (
  `inquiry_id` int(11) DEFAULT NULL,
  `witness_type` varchar(1) DEFAULT NULL,
  `witness_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_witnesses
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_invitees`;
CREATE TABLE `tbl_invitees` (
  `invitees_id` bigint(20) DEFAULT NULL,
  `invitees_name` varchar(150) DEFAULT NULL,
  `invitees_organization` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_invitees
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_language`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_language`;
CREATE TABLE `tbl_language` (
  `language_id` smallint(6) DEFAULT NULL,
  `language` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_language
-- ----------------------------
INSERT INTO `tbl_language` VALUES ('1', 'English');
INSERT INTO `tbl_language` VALUES ('2', 'Bangla');

-- ----------------------------
-- Table structure for `tbl_logistic_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_logistic_member`;
CREATE TABLE `tbl_logistic_member` (
  `logistic_member_id` smallint(6) DEFAULT NULL,
  `logistic_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_logistic_member
-- ----------------------------
INSERT INTO `tbl_logistic_member` VALUES ('1', 'test', 'test', '', '010010001', '', 'sd@y.com', 'sd', 'sd', '1', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('2', 'test', 'à§«à§¬à§«à§¬', '012121212', '121212121212', '121212121', 'public_taufiq@yahoo.com', '		gfhgfhf fg hfghfgh fg hg						', '		fgh  fhgfgh						', '1', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('3', 'test', 'X', '12121212121', '1212121', '121212121', 'public_taufiq@yahoo.com', 'ghjhgjgh				', 'hjgj						', '1', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('4', 'data_member logistic member', 'data_designation logistic member', '015216521654', '251654844554', '3541564', 'data_email@email.com', 'data_address logistic member', 'data_comment logistic member', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_media`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_media`;
CREATE TABLE `tbl_media` (
  `media_id` int(11) DEFAULT NULL,
  `media_name` varchar(250) DEFAULT NULL,
  `media_type` varchar(50) DEFAULT NULL,
  `contact_person` varchar(250) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `media_address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_media
-- ----------------------------
INSERT INTO `tbl_media` VALUES ('1', 'test', 'TV Media', 'test111', 'test', '12321', '6666666', '4442422442', 'sa_sarker10@yahoo.com', 'ma', '7777', '1', '0', '1');
INSERT INTO `tbl_media` VALUES ('2', 'media', 'Print Media', 'media', 'media', '12321', '111', '1112112121212', 'sa_sarker10@yahoo.com', '							saas									', '							saaasa									', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_adviser`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_adviser`;
CREATE TABLE `tbl_meeting_notice_adviser` (
  `meeting_notice_id` int(11) DEFAULT NULL,
  `adviser_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_adviser
-- ----------------------------
INSERT INTO `tbl_meeting_notice_adviser` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_adviser` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_adviser` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_adviser` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_committee`;
CREATE TABLE `tbl_meeting_notice_committee` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `member_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_committee
-- ----------------------------
INSERT INTO `tbl_meeting_notice_committee` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('12', '0', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('17', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('17', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('18', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('18', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('19', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('19', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('21', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('21', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('23', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('24', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('24', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('25', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('25', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_inquiry_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_inquiry_tag`;
CREATE TABLE `tbl_meeting_notice_inquiry_tag` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_inquiry_tag
-- ----------------------------
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('2', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('3', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('4', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('5', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('6', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('7', '6', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('11', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('13', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('14', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('15', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('16', '7', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('17', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('18', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('19', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('20', '7', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('21', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('22', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('23', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('24', '7', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('25', '7', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('26', '9', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('27', '8', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('28', '10', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('29', '7', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('30', '13', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_invitees`;
CREATE TABLE `tbl_meeting_notice_invitees` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `invitees_id` bigint(20) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_invitees
-- ----------------------------
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('12', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('13', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('13', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_logistic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_logistic`;
CREATE TABLE `tbl_meeting_notice_logistic` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `logistic_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_logistic
-- ----------------------------
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('7', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('8', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('10', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('11', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('14', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('14', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('15', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('15', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('16', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('16', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('17', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('18', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('19', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('20', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('20', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('21', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('22', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('22', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('23', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('23', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('24', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('24', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('24', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('24', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('25', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('25', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('25', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('25', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('26', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('26', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('27', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('27', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('28', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('28', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('29', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('29', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('30', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('30', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('30', '4', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_master`;
CREATE TABLE `tbl_meeting_notice_master` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `gov_no_pix` varchar(50) DEFAULT NULL,
  `gov_no_postfix` varchar(10) DEFAULT NULL,
  `sitting_no` varchar(50) DEFAULT NULL,
  `en_date` datetime DEFAULT NULL,
  `bn_date` varchar(250) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `venue_id` smallint(6) DEFAULT NULL,
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `private_business_before` longtext,
  `public_business` longtext,
  `private_business_after` longtext,
  `remarks` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `is_approved` smallint(6) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `is_cancel` smallint(6) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `is_adjourned` smallint(6) DEFAULT NULL,
  `adjourned_date` datetime DEFAULT NULL,
  `is_revised` smallint(6) DEFAULT NULL,
  `revised_date` datetime DEFAULT NULL,
  `is_display` smallint(6) DEFAULT NULL,
  `is_complete` smallint(6) DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_master
-- ----------------------------
INSERT INTO `tbl_meeting_notice_master` VALUES ('1', 'English10001', '1', '1', '2014-01-31 00:00:00', 'à§§à§® à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '1sdfsdfsd', '1', '0', 'asd', 'asd', 'asd', '', '1', '1', 'ISSUED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('2', 'Bangla10001', 'à§§', 'à§§', '2013-12-21 00:00:00', 'à§©à§¦ à¦•à¦¾à¦°à§à¦¤à¦¿à¦• à§§à§ªà§¨à§¦', '10.10am', '2', '0', 'à§Ÿà¦¾', 'à¦«', 'à¦«', '', '2', '2', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-11-13 14:01:14', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('3', 'Bangla10001', 'à§§', 'à§§/à§§', '2013-12-21 00:00:00', 'à§©à§¦ à¦•à¦¾à¦°à§à¦¤à¦¿à¦• à§§à§ªà§¨à§¦', '10.10am', '2', '0', 'à§Ÿà¦¾', 'à¦«', 'à¦«', null, '2', '2', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-11-13 14:01:31', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('30', 'English10001', '6', '10', '2014-02-18 00:00:00', 'à§¬ à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '09:25', '1', '1', 'df df dsfsdf', 'dsf dsfsd fds', 'f dsafds', 'sdf sdfsdf', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '9:55');
INSERT INTO `tbl_meeting_notice_master` VALUES ('4', 'Bangla10001', 'à§§', 'à§§/à§¨', '2013-12-21 00:00:00', 'à§©à§¦ à¦•à¦¾à¦°à§à¦¤à¦¿à¦• à§§à§ªà§¨à§¦', '10.10am', '2', '0', null, null, null, null, '2', '2', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-11-14 13:49:48', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('6', 'Bangla10001', 'à§§', 'à§§/à§©', '2013-12-21 00:00:00', 'à§§à§« à¦…à¦—à§à¦°à¦¹à¦¾à§Ÿà¦£ à§§à§ªà§¨à§¦', '10.10am', '2', '0', '', '', '', null, '2', '2', 'ISSUED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('7', 'EnglishEC000001', '1', '1', '2013-12-21 00:00:00', 'à§§à§¨ à¦…à¦—à§à¦°à¦¹à¦¾à§Ÿà¦£ à§§à§ªà§¨à§¦', '10.10am', '1', '0', 'asd', 'asd', 'asd', '', '1', '3', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('8', 'English10001', '1', '3', '2014-01-31 00:00:00', 'à§§à§® à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10:00', '1', '0', 'picasa private business', 'picasa public business', 'picasa private business2', 'picasa remarks', '1', '1', 'ISSUED', '1', '2014-02-04 11:39:20', '1', '2014-02-08 21:31:38', '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('9', 'Bangla10001', '1', 'à§¨', '2014-01-30 00:00:00', 'à§§à§­ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10:00', '2', '0', 'I am an english which should be bangla general discussion 1', 'I am an english which should be bangla public discussion', 'I am an english which should be bangla general discussion 2', 'I am an english which should be bangla remarks', '2', '2', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('10', 'English10001', '1', '4', '2014-01-31 00:00:00', 'à§§à§® à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '13:45dsfdsfsdf', '1', '0', 'dsfds', 'dsfd', 'dsfd', 'dsf', '1', '1', 'ISSUED', '1', '2014-02-04 11:39:12', '1', '2014-02-08 21:30:37', '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('11', 'English10001', '10', '5', '2014-01-28 00:00:00', 'à§§à§« à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '15pm', '1', '0', '&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;', 'Public BusinessPublic BusinessPublic BusinessPublic BusinessPublic BusinessPublic Business', 'Private Business			Private Business			Private Business			Private Business			Private Business', '', '1', '1', 'ADJOURNED', '1', '2014-02-04 11:35:57', '1', '2014-02-08 14:59:56', '0', null, '1', '2014-02-09 18:11:23', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('5', 'English10001', '2', '2', '2014-01-24 00:00:00', 'à§§à§§ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10.10pm', '3', '0', 'P', '', '', '', '1', '1', 'ADJOURNED', '1', '2014-02-03 20:14:39', '1', '2014-02-04 11:32:38', '0', null, '1', '2014-02-06 17:33:35', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('12', 'English10001', '2', '1', '2014-02-27 00:00:00', 'à§§à§« à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '10:50', '22', '1', '2', 'Mehedi', 'sdf sdaf dsf', 'sdf sdafsd f', '0', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('13', 'English10001', '6', '2', '2014-02-20 00:00:00', 'à§® à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '', '16', '1', '3', 'Mehedi 2', '', '', '0', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('14', 'English10001', '4', '1', '2014-01-30 00:00:00', 'à§§à§­ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:55', '1', '1', '', '', '', '', '1', '1', 'ISSUED', '1', '2014-02-08 15:25:29', '1', '2014-02-08 15:26:06', '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '07:05');
INSERT INTO `tbl_meeting_notice_master` VALUES ('15', 'English10001', '5', '2', '2014-02-05 00:00:00', 'à§¨à§© à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '15:30', '1', '1', 'dsfsdf sdfsd fsdf', 'sdfds fsd', 'sdf sdf', 'sdf ds', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '0:15');
INSERT INTO `tbl_meeting_notice_master` VALUES ('16', 'English10001', '4', '3', '2014-01-31 00:00:00', 'à§§à§® à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:20', '1', '1', '', '', '', 'Something remarks', '1', '1', 'ADJOURNED', '1', '2014-02-09 18:07:55', '1', '2014-02-09 18:08:31', '0', null, '1', '2014-02-09 18:13:16', '0', null, '0', '0', null, '0', '1', '9:50');
INSERT INTO `tbl_meeting_notice_master` VALUES ('17', 'English10001', '2', '2/1', '2014-01-24 00:00:00', 'à§§à§§ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10.10pm', '3', '0', 'P', '', '', '', '1', '1', 'REVISED', '1', '2014-02-06 17:33:48', '1', '2014-02-06 17:34:00', '0', null, '0', null, '1', '2014-02-06 17:34:57', '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('18', 'English10001', '2', '2/1', '2014-01-24 00:00:00', 'à§§à§§ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10.10pm', '3', '0', 'P', '', '', '', '1', '1', 'ADJOURNED', '1', '2014-02-08 14:54:35', '1', '2014-02-08 14:59:12', '0', null, '1', '2014-02-08 15:07:21', '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('19', 'English10001', '2', '2/1', '2014-01-27 00:00:00', 'à§§à§ª à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10.10pm', '3', '0', 'P', '', '', '', '1', '1', 'REVISED', '0', null, '0', null, '0', null, '0', null, '0', null, '0', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('20', 'English10001', 'PAC2013000', '4', '2014-01-30 00:00:00', 'à§§à§­ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:45', '1', '1', '', '', '', '', '1', '1', 'ADJOURNED', '1', '2014-02-08 15:06:26', '1', '2014-02-09 18:10:36', '0', null, '1', '2014-02-09 18:11:51', '0', null, '0', '0', null, '0', '1', '10:15');
INSERT INTO `tbl_meeting_notice_master` VALUES ('21', 'English10001', '2', '2/2', '2014-01-24 00:00:00', 'à§§à§§ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '10.10pm', '3', '0', 'P', '', '', '', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('22', 'English10001', '21', '5', '2014-02-12 00:00:00', 'à§©à§¦ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:10', '1', '1', '', '', '', '', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '9:40');
INSERT INTO `tbl_meeting_notice_master` VALUES ('23', 'English10001', '10', '5/1', '2014-01-28 00:00:00', 'à§§à§« à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '15pm', '1', '0', '&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;div&gt;\r\n		&lt;div&gt;&lt;b&gt;ITEMS&lt;/b&gt;&lt;/div&gt;\r\n		\r\n		&lt;/div&gt;	\r\n		\r\n		\r\n		    &lt;div style=&quot;float:left;width:15%;&quot;&gt;\r\n			Private Business			&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;&lt;/div&gt;', 'Public BusinessPublic BusinessPublic BusinessPublic BusinessPublic BusinessPublic Business', 'Private Business			Private Business			Private Business			Private Business			Private Business', '', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('24', 'English10001', 'PAC2013000', '4/1', '2014-01-30 00:00:00', 'à§§à§­ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:45', '1', '1', '', '', '', '', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('25', 'English10001', '4', '3/1', '2014-01-31 00:00:00', 'à§§à§® à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:20', '1', '1', '', '', '', 'Something remarks', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', null);
INSERT INTO `tbl_meeting_notice_master` VALUES ('26', 'English10001', '1', '6', '2014-02-09 00:00:00', 'à§¨à§­ à¦®à¦¾à¦˜ à§§à§ªà§¨à§¦', '09:20', '1', '1', '', '', '', '', '1', '1', 'ISSUED', '1', '2014-02-09 20:30:03', '1', '2014-02-09 20:30:33', '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '9:50');
INSERT INTO `tbl_meeting_notice_master` VALUES ('27', 'English10001', '1', '7', '2014-03-05 00:00:00', 'à§¨à§§ à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '09:15', '1', '1', '', '', '', '', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '9:45');
INSERT INTO `tbl_meeting_notice_master` VALUES ('28', 'English10001', '2', '8', '2014-03-06 00:00:00', 'à§¨à§¨ à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '10:15', '1', '1', '', '', '', '', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '10:45');
INSERT INTO `tbl_meeting_notice_master` VALUES ('29', 'English10001', '2', '9', '2014-02-20 00:00:00', 'à§® à¦«à¦¾à¦²à§à¦—à§à¦¨ à§§à§ªà§¨à§¦', '09:55', '3', '1', '', '', '', '', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '1', '0', null, '0', '1', '9:45');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_notification`;
CREATE TABLE `tbl_meeting_notice_notification` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `notification_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_notification
-- ----------------------------
INSERT INTO `tbl_meeting_notice_notification` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('12', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('12', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('12', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('13', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('13', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('13', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('13', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('30', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_officer`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_officer`;
CREATE TABLE `tbl_meeting_notice_officer` (
  `meeting_notice_id` int(11) DEFAULT NULL,
  `officer_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_officer
-- ----------------------------
INSERT INTO `tbl_meeting_notice_officer` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('7', '4', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_officer` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_witness`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_witness`;
CREATE TABLE `tbl_meeting_notice_witness` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `witness_id` int(6) DEFAULT NULL,
  `witness_type` char(1) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_witness
-- ----------------------------
INSERT INTO `tbl_meeting_notice_witness` VALUES ('11', '1', 'O', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('12', '1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('12', '2', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('13', '1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('13', '2', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('14', '2', 'T', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('14', '1', 'O', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('15', '4', 'T', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('23', '1', 'O', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('30', '5', 'T', '0', '1');

-- ----------------------------
-- Table structure for `tbl_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `menu_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `sl_no` int(11) DEFAULT NULL,
  `en_menu_name` varchar(255) DEFAULT NULL,
  `bn_menu_name` varchar(255) DEFAULT NULL,
  `menu_link` text,
  PRIMARY KEY (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO `tbl_menu` VALUES ('1', '0', '1', 'System', 'à¦¸à¦¿à¦¸à§à¦Ÿà§‡à¦®', null);
INSERT INTO `tbl_menu` VALUES ('2', '0', '2', 'Security', 'à¦¨à¦¿à¦°à¦¾à¦ªà¦¤à§à¦¤à¦¾', null);
INSERT INTO `tbl_menu` VALUES ('3', '0', '3', 'Document Database', 'à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦¡à¦¾à¦Ÿà¦¾à¦¬à§‡à¦¸', null);
INSERT INTO `tbl_menu` VALUES ('4', '0', '4', 'Inquiry', 'à¦¤à¦¦à¦¨à§à¦¤', null);
INSERT INTO `tbl_menu` VALUES ('403', '4', '4', 'Sitting ', 'à¦¸à¦­à¦¾', null);
INSERT INTO `tbl_menu` VALUES ('101', '1', '1', 'Parliament', 'à¦¸à¦‚à¦¸à¦¦', 'parliament.php');
INSERT INTO `tbl_menu` VALUES ('102', '1', '2', 'Session', 'à¦…à¦§à¦¿à¦¬à§‡à¦¶à¦¨', 'session.php');
INSERT INTO `tbl_menu` VALUES ('104', '1', '4', 'Committee', 'à¦•à¦®à¦¿à¦Ÿà¦¿', 'committee.php');
INSERT INTO `tbl_menu` VALUES ('107', '1', '7', 'Sub Committee', 'à¦¸à¦¾à¦¬ à¦•à¦®à¦¿à¦Ÿà¦¿', 'sub_committee.php');
INSERT INTO `tbl_menu` VALUES ('109', '1', '9', 'Ministry', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼', 'ministry.php');
INSERT INTO `tbl_menu` VALUES ('110', '1', '10', 'Ministry Member', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼ à¦¸à¦¦à¦¸à§à¦¯', 'ministry_member.php');
INSERT INTO `tbl_menu` VALUES ('111', '1', '11', 'Organization', 'à¦¸à¦‚à¦¸à§à¦¥à¦¾', 'organization.php');
INSERT INTO `tbl_menu` VALUES ('112', '1', '12', 'Organization Member', 'à¦¸à¦‚à¦¸à§à¦¥à¦¾ à¦¸à¦¦à¦¸à§à¦¯', 'organization_member.php');
INSERT INTO `tbl_menu` VALUES ('119', '1', '19', 'Venue', 'à¦®à¦¿à¦²à¦¨à¦¸à§à¦¥à¦¨', 'venue.php');
INSERT INTO `tbl_menu` VALUES ('113', '1', '13', 'Notification Member', 'à¦…à¦¬à¦¹à¦¿à¦¤ à¦¸à¦¦à¦¸à§à¦¯', 'notification_member.php');
INSERT INTO `tbl_menu` VALUES ('114', '1', '14', 'Logistic Member', 'à¦²à¦œà¦¿à¦¸à§à¦Ÿà¦¿à¦• à¦¸à¦¦à¦¸à§à¦¯', 'logistic_member.php');
INSERT INTO `tbl_menu` VALUES ('201', '2', '1', 'User', 'à¦¬à§à¦¯à¦¬à¦¹à¦¾à¦°à¦•à¦¾à¦°à§€', 'user_info.php');
INSERT INTO `tbl_menu` VALUES ('202', '2', '2', 'User Type', 'à¦¬à§à¦¯à¦¬à¦¹à¦¾à¦°à¦•à¦¾à¦°à§€ à¦ªà§à¦°à¦•à¦¾à¦°', 'group_info.php');
INSERT INTO `tbl_menu` VALUES ('301', '3', '1', 'Document Registration', 'à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦¨à¦¿à¦¬à¦¨à§à¦§à¦¨', 'document_registration.php');
INSERT INTO `tbl_menu` VALUES ('302', '10000', '2', 'Document Key Areas and Recommendation', ' à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦®à§‚à¦² à¦à¦²à¦¾à¦•à¦¾à¦¸à¦®à§‚à¦¹ à¦à¦¬à¦‚ à¦ªà§à¦°à¦¸à§à¦¤à¦¾à¦¬à¦¨à¦¾', 'document_registration_key.php');
INSERT INTO `tbl_menu` VALUES ('405', '10000', '2', 'Inquiry Scheduling', 'à¦¤à¦¦à¦¨à§à¦¤ à¦¸à¦®à¦¯à¦¼à¦¸à§‚à¦šà§€', 'inquiry_scheduling.php');
INSERT INTO `tbl_menu` VALUES ('401', '4', '1', 'Manage Inquiry', ' à¦¤à¦¦à¦¨à§à¦¤ à¦¤à§ˆà¦°à¦¿ à¦•à¦°à§à¦¨', 'create_inquiry.php');
INSERT INTO `tbl_menu` VALUES ('402', '4', '3', 'Evidence', 'à¦ªà§à¦°à¦®à¦¾à¦£ à¦—à§à¦°à¦¹à¦£', null);
INSERT INTO `tbl_menu` VALUES ('40401', '404', '4', 'Inquiry Report Creation', 'à¦¤à¦¦à¦¨à§à¦¤ à¦ªà§à¦°à¦¤à¦¿à¦¬à§‡à¦¦à¦¨ à¦¤à§ˆà¦°à§€', 'inquiry_report.php');
INSERT INTO `tbl_menu` VALUES ('40402', '404', '5', 'Follow Up Inquiry', 'à¦¤à¦¦à¦¨à§à¦¤ à¦…à¦—à§à¦°à¦—à¦¤à¦¿', 'follow_up.php');
INSERT INTO `tbl_menu` VALUES ('40301', '403', '1', 'Manage Sitting', 'à¦¸à¦­à¦¾ à¦¨à§‹à¦Ÿà¦¿à¦¶', 'meeting_notice.php');
INSERT INTO `tbl_menu` VALUES ('40302', '403', '2', 'Briefing Note', 'à¦¬à§à¦°à¦¿à¦«à¦¿à¦‚ à¦¨à§‹à¦Ÿ', 'briefing_note.php');
INSERT INTO `tbl_menu` VALUES ('40303', '403', '3', 'Record of Decisions', 'à¦°à§‡à¦•à¦°à§à¦¡ à¦“ à¦¸à¦¿à¦¦à§à¦§à¦¾à¦¨à§à¦¤', 'record_decisions.php');
INSERT INTO `tbl_menu` VALUES ('40304', '403', '4', 'Multimedia & Verbatim Recording', 'à¦®à¦¾à¦²à§à¦Ÿà¦¿à¦®à¦¿à¦¡à¦¿à¦¯à¦¼à¦¾ à¦“ à¦§à¦¾à¦°à¦£à¦•à§ƒà¦¤ à¦°à§‡à¦•à¦°à§à¦¡à¦¿à¦‚', 'multimedia_verbatim.php');
INSERT INTO `tbl_menu` VALUES ('105', '1', '5', 'Committee Member', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦¸à¦¦à¦¸à§à¦¯', 'committee_member.php');
INSERT INTO `tbl_menu` VALUES ('116', '1', '16', 'Others Organization/Ministry', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯ à¦¸à¦‚à¦¸à§à¦¥à¦¾ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼', 'others_organization_ministry.php');
INSERT INTO `tbl_menu` VALUES ('117', '1', '17', 'Others Witness', 'à¦…à¦¨à§à¦¯à¦¦à§‡à¦° à¦¸à¦¾à¦•à§à¦·à§€', 'others_witness.php');
INSERT INTO `tbl_menu` VALUES ('120', '10000', '20', 'Observers', 'à¦†à¦®à¦¨à§à¦¤à§à¦°à¦£', 'invites.php');
INSERT INTO `tbl_menu` VALUES ('40201', '402', '1', 'Request', 'à¦…à¦¨à§à¦°à§‹à¦§', 'request_evidence.php');
INSERT INTO `tbl_menu` VALUES ('40202', '402', '2', 'Receive', 'à¦—à§à¦°à¦¹à¦£', 'receive_evidance.php');
INSERT INTO `tbl_menu` VALUES ('404', '4', '6', 'Report', 'à¦°à¦¿à¦ªà§‹à¦°à§à¦Ÿ', null);
INSERT INTO `tbl_menu` VALUES ('108', '1', '8', 'Sub Committee Member', 'à¦¸à¦¾à¦¬ à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦¸à¦¦à¦¸à§à¦¯', 'sub_committee_member.php');
INSERT INTO `tbl_menu` VALUES ('118', '1', '18', 'Media', 'à¦®à¦¾à¦§à§à¦¯à¦®', 'media_info.php');
INSERT INTO `tbl_menu` VALUES ('106', '1', '6', 'Committee Secretariat', 'à¦•à¦®à¦¿à¦Ÿà¦¿ à¦…à¦«à¦¿à¦¸', 'committee_officer.php');
INSERT INTO `tbl_menu` VALUES ('103', '1', '3', 'Parliament Member', 'à¦¸à¦‚à¦¸à¦¦ à¦¸à¦¦à¦¸à§à¦¯', 'parliament_member.php');
INSERT INTO `tbl_menu` VALUES ('115', '1', '15', 'Specialist Adviser', 'à¦¬à¦¿à¦¶à§‡à¦·à¦œà§à¦ž à¦‰à¦ªà¦¦à§‡à¦·à§à¦Ÿà¦¾', 'specialist_adviser.php');
INSERT INTO `tbl_menu` VALUES ('121', '1', '21', 'Committee to Look After', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿', 'officer_committee_tag.php');

-- ----------------------------
-- Table structure for `tbl_ministry`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ministry`;
CREATE TABLE `tbl_ministry` (
  `ministry_id` smallint(6) DEFAULT NULL,
  `ministry_name` varchar(150) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_ministry
-- ----------------------------
INSERT INTO `tbl_ministry` VALUES ('1', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼à§‡à¦° ', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼à§‡à¦° ', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼à§‡à¦° ', '444444444444444', '666666666666666', '', '', '								', '								', '2', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('2', 'à¦¤à¦¥à§à¦¯ à¦¢à§‹à¦•à¦¾à¦¨', 'à¦†à¦ªà¦¡à§‡à¦Ÿ à¦•à¦°à§à¦¨', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼à§‡à¦°', '444444444444444', '', '', '', '								', '								', '2', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('3', 'Food Ministry', 'asas', 'sss', '1121212', '21121212', '12112', 'sa_sarker10@yahoo.com', '		as						', '		asaasa						', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_ministry_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ministry_member`;
CREATE TABLE `tbl_ministry_member` (
  `ministry_member_id` int(11) DEFAULT NULL,
  `ministry_id` smallint(6) DEFAULT NULL,
  `ministry_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_ministry_member
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_notification_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notification_member`;
CREATE TABLE `tbl_notification_member` (
  `notification_member_id` smallint(6) DEFAULT NULL,
  `notification_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_notification_member
-- ----------------------------
INSERT INTO `tbl_notification_member` VALUES ('1', 'data_member notification member', 'data_designation notification member', '015216521654', '251654844554', '3541564', 'data_email@email.com', 'data_address notification member', 'data_comment notification member', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_organization`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_organization`;
CREATE TABLE `tbl_organization` (
  `organization_id` smallint(6) DEFAULT NULL,
  `organization_name` varchar(150) DEFAULT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_organization
-- ----------------------------
INSERT INTO `tbl_organization` VALUES ('1', 'Brac Bank', 'Brac', 'data_organization contact person', 'data_designation organization designation', '015216521654', '251654844554', '3541564', 'data_email@email.com', 'data_ address organization	', 'data_ comment organization					', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_organization_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_organization_member`;
CREATE TABLE `tbl_organization_member` (
  `organization_member_id` int(11) DEFAULT NULL,
  `organization_id` smallint(6) DEFAULT NULL,
  `organization_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_organization_member
-- ----------------------------
INSERT INTO `tbl_organization_member` VALUES ('1', '1', 'data_member organization member', 'data_designation organization member', '546546546', '5456654', '3541564', 'data_email@email.com', 'data_address organization member		', 'data_comment organization member		', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_others`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_others`;
CREATE TABLE `tbl_others` (
  `others_org_ministry_id` bigint(20) DEFAULT NULL,
  `others_org_ministry` varchar(150) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_others
-- ----------------------------
INSERT INTO `tbl_others` VALUES ('1', 'data_ Organzation Ministry', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_other_witnessess`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_other_witnessess`;
CREATE TABLE `tbl_other_witnessess` (
  `witness_id` int(11) DEFAULT NULL,
  `witness_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_other_witnessess
-- ----------------------------
INSERT INTO `tbl_other_witnessess` VALUES ('1', 'tnaya', 'no;ra', '', '07900497242', '', 'tkarlebach@eunoia.co.uk', '	;d giew							', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('2', 'demo _ Witness Name *', 'demo _ designation', '5646', '018455236', '88029966325', 'sa_sarker10@yahoo.com', 'ds fdsf sdfsd fsdafsdaf sd', '				  			sdfdsf	', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('3', 'demo _ Witness Name * 2', 'demo _ designation 2', '02558989', '018455236', '12112', 'sa_sarker10@yahoo.com', 'demo address 2', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('4', 'demo _ Witness Name * 2', 'demo _ designation 2', '02558989', '018455236', '12112', 'sa_sarker10@yahoo.com', 'demo address 2', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('5', 'demo _ Witness Name * 3', 'demo _ designation .3', '02558989', '018455236', '7676', 'sa_sarker@yahoo.com', 'sdfsd fds		', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('6', 'demoWitnessName4', 'dermoDesignation4', '02558989', '018455236', '7676', 'sa_sarker@yahoo.com', 'ksdjf lkjds fj', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('7', 'demo _ Witness Name * 5', 'demo _ designation .5', '02558989', '456645', '7676', 'sa_sarker@yahoo.com', '						sdf sdf sdfsd', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('8', 'demo _ Witness Name * 7', 'demo _ designation .7', '6545454545', '018455236', '88029966325', 'saker@yahho.com', '			dsf dsfsd fsdf			 sdfsd		', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('9', 'demo _ Witness Name * 8', 'demo _ designation .8', '02558989', '018455236', '7676', 'sa_sarker@yahoo.com', '	fsd							dsfsd fsd', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('10', 'demo _ Witness Name * 9', 'demo _ designation .9', '02558989', '018455236', '7676', 'sa_sarker@yahoo.com', '								dsafsd fsd fsdfsd', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('11', 'demo _ Witness Name * 10', 'demo _ designation .10', '02558989', '018455236', '', 'sa_sarker@yahoo.com', '								ds fsdf sd', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('12', 'demo _ Witness Name * 11', 'demo _ designation .11', '02558989', '018455236', '7676', 'sa_sarker@yahoo.com', '						sdf sdwsre w', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('13', 'demo _ Witness Name * 12', 'demo _ designation .12', '', '456645', '', 'sa_sarker@yahoo.com', '						das dasdassdsa', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('14', 'demo _ Witness Name * 13', 'demo _ designation .13', '', '018455236', '', 'sa_sarker@yahoo.com', '								sdfdsfsdfsd', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('15', 'demo _ Witness Name * 14', 'demo _ designation .14', '', '018455236', '', 'sa_sarker@yahoo.com', 'awsedasda		', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('16', 'demo _ Witness Name * 15', 'demo _ designation .15', '', '018455236', '', 'sa_sarker@yahoo.com', '							dsfds fsdfds	', '				  			sdfs	', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('17', 'demo _ Witness Name * 16', 'demo _ designation .16', '', '018455236', '', 'sa_sarker@yahoo.com', '							edfdsf sdfsdf	', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('18', 'demo _ Witness Name * 17', 'demo _ designation .17', '', '018455236', '', 'sa_sarker@yahoo.com', '								dsfdsfds', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('19', 'demo _ Witness Name * 18', 'demo _ designation .18', '', '018455236', '', 'sakerbd@gmail.com', '								dsf sdsdfsdf', '				  				', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('20', 'demo _ Witness Name * 19', 'demo _ designation .19', '', '018455236', '', 'sa_sarker@yahoo.com', '							fdsfsdf	dsfsd fdsfsd', '				  				', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_parliament`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_parliament`;
CREATE TABLE `tbl_parliament` (
  `parliament_id` smallint(6) DEFAULT NULL,
  `parliament` varchar(50) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_parliament
-- ----------------------------
INSERT INTO `tbl_parliament` VALUES ('1', '1st Parliament', '1', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('2', 'à¦ªà§à¦°à¦¥à¦® à¦¸à¦‚à¦¸à¦¦', '2', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('3', '2nd Parliament', '1', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('4', '3rd Parliament', '1', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('5', 'à¦¬à¦¿à¦¶à§‡à¦·à¦œà§à¦ž à¦‰à¦ªà¦¦à§‡à¦·à§à¦Ÿà¦¾', '2', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('10', 'à¦œà¦¾à¦¤à§€à¦¯à¦¼ à¦¸à¦‚à¦¸à¦¦', '2', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('11', '4th Parliament', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_parliament_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_parliament_member`;
CREATE TABLE `tbl_parliament_member` (
  `member_id` int(11) DEFAULT NULL,
  `member_name` varchar(250) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `constituency` varchar(250) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_active` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_parliament_member
-- ----------------------------
INSERT INTO `tbl_parliament_member` VALUES ('1', 'dddddddddddddddddd', 'd', 'c1111', '158578774', '5445454554', '35464646', 'saker@yahho.com', '																												a																												', '																												c																												', '1', '1', '0', '1');
INSERT INTO `tbl_parliament_member` VALUES ('2', 'X', 'X', 'gdfgdg', '555555555555', '665588', '', 'sakerbd@gmail.com', '																', '																', '1', '1', '0', '1');
INSERT INTO `tbl_parliament_member` VALUES ('3', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦…à¦§à§€à¦¨à§‡ à¦•à¦®à¦¿à¦Ÿà¦¿ à¦¸à¦šà¦¿à¦¬à¦¾à¦²à¦¯à¦¼', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦…à¦§à§€à¦¨à§‡ à¦•à¦®à¦¿à¦Ÿà¦¿ à¦¸à¦šà¦¿à¦¬à¦¾à¦²à¦¯à¦¼', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿', '012121212', '1212121', '', '', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿																', 'à¦¦à§‡à¦–à¦¾à¦¶à§‹à¦¨à¦¾ à¦•à¦°à¦¾à¦° à¦œà¦¨à§à¦¯ à¦•à¦®à¦¿à¦Ÿà¦¿																', '2', '1', '0', '1');
INSERT INTO `tbl_parliament_member` VALUES ('4', 'member2', 'parliament member', 'Constituency ', '575544545', '01654546', '', '', '								', '								', '1', '1', '0', '8');

-- ----------------------------
-- Table structure for `tbl_pdf_file`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_pdf_file`;
CREATE TABLE `tbl_pdf_file` (
  `pdf_id` int(11) DEFAULT NULL,
  `pdf_type` smallint(6) DEFAULT NULL,
  `pdf_path` varchar(250) DEFAULT NULL,
  `comments` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_pdf_file
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_receive_evidence_d`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_receive_evidence_d`;
CREATE TABLE `tbl_receive_evidence_d` (
  `inquiry_id` int(11) DEFAULT NULL,
  `rec_evidence_details_id` bigint(20) DEFAULT NULL,
  `evidence_type` varchar(100) DEFAULT NULL,
  `title` longtext,
  `submitted_by` varchar(200) DEFAULT NULL,
  `receive_date` datetime DEFAULT NULL,
  `ref_no` varchar(150) DEFAULT NULL,
  `ref_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `doc_type` varchar(50) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_receive_evidence_d
-- ----------------------------
INSERT INTO `tbl_receive_evidence_d` VALUES ('2', '3', 'WrittenEvidence', 'bre', 'vfe', '0205-12-11 00:00:00', '', '0000-00-00 00:00:00', '2013-11-16 16:58:26', 'evidence/3.doc', 'application/msword', '0', '1');
INSERT INTO `tbl_receive_evidence_d` VALUES ('9', '4', 'WrittenEvidence', 'data_ title evidance registration', 'data_ submitted by evidance registration', '2014-01-31 00:00:00', '4', '0000-00-00 00:00:00', '2014-01-30 14:05:48', 'evidence/4.', '', '0', '1');
INSERT INTO `tbl_receive_evidence_d` VALUES ('2', '1', 'WrittenEvidence', 'assa', 'asas', '2005-11-12 00:00:00', 'qwqw', '2008-12-12 00:00:00', '2013-11-13 11:42:34', 'evidence/1.docx', 'application/vnd.openxmlformats-officedocument.word', '0', '1');
INSERT INTO `tbl_receive_evidence_d` VALUES ('2', '2', 'WrittenEvidence', 'tttt', 'tt', '2005-11-12 00:00:00', '333', '2008-12-12 00:00:00', '2013-11-13 11:47:30', 'evidence/2.jpeg', 'image/jpeg', '0', '1');
INSERT INTO `tbl_receive_evidence_d` VALUES ('3', '5', 'WrittenEvidence', 'Title* 1', 'Submitted By* 1', '2014-02-09 00:00:00', 'Ref No. 1', '2014-02-10 00:00:00', '2014-02-08 12:31:05', 'evidence/5.', '', '0', '1');
INSERT INTO `tbl_receive_evidence_d` VALUES ('3', '6', 'Multimedia', 'Title* 2', 'Submitted By* 2', '2014-02-10 00:00:00', 'Ref No. 2', '2014-02-11 00:00:00', '2014-02-08 12:31:05', 'evidence/6.', '', '0', '1');

-- ----------------------------
-- Table structure for `tbl_receive_evidence_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_receive_evidence_master`;
CREATE TABLE `tbl_receive_evidence_master` (
  `inquiry_id` int(11) DEFAULT NULL,
  `receive_deadline_date` datetime DEFAULT NULL,
  `remarks` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_receive_evidence_master
-- ----------------------------
INSERT INTO `tbl_receive_evidence_master` VALUES ('9', '2014-01-23 00:00:00', '			    			    something remark			  		4	  ', '0', '1');
INSERT INTO `tbl_receive_evidence_master` VALUES ('2', '2013-11-06 00:00:00', '			    			    			    			    			    			    			    			    			  			  			  			  			  			  			  			  ', '0', '1');
INSERT INTO `tbl_receive_evidence_master` VALUES ('8', '2013-11-06 00:00:00', '			  er dsf sdf dsfdfds 			  ', '0', '1');
INSERT INTO `tbl_receive_evidence_master` VALUES ('3', '2014-02-09 00:00:00', '	Something remarks		    			  ', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_adviser`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_adviser`;
CREATE TABLE `tbl_record_of_decision_adviser` (
  `meeting_notice_id` int(11) DEFAULT NULL,
  `adviser_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_adviser
-- ----------------------------
INSERT INTO `tbl_record_of_decision_adviser` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_adviser` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_adviser` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_adviser` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_committee`;
CREATE TABLE `tbl_record_of_decision_committee` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `member_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_committee
-- ----------------------------
INSERT INTO `tbl_record_of_decision_committee` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('12', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('17', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('17', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('18', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('18', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('19', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('19', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('21', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('21', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('23', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('24', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('24', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('25', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('25', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_inquiry_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_inquiry_tag`;
CREATE TABLE `tbl_record_of_decision_inquiry_tag` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_inquiry_tag
-- ----------------------------
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('1', '2', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('2', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('3', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('4', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('5', '3', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('6', '1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('7', '6', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('12', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('11', '4', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('13', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('14', '3', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('15', '4', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('16', '7', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('17', '3', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('18', '3', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('19', '3', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('20', '7', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('21', '3', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('22', '4', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('23', '4', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('24', '7', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('25', '7', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('26', '9', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('27', '8', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('28', '10', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('29', '7', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('30', '13', '0', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_invitees`;
CREATE TABLE `tbl_record_of_decision_invitees` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `invitees_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_invitees
-- ----------------------------
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('12', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('13', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('13', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_logistic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_logistic`;
CREATE TABLE `tbl_record_of_decision_logistic` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `logistic_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_logistic
-- ----------------------------
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('7', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('8', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('10', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('11', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('14', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('14', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('15', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('15', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('16', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('16', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('17', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('18', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('19', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('20', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('20', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('21', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('22', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('22', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('23', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('23', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('24', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('24', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('24', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('24', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('25', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('25', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('25', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('25', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('26', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('26', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('27', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('27', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('28', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('28', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('29', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('29', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('30', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('30', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('30', '4', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_master`;
CREATE TABLE `tbl_record_of_decision_master` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `chair_member_id` smallint(6) DEFAULT NULL,
  `private_business` longtext,
  `public_business` longtext,
  `result_of_deliberation` longtext,
  `remarks` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_master
-- ----------------------------
INSERT INTO `tbl_record_of_decision_master` VALUES ('1', '0', '', '', '', null, '1', '1', '1', '2014-01-29 11:01:57', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('2', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('3', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('4', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('5', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('6', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('7', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('8', '0', '', '', '', null, '2', '2', '1', '2014-02-02 16:27:09', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('9', '0', '', '', '', null, '2', '2', '1', '2014-01-29 11:04:17', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('10', '0', '', '', '', null, '1', '1', '1', '2014-02-09 18:43:32', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('11', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('12', '0', '', '', '', null, '0', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('13', '0', '', '', '', null, '0', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('14', '0', '', '', '', null, '1', '1', '1', '2014-02-04 11:59:36', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('15', '0', '', '', '', null, '1', '1', '1', '2014-02-09 18:43:37', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('16', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('17', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('18', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('19', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('20', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('21', '0', '', '', '', null, '2', '2', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('22', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('23', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('24', '0', '', '', '', null, '1', '1', '1', '2014-02-09 18:43:48', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('25', '0', '', '', '', null, '1', '1', '1', '2014-02-09 18:43:25', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('26', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('27', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('28', '1', 'dsfsfsd', 'fsd', 'fsdf', 'remarks', '1', '1', '0', null, '1', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('29', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('30', '0', '', '', '', null, '1', '1', '0', null, '0', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_notification`;
CREATE TABLE `tbl_record_of_decision_notification` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `notification_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_notification
-- ----------------------------
INSERT INTO `tbl_record_of_decision_notification` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('12', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('12', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('12', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('12', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('13', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('13', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('13', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('13', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('23', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('30', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_officer`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_officer`;
CREATE TABLE `tbl_record_of_decision_officer` (
  `meeting_notice_id` int(11) DEFAULT NULL,
  `officer_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_officer
-- ----------------------------
INSERT INTO `tbl_record_of_decision_officer` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('7', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('8', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('10', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('11', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('14', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('14', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('15', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('15', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('16', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('16', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('20', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('20', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('22', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('22', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('26', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('26', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('27', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('27', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('28', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('28', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('29', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('29', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('30', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_officer` VALUES ('30', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_witness`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_witness`;
CREATE TABLE `tbl_record_of_decision_witness` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `witness_type` varchar(1) DEFAULT NULL,
  `witness_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_witness
-- ----------------------------
INSERT INTO `tbl_record_of_decision_witness` VALUES ('11', 'O', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('12', '1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('12', '2', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('13', '1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('13', '2', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('14', 'O', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('14', 'T', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('15', 'T', '4', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('23', 'O', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('30', 'T', '5', '0', '1');

-- ----------------------------
-- Table structure for `tbl_request_evidence`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_request_evidence`;
CREATE TABLE `tbl_request_evidence` (
  `request_evidence_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `metting_notice_id` int(11) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `cover_letter` longtext,
  `remarks` longtext,
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_request_evidence
-- ----------------------------
INSERT INTO `tbl_request_evidence` VALUES ('2', '5', '0', '2014-01-30 00:00:00', 'à¦†à¦¬à¦¾à¦°&lt;br&gt;&lt;br&gt;			    			  ', '			    			  ', '2', '2', '0', null, '0', '2014-01-26 19:15:52', '1');
INSERT INTO `tbl_request_evidence` VALUES ('1', '3', '0', '2013-11-18 00:00:00', '			    			    			    &lt;b&gt;dsv dfs sd sdf sdf sdfsdfs dfsd sdf sdf sd&lt;/b&gt;&lt;br&gt;			  			  ', 'eqwwwwwwwwwwwwww		    			  			  			  			  ', '1', '1', '0', null, '0', '2013-11-16 16:49:38', '1');

-- ----------------------------
-- Table structure for `tbl_session`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_session`;
CREATE TABLE `tbl_session` (
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `session` varchar(50) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_session
-- ----------------------------
INSERT INTO `tbl_session` VALUES ('2', '1', 'à¦ªà§à¦°à¦¥à¦® à¦…à¦§à¦¿à¦¬à§‡à¦¶à¦¨', '2', '0', '1');
INSERT INTO `tbl_session` VALUES ('2', '2', 'à¦¦à§à¦¬à¦¿à¦¤à¦¿à§Ÿ à¦…à¦§à¦¿à¦¬à§‡à¦¶à¦¨', '2', '0', '1');
INSERT INTO `tbl_session` VALUES ('1', '3', '2nd Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('11', '4', '1st Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('11', '5', '3rd Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('11', '6', '111', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('4', '7', '1st Session', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_specialist_adviser`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_specialist_adviser`;
CREATE TABLE `tbl_specialist_adviser` (
  `adviser_id` int(11) DEFAULT NULL,
  `adviser_name` varchar(150) DEFAULT NULL,
  `adviser_organization` varchar(250) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_specialist_adviser
-- ----------------------------
INSERT INTO `tbl_specialist_adviser` VALUES ('1', 'Y', 'a1', 'd1', '012477', '256464', '12112', 'sa_sarker10@yahoo.com', '								5454								', '1', '0', '1');
INSERT INTO `tbl_specialist_adviser` VALUES ('2', 'X', '1111', '1111', '111', '111', '11111', 'sa_sarker10@yahoo.com', '	sssssssss							', '1', '0', '1');
INSERT INTO `tbl_specialist_adviser` VALUES ('3', 'à¦• à¦• à¦• à¦•', 'à¦• ', 'à¦•', '12121212121', '', '', '', '								', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_sub_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sub_committee`;
CREATE TABLE `tbl_sub_committee` (
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `sub_committee_name` varchar(150) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sub_committee
-- ----------------------------
INSERT INTO `tbl_sub_committee` VALUES ('1', '1', 'PAC Sub Committee -1', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('2', '2', 'à¦¸à¦¾à¦¬-à¦•à¦®à¦¿à¦Ÿà¦¿-à§§', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_sub_committee_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sub_committee_member`;
CREATE TABLE `tbl_sub_committee_member` (
  `sub_committee_member_id` int(11) DEFAULT NULL,
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `committee_member_id` int(11) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sub_committee_member
-- ----------------------------
INSERT INTO `tbl_sub_committee_member` VALUES ('1', '1', '1', '1', '1', '0', '1');
INSERT INTO `tbl_sub_committee_member` VALUES ('2', '1', '1', '1', '1', '0', '1');
INSERT INTO `tbl_sub_committee_member` VALUES ('3', '1', '1', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_transcript`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transcript`;
CREATE TABLE `tbl_transcript` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `transcript_id` bigint(20) DEFAULT NULL,
  `file_title` varchar(200) DEFAULT NULL,
  `file_type` varchar(20) DEFAULT NULL,
  `file_path` varchar(400) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transcript
-- ----------------------------
INSERT INTO `tbl_transcript` VALUES ('3', '1', 'Multimedia', 'audio/mpeg', 'multimedia/1.mp3', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('3', '2', 'Document', 'application/msword', 'multimedia/2.doc', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('3', '3', 'vxcv', 'application/octet-st', 'multimedia/3.log', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('5', '4', 'Test', 'audio/mpeg', 'multimedia/4.mp3', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('5', '5', 'doc', 'application/pdf', 'multimedia/5.pdf', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('5', '6', 'vfae', 'application/pdf', 'multimedia/6.pdf', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('14', '7', 'test_pdf_file', 'application/pdf', 'multimedia/7.pdf', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('14', '8', 'music', 'audio/x-ms-wma', 'multimedia/8.wma', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('5', '9', 'bethoven', 'audio/x-ms-wma', 'multimedia/9.wma', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('5', '10', 'song.wma', 'audio/x-ms-wma', 'multimedia/10.wma', '0', '1');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `is_parliament_member` smallint(6) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('1', 'admin', 'a1', '0', '2014-02-17 20:18:44', '1');
INSERT INTO `tbl_user` VALUES ('2', 'bipl', 'b1', '0', '2014-02-08 17:22:02', '1');
INSERT INTO `tbl_user` VALUES ('3', 'test', '123456', '1', '2013-11-26 12:00:59', '1');
INSERT INTO `tbl_user` VALUES ('4', 'member1', 'member1', '1', '2014-02-11 20:26:26', '1');
INSERT INTO `tbl_user` VALUES ('5', 'user1', 'user1', '1', '2014-02-09 16:11:31', '1');
INSERT INTO `tbl_user` VALUES ('6', 'user2', 'user2', '1', '2014-02-09 16:12:47', '1');
INSERT INTO `tbl_user` VALUES ('7', 'user3', 'user3', '0', '2014-02-09 16:24:14', '1');
INSERT INTO `tbl_user` VALUES ('8', 'member2', 'member2', '1', '2014-02-09 17:26:43', '1');

-- ----------------------------
-- Table structure for `tbl_user_committee_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_committee_permission`;
CREATE TABLE `tbl_user_committee_permission` (
  `committee_id` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_committee_permission
-- ----------------------------
INSERT INTO `tbl_user_committee_permission` VALUES ('1', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('2', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('1', '2');
INSERT INTO `tbl_user_committee_permission` VALUES ('3', '2');
INSERT INTO `tbl_user_committee_permission` VALUES ('3', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('5', '3');
INSERT INTO `tbl_user_committee_permission` VALUES ('4', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('7', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('2', '4');
INSERT INTO `tbl_user_committee_permission` VALUES ('3', '4');
INSERT INTO `tbl_user_committee_permission` VALUES ('7', '4');
INSERT INTO `tbl_user_committee_permission` VALUES ('1', '4');
INSERT INTO `tbl_user_committee_permission` VALUES ('5', '4');

-- ----------------------------
-- Table structure for `tbl_user_group_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_group_permission`;
CREATE TABLE `tbl_user_group_permission` (
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_group_permission
-- ----------------------------
INSERT INTO `tbl_user_group_permission` VALUES ('2', '4');
INSERT INTO `tbl_user_group_permission` VALUES ('1', '1');
INSERT INTO `tbl_user_group_permission` VALUES ('3', '2');
INSERT INTO `tbl_user_group_permission` VALUES ('5', '4');
INSERT INTO `tbl_user_group_permission` VALUES ('3', '4');
INSERT INTO `tbl_user_group_permission` VALUES ('4', '4');
INSERT INTO `tbl_user_group_permission` VALUES ('1', '4');

-- ----------------------------
-- Table structure for `tbl_venue`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_venue`;
CREATE TABLE `tbl_venue` (
  `venue_id` smallint(6) DEFAULT NULL,
  `venue_name` varchar(200) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_venue
-- ----------------------------
INSERT INTO `tbl_venue` VALUES ('1', 'Conference Room -1', '1', '0', '1');
INSERT INTO `tbl_venue` VALUES ('2', 'à¦•à§‡à¦¬à¦¿à¦¨à§‡à¦Ÿ à¦•à¦•à§à¦·', '2', '0', '1');
INSERT INTO `tbl_venue` VALUES ('3', 'Venue -1', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_year_info`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_year_info`;
CREATE TABLE `tbl_year_info` (
  `year_id` int(11) DEFAULT NULL,
  `year_name` varchar(16) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_year_info
-- ----------------------------
INSERT INTO `tbl_year_info` VALUES ('2001', '2001');
INSERT INTO `tbl_year_info` VALUES ('2002', '2002');
INSERT INTO `tbl_year_info` VALUES ('2003', '2003');
INSERT INTO `tbl_year_info` VALUES ('2004', '2004');
INSERT INTO `tbl_year_info` VALUES ('2005', '2005');
INSERT INTO `tbl_year_info` VALUES ('2006', '2006');
INSERT INTO `tbl_year_info` VALUES ('2007', '2007');
INSERT INTO `tbl_year_info` VALUES ('2008', '2008');
INSERT INTO `tbl_year_info` VALUES ('2009', '2009');
INSERT INTO `tbl_year_info` VALUES ('2010', '2010');
INSERT INTO `tbl_year_info` VALUES ('2011', '2011');
INSERT INTO `tbl_year_info` VALUES ('2012', '2012');
INSERT INTO `tbl_year_info` VALUES ('2013', '2013');
INSERT INTO `tbl_year_info` VALUES ('2014', '2014');
INSERT INTO `tbl_year_info` VALUES ('2015', '2015');
INSERT INTO `tbl_year_info` VALUES ('2016', '2016');
INSERT INTO `tbl_year_info` VALUES ('2017', '2017');
INSERT INTO `tbl_year_info` VALUES ('2018', '2018');
INSERT INTO `tbl_year_info` VALUES ('2019', '2019');
INSERT INTO `tbl_year_info` VALUES ('2020', '2020');
INSERT INTO `tbl_year_info` VALUES ('2021', '2021');
INSERT INTO `tbl_year_info` VALUES ('2022', '2022');
INSERT INTO `tbl_year_info` VALUES ('2023', '2023');
INSERT INTO `tbl_year_info` VALUES ('2024', '2024');

-- ----------------------------
-- Procedure structure for `rpt_brifeing_note`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_brifeing_note`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_brifeing_note`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
		SELECT
						c.committee_name,
						DATE_FORMAT(mn.en_date, '%d-%m-%Y') en_date,
						bn.introduction,
						bn.key_issue,
						bn.advisor_question,
						bn.witness_question,
						bn.written_evidence,
						p.parliament,
						s.session,
						bn.is_issued
		FROM tbl_briefing_note bn
		LEFT OUTER JOIN tbl_meeting_notice_master mn ON bn.metting_notice_id = mn.metting_notice_id
		LEFT OUTER JOIN tbl_meeting_notice_inquiry_tag i ON i.metting_notice_id = mn.metting_notice_id
		LEFT OUTER JOIN tbl_committee c ON mn.committee_id = c.committee_id		
		LEFT OUTER JOIN tbl_inquiry_master im ON im.inquiry_id = i.inquiry_id
		LEFT OUTER JOIN tbl_parliament p ON p.parliament_id = im.parliament_id AND p.is_delete = 0
		LEFT OUTER JOIN tbl_session s ON s.session_id = im.session_id AND s.is_delete = 0
		WHERE mn.metting_notice_id=p_meeting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_follow_up`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_follow_up`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_follow_up`(p_inquiry_report_followup_id int)
BEGIN
	#Routine body goes here...
	SELECT 	i.inquiry_no, 
					i.inquiry_title, 
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date,
					m.report_no,
					DATE_FORMAT(m.report_date,'%d-%m-%Y') report_date,
					m.title,
					m.executive_summary,
					f.background,
					DATE_FORMAT(f.followup_date,'%d-%m-%Y') followup_date,
					f.followup_no,					
					r.recommendation,
					r.organization_response,
					r.action_taken,
					r.comment,
					m.introduction,
					m.issues,
					c.committee_name,
					p.parliament,
					s.session,
					f.is_approved
					
	FROM tbl_follow_up_master f
	INNER JOIN tbl_inquiry_report_master m ON m.inquiry_report_id = f.inquiry_report_id AND m.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = f.committee_id AND c.is_delete = 0
	INNER JOIN tbl_parliament p ON p.parliament_id = i.parliament_id AND p.is_delete = 0
	INNER JOIN tbl_session s ON s.session_id = i.session_id AND s.is_delete = 0
	LEFT OUTER JOIN tbl_follow_up_recommendation r ON r.inquiry_report_followup_id = f.inquiry_report_followup_id AND r.is_delete = 0	
	WHERE f.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND f.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_inquiry_master`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_inquiry_master`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_inquiry_master`(p_inquiry_id int)
BEGIN
	#Routine body goes here...
	DROP TEMPORARY TABLE IF EXISTS doc_pre;
	CREATE TEMPORARY TABLE doc_pre AS
	(
		SELECT 	d.doc_reg_id, 
						CONCAT(m.doc_title,': ',m.doc_reg_no) doc_title
		FROM tbl_inquiry_doc_tag d
		INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = d.doc_reg_id AND m.is_delete = 0
		WHERE d.inquiry_id = p_inquiry_id
		AND d.is_delete = 0
	);	


	DROP TEMPORARY TABLE IF EXISTS min_pre;
	CREATE TEMPORARY TABLE min_pre AS
	(
		SELECT 	d.ministry_id, 
						m.ministry_name
		FROM tbl_inquiry_ministry d
		INNER JOIN tbl_ministry m ON m.ministry_id = d.ministry_id AND m.is_delete = 0
		WHERE d.inquiry_id = p_inquiry_id
		AND d.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS org_pre;
	CREATE TEMPORARY TABLE org_pre AS
	(
		SELECT 	d.organization_id, 
						m.organization_name
		FROM tbl_inquiry_organization d
		INNER JOIN tbl_organization m ON m.organization_id = d.organization_id AND m.is_delete = 0
		WHERE d.inquiry_id = p_inquiry_id
		AND d.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS oth_pre;
	CREATE TEMPORARY TABLE oth_pre AS
	(
		SELECT 	d.others_org_ministry_id, 
						m.others_org_ministry
		FROM tbl_inquiry_others d
		INNER JOIN tbl_others m ON m.others_org_ministry_id = d.others_org_ministry_id AND m.is_delete = 0
		WHERE d.inquiry_id = p_inquiry_id
		AND d.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS witness_pre;

	CREATE TEMPORARY TABLE witness_pre AS
	(
		SELECT 	t.witness_id, 
						m.ministry_member_name member_name
		FROM tbl_inquiry_witnesses t		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M' AND t.is_delete = 0
		WHERE t.is_delete = 0		
		AND t.inquiry_id = p_inquiry_id
	);

	INSERT INTO witness_pre
	SELECT 	t.witness_id, 
					o.organization_member_name member_name
	FROM tbl_inquiry_witnesses t	
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O' AND t.is_delete = 0
	WHERE t.is_delete = 0
	AND t.inquiry_id = p_inquiry_id;

	DROP TEMPORARY TABLE IF EXISTS oth_witness_pre;

	CREATE TEMPORARY TABLE oth_witness_pre AS
	(
		SELECT 	t.witness_id, 
						CONCAT(m.witness_name,', ',m.designation) witness_name
		FROM tbl_inquiry_other_witnessess t		
		INNER JOIN tbl_other_witnessess m ON m.witness_id = t.witness_id AND t.is_delete = 0
		WHERE t.is_delete = 0		
		AND t.inquiry_id = p_inquiry_id
	);


	DROP TEMPORARY TABLE IF EXISTS sch_date_pre;

	CREATE TEMPORARY TABLE sch_date_pre AS
	(
		SELECT 	t.inquiry_id, 
						DATE_FORMAT(t.schedule_date,'%d-%m-%Y') schedule_date
		FROM tbl_inquiry_scheduling t				
		WHERE t.is_delete = 0		
		AND t.inquiry_id = p_inquiry_id
	);


	DROP TEMPORARY TABLE IF EXISTS act_date_pre;

	CREATE TEMPORARY TABLE act_date_pre AS
	(
		SELECT 	i.inquiry_id, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_meeting_notice_inquiry_tag i ON i.metting_notice_id = t.metting_notice_id 
		WHERE t.is_delete = 0		
		AND i.inquiry_id = p_inquiry_id
	);

	SELECT 	i.inquiry_id, 
					i.inquiry_no,
					i.inquiry_title,					
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date,
					DATE_FORMAT(i.issued_date,'%d-%m-%Y') issued_date,
					p.parliament,
					s.session,
					c.committee_name,
					CONCAT(i.proposed_month,', ',i.proposed_year) proposed_date,					
					(SELECT GROUP_CONCAT(doc_title) FROM doc_pre) doc_title,
					(SELECT GROUP_CONCAT(ministry_name) FROM min_pre) ministry_name,
					(SELECT GROUP_CONCAT(organization_name) FROM org_pre) organization_name,
					(SELECT GROUP_CONCAT(others_org_ministry) FROM oth_pre) others_org_ministry,
					(SELECT GROUP_CONCAT(member_name) FROM witness_pre) member_name,
					(SELECT GROUP_CONCAT(witness_name) FROM oth_witness_pre) witness_name,
					(SELECT GROUP_CONCAT(schedule_date) FROM sch_date_pre) schedule_date,
					(SELECT GROUP_CONCAT(en_date) FROM act_date_pre) actual_date,
					i.is_issued
	FROM tbl_inquiry_master i
	LEFT OUTER JOIN tbl_parliament p ON i.parliament_id = p.parliament_id AND p.is_delete = 0
	LEFT OUTER JOIN tbl_session s ON s.session_id = i.session_id AND s.is_delete = 0
	LEFT OUTER JOIN tbl_committee c ON c.committee_id = i.committee_id AND c.is_delete = 0
	WHERE i.is_delete = 0
	AND i.inquiry_id = p_inquiry_id;

	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_inquiry_report`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_inquiry_report`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_inquiry_report`(p_inquiry_report_id int)
BEGIN
	#Routine body goes here...
	SELECT 	m.inquiry_report_id,
          i.inquiry_no, 
					i.inquiry_title, 
					DATE_FORMAT(i.create_date,'%d %M %Y') create_date,
					m.report_no,
					DATE_FORMAT(m.report_date,'%d %M %Y') report_date,
					m.title,
					m.executive_summary,
					r.recommendation,
					r.recom_no,
					m.introduction,
					m.issues,
					c.committee_name,
					p.parliament,
					s.session,
					m.is_approved
	FROM tbl_inquiry_report_master m
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	INNER JOIN tbl_parliament p ON p.parliament_id = i.parliament_id AND p.is_delete = 0
	INNER JOIN tbl_session s ON s.session_id = i.session_id AND s.is_delete = 0
	LEFT OUTER JOIN tbl_inquiry_report_recommendation r ON r.inquiry_report_id = m.inquiry_report_id AND r.is_delete = 0
	WHERE m.inquiry_report_id = p_inquiry_report_id
	AND m.is_delete = 0
	ORDER BY r.recom_no ASC;
	#select * from tbl_inquiry_report_master m where m.committee_id
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_meeting_notice`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_meeting_notice`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_meeting_notice`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
SELECT
c.committee_name,
mn.en_date,
mn.bn_date,
mn.time,
v.venue_name,
mn.private_business_before,
mn.public_business,
mn.private_business_after,
mn.is_issued,
mn.is_approved
FROM tbl_meeting_notice_master mn INNER JOIN tbl_committee c ON c.committee_id = mn.committee_id 
LEFT OUTER JOIN tbl_venue v ON v.venue_id=mn.venue_id
WHERE mn.metting_notice_id=p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_record_of_decision`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_record_of_decision`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_record_of_decision`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
	DROP TEMPORARY TABLE IF EXISTS member_pre;
	CREATE TEMPORARY TABLE member_pre AS
	(
		SELECT 	c.metting_notice_id, 
						m.member_name 
		FROM tbl_record_of_decision_committee c
		INNER JOIN tbl_committee_member m ON m.committee_member_id = c.member_id AND m.is_delete = 0
		WHERE c.metting_notice_id = p_meeting_notice_id
		AND c.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS logistic_pre;
	CREATE TEMPORARY TABLE logistic_pre AS
	(
		SELECT 	c.metting_notice_id, 
						m.logistic_member_name
		FROM tbl_record_of_decision_logistic c
		INNER JOIN tbl_logistic_member m ON m.logistic_member_id = c.logistic_id AND m.is_delete = 0
		WHERE c.metting_notice_id = p_meeting_notice_id
		AND c.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS witness_pre;

	CREATE TEMPORARY TABLE witness_pre AS
	(
		SELECT t.metting_notice_id, m.ministry_member_name member_name
		FROM tbl_record_of_decision_witness t		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.metting_notice_id = p_meeting_notice_id
	);

	INSERT INTO witness_pre
	SELECT t.metting_notice_id, o.organization_member_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_meeting_notice_id;

	INSERT INTO witness_pre
	SELECT t.metting_notice_id, o.witness_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_meeting_notice_id;


	
	SELECT 	DATE_FORMAT(n.en_date, '%d-%m-%Y') en_date, 
					n.bn_date, 
					n.time, 
					v.venue_name,
					cm.member_name chairman_name,
					(SELECT GROUP_CONCAT(member_name) FROM member_pre) member_name,
					(SELECT GROUP_CONCAT(logistic_member_name) FROM logistic_pre) logistic_member_name,
					(SELECT GROUP_CONCAT(member_name) FROM witness_pre) witness_member_name,
					m.private_business,
					m.public_business,
					m.result_of_deliberation,
					c.committee_name,
					p.parliament,
					s.session,
					m.is_issued
	FROM tbl_record_of_decision_master m
	LEFT OUTER JOIN tbl_meeting_notice_master n ON n.metting_notice_id = m.metting_notice_id AND n.is_delete = 0
	LEFT OUTER JOIN tbl_venue v ON v.venue_id = n.venue_id AND v.is_delete = 0
	LEFT OUTER JOIN tbl_committee_member cm ON cm.committee_member_id = m.chair_member_id AND cm.is_delete = 0
	LEFT OUTER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	LEFT OUTER JOIN tbl_meeting_notice_inquiry_tag i ON i.metting_notice_id = n.metting_notice_id
	LEFT OUTER JOIN tbl_inquiry_master im ON im.inquiry_id = i.inquiry_id
	LEFT OUTER JOIN tbl_parliament p ON p.parliament_id = im.parliament_id AND p.is_delete = 0
	LEFT OUTER JOIN tbl_session s ON s.session_id = im.session_id AND s.is_delete = 0
	WHERE m.metting_notice_id = p_meeting_notice_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `sp_upcoming_sittings`
-- ----------------------------
DROP PROCEDURE IF EXISTS `sp_upcoming_sittings`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `sp_upcoming_sittings`(p_committee_id smallint, p_language_id smallint)
BEGIN
	#Routine body goes here...
	SELECT 	m.sitting_no, 
					CONCAT(DATE_FORMAT(m.en_date,'%d-%m-%Y'),' at ',m.time) metting_date,
					v.venue_name,
					i.inquiry_no,
					i.inquiry_title					
	FROM tbl_meeting_notice_master m
	INNER JOIN tbl_venue v ON v.venue_id = m.venue_id
	LEFT OUTER JOIN tbl_meeting_notice_inquiry_tag t ON t.metting_notice_id = m.metting_notice_id
	LEFT OUTER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id	
	WHERE m.is_display = 1
	AND m.committee_id = p_committee_id
	AND m.language_id = p_language_id
	AND STR_TO_DATE(DATE_FORMAT(m.en_date,'%d-%m-%Y'), '%d-%m-%Y') >= STR_TO_DATE(DATE_FORMAT(NOW(),'%d-%m-%Y'), '%d-%m-%Y')
	AND m.is_delete = 0
	ORDER BY m.en_date ASC
	LIMIT 5;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_briefing_note
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_briefing_note t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.committee_id = p_committee_id
		AND t.language_id = p_language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, introduction, key_issue, advisor_question, witness_question, written_evidence, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_gid`(p_metting_notice_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, t.remarks
	FROM tbl_briefing_note t
	INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_i`(p_metting_notice_id int , p_introduction longtext , p_key_issue longtext , p_advisor_question longtext , p_witness_question longtext , p_written_evidence longtext , p_remarks longtext, p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_briefing_note c WHERE c.metting_notice_id = p_metting_notice_id) THEN		

		INSERT INTO tbl_briefing_note(metting_notice_id, introduction, key_issue, advisor_question, witness_question, written_evidence, remarks, language_id, committee_id, is_issued, is_delete, user_id)
		VALUES(p_metting_notice_id, p_introduction, p_key_issue, p_advisor_question, p_witness_question, p_written_evidence, p_remarks, p_language_id, p_committee_id, 0, 0, p_user_id);
	ELSE
		UPDATE tbl_briefing_note
		SET introduction = p_introduction, 
				key_issue = p_key_issue, 
				advisor_question = p_advisor_question, 
				witness_question = p_witness_question, 
				written_evidence = p_written_evidence,
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_issued`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_briefing_note
	SET is_issued = 1,
			issued_date = NOW(),
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_search`(p_start_date varchar(10), p_end_date varchar(10), p_ref_no varchar(100), p_sub_committee int, p_sitting_no varchar(100), p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.metting_notice_id, 
						m.sitting_no, 
						CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) ref_no, 
						DATE_FORMAT(m.en_date,'%d-%m-%Y') en_date, 
						m.bn_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						CASE 
							WHEN IFNULL(b.is_issued, 0) = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Briefing Note' class='gridbutton' disabled style='background-color: gray; width: 120px;'/>")
							ELSE CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Briefing Note' class='gridbutton' style='width: 120px;'/>") 
						END is_edit,
						CASE 
							WHEN IFNULL(b.is_issued, 0) = 1 THEN CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issue' class='gridbutton' disabled style='background-color: gray;'/>") 
							WHEN b.is_issued IS NULL THEN CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issue' class='gridbutton' disabled style='background-color: gray;'/>") 
							ELSE CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issue' class='gridbutton'/>")
						END is_issued,
						CONCAT("<input type='button' name='",m.metting_notice_id, "' value='Print View' class='gridbutton'/>") is_delete
		FROM tbl_meeting_notice_master m
		LEFT OUTER JOIN tbl_meeting_notice_inquiry_tag t ON t.metting_notice_id = m.metting_notice_id AND t.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		LEFT OUTER JOIN tbl_briefing_note b ON b.metting_notice_id = m.metting_notice_id 
		INNER JOIN tbl_venue v ON v.venue_id = m.venue_id
		WHERE m.committee_id = p_committee_id
		AND m.language_id = p_language_id
		AND (p_sub_committee = '' OR p_sub_committee = 0 OR m.sub_committee_id = p_sub_committee)
		AND (p_ref_no = '' OR CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_sitting_no = '' OR m.sitting_no LIKE CONCAT('%',p_sitting_no,'%'))
		AND (p_start_date='' OR p_end_date='' OR (m.en_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		AND m.is_display = 1
		ORDER BY m.en_date DESC
		/*
		SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_briefing_note t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.committee_id = p_committee_id
		AND t.language_id = p_language_id
		AND t.is_delete = 0
		*/
	);
	SET @sql = CONCAT("SELECT metting_notice_id, sitting_no, ref_no, en_date, bn_date, inquiry_no, inquiry_title, is_edit, is_issued, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.branch_id, p.branch
	FROM tbl_committee_branch p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_d`(p_branch_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_branch
	SET is_delete = 1,
			user_id = p_user_id
	WHERE branch_id = p_branch_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.branch_id, p.branch, p.language_id, l.language, p.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",p.branch_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.branch_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_branch p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT branch_id, branch, language_id, language, is_edit,is_delete FROM temp_all ORDER BY branch_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_gid`(p_branch_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.branch_id, p.branch, p.language_id, l.language, p.user_id
	FROM tbl_committee_branch p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.branch_id = p_branch_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_i`(p_branch_id smallint, p_branch varchar(100), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	IF NOT EXISTS(SELECT * FROM tbl_committee_branch c WHERE c.branch_id = p_branch_id) THEN

		SET p_branch_id = (SELECT IFNULL(MAX(branch_id), 0) + 1 FROM tbl_committee_branch);

		INSERT INTO tbl_committee_branch(branch_id, branch, language_id, is_delete, user_id)
		VALUES(p_branch_id, p_branch, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_branch
		SET branch = p_branch, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE branch_id = p_branch_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT c.committee_id, c.committee_name	
	FROM tbl_committee c 
	INNER JOIN tbl_language l ON l.language_id = c.language_id 
	INNER JOIN tbl_committee_section s ON s.section_id = c.section_id AND s.is_delete = 0
	INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id AND b.is_delete = 0
	WHERE c.language_id = p_language_id		
	AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_d`(p_committee_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE committee_id = p_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.committee_id, c.committee_name, c.short_name, c.section_id, s.section, c.branch_id, b.branch, c.contact_person, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.committee_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.committee_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		INNER JOIN tbl_committee_section s ON s.section_id = c.section_id AND s.is_delete = 0
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT committee_id, committee_name, short_name, section_id, section, branch_id, branch, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY committee_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_gid`(p_committee_id smallint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.committee_id, c.committee_name, c.short_name, c.section_id, s.section, c.branch_id, b.branch, c.contact_person, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
		FROM tbl_committee c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		INNER JOIN tbl_committee_section s ON s.section_id = c.section_id
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id
		WHERE c.committee_id = p_committee_id		
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_i`(p_committee_id smallint, p_committee_name varchar(200), p_short_name varchar(50), p_section_id smallint, p_branch_id smallint, p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_committee c WHERE c.committee_id = p_committee_id) THEN

		SET p_committee_id = (SELECT IFNULL(MAX(committee_id), 0) + 1 FROM tbl_committee);

		INSERT INTO tbl_committee(committee_id, committee_name, short_name, section_id, branch_id, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_committee_id, p_committee_name, p_short_name, p_section_id, p_branch_id, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee
		SET committee_name = p_committee_name, 
				short_name = p_short_name, 
				section_id = p_section_id, 
				branch_id = p_branch_id, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = user_id
		WHERE committee_id = p_committee_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_c`(p_committee_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	m.committee_member_id, 
					CONCAT(m.member_name,' (',m.designation, ')') member_name
	FROM tbl_committee_member m 	
	WHERE m.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_d`(p_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE committee_member_id = p_committee_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.committee_member_id, m.committee_id, t.committee_name, m.member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.committee_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.committee_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_member m 
		INNER JOIN tbl_committee t ON t.committee_id = m.committee_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.committee_id = p_committee_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT committee_member_id, committee_id, committee_name, member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY committee_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_gid`(p_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.committee_member_id, m.committee_id, t.committee_name, m.member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_committee_member m 
	INNER JOIN tbl_committee t ON t.committee_id = m.committee_id
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.committee_member_id = p_committee_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_i`(p_committee_member_id int, p_committee_id smallint , p_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_committee_member m WHERE m.committee_member_id = p_committee_member_id) THEN
		SET p_committee_member_id = (SELECT IFNULL(MAX(committee_member_id), 0) + 1 FROM tbl_committee_member);

		INSERT INTO tbl_committee_member(committee_member_id, committee_id, member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_committee_member_id, p_committee_id, p_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_member
		SET committee_id = p_committee_id, 
				member_name = p_member_name, 
				designation = p_designation,
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments,
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE committee_member_id = p_committee_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_ii`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_ii`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_ii`(p_committee_member_id int, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_committee_member WHERE committee_member_id = p_committee_member_id AND committee_id = p_committee_id;

	INSERT INTO tbl_committee_member(committee_member_id, committee_id, member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
	SELECT p.member_id, p_committee_id, p.member_name, p.designation, p.phone, p.cell_phone, p.fax, p.email, p.address, p.comments, p.language_id, 0, p_user_id FROM tbl_parliament_member p
	WHERE p.member_id = p_committee_member_id 
	AND p.is_delete = 0
	AND p.is_active = 1;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_token`(p_keyword varchar(20), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.committee_member_id, m.member_name
	FROM tbl_committee_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE m.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND m.member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_committee_tag_auto`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_committee_tag_auto`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_committee_tag_auto`(p_language_id smallint, p_committee_id smallint)
BEGIN
	#Routine body goes here...	
	SELECT m.officer_id, o.officer_name
	FROM tbl_committee_officer_committee_tag m 
	INNER JOIN tbl_committee_officer o ON o.officer_id = m.officer_id
	INNER JOIN tbl_language l ON l.language_id = o.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE o.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_committee_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_committee_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_committee_tag_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.officer_id, 
						d.officer_name, 
						d.designation, 
						CONCAT("<input type='button' onClick='saveid(",t.officer_id,",",p_committee_id,",0,",p_user_id,");' name='basic' value='Remove From List' class='gridbutton'/>") is_edit
		FROM tbl_committee_officer_committee_tag t
		INNER JOIN tbl_committee_officer d ON d.officer_id = t.officer_id AND d.is_delete = 0
		WHERE t.committee_id = p_committee_id
		AND d.language_id = p_language_id
		AND t.is_delete = 0
	);
	INSERT INTO temp_all
	SELECT 	d.officer_id, 
					d.officer_name, 
					d.designation, 
					CONCAT("<input type='button' onClick='saveid(",d.officer_id,",",p_committee_id,",1,",p_user_id,");' name='basic' value='Add To List' class='gridbutton'/>") is_edit
	FROM tbl_committee_officer d 
	WHERE d.officer_id NOT IN (SELECT officer_id FROM tbl_committee_officer_committee_tag WHERE committee_id = p_committee_id)
	AND d.language_id = p_language_id
	AND d.is_delete = 0;

	SET @sql = CONCAT("SELECT officer_id, officer_name, designation, is_edit FROM temp_all ORDER BY officer_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_committee_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_committee_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_committee_tag_gid`(p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.officer_id, o.officer_name
	FROM tbl_committee_officer_committee_tag m 
	INNER JOIN tbl_committee_officer o ON o.officer_id = m.officer_id
	INNER JOIN tbl_language l ON l.language_id = o.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE o.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_committee_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_committee_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_committee_tag_i`(p_officer_id int, p_committee_id smallint, p_type smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF (p_type = 1) THEN		
		IF NOT EXISTS(SELECT * FROM tbl_committee_officer_committee_tag c WHERE c.officer_id = p_officer_id AND c.committee_id = p_committee_id) THEN		
			INSERT INTO tbl_committee_officer_committee_tag(officer_id, committee_id, is_delete, user_id)
			VALUES(p_officer_id, p_committee_id, 0, p_user_id);
		END IF;
	ELSE
		DELETE FROM tbl_committee_officer_committee_tag
		WHERE officer_id = p_officer_id
		AND committee_id = p_committee_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_committee_tag_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_committee_tag_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_committee_tag_token`(p_keyword varchar(20), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.officer_id, o.officer_name
	FROM tbl_committee_officer_committee_tag m 
	INNER JOIN tbl_committee_officer o ON o.officer_id = m.officer_id
	INNER JOIN tbl_language l ON l.language_id = o.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE o.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND o.officer_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_d`(p_officer_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_officer
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE officer_id = p_officer_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.officer_id, c.officer_name, c.branch_id, b.branch, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.officer_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.officer_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_officer c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT officer_id, officer_name, branch_id, branch, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY officer_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_gid`(p_officer_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.officer_id, c.officer_name, c.branch_id, b.branch, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
		FROM tbl_committee_officer c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id
		WHERE c.officer_id = p_officer_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_officer_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_officer_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_officer_i`(p_officer_id int, p_officer_name varchar(250), p_branch_id smallint, p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_committee_officer c WHERE c.officer_id = p_officer_id) THEN

		SET p_officer_id = (SELECT IFNULL(MAX(officer_id), 0) + 1 FROM tbl_committee_officer);

		INSERT INTO tbl_committee_officer(officer_id, officer_name, branch_id, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_officer_id, p_officer_name, p_branch_id, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_officer
		SET officer_name = p_officer_name,
				branch_id = p_branch_id, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = user_id
		WHERE officer_id = p_officer_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.section_id, p.section
	FROM tbl_committee_section p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_d`(p_section_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_section
	SET is_delete = 1,
			user_id = p_user_id
	WHERE section_id = p_section_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.section_id, p.section, p.language_id, l.language, p.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",p.section_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.section_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_section p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT section_id, section, language_id, language, is_edit,is_delete FROM temp_all ORDER BY section_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_gid`(p_section_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.section_id, p.section, p.language_id, l.language, p.user_id
	FROM tbl_committee_section p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.section_id = p_section_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_i`(p_section_id smallint, p_section varchar(100), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	IF NOT EXISTS(SELECT * FROM tbl_committee_section c WHERE c.section_id = p_section_id) THEN

		SET p_section_id = (SELECT IFNULL(MAX(section_id), 0) + 1 FROM tbl_committee_section);

		INSERT INTO tbl_committee_section(section_id, section, language_id, is_delete, user_id)
		VALUES(p_section_id, p_section, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_section
		SET section = p_section, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE section_id = p_section_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_d`(p_doc_reg_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_key_areas
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_gid`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT k.doc_reg_id, k.key_area, k.recommendations, k.result_forcasting, k.is_delete, k.user_id
	FROM tbl_document_key_areas k
	INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = k.doc_reg_id
	WHERE k.is_delete = 0
	AND k.doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_i`(p_doc_reg_id int, p_key_area longtext, p_recommendations longtext, p_result_forcasting longtext, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_document_key_areas c WHERE c.doc_reg_id = p_doc_reg_id) THEN

		INSERT INTO tbl_document_key_areas(doc_reg_id, key_area, recommendations, result_forcasting, is_delete, user_id)
		VALUES(p_doc_reg_id, p_key_area, p_recommendations, p_result_forcasting, 0, p_user_id);

	ELSE
		UPDATE tbl_document_key_areas
		SET key_area = p_key_area, 
				recommendations = p_recommendations, 
				result_forcasting = p_result_forcasting, 
				user_id = p_user_id
		WHERE doc_reg_id = p_doc_reg_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_search`(p_doc_reg_no varchar(50),  p_start_date datetime, p_end_date datetime, p_doc_title varchar(256), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.doc_reg_id, p.doc_reg_no, DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') doc_reg_date, p.doc_title, p.language_id, p.committee_id, c.committee_name, l.language, p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' style='width:100px' name='basic' value='Key Area Entry' class='gridbutton'/>") is_edit,
        CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' style='width:100px' name='basic' value='Print' class='gridbutton'/>") is_print							
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_doc_reg_no = '' OR p.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND (p_doc_title = '' OR p.doc_title LIKE CONCAT('%',p_doc_title,'%'))		
		AND p.is_delete = 0
		ORDER BY p.doc_reg_date DESC
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, language_id, committee_id, committee_name, language, is_edit, is_print FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_d`(p_doc_reg_detail_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_registration_details	
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_detail_id  = p_doc_reg_detail_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_gall`(p_doc_reg_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	d.doc_reg_detail_id, 
						d.doc_reg_id, 
						d.doc_title, 
						d.file_name, 
						d.doc_type, 
						d.user_id,
						CONCAT("<input type='button' onClick='popupwinid(",d.doc_reg_detail_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deletei(",d.doc_reg_detail_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='download(",d.doc_reg_detail_id,");' name='basic' value='View' class='gridbutton'/>") is_download
		FROM tbl_document_registration_details d
		INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = d.doc_reg_id AND m.is_delete = 0
		WHERE d.doc_reg_id  = p_doc_reg_id
		AND d.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_detail_id, doc_reg_id, doc_title, file_name, doc_type, user_id, is_edit, is_delete, is_download FROM temp_all ORDER BY doc_reg_detail_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_gid`(p_doc_reg_detail_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	d.doc_reg_detail_id, 
					d.doc_reg_id, 
					d.doc_title, 
					d.file_name, 
					d.doc_type, 					
					d.user_id
	FROM tbl_document_registration_details d
	INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = d.doc_reg_id AND m.is_delete = 0
	WHERE d.doc_reg_detail_id  = p_doc_reg_detail_id
	AND d.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_i`(p_doc_reg_detail_id bigint, p_doc_reg_id int, p_doc_title varchar(200), p_doc_type varchar(50), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_name varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = 'document/';
	IF NOT EXISTS(SELECT * FROM tbl_document_registration_details c WHERE c.doc_reg_detail_id = p_doc_reg_detail_id) THEN

		SET p_doc_reg_detail_id = (SELECT IFNULL(MAX(doc_reg_detail_id), 0) + 1 FROM tbl_document_registration_details);

		SET v_file_name = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

		INSERT INTO tbl_document_registration_details(doc_reg_detail_id, doc_reg_id, doc_title, file_name, doc_type, is_delete, user_id)
		VALUES(p_doc_reg_detail_id, p_doc_reg_id, p_doc_title, v_file_name, p_doc_type, 0, p_user_id);
	ELSE
		IF(TRIM(p_doc_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_name = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

			UPDATE tbl_document_registration_details
			SET doc_reg_id = p_doc_reg_id, 
					doc_title = p_doc_title, 
					file_name = v_file_name,
					doc_type = p_doc_type, 
					user_id = p_user_id
			WHERE doc_reg_detail_id = p_doc_reg_detail_id;  
		ELSE      
			SET v_file_name = (SELECT file_name FROM tbl_document_registration_details WHERE doc_reg_detail_id = p_doc_reg_detail_id);
			UPDATE tbl_document_registration_details
			SET doc_reg_id = p_doc_reg_id, 
					doc_title = p_doc_title,
					user_id = p_user_id
			WHERE doc_reg_detail_id = p_doc_reg_detail_id;
		END IF;
	END IF;

	SELECT p_doc_reg_detail_id doc_reg_detail_id, v_file_name file_name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.doc_reg_id, p.doc_reg_no
	FROM tbl_document_registration_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_d`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_registration_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_gall`(p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	p.doc_reg_id, 
						p.doc_reg_no, 
						DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') as doc_reg_date, 
						p.doc_title, 
						p.ref_no,
						p.ref_date,
						p.create_date,
						p.language_id, 
						p.committee_id, 
						c.committee_name, 
						l.language, 
						p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
				CONCAT("<input type='button' onClick='deleteid(",p.doc_reg_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND p.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, committee_name, language, is_edit, is_delete FROM temp_all ORDER BY doc_reg_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_gid`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	m.doc_reg_id, 
					m.doc_reg_no, 
					DATE_FORMAT(m.doc_reg_date,'%d-%m-%Y') as doc_reg_date, 
					m.doc_title, 
					m.ref_no,
					DATE_FORMAT(m.ref_date,'%d-%m-%Y') as ref_date,
					m.create_date,
					m.language_id, 
					l.`language`, 
					m.committee_id, 
					c.committee_name, 
					m.is_delete, 
					m.user_id
	FROM tbl_document_registration_master m
	INNER JOIN tbl_language l ON l.language_id = m.language_id
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id
	WHERE m.is_delete = 0
	AND m.doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_i`(p_doc_reg_id int, p_doc_reg_no varchar(50), p_doc_reg_date varchar(10), p_doc_title text, p_ref_no varchar(150), p_ref_date varchar(10), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_document_registration_master c WHERE c.doc_reg_id = p_doc_reg_id) THEN

		SET p_doc_reg_id = (SELECT IFNULL(MAX(doc_reg_id), 0) + 1 FROM tbl_document_registration_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(doc_reg_id), 0) + 1 FROM tbl_document_registration_master WHERE committee_id = p_committee_id);		

		SET p_doc_reg_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		IF(p_language_id = 2) THEN
			SET p_doc_reg_no = english_bangla_number(p_doc_reg_no);
		END IF;

		INSERT INTO tbl_document_registration_master(doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, is_delete, user_id)
		VALUES(p_doc_reg_id, p_doc_reg_no, STR_TO_DATE(p_doc_reg_date,'%d-%m-%Y'), p_doc_title, p_ref_no, STR_TO_DATE(p_ref_date,'%d-%m-%Y'), NOW(), p_language_id, p_committee_id, 0, p_user_id);

	ELSE
		UPDATE tbl_document_registration_master
		SET doc_reg_no = p_doc_reg_no, 
				doc_reg_date = STR_TO_DATE(p_doc_reg_date,'%d-%m-%Y'), 
				doc_title = p_doc_title,
				ref_no = p_ref_no,
				ref_date = STR_TO_DATE(p_ref_date,'%d-%m-%Y'), 
				language_id = p_language_id, 
				committee_id = p_committee_id, 				
				user_id = p_user_id
		WHERE doc_reg_id = p_doc_reg_id;


		#DELETE FROM tbl_document_registration_details
		#WHERE doc_reg_id = p_doc_reg_id;
	END IF;

	SELECT p_doc_reg_id doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_search`(p_doc_reg_no varchar(50),  p_start_date varchar(10), p_end_date varchar(10), p_doc_title varchar(256), p_ref_no varchar(150), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	p.doc_reg_id, 
						p.doc_reg_no, 
						DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') doc_reg_date, 
						p.doc_title, 
						p.language_id, 
						p.committee_id, 
						c.committee_name, 
						p.ref_no,
						p.ref_date,
						p.create_date,
						l.language, 
						p.user_id,
						CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",p.doc_reg_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_doc_reg_no = '' OR p.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND (p_ref_no = '' OR p.ref_no LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_doc_title = '' OR p.doc_title LIKE CONCAT('%',p_doc_title,'%'))		
		AND (p_start_date='' OR p_end_date='' OR (p.doc_reg_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		AND p.is_delete = 0
		ORDER BY p.doc_reg_date DESC
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, committee_name, language, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.doc_reg_id, CONCAT(m.doc_reg_no,', Date:', DATE_FORMAT(m.doc_reg_date,'%d-%m-%Y')) doc_reg_no
	FROM tbl_document_registration_master m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.doc_reg_no LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	AND m.doc_reg_id NOT IN (SELECT t.doc_reg_id FROM tbl_inquiry_doc_tag t);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_approved`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_approved`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_approved`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_follow_up_master
	SET is_approved = 1,
			approved_date = NOW(),
			user_id = p_user_id
	WHERE inquiry_report_followup_id  = p_inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_d`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_follow_up_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_report_followup_id  = p_inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_gall`(p_inquiry_report_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_report_followup_id, t.inquiry_report_id, t.followup_no, DATE_FORMAT(t.followup_date,'%d-%m-%Y') followup_date, t.background, t.language_id, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.inquiry_report_followup_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.inquiry_report_followup_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_follow_up_master t		
		WHERE t.inquiry_report_id = p_inquiry_report_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_report_followup_id, inquiry_report_id, report_no, followup_no, followup_date, background, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_followup_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_gid`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_report_followup_id, t.inquiry_report_id, i.report_no, DATE_FORMAT(i.report_date, '%d-%m-%Y') report_date ,i.title, t.followup_no, DATE_FORMAT(t.followup_date, '%d-%m-%Y') followup_date, t.background, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, t.remarks
	FROM tbl_follow_up_master t
	INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = t.inquiry_report_id AND i.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_i`(p_inquiry_report_followup_id bigint , p_inquiry_report_id int , p_followup_no varchar (50) , p_followup_date varchar(10), p_background longtext, p_remarks longtext, p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_follow_up_master c WHERE c.inquiry_report_followup_id = p_inquiry_report_followup_id) THEN		

		SET p_inquiry_report_followup_id = (SELECT IFNULL(MAX(inquiry_report_followup_id), 0) + 1 FROM tbl_follow_up_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_report_followup_id), 0) + 1 FROM tbl_follow_up_master WHERE committee_id = p_committee_id);
		SET p_followup_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));
		IF(p_language_id = 2) THEN
			SET p_followup_no = english_bangla_number(p_followup_no);
		END IF;

		INSERT INTO tbl_follow_up_master(inquiry_report_followup_id, inquiry_report_id, followup_no, followup_date, background, remarks, language_id, committee_id, is_approved, is_delete, user_id)
		VALUES(p_inquiry_report_followup_id, p_inquiry_report_id, p_followup_no, STR_TO_DATE(p_followup_date,'%d-%m-%Y'), p_background, p_remarks, p_language_id, p_committee_id, 0, 0, p_user_id);
	ELSE
		UPDATE tbl_follow_up_master
		SET inquiry_report_id = p_inquiry_report_id, 
				followup_no = p_followup_no, 
				followup_date = STR_TO_DATE(p_followup_date,'%d-%m-%Y'), 
				background = p_background, 
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = p_user_id
		WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;


		DELETE FROM tbl_follow_up_recommendation WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;
	END IF;

	SELECT p_inquiry_report_followup_id inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_search`(p_startdate varchar(10), p_enddate varchar(10), p_report_no varchar(100), p_parliament_id smallint, p_inquiry_no varchar(100),  p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	/*
		DECLARE v_finished INTEGER DEFAULT 0;
		SET @sql = NULL;

		DROP TEMPORARY TABLE IF EXISTS temp_m;
		CREATE TEMPORARY TABLE temp_m AS
		(	
			SELECT t.inquiry_report_id, CONCAT("<table width='95%'><tr><td style='width:35%;'>",t.followup_no, "</td><td style='width:35%;'>", DATE_FORMAT(t.followup_date,'%d-%m-%Y'), "</td><td style='width:30%;'>" ,"<input type='button' onClick='popupwinid(", CAST(t.inquiry_report_id AS CHAR(10)), ", ", CAST(t.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Change' class='gridbutton'/></td><td>","<input type='button' onClick='printid(", CAST(t.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Print View' class='gridbutton'/></td></tr></table>") v_text
			FROM tbl_follow_up_master t					
			WHERE t.is_delete = 0
		);
		

		DROP TEMPORARY TABLE IF EXISTS temp_n;

		CREATE TEMPORARY TABLE temp_n
		(
			inquiry_report_id INT,
			v_text LONGTEXT
		);
		
		INSERT INTO temp_n
		SELECT inquiry_report_id, GROUP_CONCAT(v_text) v_text 
		FROM temp_m
		GROUP BY inquiry_report_id;

		UPDATE temp_n
		SET v_text = REPLACE(v_text, "</table>,<table width='95%'>","");

		#SELECT * FROM temp_n;
		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT 	c.inquiry_report_id, 
							c.report_no, 
							DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, 
							c.inquiry_id, 
							i.inquiry_no, 
							c.title, 
							c.parliament_id, 
							p.parliament, 
							c.session_id, 
							s.session, 
							c.executive_summary, 
							c.introduction, 
							c.issues, 
							c.language_id, 
							l.language, 
							c.committee_id, 
							b.committee_name, 
							c.user_id,	
							CASE 
							WHEN m.inquiry_report_id > 0 THEN CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,", 0);' name='basic' value='Add New' class='gridbutton' disabled style='background-color: gray;'/>") 
							ELSE CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,", 0);' name='basic' value='Add New' class='gridbutton'/>") 
							END add_new, 
							m.inquiry_report_id v,
							m.v_text					
			FROM tbl_inquiry_report_master c 
			INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
			INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
			INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
			INNER JOIN tbl_language l ON l.language_id = c.language_id 		
			INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
			LEFT OUTER JOIN temp_n m ON m.inquiry_report_id = c.inquiry_report_id
			WHERE c.language_id = p_language_id		
			AND c.committee_id = p_committee_id
			AND c.is_delete = 0		
			ORDER BY c.report_date DESC		
		);
		SET @sql = CONCAT("SELECT v, inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, user_id, add_new, v_text FROM temp_all ", p_limit);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	*/
		SET @sql = NULL;
		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT 	c.inquiry_report_id, 
							c.report_no, 
							DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, 
							c.inquiry_id, 
							i.inquiry_no, 
              i.inquiry_title,
							c.title, 
							c.parliament_id, 
							p.parliament, 
							c.session_id, 
							s.session, 
							c.executive_summary, 
							c.introduction, 
							c.issues, 
							c.language_id, 
							l.language, 
							c.committee_id, 
							b.committee_name, 
							m.followup_no,	
							DATE_FORMAT(m.followup_date,'%d-%m-%Y') followup_date,							
							CASE 
								WHEN m.inquiry_report_id IS NULL AND m.is_approved IS NULL THEN CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,", 0);' name='basic' value='Add New' class='gridbutton'/>") 
								WHEN m.inquiry_report_id > 0 AND m.is_approved = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", CAST(m.inquiry_report_id AS CHAR(10)), ", ", CAST(m.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Edit' class='gridbutton'/>")
								ELSE CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,", 0);' name='basic' value='Edit' class='gridbutton' disabled style='background-color: gray;'/>") 
							END add_new,							
							CASE 
								WHEN m.is_approved IS NULL OR m.is_approved = 1 THEN CONCAT("<input type='button' onClick='approveid(", CAST(IFNULL(m.inquiry_report_followup_id, 0) AS CHAR(10)), ");' name='basic' value='Approve' class='gridbutton' disabled style='background-color: gray;'/>") 
								WHEN m.is_approved = 0 THEN  CONCAT("<input type='button' onClick='approveid(", CAST(m.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Approve' class='gridbutton'/>")
								ELSE CONCAT("<input type='button' onClick='approveid(0);' name='basic' value='Approve' class='gridbutton'/>")
							END is_approved,
							CASE 
								WHEN m.is_approved IS NULL THEN CONCAT("<input type='button' onClick='printid(", CAST(IFNULL(m.inquiry_report_followup_id, 0) AS CHAR(10)), ");' name='basic' value='Print View' class='gridbutton' disabled style='background-color: gray;'/>") 
								ELSE CONCAT("<input type='button' onClick='printid(", CAST(IFNULL(m.inquiry_report_followup_id, 0) AS CHAR(10)), ");' name='basic' value='Print View' class='gridbutton'/>")
							END is_print
			FROM tbl_inquiry_report_master c 
			INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
			INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
			INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
			INNER JOIN tbl_language l ON l.language_id = c.language_id 		
			INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
			LEFT OUTER JOIN tbl_follow_up_master m ON m.inquiry_report_id = c.inquiry_report_id
			WHERE c.language_id = p_language_id		
			AND c.committee_id = p_committee_id
			AND c.is_delete = 0		
			ORDER BY c.report_date DESC		
		);
		SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, inquiry_title, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, add_new, followup_no, followup_date, is_approved, is_print FROM temp_all ", p_limit);
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_d`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_follow_up_recommendation
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_report_followup_id  = p_inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_gall`(p_inquiry_report_followup_id bigint, p_inquiry_report_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	IF(p_inquiry_report_followup_id != 0 ) THEN
		#SET @sql = NULL;	
		#DROP TEMPORARY TABLE IF EXISTS temp_all;
		#CREATE TEMPORARY TABLE temp_all AS
		#(
			SELECT t.inquiry_report_followup_id, i.inquiry_report_id, t.recommendation, t.organization_response, t.action_taken, t.comment
			FROM tbl_follow_up_recommendation t
			INNER JOIN tbl_follow_up_master i ON i.inquiry_report_followup_id = t.inquiry_report_followup_id AND i.is_delete = 0		
			WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
			AND t.is_delete = 0;
		#);
		#SET @sql = CONCAT("SELECT inquiry_report_followup_id, recommendation, organization_response, action_taken, comment, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_followup_id ");
		#PREPARE stmt FROM @sql;
		#EXECUTE stmt;
		#DEALLOCATE PREPARE stmt;
	ELSE
		SELECT 0 inquiry_report_followup_id, c.inquiry_report_id, c.recommendation recommendation, '' organization_response, '' action_taken, '' comment
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_gid`(p_inquiry_report_followup_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_report_followup_id, t.recommendation, t.organization_response, t.action_taken, t.comment, t.user_id
	FROM tbl_follow_up_recommendation t
	INNER JOIN tbl_follow_up_master i ON i.inquiry_report_followup_id = t.inquiry_report_followup_id AND i.is_delete = 0		
	WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_i`(p_inquiry_report_followup_id bigint , p_recommendation longtext , p_organization_response varchar(100) , p_action_taken longtext , p_comment longtext , p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE p_recom_id BIGINT;

	IF NOT EXISTS(SELECT * FROM tbl_follow_up_recommendation c WHERE c.recom_id = p_recom_id) THEN				

		SET p_recom_id = (SELECT IFNULL(MAX(recom_id), 0) + 1 FROM tbl_follow_up_recommendation);

		INSERT INTO tbl_follow_up_recommendation(inquiry_report_followup_id, recom_id, recommendation, organization_response, action_taken, comment, is_delete, user_id)
		VALUES(p_inquiry_report_followup_id, p_recom_id, p_recommendation, p_organization_response, p_action_taken, p_comment, 0, p_user_id);

	/*
	ELSE
		UPDATE tbl_follow_up_recommendation
		SET recommendation = p_recommendation, 
				organization_response = p_organization_response, 
				action_taken = p_action_taken, 
				comment = p_comment, 
				user_id = p_user_id
		WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;
	*/
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_govt_ref_no_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_govt_ref_no_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_govt_ref_no_gid`(p_committee_id smallint, p_language_id smallint)
BEGIN
		#Routine body goes here...
		SELECT c.govt_ref_no
		FROM tbl_govt_ref_no c 		
		WHERE c.committee_id = p_committee_id		
		AND c.language_id = p_language_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_gall`(p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(",p.group_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.group_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
					CONCAT("<input type='button' onClick='groupmenuid(",p.group_id,");' name='basic' value='Menu Permission' style='width: 150px;' class='gridbutton'/>") is_menu_permission
		FROM tbl_group p 	
	);
	SET @sql = CONCAT("SELECT group_id, group_name, is_edit, is_delete, is_menu_permission FROM temp_all ORDER BY group_name ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_gid`(p_group_id int)
BEGIN
	#Routine body goes here...
	SELECT p.group_id, p.group_name
	FROM tbl_group p
	WHERE p.group_id = p_group_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_i`(p_group_id int, p_group_name varchar(100))
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_group c WHERE c.group_id = p_group_id) THEN
		
			SET p_group_id = (SELECT IFNULL(MAX(group_id), 0) + 1 FROM tbl_group);

			INSERT INTO tbl_group(group_id, group_name)
			VALUES(p_group_id, p_group_name);

	ELSE
		UPDATE tbl_group
		SET group_name = p_group_name
		WHERE group_id = p_group_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_menu_permission_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_menu_permission_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_menu_permission_gall`(p_group_id int)
BEGIN
	#Routine body goes here...

#	IF p_group_id != 1 THEN
		SET @sql = NULL;
		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT p.menu_id, p.en_menu_name, p.bn_menu_name, p.menu_link, p.sl_no, p.parent_id,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,1);' value='Grant' name='basic' class='gridbutton'/>") is_view,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,1);' value='Grant' name='basic' class='gridbutton'/>") is_insert,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,1);' value='Grant' name='basic' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,1);' value='Grant' name='basic' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,1);' value='Grant' name='basic' class='gridbutton'/>") is_print
			FROM tbl_menu p 
			WHERE p.menu_id NOT IN (SELECT g.menu_id FROM tbl_group_menu_permission g WHERE g.group_id = p_group_id)
			AND p.menu_link IS NOT NULL 
			AND p.parent_id != 10000
		);

		INSERT INTO temp_all
		SELECT p.menu_id, p.en_menu_name, p.bn_menu_name, p.menu_link, p.sl_no, p.parent_id,
			CASE 
				WHEN pm.is_view = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_view = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,1);' value='Grant' name='basic' class='gridbutton'/>")
			END is_view,
			CASE 
				WHEN pm.is_insert = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_insert = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_insert,
			CASE 
				WHEN pm.is_edit = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_edit = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_edit,
			CASE 
				WHEN pm.is_delete = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_delete = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_delete,
			CASE 
				WHEN pm.is_print = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_print = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,1);' value='Grant' name='basic' class='gridbutton'/>")
			END is_print
		FROM tbl_menu p
		INNER JOIN tbl_group_menu_permission pm ON pm.menu_id = p.menu_id
		WHERE pm.group_id = p_group_id		
		AND p.menu_link IS NOT NULL
		AND p.parent_id != 10000;
		#WHERE p.menu_id IN (SELECT g.menu_id FROM tbl_group_menu_permission g WHERE g.group_id = p_group_id);

		SET @sql = CONCAT("SELECT menu_id, en_menu_name, bn_menu_name, menu_link, is_view, is_insert, is_edit, is_delete, is_print FROM temp_all ORDER BY menu_id, sl_no ");
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
#	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_menu_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_menu_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_menu_permission_i`(p_group_id int, p_menu_id int, p_statustype char(1), p_status tinyint)
BEGIN
	#Routine body goes here...

	DECLARE v_is_view TINYINT;
	DECLARE v_is_insert TINYINT;
	DECLARE v_is_edit TINYINT;
	DECLARE v_is_delete TINYINT;
	DECLARE v_is_print TINYINT;

	IF NOT EXISTS(SELECT * FROM tbl_group_menu_permission c WHERE c.group_id = p_group_id AND c.menu_id = p_menu_id) THEN
		INSERT INTO tbl_group_menu_permission(group_id, menu_id, is_view, is_insert, is_edit, is_delete, is_print)
		VALUES(p_group_id, p_menu_id, 0, 0, 0, 0, 0);
	END IF;

	IF(p_statustype = 'v') THEN
		UPDATE tbl_group_menu_permission
		SET is_view = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'i') THEN
		UPDATE tbl_group_menu_permission
		SET is_insert = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'e') THEN
		UPDATE tbl_group_menu_permission
		SET is_edit = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'd') THEN
		UPDATE tbl_group_menu_permission
		SET is_delete = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'p') THEN
		UPDATE tbl_group_menu_permission
		SET is_print = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	END IF;

	SET v_is_view = (SELECT is_view FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id);
	SET v_is_insert = (SELECT is_insert FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id);
	SET v_is_edit = (SELECT is_edit FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id);
	SET v_is_delete = (SELECT is_delete FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id);
	SET v_is_print = (SELECT is_print FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id);

	IF(v_is_view = 0 AND v_is_insert = 0 AND v_is_edit = 0 AND v_is_delete = 0 AND v_is_print = 0) THEN	
		DELETE FROM tbl_group_menu_permission WHERE group_id = p_group_id AND menu_id = p_menu_id;
	END IF;
	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_permission`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_permission`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_permission`(p_user_id int)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.group_id, ",", p_user_id,",0);' value='Grant' name='basic' class='gridbutton'/>") is_edit
		FROM tbl_group p 
		WHERE p.group_id NOT IN (SELECT g.group_id FROM tbl_user_group_permission g WHERE g.user_id = p_user_id)		
	);

	INSERT INTO temp_all
	SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.group_id, ",", p_user_id,",1);' value='Revoke' name='basic' class='gridbuttonr' checked />") is_edit
	FROM tbl_group p 
	WHERE p.group_id IN (SELECT g.group_id FROM tbl_user_group_permission g WHERE g.user_id = p_user_id);

	SET @sql = CONCAT("SELECT group_id, group_name, is_edit FROM temp_all ORDER BY group_name ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_adviser_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_adviser_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_adviser_d`(p_inquiry_id int , p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_adviser
	WHERE inquiry_id = p_inquiry_id
	AND adviser_id = p_adviser_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_adviser_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_adviser_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_adviser_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.adviser_id, d.adviser_name
		FROM tbl_inquiry_adviser t
		INNER JOIN tbl_specialist_adviser d ON d.adviser_id = t.adviser_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT adviser_id, adviser_name FROM temp_all ORDER BY adviser_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_adviser_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_adviser_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_adviser_gid`(p_inquiry_id int , p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.adviser_id, d.adviser_name
	FROM tbl_inquiry_adviser t
	INNER JOIN tbl_specialist_adviser d ON d.adviser_id = t.adviser_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id	
	AND t.adviser_id = p_adviser_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_adviser_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_adviser_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_adviser_i`(p_inquiry_id int, p_adviser_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_adviser c WHERE c.inquiry_id = p_inquiry_id AND c.adviser_id = p_adviser_id) THEN		
		INSERT INTO tbl_inquiry_adviser(inquiry_id, adviser_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_adviser_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_adviser
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND adviser_id = p_adviser_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_d`(p_inquiry_id int , p_doc_reg_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_doc_tag
	WHERE inquiry_id = p_inquiry_id
	AND doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.doc_reg_id, CONCAT(d.doc_reg_no,'- Date:', DATE_FORMAT(d.doc_reg_date,'%d-%m-%Y')) doc_reg_no
		FROM tbl_inquiry_doc_tag t
		INNER JOIN tbl_document_registration_master d ON d.doc_reg_id = t.doc_reg_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no FROM temp_all ORDER BY doc_reg_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_gid`(p_inquiry_id int , p_doc_reg_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.doc_reg_id, d.doc_reg_no FROM tbl_inquiry_doc_tag t
	INNER JOIN tbl_document_registration_master d ON d.doc_reg_id = t.doc_reg_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.doc_reg_id = p_doc_reg_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_i`(p_inquiry_id int , p_doc_reg_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_doc_tag c WHERE c.inquiry_id = p_inquiry_id AND c.doc_reg_id = p_doc_reg_id) THEN		
		INSERT INTO tbl_inquiry_doc_tag(inquiry_id, doc_reg_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_doc_reg_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_doc_tag
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND doc_reg_id = p_doc_reg_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_c`(p_language_id smallint, p_committee_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.inquiry_id, p.inquiry_no
	FROM tbl_inquiry_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
	WHERE p.is_delete = 0
	AND p.language_id = p_language_id
	AND p.committee_id = p_committee_id
	AND p.inquiry_id NOT IN (SELECT r.inquiry_id FROM tbl_record_of_decision_inquiry_tag r WHERE r.status = 1);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_complete_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_complete_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_complete_c`(p_language_id smallint, p_committee_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.inquiry_id, p.inquiry_no
	FROM tbl_inquiry_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
	INNER JOIN tbl_record_of_decision_inquiry_tag r ON r.inquiry_id = p.inquiry_id
	WHERE p.is_delete = 0
	AND r.status = 1
	AND p.language_id = p_language_id
	AND p.committee_id = p_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_d`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_id  = p_inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_gid`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	p.inquiry_id, 
					p.inquiry_no, 
					DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
					p.inquiry_title, 
					p.proposed_month, 
					p.proposed_year,
					p.language_id, 
					l.language, 
					p.committee_id, 
					c.committee_name,
					p.parliament_id,
					p.session_id,
					p.user_id,
					p.remarks
	FROM tbl_inquiry_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
	WHERE p.inquiry_id  = p_inquiry_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_i`(p_inquiry_id int , p_inquiry_no varchar(50) , p_create_date varchar(10), p_proposed_month varchar(10), p_proposed_year varchar(4), p_parliament_id smallint, p_session_id smallint, p_doc_reg varchar(256), p_inquiry_title text, p_remarks longtext, p_language_id smallint , p_committee_id smallint, p_user_id int, p_ministry varchar(256), p_organization varchar(256), p_others varchar(256), p_witness varchar(256), p_others_witness varchar(256), p_adviser varchar(256))
BEGIN
	#Routine body goes here...
	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);
	DECLARE v_sl_no INT;		
	DECLARE v_committee_short_name varchar(50);

	IF NOT EXISTS(SELECT * FROM tbl_inquiry_master c WHERE c.inquiry_id = p_inquiry_id) THEN

		SET p_inquiry_id = (SELECT IFNULL(MAX(inquiry_id), 0) + 1 FROM tbl_inquiry_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_id), 0) + 1 FROM tbl_inquiry_master WHERE committee_id = p_committee_id AND language_id = p_language_id);		

		SET v_committee_short_name = (SELECT short_name FROM tbl_committee c WHERE c.committee_id = p_committee_id);

		#SET p_inquiry_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		SET p_inquiry_no = CONCAT(YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		IF(p_language_id = 2) THEN
			SET p_inquiry_no = english_bangla_number(p_inquiry_no);
		END IF;

		SET p_inquiry_no = CONCAT(v_committee_short_name, p_inquiry_no);

		INSERT INTO tbl_inquiry_master(inquiry_id, inquiry_no, create_date, inquiry_title, proposed_month, proposed_year, parliament_id, session_id, remarks, language_id, committee_id, is_issued, is_delete, user_id)
		VALUES(p_inquiry_id, p_inquiry_no, STR_TO_DATE(p_create_date, '%d-%m-%Y') , p_inquiry_title, p_proposed_month, p_proposed_year, p_parliament_id, p_session_id, p_remarks, p_language_id, p_committee_id, 0, 0, p_user_id);
	ELSE		
		UPDATE tbl_inquiry_master
		SET inquiry_no = p_inquiry_no, 
				create_date = STR_TO_DATE(p_create_date, '%d-%m-%Y'), 
				inquiry_title = p_inquiry_title,  
				proposed_month = p_proposed_month,
				proposed_year = p_proposed_year,	
				parliament_id = p_parliament_id,
				session_id = p_session_id,
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id, 				
				user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id;

	END IF;

	#Document Register Information

	DELETE FROM tbl_inquiry_doc_tag WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_doc_reg != '' THEN		
		SET occurance = LENGTH(p_doc_reg)-LENGTH(replace(p_doc_reg,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_doc_reg, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_doc_tag_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Document Register Information


	#Ministry Information
	DELETE FROM tbl_inquiry_ministry WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_ministry != '' THEN		
		SET occurance = LENGTH(p_ministry)-LENGTH(replace(p_ministry,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_ministry, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_ministry_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Ministry Information


	#organization Information

	DELETE FROM tbl_inquiry_organization WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_organization != '' THEN		
		SET occurance = LENGTH(p_organization)-LENGTH(replace(p_organization,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_organization, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_organization_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#organization Information


	#Others Information

	DELETE FROM tbl_inquiry_others WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_others != '' THEN		
		SET occurance = LENGTH(p_others)-LENGTH(replace(p_others,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_others, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_others_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Others Information

	#witness  Information

	DELETE FROM tbl_inquiry_witnesses WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness )-LENGTH(replace(p_witness ,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness , ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_inquiry_witnesses_i(p_inquiry_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#others witness Information

	DELETE FROM tbl_inquiry_other_witnessess WHERE inquiry_id = p_inquiry_id;


	SET p_index = 1;
	IF p_others_witness != '' THEN		
		SET occurance = LENGTH(p_others_witness)-LENGTH(replace(p_others_witness,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_others_witness, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_other_witnessess_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#others witness Information


	#Specialist Advisor Information

	DELETE FROM tbl_inquiry_adviser WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_adviser != '' THEN		
		SET occurance = LENGTH(p_adviser)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_adviser_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Specialist Advisor Information

  SELECT p_inquiry_id inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_issued`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_master
	SET is_issued = 1,
			issued_date = NOW(),
			user_id = p_user_id
	WHERE inquiry_id  = p_inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(250), p_proposed_month varchar(10), p_proposed_year varchar(4), p_doc_reg_no varchar(50), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT  p.inquiry_id, 
						p.inquiry_no, 
						p.inquiry_title, 
						DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date,
						CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
						p.language_id,
						p.parliament_id,
						p.session_id,
						CASE
						WHEN p.is_issued = 0 THEN CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' name='basic' value='Edit' class='gridbutton'/>") 
						WHEN p.is_issued = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' name='basic' value='Edit' class='gridbutton' disabled style='background-color: gray;' />") 
						END is_edit,
						CASE 
						WHEN p.is_issued = 0 THEN CONCAT("<input type='button' onClick='issued(",p.inquiry_id,");' name='basic' value='Issue' class='gridbutton'/>") 
						WHEN p.is_issued = 1 THEN CONCAT("<input type='button' onClick='issued(",p.inquiry_id,");' name='basic' value='Issue' class='gridbutton' disabled style='background-color: gray;'/>") 
						END is_issued,
						CONCAT("<input type='button'  name='",p.inquiry_id,"' value='Print View' class='gridbutton'/>") is_print,
						CONCAT("<input type='button' onClick='deleteid(",p.inquiry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete	
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag d ON d.inquiry_id = p.inquiry_id AND d.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master r ON r.doc_reg_id = d.doc_reg_id AND r.is_delete = 0		
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%', p_inquiry_no, '%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))
		AND (p_proposed_month = '' OR p.proposed_month = p_proposed_month)
		AND (p_proposed_year = '' OR p.proposed_year = p_proposed_year)
		AND (p_doc_reg_no = '' OR r.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND p.is_delete = 0
		ORDER BY p.create_date DESC
		/*
		SELECT p.inquiry_id, p.inquiry_no, DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date , p.inquiry_title, p.language_id, l.`language`, p.committee_id, c.committee_name, p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
				CONCAT("<input type='button' onClick='deleteid(",p.inquiry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))		
		AND p.is_delete = 0*/
	);
	SET @sql = CONCAT("SELECT DISTINCT inquiry_id, inquiry_no, create_date, inquiry_title, proposed_date, is_edit, is_issued, is_print, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_token`(p_keyword varchar(20), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.inquiry_id, CONCAT(m.inquiry_no,":",m.inquiry_title) inquiry_no
	FROM tbl_inquiry_master m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND CONCAT(m.inquiry_no," ",m.inquiry_title) LIKE CONCAT('%',p_keyword,'%')
	AND m.inquiry_id NOT IN (SELECT r.inquiry_id FROM tbl_record_of_decision_inquiry_tag r WHERE r.status = 1)
  #AND m.inquiry_no LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_d`(p_inquiry_id int, p_ministry_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_ministry
	WHERE inquiry_id = p_inquiry_id
	AND ministry_id = p_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.ministry_id, d.ministry_name
		FROM tbl_inquiry_ministry t
		INNER JOIN tbl_ministry d ON d.ministry_id = t.ministry_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT ministry_id, ministry_name FROM temp_all ORDER BY ministry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_gid`(p_inquiry_id int , p_ministry_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.ministry_id, d.ministry_name
	FROM tbl_inquiry_ministry t
	INNER JOIN tbl_ministry d ON d.ministry_id = t.ministry_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.ministry_id = p_ministry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_i`(p_inquiry_id int , p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_ministry c WHERE c.inquiry_id = p_inquiry_id AND c.ministry_id = p_ministry_id) THEN		
		INSERT INTO tbl_inquiry_ministry(inquiry_id, ministry_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_ministry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_ministry
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND ministry_id = p_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_d`(p_inquiry_id int, p_organization_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_organization
	WHERE inquiry_id = p_inquiry_id
	AND organization_id = p_organization_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.organization_id, d.organization_name
		FROM tbl_inquiry_organization t
		INNER JOIN tbl_organization d ON d.organization_id = t.organization_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT organization_id, organization_name FROM temp_all ORDER BY organization_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_gid`(p_inquiry_id int , p_organization_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.organization_id, d.organization_name 
	FROM tbl_inquiry_organization t
	INNER JOIN tbl_organization d ON d.organization_id = t.organization_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.organization_id = p_organization_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_i`(p_inquiry_id int , p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_organization c WHERE c.inquiry_id = p_inquiry_id AND c.organization_id = p_organization_id) THEN		
		INSERT INTO tbl_inquiry_organization(inquiry_id, organization_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_organization_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_organization
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND organization_id = p_organization_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_d`(p_others_org_ministry_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_others
	SET is_delete = 1,
			user_id = p_user_id
	WHERE others_org_ministry_id = p_others_org_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.others_org_ministry_id, o.others_org_ministry
		FROM tbl_inquiry_others t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_others o ON o.others_org_ministry_id = t.others_org_ministry_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id		
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT others_org_ministry_id, others_org_ministry FROM temp_all ORDER BY others_org_ministry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_gid`(p_others_org_ministry_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.others_org_ministry_id, o.others_org_ministry				
	FROM tbl_inquiry_others t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	INNER JOIN tbl_others o ON o.others_org_ministry_id = t.others_org_ministry_id AND o.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = o.language_id
	WHERE t.others_org_ministry_id = p_others_org_ministry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_i`(p_inquiry_id int , p_others_org_ministry_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_others c WHERE c.others_org_ministry_id = p_others_org_ministry_id AND c.inquiry_id = p_inquiry_id) THEN		

		INSERT INTO tbl_inquiry_others(inquiry_id, others_org_ministry_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_others_org_ministry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_others
		SET user_id = p_user_id
		WHERE others_org_ministry_id = p_others_org_ministry_id
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_d`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_other_witnessess
	SET is_delete = 1,
			user_id = p_user_id
	WHERE witness_id = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, o.witness_name, o.designation, o.phone, o.cell_phone, o.fax, o.email, o.address, o.comments, o.language_id, l.`language`, t.user_id
		FROM tbl_inquiry_other_witnessess t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, witness_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_gid`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, o.witness_name, o.designation, o.phone, o.cell_phone, o.fax, o.email, o.address, o.comments, o.language_id, l.`language`, t.user_id
	FROM tbl_inquiry_other_witnessess t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	WHERE t.witness_id = p_witness_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_i`(p_inquiry_id int, p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_other_witnessess c WHERE c.witness_id = p_witness_id AND c.inquiry_id = p_inquiry_id) THEN		

		INSERT INTO tbl_inquiry_other_witnessess(inquiry_id, witness_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_witness_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_other_witnessess
		SET user_id = p_user_id
		WHERE witness_id = p_witness_id
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_approved`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_approved`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_approved`(p_inquiry_report_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_report_master
	SET is_approved = 1,
			approved_date = NOW(),
			user_id = p_user_id	
	WHERE inquiry_report_id = p_inquiry_report_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_d`(p_inquiry_report_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_report_master
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE inquiry_report_id = p_inquiry_report_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.inquiry_report_id, c.report_no, DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, c.inquiry_id, i.inquiry_no, c.title, c.parliament_id, p.parliament, c.session_id, s.`session`, c.executive_summary, c.introduction, c.issues, c.language_id, l.`language`, c.committee_id, b.committee_name, c.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.committee_id = p_committee_id
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_gid`(p_inquiry_report_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT 	c.inquiry_report_id, 
						c.report_no, 
						DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, 
						c.inquiry_id, 
						i.inquiry_no, 
						c.title, 
						c.parliament_id, 
						p.parliament, 
						c.session_id, 
						s.session, 
						c.executive_summary, 
						c.introduction, 
						c.issues, 
						c.language_id, 
						l.language, 
						c.committee_id, 
						b.committee_name, 
						c.user_id, 
						c.remarks
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_i`(p_inquiry_report_id int, p_report_no varchar(30), p_report_date varchar(10), p_inquiry_id int, p_title longtext, p_executive_summary longtext, p_introduction longtext, p_issues longtext, p_remarks longtext, p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_report_master c WHERE c.inquiry_report_id = p_inquiry_report_id) THEN

		SET p_inquiry_report_id = (SELECT IFNULL(MAX(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master WHERE committee_id = p_committee_id);
		SET p_report_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));
		IF(p_language_id = 2) THEN
			SET p_report_no = english_bangla_number(p_report_no);
		END IF;

#		INSERT INTO tbl_inquiry_report_master(inquiry_report_id, report_no, report_date, inquiry_id, title, parliament_id, session_id, executive_summary, introduction, issues, language_id, committee_id, is_delete, user_id)
#		VALUES(p_inquiry_report_id, p_report_no, STR_TO_DATE(p_report_date,'%d-%m-%Y'), p_inquiry_id, p_title, p_parliament_id, p_session_id, p_executive_summary, p_introduction, p_issues, p_language_id, p_committee_id, 0, p_user_id);

		INSERT INTO tbl_inquiry_report_master(inquiry_report_id, report_no, report_date, inquiry_id, title, executive_summary, introduction, issues, remarks, language_id, committee_id, is_delete, user_id)
		VALUES(p_inquiry_report_id, p_report_no, STR_TO_DATE(p_report_date,'%d-%m-%Y'), p_inquiry_id, p_title, p_executive_summary, p_introduction, p_issues, p_remarks, p_language_id, p_committee_id, 0, p_user_id);


	ELSE
		UPDATE tbl_inquiry_report_master
		SET report_no = p_report_no, 
				report_date = STR_TO_DATE(p_report_date,'%d-%m-%Y'), 
				inquiry_id = p_inquiry_id, 
				title = p_title, 
#				parliament_id = p_parliament_id, 
	#			session_id = p_session_id, 
				executive_summary = p_executive_summary, 
				introduction = p_introduction, 
				issues = p_issues, 
				remarks = p_remarks, 
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = user_id
		WHERE inquiry_report_id = p_inquiry_report_id;

		DELETE FROM tbl_inquiry_report_recommendation
		WHERE inquiry_report_id = p_inquiry_report_id;
	END IF;
	SELECT p_inquiry_report_id inquiry_report_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_search`(p_start_date varchar(10), p_end_date varchar(10), p_report_no varchar(100), p_parliament_id smallint, p_inquiry_no varchar(100),  p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	c.inquiry_report_id, 
						c.report_no, 
						DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, 
						c.inquiry_id, 
						i.inquiry_no, 
						i.inquiry_title,
						c.title, 
						c.parliament_id, 
						p.parliament, 
						c.session_id, 
						s.session, 
						c.executive_summary, 
						c.introduction, 
						c.issues, 
						c.language_id, 
						l.language, 
						c.committee_id, 
						b.committee_name, 
						CASE 
							WHEN c.is_approved = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,");' name='basic' value='Edit' class='gridbutton' disabled style='background-color: gray;'/>")
							ELSE CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,");' name='basic' value='Edit' class='gridbutton'/>") 
						END is_edit,
						CASE 
							WHEN c.is_approved = 1 THEN CONCAT("<input type='button' onClick='approvedid(",c.inquiry_report_id,");' name='basic' value='Approved' class='gridbutton' disabled style='background-color: gray;'/>")
							ELSE CONCAT("<input type='button' onClick='approvedid(",c.inquiry_report_id,");' name='basic' value='Approved' class='gridbutton'/>")
						END is_approved,
						CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Print View' class='gridbutton'/>") is_print
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.committee_id = p_committee_id
		AND (p_start_date ='' OR p_end_date='' OR (c.report_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y')))
		AND (p_report_no = '' OR c.report_no LIKE CONCAT('%',p_report_no,'%'))
		AND (TRIM(p_parliament_id) = '' OR p_parliament_id = 0 OR c.parliament_id = p_parliament_id)
		AND (p_inquiry_no = '' OR i.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND c.is_delete = 0		
		ORDER BY c.report_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, inquiry_title, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, is_edit, is_delete, is_print, is_approved FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_d`(p_inquiry_report_id int, p_recom_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		DELETE FROM tbl_inquiry_report_recommendation
		WHERE inquiry_report_id = p_inquiry_report_id
		AND recom_id = p_recom_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_gall`(p_inquiry_report_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.inquiry_report_id, c.recom_id, c.recom_no, c.recommendation, c.user_id
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0
		ORDER BY c.recom_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_gid`(p_recom_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.inquiry_report_id, c.recom_id, c.recom_no, c.recommendation, c.user_id
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.recom_id = p_recom_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_i`(p_inquiry_report_id int, p_recom_id int, p_recom_no varchar (50), p_recommendation longtext, p_user_id int)
BEGIN
	#Routine body goes here...	
	DECLARE p_index int;
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_report_recommendation c WHERE c.recom_id = p_recom_id) THEN
		
		SET p_recom_id = (SELECT IFNULL(MAX(recom_id), 0) + 1 FROM tbl_inquiry_report_recommendation);

		SET p_index = (SELECT IFNULL(COUNT(*), 0) + 1 FROM tbl_inquiry_report_recommendation WHERE inquiry_report_id = p_inquiry_report_id);
		
		SET p_recom_no = CONCAT('Recommendation ',p_index);

		/*SET p_recom_no = p_recom_no;*/

		INSERT INTO tbl_inquiry_report_recommendation(inquiry_report_id, recom_id, recom_no, recommendation, is_delete, user_id)
		VALUES(p_inquiry_report_id, p_recom_id, p_recom_no, p_recommendation, 0, p_user_id);
	/*
	ELSE
		UPDATE tbl_inquiry_report_recommendation
		SET inquiry_report_id = p_inquiry_report_id,
				recom_no = p_recom_no, 
				recommendation = p_recommendation, 				
				user_id = user_id
		WHERE recom_id = p_recom_id;
	*/
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_gall`(p_inquiry_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT 	s.schedule_no,	
						DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date					
		FROM tbl_inquiry_scheduling s 
		WHERE s.inquiry_id  = p_inquiry_id
		AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_gid`(p_inquiry_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT p.inquiry_id, 
					p.inquiry_no, 
					p.inquiry_title, 
					CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
					s.schedule_id, 					
					DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
					DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date, 
					s.schedule_no, 										
					p.language_id				 
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_scheduling s ON s.inquiry_id = p.inquiry_id AND s.is_delete = 0		
		WHERE p.inquiry_id  = p_inquiry_id
		#AND IFNULL(s.schedule_no,0) = p_schedule_no
		AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_i`(p_schedule_id int, p_inquiry_id int, p_schedule_no smallint, p_schedule_date varchar(10), p_user_id int)
BEGIN

	IF p_schedule_date != '' THEN
		IF NOT EXISTS(SELECT * FROM tbl_inquiry_scheduling WHERE inquiry_id = p_inquiry_id AND schedule_no = p_schedule_no) THEN
			
			SET p_schedule_id = (SELECT IFNULL(MAX(schedule_id), 0) + 1 FROM tbl_inquiry_scheduling);
		
			SET p_schedule_no = (SELECT IFNULL(MAX(schedule_no), 0) + 1 FROM tbl_inquiry_scheduling WHERE inquiry_id = p_inquiry_id);

			INSERT INTO tbl_inquiry_scheduling(schedule_id, inquiry_id, schedule_no, schedule_date, entry_date, is_delete, user_id)
			VALUES(p_schedule_id, p_inquiry_id, p_schedule_no, STR_TO_DATE(p_schedule_date, '%d-%m-%Y'), NOW(), 0, p_user_id);
	 
		ELSE
			UPDATE tbl_inquiry_scheduling
			SET schedule_date = STR_TO_DATE(p_schedule_date, '%d-%m-%Y'),
					user_id = p_user_id
			WHERE inquiry_id = p_inquiry_id
			AND schedule_no = p_schedule_no;
		END IF;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(250), p_proposed_month varchar(10), p_proposed_year varchar(4), p_doc_reg_no varchar(50), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
  SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT DISTINCT	p.inquiry_id, 
						p.inquiry_no, 
						p.inquiry_title, 
						DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
						DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date, 
						s.schedule_no, 
						CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
						p.language_id,						
						CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' style='width:100px' name='basic' value='Set Schedule' class='gridbutton'/>") is_edit
						#CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,",", IFNULL(s.schedule_no,0),");' style='width:100px' name='basic' value='Set Schedule' class='gridbutton'/>") is_edit
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag d ON d.inquiry_id = p.inquiry_id AND d.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master r ON r.doc_reg_id = d.doc_reg_id AND r.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_scheduling s ON s.inquiry_id = p.inquiry_id AND s.is_delete = 0
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%', p_inquiry_no, '%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))
		AND (p_proposed_month = '' OR p.proposed_month = p_proposed_month)
		AND (p_proposed_year = '' OR p.proposed_year = p_proposed_year)
		AND (p_doc_reg_no = '' OR r.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND p.is_delete = 0
		ORDER BY p.create_date DESC
	);
	SET @sql = CONCAT("SELECT DISTINCT inquiry_id, inquiry_no, create_date, inquiry_title, proposed_date, is_edit FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_d`(p_witness_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_witnesses
	SET is_delete = 1,
			user_id = p_user_id
	WHERE witness_id = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_mem;

	CREATE TEMPORARY TABLE temp_mem AS
	(
		SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.inquiry_id = p_inquiry_id
	);

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
	FROM tbl_inquiry_witnesses t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.inquiry_id = p_inquiry_id;

	#SELECT * FROM temp_mem;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
		#SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, m.member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
		INNER JOIN temp_mem m ON m.witness_id = t.witness_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_gid`(p_witness_id smallint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, t.witness_type
	FROM tbl_inquiry_witnesses t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	WHERE t.witness_id = p_witness_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_i`(p_inquiry_id int, p_witness_type varchar(1), p_witness_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_witnesses c WHERE c.witness_id = p_witness_id AND c.inquiry_id = p_inquiry_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_inquiry_witnesses(inquiry_id, witness_type, witness_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_witness_type, p_witness_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_witnesses
		SET user_id = p_user_id
		WHERE witness_id = p_witness_id
		AND inquiry_id = p_inquiry_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT CONCAT(m.ministry_member_id,'#','M') w_id, m.ministry_member_name w_name
	FROM tbl_ministry_member m
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	WHERE m.language_id = p_language_id
	AND m.ministry_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.organization_member_id,'#','O') w_id , m.organization_member_name w_name
	FROM tbl_organization_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_organization o ON o.organization_id = m.organization_id
	WHERE m.language_id = p_language_id
	AND m.organization_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_d`(p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_invitees
	SET is_delete = 1,
			user_id = p_user_id
	WHERE invitees_id = p_invitees_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.invitees_id, m.invitees_name, m.invitees_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, l.`language`, m.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",m.invitees_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.invitees_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_invitees m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 		
		WHERE m.language_id = p_language_id		
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name, invitees_organization, designation, phone, cell_phone, fax, email, address, comments, language_id, language, user_id, is_edit, is_delete FROM temp_all ORDER BY invitees_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_gid`(p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.invitees_id, m.invitees_name, m.invitees_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, l.`language`, m.user_id
	FROM tbl_invitees m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.invitees_id = p_invitees_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_i`(p_invitees_id bigint , p_invitees_name varchar(150) , p_invitees_organization varchar(150) , p_designation varchar(100) , p_phone varchar(50) , p_cell_phone varchar(50) , p_fax varchar(50) , p_email varchar(150) , p_address varchar(250) , p_comments varchar(250) , p_language_id smallint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_invitees m WHERE m.invitees_id = p_invitees_id) THEN
		SET p_invitees_id = (SELECT IFNULL(MAX(invitees_id), 0) + 1 FROM tbl_invitees);

		INSERT INTO tbl_invitees(invitees_id, invitees_name, invitees_organization, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_invitees_id, p_invitees_name, p_invitees_organization, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_invitees
		SET invitees_name = p_invitees_name, 
				invitees_organization = p_invitees_organization,
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE invitees_id = p_invitees_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_token`(p_keyword varchar(20), p_language_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.invitees_id, m.invitees_name
	FROM tbl_invitees m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 	
	WHERE m.language_id = p_language_id	
	AND m.invitees_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_language_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_language_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_language_c`()
BEGIN
	#Routine body goes here...
	SELECT l.language_id, l.language 
	FROM tbl_language l
	ORDER BY l.language_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_d`(p_logistic_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_logistic_member
	SET is_delete = 1,
			user_id = p_user_id
	WHERE logistic_member_id = p_logistic_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_gall`(p_language_id smallint)
BEGIN
	#Routine body goes here...
	SELECT m.logistic_member_id, m.logistic_member_name
	FROM tbl_logistic_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_gid`(p_logistic_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.logistic_member_id, m.logistic_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_logistic_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.logistic_member_id = p_logistic_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_i`(p_logistic_member_id smallint, p_logistic_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_logistic_member m WHERE m.logistic_member_id = p_logistic_member_id) THEN
		SET p_logistic_member_id = (SELECT IFNULL(MAX(logistic_member_id), 0) + 1 FROM tbl_logistic_member);

		INSERT INTO tbl_logistic_member(logistic_member_id, logistic_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_logistic_member_id, p_logistic_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_logistic_member
		SET logistic_member_name = p_logistic_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE logistic_member_id = p_logistic_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_search`(p_logistic_member_name varchar(150), p_designation varchar(150), p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.logistic_member_id, m.logistic_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.logistic_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.logistic_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_logistic_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_logistic_member_name = '' OR m.logistic_member_name LIKE CONCAT('%',p_logistic_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT logistic_member_id, logistic_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY logistic_member_id DESC ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.logistic_member_id, m.logistic_member_name
	FROM tbl_logistic_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.logistic_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_media_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_media_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_media_d`(p_media_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_media
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE media_id = p_media_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_media_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_media_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_media_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	c.media_id, 
						c.media_name, 
						c.media_type, 
						c.contact_person, 
						c.designation, 
						c.phone, 
						c.cell_phone, 
						c.fax, 
						c.email, 
						c.media_address, 
						c.comments, 
						c.language_id,
						l.language,
						CONCAT("<input type='button' onClick='popupwinid(",c.media_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",c.media_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_media c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT media_id, media_name, media_type, contact_person, designation, phone, cell_phone, fax, email, media_address, comments, language_id, language, is_edit, is_delete FROM temp_all ORDER BY media_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_media_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_media_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_media_gid`(p_media_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT 	c.media_id, 
						c.media_name, 
						c.media_type, 
						c.contact_person, 
						c.designation, 
						c.phone, 
						c.cell_phone, 
						c.fax, 
						c.email, 
						c.media_address, 
						c.comments, 
						c.language_id,
						l.language
		FROM tbl_media c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		WHERE c.media_id = p_media_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_media_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_media_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_media_i`(p_media_id int, p_media_name varchar(250), p_media_type varchar(50), p_contact_person varchar(250), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_media_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_media m WHERE m.media_id = p_media_id) THEN
		SET p_media_id = (SELECT IFNULL(MAX(media_id), 0) + 1 FROM tbl_media);

		INSERT INTO tbl_media(media_id, media_name, media_type, contact_person, designation, phone, cell_phone, fax, email, media_address, comments, language_id, is_delete, user_id)
		VALUES(p_media_id, p_media_name, p_media_type, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_media_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_media
		SET media_name = p_media_name, 
				media_type = p_media_type, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				media_address = p_media_address, 
				comments = p_comments,
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE media_id = p_media_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_media_type_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_media_type_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_media_type_c`()
BEGIN
	#Routine body goes here...
	SELECT 'Print Media' type_id,  'Print Media' type_value
	UNION ALL
	SELECT 'TV Media' type_id,  'TV Media' type_value
	UNION ALL
	SELECT 'Internet Media' type_id,  'Internet Media' type_value
	UNION ALL
	SELECT 'Radio Media' type_id,  'Radio Media' type_value;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_adviser_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_adviser_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_adviser_d`(p_meeting_notice_id int, p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_adviser
	WHERE adviser_id = p_adviser_id
	AND meeting_notice_id = p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_adviser_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_adviser_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_adviser_gall`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.meeting_notice_id, t.adviser_id, d.adviser_name
		FROM tbl_meeting_notice_adviser t
		INNER JOIN tbl_specialist_adviser d ON d.adviser_id = t.adviser_id AND d.is_delete = 0
		WHERE t.meeting_notice_id = p_meeting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT adviser_id, adviser_name FROM temp_all ORDER BY adviser_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_adviser_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_adviser_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_adviser_gid`(p_meeting_notice_id int, p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.adviser_id, i.adviser_name, i.designation, t.meeting_notice_id
	FROM tbl_meeting_notice_adviser t
	INNER JOIN tbl_specialist_adviser i ON i.adviser_id = t.adviser_id AND i.is_delete = 0
	WHERE t.meeting_notice_id = p_meeting_notice_id
	AND t.adviser_id =  p_adviser_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_adviser_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_adviser_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_adviser_i`(p_metting_notice_id int, p_adviser_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_adviser c WHERE c.meeting_notice_id = p_metting_notice_id AND c.adviser_id = p_adviser_id) THEN		
		INSERT INTO tbl_meeting_notice_adviser(meeting_notice_id, adviser_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_adviser_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_adviser
		SET user_id = p_user_id
		WHERE meeting_notice_id = p_metting_notice_id 
		AND adviser_id = p_adviser_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_d`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_committee
	WHERE metting_notice_id = p_metting_notice_id 
	AND member_id = p_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_gall`(p_metting_notice_id int, p_sub_committee_id int, p_committee_id smallint, p_language_id smallint)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;

	IF (p_metting_notice_id = 0 AND p_sub_committee_id != 0) THEN
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT p_metting_notice_id metting_notice_id, t.committee_member_id member_id, i.member_name, t.user_id
			FROM tbl_sub_committee_member t 
			INNER JOIN tbl_committee_member i ON i.committee_member_id = t.committee_member_id
			INNER JOIN tbl_committee c ON c.committee_id = t.committee_id
			WHERE t.committee_id = p_committee_id
			AND t.language_id = p_language_id
			AND t.is_delete = 0
		);
	ELSEIF(p_metting_notice_id = 0) THEN	
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT p_metting_notice_id metting_notice_id, t.committee_member_id member_id, t.member_name, t.user_id
			FROM tbl_committee_member t
			INNER JOIN tbl_committee c ON c.committee_id = t.committee_id
			WHERE t.committee_id = p_committee_id
			AND t.language_id = p_language_id
			AND t.is_delete = 0
		);
	ELSE
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
			FROM tbl_meeting_notice_committee t
			INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
			INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
			WHERE t.metting_notice_id = p_metting_notice_id 
			AND t.is_delete = 0
		);
	END IF;
	SET @sql = CONCAT("SELECT member_id, member_name FROM temp_all ORDER BY member_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_gid`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
	FROM tbl_meeting_notice_committee t
	INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.member_id = p_member_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_i`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_committee c WHERE c.metting_notice_id = p_metting_notice_id AND c.member_id = p_member_id) THEN		
		INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_member_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_committee
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND member_id = p_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_d`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_inquiry_tag
	WHERE inquiry_id = p_inquiry_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, CONCAT(i.inquiry_no,':',i.inquiry_title) inquiry_no, i.inquiry_title, t.metting_notice_id
		FROM tbl_meeting_notice_inquiry_tag t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no FROM temp_all ORDER BY inquiry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_gall_by_inquiry_title`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_gall_by_inquiry_title`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_gall_by_inquiry_title`(p_inquiry_title text)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, CONCAT(i.inquiry_no,':',i.inquiry_title) inquiry_no, i.inquiry_title, t.metting_notice_id
		FROM tbl_meeting_notice_inquiry_tag t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE i.inquiry_title = p_inquiry_title
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT * FROM temp_all ORDER BY inquiry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_gid`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.metting_notice_id
	FROM tbl_meeting_notice_inquiry_tag t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.inquiry_id =  p_inquiry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_i`(p_metting_notice_id int, p_inquiry_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_inquiry_tag c WHERE c.metting_notice_id = p_metting_notice_id AND c.inquiry_id = p_inquiry_id) THEN		
		INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_inquiry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_inquiry_tag
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_d`(p_metting_notice_id int, p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_invitees
	WHERE invitees_id  = p_invitees_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name
		FROM tbl_meeting_notice_invitees t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0				
		INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id		
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name FROM temp_all ORDER BY invitees_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_gid`(p_invitees_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name, v.invitees_organization, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_meeting_notice_invitees t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.invitees_id = p_invitees_id
	AND l.language_id = m.language_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_i`(p_metting_notice_id int , p_invitees_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_invitees c WHERE c.invitees_id = p_invitees_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_invitees_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_invitees
		SET user_id = p_user_id
		WHERE invitees_id = p_invitees_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_d`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_logistic
	WHERE metting_notice_id = p_metting_notice_id 
	AND logistic_id = p_logistic_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
		FROM tbl_meeting_notice_logistic t
		INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.logistic_id AND i.is_delete = 0		
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT logistic_id, logistic_member_name FROM temp_all ORDER BY logistic_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_gid`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
	FROM tbl_meeting_notice_logistic t
	INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.p_logistic_id AND i.is_delete = 0	
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.logistic_id = p_logistic_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_i`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_logistic c WHERE c.metting_notice_id = p_metting_notice_id AND c.logistic_id = p_logistic_id) THEN		
		INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_logistic_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_logistic
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND logistic_id = p_logistic_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_adjourned`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_adjourned`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_adjourned`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		DECLARE v_metting_notice_id INT;	
		DECLARE v_length INT;
		DECLARE v_committee_id SMALLINT;
		DECLARE v_language_id SMALLINT;		
		
		DECLARE v_sub_sitting_no INT;
		DECLARE v_sitting_no VARCHAR(150);
		DECLARE v_sitting_no_b VARCHAR(150);
		DECLARE v_sub_committee_id INT;

	
		SET v_sitting_no =  (SELECT SUBSTRING_INDEX(sitting_no, '/', 1) FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
		SET v_committee_id = (SELECT committee_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
		SET v_sub_committee_id = (SELECT sub_committee_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
		SET v_language_id = (SELECT language_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);

		SET v_length = LENGTH(v_sitting_no);

		SET v_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);
		IF (v_sub_committee_id IS NULL) THEN			
			SET v_sub_sitting_no = (SELECT COUNT(DISTINCT sitting_no) FROM tbl_meeting_notice_master WHERE sitting_no like CONCAT(v_sitting_no,'%') AND committee_id = v_committee_id AND language_id = v_language_id);					
			#SET v_sub_sitting_no = (SELECT COUNT(DISTINCT sitting_no) FROM tbl_meeting_notice_master WHERE LEFT(sitting_no, v_length) = v_sitting_no AND committee_id = v_committee_id AND language_id = v_language_id);					
		ELSE
			SET v_sub_sitting_no = (SELECT COUNT(DISTINCT sitting_no) FROM tbl_meeting_notice_master WHERE sitting_no like CONCAT(v_sitting_no,'%') AND committee_id = v_committee_id AND language_id = v_language_id AND sub_committee_id = v_sub_committee_id);					
			#SET v_sub_sitting_no = (SELECT COUNT(DISTINCT sitting_no) FROM tbl_meeting_notice_master WHERE LEFT(sitting_no, v_length) = v_sitting_no AND committee_id = v_committee_id AND language_id = v_language_id AND sub_committee_id = v_sub_committee_id);		
		END IF;
		#SELECT v_sub_sitting_no;
		#Set New Sitting Number
		IF(v_language_id = 2) THEN
			SET v_sitting_no_b = english_bangla_number(v_sub_sitting_no);
			SET v_sitting_no = CONCAT(v_sitting_no,'/',v_sitting_no_b); 
		ELSE
			SET v_sitting_no = CONCAT(v_sitting_no,'/',v_sub_sitting_no); 
		END IF;				
		
		#SELECT v_sitting_no_b;
			
		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, is_display, is_complete, is_delete, user_id)
		SELECT v_metting_notice_id, gov_no_pix, gov_no_postfix, v_sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, 'ADJOURNED', 0, 0, 0, 0, 0, is_display, is_complete, is_delete, p_user_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id;
		

		UPDATE tbl_meeting_notice_master
		SET is_display = 0,
				is_adjourned = 1,
				adjourned_date = NOW(),
				status = 'ADJOURNED'
		WHERE metting_notice_id = p_metting_notice_id;



	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
	SELECT v_metting_notice_id, inquiry_id, is_delete, p_user_id FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;	

	#Inquires Tag Information


	#committee Information	

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
	SELECT v_metting_notice_id, member_id, is_delete, p_user_id FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;	

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
	SELECT v_metting_notice_id, witness_id, witness_type, is_delete, p_user_id FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;	

	#witness  Information


	#p_logistics Information
	
	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
	SELECT v_metting_notice_id, logistic_id, is_delete, p_user_id FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;	

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
	SELECT v_metting_notice_id, notification_id, is_delete, p_user_id FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;	

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
	SELECT v_metting_notice_id, invitees_id, is_delete, p_user_id FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;	

	#p_invitees Information






	#===========================Record of decision Part====================================================

	#tbl_record_of_decision_master 

	#IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, issued_date, is_delete, user_id)
		SELECT v_metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, issued_date, is_delete, p_user_id FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = v_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = v_metting_notice_id;

		#Record of decision Part
	#END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_approved`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_approved`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_approved`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_approved = 1, 
				approved_date = NOW(),	
				status = 'APPROVED',  
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_bar_chart`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_bar_chart`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_bar_chart`(upcomming bool)
BEGIN
	#Routine body goes here...
	DECLARE maxDate DATETIME;
	SELECT MAX(tbl_meeting_notice_master.en_date) FROM tbl_meeting_notice_master INTO maxDate;
	IF (upcomming = 0) THEN
		SELECT COUNT(*) as meetingCount, DATE_FORMAT(en_date, '%b') as monthName, YEAR(en_date) as yearName FROM tbl_meeting_notice_master WHERE en_date <= CURDATE() AND en_date 
		BETWEEN CONCAT(YEAR(DATE_SUB(maxDate, INTERVAL 6 MONTH)),'-',MONTH(DATE_SUB(maxDate, INTERVAL 6 MONTH)),'-','01') 
		AND maxDate + INTERVAL 1 DAY GROUP BY MONTH(en_date) ORDER BY en_date;
	ELSE
		SELECT COUNT(*) as meetingCount, DATE_FORMAT(en_date, '%b') as monthName, YEAR(en_date) as yearName FROM tbl_meeting_notice_master WHERE en_date > CURDATE() AND en_date 
		BETWEEN CONCAT(YEAR(DATE_SUB(maxDate, INTERVAL 6 MONTH)),'-',MONTH(DATE_SUB(maxDate, INTERVAL 6 MONTH)),'-','01') 
		AND maxDate + INTERVAL 1 DAY GROUP BY MONTH(en_date) ORDER BY en_date;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_cancel`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_cancel`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_cancel`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_cancel = 1, 
				status = 'CANCEL', 
				cancel_date = NOW(),
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_meeting_notice_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.metting_notice_id, 
						t.gov_no_pix, 
						t.gov_no_postfix, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 
						t.bn_date, 
						t.time, 
						t.venue_id, 
						v.venue_name, 
						t.sub_committee_id, 
						s.sub_committee_name, 
						t.private_business_before, 
						t.public_business, 
						t.private_business_after, 
						t.language_id, 
						l.language, 
						t.committee_id, 
						c.committee_name, 
						t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0
		INNER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		WHERE t.language_id = p_language_id
		AND t.committee_id = p_committee_id
		AND t.is_delete = 0
		ORDER BY t.en_date DESC
	);
	SET @sql = CONCAT("SELECT metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, venue_name, sub_committee_id, sub_committee_name, private_business_before, public_business, private_business_after, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_gid`(p_metting_notice_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	t.metting_notice_id, 
					t.gov_no_pix, 
					t.gov_no_postfix, 
					t.sitting_no, 
					DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 
					t.bn_date, 
					t.time, 
					t.venue_id, 
					v.venue_name, 
					t.sub_committee_id, 
					s.sub_committee_name, 
					t.private_business_before, 
					t.public_business, 
					t.private_business_after, 
					t.language_id, 
					l.language, 
					t.committee_id, 
					c.committee_name,
					t.remarks,
					t.end_time
	FROM tbl_meeting_notice_master t
	INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
	LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_i`(p_metting_notice_id int, p_gov_no_pix varchar(50), p_gov_no_postfix varchar(10) , p_sitting_no varchar(50), p_en_date varchar(10), p_bn_date varchar(250) , p_time varchar(50), p_end_time varchar(50),  p_venue_id smallint , p_sub_committee_id smallint, p_inquiries varchar(250), p_private_business_before longtext, p_public_business longtext, p_private_business_after longtext, p_remarks longtext, p_committee varchar(250), p_witness varchar(250), p_logistics varchar(250), p_notification varchar(250), p_adviser varchar(250), p_officer varchar(250), p_invitees varchar(250),  p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	DECLARE v_sl_no INT;		

	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_master p WHERE p.metting_notice_id = p_metting_notice_id) THEN		

		SET p_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);

		IF(p_sub_committee_id = 0 OR p_sub_committee_id = '') THEN
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id AND is_display = 1);		
		ELSE
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id AND sub_committee_id = p_sub_committee_id AND is_display = 1);		
		END IF;

		#SET p_sitting_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		SET p_sitting_no = CONCAT('',v_sl_no,'');

		IF(p_language_id = 2) THEN
			SET p_sitting_no = english_bangla_number(p_sitting_no);
		END IF;

		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, is_display, is_complete, is_delete, user_id, end_time)
		VALUES(p_metting_notice_id, p_gov_no_pix, p_gov_no_postfix, p_sitting_no, STR_TO_DATE(p_en_date,'%d-%m-%Y'), p_bn_date, p_time, p_venue_id, p_sub_committee_id, p_private_business_before, p_public_business, p_private_business_after, p_remarks, p_language_id, p_committee_id, 'NEW', 0, 0, 0, 0, 0, 1, 0, 0, p_user_id, p_end_time);
	ELSE
		UPDATE tbl_meeting_notice_master
		SET gov_no_pix = p_gov_no_pix, 
				gov_no_postfix = p_gov_no_postfix, 
				sitting_no = p_sitting_no, 
				en_date = STR_TO_DATE(p_en_date,'%d-%m-%Y'), 
				bn_date = p_bn_date, 
				time = p_time, 
				venue_id = p_venue_id, 
				sub_committee_id = p_sub_committee_id, 
				private_business_before = p_private_business_before, 
				public_business = p_public_business, 
				private_business_after = p_private_business_after, 
				language_id = p_language_id, 
				committee_id = p_committee_id,  
				user_id = p_user_id,
				end_time = p_end_time
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;


	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_inquiries != '' THEN		
		SET occurance = LENGTH(p_inquiries)-LENGTH(replace(p_inquiries,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_inquiries, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_inquiry_tag_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Inquires Tag Information


	#committee Information

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness )-LENGTH(replace(p_witness ,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness , ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_meeting_notice_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#p_logistics Information

	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistics != '' THEN		
		SET occurance = LENGTH(p_logistics)-LENGTH(replace(p_logistics,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistics, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information


	#p_adviser Information

	DELETE FROM tbl_meeting_notice_adviser WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_adviser != '' THEN		
		SET occurance = LENGTH(p_adviser)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_adviser_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_adviser Information


	#p_officer Information

	DELETE FROM tbl_meeting_notice_officer WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_officer != '' THEN		
		SET occurance = LENGTH(p_officer)-LENGTH(replace(p_officer,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_officer, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_officer_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_officer Information

	#SELECT p_metting_notice_id metting_notice_id;











	#Record of decision Part


	#tbl_record_of_decision_master 

	IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, is_delete, user_id)
		VALUES(p_metting_notice_id, 0, '', '', '', p_language_id, p_committee_id , 0, 0, 0, p_user_id);

	END IF;
		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = p_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_adviser WHERE meeting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_adviser(meeting_notice_id, adviser_id, is_delete, user_id)
		SELECT meeting_notice_id, adviser_id, is_delete, user_id FROM tbl_meeting_notice_adviser WHERE meeting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_officer WHERE meeting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_officer(meeting_notice_id, officer_id, is_delete, user_id)
		SELECT meeting_notice_id, officer_id, is_delete, user_id FROM tbl_meeting_notice_officer WHERE meeting_notice_id = p_metting_notice_id;		

		#Record of decision Part
	#END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_issued`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_issued = 1, 
				status = 'ISSUED',  
				issued_date = NOW(),
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_revised`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_revised`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_revised`(p_metting_notice_id int, p_en_date varchar(10), p_bn_date varchar(250), p_time varchar(50), p_user_id int)
BEGIN
	#Routine body goes here...
		DECLARE v_metting_notice_id INT;		

		SET v_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);

		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, is_display, is_complete, is_delete, user_id)
		SELECT v_metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, STR_TO_DATE(p_en_date,'%d-%m-%Y') , p_bn_date, p_time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, 'REVISED', 0, 0, 0, 0, 0, is_display, is_complete, is_delete, p_user_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id;

		UPDATE tbl_meeting_notice_master
		SET is_display = 0,
				is_revised = 1,
				revised_date = NOW(),
				status = 'REVISED'
		WHERE metting_notice_id = p_metting_notice_id;



	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
	SELECT v_metting_notice_id, inquiry_id, is_delete, p_user_id FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;	

	#Inquires Tag Information


	#committee Information	

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
	SELECT v_metting_notice_id, member_id, is_delete, p_user_id FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;	

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
	SELECT v_metting_notice_id, witness_id, witness_type, is_delete, p_user_id FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;	

	#witness  Information


	#p_logistics Information
	
	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
	SELECT v_metting_notice_id, logistic_id, is_delete, p_user_id FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;	

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
	SELECT v_metting_notice_id, notification_id, is_delete, p_user_id FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;	

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
	SELECT v_metting_notice_id, invitees_id, is_delete, p_user_id FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;	

	#p_invitees Information






	#===========================Record of decision Part====================================================

	#tbl_record_of_decision_master 

	#IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, issued_date, is_delete, user_id)
		SELECT v_metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, issued_date, is_delete, p_user_id FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = v_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = v_metting_notice_id;

		#Record of decision Part
	#END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_search`(p_start_date varchar(10), p_end_date varchar(10), p_ref_no varchar(100), p_sub_committee int, p_sitting_no varchar(100), p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
/*
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.gov_no_pix, t.gov_no_postfix, t.sitting_no, DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, t.bn_date, t.time, t.venue_id, v.venue_name, t.sub_committee_id, s.sub_committee_name, t.private_business_before, t.public_business, t.private_business_after, t.language_id, l.language, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete ,
          CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton'/>") is_approved,
          CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton'/>") is_issued,
          CONCAT("<input type='button' onClick='printid(",t.metting_notice_id, ");' name='basic' value='Print' class='gridbutton'/>") is_print,
          CONCAT("<input type='button' onClick='multimedia(",t.metting_notice_id, ");' name='basic' value='Multimedia' class='gridbutton'/>") is_multimedia,
          CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Rec. of Dec.' class='gridbutton'/>") is_record_of_dec
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		WHERE t.language_id = p_language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, venue_name, sub_committee_id, sub_committee_name, private_business_before, public_business, private_business_after, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete, is_approved, is_issued, is_print , is_multimedia, is_record_of_dec FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
*/
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.metting_notice_id, 
						m.sitting_no, 
						CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) ref_no, 
						DATE_FORMAT(m.en_date,'%d-%m-%Y') en_date, 
						m.bn_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						v.venue_name,
						m.status,
						CASE
						WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton' style='width: 45px;'/>")
						WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton' disabled style='background-color:gray;width: 45px;' />")
						END is_edit,
						CASE
						WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='deleteid(",m.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") 
						WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='deleteid(",m.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton' disabled style='background-color:gray;'/>") 
						END is_delete,
						CASE
							WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='approvedid(",m.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton' style='width: 65px;'/>") 
							WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='approvedid(",m.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton' disabled style='background-color:gray;width: 65px;' />")
						END is_approved,						
						CASE
							WHEN m.is_issued = 0 AND m.is_approved = 1 THEN CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton' style='width: 55px;'/>")
							WHEN m.is_issued = 0 AND m.is_approved = 0 THEN CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton' disabled style='background-color:gray;width:55px;'/>") 
							ELSE CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton' disabled style='background-color:gray;width:55px;'/>") 
						END is_issued,
						CASE
							WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='cancelid(",m.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' disabled style='background-color:gray;width:60px;'/>") 
							WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='cancelid(",m.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' style='width: 60px;'/>") 
							ELSE CONCAT("<input type='button' onClick='cancelid(",m.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' disabled style='background-color:gray;' style='width: 60px;'/>")
						END is_cancled,
						CASE
							WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='adjournedid(",m.metting_notice_id, ");' name='basic' value='Adjourn' class='gridbutton' disabled style='background-color:gray;width: 62px;'/>") 
							WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='adjournedid(",m.metting_notice_id, ");' name='basic' value='Adjourn' class='gridbutton' style='width: 62px;'/>") 
							ELSE CONCAT("<input type='button' onClick='adjournedid(",m.metting_notice_id, ");' name='basic' value='Adjourn' class='gridbutton' disabled style='background-color:gray;' style='width: 62px;'/>") 
						END is_adjourned,
						CASE
							WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='revisedid(",m.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton' disabled style='background-color:gray;width: 60px;'/>") 
							WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='revisedid(",m.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton' style='width: 60px;'/>") 
							ELSE CONCAT("<input type='button' onClick='revisedid(",m.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton' disabled style='background-color:gray;' style='width: 60px;'/>")
						END is_revised,
						CASE
							WHEN m.is_issued = 0 THEN CONCAT("<input type='button' onClick='viewnoticeid(",m.metting_notice_id, ");' name='basic' value='Notices' class='gridbutton' disabled style='background-color:gray;width: 60px;' />") 
							WHEN m.is_issued = 1 THEN CONCAT("<input type='button' onClick='viewnoticeid(",m.metting_notice_id, ");' name='basic' value='Notices' class='gridbutton' style='width: 60px;'/>")
						END is_view_notice,
						CONCAT("<input type='button' name='",m.metting_notice_id,"' value='Print View' class='gridbutton'/>") is_print,

						CONCAT("<input type='button' onClick='multimedia(",m.metting_notice_id, ");' name='basic' value='Multimedia' class='gridbutton'/>") is_multimedia,
						CASE
							WHEN d.is_issued = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Rec. of Dec.' class='gridbutton' disabled style='background-color:gray;' />") 
							ELSE CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Rec. of Dec.' class='gridbutton'/>") 
						END is_record_of_dec,
						CASE
							WHEN d.is_issued = 1 THEN CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issue' class='gridbutton' disabled style='background-color:gray;'/>") 
							ELSE CONCAT("<input type='button' onClick='issuedid(",m.metting_notice_id, ");' name='basic' value='Issue' class='gridbutton'/>")
						END is_record_of_dec_issue
		FROM tbl_meeting_notice_master m
		INNER JOIN tbl_venue v ON v.venue_id = m.venue_id AND v.is_delete = 0
		LEFT OUTER JOIN tbl_meeting_notice_inquiry_tag t ON t.metting_notice_id = m.metting_notice_id AND t.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
		LEFT OUTER JOIN tbl_record_of_decision_master d ON d.metting_notice_id = m.metting_notice_id
		WHERE m.committee_id = p_committee_id
		AND m.language_id = p_language_id
		AND (p_sub_committee = '' OR p_sub_committee = 0 OR m.sub_committee_id = p_sub_committee)
		AND (p_ref_no = '' OR CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_sitting_no = '' OR m.sitting_no LIKE CONCAT('%',p_sitting_no,'%'))
		AND (p_start_date='' OR p_end_date='' OR (m.en_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		AND m.is_delete = 0 
		AND m.is_display = 1
		ORDER BY m.en_date DESC
	);
	SET @sql = CONCAT("SELECT metting_notice_id, sitting_no, ref_no, en_date, bn_date, inquiry_no, inquiry_title, venue_name, status, is_edit, is_delete, is_approved, is_issued,  is_cancled, is_adjourned, is_revised, is_view_notice, is_print, is_multimedia, is_record_of_dec, is_record_of_dec_issue FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_view`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_view`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_view`(p_metting_notice_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...	
	DECLARE v_length INT;	
	DECLARE v_sitting_no VARCHAR(50);
	DECLARE v_sub_committee_id INT;

	SET v_sitting_no =  (SELECT SUBSTRING_INDEX(sitting_no, '/', 1) FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
	SET v_length = LENGTH(v_sitting_no);
	SET v_sub_committee_id = (SELECT sub_committee_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.metting_notice_id, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 					
						v.venue_name, 
						s.sub_committee_name,
						t.status,
						t.adjourned_date modify_date,
						CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Notice' class='gridbutton'/>") is_view_notice
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		WHERE LEFT(t.sitting_no, v_length) = v_sitting_no
		AND t.is_adjourned = 1
		AND t.is_delete = 0
	);

	INSERT INTO temp_all
	SELECT 	t.metting_notice_id, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 					
						v.venue_name, 
						s.sub_committee_name,
						t.status,
						t.revised_date,
						CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Notice' class='gridbutton'/>") is_view_notice
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		WHERE LEFT(t.sitting_no, v_length) = v_sitting_no
		AND t.is_revised = 1
		AND t.is_delete = 0;


	INSERT INTO temp_all
	SELECT 	t.metting_notice_id, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 					
						v.venue_name, 
						s.sub_committee_name,
						t.status,
						t.cancel_date,
						CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Notice' class='gridbutton'/>") is_view_notice
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		WHERE LEFT(t.sitting_no, v_length) = v_sitting_no
		AND t.is_cancel = 1
		AND t.is_delete = 0;


	INSERT INTO temp_all
	SELECT 	t.metting_notice_id, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 					
						v.venue_name, 
						s.sub_committee_name,
						t.status,
						NULL,
						CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Notice' class='gridbutton'/>") is_view_notice
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		WHERE LEFT(t.sitting_no, v_length) = v_sitting_no
		AND t.is_display = 1
		AND t.is_cancel = 0
		AND t.is_delete = 0;

	IF (v_sub_committee_id IS NULL) THEN
		SELECT 	DISTINCT metting_notice_id, 
						status, 
						sitting_no, 
						en_date, 
						venue_name, 
						modify_date, 
						is_view_notice 
		FROM temp_all 
		ORDER BY modify_date ASC;
	ELSE
		SELECT 	DISTINCT t.metting_notice_id, 
						t.status, 
						t.sitting_no, 
						t.en_date, 
						t.venue_name, 
						t.modify_date, 
						t.is_view_notice 
		FROM temp_all t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id
		WHERE m.sub_committee_id = v_sub_committee_id
		ORDER BY modify_date ASC;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_d`(p_metting_notice_id int, p_notification_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_notification
	WHERE notification_id  = p_notification_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name
		FROM tbl_meeting_notice_notification t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0						
		INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id		
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT notification_id, notification_member_name FROM temp_all ORDER BY notification_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_gid`(p_metting_notice_id int, p_notification_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_meeting_notice_notification t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.notification_id = p_notification_id
	AND l.language_id = m.language_id
	AND t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_i`(p_metting_notice_id int , p_notification_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_notification c WHERE c.notification_id = p_notification_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_notification_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_notification
		SET user_id = p_user_id
		WHERE notification_id = p_notification_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_officer_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_officer_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_officer_d`(p_meeting_notice_id int, p_officer_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_officer
	WHERE officer_id = p_officer_id
	AND meeting_notice_id = p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_officer_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_officer_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_officer_gall`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.meeting_notice_id, t.officer_id, d.officer_name
		FROM tbl_meeting_notice_officer t
		INNER JOIN tbl_committee_officer d ON d.officer_id = t.officer_id AND d.is_delete = 0
		WHERE t.meeting_notice_id = p_meeting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT officer_id, officer_name FROM temp_all ORDER BY officer_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_officer_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_officer_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_officer_gid`(p_meeting_notice_id int, p_officer_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.officer_id, i.officer_name, i.designation, t.meeting_notice_id
	FROM tbl_meeting_notice_officer t
	INNER JOIN tbl_committee_officer i ON i.officer_id = t.officer_id AND i.is_delete = 0
	WHERE t.meeting_notice_id = p_meeting_notice_id
	AND t.officer_id = p_officer_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_officer_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_officer_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_officer_i`(p_metting_notice_id int, p_officer_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_officer c WHERE c.meeting_notice_id = p_metting_notice_id AND c.officer_id = p_officer_id) THEN		
		INSERT INTO tbl_meeting_notice_officer(meeting_notice_id, officer_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_officer_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_officer
		SET user_id = p_user_id
		WHERE meeting_notice_id = p_metting_notice_id 
		AND officer_id = p_officer_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_d`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_meeting_notice_witness
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id = p_metting_notice_id 
	AND witness_id = p_witness_id
	AND witness_type = p_witness_type;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_gall`(p_metting_notice_id int, p_inquiry_id int)
BEGIN
	#Routine body goes here...	

	IF(p_metting_notice_id = 0) THEN
		
		DROP TEMPORARY TABLE IF EXISTS temp_mem;

		CREATE TEMPORARY TABLE temp_mem AS
		(
			SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
			FROM tbl_inquiry_witnesses t
			INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
			INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
			WHERE t.is_delete = 0		
			AND t.inquiry_id = p_inquiry_id
		);

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
		WHERE t.is_delete = 0
		AND t.inquiry_id = p_inquiry_id;

		INSERT INTO temp_mem
		SELECT t.witness_id, 'T' witness_type, o.witness_name member_name
		FROM tbl_inquiry_other_witnessess t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0;
		
		SELECT CONCAT(t.witness_id, '#', t.witness_type) witness_id, t.member_name
		FROM temp_mem t
		ORDER BY t.witness_type, t.witness_id;

	ELSE
		SET @sql = NULL;	
		DROP TEMPORARY TABLE IF EXISTS temp_mem;

		CREATE TEMPORARY TABLE temp_mem AS
		(
			SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
			FROM tbl_meeting_notice_witness t
			INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
			INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
			WHERE t.is_delete = 0		
			AND t.metting_notice_id = p_metting_notice_id
		);

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
		FROM tbl_meeting_notice_witness t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
		WHERE t.is_delete = 0
		AND t.metting_notice_id = p_metting_notice_id;

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.witness_name member_name
		FROM tbl_meeting_notice_witness t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
		WHERE t.is_delete = 0
		AND t.metting_notice_id = p_metting_notice_id;


		#SELECT * FROM temp_mem;

		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT t.metting_notice_id, i.sitting_no, i.en_date, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
			FROM tbl_meeting_notice_witness t
			INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
			INNER JOIN temp_mem m ON m.witness_id = t.witness_id
			WHERE t.metting_notice_id = p_metting_notice_id
			AND t.is_delete = 0
		);
		SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_gid`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.witness_id, t.witness_type, t.user_id
	FROM tbl_meeting_notice_witness t
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.witness_id = p_witness_id
	AND t.witness_type = p_witness_type
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_i`(p_metting_notice_id int, p_witness_type char(1), p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_witness c WHERE c.metting_notice_id = p_metting_notice_id AND c.witness_id = p_witness_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
		VALUES(p_metting_notice_id, p_witness_id, p_witness_type, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_witness
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND witness_id = p_witness_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT CONCAT(m.ministry_member_id,'#','M') w_id, m.ministry_member_name w_name
	FROM tbl_ministry_member m
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	WHERE m.language_id = p_language_id
	AND m.ministry_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.organization_member_id,'#','O') w_id , m.organization_member_name w_name
	FROM tbl_organization_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_organization o ON o.organization_id = m.organization_id
	WHERE m.language_id = p_language_id
	AND m.organization_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.witness_id,'#','T') w_id , m.witness_name w_name
	FROM tbl_other_witnessess m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.witness_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_menu_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_menu_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_menu_gall`(p_parent_id smallint, p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...
	DROP TEMPORARY TABLE IF EXISTS temp_all;	
	IF(p_language_id = 1) THEN	
		CREATE TEMPORARY TABLE temp_all AS
		(		
			SELECT m.menu_id, m.en_menu_name menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
			WHERE m.parent_id = p_parent_id AND m.menu_link IS NULL	
		);
	ELSE
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT m.menu_id, m.bn_menu_name menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
			WHERE m.parent_id = p_parent_id AND m.menu_link IS NULL
		);
	END IF;

	#IF(p_parent_id != 0) THEN
		#DELETE FROM temp_all;
	#END IF;
	/*
	IF(p_parent_id != 0) THEN
		IF(p_language_id = 1) THEN
				INSERT INTO temp_all
				SELECT m.menu_id, m.en_menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
				WHERE m.parent_id = p_parent_id AND m.menu_link IS NULL;
		ELSE
				INSERT INTO temp_all
				SELECT m.menu_id, m.bn_menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
				WHERE m.parent_id = p_parent_id AND m.menu_link IS NULL;
		END IF;
	END IF;
	*/
	IF(p_language_id = 1) THEN
		INSERT INTO temp_all
		SELECT m.menu_id, m.en_menu_name menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
		WHERE m.parent_id = p_parent_id
		AND m.menu_id IN (SELECT p.menu_id FROM tbl_group_menu_permission p INNER JOIN tbl_user_group_permission g ON g.group_id = p.group_id WHERE g.user_id = p_user_id AND (p.is_view =1 OR p.is_insert = 1 OR p.is_edit = 1 OR p.is_delete = 1 OR p.is_print = 1))
		ORDER BY m.sl_no;
	ELSE
		INSERT INTO temp_all
		SELECT m.menu_id, m.bn_menu_name menu_name, m.menu_link, m.sl_no  FROM tbl_menu m
		WHERE m.parent_id = p_parent_id
		AND m.menu_id IN (SELECT p.menu_id FROM tbl_group_menu_permission p INNER JOIN tbl_user_group_permission g ON g.group_id = p.group_id WHERE g.user_id = p_user_id AND (p.is_view =1 OR p.is_insert = 1 OR p.is_edit = 1 OR p.is_delete = 1 OR p.is_print = 1))
		ORDER BY m.sl_no;
	END IF;
	SELECT DISTINCT menu_id, menu_name, menu_link FROM temp_all ORDER BY sl_no ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_id, m.ministry_name
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_d`(p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_ministry
	SET is_delete = 1
	WHERE ministry_id = p_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_id, m.ministry_name, m.contact_person, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.ministry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_id, ministry_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_gid`(p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_id, m.ministry_name, m.contact_person, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.ministry_id = p_ministry_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_i`(p_ministry_id smallint, p_ministry_name varchar(150), p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_ministry m WHERE m.ministry_id = p_ministry_id) THEN
		SET p_ministry_id = (SELECT IFNULL(MAX(ministry_id), 0) + 1 FROM tbl_ministry);

		INSERT INTO tbl_ministry(ministry_id, ministry_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_ministry_id, p_ministry_name, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_ministry
		SET ministry_name = p_ministry_name, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE ministry_id = p_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_d`(p_ministry_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_ministry_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE ministry_member_id = p_ministry_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.ministry_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,",",m.ministry_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry_member m 
		INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_member_id, ministry_id, ministry_name, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_gid`(p_ministry_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language
	FROM tbl_ministry_member m 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.ministry_member_id = p_ministry_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_i`(p_ministry_member_id int, p_ministry_id smallint, p_ministry_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_ministry_member m WHERE m.ministry_member_id = p_ministry_member_id) THEN
		SET p_ministry_member_id = (SELECT IFNULL(MAX(ministry_member_id), 0) + 1 FROM tbl_ministry_member);

		INSERT INTO tbl_ministry_member(ministry_member_id, ministry_id, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, is_delete, user_id)
		VALUES(p_ministry_member_id, p_ministry_id, p_ministry_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_ministry_member
		SET ministry_id = p_ministry_id, 
				ministry_member_name = p_ministry_member_name, 
				designation = p_designation, 				
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE ministry_member_id = p_ministry_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_search`(p_ministry_id smallint, p_ministry_member_name varchar(150), p_designation varchar(100),  p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid_member(",m.ministry_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,",",m.ministry_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry_member m 
		INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_ministry_id = 0 OR m.ministry_id = p_ministry_id)
		AND (p_ministry_member_name = '' OR m.ministry_member_name LIKE CONCAT('%',p_ministry_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_member_id, ministry_id, ministry_name, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_member_id DESC ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.ministry_id, m.ministry_name
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.ministry_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_month_info_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_month_info_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_month_info_gall`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 'January' id, 'January' month_name
	UNION ALL  
	SELECT 'February' id, 'February' month_name
	UNION ALL  
	SELECT 'March' id, 'March' month_name
	UNION ALL  
	SELECT 'April' id, 'April' month_name
	UNION ALL  
	SELECT 'May' id, 'May' month_name
	UNION ALL  
	SELECT 'June' id, 'June' month_name
	UNION ALL  
	SELECT 'July' id, 'July' month_name
	UNION ALL  
	SELECT 'August' id, 'August' month_name
	UNION ALL  
	SELECT 'September' id, 'September' month_name
	UNION ALL  
	SELECT 'October' id, 'October' month_name
	UNION ALL  
	SELECT 'November' id, 'November' month_name
	UNION ALL  
	SELECT 'December' id, 'December' month_name;                              
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_d`(p_notification_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_notification_member
	SET is_delete = 1,
			user_id = p_user_id
	WHERE notification_member_id = p_notification_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.notification_member_id, m.notification_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.notification_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.notification_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_notification_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY notification_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_gid`(p_notification_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.notification_member_id, m.notification_member_name, m.designation,  m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_notification_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.notification_member_id = p_notification_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_i`(p_notification_member_id smallint, p_notification_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_notification_member m WHERE m.notification_member_id = p_notification_member_id) THEN
		SET p_notification_member_id = (SELECT IFNULL(MAX(notification_member_id), 0) + 1 FROM tbl_notification_member);

		INSERT INTO tbl_notification_member(notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_notification_member_id, p_notification_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_notification_member
		SET notification_member_name = p_notification_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE notification_member_id = p_notification_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_search`(p_notification_member_name varchar(150), p_designation varchar(150), p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.notification_member_id, m.notification_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.notification_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.notification_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_notification_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_notification_member_name = '' OR m.notification_member_name LIKE CONCAT('%',p_notification_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY notification_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.notification_member_id, m.notification_member_name
	FROM tbl_notification_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.notification_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_c`(p_language_id smallint, p_user_id int )
BEGIN
	#Routine body goes here...
	SELECT p.organization_id, p.organization_name
	FROM tbl_organization p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_d`(p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_organization
	SET is_delete = 1,
			user_id = p_user_id
	WHERE organization_id = p_organization_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.organization_id, p.organization_name, p.short_name, p.contact_person, p.designation, p.phone, p.cell_phone, p.fax, p.email, p.address, p.comments, p.language_id, p.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.organization_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.organization_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_id, organization_name, short_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_gid`(p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.organization_id, p.organization_name, p.short_name, p.contact_person, p.designation, p.phone, p.cell_phone, p.fax, p.email, p.address, p.comments, p.language_id, p.user_id, l.language
	FROM tbl_organization p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.organization_id = p_organization_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_i`(p_organization_id smallint, p_organization_name varchar(150), p_short_name varchar(5), p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_organization c WHERE c.organization_id = p_organization_id) THEN

		SET p_organization_id = (SELECT IFNULL(MAX(organization_id), 0) + 1 FROM tbl_organization);

		INSERT INTO tbl_organization(organization_id, organization_name, short_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_organization_id, p_organization_name, p_short_name, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_organization
		SET organization_name = p_organization_name, 
				short_name = p_short_name, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE organization_id = p_organization_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_d`(p_organization_member_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_organization_member 
	SET is_delete = 1,
	user_id = p_user_id
	WHERE organization_member_id = p_organization_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.organization_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.organization_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization_member c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_member_id, organization_id, organization_name, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_gid`(p_organization_member_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
	FROM tbl_organization_member c 
	INNER JOIN tbl_language l ON l.language_id = c.language_id
	INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
	WHERE c.organization_member_id = p_organization_member_id		
	AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_i`(p_organization_member_id int, p_organization_id smallint, p_organization_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_organization_member c WHERE c.organization_member_id = p_organization_member_id) THEN

		SET p_organization_member_id = (SELECT IFNULL(MAX(organization_member_id), 0) + 1 FROM tbl_organization_member);

		INSERT INTO tbl_organization_member(organization_member_id, organization_id, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_organization_member_id, p_organization_id, p_organization_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_organization_member
		SET organization_id = p_organization_id, 
				organization_member_name = p_organization_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = user_id
		WHERE organization_member_id = p_organization_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_search`(p_organization_id smallint, p_organization_member_name varchar(150), p_designation varchar(100),  p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid_member(",c.organization_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.organization_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization_member c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
		WHERE c.language_id = p_language_id						
		AND (p_organization_id = 0 OR c.organization_id = p_organization_id)
		AND (p_organization_member_name = '' OR c.organization_member_name LIKE CONCAT('%',p_organization_member_name,'%'))
		AND (p_designation = '' OR c.designation LIKE CONCAT('%',p_designation,'%'))
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_member_id, organization_id, organization_name, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.organization_id, m.organization_name
	FROM tbl_organization m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.organization_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_d`(p_others_org_ministry_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_others
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE others_org_ministry_id  = p_others_org_ministry_id ;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.others_org_ministry_id, c.others_org_ministry, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.others_org_ministry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.others_org_ministry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_others c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT others_org_ministry_id, others_org_ministry, language_id, language, is_edit, is_delete FROM temp_all ORDER BY others_org_ministry_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_gid`(p_others_org_ministry_id smallint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.others_org_ministry_id, c.others_org_ministry, c.language_id, c.user_id, l.language
		FROM tbl_others c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.others_org_ministry_id = p_others_org_ministry_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_i`(p_others_org_ministry_id bigint , p_others_org_ministry varchar (150) , p_language_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_others c WHERE c.others_org_ministry_id = p_others_org_ministry_id) THEN

		SET p_others_org_ministry_id = (SELECT IFNULL(MAX(others_org_ministry_id), 0) + 1 FROM tbl_others);

		INSERT INTO tbl_others(others_org_ministry_id, others_org_ministry, language_id, is_delete, user_id)
		VALUES(p_others_org_ministry_id, p_others_org_ministry, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_others
		SET others_org_ministry = p_others_org_ministry,
				language_id = p_language_id, 
				user_id = user_id
		WHERE others_org_ministry_id = p_others_org_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.others_org_ministry_id, m.others_org_ministry
	FROM tbl_others m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.others_org_ministry LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_d`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_other_witnessess
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE witness_id  = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.witness_id, c.witness_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.witness_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.witness_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_other_witnessess c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT witness_id, witness_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY witness_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_gid`(p_witness_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.witness_id, c.witness_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
		FROM tbl_other_witnessess c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.witness_id = p_witness_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_i`(p_witness_id int , p_witness_name varchar (150) , p_designation varchar(100) , p_phone varchar(50) , p_cell_phone varchar(50) , p_fax varchar(50) , p_email varchar(150) , p_address varchar(250) , p_comments varchar(250) , p_language_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_other_witnessess c WHERE c.witness_id = p_witness_id) THEN

		SET p_witness_id = (SELECT IFNULL(MAX(witness_id), 0) + 1 FROM tbl_other_witnessess);

		INSERT INTO tbl_other_witnessess(witness_id, witness_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_witness_id, p_witness_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_other_witnessess
		SET witness_name = p_witness_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments,
				language_id = p_language_id, 
				user_id = user_id
		WHERE witness_id = p_witness_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.witness_id, m.witness_name
	FROM tbl_other_witnessess m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.witness_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.parliament_id, p.parliament FROM tbl_parliament p
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0
	ORDER BY p.parliament_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_d`(p_parliament_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_parliament
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE parliament_id = p_parliament_id ;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	#DECLARE p_count int;
	SET @sql = NULL;

	#SET p_count = (SELECT COUNT(*) FROM tbl_parliament p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id);

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT CONCAT('<font style="font-family: "Siyam Rupali"">', p.parliament_id,"</font>") parliament_id, 
    case 
	     when p_language_id=2 then CONCAT("<font size='4'>", p.parliament,"</font>")
	     else p.parliament
    end as parliament
    , p.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.parliament_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.parliament_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_parliament p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, language_id, language, is_edit,is_delete FROM temp_all ORDER BY parliament_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_gid`(p_parliament_id smallint)
BEGIN
	#Routine body goes here...
	SELECT p.parliament_id, p.parliament, p.language_id, p.is_delete, l.language FROM tbl_parliament p
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.parliament_id = p_parliament_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_i`(p_parliament_id smallint, p_parliament varchar(50), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_parliament p WHERE p.parliament_id = p_parliament_id) THEN

		SET p_parliament_id = (SELECT IFNULL(MAX(parliament_id), 0) + 1 FROM tbl_parliament);

		INSERT INTO tbl_parliament(parliament_id, parliament, language_id, is_delete, user_id)
		VALUES(p_parliament_id, p_parliament, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_parliament
		SET parliament = p_parliament, 
				language_id = p_language_id,
				user_id = p_user_id
				#is_delete = 0

		WHERE parliament_id = p_parliament_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_member_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_member_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_member_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.member_id, p.member_name
	FROM tbl_parliament_member p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id
	AND p.is_active = 1
	AND p.is_delete = 0;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_member_d`(p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_parliament_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE member_id = p_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.member_id, m.member_name, m.designation, m.constituency, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, l.language,
						CONCAT("<input type='button' onClick='popupwinid(",m.member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",m.member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_parliament_member m 		
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id		
		AND m.is_delete = 0	
		#AND m.is_active = 1
	);
	SET @sql = CONCAT("SELECT member_id, member_name, designation, constituency, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit, is_delete FROM temp_all ORDER BY member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_member_gid`(p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	m.member_id, m.member_name, m.designation, m.constituency, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.is_active, l.language
	FROM tbl_parliament_member m 		
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.member_id = p_member_id
	#AND m.is_active = 1
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_member_i`(p_member_id int, p_member_name varchar(250), p_designation varchar(100), p_constituency varchar(250), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_is_active smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_parliament_member m WHERE m.member_id = p_member_id) THEN
		SET p_member_id = (SELECT IFNULL(MAX(member_id), 0) + 1 FROM tbl_parliament_member);

		INSERT INTO tbl_parliament_member(member_id, member_name, designation, constituency, phone, cell_phone, fax, email, address, comments, language_id, is_active, is_delete, user_id)
		VALUES(p_member_id, p_member_name, p_designation, p_constituency, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, p_is_active, 0, p_user_id);

	ELSE
		UPDATE tbl_parliament_member
		SET member_name = p_member_name, 
				designation = p_designation,
				constituency = p_constituency,
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments,
				language_id = p_language_id, 
				is_active = p_is_active,
				user_id = p_user_id
		WHERE member_id = p_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_pdf_file_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_pdf_file_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_pdf_file_gid`(p_pdf_id int, p_pdf_type smallint)
BEGIN
	#Routine body goes here...
	DECLARE v_path VARCHAR(200);
	SET v_path = (SELECT p.download_path FROM tbl_download_path p WHERE p.is_active = 1);
	SELECT 	t.pdf_id, 
					CONCAT(v_path, t.pdf_path) pdf_path
	FROM tbl_pdf_file t	
	WHERE t.pdf_id = p_pdf_id
	AND t.pdf_type = p_pdf_type;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_pdf_file_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_pdf_file_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_pdf_file_i`(p_pdf_id int, p_pdf_type smallint, p_pdf_path varchar(250), p_user_id int)
BEGIN
	#Routine body goes here...	
		IF NOT EXISTS(SELECT * FROM tbl_pdf_file c WHERE c.pdf_id = p_pdf_id AND c.pdf_type = p_pdf_type) THEN
			INSERT INTO tbl_pdf_file(pdf_id, pdf_type, pdf_path, comments, user_id)
			VALUES(p_pdf_id, p_pdf_type, p_pdf_path, 'pdf type : 1 for Manage Inquiry, 2 for Evidence Request, 3 for Manage Sitting, 4 for Briefing Note, 5 for Record of Dession, 6 for Inquiry Report, 7 for Followup Report', p_user_id);
		ELSE
			UPDATE tbl_pdf_file
			SET pdf_type = p_pdf_type, 
					pdf_path = p_pdf_path, 
					user_id = p_user_id
			WHERE pdf_id = p_pdf_id
			AND pdf_type = p_pdf_type;
		END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_d`(p_rec_evidence_details_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_receive_evidence_d
	SET is_delete = 1,
			user_id = p_user_id
	WHERE rec_evidence_details_id  = p_rec_evidence_details_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.inquiry_id, 
						DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
						t.rec_evidence_details_id, 
						t.evidence_type, 
						t.title, 
						t.submitted_by, 
						DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, 
						t.ref_no, 
						DATE_FORMAT(t.ref_date, '%d-%m-%Y') ref_date, 
						t.file_name,
						t.doc_type,
						t.create_date,
						t.user_id
		FROM tbl_receive_evidence_d t
		INNER JOIN tbl_receive_evidence_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
		/*
		SELECT t.doc_reg_id, DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, t.rec_evidence_details_id, t.evidence_type, t.title, t.submitted_by, DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, t.user_id
		FROM tbl_receive_evidence_d t
		INNER JOIN tbl_receive_evidence_master i ON i.doc_reg_id = t.doc_reg_id AND i.is_delete = 0		
		WHERE t.doc_reg_id = p_doc_reg_id
		AND t.is_delete = 0
		*/
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, rec_evidence_details_id, evidence_type, title, submitted_by, receive_date, ref_no, ref_date, file_name, doc_type, user_id FROM temp_all ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_gid`(p_rec_evidence_details_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	t.inquiry_id, 
					DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
					t.rec_evidence_details_id, 
					t.evidence_type, 
					t.title, 
					t.submitted_by, 
					DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, 
					t.ref_no, 
					DATE_FORMAT(t.ref_date, '%d-%m-%Y') ref_date, 
					t.file_name,
					t.doc_type,
					t.create_date,
					t.user_id
	FROM tbl_receive_evidence_d t
	INNER JOIN tbl_receive_evidence_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
	WHERE t.rec_evidence_details_id = p_rec_evidence_details_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_i`(p_inquiry_id int , p_rec_evidence_details_id bigint , p_evidence_type varchar (100) , p_title longtext , p_submitted_by varchar (200) , p_receive_date varchar(10), p_ref_no varchar(150), p_ref_date varchar(10), p_doc_type varchar(50), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_name varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = 'evidence/';

	IF NOT EXISTS(SELECT * FROM tbl_receive_evidence_d c WHERE c.rec_evidence_details_id = p_rec_evidence_details_id) THEN		

		SET p_rec_evidence_details_id = (SELECT IFNULL(MAX(rec_evidence_details_id), 0) + 1 FROM tbl_receive_evidence_d);

		SET v_file_name = CONCAT(v_path, p_rec_evidence_details_id, '.', p_extension);

		INSERT INTO tbl_receive_evidence_d(inquiry_id, rec_evidence_details_id, evidence_type, title, submitted_by, receive_date, ref_no, ref_date, file_name, doc_type, create_date, is_delete, user_id)
		VALUES(p_inquiry_id, p_rec_evidence_details_id, p_evidence_type, p_title, p_submitted_by, STR_TO_DATE(p_receive_date,'%d-%m-%Y'), p_ref_no, STR_TO_DATE(p_ref_date, '%d-%m-%Y'), v_file_name, p_doc_type, NOW(), 0, p_user_id);
	ELSE

		IF(TRIM(p_doc_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_name = CONCAT(v_path, p_rec_evidence_details_id, '.', p_extension);
	
			UPDATE tbl_receive_evidence_d
			SET inquiry_id = p_inquiry_id, 
					evidence_type = p_evidence_type, 
					title = p_title, 
					submitted_by = p_submitted_by, 
					receive_date = STR_TO_DATE(p_receive_date,'%d-%m-%Y'),
					ref_no = p_ref_no,
					ref_date = STR_TO_DATE(p_ref_date, '%d-%m-%Y'),
					file_name = v_file_name, 
					doc_type = p_doc_type,
					user_id = p_user_id
			WHERE rec_evidence_details_id = p_rec_evidence_details_id;
		ELSE

			UPDATE tbl_receive_evidence_d
			SET inquiry_id = p_inquiry_id, 
					evidence_type = p_evidence_type, 
					title = p_title, 
					submitted_by = p_submitted_by, 
					receive_date = STR_TO_DATE(p_receive_date,'%d-%m-%Y'),
					ref_no = p_ref_no,
					ref_date = STR_TO_DATE(p_ref_date, '%d-%m-%Y'),
					user_id = p_user_id
			WHERE rec_evidence_details_id = p_rec_evidence_details_id;
		END IF;
	END IF;

	SELECT p_rec_evidence_details_id rec_evidence_details_id, v_path file_path, v_file_name file_name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_d`(p_doc_reg_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_receive_evidence_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.inquiry_id, 
						DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
						t.user_id, 
						i.inquiry_no, 
						i.inquiry_title,
						DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date, 
						CONCAT("<input type='button' onClick='popupwinid(",t.inquiry_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",t.inquiry_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_receive_evidence_master t		
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.is_delete = 0
		AND i.language_id  = p_language_id 
		AND i.committee_id = p_committee_id
		ORDER BY t.receive_deadline_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, inquiry_no, inquiry_title, create_date, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_gid`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	
	SELECT 	i.inquiry_id, 
					DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
					i.inquiry_no, 
					i.inquiry_title,
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date,
					t.remarks
	FROM tbl_inquiry_master i
	LEFT OUTER JOIN tbl_receive_evidence_master t ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0	
	WHERE i.inquiry_id = p_inquiry_id
	AND IFNULL(t.is_delete,0) = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_i`(p_inquiry_id int , p_receive_deadline_date varchar(10), p_remarks longtext, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_receive_evidence_master c WHERE c.inquiry_id = p_inquiry_id) THEN				

		INSERT INTO tbl_receive_evidence_master(inquiry_id, receive_deadline_date, remarks, is_delete, user_id)
		VALUES(p_inquiry_id, STR_TO_DATE(p_receive_deadline_date,'%d-%m-%Y'), p_remarks, 0, p_user_id);
	ELSE
		UPDATE tbl_receive_evidence_master
		SET receive_deadline_date = STR_TO_DATE(p_receive_deadline_date,'%d-%m-%Y'), 
				remarks = p_remarks,
				user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id;
	END IF;

	#DELETE FROM tbl_receive_evidence_d WHERE inquiry_id = p_inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(256), p_start_date varchar(10), p_end_date varchar(10), p_organization_name varchar(150), p_doc_reg_no varchar(150), p_deadline varchar(10), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	i.inquiry_id, 
						DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date, 
						CONCAT("<input type='button' onClick='popupwinid(",i.inquiry_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",i.inquiry_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_inquiry_master i
		LEFT OUTER JOIN tbl_receive_evidence_master t ON i.inquiry_id = t.inquiry_id AND t.is_delete = 0
		LEFT OUTER JOIN tbl_language l ON l.language_id = i.language_id 
		LEFT OUTER JOIN tbl_committee c ON c.committee_id = i.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_organization io ON io.inquiry_id = i.inquiry_id AND io.is_delete = 0
		LEFT OUTER JOIN tbl_organization o ON o.organization_id = io.organization_id AND o.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag dt ON dt.inquiry_id = i.inquiry_id AND dt.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master d ON d.doc_reg_id = dt.doc_reg_id AND d.is_delete = 0
		WHERE i.language_id  = p_language_id 
		AND i.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR i.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND (p_inquiry_title = '' OR i.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))	
		AND (p_organization_name = '' OR o.organization_name LIKE CONCAT('%', p_organization_name, '%'))
		AND (p_doc_reg_no = '' OR d.doc_reg_no LIKE CONCAT('%', p_doc_reg_no, '%'))
		AND (p_start_date = '' OR p_end_date = '' OR (i.create_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))			
		AND (p_deadline = '' OR STR_TO_DATE(DATE_FORMAT(t.receive_deadline_date, '%d-%m-%Y'), '%d-%m-%Y') = STR_TO_DATE(p_deadline, '%d-%m-%Y'))
		AND i.is_delete = 0
		ORDER BY i.create_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, inquiry_no, inquiry_title, create_date, is_edit, is_delete	FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_adviser_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_adviser_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_adviser_d`(p_meeting_notice_id int, p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_adviser
	WHERE adviser_id = p_adviser_id
	AND meeting_notice_id = p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_adviser_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_adviser_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_adviser_gall`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.meeting_notice_id, t.adviser_id, d.adviser_name
		FROM tbl_record_of_decision_adviser t
		INNER JOIN tbl_specialist_adviser d ON d.adviser_id = t.adviser_id AND d.is_delete = 0
		WHERE t.meeting_notice_id = p_meeting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT adviser_id, adviser_name FROM temp_all ORDER BY adviser_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_adviser_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_adviser_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_adviser_gid`(p_meeting_notice_id int, p_adviser_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.adviser_id, i.adviser_name, i.designation, t.meeting_notice_id
	FROM tbl_record_of_decision_adviser t
	INNER JOIN tbl_specialist_adviser i ON i.adviser_id = t.adviser_id AND i.is_delete = 0
	WHERE t.meeting_notice_id = p_meeting_notice_id
	AND t.adviser_id =  p_adviser_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_adviser_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_adviser_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_adviser_i`(p_metting_notice_id int, p_adviser_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_adviser c WHERE c.meeting_notice_id = p_metting_notice_id AND c.adviser_id = p_adviser_id) THEN		
		INSERT INTO tbl_record_of_decision_adviser(meeting_notice_id, adviser_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_adviser_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_adviser
		SET user_id = p_user_id
		WHERE meeting_notice_id = p_metting_notice_id 
		AND adviser_id = p_adviser_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_all_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_all_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_all_gall`(p_type varchar(20),  p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
CASE p_type
    WHEN 'cmember' THEN
			CALL tbl_record_of_decision_committee_gall(p_meeting_notice_id);
		
		WHEN 'logistic' THEN
			CALL tbl_record_of_decision_logistic_gall(p_meeting_notice_id);

		WHEN 'witness' THEN
			CALL tbl_record_of_decision_witness_gall(p_meeting_notice_id);

WHEN 'notification' THEN
			CALL tbl_record_of_decision_notification_gall(p_meeting_notice_id);

WHEN 'invitees' THEN
			CALL tbl_record_of_decision_invitees_gall(p_meeting_notice_id);

END CASE;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_all_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_all_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_all_token`(p_type varchar(20), p_keyword varchar(100), p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
	CASE p_type
			WHEN 'cmember' THEN
				SELECT mn.member_id id, cm.member_name name
				FROM tbl_meeting_notice_committee mn 
				INNER JOIN tbl_committee_member cm ON mn.member_id = cm.committee_member_id
				WHERE mn.metting_notice_id=p_meeting_notice_id AND cm.member_name LIKE CONCAT('%',p_keyword,'%');
			
			WHEN 'logistic' THEN
				SELECT mn.logistic_id id, lm.logistic_member_name name
				FROM tbl_meeting_notice_logistic mn
				INNER JOIN tbl_logistic_member lm ON lm.logistic_member_id = mn.logistic_id
				WHERE mn.metting_notice_id=p_meeting_notice_id AND lm.logistic_member_name LIKE CONCAT('%',p_keyword,'%');

			WHEN 'witness' THEN
			
				DROP TEMPORARY TABLE IF EXISTS temp_mem;
				CREATE TEMPORARY TABLE temp_mem AS
				(
					SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
					FROM tbl_meeting_notice_witness t				
					INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id
					WHERE t.is_delete = 0		
					AND t.witness_type = 'M'
					AND t.metting_notice_id = p_meeting_notice_id
				);

				INSERT INTO temp_mem
				SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
				FROM tbl_meeting_notice_witness t			
				INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id
				WHERE t.is_delete = 0
				AND t.witness_type = 'O'
				AND t.metting_notice_id = p_meeting_notice_id;

				INSERT INTO temp_mem
				SELECT t.witness_id, t.witness_type, o.witness_name member_name
				FROM tbl_meeting_notice_witness t			
				INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0			
				WHERE t.metting_notice_id = p_meeting_notice_id
				AND t.witness_type = 'T'
				AND t.is_delete = 0;

				SELECT CONCAT(t.witness_id, '#', t.witness_type) id, t.member_name name
				FROM temp_mem t
				WHERE t.member_name LIKE CONCAT('%',p_keyword,'%');

	WHEN 'notification' THEN
				SELECT mn.notification_id id, lm.notification_member_name name
				FROM tbl_meeting_notice_notification mn
				INNER JOIN tbl_notification_member lm ON lm.notification_member_id = mn.notification_id
				WHERE mn.metting_notice_id = p_meeting_notice_id AND lm.notification_member_name LIKE CONCAT('%',p_keyword,'%');

	WHEN 'invitees' THEN
				SELECT mn.invitees_id id, lm.invitees_name name
				FROM tbl_meeting_notice_invitees mn
				INNER JOIN tbl_invitees lm ON lm.invitees_id = mn.invitees_id
				WHERE mn.metting_notice_id = p_meeting_notice_id AND lm.invitees_name LIKE CONCAT('%',p_keyword,'%');

	END CASE;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_chair_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_chair_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_chair_c`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
SELECT mn.member_id id, cm.member_name name
FROM tbl_meeting_notice_committee mn 
INNER JOIN tbl_committee_member cm ON mn.member_id = cm.committee_member_id
WHERE mn.metting_notice_id=p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_d`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_committee
	WHERE metting_notice_id = p_metting_notice_id 
	AND member_id = p_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
		FROM tbl_record_of_decision_committee t
		INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
		INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT member_id, member_name FROM temp_all ORDER BY member_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_gid`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
	FROM tbl_record_of_decision_committee t
	INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.member_id = p_member_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_i`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_committee c WHERE c.metting_notice_id = p_metting_notice_id AND c.member_id = p_member_id) THEN		
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_member_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_committee
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND member_id = p_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_d`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_inquiry_tag
	WHERE inquiry_id = p_inquiry_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, t.metting_notice_id, 
		CASE 
				WHEN t.`status` = 0 THEN "Ongoing"
				WHEN t.`status` = 1 THEN "Completed"
		END AS `status`,
		CASE 
			WHEN t.`status` = 0 THEN CONCAT("<input type='button' onClick='settled(",t.metting_notice_id,",", t.inquiry_id,");' name='basic' style='width:120px;background-color:green;' value='Completed' class='gridbutton'/>") 
			WHEN t.`status` = 1 THEN CONCAT("<input type='button' onClick='settled(",t.metting_notice_id,",", t.inquiry_id,");' name='basic' style='width:120px;background-color:red;' value='Ongoing' class='gridbutton'/>") 			
		END AS is_settle
		FROM tbl_record_of_decision_inquiry_tag t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no, status, is_settle FROM temp_all ORDER BY inquiry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_gid`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.metting_notice_id, t.`status`
	FROM tbl_record_of_decision_inquiry_tag t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.inquiry_id =  p_inquiry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_i`(p_metting_notice_id int, p_inquiry_id int , p_user_id int)
BEGIN
	#Routine body goes here...
# New Code By Pijush

	DECLARE p_status INT;
	SET p_status = (SELECT it.status FROM tbl_record_of_decision_inquiry_tag it WHERE it.metting_notice_id = p_metting_notice_id AND it.inquiry_id=p_inquiry_id);

	IF(p_status = 0) THEN
		UPDATE tbl_record_of_decision_inquiry_tag		
		SET user_id = p_user_id,
				status = 1
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	ELSE
		UPDATE tbl_record_of_decision_inquiry_tag		
		SET user_id = p_user_id,
				status = 0
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	END IF;


# Old Code by taufiq
#	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_inquiry_tag c WHERE c.metting_notice_id = p_metting_notice_id AND c.inquiry_id = p_inquiry_id) THEN		
#		INSERT INTO tbl_record_of_decision_inquiry_tag(metting_notice_id, inquiry_id, status, is_delete, user_id)
#		VALUES(p_metting_notice_id, p_inquiry_id, p_status, 0, p_user_id);
#	ELSE
#		UPDATE tbl_record_of_decision_inquiry_tag		
#		SET user_id = p_user_id,
#				status = p_status
#		WHERE metting_notice_id = p_metting_notice_id 
#		AND inquiry_id = p_inquiry_id;
#	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_d`(p_metting_notice_id int, p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_invitees
	WHERE invitees_id  = p_invitees_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name
		FROM tbl_record_of_decision_invitees t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0				
		INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id		
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name FROM temp_all ORDER BY invitees_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_gid`(p_invitees_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name, v.invitees_organization, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_record_of_decision_invitees t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.invitees_id = p_invitees_id
	AND l.language_id = m.language_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_i`(p_metting_notice_id int , p_invitees_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_invitees c WHERE c.invitees_id = p_invitees_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_invitees_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_invitees
		SET user_id = p_user_id
		WHERE invitees_id = p_invitees_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_d`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_logistic
	WHERE metting_notice_id = p_metting_notice_id 
	AND logistic_id = p_logistic_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
		FROM tbl_record_of_decision_logistic t
		INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.logistic_id AND i.is_delete = 0		
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT logistic_id, logistic_member_name FROM temp_all ORDER BY logistic_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_gid`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
	FROM tbl_record_of_decision_logistic t
	INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.p_logistic_id AND i.is_delete = 0	
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.logistic_id = p_logistic_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_i`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_logistic c WHERE c.metting_notice_id = p_metting_notice_id AND c.logistic_id = p_logistic_id) THEN		
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_logistic_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_logistic
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND logistic_id = p_logistic_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_record_of_decision_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.chair_member_id, t.private_business, t.public_business, t.result_of_deliberation, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_record_of_decision_master t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.language_id = p_language_id
		AND t.committee_id = p_committee_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_gid`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	t.metting_notice_id, 
					t.chair_member_id, 
					t.private_business, 
					t.public_business, 
					t.result_of_deliberation, 
					t.remarks,
					t.language_id, 
					l.language, 
					t.committee_id, 
					c.committee_name, 
					t.user_id
	FROM tbl_record_of_decision_master t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_i`(p_metting_notice_id int, p_chair_member_id smallint, p_committee varchar(100), p_witness varchar(100), p_adviser varchar(100), p_officer varchar(100), p_logistic varchar(100), p_notification varchar(100), p_invitees varchar(100),  p_private_business longtext , p_public_business longtext, p_result_of_deliberation longtext , p_remarks longtext, p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	#IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_master c WHERE c.metting_notice_id = p_metting_notice_id) THEN		
	#
	#	INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id , is_delete, user_id)
	#	VALUES(p_metting_notice_id, p_chair_member_id, p_private_business, p_public_business, p_result_of_deliberation, p_language_id, p_committee_id , 0, p_user_id);
	#ELSE
		UPDATE tbl_record_of_decision_master
		SET chair_member_id = p_chair_member_id, 
				private_business = p_private_business, 
				public_business = p_public_business, 
				result_of_deliberation = p_result_of_deliberation,
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id, 
				flag = 1,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	#END IF;

	#committee Information

	DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness)-LENGTH(replace(p_witness,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness, ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_record_of_decision_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


#adviser  Information

	DELETE FROM tbl_record_of_decision_adviser WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_adviser  != '' THEN		
		SET occurance = LENGTH(p_adviser)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			#SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1);
			#SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			#SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_adviser_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#adviser  Information


#officer  Information

	DELETE FROM tbl_record_of_decision_officer WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_officer  != '' THEN		
		SET occurance = LENGTH(p_officer)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			#SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1);
			#SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			#SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_officer, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_officer_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#officer  Information


	#p_logistics Information

	DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistic != '' THEN		
		SET occurance = LENGTH(p_logistic)-LENGTH(replace(p_logistic,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistic, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_issued`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_record_of_decision_master
	SET is_issued = 1,
			issued_date = NOW(),
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;

	#UPDATE tbl_record_of_decision_inquiry_tag
	#SET status = 1,
	#		user_id = p_user_id
	#WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_d`(p_metting_notice_id int, p_notification_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_notification
	WHERE notification_id  = p_notification_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name
		FROM tbl_record_of_decision_notification t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0						
		INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id		
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT notification_id, notification_member_name FROM temp_all ORDER BY notification_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_gid`(p_metting_notice_id int, p_notification_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_record_of_decision_notification t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.notification_id = p_notification_id
	AND l.language_id = m.language_id
	AND t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_i`(p_metting_notice_id int , p_notification_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_notification c WHERE c.notification_id = p_notification_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_notification_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_notification
		SET user_id = p_user_id
		WHERE notification_id = p_notification_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_officer_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_officer_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_officer_d`(p_meeting_notice_id int, p_officer_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_officer
	WHERE officer_id = p_officer_id
	AND meeting_notice_id = p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_officer_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_officer_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_officer_gall`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.meeting_notice_id, t.officer_id, d.officer_name
		FROM tbl_record_of_decision_officer t
		INNER JOIN tbl_committee_officer d ON d.officer_id = t.officer_id AND d.is_delete = 0
		WHERE t.meeting_notice_id = p_meeting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT officer_id, officer_name FROM temp_all ORDER BY officer_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_officer_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_officer_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_officer_gid`(p_meeting_notice_id int, p_officer_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.officer_id, i.officer_name, i.designation, t.meeting_notice_id
	FROM tbl_record_of_decision_officer t
	INNER JOIN tbl_committee_officer i ON i.officer_id = t.officer_id AND i.is_delete = 0
	WHERE t.meeting_notice_id = p_meeting_notice_id
	AND t.officer_id = p_officer_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_officer_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_officer_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_officer_i`(p_metting_notice_id int, p_officer_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_officer c WHERE c.meeting_notice_id = p_metting_notice_id AND c.officer_id = p_officer_id) THEN		
		INSERT INTO tbl_record_of_decision_officer(meeting_notice_id, officer_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_officer_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_officer
		SET user_id = p_user_id
		WHERE meeting_notice_id = p_metting_notice_id 
		AND officer_id = p_officer_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_d`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_record_of_decision_witness
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id = p_metting_notice_id 
	AND witness_id = p_witness_id
	AND witness_type = p_witness_type;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_mem;

	CREATE TEMPORARY TABLE temp_mem AS
	(
		SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
		FROM tbl_record_of_decision_witness t		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.metting_notice_id = p_metting_notice_id
	);

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_metting_notice_id;

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.witness_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_metting_notice_id;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, i.private_business, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
		FROM tbl_record_of_decision_witness t	
		INNER JOIN tbl_record_of_decision_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN temp_mem m ON m.witness_id = t.witness_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_gid`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.witness_id, t.witness_type, t.user_id
	FROM tbl_record_of_decision_witness t
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.witness_id = p_witness_id
	AND t.witness_type = p_witness_type
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_i`(p_metting_notice_id int, p_witness_type char(1), p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_witness c WHERE c.metting_notice_id = p_metting_notice_id AND c.witness_id = p_witness_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
		VALUES(p_metting_notice_id, p_witness_id, p_witness_type, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_witness
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND witness_id = p_witness_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_d`(p_request_evidence_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_request_evidence
	SET is_delete = 1,
			user_id = p_user_id
	WHERE m.request_evidence_id = p_request_evidence_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.request_evidence_id, 
						m.inquiry_id, 
						i.inquiry_no, 
						i.inquiry_title, 
						m.metting_notice_id, 
						DATE_FORMAT(m.request_date,'%d-%m-%Y') request_date, 
						m.cover_letter, 
						CASE 
							WHEN m.is_issued = 0 THEN CONCAT("<input type='button' onClick='popupwinid(",m.request_evidence_id,");' name='basic' value='Edit' class='gridbutton'/>") 
							ELSE CONCAT("<input type='button' onClick='popupwinid(",m.request_evidence_id,");' name='basic' value='Edit' class='gridbutton' disabled style='background-color:gray;'/>")
						END is_edit,
						CASE 
							WHEN m.is_issued = 0 THEN CONCAT("<input type='button' onClick='issuedid(",m.request_evidence_id,");' name='basic' value='Issue' class='gridbutton'/>") 
							ELSE CONCAT("<input type='button' onClick='issuedid(",m.request_evidence_id,");' name='basic' value='Issue' class='gridbutton' disabled style='background-color:gray;'/>")
						END is_issued,
						CONCAT("<input type='button' onClick='deleteid(",m.request_evidence_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' name='",m.request_evidence_id,"' value='Print View' class='gridbutton'/>") is_print
		FROM tbl_request_evidence m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 				
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
		WHERE m.language_id = p_language_id		
		AND m.committee_id = p_committee_id
		AND m.is_delete = 0		
		ORDER BY m.request_date DESC
	);
	SET @sql = CONCAT("SELECT request_evidence_id, inquiry_id, inquiry_no, inquiry_title, metting_notice_id, request_date, cover_letter, is_edit, is_issued, is_delete, is_print FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_gid`(p_request_evidence_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT 	m.request_evidence_id, 
					m.inquiry_id, 
					i.inquiry_no, 
					i.inquiry_title, 
					m.metting_notice_id, 
					DATE_FORMAT(m.request_date,'%d-%m-%Y') request_date, 
					m.cover_letter, 
					p.parliament,
					s.session,
					m.user_id,
					m.remarks,
					m.is_issued
	FROM tbl_request_evidence m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 			
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_parliament p ON p.parliament_id = i.parliament_id 
	INNER JOIN tbl_session s ON s.session_id = i.session_id
	WHERE m.request_evidence_id = p_request_evidence_id			
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_i`(p_request_evidence_id int, p_inquiry_id int, p_metting_notice_id int, p_request_date varchar(10), p_cover_letter longtext, p_remarks longtext, p_committee_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_request_evidence m WHERE m.request_evidence_id = p_request_evidence_id) THEN
		SET p_request_evidence_id = (SELECT IFNULL(MAX(request_evidence_id), 0) + 1 FROM tbl_request_evidence);

		INSERT INTO tbl_request_evidence(request_evidence_id, inquiry_id, metting_notice_id, request_date, cover_letter, remarks, committee_id, language_id, is_issued, is_delete, entry_date, user_id)
		VALUES(p_request_evidence_id, p_inquiry_id, p_metting_notice_id, STR_TO_DATE(p_request_date,'%d-%m-%Y'), p_cover_letter, p_remarks, p_committee_id, p_language_id, 0, 0, NOW(), p_user_id);

	ELSE
		UPDATE tbl_request_evidence
		SET inquiry_id = p_inquiry_id, 
				metting_notice_id = p_metting_notice_id,				
				request_date = STR_TO_DATE(p_request_date,'%d-%m-%Y'), 
				cover_letter = p_cover_letter,
				remarks = p_remarks,
				committee_id = p_committee_id,
				language_id = p_language_id,
				user_id = p_user_id
		WHERE request_evidence_id = p_request_evidence_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_issued`(p_request_evidence_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_request_evidence
	SET is_issued = 1,
			issued_date = NOW(),
			user_id = p_user_id
	WHERE request_evidence_id = p_request_evidence_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_prepopulated`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_prepopulated`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_prepopulated`(p_request_evidence_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, CONCAT(i.inquiry_no,':',i.inquiry_title) inquiry_no
		FROM tbl_request_evidence t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.request_evidence_id = p_request_evidence_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no FROM temp_all ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT s.session_id, s.`session` FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.language_id = p_language_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_c_by_parliament`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_c_by_parliament`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_c_by_parliament`(p_parliament_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT s.session_id, s.`session` FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.language_id = p_language_id
	AND s.parliament_id = p_parliament_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_d`(p_session_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_session
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE session_id = p_session_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.parliament_id, p.parliament, s.session_id, s.session, s.language_id, l.`language`,
					CONCAT("<input type='button' onClick='popupwinid(",s.session_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.session_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_session s 
		INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
		INNER JOIN tbl_language l ON s.language_id = l.language_id
		WHERE s.language_id = p_language_id
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, session_id, session, language_id, language, is_edit, is_delete FROM temp_all ORDER BY session_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_gid`(p_session_id smallint)
BEGIN
	#Routine body goes here...
	SELECT s.parliament_id, p.parliament, s.session_id, s.`session`, s.language_id, l.language FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.session_id = p_session_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_i`(p_parliament_id smallint, p_session_id smallint, p_session varchar(50), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
		IF NOT EXISTS(SELECT * FROM tbl_session s WHERE s.session_id = p_session_id) THEN

		SET p_session_id =	(SELECT IFNULL(MAX(session_id), 0) + 1 FROM tbl_session);

		INSERT INTO tbl_session(parliament_id, session_id, `session`, language_id, is_delete, user_id)
		VALUES(p_parliament_id, p_session_id, p_session, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_session
		SET parliament_id = p_parliament_id, 
				`session` = p_session,
				language_id = p_language_id,
				user_id  = p_user_id 
		WHERE session_id = p_session_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_par_id`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_par_id`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_par_id`(p_language_id smallint, p_parliament_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.parliament_id, p.parliament, s.session_id, s.`session`, s.language_id, l.`language`,
					CONCAT("<input type='button' onClick='popupwinid(",s.session_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.session_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_session s 
		INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
		INNER JOIN tbl_language l ON s.language_id = l.language_id
		WHERE s.language_id = p_language_id
		AND s.parliament_id = p_parliament_id
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, session_id, session, language_id, language, is_edit, is_delete FROM temp_all ORDER BY session_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_specialist_adviser_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_specialist_adviser_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_specialist_adviser_d`(p_adviser_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_specialist_adviser
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE adviser_id = p_adviser_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_specialist_adviser_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_specialist_adviser_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_specialist_adviser_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.adviser_id, m.adviser_name, m.adviser_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.adviser_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.adviser_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_specialist_adviser m 		
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT adviser_id, adviser_name, adviser_organization, designation, phone, cell_phone, fax, email, address, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY adviser_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_specialist_adviser_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_specialist_adviser_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_specialist_adviser_gid`(p_adviser_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.adviser_id, m.adviser_name, m.adviser_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language
	FROM tbl_specialist_adviser m 	
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.adviser_id = p_adviser_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_specialist_adviser_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_specialist_adviser_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_specialist_adviser_i`(p_adviser_id int, p_adviser_name varchar(150), p_adviser_organization varchar(250), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_specialist_adviser m WHERE m.adviser_id = p_adviser_id) THEN
		SET p_adviser_id = (SELECT IFNULL(MAX(adviser_id), 0) + 1 FROM tbl_specialist_adviser);

		INSERT INTO tbl_specialist_adviser(adviser_id, adviser_name, adviser_organization, designation, phone, cell_phone, fax, email, address, language_id, is_delete, user_id)
		VALUES(p_adviser_id, p_adviser_name, p_adviser_organization, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_specialist_adviser
		SET adviser_name = p_adviser_name, 
				adviser_organization = p_adviser_organization, 
				designation = p_designation, 				
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE adviser_id = p_adviser_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_specialist_adviser_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_specialist_adviser_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_specialist_adviser_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.adviser_id, m.adviser_name
	FROM tbl_specialist_adviser m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.adviser_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_c`(p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT s.sub_committee_id, s.sub_committee_name
		FROM tbl_sub_committee s 
		INNER JOIN tbl_language l ON l.language_id = s.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
		WHERE s.language_id = p_language_id
		AND s.committee_id = p_committee_id 
		AND s.is_delete = 0		;
	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_d`(p_sub_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_sub_committee
	SET is_delete = 1,
	user_id = p_user_id
	WHERE sub_committee_id = p_sub_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.sub_committee_id, s.committee_id, c.committee_name, s.sub_committee_name, s.user_id, s.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",s.sub_committee_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.sub_committee_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_sub_committee s 
		INNER JOIN tbl_language l ON l.language_id = s.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
		WHERE s.language_id = p_language_id
		AND s.committee_id = p_committee_id 
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT sub_committee_id, committee_id, committee_name, sub_committee_name, language_id, language, is_edit,is_delete FROM temp_all ORDER BY sub_committee_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_gid`(p_sub_committee_id smallint)
BEGIN
	#Routine body goes here...
	SELECT s.sub_committee_id, s.committee_id, c.committee_name, s.sub_committee_name, s.user_id, s.language_id, l.language
	FROM tbl_sub_committee s 
	INNER JOIN tbl_language l ON l.language_id = s.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
	WHERE s.sub_committee_id = p_sub_committee_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_i`(p_sub_committee_id smallint, p_committee_id smallint, p_sub_committee_name varchar(150), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_sub_committee c WHERE c.sub_committee_id = p_sub_committee_id) THEN

		SET p_sub_committee_id = (SELECT IFNULL(MAX(sub_committee_id), 0) + 1 FROM tbl_sub_committee);

		INSERT INTO tbl_sub_committee(sub_committee_id, committee_id, sub_committee_name, language_id, is_delete, user_id)
		VALUES(p_sub_committee_id, p_committee_id, p_sub_committee_name, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_sub_committee
		SET committee_id = p_committee_id, 
				sub_committee_name = p_sub_committee_name, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE sub_committee_id = p_sub_committee_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_member_d`(p_sub_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_sub_committee_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE sub_committee_member_id = p_sub_committee_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_member_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	s.sub_committee_member_id, 
						s.sub_committee_id,
						a.sub_committee_name,
						s.committee_member_id,
						m.member_name,
						m.designation,
						s.committee_id, 
						c.committee_name, 
						s.language_id, 
						l.language,
						CONCAT("<input type='button' onClick='popupwinid(",s.sub_committee_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.sub_committee_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_sub_committee_member s 
		INNER JOIN tbl_language l ON l.language_id = s.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
		INNER JOIN tbl_sub_committee a ON a.sub_committee_id = s.sub_committee_id AND a.is_delete = 0
		INNER JOIN tbl_committee_member m ON m.committee_member_id = s.committee_member_id AND m.is_delete = 0
		WHERE s.language_id = p_language_id
		AND s.committee_id = p_committee_id 
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT sub_committee_member_id, sub_committee_id, sub_committee_name, committee_member_id, member_name, designation, committee_id, committee_name, is_edit, is_delete FROM temp_all ORDER BY sub_committee_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_member_gid`(p_sub_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT 	s.sub_committee_member_id, 
					s.sub_committee_id, 
					s.committee_member_id, 
					s.committee_id, 
					s.language_id, 
					s.is_delete, 
					s.user_id
	FROM tbl_sub_committee_member s 
	WHERE s.sub_committee_member_id = p_sub_committee_member_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_member_i`(p_sub_committee_member_id int, p_sub_committee_id smallint, p_committee_member_id int, p_committee_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_sub_committee_member m WHERE m.sub_committee_member_id = p_sub_committee_member_id) THEN
		SET p_sub_committee_member_id = (SELECT IFNULL(MAX(sub_committee_member_id),0) + 1 FROM tbl_sub_committee_member);
		INSERT INTO tbl_sub_committee_member(sub_committee_member_id, sub_committee_id, committee_member_id, committee_id, language_id, is_delete, user_id)
		VALUES(p_sub_committee_member_id, p_sub_committee_id, p_committee_member_id, p_committee_id, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_sub_committee_member
		SET sub_committee_id = p_sub_committee_id, 
				committee_member_id = p_committee_member_id,
				committee_id = p_committee_id, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE sub_committee_member_id = p_sub_committee_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_d`(p_transcript_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_transcript
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE transcript_id = p_transcript_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_gall`(p_metting_notice_id int, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	c.metting_notice_id, 
						c.transcript_id, 
						c.file_title, 
						c.file_type, 
						c.file_path, 
						c.user_id, 
						d.committee_name,
						CASE
							WHEN LOWER(RIGHT(c.file_path,3)) = 'mp3' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'mp4' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'flv' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'm4v' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'ash' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'ebm' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'ogg' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'oga' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = '3u8' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'jpg' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'peg' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							WHEN LOWER(RIGHT(c.file_path,3)) = 'gif' THEN CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") 
							ELSE CONCAT("<input type='button' onClick='viewid(",c.transcript_id,");' name='basic' value='View' class='gridbutton'/>") 
						END is_edit,
						CONCAT("<input type='button'  name='delete' onClick='deleteid(",c.transcript_id,",",c.metting_notice_id,");'  value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_transcript c 
		INNER JOIN tbl_meeting_notice_master m On m.metting_notice_id = c.metting_notice_id AND m.is_delete = 0
		INNER JOIN tbl_committee d ON d.committee_id = m.committee_id AND d.is_delete = 0
		WHERE c.metting_notice_id = p_metting_notice_id
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT metting_notice_id, transcript_id, file_title, file_type, file_path, is_edit, is_delete FROM temp_all ORDER BY transcript_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_gid`(p_transcript_id bigint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.metting_notice_id, c.transcript_id, c.file_title, c.file_type, c.file_path, c.user_id
		FROM tbl_transcript c 
		WHERE c.transcript_id = p_transcript_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_i`(p_transcript_id bigint, p_metting_notice_id int, p_file_title varchar(200), p_file_type varchar (20), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_path varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = 'multimedia/';
	IF NOT EXISTS(SELECT * FROM tbl_transcript c WHERE c.transcript_id = p_transcript_id) THEN

		SET p_transcript_id = (SELECT IFNULL(MAX(transcript_id), 0) + 1 FROM tbl_transcript);

		SET v_file_path = CONCAT(v_path, p_transcript_id, '.', p_extension);

		INSERT INTO tbl_transcript(metting_notice_id, transcript_id, file_title, file_type, file_path, is_delete, user_id)
		VALUES(p_metting_notice_id, p_transcript_id, p_file_title, p_file_type, v_file_path, 0, p_user_id);
	ELSE
		IF(TRIM(p_file_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_path = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

			UPDATE tbl_transcript
			SET metting_notice_id = p_metting_notice_id, 
					file_title = p_file_title, 
					file_path = v_file_path,
					file_type = p_file_type, 			
					user_id = p_user_id
			WHERE transcript_id = p_transcript_id;  
		ELSE      
			SET v_file_path = (SELECT file_path FROM tbl_transcript WHERE transcript_id = p_transcript_id);
			UPDATE tbl_transcript
			SET metting_notice_id = p_metting_notice_id, 
					file_title = p_file_title, 							
					user_id = p_user_id
			WHERE transcript_id = p_transcript_id;
		END IF;
	END IF;

	SELECT p_transcript_id transcript_id, v_file_path file_path;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_activation`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_activation`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_activation`(p_user_id int, p_is_active tinyint)
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET is_active = p_is_active,
			last_login = NOW()
	WHERE user_id = p_user_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_change_pwd`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_change_pwd`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_change_pwd`(p_user_id int, p_password varchar(128))
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET password = p_password,
			last_login = NOW()
	WHERE user_id = p_user_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_d`(p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_user_committee_permission
	WHERE committee_id = p_committee_id 
	AND user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_gall`(p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.committee_id, p.committee_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.committee_id, ",", p_user_id,",0);' value='Grant' name='basic' class='gridbutton'/>") is_edit
		FROM tbl_committee p 
		WHERE p.committee_id NOT IN (SELECT g.committee_id FROM tbl_user_committee_permission g WHERE g.user_id = p_user_id)		
		AND p.language_id = p_language_id
	);

	INSERT INTO temp_all
	SELECT p.committee_id, p.committee_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.committee_id, ",", p_user_id,",1);' value='Revoke' name='basic' class='gridbuttonr' checked />") is_edit
	FROM tbl_committee p 
	WHERE p.committee_id IN (SELECT g.committee_id FROM tbl_user_committee_permission g WHERE g.user_id = p_user_id)
	AND p.language_id = p_language_id;

	SET @sql = CONCAT("SELECT committee_id, committee_name, is_edit FROM temp_all ORDER BY committee_name ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_gid`(p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...	
	SELECT p.committee_id, p.committee_name
	FROM tbl_user_committee_permission g 
	INNER JOIN tbl_committee p ON p.committee_id = g.committee_id 
	WHERE g.user_id = p_user_id
	AND p.language_id = p_language_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_i`(p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user_committee_permission c WHERE c.committee_id = p_committee_id AND c.user_id = p_user_id) THEN		
			INSERT INTO tbl_user_committee_permission(committee_id, user_id)
			VALUES(p_committee_id, p_user_id);			
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_gall`(p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.user_id, p.user_name, p.password, DATE_FORMAT(p.last_login,'%d-%m-%Y') last_login,
			CASE 
				WHEN p.is_active = 1 THEN CONCAT("<input type='button' onClick='activeid(",p.user_id,");' name='basic' value='Inactive' class='gridbutton'/>") 
				WHEN p.is_active = 0 THEN CONCAT("<input type='button' onClick='activeid(",p.user_id,");' name='basic' value='Active' class='gridbutton'/>") 
			END is_active,
					CONCAT("<input type='button' onClick='popupchangeid(",p.user_id,");' name='basic' value='Change Password' class='gridbutton' style='width: 130px;'/>") is_edit,
					CONCAT("<input type='button' onClick='popgroupid(",p.user_id,");' name='basic' value='Group Permission' class='gridbutton' style='width: 130px;'/>") is_delete,
					CONCAT("<input type='button' onClick='popcommitteeid(",p.user_id,");' name='basic' value='Committee Permission' class='gridbutton' style='width: 150px;'/>") is_committee
		FROM tbl_user p 	
	);
	SET @sql = CONCAT("SELECT user_id, user_name, last_login, is_active, is_edit, is_delete, is_committee FROM temp_all ORDER BY user_name ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gbyname`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gbyname`;
DELIMITER ;;
CREATE DEFINER=`sa`@`` PROCEDURE `tbl_user_gbyname`(p_user_name varchar(128), p_password varchar(128))
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET last_login = NOW()
	WHERE user_name = p_user_name;

	SELECT u.user_id, u.user_name, u.password, u.last_login, u.is_parliament_member
	FROM tbl_user u
	WHERE u.user_name = p_user_name
	AND u.password = p_password
	AND u.is_active = 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_gid`(p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.user_id, p.user_name, p.password, p.last_login, p.is_active, p.is_parliament_member 
	FROM tbl_user p
	WHERE p.user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_group_permission_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_group_permission_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_group_permission_d`(p_group_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_user_group_permission
	WHERE group_id = p_group_id 
	AND user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_group_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_group_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_group_permission_i`(p_group_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user_group_permission c WHERE c.group_id = p_group_id AND c.user_id = p_user_id) THEN		
			INSERT INTO tbl_user_group_permission(group_id, user_id)
			VALUES(p_group_id, p_user_id);			
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_i`(p_user_id int, p_user_name varchar(128), p_password varchar(128), p_is_parliament_member smallint)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user c WHERE c.user_id = p_user_id) THEN

		IF NOT EXISTS(SELECT * FROM tbl_user c WHERE c.user_name = p_user_name) THEN 
			SET p_user_id = (SELECT IFNULL(MAX(user_id), 0) + 1 FROM tbl_user);

			INSERT INTO tbl_user(user_id, user_name, password, last_login, is_active, is_parliament_member)
			VALUES(p_user_id, p_user_name, p_password, NOW(), 1, p_is_parliament_member);
			SELECT * FROM tbl_user WHERE user_id = p_user_id;
		END IF;	
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.venue_id, p.venue_name
	FROM tbl_venue p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_d`(p_venue_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_venue
	SET is_delete = 1
	WHERE venue_id = p_venue_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.venue_id, p.venue_name, p.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.venue_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.venue_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_venue p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT venue_id, venue_name, language_id, language, is_edit,is_delete FROM temp_all ORDER BY venue_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_gid`(p_venue_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.venue_id, p.venue_name, p.language_id, l.language
	FROM tbl_venue p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.venue_id = p_venue_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_i`(p_venue_id smallint, p_venue_name varchar(200), p_language_id smallint, p_user_id int)
BEGIN
#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_venue c WHERE c.venue_id = p_venue_id) THEN

		SET p_venue_id = (SELECT IFNULL(MAX(venue_id), 0) + 1 FROM tbl_venue);

		INSERT INTO tbl_venue(venue_id, venue_name, language_id, is_delete, user_id)
		VALUES(p_venue_id, p_venue_name, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_venue
		SET venue_name = p_venue_name, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE venue_id = p_venue_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_year_info_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_year_info_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_year_info_gall`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT y.year_id, y.year_name FROM tbl_year_info y ORDER BY y.year_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tibl_briefing_note_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tibl_briefing_note_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tibl_briefing_note_i`(p_metting_notice_id int , p_introduction longtext , p_key_issue longtext , p_advisor_question longtext , p_witness_question longtext , p_written_evidence longtext, p_remarks longtext, p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_briefing_note c WHERE c.metting_notice_id = p_metting_notice_id) THEN		

		INSERT INTO tbl_briefing_note(metting_notice_id, introduction, key_issue, advisor_question, witness_question, written_evidence, remarks, language_id, committee_id, is_issued, is_delete, user_id)
		VALUES(p_metting_notice_id, p_introduction, p_key_issue, p_advisor_question, p_witness_question, p_written_evidence, p_remarks, p_language_id, p_committee_id, 0, 0, p_user_id);
	ELSE
		UPDATE tbl_briefing_note
		SET introduction = p_introduction, 
				key_issue = p_key_issue, 
				advisor_question = p_advisor_question, 
				witness_question = p_witness_question, 
				written_evidence = p_written_evidence, 
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tibl_inquiry_report_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tibl_inquiry_report_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tibl_inquiry_report_master_i`(p_inquiry_report_id int, p_report_no varchar(30), p_report_date varchar(10), p_inquiry_id int, p_title longtext, p_executive_summary longtext, p_introduction longtext, p_issues longtext,  p_remarks longtext, p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	DECLARE p_parliament_id smallint;
	DECLARE p_session_id smallint;

	SET p_parliament_id = (SELECT m.parliament_id FROM tbl_inquiry_master m WHERE m.inquiry_id = p_inquiry_id);
	SET p_session_id = (SELECT m.session_id FROM tbl_inquiry_master m WHERE m.inquiry_id = p_inquiry_id);
	
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_report_master c WHERE c.inquiry_report_id = p_inquiry_report_id) THEN

		SET p_inquiry_report_id = (SELECT IFNULL(MAX(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master WHERE committee_id = p_committee_id);
		SET p_report_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));
		IF(p_language_id = 2) THEN
			SET p_report_no = english_bangla_number(p_report_no);
		END IF;

		INSERT INTO tbl_inquiry_report_master(inquiry_report_id, report_no, report_date, inquiry_id, title, parliament_id, session_id, executive_summary, introduction, issues, remarks, language_id, committee_id, is_approved, is_delete, user_id)
		VALUES(p_inquiry_report_id, p_report_no, STR_TO_DATE(p_report_date,'%d-%m-%Y'), p_inquiry_id, p_title, p_parliament_id, p_session_id, p_executive_summary, p_introduction, p_issues, p_remarks, p_language_id, p_committee_id, 0, 0, p_user_id);

	ELSE
		UPDATE tbl_inquiry_report_master
		SET report_no = p_report_no, 
				report_date = STR_TO_DATE(p_report_date,'%d-%m-%Y'), 
				inquiry_id = p_inquiry_id, 
				title = p_title, 
				parliament_id = p_parliament_id, 
				session_id = p_session_id, 
				executive_summary = p_executive_summary, 
				introduction = p_introduction, 
				issues = p_issues, 
				remarks = p_remarks, 
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = user_id				
		WHERE inquiry_report_id = p_inquiry_report_id;

		DELETE FROM tbl_inquiry_report_recommendation
		WHERE inquiry_report_id = p_inquiry_report_id;
	END IF;
	SELECT p_inquiry_report_id inquiry_report_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tibl_meeting_notice_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tibl_meeting_notice_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tibl_meeting_notice_master_i`(p_metting_notice_id int, p_gov_no_pix varchar(50), p_gov_no_postfix varchar(10) , p_sitting_no varchar(50), p_en_date varchar(10), p_bn_date varchar(250) , p_time varchar(50), p_venue_id smallint , p_sub_committee_id smallint, p_inquiries varchar(250), p_private_business_before longtext, p_public_business longtext, p_private_business_after longtext, p_remarks longtext, p_committee varchar(250), p_witness varchar(250), p_logistics varchar(250), p_notification varchar(250), p_adviser varchar(250), p_officer varchar(250), p_invitees varchar(250),  p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	DECLARE v_sl_no INT;		

	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_master p WHERE p.metting_notice_id = p_metting_notice_id) THEN		

		SET p_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);

		IF(p_sub_committee_id = 0 OR p_sub_committee_id = '') THEN
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id AND is_display = 1);		
		ELSE
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id AND sub_committee_id = p_sub_committee_id AND is_display = 1);		
		END IF;

		#SET p_sitting_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		SET p_sitting_no = CONCAT('',v_sl_no,'');

		IF(p_language_id = 2) THEN
			SET p_sitting_no = english_bangla_number(p_sitting_no);
		END IF;

		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, remarks, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, is_display, is_complete, is_delete, user_id)
		VALUES(p_metting_notice_id, p_gov_no_pix, p_gov_no_postfix, p_sitting_no, STR_TO_DATE(p_en_date,'%d-%m-%Y'), p_bn_date, p_time, p_venue_id, p_sub_committee_id, p_private_business_before, p_public_business, p_private_business_after, p_remarks, p_language_id, p_committee_id, 'NEW', 0, 0, 0, 0, 0, 1, 0, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_master
		SET gov_no_pix = p_gov_no_pix, 
				gov_no_postfix = p_gov_no_postfix, 
				sitting_no = p_sitting_no, 
				en_date = STR_TO_DATE(p_en_date,'%d-%m-%Y'), 
				bn_date = p_bn_date, 
				time = p_time, 
				venue_id = p_venue_id, 
				sub_committee_id = p_sub_committee_id, 
				private_business_before = p_private_business_before, 
				public_business = p_public_business, 
				private_business_after = p_private_business_after, 
				language_id = p_language_id, 
				committee_id = p_committee_id,  
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;


	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_inquiries != '' THEN		
		SET occurance = LENGTH(p_inquiries)-LENGTH(replace(p_inquiries,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_inquiries, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_inquiry_tag_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Inquires Tag Information


	#committee Information

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness )-LENGTH(replace(p_witness ,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness , ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_meeting_notice_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#p_logistics Information

	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistics != '' THEN		
		SET occurance = LENGTH(p_logistics)-LENGTH(replace(p_logistics,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistics, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information


	#p_adviser Information

	DELETE FROM tbl_meeting_notice_adviser WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_adviser != '' THEN		
		SET occurance = LENGTH(p_adviser)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_adviser_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_adviser Information


	#p_officer Information

	DELETE FROM tbl_meeting_notice_officer WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_officer != '' THEN		
		SET occurance = LENGTH(p_officer)-LENGTH(replace(p_officer,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_officer, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_officer_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_officer Information

	#SELECT p_metting_notice_id metting_notice_id;











	#Record of decision Part


	#tbl_record_of_decision_master 

	IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_issued, is_delete, user_id)
		VALUES(p_metting_notice_id, 0, '', '', '', p_language_id, p_committee_id , 0, 0, 0, p_user_id);

	END IF;
		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = p_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_adviser WHERE meeting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_adviser(meeting_notice_id, adviser_id, is_delete, user_id)
		SELECT meeting_notice_id, adviser_id, is_delete, user_id FROM tbl_meeting_notice_adviser WHERE meeting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_officer WHERE meeting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_officer(meeting_notice_id, officer_id, is_delete, user_id)
		SELECT meeting_notice_id, officer_id, is_delete, user_id FROM tbl_meeting_notice_officer WHERE meeting_notice_id = p_metting_notice_id;		

		#Record of decision Part
	#END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tibl_record_of_decision_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tibl_record_of_decision_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tibl_record_of_decision_master_i`(p_metting_notice_id int, p_chair_member_id smallint, p_committee varchar(100), p_witness varchar(100),  p_adviser varchar(250), p_officer varchar(250),  p_logistic varchar(100), p_notification varchar(100), p_invitees varchar(100), p_private_business longtext , p_public_business longtext, p_result_of_deliberation longtext, p_remarks longtext, p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	#IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_master c WHERE c.metting_notice_id = p_metting_notice_id) THEN		
	#
	#	INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id , is_delete, user_id)
	#	VALUES(p_metting_notice_id, p_chair_member_id, p_private_business, p_public_business, p_result_of_deliberation, p_language_id, p_committee_id , 0, p_user_id);
	#ELSE
		UPDATE tbl_record_of_decision_master
		SET chair_member_id = p_chair_member_id, 
				private_business = p_private_business, 
				public_business = p_public_business, 
				result_of_deliberation = p_result_of_deliberation, 
				remarks = p_remarks,
				language_id = p_language_id, 
				committee_id = p_committee_id, 
				flag = 1,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	#END IF;

	#committee Information

	DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness)-LENGTH(replace(p_witness,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness, ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_record_of_decision_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#p_logistics Information

	DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistic != '' THEN		
		SET occurance = LENGTH(p_logistic)-LENGTH(replace(p_logistic,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistic, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information

	#p_adviser Information

	DELETE FROM tbl_record_of_decision_adviser WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_adviser != '' THEN		
		SET occurance = LENGTH(p_adviser)-LENGTH(replace(p_adviser,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_adviser, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_adviser_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_adviser Information


	#p_officer Information

	DELETE FROM tbl_record_of_decision_officer WHERE meeting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_officer != '' THEN		
		SET occurance = LENGTH(p_officer)-LENGTH(replace(p_officer,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_officer, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_officer_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_officer Information


END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `bangla_english_number`
-- ----------------------------
DROP FUNCTION IF EXISTS `bangla_english_number`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `bangla_english_number`(p_bangla varchar(150)) RETURNS varchar(150) CHARSET utf8
BEGIN
	#Routine body goes here...
	declare ret_value varchar(150);
	declare len int;
	declare pos int;
  declare str varchar(6);
  set len= length(p_bangla);
  set pos=1;
	set ret_value='';
	while  len > 0 do 
     set  str = substr(p_bangla,pos,3);
		 case str
				when 'à§¦' THEN set ret_value= CONCAT(ret_value,'0');
				when 'à§§' THEN set ret_value= CONCAT(ret_value,'1');
				when 'à§¨' THEN set ret_value= CONCAT(ret_value,'2');

				when 'à§©' THEN set ret_value= CONCAT(ret_value,'3');
				when 'à§ª' THEN set ret_value= CONCAT(ret_value,'4');
				when 'à§«' THEN set ret_value= CONCAT(ret_value,'5');

				when 'à§¬' THEN set ret_value= CONCAT(ret_value,'6');
				when 'à§­' THEN set ret_value= CONCAT(ret_value,'7');
				when 'à§®' THEN set ret_value= CONCAT(ret_value,'8');
				when 'à§¯' THEN set ret_value= CONCAT(ret_value,'9');

     end case;
     #set ret_value= CONCAT(ret_value,str);
		 set pos=pos + 3;
		 set len=len - 6;
  end while;
	RETURN ret_value;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `english_bangla_number`
-- ----------------------------
DROP FUNCTION IF EXISTS `english_bangla_number`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `english_bangla_number`(p_english varchar(150)) RETURNS varchar(150) CHARSET utf8
BEGIN
	#Routine body goes here...
	declare ret_value varchar(150);
	declare len int;
	declare pos int;
  declare str varchar(6);
  set len= length(p_english);
  set pos=1;
	set ret_value='';
	while  len > 0 do 
     set  str = substr(p_english,pos,1);
		 case str
				when '0' THEN set ret_value= CONCAT(ret_value,'à§¦');
				when '1' THEN set ret_value= CONCAT(ret_value,'à§§');
				when '2' THEN set ret_value= CONCAT(ret_value,'à§¨');

				when '3' THEN set ret_value= CONCAT(ret_value,'à§©');
				when '4' THEN set ret_value= CONCAT(ret_value,'à§ª');
				when '5' THEN set ret_value= CONCAT(ret_value,'à§«');

				when '6' THEN set ret_value= CONCAT(ret_value,'à§¬');
				when '7' THEN set ret_value= CONCAT(ret_value,'à§­');
				when '8' THEN set ret_value= CONCAT(ret_value,'à§®');
				when '9' THEN set ret_value= CONCAT(ret_value,'à§¯');

     end case;
     #set ret_value= CONCAT(ret_value,str);
		 set pos=pos + 1;
		 set len=len - 1;
  end while;
	RETURN ret_value;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `fnStripTags`
-- ----------------------------
DROP FUNCTION IF EXISTS `fnStripTags`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `fnStripTags`( Dirty varchar(4000) ) RETURNS varchar(4000) CHARSET utf8
    DETERMINISTIC
BEGIN
  DECLARE iStart, iEnd, iLength int;
    WHILE Locate( '<', Dirty ) > 0 And Locate( '>', Dirty, Locate( '<', Dirty )) > 0 DO
      BEGIN
        SET iStart = Locate( '<', Dirty ), iEnd = Locate( '>', Dirty, Locate('<', Dirty ));
        SET iLength = ( iEnd - iStart) + 1;
        IF iLength > 0 THEN
          BEGIN
            SET Dirty = Insert( Dirty, iStart, iLength, '');
          END;
        END IF;
      END;
    END WHILE;
    RETURN Dirty;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `zzz`
-- ----------------------------
DROP FUNCTION IF EXISTS `zzz`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `zzz`() RETURNS int(11)
BEGIN
	#Routine body goes here...

	RETURN 0;
END
;;
DELIMITER ;
