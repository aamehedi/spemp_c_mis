<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("others_organization_ministry_popup.php?others_org_ministry_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:850px;dialogHeight:480px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
        else {

        }
    }
</script>

<?php
require_once('../model/others_info.php');
?>
<?php
$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}

?>


<div id="head_info">
    <?php if ($_SESSION['language_id'] === '1'): ?>
        Others Organization Ministry Information
    <?php else: ?>
        অন্য সংস্থা মন্ত্রণালয় তথ্য
    <?php endif; ?>

</div>
<div>
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
</div>

<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $user_name;
    $committe_id;
    $committe_name;
    $param = array($id, $parliament);
    echo $others_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>