<?php
session_start();
?>
<?php
require_once('../model/request_evidence_info.php');
require_once('../model/inquiry_scheduling_info.php');
require_once('../model/create_inquiry_info.php');
?>
<?php
$user_id = $_SESSION['user_id'];
if ($_GET['inquiry_id'] != NULL) {
    $result = $create_inquiry_info->editrow(array($_GET['inquiry_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="styles/jquery-ui-1.10.4.custom.min.css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }

        .validationRequired {
            display: inline !important;
            visibility: hidden;
            position: absolute;
            top: 0px;
            left: 0px;
            width: 85%;
        }
    </style>
    <!--ajax Combo-->
    <script>
        function show(str) {
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "session_combo.php?parliament_id=" + str, true);
            xmlhttp.send();
        }
    </script>
    <!--ajax Combo-->
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
            jQuery("#create_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0,
                beforeShowDay: function (date) {
                    var weekend = date.getDay() == 5 || date.getDay() == 6;
                    return [true, weekend ? 'myweekend' : ''];
                }

            });
        });
    </script>
    <!--Validation End-->
</head>
<body style="margin:10px 10px 10px 20px;">
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<input type="hidden" id="num" value="<?php echo $create_inquiry_info->getcount($_GET['inquiry_id'], $user_id); ?>"/>
<script>
    id = document.getElementById('num').value;
    $(document).ready(function () {
        $('#demo-input-facebook-theme5').addClass('validationRequired');
        $("#btn2").click(function () {
            id++;
            $("#btn1").append("<div  style='float:left;padding-left:2px;height: 100px;'><input type='hidden' name='schedule_no[]' value='0'/><input type='text' class='schedule_date' name='schedule_date[]' id='" + id + "'/></div>");
            $("#" + id).datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend, beforeShow: applyProposedDateValidation
            });
        });
    });
    function calculateWeekend(date) {
        var weekend = date.getDay() == 5 || date.getDay() == 6;
        return [true, weekend ? 'myweekend' : ''];
    }
    ;
    function applyProposedDateValidation(input, inst) {
        if ($('#proposed_month').val() == "" || $('#proposed_year').val() == "") {
            alert("Plsease select proposed month and year");
            return {beforeShowDay: function (date) {
                return [false, ''];
            }};
        } else {
            var minDate = new Date(Date.parse($('#proposed_month').val() + ' 1, ' + $('#proposed_year').val()));
            return {minDate: minDate, beforeShowDay: calculateWeekend};
        }
    }
    ;
</script>
<?php
$xml = simplexml_load_file("xml/create_inquiry_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == '1') {
        $english_heading = $information->heading;
        $english_headingpopup = $information->headingpopup;
        $english_doc_reg_no = $information->doc_reg_no;
        $english_id = $information->id;
        $english_doc_reg_date = $information->doc_reg_date;
        $english_doc_title = $information->doc_title;
        $english_designation = $information->designation;
        $english_doc_name = $information->doc_name;
        $english_to = $information->to;
        $english_year = $information->year;
        $english_month = $information->month;
        $english_date = $information->date;
        $english_inquiry_title = $information->inquiry_title;
        $english_inquiry_no = $information->inquiry_no;
        $english_create_date = $information->create_date;
        $english_proposed_date = $information->proposed_date;
        $english_ministry = $information->ministry;
        $english_organization = $information->organization;
        $english_others = $information->others;
        $english_witnesses = $information->witnesses;
        $english_other_witnessess = $information->other_witnessess;
        $english_remarks = $information->remarks;
        $english_specialist_adviser = $information->specialist_adviser;
    } else {
        $bangla_heading = $information->heading;
        $bangla_headingpopup = $information->headingpopup;
        $bangla_doc_reg_no = $information->doc_reg_no;
        $bangla_id = $information->id;
        $bangla_doc_reg_date = $information->doc_reg_date;
        $bangla_doc_title = $information->doc_title;
        $bangla_designation = $information->designation;
        $bangla_doc_name = $information->doc_name;
        $bangla_to = $information->to;
        $bangla_year = $information->year;
        $bangla_month = $information->month;
        $bangla_date = $information->date;
        $bangla_inquiry_title = $information->inquiry_title;
        $bangla_inquiry_no = $information->inquiry_no;
        $bangla_create_date = $information->create_date;
        $bangla_proposed_date = $information->proposed_date;
        $bangla_ministry = $information->ministry;
        $bangla_organization = $information->organization;
        $bangla_others = $information->others;
        $bangla_witnesses = $information->witnesses;
        $bangla_other_witnessess = $information->other_witnessess;
        $bangla_remarks = $information->remarks;
        $bangla_specialist_adviser = $information->specialist_adviser;
    }
}
?>
<form id="formID" class="formular" name="overlay_form" action="#" method="post">
<h2><?php echo $english_headingpopup . ' / ' . $bangla_headingpopup; ?></h2>
</br>
<div style="width:1380px;">
<!--Starting Enlish Part of the Form-->
<div style="margin-bottom:5px;">
<input type="hidden" name="inquery_id" value="<?php echo isset($result->inquiry_id) ? $result->inquiry_id : ''; ?>">

<div style="width:15%;float:left;">
    <?php echo $english_inquiry_no; ?>
</div>
<div style="width:15%;float:left;">
    <input type="text" readonly="true" class='validate[required] text-input bangla' name="inquery_no"
           value="<?php echo isset($result->inquiry_no) ? $result->inquiry_no : '[Auto]'; ?>"/>
</div>
<div style="width:8%;float:left;">
    <?php echo $english_create_date; ?> *
</div>
<div style="width:8%;float:left;">
    <input style='width:70%' class='validate[required] text-input bangla' id="create_date" type="text"
           name="create_date"
           value="<?php echo isset($result->create_date) ? $result->create_date : date('d-m-Y'); ?>"/>
</div>
<div>
    <div style='float:left;width:10%;'><?php echo $english_proposed_date; ?> *</div>
    <div style='float:left;width:15%;'>
        <div style="float:left;">
            <?php
            $english_id = isset($result->proposed_month) ? $result->proposed_month : '';
            //echo $english_id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select id="proposed_month"  name="proposed_month" class="validate[required] text-input bangla"/>';
            echo $create_inquiry_info->comboview_month($language_id, $user_id, $english_id);
            echo '</select>';
            ?>
        </div>
        <div style="float:left;margin-left:10px;">
            <?php
            $english_id = isset($result->proposed_year) ? $result->proposed_year : '';
            //echo $result->section_id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select id="proposed_year"  name="proposed_year" class="validate[required] text-input bangla"/>';
            echo $create_inquiry_info->comboview_year($language_id, $user_id, $english_id);
            echo '</select>';
            ?>
        </div>
    </div>

    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;"></div>
<div>
    <div style="float:left;width:15%;">
        <?php echo 'Parliament'; ?> *
    </div>
    <div style="float:left;width:15%;">
        <?php
        $english_id = isset($result->parliament_id) ? $result->parliament_id : '';
        //echo $result->parliament_id;
        //$english_id=1;
        $language_id = $_SESSION['language_id'];
        $user_id = $_SESSION['user_id'];
        echo '<select  name="parliament_id" onchange="show(this.value)" class="validate[required] text-input bangla" />';
        echo $request_evidence_info->comboview_parliament($language_id, $user_id, $english_id);
        echo '</select>';
        ?>
    </div>
    <div style="float:left;width:8%;">
        <?php echo 'Session'; ?> *
    </div>
    <div style="float:left;width:15%;" id="txtHint">
        <?php
        if ($_GET['inquiry_id'] != 0) {
            $english_id = isset($result->session_id) ? $result->session_id : '';
            //echo $result->section_id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select  name="session_id" class="validate[required] text-input bangla" />';
            echo $request_evidence_info->comboview_session($language_id, $user_id, $english_id);
            echo '</select>';
        } else {
            echo '<select name="session_id" class="validate[required] text-input bangla">';
            echo '<option value="">Select</option>';
            echo '</select>';
        }
        ?>

    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;"></div>
<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_doc_reg_no; ?>
    </div>
    <div style="float:left;width:75%;position: relative;">
        <input type="text" class="text-input bangla" id="demo-input-facebook-theme5" name="doc_reg_no"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_document($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_document($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>
<div>

<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;margin-top: 29px;">
        <?php echo $english_inquiry_title; ?> *
    </div>
    <div style="float:left;width:75%;">
        <textarea style="font-size: 11px;width:100%; height:50px;" class='validate[required] text-input bangla'
                  id="area1"
                  name="inquery_title"><?php echo isset($result->inquiry_title) ? $result->inquiry_title : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_remarks; ?>
    </div>
    <div style="float:left;width:75%;">
        <textarea style="font-size: 11px;width:100%; height:50px;" class='text-input bangla'
                  name="remarks"><?php echo isset($result->remarks) ? $result->remarks : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input type="hidden" value="<?php echo $_SESSION['committee_id']; ?>" name="committee_id">
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
</div>

<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_ministry; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme1" name="ministry"/>

        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_ministry($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_ministry($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<button onclick="ministry_token()">+</button>-->

        <script>
            function ministry_token() {
                alert("assaasasas");
            }
        </script>
    </div>

    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_organization; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme2" name="organization"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_organization($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_organization($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }


        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>

<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $english_others; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme9" name="others"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_others($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_others($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }
        ?>

    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('others_popup.php','1376201640569','width=400,height=150,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=700,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_witnesses; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme3" name="witness"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_witness($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_witness($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_other_witnessess; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme6" name="other_witness"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) // if new inquiry
        {
            if ($create_inquiry_info->gridview_token_pree_popolated_others_witness($inquiry_id) != ']') // if details data found
            {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_others_witness($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else // if details data not found
            {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else // if existing inquiry
        {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('others_witness_popup.php?witness_id=0','1376201640569','width=620,height=420,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=500,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<!--SP Adviser-->

<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $english_specialist_adviser; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme11" name="adviser"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_adviser($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme11").tokenInput("adviser_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_adviser($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme11").tokenInput("adviser_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme11").tokenInput("adviser_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }


        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>


<div>
    <div style="float:left;width:15%;"><label><?php echo 'Schedule Date'; ?></label></div>

    <div style="float:left;width:25%;">
        <input type="button" name="basic" id="btn2" value="+"/>
    </div>
    <div class="divclear"></div>
</div>
<div style="overflow-y:scroll; height:200px; width:75%;" id="btn1">

    <?php
    if ($_GET['inquiry_id'] != NULL) {
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        echo $inquiry_scheduling_info->gridviewdetails($inquiry_id, $user_id);
    }
    ?>
</div>

<div>
    <div style="float:left;width:25%;"><label><?php echo 'Actual Date'; ?></label></div>
    <div class="divclear"></div>
</div>
<div>
    <div style="float:left;width:20%;">
        &nbsp;&nbsp;
    </div>

</div>
</div>
</div>
<!--Ending English Part of the Form-->

</div>
<!--    Submision Start      -->
<div style="float:left;width:50%;">
    <script>
        function myFunction() {
            window.close();
        }
    </script>

    &nbsp;&nbsp;&nbsp;&nbsp;
    <button onClick="myFunction()" class="popupbutton">Close</button>
    &nbsp;&nbsp;<input id="send" name="btn_save_inqueiry" type="submit" value="Save" class="popupbutton"/>
</div>
<div class="divclear"></div>
<!--     Submission Ends     -->
</form>
</body>
</html>
