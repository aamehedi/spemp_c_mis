<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/inquiry_report_info.php');
require_once('../model/meeting_notice_info.php');
require_once('../model/record_decisions_info.php');
require_once('../biz/receive_evidance_biz.php');

$introductions = '';
$issues = '';
$resultRecommendations = '';
if ($_GET['inquiry_report_id'] != NULL) {
    $resultAll = $inquiry_report_info->rpt_inquiry_report_getall(array($_GET['inquiry_report_id']));
    $result = mysqli_fetch_object($resultAll);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<!--
<html xmlns="http://www.w3.org/1999/xhtml">
-->
<head>
    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Inquiry Report</title>
    <?php else: ?>
        <title>তদন্ত রিপোর্ট</title>
    <?php endif; ?>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
    <style>
        @page {
            size: 8.5in 11in;
            margin: 2cm
        }

        @media p {
            page-break-before:always
        }
    </style>
</head>

<body>
<?php if ($result->is_approved === '1') : ?>
<div style="margin:10px; width:100%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:100%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat;backgroun-size: 200px 180px"
        id="message_area">
        <?php endif; ?>

        <!-- Cover Letter Start -->
        <div style="min-height: 1000px;text-align: center;">
            <div style="height:80px;">
                <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            </div>
            <?php if (isset($language_id) && $language_id === '1'): ?>
                <p style="font-size: 34pt;font-weight: bold;text-align: center;"><?php echo htmlspecialchars_decode($result->committee_name) . ' of the ' . htmlspecialchars_decode($result->parliament) . ' of Bangladesh - ' . htmlspecialchars_decode($result->title); ?></p>
                <p style="font-size: 22pt;text-align: center;"><?php echo 'Inquiry Report No.' . htmlspecialchars_decode($result->report_no); ?></p>
                <?php if (!empty($result->report_date)) : ?>
                    <p style="font-size: 22pt;text-align: center;"><?php echo 'Date ' . htmlspecialchars_decode($result->report_date); ?></p>
                <?php endif; ?>
            <?php else: ?>
                <p style="font-size: 34pt;font-weight: bold;text-align: center;"><?php echo 'গণপ্রজাতন্ত্রী বাংলাদেশের ' . htmlspecialchars_decode($result->parliament) . ' এর ' . htmlspecialchars_decode($result->committee_name) . ' - ' . htmlspecialchars_decode($result->title); ?></p>
                <p style="font-size: 22pt;text-align: center;"><?php echo 'তদন্ত রিপোর্ট নং.' . htmlspecialchars_decode($result->report_no); ?></p>
                <?php if (!empty($result->report_date)) : ?>
                    <p style="font-size: 22pt;text-align: center;"><?php echo 'তারিখ ' . htmlspecialchars_decode($result->report_date); ?></p>
                <?php endif; ?>
            <?php endif; ?>
        </div>


        <!-- Cover Letter End -->
        <?php if (isset($language_id) && $language_id === '1'): ?>
            <b>Executive Summary</b>
            <p><?php echo htmlspecialchars_decode($result->executive_summary); ?></p>
            <b>Recommendations</b>
            <ol type="1">
                <?php
                mysqli_data_seek($resultAll, 0);
                while ($rowRecommendation = mysqli_fetch_object($resultAll)) :
                    ?>
                    <li><?php echo htmlspecialchars_decode($rowRecommendation->recommendation); ?></li>
                <?php endwhile; ?>
            </ol>
            <b>Introduction</b>
            <p><?php echo $result->introduction; ?></p>



            <?php
            $resultMeetingNotice = $meeting_notice_info->getall_meeting_notice_by_inquiry_title($result->inquiry_title);
            $count = 0;
            while ($rowMeetingNotice = mysqli_fetch_assoc($resultMeetingNotice)) {
                echo '<div style="min-height: 1000px;">';
                if ($count === 0) {
                    echo '<b>Annex A - Record of Decisions</b>';
                }
                $resultRecordOfDecision = $record_decisions_info->rpt_record_of_decision_getone(array($rowMeetingNotice['metting_notice_id']));
                $html = '<p style="font-size: 24pt;text-align: center;font-weight: bold;">RECORD OF DECISION</p>';
                $html .= '<b>Date: </b>' . htmlspecialchars_decode($resultRecordOfDecision->en_date) . ' and ' . htmlspecialchars_decode($resultRecordOfDecision->bn_date) . '<br>';
                $html .= '<b>Time: </b>' . htmlspecialchars_decode($resultRecordOfDecision->time) . '<br>';
                $html .= '<b>Location: </b>' . htmlspecialchars_decode($resultRecordOfDecision->venue_name) . '<br>';
                $html .= '<b>Chair: </b>' . htmlspecialchars_decode($resultRecordOfDecision->chairman_name) . '<br>';
                $html .= '<b>Members Present: </b>' . htmlspecialchars_decode($resultRecordOfDecision->member_name) . '<br>';
                $html .= '<b>Officials Presents: </b>' . htmlspecialchars_decode($resultRecordOfDecision->logistic_member_name) . '<br>';
                $html .= '<b>Witnessess: </b>' . htmlspecialchars_decode($resultRecordOfDecision->witness_member_name) . '<br><br>';
                // $html .= '<b>PRIVATE BUSINESS</b><br>'.htmlspecialchars_decode($resultRecordOfDecision->private_business).'<br>';
                $html .= '<b>PUBLIC BUSINESS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->public_business) . '<br>';
                $html .= '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->result_of_deliberation) . '<br>';
                echo $html;
                if ($count === 0) {
                    $count++;
                }
                echo '</div>';
            }
            ?>

            <b>Annex B - List of Evidence Received</b>
            <?php
            $receiveEvidanceBiz = new receive_evidance_biz();
            $resultReceiveEvidance = $receiveEvidanceBiz->getAllRawDetails($result->inquiry_id);
            $oralEvidance = array();
            $writtenEvidance = array();
            while ($rowEvidance = mysqli_fetch_assoc($resultReceiveEvidance)) {
                if ($rowEvidance['evidence_type'] === 'Multimedia') {
                    array_push($oralEvidance, $rowEvidance['submitted_by']);
                } else {
                    array_push($writtenEvidance, $rowEvidance['submitted_by']);
                }
            }
            ?>
            <p style="font-size: 12pt;font-weight: bold;">Oral Evidence</p>
            <?php
            foreach ($oralEvidance as $value) {
                echo $value;
            }
            ?>
            <p style="font-size: 12pt;font-weight: bold;">Written Evidence</p>
            <?php
            foreach ($writtenEvidance as $value) {
                echo $value;
            }
            ?>
        <?php else: ?>
            <b>নির্বাহী সারসংক্ষেপ</b>
            <p><?php echo htmlspecialchars_decode($result->executive_summary); ?></p>
            <b>প্রস্তাবনা</b>
            <ol type="1">
                <?php
                mysqli_data_seek($resultAll, 0);
                while ($rowRecommendation = mysqli_fetch_object($resultAll)) :
                    ?>
                    <li><?php echo htmlspecialchars_decode($rowRecommendation->recommendation); ?></li>
                <?php endwhile; ?>
            </ol>
            <b>ভূমিকা</b>
            <p><?php echo $result->introduction; ?></p>



            <?php
            $resultMeetingNotice = $meeting_notice_info->getall_meeting_notice_by_inquiry_title($result->inquiry_title);
            $count = 0;
            while ($rowMeetingNotice = mysqli_fetch_assoc($resultMeetingNotice)) {
                echo '<div style="min-height: 1000px;">';
                if ($count === 0) {
                    echo '<b>Annex A - সিদ্ধান্তসমূহ</b>';
                }
                $resultRecordOfDecision = $record_decisions_info->rpt_record_of_decision_getone(array($rowMeetingNotice['metting_notice_id']));
                $html = '<p style="font-size: 24pt;text-align: center;font-weight: bold;">সিদ্ধান্তসমূহ</p>';
                $html .= '<b>তারিখ: </b>' . htmlspecialchars_decode($resultRecordOfDecision->en_date) . ' and ' . htmlspecialchars_decode($resultRecordOfDecision->bn_date) . '<br>';
                $html .= '<b>সময়: </b>' . htmlspecialchars_decode($resultRecordOfDecision->time) . '<br>';
                $html .= '<b>স্থান: </b>' . htmlspecialchars_decode($resultRecordOfDecision->venue_name) . '<br>';
                $html .= '<b>চেয়ার: </b>' . htmlspecialchars_decode($resultRecordOfDecision->chairman_name) . '<br>';
                $html .= '<b>উপস্থিত সদস্যবৃন্দ: </b>' . htmlspecialchars_decode($resultRecordOfDecision->member_name) . '<br>';
                $html .= '<b>পস্থিত কর্মকর্তাবৃন্দ: </b>' . htmlspecialchars_decode($resultRecordOfDecision->logistic_member_name) . '<br>';
                $html .= '<b>সাক্ষীগন: </b>' . htmlspecialchars_decode($resultRecordOfDecision->witness_member_name) . '<br><br>';
                // $html .= '<b>PRIVATE BUSINESS</b><br>'.htmlspecialchars_decode($resultRecordOfDecision->private_business).'<br>';
                $html .= '<b>সাধারণ আলোচ্য বিষয়</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->public_business) . '<br>';
                $html .= '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->result_of_deliberation) . '<br>';
                echo $html;
                if ($count === 0) {
                    $count++;
                }
                echo '</div>';
            }
            ?>

            <b>Annex B - গৃহীত প্রমাণাদির তালিকা</b>
            <?php
            $receiveEvidanceBiz = new receive_evidance_biz();
            $resultReceiveEvidance = $receiveEvidanceBiz->getAllRawDetails($result->inquiry_id);
            $oralEvidance = array();
            $writtenEvidance = array();
            while ($rowEvidance = mysqli_fetch_assoc($resultReceiveEvidance)) {
                if ($rowEvidance['evidence_type'] === 'Multimedia') {
                    array_push($oralEvidance, $rowEvidance['submitted_by']);
                } else {
                    array_push($writtenEvidance, $rowEvidance['submitted_by']);
                }
            }
            ?>
            <p style="font-size: 12pt;font-weight: bold;">Oral Evidence</p>
            <?php
            foreach ($oralEvidance as $value) {
                echo $value;
            }
            ?>
            <p style="font-size: 12pt;font-weight: bold;">Written Evidence</p>
            <?php
            foreach ($writtenEvidance as $value) {
                echo $value;
            }
            ?>
        <?php endif; ?>

        <?php if ($result->is_approved !== '1') : ?>
            <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                          width="20"
                                                                                                          src="img/printer-icon2.png"/></a>
            </div>
        <?php endif; ?>
    </div>
</body>
</html>