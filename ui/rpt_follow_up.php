<?php
session_start();
$language_id = (!empty($_SESSION['language_id'])) ? $_SESSION['language_id'] : $_GET['language_id'];
require_once('../model/follow_up_info.php');
$introductions = '';
$issues = '';
if ($_GET['inquiry_report_followup_id'] != NULL) {
    $result1 = $follow_up_info->rpt_follow_up_getall(array($_GET['inquiry_report_followup_id']));
    $result = mysqli_fetch_object($result1);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php if (isset($language_id) && $language_id === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>

<head>
    <?php if (isset($language_id) && $language_id === '1'): ?>
        <title>Follow Up Report</title>
    <?php else: ?>
        <title>তদন্ত অগ্রগতি রিপোর্ট</title>
    <?php endif; ?>


    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript'>
    </script>
    <script>
        function printpage() {
            window.print();
        }
        function PrintDiv() {
            var divToPrint = document.getElementById('message_area');
            var popupWin = window.open('', '_blank', 'width=300,height=300');
            popupWin.document.open();
            popupWin.document.write('<html><body onload="window.print();close()">' + divToPrint.innerHTML + '</html>');
            //popupWin.document.close();			
            if (popupWin.onload())
                popupWin.close();
        }
    </script>
    <style>
        table {
            border-collapse: collapse;
        }

        table, th, td {
            border: 1px solid black;
        }

    </style>
</head>

<body>
<?php if ($result->is_approved === '1') : ?>
<div style="margin:10px; width:100%;" id="message_area">
    <?php else : ?>
    <div
        style="margin:10px; width:100%;background-image: url(styles/draft_report.JPG); background-repeat: no-repeat;backgroun-size: 200px 180px"
        id="message_area">
        <?php endif; ?>

        <!-- Cover Letter Start -->
        <div style="min-height: 1000px;text-align: center;">
            <div style="height:80px;">
                <div style="float:left; height:80px; width:80px;"><img src="img/logo.png"/></div>
            </div>
            <?php if (isset($language_id) && $language_id === '1'): ?>
                <p style="font-size: 34pt;font-weight: bold;text-align: center;"><?php echo htmlspecialchars_decode($result->committee_name) . ' of the ' . htmlspecialchars_decode($result->parliament) . ' of Bangladesh - ' . htmlspecialchars_decode($result->title); ?></p>
                <p style="font-size: 22pt;text-align: center;"><?php echo 'Inquiry Report No.' . htmlspecialchars_decode($result->report_no) . '/F - Follow Up'; ?></p>
                <?php if (!empty($result->followup_date)) : ?>
                    <p style="font-size: 22pt;text-align: center;"><?php echo htmlspecialchars_decode($result->followup_date); ?></p>
                <?php endif; ?>
            <?php else: ?>
                <p style="font-size: 34pt;font-weight: bold;text-align: center;"><?php echo 'গণপ্রজাতন্ত্রী বাংলাদেশের ' . htmlspecialchars_decode($result->parliament) . ' এর ' . htmlspecialchars_decode($result->committee_name) . ' - ' . htmlspecialchars_decode($result->title); ?></p>
                <p style="font-size: 22pt;text-align: center;"><?php echo 'তদন্ত রিপোর্ট নং.' . htmlspecialchars_decode($result->report_no) . '/এফ - তদন্ত অগ্রগতি'; ?></p>
                <?php if (!empty($result->followup_date)) : ?>
                    <p style="font-size: 22pt;text-align: center;"><?php echo htmlspecialchars_decode($result->followup_date); ?></p>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>

    <!-- Cover Letter End -->


    <?php if (isset($language_id) && $language_id === '1'): ?>
        <p style="font-size: 14pt;font-weight: bold;">Background</p>
        <?php echo htmlspecialchars_decode($result->background); ?>
        <p style="font-size: 14pt;font-weight: bold;">Recommendations</p>
        <table width="100%" border="0">
            <tr style="background-color:#B7DDE7;">
                <th style="width:25%;">Committee Recommendation</th>
                <th style="width:25%;">Response of Organisation</th>
                <th style="width:25%;">Action Taken</th>
                <th style="width:25%;">Comment</th>
            </tr>
            <tr>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->recommendation); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->organization_response); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->action_taken); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->comment); ?></td>
            </tr>
            <?php while ($tableRow = $result1->fetch_object()) : ?>
                <tr>
                    <td><?php echo htmlspecialchars_decode($tableRow->recommendation); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->organization_response); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->action_taken); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->comment); ?></td>
                </tr>
            <?php endwhile; ?>
        </table>
    <?php else: ?>
        <p style="font-size: 14pt;font-weight: bold;">পটভূমিকা</p>
        <?php echo htmlspecialchars_decode($result->background); ?>
        <p style="font-size: 14pt;font-weight: bold;">প্রস্তাবনা</p>
        <table width="100%" border="0">
            <tr style="background-color:#B7DDE7;">
                <th style="width:25%;">কমিটি প্রস্তাবনা</th>
                <th style="width:25%;">প্রতিষ্ঠানের প্রতিক্রিয়া</th>
                <th style="width:25%;">গৃহীত পদক্ষেপ</th>
                <th style="width:25%;">মন্তব্য</th>
            </tr>
            <tr>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->recommendation); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->organization_response); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->action_taken); ?></td>
                <td style="width:25%;"><?php echo htmlspecialchars_decode($result->comment); ?></td>
            </tr>
            <?php while ($tableRow = $result1->fetch_object()) : ?>
                <tr>
                    <td><?php echo htmlspecialchars_decode($tableRow->recommendation); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->organization_response); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->action_taken); ?></td>
                    <td><?php echo htmlspecialchars_decode($tableRow->comment); ?></td>
                </tr>
            <?php endwhile; ?>
        </table>
    <?php endif; ?>



    <?php if ($result->is_approved !== '1') : ?>
        <div style="margin:10px; width:80%;" align="right"><br/><a href="javascript:PrintDiv();"><img height="20"
                                                                                                      width="20"
                                                                                                      src="img/printer-icon2.png"/></a>
        </div>
    <?php endif; ?>
</body>
</html>
