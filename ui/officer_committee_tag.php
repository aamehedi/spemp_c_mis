<?php
require_once('header.php');
require_once('aside_left.php');
require_once('menu.php');
?>


<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("parliament_popup.php?parliament_id=" + p, "", "dialogTop:325px;dialogLeft:400px;dialogWidth:470px;dialogHeight:190px;status: No; ");
    }

    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        if (r == true) {
            window.location.href = "parliament_delete.php?parliament_id=" + p;
        }
    }
</script>

<?php
require_once('../model/officer_committee_tag.php');

$xml = simplexml_load_file("xml/parliament.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $parliament_information = $information->parliament_information;
        $id = $information->id;
        $parliament = $information->parliament;
    }
}
?>
<div id="head_info" style="margin-top: 10px; margin-bottom: 10px;">
    <?php if ($_SESSION['language_id'] === '1') : ?>
        Committee to Look After
    <?php else : ?>
        দেখাশোনা করার জন্য কমিটি
    <?php endif; ?>
</div>
<div style="padding-bottom:10px;"><br/>
</div>
<script>
    function saveid(officer_id, committee_id, type, user_id) {
        if (officer_id == "" && committee_id == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "officer_committee_tag_data.php?officer_id=" + officer_id + "&committee_id=" + committee_id + "&type=" + type + "&user_id=" + user_id, true);
        xmlhttp.send();
    }
</script>
<div class="content" id="conteudo">
    <div id="txtHint">
        <?php
        $language_id = $_SESSION['language_id'];
        $committee_id = $_SESSION['committee_id'];
        //echo $committee_id;
        $user_id = $_SESSION['user_id'];
        $param = array($id, $parliament);
        echo $officer_committee_tag->gridview($committee_id, $language_id, $user_id, $param);
        ?>
    </div>
</div>
</div>
<?php
require_once('aside_right.php');
require_once('footer.php');
?>	 