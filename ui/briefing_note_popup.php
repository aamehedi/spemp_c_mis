<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--	<script type='text/javascript' src='js/jquery.js'></script>-->
    <!--	<script type='text/javascript' src='js/jquery.simplemodal.js'></script>-->
    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            new nicEditor().panelInstance('area4');
            new nicEditor().panelInstance('area5');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <!--validation start-->
    <!--	<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">-->
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <!--	<script src="validation_form/jquery-1.js" type="text/javascript"></script>-->
    <!--	<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>-->
    <!--	<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>-->
    <!--	<script>-->
    <!--		jQuery(document).ready(function(){-->
    <!--			// binds form submission and fields to the validation engine-->
    <!--			jQuery("#formID").validationEngine();-->
    <!--		});-->
    <!---->
    <!--		/**-->
    <!--		*-->
    <!--		* @param {jqObject} the field where the validation applies-->
    <!--		* @param {Array[String]} validation rules for this field-->
    <!--		* @param {int} rule index-->
    <!--		* @param {Map} form options-->
    <!--		* @return an error string if validation failed-->
    <!--		*/-->
    <!--		function checkHELLO(field, rules, i, options){-->
    <!--			if (field.val() != "HELLO") {-->
    <!--				// this allows to use i18 for the error msgs-->
    <!--				return options.allrules.validate2fields.alertText;-->
    <!--			}-->
    <!--		}-->
    <!--	</script>-->
    <!--Validation End-->
    <script type='text/javascript'>
        function popupwinid(p) {
            //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
            window.showModalDialog("meeting_notice_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
    </script>
</head>
<body>
<?php
require_once('../model/briefing_note_info.php');
require_once('../model/meeting_notice_info.php');
?>

<?php
$user_id = $_SESSION['user_id'];
//echo $_GET['ministry_member_id'];
if ($_GET['metting_notice_id'] != NULL) {
    $result1 = $briefing_note_info->editrow(array($_GET['metting_notice_id'], $user_id));
}

if ($_GET['metting_notice_id'] != NULL) {
    $result = $meeting_notice_info->editrow(array($_GET['metting_notice_id'], $user_id));
}

?>
<?php
$xml = simplexml_load_file("xml/briefing_note_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $committee = $information->committee;
        $introduction = $information->introduction;
        $key_issue = $information->key_issue;
        $advisor_question = $information->advisor_question;
        $witness_question = $information->witness_question;
        $written_evidence = $information->written_evidence;
        $date_start = $information->date_start;
        $data_end = $information->data_end;
        $date = $information->date;
        $time = $information->time;
        $venue = $information->venue;
        $inquiry_title = $information->inquiry_title;
    }
}
?>
<form id="formID" class="formular" method="post" action="#">
    <input type="hidden" name="metting_notice_id" value="<?php echo $_GET['metting_notice_id']; ?>"/>

    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div>
        <!--meeting-->
        <input type="hidden" name="metting_notice_id"
               value="<?php echo isset($result->metting_notice_id) ? $result->metting_notice_id : ''; ?>"/>
<!--        --><?php //var_dump($result) ?>

        <div style="width:65%; float:left;">
            <div>
                <div style="width:28%; float:left;"><?php echo $inquiry_title; ?></div>
                <div style="width:37%; float:left;">
<!--                    <input id="name" type="text" name="p_gov_no_pix" style="width:95%;"-->
<!--                           value="--><?php //echo $result->gov_no_pix . '/' . $result->gov_no_postfix ?><!--" readonly="true"/>-->
                    <input id="name" type="text" name="inquiry_title" style="width:95%;"
                           value="<?php echo strip_tags(htmlspecialchars_decode($result->inquiry_title)) ?>" readonly="true"/>
                </div>
                <div style="width:12%; float:left;"><?php echo $sitting_no; ?></div>
                <div style="width:18%; float:left;"><input id="name" type="text" name="sitting_no"
                                                           style="width:70%;color:#0000FF;"
                                                           onClick="<?php echo 'popupwinid(' . $result->metting_notice_id . ')'; ?>"
                                                           value="<?php echo isset($result->sitting_no) ? $result->sitting_no : ''; ?>"
                                                           readonly="true"/></div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:28%; float:left;"><?php echo $date; ?></div>
                <div style="width:25%; float:left;">
                    <input style='width:70%' id="demo1" type="text" name="en_date"
                           value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>" readonly="true"/>
                </div>
                <div style="width:12%; float:left;"><input id="name" type="text" name="bn_date" style="width:94%;"
                                                           value="<?php echo isset($result->bn_date) ? $result->bn_date : ''; ?>"
                                                           readonly="true"/></div>
                <div style="width:12%; float:left;"><?php echo $time; ?></div>
                <div style="width:22%; float:left;"><input id="name" type="text" name="time" style="width:70%;"
                                                           value="<?php echo isset($result->time) ? $result->time : ''; ?>"
                                                           readonly="true"/></div>
                <div class="divclear"></div>
            </div>
        </div>
        <div style="width:35%; float:left; margin-top:-10px;">
            <div>
                <div style="width:25%; float:left;"><?php echo $venue; ?></div>
                <div style="width:75%; float:left;">
                    <input id="name" type="text" style="width:180px;" name="venue_name"
                           value="<?php echo isset($result->venue_name) ? $result->venue_name : ''; ?>"
                           readonly="true"/>

                </div>
                <div class="divclear"></div>
            </div>
            <div>
                <div style="width:25%; float:left;"><?php echo $committee; ?></div>
                <div style="width:75%; float:left;">
                    <input id="name" type="text" name="venue_name"
                           value="<?php echo isset($result->sub_committee_name) ? $result->sub_committee_name : ''; ?>"
                           readonly="true"/>
                </div>
                <div class="divclear"></div>
            </div>
        </div>
        <div class="divclear"></div>
    </div>
    <!--meeting-->
    <!--Introduction-->
    <div>
        <div style="float:left;width:18%;">
            <label><b><?php echo $introduction; ?></b> </label>
        </div>
        <div style="float:left;width:80%;">
            <textarea type='text' name='introduction' id="area1" class='validate[required] text-input bangla'
                      style="font-size: 11px;width: 100%; height:40px;">   <?php echo isset($result1->introduction) ? $result1->introduction : ''; ?></textarea>
        </div>
    </div>
    <!--Key Issues-->
    <div>
        <div style="float:left;width:18%;">
            <label><b><?php echo $key_issue; ?></b> </label>
        </div>
        <div style="float:left;width:80%;">
            <textarea type='text' name='key_issue' id="area2" class='validate[required] text-input bangla'
                      style="font-size: 11px;width: 100%; height:40px;"><?php echo isset($result1->key_issue) ? $result1->key_issue : ''; ?></textarea>
        </div>
    </div>
    <!--Possible Questions for Special Advisors-->
    <div>
        <div style="float:left;width:18%;">
            <label><b><?php echo $advisor_question; ?></b> </label>
        </div>
        <div style="float:left;width:80%;">
            <textarea type='text' name='advisor_question' class='validate[required] text-input bangla' id="area3"
                      style="font-size: 11px;width: 100%; height:40px;"><?php echo isset($result1->advisor_question) ? $result1->advisor_question : ''; ?></textarea>
        </div>
    </div>
    <!--Possible Questions for Witness-->
    <div>
        <div style="float:left;width:18%;">
            <label><b><?php echo $witness_question; ?></b> </label>
        </div>
        <div style="float:left;width:80%;">
            <textarea type='text' name='witness_question' id="area4" class='validate[required] text-input bangla'
                      style="font-size: 11px;width: 100%; height:40px;"><?php echo isset($result1->witness_question) ? $result1->witness_question : ''; ?></textarea>
        </div>
    </div>
    <!--Written Evidance-->
    <div>
        <div style="float:left;width:18%;">
            <label><b><?php echo $written_evidence; ?></b> </label>
        </div>
        <div style="float:left;width:80%;">
            <textarea type='text' name='written_evidence' id="area5" class='validate[required] text-input bangla'
                      style="font-size: 11px;width: 100%; height:40px;"><?php echo isset($result1->written_evidence) ? $result1->written_evidence : ''; ?></textarea>
        </div>
    </div>
    <div>
        <?php //echo isset($result->remarks) ? $result->remarks: ''; ?>
        <div style="float:left;width:18%;"><label><?php echo 'Remarks'; ?></label></div>
        <div style="float:left;width:80%;">
            <textarea style="width:500px; height:50px;"
                      name="remarks"><?php echo isset($result1->remarks) ? $result1->remarks : ''; ?></textarea>
        </div>
        <div class="divclear"></div>
    </div>
    <br>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
    <input name="committee_id" type="hidden" value="<?php echo 1; //$_SESSION['committee_id']; ?>"/>

    <div>
        <div class="columnAbutton">
        </div>
        <div class="columnBbutton">
            &nbsp;&nbsp;<input type="submit" name="btn_close" value="Close" class="popupbutton"/>
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp;
            &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
            <input id="send" name="btn_save_b" type="submit" value="Save" class="popupbutton"/>

        </div>
        <div class="divclear"></div>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
