<?php
session_start();
//echo $_SESSION['language_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/committee_member_info.php');
?>
<?php
$user_id = $_SESSION['user_id'];
//echo $_GET['organization_member_id'];
if ($_GET['committee_member_id'] != NULL) {
    $result = $committee_member_info->editrow(array($_GET['committee_member_id'], $user_id));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <?php
    $xml = simplexml_load_file("xml/committee_member_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $organization_member_name = $information->organization_member_name;
            $organization_name = $information->organization_name;
            $comments = $information->comment;
            $designation = $information->designation;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript">
    </script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;">

<form id="formID" class="formular" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    </br>
    <div style="width:820px;">
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?></label></div>
            <div class="columnB"><input id="name" type="hidden" name="committee_member_id"
                                        value="<?php echo isset($result->committee_member_id) ? $result->committee_member_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>

            <div class="divclear"></div>
        </div>
        <!--<div>
				<div class="columnA"><label><?php /*?><?php echo $organization_name;?><?php */ ?></label>
				</div>
			    <div class="columnB">
				
				<?php /*?><?php				   
				  $committee_id=isset($result->committee_id) ? $result->committee_id: '';
		          $language_id=$_SESSION['language_id'];
			      $user_id=$_SESSION['user_id'];
		          echo $committee_member_info->comboview($language_id,$user_id,$committee_id);
		  	    ?><?php */
        ?>
				</div>
				<div class="divclear"></div>
			</div>-->
        <div>
            <div class="columnA"><label><?php echo $organization_member_name; ?> *</label>
            </div>
            <div class="columnB">
                <input type='text' name='member_name' class='validate[required,maxSize[80]] text-input bangla'
                       value="<?php echo isset($result->member_name) ? $result->member_name : ''; ?>">
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $designation; ?> *</label>
            </div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='designation' class='validate[required,maxSize[80]] text-input bangla'
                           value="<?php echo isset($result->designation) ? $result->designation : ''; ?>">
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $phone; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='phone' class='validate[maxSize[80],custom[phone]] text-input bangla'
                           value="<?php echo isset($result->phone) ? $result->phone : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $cell_phone; ?> *
                </div>
                <div style="float:left;">
                    <input type='text' name='cell_phone'
                           class='validate[required,maxSize[80],custom[cellphone]] text-input bangla'
                           value="<?php echo isset($result->cell_phone) ? $result->cell_phone : ''; ?>"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $fax; ?></label></div>
            <div class="columnB">
                <div style="float:left;">
                    <input type='text' name='fax' class='validate[maxSize[80],custom[fax]] text-input bangla'
                           value="<?php echo isset($result->fax) ? $result->fax : ''; ?>">
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    <?php echo $email; ?>
                </div>
                <div style="float:left;">
                    <input type='text' name='email' class='validate[maxSize[80],custom[email]] text-input bangla'
                           value="<?php echo isset($result->email) ? $result->email : ''; ?>"></input>
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $address; ?> *</label></div>
            <div class="columnB">
                <textarea type='text' name='address' class='validate[required] text-input bangla'
                          style="font-size: 11px;width: 450px;"><?php echo isset($result->address) ? $result->address : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $comments; ?></label></div>
            <div class="columnB">
                <textarea type='text' name='comments'
                          style="font-size: 11px;width: 450px;"><?php echo isset($result->comments) ? $result->comments : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
