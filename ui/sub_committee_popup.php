<?php
session_start();

//echo $_SESSION['language_id'];
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/commiittee_info.php');
require_once('../model/sub_committee_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['sub_committee_id'] === '0') {
    $result = $sub_committee->editrow(array($_GET['sub_committee_id'], $_GET['committee_id']));
} else {
    $result = $sub_committee->editrow(array($_GET['sub_committee_id'], '0'));
}

if (isset($_GET['committee_id'])) {
    $result2 = $commiittee_info->editrow(array($_GET['committee_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/sub_committee_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {

            $heading = $information->heading;
            $id = $information->id;
            $committee = $information->committee;
            $sub_committee_name = $information->sub_committee_name;
            $period_from = $information->period_from;
            $period_to = $information->period_to;
            $is_active = $information->is_active;
        }
    }

    ?>

    <!--validation start -->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <link rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css" type="text/css">


    <script src="js/jquery-1.7.min.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>

    <script type="text/javascript">
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
            jQuery("#period_from").datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
            jQuery("#period_to").datepicker({ dateFormat: "yy-mm-dd", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });

        /**
         *
         * @param {jqObject} the field where the validation applies
         * @param {Array[String]} validation rules for this field
         * @param {int} rule index
         * @param {Map} form options
         * @return an error string if validation failed
         */
        function checkHELLO(field, rules, i, options) {
            if (field.val() != "HELLO") {
                // this allows to use i18 for the error msgs
                return options.allrules.validate2fields.alertText;
            }
        }
    </script>

    <!--Validation End -->


</head>
<body style="background-color:#FFFFFF;">

<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2><?php echo $heading; ?></h2>
    <br/>

    <div>

        <div>
            <div class="columnA"><label><?php echo $committee; ?></label></div>

            <div class="columnB"><input id="name" type="text" name="committee_name"
                                        value="<?php echo isset($result->committee_name) ? $result->committee_name : $result2->committee_name; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <div style="display:none;">
            <div class="columnA"><label><?php echo $id; ?></label></div>
            <div class="columnB"><input id="name" type="text" name="sub_committee_id"
                                        value="<?php echo isset($result->sub_committee_id) ? $result->sub_committee_id : '[Auto]'; ?>"
                                        readonly="true"/></div>
            <div class="divclear"></div>
        </div>
        <input name="committee_id" type="hidden"
               value="<?php echo isset($result2->committee_id) ? $result2->committee_id : $result->committee_id; ?>">

        <div>
            <div class="columnA"><label><?php echo $sub_committee_name; ?> *</label></div>
            <div class="columnB"><input id="name" type="text" class="validate[required,maxSize[150]] text-input bangla"
                                        name="sub_committee_name"
                                        value="<?php echo isset($result->sub_committee_name) ? $result->sub_committee_name : 'Sub Committee -' . $result->subCommitteeNumber; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>

        <div>
            <div class="columnA"><label><?php echo $period_from; ?> *</label></div>
            <div class="columnB"><input id="period_from" type="text" class="validate[required] text-input bangla"
                                        name="period_from"
                                        value="<?php echo isset($result->period_from) ? substr($result->period_from, 0, 10) : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $period_to; ?> *</label></div>
            <div class="columnB"><input id="period_to" type="text" class="validate[required] text-input bangla"
                                        name="period_to"
                                        value="<?php echo isset($result->period_to) ? substr($result->period_to, 0, 10) : ''; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div class="columnA"><label><?php echo $is_active; ?> *</label></div>
            <div class="columnB"><input id="is_active" type="checkbox" class="text-input bangla" name="is_active"
                                        value="1" <?php echo (isset($result->is_active) && $result->is_active == '1') ? 'checked' : ''; ?> />
            </div>
            <div class="divclear"></div>
        </div>
        <br/>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save_commiitee" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;&nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">Close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>
