<?php
session_start();

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).

require_once('MYPDF.php');

require_once('../model/inquiry_report_info.php');
require_once('../model/meeting_notice_info.php');
require_once('../model/record_decisions_info.php');
require_once('../biz/receive_evidance_biz.php');

$user_id = $_SESSION['user_id'];
$introductions = '';
$issues = '';
if ($_GET['inquiry_report_id'] != NULL) {
    $result1 = $inquiry_report_info->rpt_inquiry_report_getall(array($_GET['inquiry_report_id']));

    $result = mysqli_fetch_object($result1);
}

// var_dump($result);


// var_dump($result);
// die();

$confData = array(
    'pageTitle' => 'Inquiry Report',
    'committeeName' => '',
    'parliamentName' => '',
    'reportName' => '',
    'language' => ($_SESSION['language_id'] === '1') ? 'english' : 'bangla',
    'isDefault' => false
);

// var_dump($confData);
// create new PDF document
$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false, $confData);
$pdf->SetHeaderMargin(20);

$pdf->AddPage();

// Logo
$image_file = 'img/logo.png';
$pdf->Image($image_file, 10, 10, 25, '', 'PNG', '', 'T', false, 300, '', false, false, 0, false, false, false);

if ($result->is_approved === '0') {
    $img_file = K_PATH_IMAGES . 'draft_report.JPG';
    $pdf->Image($img_file, 60, 80, 60, 50, '', '', '', false, 300, '', false, false, 0);
}

// set text shadow effect
// $pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));
// var_dump($pdf->fontName);

$pdf->setCellHeightRatio(1.2);

$pdf->SetFont('cambriab', '', 36);
$pdf->MultiCell(0, 50, '');
$pdf->MultiCell(0, 0, $result->committee_name . ' of the ' . $result->parliament . ' of Bangladesh - ' . $result->title, false, 'C', false, 1);

$pdf->SetFont('cambria', '', 22);
$pdf->MultiCell(0, 4, '');
// $pdf->Cell(0, 40, 'Inquiry Report No.'.$result->report_no, 'TBRL', 1, 'C', false, '', 0, false, 'C', 'B');
$pdf->MultiCell(0, 0, 'Inquiry Report No.' . $result->report_no, false, 'C', false, 1, '', '', true, '', false, '', '', 'B');
if (!empty($result->report_date)) {
    $pdf->MultiCell(0, 4, '');
    $pdf->MultiCell(0, 0, $result->report_date, false, 'C', false, 1);
}

// add a page
$pdf->AddPage();
$pdf->Bookmark('Executive Summary                                                                                                                              Page', 0, 0, '', '', array(0, 0, 0));
$pdf->SetFont('cambriab', '', 14);
$pdf->Cell(0, 10, 'Executive Summary', 0, 1, 'L');
$pdf->SetFont('georgia', '', 12);
$pdf->MultiCell(0, 0, $result->executive_summary, false, 'L', false, 1, '', '', true, '', false, '', '', 'B');

$pdf->SetFont('cambriab', '', 14);
$pdf->Bookmark('Recommendations                                                                                                                                Page', 0, 0, '', '', array(0, 0, 0));
$pdf->Cell(0, 10, 'Recommendations', 0, 1, 'L');
$pdf->SetFont('georgia', '', 12);
$pdf->MultiCell(0, 0, $result->recommendation, false, 'L', false, 1, '', '', true, '', false, '', '', 'B');

$pdf->AddPage();
$pdf->SetFont('cambriab', '', 14);
$pdf->Bookmark('Introduction                                                                                                                                            Page', 0, 0, '', '', array(0, 0, 0));
$pdf->Cell(0, 10, 'Introduction', 0, 1, 'L');
$pdf->SetFont('georgia', '', 12);
$pdf->MultiCell(0, 0, $result->introduction, false, 'L', false, 1, '', '', true, '', false, '', '', 'B');


$pdf->AddPage();
$pdf->SetFont('cambriab', '', 14);
$pdf->Bookmark('Annex A – Record of Decisions                                                                                                        Page', 0, 0, '', '', array(0, 0, 0));
$pdf->Cell(0, 10, 'Annex A – Record of Decisions', 0, 1, 'L');

$resultMeetingNotice = $meeting_notice_info->getall_meeting_notice_by_inquiry_title($result->inquiry_title);
// $firstROD = true;
$inquiryID = '';
while ($row = mysqli_fetch_assoc($resultMeetingNotice)) {
    // var_dump($row);
    $resultRecordOfDecision = $record_decisions_info->rpt_record_of_decision_getone(array($row['metting_notice_id']));
    if (!empty($inquiryID)) {
        $pdf->AddPage();
    } else {
        $inquiryID = $row['inquiry_id'];
    }
    $pdf->SetFont('cambriab', '', 20);
    $pdf->MultiCell(0, 0, 'RECORD OF DECISION', false, 'C', false, 1);
    $pdf->SetFont('cambria', '', 12);

    $html = '<b>Date: </b>' . htmlspecialchars_decode($resultRecordOfDecision->en_date) . ' and ' . htmlspecialchars_decode($resultRecordOfDecision->bn_date) . '<br>';
    $html .= '<b>Time: </b>' . htmlspecialchars_decode($resultRecordOfDecision->time) . '<br>';
    $html .= '<b>Location: </b>' . htmlspecialchars_decode($resultRecordOfDecision->venue_name) . '<br>';
    $html .= '<b>Chair: </b>' . htmlspecialchars_decode($resultRecordOfDecision->chairman_name) . '<br>';
    $html .= '<b>Members Present: </b>' . htmlspecialchars_decode($resultRecordOfDecision->member_name) . '<br>';
    $html .= '<b>Officials Presents: </b>' . htmlspecialchars_decode($resultRecordOfDecision->logistic_member_name) . '<br>';
    $html .= '<b>Witnessess: </b>' . htmlspecialchars_decode($resultRecordOfDecision->witness_member_name) . '<br><br>';
    $html .= '<b>PRIVATE BUSINESS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->private_business) . '<br>';
    $html .= '<b>PUBLIC BUSINESS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->public_business) . '<br>';
    $html .= '<b>RESULT OF DELIBERATIONS</b><br>' . htmlspecialchars_decode($resultRecordOfDecision->result_of_deliberation) . '<br>';


    $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
    $html = '';
}

$receiveEvidanceBiz = new receive_evidance_biz();
$resultReceiveEvidance = $receiveEvidanceBiz->getAllRawDetails($inquiryID);
$oralEvidance = array();
$writtenEvidance = array();
while ($rowEvidance = mysqli_fetch_assoc($resultReceiveEvidance)) {
    if ($rowEvidance['evidence_type'] === 'Multimedia') {
        array_push($oralEvidance, $rowEvidance['submitted_by']);
    } else {
        array_push($writtenEvidance, $rowEvidance['submitted_by']);
    }
}
$pdf->AddPage();
$pdf->SetFont('cambriab', '', 14);
$pdf->Bookmark('Annex B – List of Evidence Received                                                                                             Page', 0, 0, '', '', array(0, 0, 0));
$pdf->Cell(0, 10, 'Annex B – List of Evidence Received', 0, 1, 'L');
$pdf->SetFont('georgia', 'B', 12);
$pdf->Cell(0, 10, 'Oral Evidance', 0, 1, 'L');
$pdf->SetFont('georgia', '');
foreach ($oralEvidance as $value) {
    $pdf->MultiCell(0, 0, $value, false, 'L', false, 1, 25, '', true, '', false, '', '', 'B');
}
$pdf->SetFont('georgia', 'B');
$pdf->Cell(0, 10, 'Written Evidance', 0, 1, 'L');
$pdf->SetFont('georgia', '');
foreach ($writtenEvidance as $value) {
    $pdf->MultiCell(0, 0, $value, false, 'L', false, 1, 25, '', true, '', false, '', '', 'B');
}


// $pdf->Cell(0, 10, 'Paragraph 1.1', 0, 1, 'L');


// $html = '<div style="margin-top:10px;">';
// $html .= '<b>Introduction</b><br>'.htmlspecialchars_decode($result->introduction).'<br>';
// $html .= '<b>Key Issues</b><br>'.htmlspecialchars_decode($result->key_issue).'<br>'; 
// $html .= '<b>Possible Questions for Specialist Advisors</b><br>'.htmlspecialchars_decode($result->advisor_question).'<br>'; 
// $html .= '<b>Possible Questions for Witnesses</b><br>'.htmlspecialchars_decode($result->witness_question).'<br>'; 
// $html .= '<b>Written Evidence</b><br>'.htmlspecialchars_decode($result->written_evidence).'<br>'; 
// $html .= '</div>';

// $pdf->SetFont('cambriab');

// $html = '<b>Introduction</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '     '.htmlspecialchars_decode($result->introduction).'<br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Key Issues</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->key_issue).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Possible Questions for Specialist Advisors</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->advisor_question).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Possible Questions for Witnesses</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->witness_question).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('cambriab');
// $html = '<b>Written Evidence</b><br>';
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// $pdf->SetFont('times');
// $html = '      '.htmlspecialchars_decode($result->written_evidence).'<br>'; 
// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// $pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);


// . . . . . . . . . . . . . . . . . . . . . . . . . . . . . .

// add a new page for TOC
$pdf->addTOCPage();


// $pdf->AddPage();
// $pdf->Bookmark('Paragraph 1.3', 1, 0, '', '', array(128,0,0));
// $pdf->Cell(0, 10, 'This is a cover page', 0, 1, 'L');

// $pdf->AddPage();
// write the TOC title
$pdf->SetFont('cambriab', '', 18);
$pdf->MultiCell(0, 0, 'Table Of Content', 0, 'C', 0, 1, '', '', true, 0);
$pdf->Ln();

$pdf->SetFont('cambria', '', 12);

// add a simple Table Of Content at first page
// (check the example n. 59 for the HTML version)
$pdf->addTOC(2, 'cambria', ' ', 'INDEX', '', array(0, 0, 0));

// end of TOC page
$pdf->endTOCPage();


// ---------------------------------------------------------


// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
// $pdf->Output('pdf_rpt_briefing_note_'.$_GET['inquiry_report_id'].'.pdf', 'I');
if ($result->is_approved === '0') {
    $pdf->Output('pdf_rpt_inquiry_report_' . $_GET['inquiry_report_id'] . '.pdf', 'I');
} else {
    $pdf->Output('issued_approved_pdf/pdf_rpt_inquiry_report_' . $_GET['inquiry_report_id'] . '.pdf', 'F');
    echo "<script>window.close();</script>";
}
//============================================================+
// END OF FILE
//============================================================+
