<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link rel="stylesheet" href="styles/jquery-ui-1.10.4.custom.min.css" type="text/css"/>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
    $(document).ready(function () {
        $("#start_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
        $("#end_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: calculateWeekend});
    });
    function calculateWeekend(date) {
        var weekend = date.getDay() == 5 || date.getDay() == 6;
        return [true, weekend ? 'myweekend' : ''];
    }
    ;
</script>

<script type='text/javascript'>
    function popupwinid(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("receive_evidance_popup.php?doc_reg_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }
</script>
<?php
require_once('../model/receive_evidance_info.php');
?>
<?php
$xml = simplexml_load_file("xml/receive_evidance_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date = $information->doc_reg_date;
        $doc_title = $information->doc_title;
        $designation = $information->designation;
        $doc_name = $information->doc_name;
        $to = $information->to;
        $year = $information->year;
        $month = $information->month;
        $date = $information->date;
        $inquiry_title = $information->inquiry_title;
        $inquiry_no = $information->inquiry_no;
        $create_date = $information->create_date;
    }
}
?>

<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<?php
if (isset($_GET['inquiry_no']) || isset($_GET['inquiry_title']) || isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['organization_name']) || isset($_GET['doc_reg_no']) || isset($_GET['deadline'])) {
    echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
} else {
    echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
}
?>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <fieldset>
        <legend>Search Criteria</legend>
        <div style='margin:3px;'>
            <div>
                <div style='float:left;width:48%;'>
                    <div style='float:left;width:48%;'>
                        <?php echo $inquiry_no; ?>
                    </div>
                    <div style='float:left;width:48%;'>
                        <input type="text" style='width:90%' name="inquiry_no"
                               value="<?php echo isset($_GET['inquiry_no']) ? $_GET['inquiry_no'] : ''; ?>"/>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div style='float:left;width:48%;'>
                    <div style='float:left;width:48%;'>
                        <?php echo $inquiry_title; ?>
                    </div>
                    <div style='float:left;width:48%;'>
                        <input type="text" style='width:90%' name="inquiry_title"
                               value="<?php echo isset($_GET['inquiry_title']) ? $_GET['inquiry_title'] : ''; ?>"/>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div style='clear:both'></div>
            </div>
            <div>
                <div style='float:left;width:48%;'>
                    <div style='float:left;width:48%;'>
                        From
                    </div>
                    <div style='float:left;width:48%;'>
                        <input type="text" style='width:90%' id="start_date" name="start_date"/>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div style='float:left;width:48%;'>
                    <div style='float:left;width:48%;'>
                        <?php echo $to; ?>
                    </div>
                    <div style='float:left;width:48%;'>
                        <input type="text" style='width:90%' id="end_date" name="end_date"/>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div>
                    <div style='float:left;width:48%;'>
                        <div style='float:left;width:48%;'>
                            Organization Name
                        </div>
                        <div style='float:left;width:48%;'>
                            <input type="text" style='width:90%' name="organization_name"/>
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='float:left;width:48%;'>
                        <div style='float:left;width:48%;'>
                            p_doc_reg_no
                        </div>
                        <div style='float:left;width:48%;'>
                            <input type="text" style='width:90%' name="doc_reg_no"/>
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div>
                    <div style='float:left;width:48%;'>
                        <div style='float:left;width:48%;'>
                            Deadline
                        </div>
                        <div style='float:left;width:48%;'>
                            <input type="text" style='width:90%' id="calendar3" name="deadline"/>
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='float:left;width:48%;'>
                        <div style='float:left;width:48%;'>
                            &nbsp;
                        </div>
                        <div style='float:left;width:48%;'>
                            <input type="submit" value="Search" name="search_button" class="newbutton"/>
                        </div>
                        <div style='clear:both'></div>
                    </div>
                    <div style='clear:both'></div>
                </div>
                <div style='clear:both'></div>
            </div>
            <div>
            </div>
    </fieldset>
</form>
</div>
<?php
if (isset($_GET['inquiry_no'])) {
    $inquiry_n = $_GET['inquiry_no'];
}
if (isset($_GET['inquiry_title'])) {
    $inquiry_title = $_GET['inquiry_title'];
}
if (isset($_GET['start_date'])) {
    $start_date = $_GET['start_date'];
}
if (isset($_GET['end_date'])) {
    $end_date = $_GET['end_date'];
}
if (isset($_GET['organization_name'])) {
    $organization_name = $_GET['organization_name'];
}

if (isset($_GET['doc_reg_no'])) {
    $doc_reg_no = $_GET['doc_reg_no'];
}
if (isset($_GET['deadline'])) {
    $deadline = $_GET['deadline'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
?>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($inquiry_no, $inquiry_title, $create_date);
    if (isset($_GET['inquiry_no']) || isset($_GET['inquiry_title']) || isset($_GET['start_date']) || isset($_GET['end_date']) || isset($_GET['organization_name']) || isset($_GET['doc_reg_no']) || isset($_GET['deadline'])) {

        echo $receive_evidance_info->searchgridview($inquiry_n, $inquiry_title, $start_date, $end_date, $organization_name, $doc_reg_no, $deadline, $language_id, $committe_id, $user_id, $param);
    } else {
        echo $receive_evidance_info->searchgridview(' ', ' ', ' ', ' ', ' ', ' ', ' ', $language_id, $committe_id, $user_id, $param);
    }
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>