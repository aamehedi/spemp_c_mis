<?php
session_start();
?>
<?php
require_once('../model/request_evidence_info.php');
?>
<?php
require_once('../model/inquiry_scheduling_info.php');
?>
<?php
require_once('../model/create_inquiry_info.php');

$user_id = $_SESSION['user_id'];
if ($_GET['inquiry_id'] != NULL) {
    $result = $create_inquiry_info->editrow(array($_GET['inquiry_id'], $user_id));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">
    <script src="codebase_m/dhtmlxcalendar.js"></script>
    <style>
        #calendar,
        #calendar2,
        #calendar3,
        #calendar4 {
            border: 1px solid #909090;
            font-family: Tahoma;
            font-size: 12px;
        }
    </style>
    <script>
        var myCalendar;
        function doOnLoad() {
            myCalendar = new dhtmlXCalendarObject(["calendar", "calendar2", "calendar3", "calendar4"]);
        }
    </script>
    <script language="javascript" type="text/javascript" src="datepopup/datetimepicker.js">

        //Date Time Picker script- by TengYong Ng of http://www.rainforestnet.com
        //Script featured on JavaScript Kit (http://www.javascriptkit.com)
        //For this script, visit http://www.javascriptkit.com
    </script>
    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <link rel="stylesheet" type="text/css" href="codebase_m/dhtmlxcalendar.css">
    <link rel="stylesheet" type="text/css" href="codebase_m/skins/dhtmlxcalendar_dhx_skyblue.css">

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--ajax Combo-->
    <script>
        function show(str) {
            if (str == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "session_combo.php?parliament_id=" + str, true);
            xmlhttp.send();
        }
    </script>
    <!--ajax Combo-->
</head>
<body onLoad="doOnLoad();" style="margin:10px 10px 10px 20px;">
<script type="text/javascript" src="js/jquery-1.7.min.js"></script>
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<script>
    $(document).ready(function () {
        $("#btn2").click(function () {
            $("#btn1").append("<div  style='float:left;padding-left:2px;'><input type='hidden' name='schedule_no[]' value='0' /><input type='text' name='schedule_date[]' id='calendar4'/></div>");
        });
    });
</script>
<?php
$xml = simplexml_load_file("xml/create_inquiry_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date = $information->doc_reg_date;
        $doc_title = $information->doc_title;
        $designation = $information->designation;
        $doc_name = $information->doc_name;
        $to = $information->to;
        $year = $information->year;
        $month = $information->month;
        $date = $information->date;
        $inquiry_title = $information->inquiry_title;
        $inquiry_no = $information->inquiry_no;
        $create_date = $information->create_date;
        $proposed_date = $information->proposed_date;
        $ministry = $information->ministry;
        $organization = $information->organization;
        $others = $information->others;
        $witnesses = $information->witnesses;
        $other_witnessess = $information->other_witnessess;
    }
}
?>
<form id="formID" class="formular" action="#" method="post">
<h2><?php echo $headingpopup; ?></h2>
</br>
<div style="width:1380px;">
<div style="margin-bottom:5px;">
<input type="hidden" name="inquery_id" value="<?php echo isset($result->inquiry_id) ? $result->inquiry_id : ''; ?>">

<div style="width:15%;float:left;">
    <?php echo $inquiry_no; ?>
</div>
<div style="width:15%;float:left;">
    <input type="text" readonly="true" class='validate[required] text-input bangla' name="inquery_no"
           value="<?php echo isset($result->inquiry_no) ? $result->inquiry_no : '[Auto]'; ?>"/>
</div>
<div style="width:8%;float:left;">
    <?php echo $create_date; ?> *
</div>
<div style="width:8%;float:left;">
    <input style='width:70%' class='validate[required] text-input bangla' id="calendar2" type="text" name="create_date"
           value="<?php echo isset($result->create_date) ? $result->create_date : date('d-m-Y'); ?>" disabled/>
</div>
<div>
    <div style='float:left;width:10%;'><?php echo $proposed_date; ?> *</div>
    <div style='float:left;width:15%;'>
        <div style="float:left;">
            <?php
            $id = isset($result->proposed_month) ? $result->proposed_month : '';
            //echo $id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select  name="proposed_month" disabled />';
            echo $create_inquiry_info->comboview_month($language_id, $user_id, $id);
            echo '</select>';
            ?>
        </div>
        <div style="float:left;margin-left:10px;">
            <?php
            $id = isset($result->proposed_year) ? $result->proposed_year : '';
            //echo $result->section_id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select  name="proposed_year" disabled />';
            echo $create_inquiry_info->comboview_year($language_id, $user_id, $id);
            echo '</select>';
            ?>
        </div>
    </div>

    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;"></div>
<div>
    <div style="float:left;width:15%;">
        <?php echo 'Parliament'; ?> *
    </div>
    <div style="float:left;width:15%;">
        <?php
        $id = isset($result->parliament_id) ? $result->parliament_id : '';
        //echo $result->parliament_id;
        //$id=1;
        $language_id = $_SESSION['language_id'];
        $user_id = $_SESSION['user_id'];
        echo '<select  name="parliament_id" onchange="show(this.value)" disabled/>';
        echo $request_evidence_info->comboview_parliament($language_id, $user_id, $id);
        echo '</select>';
        ?>
    </div>
    <div style="float:left;width:8%;">
        <?php echo 'Session'; ?> *
    </div>
    <div style="float:left;width:15%;" id="txtHint">
        <?php
        if ($_GET['inquiry_id'] != 0) {
            $id = isset($result->session_id) ? $result->session_id : '';
            //echo $result->section_id;
            $language_id = $_SESSION['language_id'];
            $user_id = $_SESSION['user_id'];
            echo '<select  name="session_id" disabled/>';
            echo $request_evidence_info->comboview_session($language_id, $user_id, $id);
            echo '</select>';
        } else {
            echo '<select>';
            echo '<option value="0">Select</option>';
            echo '</select>';
        }
        ?>

    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;"></div>
<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $doc_reg_no; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme5" name="doc_reg_no"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_document($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_document($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme5").tokenInput("document_reg_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>
<div>

<div style="margin-bottom:5px;">
    <div style="float:left;width:15%;">
        <?php echo $inquiry_title; ?> *
    </div>
    <div style="float:left;width:75%;">
        <textarea style="font-size: 11px;width:100%; height:50px;" class='validate[required] text-input bangla'
                  name="inquery_title" disabled/>
        <?php echo isset($result->inquiry_title) ? $result->inquiry_title : ''; ?>
        </textarea>
    </div>
    <div class="divclear"></div>
    <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
    <input type="hidden" value="<?php echo $_SESSION['committee_id']; ?>" name="committee_id">
    <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>
</div>

<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $ministry; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme1" name="ministry"/>

        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_ministry($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_ministry($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme1").tokenInput("ministry_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<button onclick="ministry_token()">+</button>-->

        <script>
            function ministry_token() {
                alert("assaasasas");
            }
        </script>
    </div>

    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $organization; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme2" name="organization"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_organization($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_organization($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme2").tokenInput("organization_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }


        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>

<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $others; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme9" name="others"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {
            if ($create_inquiry_info->gridview_token_pree_popolated_others($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_others($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme9").tokenInput("others_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>

    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('others_popup.php','1376201640569','width=400,height=150,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=700,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $witnesses; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme3" name="witness"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) {

            if ($create_inquiry_info->gridview_token_pree_popolated_witness($inquiry_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_witness($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme3").tokenInput("witness_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <!--<input type="button" value="+" name="" />-->
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-bottom:5px;display:none;">
    <div style="float:left;width:15%;">
        <?php echo $other_witnessess; ?>
    </div>
    <div style="float:left;width:75%;">
        <input type="text" id="demo-input-facebook-theme6" name="other_witness"/>
        <?php
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        if ($inquiry_id != 0) // if new inquiry
        {
            if ($create_inquiry_info->gridview_token_pree_popolated_others_witness($inquiry_id) != ']') // if details data found
            {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
                echo 'prePopulate: ';
                echo $create_inquiry_info->gridview_token_pree_popolated_others_witness($inquiry_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else // if details data not found
            {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else // if existing inquiry
        {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme6").tokenInput("other_witness_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('others_witness_popup.php?witness_id=0','1376201640569','width=620,height=420,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=500,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<div>
    <div style="float:left;width:15%;"><label><?php echo 'Schedule Date'; ?></label></div>

    <div style="float:left;width:25%;">
    </div>
    <div class="divclear"></div>
</div>
<div style="overflow-y:scroll;  width:75%;" id="btn1">

    <?php
    if ($_GET['inquiry_id'] != NULL) {
        $inquiry_id = $_GET['inquiry_id'];
        $user_id = $_SESSION['user_id'];
        echo $inquiry_scheduling_info->gridviewdetails_issued($inquiry_id, $user_id);
    }
    ?>
</div>

<div>
    <div style="float:left;width:25%;"><label><?php echo 'Actual Date'; ?></label></div>
    <div class="divclear"></div>
</div>
<div>
    <div style="float:left;width:20%;">
        &nbsp;&nbsp;
    </div>
    <script>
        function show_alert() {
            var r = confirm("Do You Want To Issued  This Record Or Data?");
            //alert('Delete'+p);
            if (r == false) {
                return false;
            }
        }
    </script>
    <div style="float:left;width:50%;">
        <input type="submit" name="btn_close" value="Close" class="popupbutton"/>
        &nbsp;&nbsp;<input id="send" name="btn_issued" type="submit" value="Issued" class="popupbutton"
                           onClick="return show_alert();"/>
    </div>
    <div class="divclear"></div>
</div>
</div>
</form>
</body>
</html>
