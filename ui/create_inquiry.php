<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<style type="text/css">
    .slider {
        display: none;
    }

    .collapseSlider {
        display: none;
    }

    .sliderExpanded .collapseSlider {
        display: block;
    }

    .sliderExpanded .expandSlider {
        display: none;
    }
</style>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript">
    function toggleSlides() {
        $('.toggler').click(function (e) {
            var id = $(this).attr('id');
            var widgetId = id.substring(id.indexOf('-') + 1, id.length);
            $('#' + widgetId).slideToggle();
            $(this).toggleClass('sliderExpanded');
            $('.closeSlider').click(function () {
                $(this).parent().hide('slow');
                var relatedToggler = 'toggler-' + $(this).parent().attr('id');
                $('#' + relatedToggler).removeClass('sliderExpanded');
            });
        });
    }
    ;
    $(function () {
        toggleSlides();
    });
</script>
<script type="text/javascript">

</script>
<script type='text/javascript'>
    function popupwinid(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("create_inquiry_popup.php?inquiry_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")

        //return false;
    }
    function issued(p) {
        //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
        window.showModalDialog("create_inquiry_issued.php?inquiry_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        //return false;
    }

    $(document).ready(function () {

        $('input[value="Print View"]').click(function () {
            if ($(this).prev().val() == '1') {
                window.showModalDialog("../pdf/create_inquiry_pdf/pdf_rpt_create_inquiry_" + $(this).attr('name') + ".pdf", "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px");
            } else {
                window.showModalDialog("create_inquiry_print.php?inquiry_id=" + $(this).attr('name'), "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            }
            ;
        });
    });
</script>
<?php
require_once('../model/create_inquiry_info.php');
?>
<?php
$xml = simplexml_load_file("xml/create_inquiry_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $doc_reg_no = $information->doc_reg_no;
        $id = $information->id;
        $doc_reg_date = $information->doc_reg_date;
        $doc_title = $information->doc_title;
        $designation = $information->designation;
        $doc_name = $information->doc_name;
        $to = $information->to;
        $year = $information->year;
        $month = $information->month;
        $date = $information->date;
        $inquiry_title = $information->inquiry_title;
        $inquiry_no = $information->inquiry_no;
        $create_date = $information->create_date;
        $ministry = $information->ministry;
        $organization = $information->inquiry_no;
        $others = $information->others;
        $witnesses = $information->witnesses;
        $other_witnessess = $information->other_witnessess;
        $inquery_no = $information->inquery_no;
        $inquery_title = $information->inquery_title;
        $proposed_date = $information->proposed_date;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
    <p class="toggler" id="toggler-slideOne" style="float:right; ">
        <span class="expandSlider">Show Search</span><span class="collapseSlider">Hide Search</span>
    </p>
</div>
<?php
if (isset($_GET['inquiry_no']) || isset($_GET['inquiry_title']) || isset($_GET['schedule_month']) || isset($_GET['schedule_year']) || isset($_GET['doc_reg_no'])) {
    echo '<div style="margin-top:1px; width:100%;"  id="slideOne">';
} else {
    echo '<div style="margin-top:1px; width:100%;" class="slider" id="slideOne">';
}
?>
<form id="overlay_form" name="overlay_form" method="get" action="#">
    <fieldset>
        <legend>Search Criteria</legend>
        <div style='margin:3px;'>
            <div>
                <div style='float:left;width:24.5%;'><?php echo $inquery_no; ?></div>
                <div style='float:left;width:24.5%;'><input type="text" style='width:90%' name="inquery_no"
                                                            value="<?php echo isset($_GET['inquery_no']) ? $_GET['inquery_no'] : ''; ?>"/>
                </div>
                <div style='float:left;width:24.5%;'><?php echo $inquery_title; ?></div>
                <div style='float:left;width:24.5%;'>
                    <input type="text" style='width:90%' name="inquiry_title"
                           value="<?php echo isset($_GET['inquiry_title']) ? $_GET['inquiry_title'] : ''; ?>"/>
                </div>
                <div style='clear:both'></div>
            </div>
            <div>
                <div style='float:left;width:24.5%;'><?php echo $proposed_date; ?></div>
                <div style='float:left;width:24.5%;'>
                    <div style="float:left;">
                        <select name="schedule_month" size="1">
                            <option value="" selected="selected">Selected</option>
                            <option value="January">January</option>
                            <option value="February">February</option>
                            <option value="March">March</option>
                            <option value="April">April</option>
                            <option value="May">May</option>
                            <option value="June">June</option>
                            <option value="July">July</option>
                            <option value="August">August</option>
                            <option value="September">September</option>
                            <option value="October">October</option>
                            <option value="November">November</option>
                            <option value="December">December</option>
                        </select>
                    </div>
                    <div style="float:left;margin-left:10px;">
                        <select name="schedule_year" size="1">
                            <option value="" selected="selected">Selected</option>
                            <option value="2001">2001</option>
                            <option value="2002">2002</option>
                            <option value="2003">2003</option>
                            <option value="2004">2004</option>
                            <option value="2005">2005</option>
                            <option value="2006">2006</option>
                            <option value="2007">2007</option>
                            <option value="2008">2008</option>
                            <option value="2009">2009</option>
                            <option value="2010">2010</option>
                            <option value="2011">2011</option>
                            <option value="2012">2012</option>
                            <option value="2013">2013</option>
                            <option value="2014">2014</option>
                            <option value="2015">2015</option>
                            <option value="2010">2016</option>
                            <option value="2011">2017</option>
                            <option value="2012">2019</option>
                            <option value="2013">2020</option>
                            <option value="2014">2021</option>
                            <option value="2015">2022</option>
                            <option value="2013">2023</option>
                            <option value="2014">2024</option>
                            <option value="2015">2025</option>
                        </select>
                    </div>

                </div>
                <div style="float:left;width:24.5%;">
                    <?php echo $doc_reg_no; ?>
                </div>
                <div style="float:left;width:24.5%;">
                    <input type="text" style='width:90%' name="doc_reg_no"
                           value="<?php echo isset($_GET['doc_reg_no']) ? $_GET['doc_reg_no'] : ''; ?>"/>
                </div>
                <div style='clear:both'></div>
            </div>
            <div>
                <input type="submit" value="Search" name="search_button" class="newbutton"/>
            </div>
            <div style='clear:both'></div>

        </div>
    </fieldset>
</form>
</div>
<?php
if (isset($_GET['inquery_no'])) {
    $inquiry_no = $_GET['inquery_no'];
}
if (isset($_GET['inquiry_title'])) {
    $inquiry_title = $_GET['inquiry_title'];
}

if (isset($_GET['schedule_month'])) {
    $schedule_month = $_GET['schedule_month'];
}
if (isset($_GET['schedule_year'])) {
    $schedule_year = $_GET['schedule_year'];
}
if (isset($_GET['doc_reg_no'])) {
    $doc_reg_no = $_GET['doc_reg_no'];
}
//echo $doc_reg_no.'--'.$doc_reg_date_start.'--'.$doc_reg_date_end;
//var_dump($_SESSION);
?>
<div class="content" id="conteudo">
    <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>

    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $committe_id = $_SESSION['committee_id'];
    $param = array($inquery_no, $inquery_title, $proposed_date);
    if (isset($_GET['inquiry_no']) || isset($_GET['inquiry_title']) || isset($_GET['schedule_month']) || isset($_GET['schedule_year']) || isset($_GET['doc_reg_no'])) {
        echo $create_inquiry_info->searchgridview($inquiry_no, $inquiry_title, $schedule_month, $schedule_year, $doc_reg_no, $language_id, $committe_id, $user_id, $param);
    } else {
        echo $create_inquiry_info->searchgridview('', '', '', '', '', $language_id, $committe_id, $user_id, $param);
    }
    ?>

</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>