/*
Navicat MySQL Data Transfer

Source Server         : mis_local
Source Server Version : 50141
Source Host           : 192.168.3.23:3306
Source Database       : dbmis

Target Server Type    : MYSQL
Target Server Version : 50141
File Encoding         : 65001

Date: 2013-10-23 11:06:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_briefing_note`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_briefing_note`;
CREATE TABLE `tbl_briefing_note` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `introduction` longtext,
  `key_issue` longtext,
  `advisor_question` longtext,
  `witness_question` longtext,
  `written_evidence` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_briefing_note
-- ----------------------------
INSERT INTO `tbl_briefing_note` VALUES ('1', 'This&amp;nbsp; document&amp;nbsp; outlines&amp;nbsp; advice&amp;nbsp; on&amp;nbsp; issues&amp;nbsp; the&amp;nbsp; PAC&amp;nbsp; may&amp;nbsp; wish&amp;nbsp; to&amp;nbsp; consider&amp;nbsp; when&amp;nbsp; scrutinizing&amp;nbsp; the audit report on the Ministry of Agriculture for the fiscal years 2006/07, 2007/08 and 2008/09. The report was published in June 2012 after an audit that commenced in 2010. &lt;br&gt;&amp;nbsp;&lt;br&gt;Unusually, in terms of good practice internationally, the report is consolidated and not presented separately for each year â€“ this may be because of the overall conclusion that it is not possible to present an audit conclusion (page 19).&lt;br&gt;', 'The&amp;nbsp;&amp;nbsp;&amp;nbsp; key&amp;nbsp;&amp;nbsp;&amp;nbsp; issues&amp;nbsp;&amp;nbsp;&amp;nbsp; raised&amp;nbsp;&amp;nbsp;&amp;nbsp; in&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; report&amp;nbsp;&amp;nbsp;&amp;nbsp; include:&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; â€¢&amp;nbsp; A&amp;nbsp;&amp;nbsp;&amp;nbsp; discrepancy&amp;nbsp;&amp;nbsp;&amp;nbsp; between&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; funds&amp;nbsp;&amp;nbsp;&amp;nbsp; allocated&amp;nbsp;&amp;nbsp;&amp;nbsp; for&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; establishment&amp;nbsp;&amp;nbsp;&amp;nbsp; of&amp;nbsp;&amp;nbsp;&amp;nbsp; regional&amp;nbsp;&amp;nbsp;&amp;nbsp; offices&amp;nbsp;&amp;nbsp;&amp;nbsp; and&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; receipts&amp;nbsp;&amp;nbsp;&amp;nbsp; provided&amp;nbsp;&amp;nbsp;&amp;nbsp; for&amp;nbsp;&amp;nbsp;&amp;nbsp; this&amp;nbsp;&amp;nbsp;&amp;nbsp; expenditure&amp;nbsp;&amp;nbsp; &lt;br&gt;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; â€¢&amp;nbsp; Inadequate&amp;nbsp;&amp;nbsp;&amp;nbsp; information&amp;nbsp;&amp;nbsp;&amp;nbsp; available&amp;nbsp;&amp;nbsp;&amp;nbsp; to&amp;nbsp;&amp;nbsp;&amp;nbsp; assess&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; current&amp;nbsp;&amp;nbsp;&amp;nbsp; volume&amp;nbsp;&amp;nbsp;&amp;nbsp; &lt;br&gt;', 'Members may wish to ask the OCAG representatives some general questions before calling the &lt;br&gt;witnesses from the Ministry: &lt;br&gt;&amp;nbsp;&lt;br&gt;â€¢&amp;nbsp; What&amp;nbsp; standards&amp;nbsp; has&amp;nbsp; the&amp;nbsp; Ministryâ€™s&amp;nbsp; internal&amp;nbsp; audit&amp;nbsp; department&amp;nbsp; established&amp;nbsp; for&amp;nbsp; the &lt;br&gt;conduct of such audits?&amp;nbsp; &lt;br&gt;â€¢&amp;nbsp; Why were three years considered together? &lt;br&gt;â€¢&amp;nbsp; Why did it take two years to present the report? &lt;br&gt;', 'Financial&amp;nbsp;&amp;nbsp;&amp;nbsp; Officer&amp;nbsp;&amp;nbsp;&amp;nbsp; 1)&amp;nbsp; I would like to deal with paragraphs 155 and 156. Can you explain why proper records &lt;br&gt;were not being maintained? &lt;br&gt;&amp;nbsp;2)&amp;nbsp; Turning&amp;nbsp; to&amp;nbsp; the&amp;nbsp; management&amp;nbsp; response&amp;nbsp; contained&amp;nbsp; in&amp;nbsp; para&amp;nbsp; 177,&amp;nbsp; on&amp;nbsp; what&amp;nbsp; basis&amp;nbsp; do&amp;nbsp; you &lt;br&gt;state that all supporting documentation was maintained? &lt;br&gt;&amp;nbsp; 3)&amp;nbsp; I&amp;nbsp; note&amp;nbsp; that&amp;nbsp; you&amp;nbsp; have&amp;nbsp; not&amp;nbsp; responded&amp;nbsp; to&amp;nbsp; the&amp;nbsp; Management&amp;nbsp; letter&amp;nbsp; in&amp;nbsp; respect&amp;nbsp; of&amp;nbsp; the &lt;br&gt;recommendations&amp;nbsp; in&amp;nbsp; paras&amp;nbsp; 186&amp;nbsp; â€“&amp;nbsp; 190.&amp;nbsp; Have&amp;nbsp; you&amp;nbsp; now&amp;nbsp; put&amp;nbsp; in&amp;nbsp; place&amp;nbsp; the&amp;nbsp; system &lt;br&gt;recommended in para 186? &lt;br&gt;&amp;nbsp; 4)&amp;nbsp; What&amp;nbsp; action&amp;nbsp; has&amp;nbsp; been&amp;nbsp; taken&amp;nbsp; to&amp;nbsp; provide&amp;nbsp; the&amp;nbsp; justifications&amp;nbsp; for&amp;nbsp; the&amp;nbsp; unauthorized&amp;nbsp; check &lt;br&gt;disbursement vouchers? &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;Head&amp;nbsp;&amp;nbsp;&amp;nbsp; of&amp;nbsp;&amp;nbsp;&amp;nbsp; Personnel&amp;nbsp;&amp;nbsp;&amp;nbsp; 1)&amp;nbsp; There&amp;nbsp; seems&amp;nbsp; to&amp;nbsp; be&amp;nbsp; a&amp;nbsp; recurring&amp;nbsp; theme&amp;nbsp; of&amp;nbsp; failure&amp;nbsp; to&amp;nbsp; answer&amp;nbsp; Management&amp;nbsp; letters.&amp;nbsp; Why 17 has this situation been allowed to continue? &lt;br&gt;&amp;nbsp;2)&amp;nbsp; I&amp;nbsp;&amp;nbsp;&amp;nbsp; note&amp;nbsp;&amp;nbsp;&amp;nbsp; that&amp;nbsp;&amp;nbsp;&amp;nbsp; there&amp;nbsp;&amp;nbsp;&amp;nbsp; has&amp;nbsp;&amp;nbsp;&amp;nbsp; been&amp;nbsp;&amp;nbsp;&amp;nbsp; no&amp;nbsp;&amp;nbsp;&amp;nbsp; response&amp;nbsp;&amp;nbsp;&amp;nbsp; to&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; Management&amp;nbsp;&amp;nbsp;&amp;nbsp; letter&amp;nbsp;&amp;nbsp;&amp;nbsp; regarding&amp;nbsp;&amp;nbsp;&amp;nbsp; the&amp;nbsp;&amp;nbsp;&amp;nbsp; recommendations&amp;nbsp;&amp;nbsp;&amp;nbsp; to&amp;nbsp;&amp;nbsp;&amp;nbsp; put&amp;nbsp;&amp;nbsp;&amp;nbsp; effective&amp;nbsp;&amp;nbsp;&amp;nbsp; controls&amp;nbsp;&amp;nbsp;&amp;nbsp; in&amp;nbsp;&amp;nbsp;&amp;nbsp; place&amp;nbsp;&amp;nbsp;&amp;nbsp; â€“&amp;nbsp;&amp;nbsp;&amp;nbsp; what&amp;nbsp;&amp;nbsp;&amp;nbsp; has&amp;nbsp;&amp;nbsp;&amp;nbsp; actually&amp;nbsp;&amp;nbsp;&amp;nbsp; been&amp;nbsp;&amp;nbsp;&amp;nbsp; done?&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; 3)&amp;nbsp; How can the Comptroller offer effective management oversight if the pay rolls &lt;br&gt;prepared by the personnel section are not validated?&amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp; Head&amp;nbsp;&amp;nbsp;&amp;nbsp; of&amp;nbsp;&amp;nbsp;&amp;nbsp; Travel&amp;nbsp;&amp;nbsp;&amp;nbsp; 1)&amp;nbsp; What&amp;nbsp; processes&amp;nbsp; are&amp;nbsp; in&amp;nbsp; place&amp;nbsp; to&amp;nbsp; ensure&amp;nbsp; that&amp;nbsp; only&amp;nbsp; registered&amp;nbsp; businesses&amp;nbsp; are&amp;nbsp; placed&amp;nbsp; on &lt;br&gt;the authorized procurement list?&lt;br&gt;&lt;br&gt;Ban&lt;br&gt;', '&lt;i&gt;&amp;nbsp;Written evidence submitted by the Family Farmersâ€™ Association &lt;br&gt;&amp;nbsp;Written evidence submitted by Philip Robinson &lt;br&gt;&amp;nbsp;Written evidence submitted by DG &amp;amp; GE Purser &lt;br&gt;&amp;nbsp;Written evidence submitted by Martin Hancox &lt;br&gt;&lt;/i&gt;&lt;i&gt;&amp;nbsp;Written evidence submitted by Ian Kett &lt;/i&gt;&lt;br&gt;&lt;i&gt;&amp;nbsp;Written evidence submitted by Keith Meldrum &lt;/i&gt;&lt;br&gt;&lt;i&gt;&amp;nbsp;Written evidence submitted by the Humane Society &lt;/i&gt;&lt;br&gt;&lt;i&gt;&amp;nbsp;Written evidence submitted by Network For Animals &lt;br&gt;&amp;nbsp;Written evidence submitted by Veterinary Association of Wildlife Management ( VAWM) &lt;br&gt;&amp;nbsp;Written evidence submitted by the Society of Biology &lt;br&gt;&amp;nbsp;Written evidence submission submitted by Brock Vaccination &lt;br&gt;&amp;nbsp;Written evidence submitted by the Roslin Institute &lt;br&gt;&amp;nbsp;Written evidence submitted by Rethink Bovine TB &lt;br&gt;&amp;nbsp;Written evidence submitted by The Wildlife Trusts &lt;br&gt;&amp;nbsp;Written evidence submitted by the International Fund for Animal Welfare &lt;br&gt;&amp;nbsp;Written evidence submitted by Secret World Wildlife Rescue &lt;br&gt;&amp;nbsp;Written evidence submitted by the NFU &lt;br&gt;&amp;nbsp;Written evidence submitted by Carla Kidd&lt;/i&gt;&lt;br&gt;', '1', '1', '0', '1');
INSERT INTO `tbl_briefing_note` VALUES ('3', 'hjkhk&lt;br&gt;\r\n', '&lt;br&gt;\r\n&lt;b&gt;jlkj&lt;/b&gt;&lt;br&gt;\r\n', '&lt;br&gt;\r\n&lt;b&gt;hkjhkj&lt;/b&gt;&lt;br&gt;', '&lt;br&gt;\r\n&lt;b&gt;jhlkjhl&lt;/b&gt;&lt;br&gt;\r\n', '&lt;br&gt;\r\n&lt;b&gt;hkjhk&lt;/b&gt;&lt;br&gt;\r\n', '1', '1', '0', '1');
INSERT INTO `tbl_briefing_note` VALUES ('2', 'sdgfdshf', 'sdgfdshf&lt;br&gt;\r\n&lt;br&gt;\r\n', 'sdgfdshf', 'sdgfdshf', 'sdgfdshf', '1', '1', '0', '1');
INSERT INTO `tbl_briefing_note` VALUES ('5', '&lt;br&gt;\r\n&lt;b&gt;101&lt;/b&gt;&lt;br&gt;\r\n', '&lt;br&gt;\r\n&lt;b&gt;1000&lt;/b&gt;&lt;br&gt;\r\n', '&lt;br&gt;\r\n&lt;b&gt;1000&lt;/b&gt;&lt;br&gt;', '&lt;br&gt;\r\n&lt;b&gt;1000&lt;/b&gt;&lt;br&gt;\r\n', '2000&lt;br&gt;\r\n', '2', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee`;
CREATE TABLE `tbl_committee` (
  `committee_id` smallint(6) DEFAULT NULL,
  `committee_name` varchar(200) DEFAULT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `section_id` smallint(6) DEFAULT NULL,
  `branch_id` smallint(6) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee
-- ----------------------------
INSERT INTO `tbl_committee` VALUES ('1', 'Public Accounts Committee', 'PAC', '3', '2', 'po', 'aaa', '032', '', '', '', 'ggjdgdlgdgddd', 'dssdsdds', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('2', 'Estimate Committee', 'PEC', '2', '2', 'dfsfsdfs', 'fdfd', '01745218', '254163987', '3164646', 'fsss@41313', 'dfdrere', 'fdfedrerew', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('3', 'Public Undertakings Committee', 'PUC', '3', '2', 'Surath', 'Committee Secretary', '', '', '', '', '', '', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('4', 'à¦ªà¦¾à¦¬à¦²à¦¿à¦• à¦…à§à¦¯à¦¾à¦•à¦¾à¦‰à¦¨à§à¦Ÿ à¦•à¦®à¦¿à¦Ÿà¦¿', 'à¦ªà¦¿à¦à¦šà¦¿', '12', '6', '010101', '010101', '010101', '010101', '010101', 'roufronyict@gmail.com', 'à¦¨à¦¾à¦®', 'à¦¨à¦¾à¦®', '2', '0', '1');
INSERT INTO `tbl_committee` VALUES ('5', 'à¦ªà¦¾à¦¬à¦²à¦¿à¦• à¦…à¦¨à§à¦®à¦¾à¦¨ à¦•à¦®à¦¿à¦Ÿà¦¿', 'PUC', '12', '6', '', '', '', '', '', '', '', '', '2', '0', '1');
INSERT INTO `tbl_committee` VALUES ('6', 'Labour Committee', 'LC', '10', '3', 'bgs', 'bgs', '', '', '', '', '', '', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('7', 'fdfd', 'fdfd', '2', '1', 'yuhnj', 'fdfdfd', '3131313', '022331', '', 'gfgfgfgfgf@1313', 'fdfd', 'fdwrewrcvcx', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('8', 'fdf', 'fd', '7', '3', 'dfd', 'dfddfdfdfd', '2525874133665', '25856974', '256', 'S64646@12555', 'dfd', 'fdfd', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('9', 'rerewrewrwerwerewrew', 'fdf', '3', '1', 'fdfdf', 'fdfdfdfd', '877474//7/7', '', '', '', 'dfsfsfa', 'fsfsf', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('10', 'dfdsf', 'dfdsf', '3', '2', 'df', 'fdf', '4242424', '43424', '2424234234', 'fdfsfa@45325245', 'fffdfd', 'dfafafd', '1', '0', '1');
INSERT INTO `tbl_committee` VALUES ('11', 'qeweqewqewqeq', 'dfsaf', '2', '3', 'fdfd', 'fdfdf', '4535353', '345345435', '5543535345435', 'dffdsfdsf@353535', 'dfdsfdsfd', 'dfsfsfdfdfdrewrwerwerwerwerw', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_branch`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_branch`;
CREATE TABLE `tbl_committee_branch` (
  `branch_id` smallint(6) DEFAULT NULL,
  `branch` varchar(100) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_branch
-- ----------------------------
INSERT INTO `tbl_committee_branch` VALUES ('1', 'BB', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('2', 'branch1', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('3', 'branch2', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('4', 'branch5', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('5', 'branch6', '1', '0', '1');
INSERT INTO `tbl_committee_branch` VALUES ('6', 'à¦¨à¦¾à¦®', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_member`;
CREATE TABLE `tbl_committee_member` (
  `committee_member_id` int(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_member
-- ----------------------------
INSERT INTO `tbl_committee_member` VALUES ('1', '1', 'Mr. M.A. Mannan, MP', 'Chairman', '1715039307', '11221122', '', '', 'Chairman																								', '																																								', '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('2', '1', 'Prof. Md. Ali Ashraf, MP', 'Member', '1711564571', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('3', '1', 'Mr. Md. Abdus Shahid, MP', 'Member', '8158898', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('4', '1', 'Mr. Salah Uddin Quader Chowdhury, MP', 'Member', '8834033', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('5', '1', 'Dr.T.I.M. Fazlay Rabbi Chowdhury, MP', 'Member', '1718148514', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('6', '1', 'Mr. M.K. Anwar, MP', 'Member', '1711534645', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('7', '1', 'Mr. Md. Emaj Uddin Pramanik, MP', 'Member', '1715138844', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('8', '1', 'Mr. Md. Sayedul Haque, MP', 'Member', '1717404110', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('9', '1', 'Mr. A.K.M. Rahmatullah, MP', 'Member', '1713030811', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('10', '1', 'Mr. Dhirendro Debnath Shambhu, MP', 'Member', '1715016030', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('11', '1', 'Khan Tipu Sultan, MP', 'Member', '1712155336', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('12', '1', 'Khandaker Asaduzzaman, MP', 'Member', '1711150049', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('13', '1', 'Major General Abdus Salam, MP', 'Member', '1711562288', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('14', '1', 'Mr. Narayon Chandra Chanda, MP', 'Member', '1711217548', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('15', '1', 'Mosammat Farida Akter, MP', 'Member', '1728912935', null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('16', '4', 'à¦¸à¦¦à¦¸à§à¦¯ ', 'à¦¸à¦¦à¦¸à§à¦¯ ', '', '01010010101', '', '', 'à¦¸à¦¦à¦¸à§à¦¯ 								', '								', '2', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('17', '1', 'Khan Tipu Sultan MP', 'Design', '3423', '3423', '', '', '		abc						', '								', '1', '0', '1');
INSERT INTO `tbl_committee_member` VALUES ('18', '4', 'à§¦à§¦à§¦à§§', 'à§¦à§¦à§¦à§§', '', '01010010101', '', '', 'à§¦à§§à§¦à§§à§¦à§§à§¦à§§à§¦								', '								', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_committee_section`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_committee_section`;
CREATE TABLE `tbl_committee_section` (
  `section_id` smallint(6) DEFAULT NULL,
  `section` varchar(100) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_committee_section
-- ----------------------------
INSERT INTO `tbl_committee_section` VALUES ('1', 'section1', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('2', 'section2', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('3', 'section3', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('4', 'section4', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('5', 'section5', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('6', 'section7', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('7', 'section8', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('8', 'section11', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('9', 'section12', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('10', 'section13', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('15', 'section15', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('12', 'à¦…à¦¨à§à¦šà§à¦›à§‡à¦¦ 1', '2', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('13', '444', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('14', '111', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('16', 'section16', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('17', 'section20', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('18', 'aa', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('19', 'oooo', '1', '0', '1');
INSERT INTO `tbl_committee_section` VALUES ('20', 'qqqq', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_document_key_areas`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_key_areas`;
CREATE TABLE `tbl_document_key_areas` (
  `doc_reg_id` int(11) DEFAULT NULL,
  `key_area` longtext,
  `recommendations` longtext,
  `result_forcasting` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_key_areas
-- ----------------------------
INSERT INTO `tbl_document_key_areas` VALUES ('11', 'rwrf', 'dasd			adasd', 'asdasdasdas', '0', '1');
INSERT INTO `tbl_document_key_areas` VALUES ('13', 'According to Subrata Sarkar', 'According to Subrata SArkar', 'According to Subrata SArkar', '0', '1');
INSERT INTO `tbl_document_key_areas` VALUES ('1', 'Key Area', 'Have some Recommendation fsdf', 'uusdopusoduosdsdsdddsdd', '0', '1');
INSERT INTO `tbl_document_key_areas` VALUES ('2', 'wwwwwww', 'sssss', 'fdfd', '0', '1');

-- ----------------------------
-- Table structure for `tbl_document_registration_details`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_registration_details`;
CREATE TABLE `tbl_document_registration_details` (
  `doc_reg_detail_id` bigint(20) DEFAULT NULL,
  `doc_reg_id` int(11) DEFAULT NULL,
  `doc_title` varchar(200) DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `doc_type` varchar(50) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_registration_details
-- ----------------------------
INSERT INTO `tbl_document_registration_details` VALUES ('1', '1', 'aasa', 'C:/upload/1.xml', 'text/xml', '0', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('2', '2', 'doc1', 'C:/upload/2.xml', 'text/xml', '0', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('3', '2', 'doc2', 'C:/upload/3.xls', 'application/vnd.ms-excel', '1', '1');
INSERT INTO `tbl_document_registration_details` VALUES ('4', '2', 'sd', 'C:/upload/4.', '', '0', '1');

-- ----------------------------
-- Table structure for `tbl_document_registration_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_document_registration_master`;
CREATE TABLE `tbl_document_registration_master` (
  `doc_reg_id` int(11) DEFAULT NULL,
  `doc_reg_no` varchar(50) DEFAULT NULL,
  `doc_reg_date` datetime DEFAULT NULL,
  `doc_title` text,
  `ref_no` varchar(150) DEFAULT NULL,
  `ref_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_document_registration_master
-- ----------------------------
INSERT INTO `tbl_document_registration_master` VALUES ('1', '0120130001', '2013-09-29 00:00:00', 'as as asa dgdsf sdfgdsfgsd sdfgs fgsd sdkf askjdf sakldf sdkjlfs fkjsa fkjsa fkjslaf sakljf sakjf salfkjsa fkljsa fklajsf aslkjf askljf askf asdkjf skljf sakf askf sakjfsadkf safkljsa fkljsa fkljsa flkas flksad fksafj sadk', 'rrrr', '2013-10-09 00:00:00', null, '1', '1', '0', '1');
INSERT INTO `tbl_document_registration_master` VALUES ('2', '0120130002', '2013-10-01 00:00:00', 'title111111', 'ref-001', '2013-10-08 00:00:00', '2013-10-03 16:06:06', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_follow_up_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_follow_up_master`;
CREATE TABLE `tbl_follow_up_master` (
  `inquiry_report_followup_id` bigint(20) DEFAULT NULL,
  `inquiry_report_id` int(11) DEFAULT NULL,
  `followup_no` varchar(50) DEFAULT NULL,
  `followup_date` datetime DEFAULT NULL,
  `background` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_follow_up_master
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_follow_up_recommendation`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_follow_up_recommendation`;
CREATE TABLE `tbl_follow_up_recommendation` (
  `inquiry_report_followup_id` bigint(20) DEFAULT NULL,
  `recom_id` bigint(20) DEFAULT NULL,
  `recommendation` longtext,
  `organization_response` varchar(100) DEFAULT NULL,
  `action_taken` longtext,
  `comment` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_follow_up_recommendation
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_group`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group`;
CREATE TABLE `tbl_group` (
  `group_id` int(11) DEFAULT NULL,
  `group_name` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_group
-- ----------------------------
INSERT INTO `tbl_group` VALUES ('1', 'borhan');
INSERT INTO `tbl_group` VALUES ('3', 'af');
INSERT INTO `tbl_group` VALUES ('2', 'User');
INSERT INTO `tbl_group` VALUES ('4', 'dfdfd');
INSERT INTO `tbl_group` VALUES ('5', 'ssssssss');
INSERT INTO `tbl_group` VALUES ('6', 'sww2');

-- ----------------------------
-- Table structure for `tbl_group_menu_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_group_menu_permission`;
CREATE TABLE `tbl_group_menu_permission` (
  `group_id` int(11) DEFAULT NULL,
  `menu_id` int(11) DEFAULT NULL,
  `is_view` tinyint(4) DEFAULT NULL,
  `is_insert` tinyint(4) DEFAULT NULL,
  `is_edit` tinyint(4) DEFAULT NULL,
  `is_delete` tinyint(4) DEFAULT NULL,
  `is_print` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_group_menu_permission
-- ----------------------------
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '6', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '7', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '8', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '9', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '20', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '10', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '11', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '12', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '13', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '14', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '15', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '16', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '17', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '18', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '19', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '21', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '22', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '31', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '32', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '33', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '34', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '35', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '36', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '41', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '40', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '39', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '38', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '37', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '42', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '43', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '5', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('1', '25', '1', '1', '1', '1', '1');
INSERT INTO `tbl_group_menu_permission` VALUES ('3', '7', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('3', '16', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('5', '6', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('5', '36', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('5', '8', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('5', '15', '0', '0', '0', '0', '0');
INSERT INTO `tbl_group_menu_permission` VALUES ('5', '16', '0', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for `tbl_inquiry_doc_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_doc_tag`;
CREATE TABLE `tbl_inquiry_doc_tag` (
  `inquiry_id` int(11) DEFAULT NULL,
  `doc_reg_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_doc_tag
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_inquiry_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_master`;
CREATE TABLE `tbl_inquiry_master` (
  `inquiry_id` int(11) DEFAULT NULL,
  `inquiry_no` varchar(50) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `inquiry_title` text,
  `proposed_month` varchar(50) DEFAULT NULL,
  `proposed_year` varchar(4) DEFAULT NULL,
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_master
-- ----------------------------
INSERT INTO `tbl_inquiry_master` VALUES ('5', 'PAC20130003', '2013-10-09 00:00:00', 'Government Finance Accounts 2006-2007 and 2007-2008', 'October', '2013', '1', '1', '1', '1', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('6', 'à¦ªà¦¿à¦à¦šà¦¿20130002', '2013-10-10 00:00:00', 'M', 'December', '2013', '1', '1', '4', '1', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('4', 'PAC20130002', '2013-10-01 00:00:00', 'sdfs sdfsdv &lt;br&gt;', 'May', '2002', '1', '1', '1', '1', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('1', 'PAC20130001', '2013-10-06 15:09:50', 'This is &lt;b&gt;first &lt;/b&gt;&lt;i&gt;Inquiry&lt;/i&gt;', 'May', '2007', '1', '1', '1', '1', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('2', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§§', '2013-10-06 00:00:00', 'à¦¤à¦¦à¦¨à§à¦¤ à¦¶à¦¿à¦°à§‹à¦¨à¦¾à¦®', 'October', '2013', '3', '5', '4', '2', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('7', 'PAC20130004', '2013-10-10 00:00:00', 'title 1111&lt;br&gt;', 'January', '2002', '2', '2', '1', '1', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('8', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§¨', '2013-10-21 00:00:00', 'à¦¤à¦¦à¦¨à§à¦¤ à¦¶à¦¿à¦°à§‹à¦¨à¦¾à¦®', 'May', '2014', '3', '5', '4', '2', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('10', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§ª', '2013-10-21 00:00:00', 'à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶ à¦ªà¦¾à¦°à¦¿à¦²à¦¾à¦®à§‡à¦¨à§à¦¤ à§¨à§¦à§¦à§­-à§¨à§¦à§¦à§®&lt;br&gt;', 'March', '2014', '3', '5', '4', '2', '0', '1');
INSERT INTO `tbl_inquiry_master` VALUES ('9', 'à¦ªà¦¿à¦à¦šà¦¿à§¨à§¦à§§à§©à§¦à§¦à§¦à§©', '2013-10-21 00:00:00', 'à¦¤à¦¦à¦¨à§à¦¤ à¦¶à¦¿à¦°à§‹à¦¨à¦¾à¦®', 'February', '2014', '3', '5', '4', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_ministry`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_ministry`;
CREATE TABLE `tbl_inquiry_ministry` (
  `inquiry_id` int(11) DEFAULT NULL,
  `ministry_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_ministry
-- ----------------------------
INSERT INTO `tbl_inquiry_ministry` VALUES ('5', '27', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_organization`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_organization`;
CREATE TABLE `tbl_inquiry_organization` (
  `inquiry_id` int(11) DEFAULT NULL,
  `organization_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_organization
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_inquiry_others`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_others`;
CREATE TABLE `tbl_inquiry_others` (
  `inquiry_id` int(11) DEFAULT NULL,
  `others_org_ministry_id` bigint(20) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_others
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_inquiry_other_witnessess`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_other_witnessess`;
CREATE TABLE `tbl_inquiry_other_witnessess` (
  `inquiry_id` int(11) DEFAULT NULL,
  `witness_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_other_witnessess
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_inquiry_report_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_report_master`;
CREATE TABLE `tbl_inquiry_report_master` (
  `inquiry_report_id` int(11) DEFAULT NULL,
  `report_no` varchar(30) DEFAULT NULL,
  `report_date` datetime DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `title` longtext,
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `executive_summary` longtext,
  `introduction` longtext,
  `issues` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_report_master
-- ----------------------------
INSERT INTO `tbl_inquiry_report_master` VALUES ('2', 'à§¦à§ªà§¨à§¦à§§à§©à§¦à§¦à§¦à§§', '2013-10-17 00:00:00', '10', 'Inquiry of Â à¦¬à¦¾à¦‚à¦²à¦¾à¦¦à§‡à¦¶ à¦ªà¦¾à¦°à¦¿à¦²à¦¾à¦®à§‡à¦¨à§à¦¤ à§¨à§¦à§¦à§­-à§¨à§¦à§¦à§®&lt;br&gt;', '3', '5', 'gdfgdfg&lt;br&gt;', 'sdfgs ', 'sdfgs ', '2', '4', '0', '1');
INSERT INTO `tbl_inquiry_report_master` VALUES ('1', '0120130001', '2013-10-02 00:00:00', '4', 'sdfasdf&lt;br&gt;', '1', '1', 'asdfa&lt;br&gt;', 'asdf ', 'asdf ', '1', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_report_recommendation`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_report_recommendation`;
CREATE TABLE `tbl_inquiry_report_recommendation` (
  `inquiry_report_id` int(11) DEFAULT NULL,
  `recom_id` int(11) DEFAULT NULL,
  `recom_no` varchar(50) DEFAULT NULL,
  `recommendation` longtext,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_report_recommendation
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_inquiry_scheduling`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_scheduling`;
CREATE TABLE `tbl_inquiry_scheduling` (
  `schedule_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT '0',
  `schedule_no` smallint(6) DEFAULT '0',
  `schedule_date` datetime DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_scheduling
-- ----------------------------
INSERT INTO `tbl_inquiry_scheduling` VALUES ('1', '4', '1', '2013-10-03 00:00:00', '2013-10-02 12:39:13', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('2', '4', '2', '2014-10-03 00:00:00', '2013-10-02 12:44:32', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('3', '4', '3', '2013-11-02 00:00:00', '2013-10-02 12:46:26', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('4', '5', '1', '2013-10-21 00:00:00', '2013-10-10 10:21:03', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('5', '5', '2', '2013-10-09 00:00:00', '2013-10-20 13:59:59', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('6', '7', '1', '2013-10-02 00:00:00', '2013-10-20 14:51:10', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('7', '7', '2', '2013-10-09 00:00:00', '2013-10-20 14:51:10', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('8', '7', '3', '2013-11-02 00:00:00', '2013-10-20 14:53:18', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('9', '5', '3', '2013-10-02 00:00:00', '2013-10-20 14:57:25', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('10', '5', '4', '2013-11-14 00:00:00', '2013-10-20 14:57:25', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('11', '7', '4', '2013-11-02 00:00:00', '2013-10-20 14:58:35', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('12', '2', '1', '2014-10-03 00:00:00', '2013-10-21 12:02:15', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('13', '8', '1', '2014-10-03 00:00:00', '2013-10-21 12:25:32', '0', '1');
INSERT INTO `tbl_inquiry_scheduling` VALUES ('14', '9', '1', '2014-10-10 00:00:00', '2013-10-21 12:30:40', '0', '1');

-- ----------------------------
-- Table structure for `tbl_inquiry_witnesses`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_inquiry_witnesses`;
CREATE TABLE `tbl_inquiry_witnesses` (
  `inquiry_id` int(11) DEFAULT NULL,
  `witness_type` varchar(1) DEFAULT NULL,
  `witness_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_inquiry_witnesses
-- ----------------------------

-- ----------------------------
-- Table structure for `tbl_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_invitees`;
CREATE TABLE `tbl_invitees` (
  `invitees_id` bigint(20) DEFAULT NULL,
  `invitees_name` varchar(150) DEFAULT NULL,
  `invitees_organization` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_invitees
-- ----------------------------
INSERT INTO `tbl_invitees` VALUES ('1', 'Invites-1', 'XYZ', 'Desg1', '01190382204', '1546546', '11112', 'sa@y.com', 'qqqqwqw																							', '				   				   				   				   				   				   cccc																								', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('2', 'Invites-2', 'XYZ', 'Desg2', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('3', 'Invites-3', 'XYZ', 'Desg3', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('4', 'Invites-4', 'XYZ', 'Desg4', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('5', 'Invites-5', 'XYZ', 'Desg5', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('6', 'Invites-6', 'XYZ', 'Desg6', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('7', 'Invites-7', 'XYZ', 'Desg7', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('8', 'Invites-8', 'XYZ', 'Desg8', '1', '1', '1', '1', '12', '1', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('9', 'invitees7', 'i', 'i', '', '', '', '', '				', '				', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('10', 'invitees111', 'wwwww', '', '', '', '', '', '				  								', '				   								', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('11', 'invitees111', '1', '1', '1', '1', '', '', '				', '				', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('12', 'invitees10', '1', '1', '1', '1', '1', '1', '	1			', '		1		', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('13', 'invitees11', '1', '1', '1', '', '', '', '				', '				', '1', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('14', 'asdsadas', 'adasd', 'asdasd', 'dasd', '42334', 'daasd', '', '	dasdasdasd			', '				', '2', '0', '1');
INSERT INTO `tbl_invitees` VALUES ('15', 'Pijush Sarkar', 'nhdfkjdsh', 'hswdh', '', '01817011024', '', 'pijushsk@yahoo.com', 'sds				  				', '				   				', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_language`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_language`;
CREATE TABLE `tbl_language` (
  `language_id` smallint(6) DEFAULT NULL,
  `language` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_language
-- ----------------------------
INSERT INTO `tbl_language` VALUES ('1', 'English');
INSERT INTO `tbl_language` VALUES ('2', 'Bangla');

-- ----------------------------
-- Table structure for `tbl_logistic_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_logistic_member`;
CREATE TABLE `tbl_logistic_member` (
  `logistic_member_id` smallint(6) DEFAULT NULL,
  `logistic_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_logistic_member
-- ----------------------------
INSERT INTO `tbl_logistic_member` VALUES ('2', 'logistic', 'ddd', '', '', '', '', 'S', '																																								', '1', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('3', 'dcadasd', 'adas', 'asdasd', 'asdas', 'adas', 'dasd', '	dasd							', '		adasda						', '2', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('1', 'Logistic-1', 'dd', '', '', '', '', '																					ddd																			', '												dddd																												', '1', '0', '1');
INSERT INTO `tbl_logistic_member` VALUES ('4', 'dfdsfdsa', 'dfdsfsdf', '46464646465', '646464646', '6464646464646465', 'dfsdfsdfsdf@464646465', 'fsfsdfsd', 'fsdfdsfsdfsdfsd', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_committee`;
CREATE TABLE `tbl_meeting_notice_committee` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `member_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_committee
-- ----------------------------
INSERT INTO `tbl_meeting_notice_committee` VALUES ('4', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('3', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('5', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('1', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('6', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('7', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('7', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('8', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('8', '10', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('9', '10', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('10', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('10', '10', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('11', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('11', '10', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('12', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_committee` VALUES ('12', '10', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_inquiry_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_inquiry_tag`;
CREATE TABLE `tbl_meeting_notice_inquiry_tag` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_inquiry_tag
-- ----------------------------
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('1', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('2', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('3', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('4', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('5', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('6', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('7', '5', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('8', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('9', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('10', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('11', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_inquiry_tag` VALUES ('12', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_invitees`;
CREATE TABLE `tbl_meeting_notice_invitees` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `invitees_id` bigint(20) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_invitees
-- ----------------------------
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('4', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('3', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('1', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('3', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('2', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('4', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('5', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('6', '3', '0', '1');
INSERT INTO `tbl_meeting_notice_invitees` VALUES ('6', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_logistic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_logistic`;
CREATE TABLE `tbl_meeting_notice_logistic` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `logistic_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_logistic
-- ----------------------------
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('2', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('3', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('4', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_meeting_notice_logistic` VALUES ('6', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_master`;
CREATE TABLE `tbl_meeting_notice_master` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `gov_no_pix` varchar(50) DEFAULT NULL,
  `gov_no_postfix` varchar(10) DEFAULT NULL,
  `sitting_no` varchar(50) DEFAULT NULL,
  `en_date` datetime DEFAULT NULL,
  `bn_date` varchar(250) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `venue_id` smallint(6) DEFAULT NULL,
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `private_business_before` longtext,
  `public_business` longtext,
  `private_business_after` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `status` varchar(100) DEFAULT NULL,
  `is_approved` smallint(6) DEFAULT NULL,
  `approved_date` datetime DEFAULT NULL,
  `is_issued` smallint(6) DEFAULT NULL,
  `issued_date` datetime DEFAULT NULL,
  `is_cancel` smallint(6) DEFAULT NULL,
  `cancel_date` datetime DEFAULT NULL,
  `is_adjourned` smallint(6) DEFAULT NULL,
  `adjourned_date` datetime DEFAULT NULL,
  `is_revised` smallint(6) DEFAULT NULL,
  `revised_date` datetime DEFAULT NULL,
  `is_display` smallint(6) DEFAULT NULL,
  `is_complete` smallint(6) DEFAULT NULL,
  `complete_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_master
-- ----------------------------
INSERT INTO `tbl_meeting_notice_master` VALUES ('1', '11.00.0000.731.01.021.13', '01424', '1', '2013-10-09 00:00:00', 'à§¨à§ª à¦†à¦¶à§à¦¬à¦¿à¦¨ à§§à§ªà§¨à§¦', '10:45PM', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'NEW', '0', null, '0', null, '0', null, '0', null, '0', null, '0', '0', null, '0', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('2', '11.00.0000.731.01.021.13', '01424', '1/1', '2013-10-09 00:00:00', 'à§¨à§ª à¦†à¦¶à§à¦¬à¦¿à¦¨ à§§à§ªà§¨à§¦', '10:45PM', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-10-22 14:43:12', '0', null, '0', '0', null, '0', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('3', '11.00.0000.731.01.021.13', '01424', '1/2', '2013-10-09 00:00:00', 'à§¨à§ª à¦†à¦¶à§à¦¬à¦¿à¦¨ à§§à§ªà§¨à§¦', '10:45PM', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-10-22 14:44:16', '0', null, '0', '0', null, '0', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('4', '11.00.0000.731.01.021.13', '01424', '1/2', '2013-10-10 00:00:00', 'BANGLA', '10:10am', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'ISSUED', '1', '2013-10-22 14:47:34', '1', '2013-10-22 14:47:38', '0', null, '0', null, '1', '2013-10-22 14:46:17', '0', '0', null, '0', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('5', '11.00.0000.731.01.021.13', '01424', '1/2', '2013-10-10 00:00:00', 'BANGLA', '10:10am', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'ISSUED', '1', '2013-10-22 14:47:47', '1', '2013-10-22 14:47:56', '0', null, '0', null, '1', '2013-10-22 14:47:42', '0', '0', null, '0', '1');
INSERT INTO `tbl_meeting_notice_master` VALUES ('6', '11.00.0000.731.01.021.13', '01424', '1/5', '2013-10-10 00:00:00', 'BANGLA', '10:10am', '2', '4', 'Private', 'Public', 'Private', '1', '1', 'ADJOURNED', '0', null, '0', null, '0', null, '1', '2013-10-22 14:59:37', '0', null, '1', '0', null, '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_master_old`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_master_old`;
CREATE TABLE `tbl_meeting_notice_master_old` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `gov_no_pix` varchar(50) DEFAULT NULL,
  `gov_no_postfix` varchar(10) DEFAULT NULL,
  `sitting_no` varchar(50) DEFAULT NULL,
  `en_date` datetime DEFAULT NULL,
  `bn_date` varchar(250) DEFAULT NULL,
  `time` varchar(50) DEFAULT NULL,
  `venue_id` smallint(6) DEFAULT NULL,
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `private_business_before` longtext,
  `public_business` longtext,
  `private_business_after` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `is_cancel_adjourn` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `is_status` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_master_old
-- ----------------------------
INSERT INTO `tbl_meeting_notice_master_old` VALUES ('1', '11.00.0000.731.01.021.13', '01', '1', '2013-10-10 00:00:00', 'à§¨à§« à¦†à¦¶à§à¦¬à¦¿à¦¨ à§§à§ªà§¨à§¦', '22:00', '2', '0', '', '', '', '1', '0', '0', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_master_old` VALUES ('2', '11.00.0000.731.01.021.13', '22', '1', '2013-10-18 00:00:00', 'à§© à¦•à¦¾à¦°à§à¦¤à¦¿à¦• à§§à§ªà§¨à§¦', '12:00 PM', '1', '3', '', '', '', '1', '0', '0', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_notification`;
CREATE TABLE `tbl_meeting_notice_notification` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `notification_id` int(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_notification
-- ----------------------------
INSERT INTO `tbl_meeting_notice_notification` VALUES ('2', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('3', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('4', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_meeting_notice_notification` VALUES ('6', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_meeting_notice_witness`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_meeting_notice_witness`;
CREATE TABLE `tbl_meeting_notice_witness` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `witness_id` int(6) DEFAULT NULL,
  `witness_type` char(1) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_meeting_notice_witness
-- ----------------------------
INSERT INTO `tbl_meeting_notice_witness` VALUES ('2', '5', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('2', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('3', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('1', '5', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('3', '5', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('1', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('4', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('4', '5', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('5', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('5', '5', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('6', '6', 'M', '0', '1');
INSERT INTO `tbl_meeting_notice_witness` VALUES ('6', '5', 'M', '0', '1');

-- ----------------------------
-- Table structure for `tbl_menu`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_menu`;
CREATE TABLE `tbl_menu` (
  `menu_id` int(11) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `sl_no` int(11) DEFAULT NULL,
  `en_menu_name` varchar(255) DEFAULT NULL,
  `bn_menu_name` varchar(255) DEFAULT NULL,
  `menu_link` text
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_menu
-- ----------------------------
INSERT INTO `tbl_menu` VALUES ('1', '0', '1', 'System', 'à¦¸à¦¿à¦¸à§à¦Ÿà§‡à¦®', null);
INSERT INTO `tbl_menu` VALUES ('2', '0', '2', 'Security', 'à¦¨à¦¿à¦°à¦¾à¦ªà¦¤à§à¦¤à¦¾', null);
INSERT INTO `tbl_menu` VALUES ('3', '0', '3', 'Document Database', 'à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦¡à¦¾à¦Ÿà¦¾à¦¬à§‡à¦¸', null);
INSERT INTO `tbl_menu` VALUES ('4', '0', '4', 'Inquiry', 'à¦¤à¦¦à¦¨à§à¦¤', null);
INSERT INTO `tbl_menu` VALUES ('5', '4', '4', 'Sitting ', 'à¦¸à¦­à¦¾', null);
INSERT INTO `tbl_menu` VALUES ('6', '1', '1', 'Parliament', 'à¦¸à¦‚à¦¸à¦¦', 'parliament.php');
INSERT INTO `tbl_menu` VALUES ('7', '1', '2', 'Session', 'à¦…à¦§à¦¿à¦¬à§‡à¦¶à¦¨', 'session.php');
INSERT INTO `tbl_menu` VALUES ('8', '1', '3', 'Committee', 'à¦•à¦®à¦¿à¦Ÿà¦¿', 'committee.php');
INSERT INTO `tbl_menu` VALUES ('9', '1', '4', 'Sub Committee', 'à¦¸à¦¾à¦¬ à¦•à¦®à¦¿à¦Ÿà¦¿', 'sub_committee.php');
INSERT INTO `tbl_menu` VALUES ('10', '1', '6', 'Ministry', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼', 'ministry.php');
INSERT INTO `tbl_menu` VALUES ('11', '1', '7', 'Ministry Member', 'à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼ à¦¸à¦¦à¦¸à§à¦¯', 'ministry_member.php');
INSERT INTO `tbl_menu` VALUES ('12', '1', '8', 'Organization', 'à¦¸à¦‚à¦¸à§à¦¥à¦¾', 'organization.php');
INSERT INTO `tbl_menu` VALUES ('13', '1', '9', 'Organization Member', 'à¦¸à¦‚à¦¸à§à¦¥à¦¾ à¦¸à¦¦à¦¸à§à¦¯', 'organization_member.php');
INSERT INTO `tbl_menu` VALUES ('14', '1', '10', 'Venue', 'à¦®à¦¿à¦²à¦¨à¦¸à§à¦¥à¦¨', 'venue.php');
INSERT INTO `tbl_menu` VALUES ('15', '1', '11', 'Notification Member', 'à¦…à¦¬à¦¹à¦¿à¦¤ à¦¸à¦¦à¦¸à§à¦¯', 'notification_member.php');
INSERT INTO `tbl_menu` VALUES ('16', '1', '12', 'Logistic Member', 'à¦²à¦œà¦¿à¦¸à§à¦Ÿà¦¿à¦• à¦¸à¦¦à¦¸à§à¦¯', 'logistic_member.php');
INSERT INTO `tbl_menu` VALUES ('21', '2', '1', 'User', 'à¦¬à§à¦¯à¦¬à¦¹à¦¾à¦°à¦•à¦¾à¦°à§€', 'user_info.php');
INSERT INTO `tbl_menu` VALUES ('22', '2', '2', 'User Type', 'à¦¬à§à¦¯à¦¬à¦¹à¦¾à¦°à¦•à¦¾à¦°à§€ à¦ªà§à¦°à¦•à¦¾à¦°', 'group_info.php');
INSERT INTO `tbl_menu` VALUES ('31', '3', '1', 'Document Registration', 'à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦¨à¦¿à¦¬à¦¨à§à¦§à¦¨', 'document_registration.php');
INSERT INTO `tbl_menu` VALUES ('32', '10000', '2', 'Document Key Areas and Recommendation', ' à¦¡à¦•à§à¦®à§‡à¦¨à§à¦Ÿ à¦®à§‚à¦² à¦à¦²à¦¾à¦•à¦¾à¦¸à¦®à§‚à¦¹ à¦à¦¬à¦‚ à¦ªà§à¦°à¦¸à§à¦¤à¦¾à¦¬à¦¨à¦¾', 'document_registration_key.php');
INSERT INTO `tbl_menu` VALUES ('33', '10000', '2', 'Inquiry Scheduling', 'à¦¤à¦¦à¦¨à§à¦¤ à¦¸à¦®à¦¯à¦¼à¦¸à§‚à¦šà§€', 'inquiry_scheduling.php');
INSERT INTO `tbl_menu` VALUES ('34', '4', '1', 'Manage Inquiry', ' à¦¤à¦¦à¦¨à§à¦¤ à¦¤à§ˆà¦°à¦¿ à¦•à¦°à§à¦¨', 'create_inquiry.php');
INSERT INTO `tbl_menu` VALUES ('35', '4', '3', 'Evidence', 'à¦ªà§à¦°à¦®à¦¾à¦£ à¦—à§à¦°à¦¹à¦£', null);
INSERT INTO `tbl_menu` VALUES ('36', '25', '4', 'Inquiry Report Creation', 'à¦¤à¦¦à¦¨à§à¦¤ à¦ªà§à¦°à¦¤à¦¿à¦¬à§‡à¦¦à¦¨ à¦¤à§ˆà¦°à§€', 'inquiry_report.php');
INSERT INTO `tbl_menu` VALUES ('37', '25', '5', 'Follow Up Inquiry', 'à¦¤à¦¦à¦¨à§à¦¤ à¦…à¦—à§à¦°à¦—à¦¤à¦¿', 'follow_up.php');
INSERT INTO `tbl_menu` VALUES ('38', '5', '1', 'Manage Sitting', 'à¦¸à¦­à¦¾ à¦¨à§‹à¦Ÿà¦¿à¦¶', 'meeting_notice.php');
INSERT INTO `tbl_menu` VALUES ('39', '5', '2', 'Briefing Note', 'à¦¬à§à¦°à¦¿à¦«à¦¿à¦‚ à¦¨à§‹à¦Ÿ', 'briefing_note.php');
INSERT INTO `tbl_menu` VALUES ('40', '5', '3', 'Record of Decisions', 'à¦°à§‡à¦•à¦°à§à¦¡ à¦“ à¦¸à¦¿à¦¦à§à¦§à¦¾à¦¨à§à¦¤', 'record_decisions.php');
INSERT INTO `tbl_menu` VALUES ('41', '5', '4', 'Multimedia & Verbatim Recording', 'à¦®à¦¾à¦²à§à¦Ÿà¦¿à¦®à¦¿à¦¡à¦¿à¦¯à¦¼à¦¾ à¦“ à¦§à¦¾à¦°à¦£à¦•à§ƒà¦¤ à¦°à§‡à¦•à¦°à§à¦¡à¦¿à¦‚', 'multimedia_verbatim.php');
INSERT INTO `tbl_menu` VALUES ('20', '1', '5', 'Committee Member', 'à¦•à¦®à¦¿à¦Ÿà¦¿à¦° à¦¸à¦¦à¦¸à§à¦¯', 'committee_member.php');
INSERT INTO `tbl_menu` VALUES ('17', '1', '13', 'Others Organization/Ministry', 'à¦…à¦¨à§à¦¯à¦¾à¦¨à§à¦¯ à¦¸à¦‚à¦¸à§à¦¥à¦¾ à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼', 'others_organization_ministry.php');
INSERT INTO `tbl_menu` VALUES ('18', '1', '14', 'Others Witness', 'à¦…à¦¨à§à¦¯à¦¦à§‡à¦° à¦¸à¦¾à¦•à§à¦·à§€', 'others_witness.php');
INSERT INTO `tbl_menu` VALUES ('19', '1', '15', 'Observers', 'à¦†à¦®à¦¨à§à¦¤à§à¦°à¦£', 'invites.php');
INSERT INTO `tbl_menu` VALUES ('42', '35', '1', 'Request', 'à¦…à¦¨à§à¦°à§‹à¦§', 'request_evidence.php');
INSERT INTO `tbl_menu` VALUES ('43', '35', '2', 'Receive', 'à¦—à§à¦°à¦¹à¦£', 'receive_evidance.php');
INSERT INTO `tbl_menu` VALUES ('25', '4', '6', 'Report', 'à¦°à¦¿à¦ªà§‹à¦°à§à¦Ÿ', null);

-- ----------------------------
-- Table structure for `tbl_ministry`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ministry`;
CREATE TABLE `tbl_ministry` (
  `ministry_id` smallint(6) DEFAULT NULL,
  `ministry_name` varchar(150) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_ministry
-- ----------------------------
INSERT INTO `tbl_ministry` VALUES ('1', 'Ministry of Information &amp; Communication Technology', 'aaasas', 'dsssdsds', '1112112212121', '', '', '', '																																								', '																																								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('2', 'Ministry of Science and Technology', '', '', '', '', '', '', '																', '																', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('3', 'Ministry of Railways', '', '', '', '', '', '', '																', '																', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('4', 'Ministry of Expatriates Welfare and Overseas Employment', '44444', '', '', '', '', '', '																								', '																								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('5', 'Ministry of Liberation War Affairs', '', '', '', '', '', '', '																', '																', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('6', 'Ministry of Youth and Sports', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('7', 'Ministry of Water Resources', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('8', 'Ministry of Women and Children Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('9', 'Ministry of Social Welfare', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('10', 'Ministry of Shipping', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('11', 'Ministry of Disaster Management and Relief', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('12', 'Ministry of Religious Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('13', 'Ministry of Posts and Tele-Communications', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('14', 'Ministry of Planning (Planning Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('15', 'Ministry of Planning (Statistics and Informatics Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('16', 'Ministry of Planning (Implementation, Monitoring and Evaluation Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('17', 'Ministry of Local Government, Rural Development and Co-operatives', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('18', 'Ministry of Law, Justice and Parliamentary Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('19', 'Ministry of Labour &amp; Employment', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('20', 'Ministry of Textiles and Jute', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('21', 'Ministry of Information', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('22', 'Ministry of Industries', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('23', 'Ministry of Housing and Public Works', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('24', 'Ministry of Home Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('25', 'Ministry of Health and Family Welfare', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('26', 'Ministry of Foreign Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('27', 'Ministry of Finance (Finance Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('28', 'Ministry of Finance (Economic Relations Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('29', 'Ministry of Finance (Internal Resources Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('30', 'Ministry of Finance (Bank and Financial Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('31', 'Ministry of Fisheries and Livestock', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('32', 'Ministry of Public Administration', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('33', 'Ministry of Environment and Forest', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('34', 'Ministry of Power, Energy and Mineral Resources', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('35', 'Ministry of Education', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('36', 'Ministry of Food', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('37', 'Ministry of Defence', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('38', 'Ministry of Cultural Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('39', 'Ministry of Communications (Bridges Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('40', 'Ministry of Communications (Roads Division)', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('41', 'Ministry of Commerce', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('42', 'Ministry of Civil Aviation and Tourism', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('43', 'Ministry of Agriculture', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('44', 'Ministry of Primary and Mass Education', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('45', 'Ministry of Chittagong Hill Tracts Affairs', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('46', 'à¦°à§‡à¦²à¦ªà¦¥ - à¦®à¦¨à§à¦¤à§à¦°à¦£à¦¾à¦²à¦¯à¦¼', '', '', '', '', '', '', '								', '								', '2', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('47', 'ki ', '', '', '', '', '', '', '								', '								', '2', '1', '1');
INSERT INTO `tbl_ministry` VALUES ('48', '', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('49', '', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('50', '', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('51', 'fdfdsfdsfs', 'dfdfdsfdsfds', 'fdfdsfsd', '543', '856329', '5263287412589', 'fdfdfds@5236', 'fdfdsfdsfdsaf', '					trtrtre	trtrtr		', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('52', 'dfd', 'fdfdf', 'fdfdf', '4646', '6466', '466465465465', 'dffdsf@46465', 'dfdfdfdf								', '			fdfdfdsf					', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('53', 'fdfdsfdsfs', 'fdfdf', 'dfdfd', '546546546', '46464646546456', '4646546456465465', 'fdfdfdsf@13131313232', '								', '								', '1', '0', '1');
INSERT INTO `tbl_ministry` VALUES ('54', 'fdf', 'fddfd', 'fdfd', '46464', '', '', '', '								', '								', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_ministry_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_ministry_member`;
CREATE TABLE `tbl_ministry_member` (
  `ministry_member_id` int(11) DEFAULT NULL,
  `ministry_id` smallint(6) DEFAULT NULL,
  `ministry_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_ministry_member
-- ----------------------------
INSERT INTO `tbl_ministry_member` VALUES ('1', '1', 'fdsf', 'dsf', '01817011024', '1112121212', '2232323223', 'pijushsk@yahoo.com', '																	asaassaassa																																						', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('2', '1', 'Tanya', 'Team Lead International', '', '010101010', '', 'tkarlepach@eunoia.co.uk', 'd						', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('3', '2', 'A.H. Raihan', 'Local IT Consultant', '', '', '', '', '								', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('4', '3', 'Max B. Ford', 'Team Lead', '', '', '', '', '								', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('5', '4', 'Niall', 'International Consultant', '', '', '', '', '								', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('6', '5', 'Kaisar Ahmed', 'Project Cordinator', '', '', '', '', '								', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('7', '1', 'qqqq', 'Project Manager', '1111', '14544', '', 'sa_sarker@yahoo.com', '																																																																																																																																																																																																																																																										', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('8', '1', 'Member', 'deg', '11211212121', '1221', '1212212', 'sa_sarker10@yahoo.com', '										asaasassas						', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('9', '2', 'aas', 'ss44444444', '', '', '', '', '																																																								', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('10', '46', 'dasdas', 'dasd', '+8801731182779', '+8801731182779', 'asd', 'adas@org.com', '	ad			dasd				', '2', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('11', '0', 'fdfd', 'fdfd', '13213132', '131313', '6464313', 'fdfsd@342432', '		fdfdfd						', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('12', '0', 'fdf', 'dfdf', '464646', '464646', '64646', 'gfgdfg@1313', '			dfdfdfdf					', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('13', '0', 'fdfdfdsfdsfdfdsfdsfdsf', 'fdfwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww', '46464654654654656', '46465465465465', '4646546546546546466', 'fdfds@79798', '				fdfdfdfdsf				', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('14', '1', 'fdfdfdsfd', 'fdfddfdfdf', '4646465465456465465', '65465465465465464646', '797987913213131313', 'fdfdfdsfdsf@979879', '						fdfdfdsfdsfdsfdsfdsfsdfsdfsdfsdfsfsfdsf										', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('15', '8', 'dfdfdf', 'eeeeer', '4646546546546565', '465464665456456465', '79798798798798798', 'dsfsfsdfds@79879879', '												fdfdfdfdfdfdfd																	', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('16', '1', 'saasa', 'assa', '111', '111', '111', 's_s@y.com', '	dddd							', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('17', '3', 'dfdfdfdfds', 'uiuiuiuiuiuiuiuiu', '654654646546', '4646465465', '4646465', 'dfdfs@4646', '		fdfdfdfdfd						', '1', '0', '1');
INSERT INTO `tbl_ministry_member` VALUES ('18', '45', 'dfdf', 'dfdfdfdfdf', '98454571444', '746485477', '464654654654', 'dfdfds@46546', '				fdfdfdfdsfdfdfd									', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_notification_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_notification_member`;
CREATE TABLE `tbl_notification_member` (
  `notification_member_id` smallint(6) DEFAULT NULL,
  `notification_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_notification_member
-- ----------------------------
INSERT INTO `tbl_notification_member` VALUES ('1', 'Shek Hasina', 'Honâ€™ble Prime Minister', '94880535', '01817011024', '', 'pijushsk@yahoo.com', '																				15/A Ultingonj																												', '																				fjldfj																												', '1', '0', '1');
INSERT INTO `tbl_notification_member` VALUES ('2', 'Dr. Shirin Sharmin Chaudhury', 'Honâ€™ble Speaker', '', '', '', '', '																', '																', '1', '0', '1');
INSERT INTO `tbl_notification_member` VALUES ('3', 'gdfgd', 'yrhyr4y', '34r34f', 'r344', 't34tr34', 'r34r34r34', '		r34r34r34						', '				r34r				', '2', '0', '1');
INSERT INTO `tbl_notification_member` VALUES ('4', 'fdfsd', 'fdfsdf', '2541638', '79797997879', '4646464646466', 'fdfsdfsdfsd@4646465465465465456', '		fdfdsfsdfsdfsfsdfsdfsdfsdf						', '	ffsfs							', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_organization`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_organization`;
CREATE TABLE `tbl_organization` (
  `organization_id` smallint(6) DEFAULT NULL,
  `organization_name` varchar(150) DEFAULT NULL,
  `short_name` varchar(5) DEFAULT NULL,
  `contact_person` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_organization
-- ----------------------------
INSERT INTO `tbl_organization` VALUES ('1', 'Bangladesh Bank', 'BB', 'Mr. Gautom', 'DD', '', '', '', '', '																																', '																																', '1', '0', '1');
INSERT INTO `tbl_organization` VALUES ('2', 'BIWTA', '', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization` VALUES ('3', 'Bangladesh Telecom Regulatory Company', 'BTRC', '', '', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization` VALUES ('4', 'dfgdfg', 'gdf', 'dfg', 'dasd', 'dfg', 'dgf', 'zcfsd', 'oneof_rebel@yahoo.comreeman_67', '								', '								', '2', '0', '1');
INSERT INTO `tbl_organization` VALUES ('5', 'dfdfdfdf', 'fdfdf', 'dsdsdadadsad', 'fdfdfdf', '', '24343434324324324', '54654654654654646656', 'fdfdfdfsdfs@46465465', 'fdfdfdsfdsf								', 'fdfdfdfds							', '1', '0', '1');
INSERT INTO `tbl_organization` VALUES ('6', 'fdfdfdsfsfdsfdsfsdfsdf', 'fdfdf', 'fdfsfdfsdsfsdf', 'dfdfdsf', '', '', '', '', '								', '								', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_organization_member`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_organization_member`;
CREATE TABLE `tbl_organization_member` (
  `organization_member_id` int(11) DEFAULT NULL,
  `organization_id` smallint(6) DEFAULT NULL,
  `organization_member_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_organization_member
-- ----------------------------
INSERT INTO `tbl_organization_member` VALUES ('3', '4', 'g', 'gdf', '4534', '345', '353', '35', '	534							', '								', '2', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('4', '2', 'fdfdf', 'fdfdfd', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('5', '2', 'fdfdfsfs', 'dfdfsdffdsf', '64646466', '464646', '46464646', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('1', '1', 'Mr. Gautom', 'DD', '', '', '', '', '								ww								', '								a										', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('2', '4', 'j', 'gdf', '4534', '345', '353', '35', '					534											', '																', '2', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('6', '0', 'fdfdf', 'fdfdfd', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('7', '6', 'fdfsfsff', 'fdfsdfdsf', '', '', '', '', '								', '								', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('8', '1', 'fdfdf', 'fdfdfdfdsf', '464646546546546', '4646494932132', '4646464656546546', 'dfdfds@46546', '			fdfdfdfdsfdsfdsdfddsfdsfdfdsfdsfs					', '								', '1', '0', '1');
INSERT INTO `tbl_organization_member` VALUES ('9', '1', 'dfddsfsdf', 'fdfdfsfsdf', '464646546', '79798798798798798798', '46465666', 'fdfdfsfs@79798979', '								', '								', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_others`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_others`;
CREATE TABLE `tbl_others` (
  `others_org_ministry_id` bigint(20) DEFAULT NULL,
  `others_org_ministry` varchar(150) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_others
-- ----------------------------
INSERT INTO `tbl_others` VALUES ('1', '@', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('2', 'Other Ministry - 4', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('12', '!', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('3', 'Othet Organization-1', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('4', 'Other Organization -2 ', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('5', 'Othet Organization-3', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('6', 'Othet Organization-4', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('7', 'Other Organization-5', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('8', 'Other Ogr-6', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('9', 'Other Ministry -4', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('10', 'Other Ministry -5', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('11', 'fdfsdf', '1', '0', '1');
INSERT INTO `tbl_others` VALUES ('13', 'fdfdfdsf', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_other_witnessess`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_other_witnessess`;
CREATE TABLE `tbl_other_witnessess` (
  `witness_id` int(11) DEFAULT NULL,
  `witness_name` varchar(150) DEFAULT NULL,
  `designation` varchar(100) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `cell_phone` varchar(50) DEFAULT NULL,
  `fax` varchar(50) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `address` varchar(250) DEFAULT NULL,
  `comments` varchar(250) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_other_witnessess
-- ----------------------------
INSERT INTO `tbl_other_witnessess` VALUES ('1', 'Pijush Sarkar', 'Software Project Manager', '', '01817011024', '8129383', 'pijushsk@yahoo.com', '								ss																																	', '				  				  				  				  				  				  				  				  																																', '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('2', 'Subrata Sarker', 'Sr. Software Engineer', null, null, null, null, null, null, '1', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('3', 'sdf', 'fsdfsd', '44114', '4141', '', 'fsdf@dfdas.com', 'adasd								dasdasdasd', '				  				', '2', '0', '1');
INSERT INTO `tbl_other_witnessess` VALUES ('4', 'dfdsfds', 'fsfsdfsd', '46465465465', '4646465465465', '46464646465465456', 'dsfsdfsd@6465465465', '	fdfsdf							', '	fsdfsdf			  				', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_parliament`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_parliament`;
CREATE TABLE `tbl_parliament` (
  `parliament_id` smallint(6) DEFAULT NULL,
  `parliament` varchar(50) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_parliament
-- ----------------------------
INSERT INTO `tbl_parliament` VALUES ('1', '1st Parliament', '1', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('2', '2 nd Parliament', '1', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('3', 'à¦…à¦¨à§à¦°à§‹à¦§', '2', '0', '1');
INSERT INTO `tbl_parliament` VALUES ('4', '9th Parliament', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_receive_evidence_d`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_receive_evidence_d`;
CREATE TABLE `tbl_receive_evidence_d` (
  `inquiry_id` int(11) DEFAULT NULL,
  `rec_evidence_details_id` bigint(20) DEFAULT NULL,
  `evidence_type` varchar(100) DEFAULT NULL,
  `title` longtext,
  `submitted_by` varchar(200) DEFAULT NULL,
  `receive_date` datetime DEFAULT NULL,
  `ref_no` varchar(150) DEFAULT NULL,
  `ref_date` datetime DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `file_name` varchar(250) DEFAULT NULL,
  `doc_type` varchar(50) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_receive_evidence_d
-- ----------------------------
INSERT INTO `tbl_receive_evidence_d` VALUES ('7', '2', 'WrittenEvidence', '', '', '0000-00-00 00:00:00', null, null, null, null, null, '0', '1');

-- ----------------------------
-- Table structure for `tbl_receive_evidence_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_receive_evidence_master`;
CREATE TABLE `tbl_receive_evidence_master` (
  `inquiry_id` int(11) DEFAULT NULL,
  `receive_deadline_date` datetime DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_receive_evidence_master
-- ----------------------------
INSERT INTO `tbl_receive_evidence_master` VALUES ('1', '2013-10-15 00:00:00', '0', '1');
INSERT INTO `tbl_receive_evidence_master` VALUES ('0', '2013-10-15 00:00:00', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_committee`;
CREATE TABLE `tbl_record_of_decision_committee` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `member_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_committee
-- ----------------------------
INSERT INTO `tbl_record_of_decision_committee` VALUES ('5', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('1', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('4', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('3', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('7', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('7', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('6', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('8', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('8', '10', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('9', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('9', '10', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('10', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('10', '10', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('11', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('11', '10', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('12', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_committee` VALUES ('12', '10', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_inquiry_tag`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_inquiry_tag`;
CREATE TABLE `tbl_record_of_decision_inquiry_tag` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `status` int(2) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_inquiry_tag
-- ----------------------------
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('4', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('1', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('2', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('3', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('5', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('7', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('6', '5', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('8', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('9', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('10', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('11', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_inquiry_tag` VALUES ('12', '1', '0', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_invitees`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_invitees`;
CREATE TABLE `tbl_record_of_decision_invitees` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `invitees_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_invitees
-- ----------------------------
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('4', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('3', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('2', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('3', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('2', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('1', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('4', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('5', '3', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('6', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_invitees` VALUES ('6', '3', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_logistic`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_logistic`;
CREATE TABLE `tbl_record_of_decision_logistic` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `logistic_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_logistic
-- ----------------------------
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('4', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('3', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('2', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('1', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('5', '2', '0', '1');
INSERT INTO `tbl_record_of_decision_logistic` VALUES ('6', '2', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_master`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_master`;
CREATE TABLE `tbl_record_of_decision_master` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `chair_member_id` smallint(6) DEFAULT NULL,
  `private_business` longtext,
  `public_business` longtext,
  `result_of_deliberation` longtext,
  `language_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `flag` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_master
-- ----------------------------
INSERT INTO `tbl_record_of_decision_master` VALUES ('2', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('4', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('5', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('7', '1', '', '', '&lt;ul&gt;&lt;li&gt;&lt;br&gt;&lt;/li&gt;&lt;/ul&gt;', '1', '1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('8', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('9', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('3', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('1', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('6', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('10', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('11', '0', '', '', '', '1', '1', '0', '0', '1');
INSERT INTO `tbl_record_of_decision_master` VALUES ('12', '0', '', '', '', '1', '1', '0', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_notification`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_notification`;
CREATE TABLE `tbl_record_of_decision_notification` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `notification_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_notification
-- ----------------------------
INSERT INTO `tbl_record_of_decision_notification` VALUES ('4', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('3', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('2', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('1', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('5', '1', '0', '1');
INSERT INTO `tbl_record_of_decision_notification` VALUES ('6', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_record_of_decision_witness`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_record_of_decision_witness`;
CREATE TABLE `tbl_record_of_decision_witness` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `witness_type` varchar(1) DEFAULT NULL,
  `witness_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_record_of_decision_witness
-- ----------------------------
INSERT INTO `tbl_record_of_decision_witness` VALUES ('3', 'M', '6', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('2', 'M', '6', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('2', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('1', 'M', '6', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('1', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('3', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('4', 'M', '6', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('4', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('5', 'M', '6', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('5', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('6', 'M', '5', '0', '1');
INSERT INTO `tbl_record_of_decision_witness` VALUES ('6', 'M', '6', '0', '1');

-- ----------------------------
-- Table structure for `tbl_request_evidence`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_request_evidence`;
CREATE TABLE `tbl_request_evidence` (
  `request_evidence_id` int(11) DEFAULT NULL,
  `inquiry_id` int(11) DEFAULT NULL,
  `metting_notice_id` int(11) DEFAULT NULL,
  `request_date` datetime DEFAULT NULL,
  `cover_letter` longtext,
  `committee_id` smallint(6) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `entry_date` datetime DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_request_evidence
-- ----------------------------
INSERT INTO `tbl_request_evidence` VALUES ('1', '5', '0', '2013-10-07 00:00:00', '			    465464			  ', '1', '1', '0', '2013-10-07 13:04:24', '1');
INSERT INTO `tbl_request_evidence` VALUES ('2', '1', '0', '2013-10-14 00:00:00', '			    &lt;b&gt;6546464			  &lt;/b&gt;', '1', '1', '0', '2013-10-07 13:04:27', '1');
INSERT INTO `tbl_request_evidence` VALUES ('3', '1', '0', '2013-10-09 00:00:00', '			    &lt;i&gt;&lt;b&gt;uuuuuui			\r\n			  			  &lt;/b&gt;&lt;/i&gt;', '1', '1', '0', '2013-10-07 15:06:55', '1');
INSERT INTO `tbl_request_evidence` VALUES ('4', '1', '0', '2013-10-17 00:00:00', 'ghjghj			    			  ', '1', '1', '0', '2013-10-20 16:44:02', '1');

-- ----------------------------
-- Table structure for `tbl_session`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_session`;
CREATE TABLE `tbl_session` (
  `parliament_id` smallint(6) DEFAULT NULL,
  `session_id` smallint(6) DEFAULT NULL,
  `session` varchar(50) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_session
-- ----------------------------
INSERT INTO `tbl_session` VALUES ('1', '1', '1st Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('2', '2', '1st session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('2', '3', '2nd session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('2', '4', '3rd session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('3', '5', '1st Session', '2', '0', '1');
INSERT INTO `tbl_session` VALUES ('4', '6', '1st Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('4', '7', '3rd Session', '1', '0', '1');
INSERT INTO `tbl_session` VALUES ('4', '8', '2nd Session', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_sub_committee`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sub_committee`;
CREATE TABLE `tbl_sub_committee` (
  `sub_committee_id` smallint(6) DEFAULT NULL,
  `committee_id` smallint(6) DEFAULT NULL,
  `sub_committee_name` varchar(150) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_sub_committee
-- ----------------------------
INSERT INTO `tbl_sub_committee` VALUES ('11', '4', 'r24312fcw2r34', '2', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('10', '4', 'dasdasdasdasd', '2', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('12', '5', '12', '2', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('13', '2', 'fdfdsfsewrer', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('1', '3', 'PUC Committee - 1', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('2', '3', 'PUC Committee - 2', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('3', '1', 'PAC Sub Committee -1 ', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('4', '1', 'PAC Sub Committee - 2', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('5', '1', 'PAC Sub Committee - 3', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('6', '1', 'PAC Sub Committee - 4', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('7', '2', 'PEC Committee - 1', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('8', '2', 'PEC Committee - 2', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('9', '2', 'PEC Committee - 3', '1', '0', '1');
INSERT INTO `tbl_sub_committee` VALUES ('14', '2', 'rwr222222', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_transcript`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_transcript`;
CREATE TABLE `tbl_transcript` (
  `metting_notice_id` int(11) DEFAULT NULL,
  `transcript_id` bigint(20) DEFAULT NULL,
  `file_title` varchar(200) DEFAULT NULL,
  `file_type` varchar(20) DEFAULT NULL,
  `file_path` varchar(400) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_transcript
-- ----------------------------
INSERT INTO `tbl_transcript` VALUES ('1', '1', 'File Name1', 'audio/mpeg', '../upload/1.mp3', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('1', '2', 'cv', 'image/jpeg', '../upload/2.jpg', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('9', '3', 'cv', 'text/javascript', '../upload/3.js', '0', '1');
INSERT INTO `tbl_transcript` VALUES ('1', '4', 'hgth', 'video/mp4', '../upload/4.mp4', '0', '1');

-- ----------------------------
-- Table structure for `tbl_user`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user`;
CREATE TABLE `tbl_user` (
  `user_id` int(11) DEFAULT NULL,
  `user_name` varchar(128) DEFAULT NULL,
  `password` varchar(128) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_active` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user
-- ----------------------------
INSERT INTO `tbl_user` VALUES ('1', 'admin', 'a1', '2013-10-23 11:09:04', '1');
INSERT INTO `tbl_user` VALUES ('2', 'bipl', 'b1', '2013-09-09 13:24:33', '1');
INSERT INTO `tbl_user` VALUES ('3', 'v', 'v', '2013-09-19 15:56:45', '1');
INSERT INTO `tbl_user` VALUES ('4', 'vv', 'vv', '2013-09-12 17:52:04', '1');
INSERT INTO `tbl_user` VALUES ('5', 'dfdfdsfdsfdsfdsf', '524163', '2013-09-22 14:49:24', '1');
INSERT INTO `tbl_user` VALUES ('6', 'fdfsdfsdfsd', '555555555555555555555555555555', '2013-09-22 14:50:23', '1');
INSERT INTO `tbl_user` VALUES ('7', 'fdfsdfdsfdsf', '5263142555555555555555555555555555555555', '2013-09-22 14:51:02', '1');
INSERT INTO `tbl_user` VALUES ('8', 'fdfsdfdsfdfd', '5214785222222222222222222', '2013-09-22 14:52:13', '1');

-- ----------------------------
-- Table structure for `tbl_user_committee_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_committee_permission`;
CREATE TABLE `tbl_user_committee_permission` (
  `committee_id` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_committee_permission
-- ----------------------------
INSERT INTO `tbl_user_committee_permission` VALUES ('1', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('4', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('5', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('2', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('3', '1');
INSERT INTO `tbl_user_committee_permission` VALUES ('6', '1');

-- ----------------------------
-- Table structure for `tbl_user_group_permission`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_user_group_permission`;
CREATE TABLE `tbl_user_group_permission` (
  `group_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_user_group_permission
-- ----------------------------
INSERT INTO `tbl_user_group_permission` VALUES ('1', '1');

-- ----------------------------
-- Table structure for `tbl_venue`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_venue`;
CREATE TABLE `tbl_venue` (
  `venue_id` smallint(6) DEFAULT NULL,
  `venue_name` varchar(200) DEFAULT NULL,
  `language_id` smallint(6) DEFAULT NULL,
  `is_delete` smallint(6) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_venue
-- ----------------------------
INSERT INTO `tbl_venue` VALUES ('2', 'Conferance Room No # 2', '1', '0', '1');
INSERT INTO `tbl_venue` VALUES ('1', 'Conferance Room No # 1', '1', '0', '1');
INSERT INTO `tbl_venue` VALUES ('3', 'ccc1', '2', '0', '1');
INSERT INTO `tbl_venue` VALUES ('4', 'ccc1', '2', '0', '1');
INSERT INTO `tbl_venue` VALUES ('5', 'Cabinet Room', '1', '0', '1');
INSERT INTO `tbl_venue` VALUES ('6', 'fgdfgdgdgdg', '1', '0', '1');
INSERT INTO `tbl_venue` VALUES ('7', 'dfdfd', '1', '0', '1');

-- ----------------------------
-- Table structure for `tbl_year_info`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_year_info`;
CREATE TABLE `tbl_year_info` (
  `year_id` int(11) DEFAULT NULL,
  `year_name` varchar(16) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tbl_year_info
-- ----------------------------
INSERT INTO `tbl_year_info` VALUES ('2001', '2001');
INSERT INTO `tbl_year_info` VALUES ('2002', '2002');
INSERT INTO `tbl_year_info` VALUES ('2003', '2003');
INSERT INTO `tbl_year_info` VALUES ('2004', '2004');
INSERT INTO `tbl_year_info` VALUES ('2005', '2005');
INSERT INTO `tbl_year_info` VALUES ('2006', '2006');
INSERT INTO `tbl_year_info` VALUES ('2007', '2007');
INSERT INTO `tbl_year_info` VALUES ('2008', '2008');
INSERT INTO `tbl_year_info` VALUES ('2009', '2009');
INSERT INTO `tbl_year_info` VALUES ('2010', '2010');
INSERT INTO `tbl_year_info` VALUES ('2011', '2011');
INSERT INTO `tbl_year_info` VALUES ('2012', '2012');
INSERT INTO `tbl_year_info` VALUES ('2013', '2013');
INSERT INTO `tbl_year_info` VALUES ('2014', '2014');
INSERT INTO `tbl_year_info` VALUES ('2015', '2015');
INSERT INTO `tbl_year_info` VALUES ('2016', '2016');
INSERT INTO `tbl_year_info` VALUES ('2017', '2017');
INSERT INTO `tbl_year_info` VALUES ('2018', '2018');
INSERT INTO `tbl_year_info` VALUES ('2019', '2019');
INSERT INTO `tbl_year_info` VALUES ('2020', '2020');
INSERT INTO `tbl_year_info` VALUES ('2021', '2021');
INSERT INTO `tbl_year_info` VALUES ('2022', '2022');
INSERT INTO `tbl_year_info` VALUES ('2023', '2023');
INSERT INTO `tbl_year_info` VALUES ('2024', '2024');

-- ----------------------------
-- Procedure structure for `rpt_brifeing_note`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_brifeing_note`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_brifeing_note`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
		SELECT
		c.committee_name,
		DATE_FORMAT(mn.en_date, '%d-%m-%Y') en_date,
		bn.introduction,
		bn.key_issue,
		bn.advisor_question,
		bn.witness_question,
		bn.written_evidence
		FROM tbl_briefing_note bn
		INNER JOIN tbl_meeting_notice_master mn ON bn.metting_notice_id = mn.metting_notice_id
		INNER JOIN tbl_committee c ON mn.committee_id = c.committee_id		
		where mn.metting_notice_id=p_meeting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_follow_up`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_follow_up`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_follow_up`(p_inquiry_report_followup_id int)
BEGIN
	#Routine body goes here...
	SELECT 	i.inquiry_no, 
					i.inquiry_title, 
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date,
					m.report_no,
					DATE_FORMAT(m.report_date,'%d-%m-%Y') report_date,
					m.title,
					m.executive_summary,
					f.background,
					DATE_FORMAT(f.followup_date,'%d-%m-%Y') followup_date,
					f.followup_no,					
					r.recommendation,
					r.organization_response,
					r.action_taken,
					r.comment,
					m.introduction,
					m.issues,
					c.committee_name
	FROM tbl_follow_up_master f
	INNER JOIN tbl_inquiry_report_master m ON m.inquiry_report_id = f.inquiry_report_id AND m.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_follow_up_recommendation r ON r.inquiry_report_followup_id = f.inquiry_report_followup_id AND r.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = f.committee_id AND c.is_delete = 0
	WHERE f.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND f.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_inquiry_report`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_inquiry_report`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_inquiry_report`(p_inquiry_report_id int)
BEGIN
	#Routine body goes here...
	SELECT 	i.inquiry_no, 
					i.inquiry_title, 
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date,
					m.report_no,
					DATE_FORMAT(m.report_date,'%d-%m-%Y') report_date,
					m.title,
					m.executive_summary,
					r.recommendation,
					r.recom_no,
					m.introduction,
					m.issues,
					c.committee_name
	FROM tbl_inquiry_report_master m
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	LEFT OUTER JOIN tbl_inquiry_report_recommendation r ON r.inquiry_report_id = m.inquiry_report_id AND r.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE m.inquiry_report_id = p_inquiry_report_id
	AND m.is_delete = 0
	ORDER BY r.recom_no ASC;
	#select * from tbl_inquiry_report_master m where m.committee_id
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_meeting_notice`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_meeting_notice`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_meeting_notice`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
SELECT
c.committee_name,
mn.en_date,
mn.bn_date,
mn.time,
v.venue_name,
mn.private_business_before,
mn.public_business,
mn.private_business_after
FROM tbl_meeting_notice_master mn INNER JOIN tbl_committee c ON c.committee_id = mn.committee_id 
INNER JOIN tbl_venue v ON v.venue_id=mn.venue_id
WHERE mn.metting_notice_id=p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `rpt_record_of_decision`
-- ----------------------------
DROP PROCEDURE IF EXISTS `rpt_record_of_decision`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `rpt_record_of_decision`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
	DROP TEMPORARY TABLE IF EXISTS member_pre;
	CREATE TEMPORARY TABLE member_pre AS
	(
		SELECT 	c.metting_notice_id, 
						m.member_name 
		FROM tbl_record_of_decision_committee c
		INNER JOIN tbl_committee_member m ON m.committee_member_id = c.member_id AND m.is_delete = 0
		WHERE c.metting_notice_id = p_meeting_notice_id
		AND c.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS logistic_pre;
	CREATE TEMPORARY TABLE logistic_pre AS
	(
		SELECT 	c.metting_notice_id, 
						m.logistic_member_name
		FROM tbl_record_of_decision_logistic c
		INNER JOIN tbl_logistic_member m ON m.logistic_member_id = c.logistic_id AND m.is_delete = 0
		WHERE c.metting_notice_id = p_meeting_notice_id
		AND c.is_delete = 0
	);	

	DROP TEMPORARY TABLE IF EXISTS witness_pre;

	CREATE TEMPORARY TABLE witness_pre AS
	(
		SELECT t.metting_notice_id, m.ministry_member_name member_name
		FROM tbl_record_of_decision_witness t		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.metting_notice_id = p_meeting_notice_id
	);

	INSERT INTO witness_pre
	SELECT t.metting_notice_id, o.organization_member_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_meeting_notice_id;

	INSERT INTO witness_pre
	SELECT t.metting_notice_id, o.witness_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_meeting_notice_id;


	
	SELECT 	DATE_FORMAT(n.en_date, '%d-%m-%Y') en_date, 
					n.bn_date, 
					n.time, 
					v.venue_name,
					cm.member_name chairman_name,
					(SELECT GROUP_CONCAT(member_name) FROM member_pre) member_name,
					(SELECT GROUP_CONCAT(logistic_member_name) FROM logistic_pre) logistic_member_name,
					(SELECT GROUP_CONCAT(member_name) FROM witness_pre) witness_member_name,
					m.private_business,
					m.public_business,
					m.result_of_deliberation,
					c.committee_name
	FROM tbl_record_of_decision_master m
	INNER JOIN tbl_meeting_notice_master n ON n.metting_notice_id = m.metting_notice_id AND n.is_delete = 0
	INNER JOIN tbl_venue v ON v.venue_id = n.venue_id AND v.is_delete = 0
	INNER JOIN tbl_committee_member cm ON cm.committee_member_id = m.chair_member_id AND cm.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE m.metting_notice_id = p_meeting_notice_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_briefing_note
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_briefing_note t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.committee_id = p_committee_id
		AND t.language_id = p_language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, introduction, key_issue, advisor_question, witness_question, written_evidence, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_gid`(p_metting_notice_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id
	FROM tbl_briefing_note t
	INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_i`(p_metting_notice_id int , p_introduction longtext , p_key_issue longtext , p_advisor_question longtext , p_witness_question longtext , p_written_evidence longtext , p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_briefing_note c WHERE c.metting_notice_id = p_metting_notice_id) THEN		

		INSERT INTO tbl_briefing_note(metting_notice_id, introduction, key_issue, advisor_question, witness_question, written_evidence, language_id, committee_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_introduction, p_key_issue, p_advisor_question, p_witness_question, p_written_evidence, p_language_id, p_committee_id, 0, p_user_id);
	ELSE
		UPDATE tbl_briefing_note
		SET introduction = p_introduction, 
				key_issue = p_key_issue, 
				advisor_question = p_advisor_question, 
				witness_question = p_witness_question, 
				written_evidence = p_written_evidence, 
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_briefing_note_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_briefing_note_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_briefing_note_search`(p_start_date varchar(10), p_end_date varchar(10), p_ref_no varchar(100), p_sub_committee int, p_sitting_no varchar(100), p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.metting_notice_id, 
						m.sitting_no, 
						CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) ref_no, 
						DATE_FORMAT(m.en_date,'%d-%m-%Y') en_date, 
						m.bn_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						CONCAT("<input type='button' onClick='popupwinid(",m.metting_notice_id, ");' name='basic' value='Briefing Note' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='printid(",m.metting_notice_id, ");' name='basic' value='Print' class='gridbutton'/>") is_delete
		FROM tbl_meeting_notice_master m
		INNER JOIN tbl_meeting_notice_inquiry_tag t ON t.metting_notice_id = m.metting_notice_id AND t.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE m.committee_id = p_committee_id
		AND m.language_id = p_language_id
		AND (p_sub_committee = '' OR p_sub_committee = 0 OR m.sub_committee_id = p_sub_committee)
		AND (p_ref_no = '' OR CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_sitting_no = '' OR m.sitting_no LIKE CONCAT('%',p_sitting_no,'%'))
		AND (p_start_date='' OR p_end_date='' OR (m.en_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		ORDER BY m.en_date DESC
		/*
		SELECT t.metting_notice_id, t.introduction, t.key_issue, t.advisor_question, t.witness_question, t.written_evidence, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_briefing_note t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.committee_id = p_committee_id
		AND t.language_id = p_language_id
		AND t.is_delete = 0
		*/
	);
	SET @sql = CONCAT("SELECT metting_notice_id, sitting_no, ref_no, en_date, bn_date, inquiry_no, inquiry_title, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.branch_id, p.branch
	FROM tbl_committee_branch p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_d`(p_branch_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_branch
	SET is_delete = 1,
			user_id = p_user_id
	WHERE branch_id = p_branch_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.branch_id, p.branch, p.language_id, l.language, p.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",p.branch_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.branch_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_branch p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT branch_id, branch, language_id, language, is_edit,is_delete FROM temp_all ORDER BY branch_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_gid`(p_branch_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.branch_id, p.branch, p.language_id, l.language, p.user_id
	FROM tbl_committee_branch p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.branch_id = p_branch_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_branch_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_branch_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_branch_i`(p_branch_id smallint, p_branch varchar(100), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	IF NOT EXISTS(SELECT * FROM tbl_committee_branch c WHERE c.branch_id = p_branch_id) THEN

		SET p_branch_id = (SELECT IFNULL(MAX(branch_id), 0) + 1 FROM tbl_committee_branch);

		INSERT INTO tbl_committee_branch(branch_id, branch, language_id, is_delete, user_id)
		VALUES(p_branch_id, p_branch, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_branch
		SET branch = p_branch, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE branch_id = p_branch_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT c.committee_id, c.committee_name	
	FROM tbl_committee c 
	INNER JOIN tbl_language l ON l.language_id = c.language_id 
	INNER JOIN tbl_committee_section s ON s.section_id = c.section_id AND s.is_delete = 0
	INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id AND b.is_delete = 0
	WHERE c.language_id = p_language_id		
	AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_d`(p_committee_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE committee_id = p_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.committee_id, c.committee_name, c.short_name, c.section_id, s.section, c.branch_id, b.branch, c.contact_person, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.committee_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.committee_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		INNER JOIN tbl_committee_section s ON s.section_id = c.section_id AND s.is_delete = 0
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT committee_id, committee_name, short_name, section_id, section, branch_id, branch, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY committee_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_gid`(p_committee_id smallint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.committee_id, c.committee_name, c.short_name, c.section_id, s.section, c.branch_id, b.branch, c.contact_person, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
		FROM tbl_committee c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		INNER JOIN tbl_committee_section s ON s.section_id = c.section_id
		INNER JOIN tbl_committee_branch b ON b.branch_id = c.branch_id
		WHERE c.committee_id = p_committee_id		
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_i`(p_committee_id smallint, p_committee_name varchar(200), p_short_name varchar(50), p_section_id smallint, p_branch_id smallint, p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_committee c WHERE c.committee_id = p_committee_id) THEN

		SET p_committee_id = (SELECT IFNULL(MAX(committee_id), 0) + 1 FROM tbl_committee);

		INSERT INTO tbl_committee(committee_id, committee_name, short_name, section_id, branch_id, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_committee_id, p_committee_name, p_short_name, p_section_id, p_branch_id, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee
		SET committee_name = p_committee_name, 
				short_name = p_short_name, 
				section_id = p_section_id, 
				branch_id = p_branch_id, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = user_id
		WHERE committee_id = p_committee_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_d`(p_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE committee_member_id = p_committee_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.committee_member_id, m.committee_id, t.committee_name, m.member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.committee_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.committee_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_member m 
		INNER JOIN tbl_committee t ON t.committee_id = m.committee_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT committee_member_id, committee_id, committee_name, member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY committee_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_gid`(p_committee_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.committee_member_id, m.committee_id, t.committee_name, m.member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_committee_member m 
	INNER JOIN tbl_committee t ON t.committee_id = m.committee_id
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.committee_member_id = p_committee_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_i`(p_committee_member_id int, p_committee_id smallint , p_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_committee_member m WHERE m.committee_member_id = p_committee_member_id) THEN
		SET p_committee_member_id = (SELECT IFNULL(MAX(committee_member_id), 0) + 1 FROM tbl_committee_member);

		INSERT INTO tbl_committee_member(committee_member_id, committee_id, member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_committee_member_id, p_committee_id, p_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_member
		SET committee_id = p_committee_id, 
				member_name = p_member_name, 
				designation = p_designation,
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments,
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE committee_member_id = p_committee_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_member_token`(p_keyword varchar(20), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.committee_member_id, m.member_name
	FROM tbl_committee_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id AND c.is_delete = 0
	WHERE m.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND m.member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.section_id, p.section
	FROM tbl_committee_section p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_d`(p_section_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_committee_section
	SET is_delete = 1,
			user_id = p_user_id
	WHERE section_id = p_section_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.section_id, p.section, p.language_id, l.language, p.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",p.section_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.section_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_committee_section p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT section_id, section, language_id, language, is_edit,is_delete FROM temp_all ORDER BY section_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_gid`(p_section_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.section_id, p.section, p.language_id, l.language, p.user_id
	FROM tbl_committee_section p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.section_id = p_section_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_committee_section_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_committee_section_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_committee_section_i`(p_section_id smallint, p_section varchar(100), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	IF NOT EXISTS(SELECT * FROM tbl_committee_section c WHERE c.section_id = p_section_id) THEN

		SET p_section_id = (SELECT IFNULL(MAX(section_id), 0) + 1 FROM tbl_committee_section);

		INSERT INTO tbl_committee_section(section_id, section, language_id, is_delete, user_id)
		VALUES(p_section_id, p_section, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_committee_section
		SET section = p_section, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE section_id = p_section_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_d`(p_doc_reg_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_key_areas
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_gid`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT k.doc_reg_id, k.key_area, k.recommendations, k.result_forcasting, k.is_delete, k.user_id
	FROM tbl_document_key_areas k
	INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = k.doc_reg_id
	WHERE k.is_delete = 0
	AND k.doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_i`(p_doc_reg_id int, p_key_area longtext, p_recommendations longtext, p_result_forcasting longtext, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_document_key_areas c WHERE c.doc_reg_id = p_doc_reg_id) THEN

		INSERT INTO tbl_document_key_areas(doc_reg_id, key_area, recommendations, result_forcasting, is_delete, user_id)
		VALUES(p_doc_reg_id, p_key_area, p_recommendations, p_result_forcasting, 0, p_user_id);

	ELSE
		UPDATE tbl_document_key_areas
		SET key_area = p_key_area, 
				recommendations = p_recommendations, 
				result_forcasting = p_result_forcasting, 
				user_id = p_user_id
		WHERE doc_reg_id = p_doc_reg_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_key_areas_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_key_areas_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_key_areas_search`(p_doc_reg_no varchar(50),  p_start_date datetime, p_end_date datetime, p_doc_title varchar(256), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.doc_reg_id, p.doc_reg_no, DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') doc_reg_date, p.doc_title, p.language_id, p.committee_id, c.committee_name, l.language, p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' style='width:100px' name='basic' value='Key Area Entry' class='gridbutton'/>") is_edit,
        CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' style='width:100px' name='basic' value='Print' class='gridbutton'/>") is_print							
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_doc_reg_no = '' OR p.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND (p_doc_title = '' OR p.doc_title LIKE CONCAT('%',p_doc_title,'%'))		
		AND p.is_delete = 0
		ORDER BY p.doc_reg_date DESC
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, language_id, committee_id, committee_name, language, is_edit, is_print FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_d`(p_doc_reg_detail_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_registration_details	
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_detail_id  = p_doc_reg_detail_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_gall`(p_doc_reg_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	d.doc_reg_detail_id, 
						d.doc_reg_id, 
						d.doc_title, 
						d.file_name, 
						d.doc_type, 
						d.user_id,
						CONCAT("<input type='button' onClick='popupwinid(",d.doc_reg_detail_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deletei(",d.doc_reg_detail_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='download(",d.doc_reg_detail_id,");' name='basic' value='View' class='gridbutton'/>") is_download
		FROM tbl_document_registration_details d
		INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = d.doc_reg_id AND m.is_delete = 0
		WHERE d.doc_reg_id  = p_doc_reg_id
		AND d.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_detail_id, doc_reg_id, doc_title, file_name, doc_type, user_id, is_edit, is_delete, is_download FROM temp_all ORDER BY doc_reg_detail_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_gid`(p_doc_reg_detail_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	d.doc_reg_detail_id, 
					d.doc_reg_id, 
					d.doc_title, 
					d.file_name, 
					d.doc_type, 					
					d.user_id
	FROM tbl_document_registration_details d
	INNER JOIN tbl_document_registration_master m ON m.doc_reg_id = d.doc_reg_id AND m.is_delete = 0
	WHERE d.doc_reg_detail_id  = p_doc_reg_detail_id
	AND d.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_details_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_details_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_details_i`(p_doc_reg_detail_id bigint, p_doc_reg_id int, p_doc_title varchar(200), p_doc_type varchar(50), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_name varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = 'C:/upload/';
	IF NOT EXISTS(SELECT * FROM tbl_document_registration_details c WHERE c.doc_reg_detail_id = p_doc_reg_detail_id) THEN

		SET p_doc_reg_detail_id = (SELECT IFNULL(MAX(doc_reg_detail_id), 0) + 1 FROM tbl_document_registration_details);

		SET v_file_name = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

		INSERT INTO tbl_document_registration_details(doc_reg_detail_id, doc_reg_id, doc_title, file_name, doc_type, is_delete, user_id)
		VALUES(p_doc_reg_detail_id, p_doc_reg_id, p_doc_title, v_file_name, p_doc_type, 0, p_user_id);
	ELSE
		IF(TRIM(p_doc_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_name = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

			UPDATE tbl_document_registration_details
			SET doc_reg_id = p_doc_reg_id, 
					doc_title = p_doc_title, 
					file_name = v_file_name,
					doc_type = p_doc_type, 
					user_id = p_user_id
			WHERE doc_reg_detail_id = p_doc_reg_detail_id;  
		ELSE      
			SET v_file_name = (SELECT file_name FROM tbl_document_registration_details WHERE doc_reg_detail_id = p_doc_reg_detail_id);
			UPDATE tbl_document_registration_details
			SET doc_reg_id = p_doc_reg_id, 
					doc_title = p_doc_title,
					user_id = p_user_id
			WHERE doc_reg_detail_id = p_doc_reg_detail_id;
		END IF;
	END IF;

	SELECT p_doc_reg_detail_id doc_reg_detail_id, v_file_name file_name;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.doc_reg_id, p.doc_reg_no
	FROM tbl_document_registration_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_d`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_document_registration_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_gall`(p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	p.doc_reg_id, 
						p.doc_reg_no, 
						DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') as doc_reg_date, 
						p.doc_title, 
						p.ref_no,
						p.ref_date,
						p.create_date,
						p.language_id, 
						p.committee_id, 
						c.committee_name, 
						l.language, 
						p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
				CONCAT("<input type='button' onClick='deleteid(",p.doc_reg_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND p.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, committee_name, language, is_edit, is_delete FROM temp_all ORDER BY doc_reg_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_gid`(p_doc_reg_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	m.doc_reg_id, 
					m.doc_reg_no, 
					DATE_FORMAT(m.doc_reg_date,'%d-%m-%Y') as doc_reg_date, 
					m.doc_title, 
					m.ref_no,
					DATE_FORMAT(m.ref_date,'%d-%m-%Y') as ref_date,
					m.create_date,
					m.language_id, 
					l.`language`, 
					m.committee_id, 
					c.committee_name, 
					m.is_delete, 
					m.user_id
	FROM tbl_document_registration_master m
	INNER JOIN tbl_language l ON l.language_id = m.language_id
	INNER JOIN tbl_committee c ON c.committee_id = m.committee_id
	WHERE m.is_delete = 0
	AND m.doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_i`(p_doc_reg_id int, p_doc_reg_no varchar(50), p_doc_reg_date varchar(10), p_doc_title text, p_ref_no varchar(150), p_ref_date varchar(10), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_document_registration_master c WHERE c.doc_reg_id = p_doc_reg_id) THEN

		SET p_doc_reg_id = (SELECT IFNULL(MAX(doc_reg_id), 0) + 1 FROM tbl_document_registration_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(doc_reg_id), 0) + 1 FROM tbl_document_registration_master WHERE committee_id = p_committee_id);		

		SET p_doc_reg_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		IF(p_language_id = 2) THEN
			SET p_doc_reg_no = english_bangla_number(p_doc_reg_no);
		END IF;

		INSERT INTO tbl_document_registration_master(doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, is_delete, user_id)
		VALUES(p_doc_reg_id, p_doc_reg_no, STR_TO_DATE(p_doc_reg_date,'%d-%m-%Y'), p_doc_title, p_ref_no, STR_TO_DATE(p_ref_date,'%d-%m-%Y'), NOW(), p_language_id, p_committee_id, 0, p_user_id);

	ELSE
		UPDATE tbl_document_registration_master
		SET doc_reg_no = p_doc_reg_no, 
				doc_reg_date = STR_TO_DATE(p_doc_reg_date,'%d-%m-%Y'), 
				doc_title = p_doc_title,
				ref_no = p_ref_no,
				ref_date = STR_TO_DATE(p_ref_date,'%d-%m-%Y'), 
				language_id = p_language_id, 
				committee_id = p_committee_id, 				
				user_id = p_user_id
		WHERE doc_reg_id = p_doc_reg_id;


		#DELETE FROM tbl_document_registration_details
		#WHERE doc_reg_id = p_doc_reg_id;
	END IF;

	SELECT p_doc_reg_id doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_search`(p_doc_reg_no varchar(50),  p_start_date varchar(10), p_end_date varchar(10), p_doc_title varchar(256), p_ref_no varchar(150), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	p.doc_reg_id, 
						p.doc_reg_no, 
						DATE_FORMAT(p.doc_reg_date,'%d-%m-%Y') doc_reg_date, 
						p.doc_title, 
						p.language_id, 
						p.committee_id, 
						c.committee_name, 
						p.ref_no,
						p.ref_date,
						p.create_date,
						l.language, 
						p.user_id,
						CONCAT("<input type='button' onClick='popupwinid(",p.doc_reg_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",p.doc_reg_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_document_registration_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_doc_reg_no = '' OR p.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND (p_ref_no = '' OR p.ref_no LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_doc_title = '' OR p.doc_title LIKE CONCAT('%',p_doc_title,'%'))		
		AND (p_start_date='' OR p_end_date='' OR (p.doc_reg_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		AND p.is_delete = 0
		ORDER BY p.doc_reg_date DESC
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no, doc_reg_date, doc_title, ref_no, ref_date, create_date, language_id, committee_id, committee_name, language, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_document_registration_master_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_document_registration_master_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_document_registration_master_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.doc_reg_id, CONCAT(m.doc_reg_no,', Date:', DATE_FORMAT(m.doc_reg_date,'%d-%m-%Y')) doc_reg_no
	FROM tbl_document_registration_master m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.doc_reg_no LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	AND m.doc_reg_id NOT IN (SELECT t.doc_reg_id FROM tbl_inquiry_doc_tag t);
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_d`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_follow_up_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_report_followup_id  = p_inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_gall`(p_inquiry_report_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_report_followup_id, t.inquiry_report_id, t.followup_no, DATE_FORMAT(t.followup_date,'%d-%m-%Y') followup_date, t.background, t.language_id, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.inquiry_report_followup_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.inquiry_report_followup_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_follow_up_master t		
		WHERE t.inquiry_report_id = p_inquiry_report_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_report_followup_id, inquiry_report_id, report_no, followup_no, followup_date, background, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_followup_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_gid`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_report_followup_id, t.inquiry_report_id, i.report_no, DATE_FORMAT(i.report_date, '%d-%m-%Y') report_date ,i.title, t.followup_no, DATE_FORMAT(t.followup_date, '%d-%m-%Y') followup_date, t.background, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id
	FROM tbl_follow_up_master t
	INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = t.inquiry_report_id AND i.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_i`(p_inquiry_report_followup_id bigint , p_inquiry_report_id int , p_followup_no varchar (50) , p_followup_date varchar(10), p_background longtext , p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_follow_up_master c WHERE c.inquiry_report_followup_id = p_inquiry_report_followup_id) THEN		

		SET p_inquiry_report_followup_id = (SELECT IFNULL(MAX(inquiry_report_followup_id), 0) + 1 FROM tbl_follow_up_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_report_followup_id), 0) + 1 FROM tbl_follow_up_master WHERE committee_id = p_committee_id);
		SET p_followup_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));
		IF(p_language_id = 2) THEN
			SET p_followup_no = english_bangla_number(p_followup_no);
		END IF;

		INSERT INTO tbl_follow_up_master(inquiry_report_followup_id, inquiry_report_id, followup_no, followup_date, background, language_id, committee_id, is_delete, user_id)
		VALUES(p_inquiry_report_followup_id, p_inquiry_report_id, p_followup_no, STR_TO_DATE(p_followup_date,'%d-%m-%Y'), p_background, p_language_id, p_committee_id, 0, p_user_id);
	ELSE
		UPDATE tbl_follow_up_master
		SET inquiry_report_id = p_inquiry_report_id, 
				followup_no = p_followup_no, 
				followup_date = STR_TO_DATE(p_followup_date,'%d-%m-%Y'), 
				background = p_background, 
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = p_user_id
		WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;


		DELETE FROM tbl_follow_up_recommendation WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;
	END IF;

	SELECT p_inquiry_report_followup_id inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_master_search`(p_startdate varchar(10), p_enddate varchar(10), p_report_no varchar(100), p_parliament_id smallint, p_inquiry_no varchar(100),  p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	DECLARE v_finished INTEGER DEFAULT 0;
	#DECLARE v_inquiry_report_id INT;
	#DECLARE v_text LONGTEXT;
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_m;

	CREATE TEMPORARY TABLE temp_m AS
	(	
		SELECT t.inquiry_report_id, CONCAT("<table width='95%'><tr><td style='width:35%;'>",t.followup_no, "</td><td style='width:35%;'>", DATE_FORMAT(t.followup_date,'%d-%m-%y'), "</td><td style='width:30%;'>" ,"<input type='button' onClick='popupwinid(", CAST(t.inquiry_report_id AS CHAR(10)), ", ", CAST(t.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Change' class='gridbutton'/></td><td>","<input type='button' onClick='printid(", CAST(t.inquiry_report_followup_id AS CHAR(10)), ");' name='basic' value='Print' class='gridbutton'/></td></tr></table>") v_text
		FROM tbl_follow_up_master t					
		WHERE t.is_delete = 0
	);
	

	DROP TEMPORARY TABLE IF EXISTS temp_n;

	CREATE TEMPORARY TABLE temp_n
	(
		inquiry_report_id INT,
		v_text LONGTEXT
	);

/*
	CREATE TEMPORARY TABLE temp_n AS
	(
			SELECT inquiry_report_id, GROUP_CONCAT(v_text) v_text 
			FROM temp_m
			GROUP BY inquiry_report_id
	);
*/
	
	INSERT INTO temp_n
	SELECT inquiry_report_id, GROUP_CONCAT(v_text) v_text 
	FROM temp_m
	GROUP BY inquiry_report_id;

	UPDATE temp_n
	SET v_text = REPLACE(v_text, "</table>,<table width='95%'>","");

	#SELECT * FROM temp_n;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.inquiry_report_id, c.report_no, DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, c.inquiry_id, i.inquiry_no, c.title, c.parliament_id, p.parliament, c.session_id, s.`session`, c.executive_summary, c.introduction, c.issues, c.language_id, l.`language`, c.committee_id, b.committee_name, c.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,", 0);' name='basic' value='Add New' class='gridbutton'/>") add_new, m.v_text					
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		LEFT OUTER JOIN temp_n m ON m.inquiry_report_id = c.inquiry_report_id
		WHERE c.language_id = p_language_id		
		AND c.committee_id = p_committee_id
		AND c.is_delete = 0		
		ORDER BY c.report_date DESC		
	);
	SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, user_id, add_new, v_text FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_d`(p_inquiry_report_followup_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_follow_up_recommendation
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_report_followup_id  = p_inquiry_report_followup_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_gall`(p_inquiry_report_followup_id bigint, p_inquiry_report_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	IF(p_inquiry_report_followup_id != 0 ) THEN
		#SET @sql = NULL;	
		#DROP TEMPORARY TABLE IF EXISTS temp_all;
		#CREATE TEMPORARY TABLE temp_all AS
		#(
			SELECT t.inquiry_report_followup_id, i.inquiry_report_id, t.recommendation, t.organization_response, t.action_taken, t.comment
			FROM tbl_follow_up_recommendation t
			INNER JOIN tbl_follow_up_master i ON i.inquiry_report_followup_id = t.inquiry_report_followup_id AND i.is_delete = 0		
			WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
			AND t.is_delete = 0;
		#);
		#SET @sql = CONCAT("SELECT inquiry_report_followup_id, recommendation, organization_response, action_taken, comment, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_followup_id ");
		#PREPARE stmt FROM @sql;
		#EXECUTE stmt;
		#DEALLOCATE PREPARE stmt;
	ELSE
		SELECT 0 inquiry_report_followup_id, c.inquiry_report_id, c.recommendation recommendation, '' organization_response, '' action_taken, '' comment
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_gid`(p_inquiry_report_followup_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_report_followup_id, t.recommendation, t.organization_response, t.action_taken, t.comment, t.user_id
	FROM tbl_follow_up_recommendation t
	INNER JOIN tbl_follow_up_master i ON i.inquiry_report_followup_id = t.inquiry_report_followup_id AND i.is_delete = 0		
	WHERE t.inquiry_report_followup_id = p_inquiry_report_followup_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_follow_up_recommendation_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_follow_up_recommendation_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_follow_up_recommendation_i`(p_inquiry_report_followup_id bigint , p_recommendation longtext , p_organization_response varchar(100) , p_action_taken longtext , p_comment longtext , p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE p_recom_id BIGINT;

	IF NOT EXISTS(SELECT * FROM tbl_follow_up_recommendation c WHERE c.recom_id = p_recom_id) THEN				

		SET p_recom_id = (SELECT IFNULL(MAX(recom_id), 0) + 1 FROM tbl_follow_up_recommendation);

		INSERT INTO tbl_follow_up_recommendation(inquiry_report_followup_id, recom_id, recommendation, organization_response, action_taken, comment, is_delete, user_id)
		VALUES(p_inquiry_report_followup_id, p_recom_id, p_recommendation, p_organization_response, p_action_taken, p_comment, 0, p_user_id);

	/*
	ELSE
		UPDATE tbl_follow_up_recommendation
		SET recommendation = p_recommendation, 
				organization_response = p_organization_response, 
				action_taken = p_action_taken, 
				comment = p_comment, 
				user_id = p_user_id
		WHERE inquiry_report_followup_id = p_inquiry_report_followup_id;
	*/
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_gall`(p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(",p.group_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.group_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
					CONCAT("<input type='button' onClick='groupmenuid(",p.group_id,");' name='basic' value='Menu Permission' style='width: 150px;' class='gridbutton'/>") is_menu_permission
		FROM tbl_group p 	
	);
	SET @sql = CONCAT("SELECT group_id, group_name, is_edit, is_delete, is_menu_permission FROM temp_all ORDER BY group_name ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_gid`(p_group_id int)
BEGIN
	#Routine body goes here...
	SELECT p.group_id, p.group_name
	FROM tbl_group p
	WHERE p.group_id = p_group_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_i`(p_group_id int, p_group_name varchar(100))
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_group c WHERE c.group_id = p_group_id) THEN
		
			SET p_group_id = (SELECT IFNULL(MAX(group_id), 0) + 1 FROM tbl_group);

			INSERT INTO tbl_group(group_id, group_name)
			VALUES(p_group_id, p_group_name);

	ELSE
		UPDATE tbl_group
		SET group_name = p_group_name
		WHERE group_id = p_group_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_menu_permission_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_menu_permission_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_menu_permission_gall`(p_group_id int)
BEGIN
	#Routine body goes here...
	IF p_group_id != 1 THEN
		SET @sql = NULL;
		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT p.menu_id, p.en_menu_name, p.bn_menu_name, p.menu_link, p.sl_no, p.parent_id,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,1);' value='Grant' name='basic' class='gridbutton'/>") is_view,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,1);' value='Grant' name='basic' class='gridbutton'/>") is_insert,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,1);' value='Grant' name='basic' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,1);' value='Grant' name='basic' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,1);' value='Grant' name='basic' class='gridbutton'/>") is_print
			FROM tbl_menu p 
			WHERE p.menu_id NOT IN (SELECT g.menu_id FROM tbl_group_menu_permission g WHERE g.group_id = p_group_id)
			AND p.menu_link IS NOT NULL 
		);

		INSERT INTO temp_all
		SELECT p.menu_id, p.en_menu_name, p.bn_menu_name, p.menu_link, p.sl_no, p.parent_id,
			CASE 
				WHEN pm.is_view = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_view = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",1,1);' value='Grant' name='basic' class='gridbutton'/>")
			END is_view,
			CASE 
				WHEN pm.is_insert = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_insert = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",2,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_insert,
			CASE 
				WHEN pm.is_edit = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_edit = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",3,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_edit,
			CASE 
				WHEN pm.is_delete = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_delete = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",4,1);' value='Grant' name='basic' class='gridbutton'/>") 
			END is_delete,
			CASE 
				WHEN pm.is_print = 1 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,0);' value='Revoke' name='basic' class='gridbuttonr'/>") 
				WHEN pm.is_print = 0 THEN CONCAT("<input type='button' onClick='popupwinid(", p_group_id, ",", p.menu_id,",5,1);' value='Grant' name='basic' class='gridbutton'/>")
			END is_print
		FROM tbl_menu p
		INNER JOIN tbl_group_menu_permission pm ON pm.menu_id = p.menu_id
		WHERE pm.group_id = p_group_id		
		AND p.menu_link IS NOT NULL; 
		#WHERE p.menu_id IN (SELECT g.menu_id FROM tbl_group_menu_permission g WHERE g.group_id = p_group_id);

		SET @sql = CONCAT("SELECT menu_id, en_menu_name, bn_menu_name, menu_link, is_view, is_insert, is_edit, is_delete, is_print FROM temp_all ORDER BY parent_id, sl_no ");
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_menu_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_menu_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_menu_permission_i`(p_group_id int, p_menu_id int, p_statustype char(1), p_status tinyint)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_group_menu_permission c WHERE c.group_id = p_group_id AND c.menu_id = p_menu_id) THEN
		INSERT INTO tbl_group_menu_permission(group_id, menu_id, is_view, is_insert, is_edit, is_delete, is_print)
		VALUES(p_group_id, p_menu_id, 0, 0, 0, 0, 0);
	END IF;

	IF(p_statustype = 'v') THEN
		UPDATE tbl_group_menu_permission
		SET is_view = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'i') THEN
		UPDATE tbl_group_menu_permission
		SET is_insert = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'e') THEN
		UPDATE tbl_group_menu_permission
		SET is_edit = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'd') THEN
		UPDATE tbl_group_menu_permission
		SET is_delete = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	ELSEIF(p_statustype = 'p') THEN
		UPDATE tbl_group_menu_permission
		SET is_print = p_status
		WHERE group_id = p_group_id AND menu_id = p_menu_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_group_permission`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_group_permission`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_group_permission`(p_user_id int)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.group_id, ",", p_user_id,",0);' value='Grant' name='basic' class='gridbutton'/>") is_edit
		FROM tbl_group p 
		WHERE p.group_id NOT IN (SELECT g.group_id FROM tbl_user_group_permission g WHERE g.user_id = p_user_id)		
	);

	INSERT INTO temp_all
	SELECT p.group_id, p.group_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.group_id, ",", p_user_id,",1);' value='Revoke' name='basic' class='gridbuttonr' checked />") is_edit
	FROM tbl_group p 
	WHERE p.group_id IN (SELECT g.group_id FROM tbl_user_group_permission g WHERE g.user_id = p_user_id);

	SET @sql = CONCAT("SELECT group_id, group_name, is_edit FROM temp_all ORDER BY group_name ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_d`(p_inquiry_id int , p_doc_reg_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_doc_tag
	WHERE inquiry_id = p_inquiry_id
	AND doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.doc_reg_id, CONCAT(d.doc_reg_no,'- Date:', DATE_FORMAT(d.doc_reg_date,'%d-%m-%Y')) doc_reg_no
		FROM tbl_inquiry_doc_tag t
		INNER JOIN tbl_document_registration_master d ON d.doc_reg_id = t.doc_reg_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT doc_reg_id, doc_reg_no FROM temp_all ORDER BY doc_reg_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_gid`(p_inquiry_id int , p_doc_reg_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.doc_reg_id, d.doc_reg_no FROM tbl_inquiry_doc_tag t
	INNER JOIN tbl_document_registration_master d ON d.doc_reg_id = t.doc_reg_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.doc_reg_id = p_doc_reg_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_doc_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_doc_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_doc_tag_i`(p_inquiry_id int , p_doc_reg_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_doc_tag c WHERE c.inquiry_id = p_inquiry_id AND c.doc_reg_id = p_doc_reg_id) THEN		
		INSERT INTO tbl_inquiry_doc_tag(inquiry_id, doc_reg_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_doc_reg_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_doc_tag
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND doc_reg_id = p_doc_reg_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_c`(p_language_id smallint, p_committee_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.inquiry_id, p.inquiry_no
	FROM tbl_inquiry_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
	WHERE p.is_delete = 0
	AND p.language_id = p_language_id
	AND p.committee_id = p_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_d`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE inquiry_id  = p_inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_gid`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	p.inquiry_id, 
					p.inquiry_no, 
					DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
					p.inquiry_title, 
					p.proposed_month, 
					p.proposed_year,
					p.language_id, 
					l.language, 
					p.committee_id, 
					c.committee_name,
					p.parliament_id,
					p.session_id,
					p.user_id
	FROM tbl_inquiry_master p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
	WHERE p.inquiry_id  = p_inquiry_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_i`(p_inquiry_id int , p_inquiry_no varchar(50) , p_create_date varchar(10), p_proposed_month varchar(10), p_proposed_year varchar(4), p_parliament_id smallint, p_session_id smallint, p_doc_reg varchar(256), p_inquiry_title text , p_language_id smallint , p_committee_id smallint, p_user_id int, p_ministry varchar(256), p_organization varchar(256), p_others varchar(256), p_witness varchar(256), p_others_witness varchar(256))
BEGIN
	#Routine body goes here...
	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);
	DECLARE v_sl_no INT;		
	DECLARE v_committee_short_name varchar(50);

	IF NOT EXISTS(SELECT * FROM tbl_inquiry_master c WHERE c.inquiry_id = p_inquiry_id) THEN

		SET p_inquiry_id = (SELECT IFNULL(MAX(inquiry_id), 0) + 1 FROM tbl_inquiry_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_id), 0) + 1 FROM tbl_inquiry_master WHERE committee_id = p_committee_id AND language_id = p_language_id);		

		SET v_committee_short_name = (SELECT short_name FROM tbl_committee c WHERE c.committee_id = p_committee_id);

		#SET p_inquiry_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		SET p_inquiry_no = CONCAT(YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		IF(p_language_id = 2) THEN
			SET p_inquiry_no = english_bangla_number(p_inquiry_no);
		END IF;

		SET p_inquiry_no = CONCAT(v_committee_short_name, p_inquiry_no);

		INSERT INTO tbl_inquiry_master(inquiry_id, inquiry_no, create_date, inquiry_title, proposed_month, proposed_year, parliament_id, session_id, language_id, committee_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_inquiry_no, STR_TO_DATE(p_create_date, '%d-%m-%Y') , p_inquiry_title, p_proposed_month, p_proposed_year, p_parliament_id, p_session_id, p_language_id, p_committee_id, 0, p_user_id);
	ELSE		
		UPDATE tbl_inquiry_master
		SET inquiry_no = p_inquiry_no, 
				create_date = STR_TO_DATE(p_create_date, '%d-%m-%Y'), 
				inquiry_title = p_inquiry_title,  
				proposed_month = p_proposed_month,
				proposed_year = p_proposed_year,	
				parliament_id = p_parliament_id,
				session_id = p_session_id,
				language_id = p_language_id, 
				committee_id = p_committee_id, 				
				user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id;

	END IF;

	#Document Register Information

	DELETE FROM tbl_inquiry_doc_tag WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_doc_reg != '' THEN		
		SET occurance = LENGTH(p_doc_reg)-LENGTH(replace(p_doc_reg,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_doc_reg, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_doc_tag_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Document Register Information


	#Ministry Information
	DELETE FROM tbl_inquiry_ministry WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_ministry != '' THEN		
		SET occurance = LENGTH(p_ministry)-LENGTH(replace(p_ministry,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_ministry, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_ministry_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Ministry Information


	#organization Information

	DELETE FROM tbl_inquiry_organization WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_organization != '' THEN		
		SET occurance = LENGTH(p_organization)-LENGTH(replace(p_organization,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_organization, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_organization_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#organization Information


	#Others Information

	DELETE FROM tbl_inquiry_others WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_others != '' THEN		
		SET occurance = LENGTH(p_others)-LENGTH(replace(p_others,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_others, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_others_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Others Information

	#witness  Information

	DELETE FROM tbl_inquiry_witnesses WHERE inquiry_id = p_inquiry_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness )-LENGTH(replace(p_witness ,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness , ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_inquiry_witnesses_i(p_inquiry_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#others witness Information

	DELETE FROM tbl_inquiry_other_witnessess WHERE inquiry_id = p_inquiry_id;


	SET p_index = 1;
	IF p_others_witness != '' THEN		
		SET occurance = LENGTH(p_others_witness)-LENGTH(replace(p_others_witness,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_others_witness, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_inquiry_other_witnessess_i(p_inquiry_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#others witness Information

  SELECT p_inquiry_id inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(250), p_proposed_month varchar(10), p_proposed_year varchar(4), p_doc_reg_no varchar(50), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT  p.inquiry_id, 
						p.inquiry_no, 
						p.inquiry_title, 
						DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date,
						CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
						p.language_id,
						p.parliament_id,
						p.session_id,
						CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",p.inquiry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete	
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag d ON d.inquiry_id = p.inquiry_id AND d.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master r ON r.doc_reg_id = d.doc_reg_id AND r.is_delete = 0		
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%', p_inquiry_no, '%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))
		AND (p_proposed_month = '' OR p.proposed_month = p_proposed_month)
		AND (p_proposed_year = '' OR p.proposed_year = p_proposed_year)
		AND (p_doc_reg_no = '' OR r.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND p.is_delete = 0
		ORDER BY p.create_date DESC
		/*
		SELECT p.inquiry_id, p.inquiry_no, DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date , p.inquiry_title, p.language_id, l.`language`, p.committee_id, c.committee_name, p.user_id,
				CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
				CONCAT("<input type='button' onClick='deleteid(",p.inquiry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete		
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))		
		AND p.is_delete = 0*/
	);
	SET @sql = CONCAT("SELECT DISTINCT inquiry_id, inquiry_no, create_date, inquiry_title, proposed_date, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_master_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_master_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_master_token`(p_keyword varchar(20), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.inquiry_id, CONCAT(m.inquiry_no,":",m.inquiry_title) inquiry_no
	FROM tbl_inquiry_master m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.committee_id = p_committee_id
	AND CONCAT(m.inquiry_no," ",m.inquiry_title) LIKE CONCAT('%',p_keyword,'%')
  #AND m.inquiry_no LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_d`(p_inquiry_id int, p_ministry_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_ministry
	WHERE inquiry_id = p_inquiry_id
	AND ministry_id = p_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.ministry_id, d.ministry_name
		FROM tbl_inquiry_ministry t
		INNER JOIN tbl_ministry d ON d.ministry_id = t.ministry_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT ministry_id, ministry_name FROM temp_all ORDER BY ministry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_gid`(p_inquiry_id int , p_ministry_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.ministry_id, d.ministry_name
	FROM tbl_inquiry_ministry t
	INNER JOIN tbl_ministry d ON d.ministry_id = t.ministry_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.ministry_id = p_ministry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_ministry_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_ministry_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_ministry_i`(p_inquiry_id int , p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_ministry c WHERE c.inquiry_id = p_inquiry_id AND c.ministry_id = p_ministry_id) THEN		
		INSERT INTO tbl_inquiry_ministry(inquiry_id, ministry_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_ministry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_ministry
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND ministry_id = p_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_d`(p_inquiry_id int, p_organization_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_inquiry_organization
	WHERE inquiry_id = p_inquiry_id
	AND organization_id = p_organization_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.organization_id, d.organization_name
		FROM tbl_inquiry_organization t
		INNER JOIN tbl_organization d ON d.organization_id = t.organization_id AND d.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT organization_id, organization_name FROM temp_all ORDER BY organization_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_gid`(p_inquiry_id int , p_organization_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.organization_id, d.organization_name 
	FROM tbl_inquiry_organization t
	INNER JOIN tbl_organization d ON d.organization_id = t.organization_id AND d.is_delete = 0
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.inquiry_id = p_inquiry_id
	AND t.organization_id = p_organization_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_organization_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_organization_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_organization_i`(p_inquiry_id int , p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_organization c WHERE c.inquiry_id = p_inquiry_id AND c.organization_id = p_organization_id) THEN		
		INSERT INTO tbl_inquiry_organization(inquiry_id, organization_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_organization_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_organization
		SET user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id
		AND organization_id = p_organization_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_d`(p_others_org_ministry_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_others
	SET is_delete = 1,
			user_id = p_user_id
	WHERE others_org_ministry_id = p_others_org_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.others_org_ministry_id, o.others_org_ministry
		FROM tbl_inquiry_others t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_others o ON o.others_org_ministry_id = t.others_org_ministry_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id		
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT others_org_ministry_id, others_org_ministry FROM temp_all ORDER BY others_org_ministry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_gid`(p_others_org_ministry_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.others_org_ministry_id, o.others_org_ministry				
	FROM tbl_inquiry_others t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	INNER JOIN tbl_others o ON o.others_org_ministry_id = t.others_org_ministry_id AND o.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = o.language_id
	WHERE t.others_org_ministry_id = p_others_org_ministry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_others_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_others_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_others_i`(p_inquiry_id int , p_others_org_ministry_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_others c WHERE c.others_org_ministry_id = p_others_org_ministry_id AND c.inquiry_id = p_inquiry_id) THEN		

		INSERT INTO tbl_inquiry_others(inquiry_id, others_org_ministry_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_others_org_ministry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_others
		SET user_id = p_user_id
		WHERE others_org_ministry_id = p_others_org_ministry_id
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_d`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_other_witnessess
	SET is_delete = 1,
			user_id = p_user_id
	WHERE witness_id = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, o.witness_name, o.designation, o.phone, o.cell_phone, o.fax, o.email, o.address, o.comments, o.language_id, l.`language`, t.user_id
		FROM tbl_inquiry_other_witnessess t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, witness_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_gid`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, o.witness_name, o.designation, o.phone, o.cell_phone, o.fax, o.email, o.address, o.comments, o.language_id, l.`language`, t.user_id
	FROM tbl_inquiry_other_witnessess t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	WHERE t.witness_id = p_witness_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_other_witnessess_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_other_witnessess_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_other_witnessess_i`(p_inquiry_id int, p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_other_witnessess c WHERE c.witness_id = p_witness_id AND c.inquiry_id = p_inquiry_id) THEN		

		INSERT INTO tbl_inquiry_other_witnessess(inquiry_id, witness_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_witness_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_other_witnessess
		SET user_id = p_user_id
		WHERE witness_id = p_witness_id
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_d`(p_inquiry_report_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_report_master
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE inquiry_report_id = p_inquiry_report_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.inquiry_report_id, c.report_no, DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, c.inquiry_id, i.inquiry_no, c.title, c.parliament_id, p.parliament, c.session_id, s.`session`, c.executive_summary, c.introduction, c.issues, c.language_id, l.`language`, c.committee_id, b.committee_name, c.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.committee_id = p_committee_id
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY inquiry_report_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_gid`(p_inquiry_report_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.inquiry_report_id, c.report_no, DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, c.inquiry_id, i.inquiry_no, c.title, c.parliament_id, p.parliament, c.session_id, s.`session`, c.executive_summary, c.introduction, c.issues, c.language_id, l.`language`, c.committee_id, b.committee_name, c.user_id
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_i`(p_inquiry_report_id int, p_report_no varchar(30), p_report_date varchar(10), p_inquiry_id int, p_title longtext, p_parliament_id smallint, p_session_id smallint, p_executive_summary longtext, p_introduction longtext, p_issues longtext, p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_sl_no INT;	
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_report_master c WHERE c.inquiry_report_id = p_inquiry_report_id) THEN

		SET p_inquiry_report_id = (SELECT IFNULL(MAX(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master);

		SET v_sl_no = (SELECT IFNULL(COUNT(inquiry_report_id), 0) + 1 FROM tbl_inquiry_report_master WHERE committee_id = p_committee_id);
		SET p_report_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));
		IF(p_language_id = 2) THEN
			SET p_report_no = english_bangla_number(p_report_no);
		END IF;

		INSERT INTO tbl_inquiry_report_master(inquiry_report_id, report_no, report_date, inquiry_id, title, parliament_id, session_id, executive_summary, introduction, issues, language_id, committee_id, is_delete, user_id)
		VALUES(p_inquiry_report_id, p_report_no, STR_TO_DATE(p_report_date,'%d-%m-%Y'), p_inquiry_id, p_title, p_parliament_id, p_session_id, p_executive_summary, p_introduction, p_issues, p_language_id, p_committee_id, 0, p_user_id);

	ELSE
		UPDATE tbl_inquiry_report_master
		SET report_no = p_report_no, 
				report_date = STR_TO_DATE(p_report_date,'%d-%m-%Y'), 
				inquiry_id = p_inquiry_id, 
				title = p_title, 
				parliament_id = p_parliament_id, 
				session_id = p_session_id, 
				executive_summary = p_executive_summary, 
				introduction = p_introduction, 
				issues = p_issues, 
				language_id = p_language_id, 
				committee_id = p_committee_id,
				user_id = user_id
		WHERE inquiry_report_id = p_inquiry_report_id;

		DELETE FROM tbl_inquiry_report_recommendation
		WHERE inquiry_report_id = p_inquiry_report_id;
	END IF;
	SELECT p_inquiry_report_id inquiry_report_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_master_search`(p_start_date varchar(10), p_end_date varchar(10), p_report_no varchar(100), p_parliament_id smallint, p_inquiry_no varchar(100),  p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	c.inquiry_report_id, 
						c.report_no, 
						DATE_FORMAT(c.report_date,'%d-%m-%Y') report_date, 
						c.inquiry_id, 
						i.inquiry_no, 
						c.title, 
						c.parliament_id, 
						p.parliament, 
						c.session_id, 
						s.session, 
						c.executive_summary, 
						c.introduction, 
						c.issues, 
						c.language_id, 
						l.language, 
						c.committee_id, 
						b.committee_name, 
						CONCAT("<input type='button' onClick='popupwinid(",c.inquiry_report_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
						CONCAT("<input type='button' onClick='deleteid(",c.inquiry_report_id,");' name='basic' value='Print' class='gridbutton'/>") is_print
		FROM tbl_inquiry_report_master c 
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = c.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_parliament p ON p.parliament_id = c.parliament_id AND p.is_delete = 0
		INNER JOIN tbl_session s ON s.session_id = c.session_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = c.language_id 		
		INNER JOIN tbl_committee b ON b.committee_id = c.committee_id AND b.is_delete = 0
		WHERE c.language_id = p_language_id		
		AND c.committee_id = p_committee_id
		AND (p_start_date ='' OR p_end_date='' OR (c.report_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y')))
		AND (p_report_no = '' OR c.report_no LIKE CONCAT('%',p_report_no,'%'))
		AND (TRIM(p_parliament_id) = '' OR p_parliament_id = 0 OR c.parliament_id = p_parliament_id)
		AND (p_inquiry_no = '' OR i.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND c.is_delete = 0		
		ORDER BY c.report_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_report_id, report_no, report_date, inquiry_id, inquiry_no, title, parliament_id, parliament, session_id, session, executive_summary, introduction, issues, language_id, language, committee_id, committee_name, is_edit, is_delete, is_print FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_d`(p_inquiry_report_id int, p_recom_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		DELETE FROM tbl_inquiry_report_recommendation
		WHERE inquiry_report_id = p_inquiry_report_id
		AND recom_id = p_recom_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_gall`(p_inquiry_report_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.inquiry_report_id, c.recom_id, c.recom_no, c.recommendation, c.user_id
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.inquiry_report_id = p_inquiry_report_id
		AND c.is_delete = 0
		ORDER BY c.recom_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_gid`(p_recom_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.inquiry_report_id, c.recom_id, c.recom_no, c.recommendation, c.user_id
		FROM tbl_inquiry_report_recommendation c 
		INNER JOIN tbl_inquiry_report_master i ON i.inquiry_report_id = c.inquiry_report_id AND i.is_delete = 0
		WHERE c.recom_id = p_recom_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_report_recommendation_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_report_recommendation_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_report_recommendation_i`(p_inquiry_report_id int, p_recom_id int, p_recom_no varchar (50), p_recommendation longtext, p_user_id int)
BEGIN
	#Routine body goes here...	
	DECLARE p_index int;
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_report_recommendation c WHERE c.recom_id = p_recom_id) THEN
		
		SET p_recom_id = (SELECT IFNULL(MAX(recom_id), 0) + 1 FROM tbl_inquiry_report_recommendation);

		SET p_index = (SELECT IFNULL(COUNT(*), 0) + 1 FROM tbl_inquiry_report_recommendation WHERE inquiry_report_id = p_inquiry_report_id);
		
		SET p_recom_no = CONCAT('Recommendation ',p_index);

		/*SET p_recom_no = p_recom_no;*/

		INSERT INTO tbl_inquiry_report_recommendation(inquiry_report_id, recom_id, recom_no, recommendation, is_delete, user_id)
		VALUES(p_inquiry_report_id, p_recom_id, p_recom_no, p_recommendation, 0, p_user_id);
	/*
	ELSE
		UPDATE tbl_inquiry_report_recommendation
		SET inquiry_report_id = p_inquiry_report_id,
				recom_no = p_recom_no, 
				recommendation = p_recommendation, 				
				user_id = user_id
		WHERE recom_id = p_recom_id;
	*/
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_gall`(p_inquiry_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT 	s.schedule_no,	
						DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date					
		FROM tbl_inquiry_scheduling s 
		WHERE s.inquiry_id  = p_inquiry_id
		AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_gid`(p_inquiry_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT p.inquiry_id, 
					p.inquiry_no, 
					p.inquiry_title, 
					CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
					s.schedule_id, 					
					DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
					DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date, 
					s.schedule_no, 										
					p.language_id				 
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_scheduling s ON s.inquiry_id = p.inquiry_id AND s.is_delete = 0		
		WHERE p.inquiry_id  = p_inquiry_id
		#AND IFNULL(s.schedule_no,0) = p_schedule_no
		AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_i`(p_schedule_id int, p_inquiry_id int, p_schedule_no smallint, p_schedule_date varchar(10), p_user_id int)
BEGIN

	IF p_schedule_date != '' THEN
		IF NOT EXISTS(SELECT * FROM tbl_inquiry_scheduling WHERE inquiry_id = p_inquiry_id AND schedule_no = p_schedule_no) THEN
			
			SET p_schedule_id = (SELECT IFNULL(MAX(schedule_id), 0) + 1 FROM tbl_inquiry_scheduling);
		
			SET p_schedule_no = (SELECT IFNULL(MAX(schedule_no), 0) + 1 FROM tbl_inquiry_scheduling WHERE inquiry_id = p_inquiry_id);

			INSERT INTO tbl_inquiry_scheduling(schedule_id, inquiry_id, schedule_no, schedule_date, entry_date, is_delete, user_id)
			VALUES(p_schedule_id, p_inquiry_id, p_schedule_no, STR_TO_DATE(p_schedule_date, '%d-%m-%Y'), NOW(), 0, p_user_id);
	 
		ELSE
			UPDATE tbl_inquiry_scheduling
			SET schedule_date = STR_TO_DATE(p_schedule_date, '%d-%m-%Y'),
					user_id = p_user_id
			WHERE inquiry_id = p_inquiry_id
			AND schedule_no = p_schedule_no;
		END IF;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_scheduling_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_scheduling_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_scheduling_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(250), p_proposed_month varchar(10), p_proposed_year varchar(4), p_doc_reg_no varchar(50), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
  SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT DISTINCT	p.inquiry_id, 
						p.inquiry_no, 
						p.inquiry_title, 
						DATE_FORMAT(p.create_date,'%d-%m-%Y') create_date, 
						DATE_FORMAT(s.schedule_date, '%d-%m-%Y') schedule_date, 
						s.schedule_no, 
						CONCAT(p.proposed_month, ', ', p.proposed_year) proposed_date,
						p.language_id,						
						CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,");' style='width:100px' name='basic' value='Set Schedule' class='gridbutton'/>") is_edit
						#CONCAT("<input type='button' onClick='popupwinid(",p.inquiry_id,",", IFNULL(s.schedule_no,0),");' style='width:100px' name='basic' value='Set Schedule' class='gridbutton'/>") is_edit
		FROM tbl_inquiry_master p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = p.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag d ON d.inquiry_id = p.inquiry_id AND d.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master r ON r.doc_reg_id = d.doc_reg_id AND r.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_scheduling s ON s.inquiry_id = p.inquiry_id AND s.is_delete = 0
		WHERE p.language_id  = p_language_id 
		AND p.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR p.inquiry_no LIKE CONCAT('%', p_inquiry_no, '%'))
		AND (p_inquiry_title = '' OR p.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))
		AND (p_proposed_month = '' OR p.proposed_month = p_proposed_month)
		AND (p_proposed_year = '' OR p.proposed_year = p_proposed_year)
		AND (p_doc_reg_no = '' OR r.doc_reg_no LIKE CONCAT('%',p_doc_reg_no,'%'))
		AND p.is_delete = 0
		ORDER BY p.create_date DESC
	);
	SET @sql = CONCAT("SELECT DISTINCT inquiry_id, inquiry_no, create_date, inquiry_title, proposed_date, is_edit FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_d`(p_witness_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_inquiry_witnesses
	SET is_delete = 1,
			user_id = p_user_id
	WHERE witness_id = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_gall`(p_inquiry_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_mem;

	CREATE TEMPORARY TABLE temp_mem AS
	(
		SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.inquiry_id = p_inquiry_id
	);

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
	FROM tbl_inquiry_witnesses t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.inquiry_id = p_inquiry_id;

	#SELECT * FROM temp_mem;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
		#SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, m.member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
		INNER JOIN temp_mem m ON m.witness_id = t.witness_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_gid`(p_witness_id smallint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.witness_id, t.witness_type
	FROM tbl_inquiry_witnesses t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
	WHERE t.witness_id = p_witness_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_i`(p_inquiry_id int, p_witness_type varchar(1), p_witness_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_inquiry_witnesses c WHERE c.witness_id = p_witness_id AND c.inquiry_id = p_inquiry_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_inquiry_witnesses(inquiry_id, witness_type, witness_id, is_delete, user_id)
		VALUES(p_inquiry_id, p_witness_type, p_witness_id, 0, p_user_id);
	ELSE
		UPDATE tbl_inquiry_witnesses
		SET user_id = p_user_id
		WHERE witness_id = p_witness_id
		AND inquiry_id = p_inquiry_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_inquiry_witnesses_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_inquiry_witnesses_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_inquiry_witnesses_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT CONCAT(m.ministry_member_id,'#','M') w_id, m.ministry_member_name w_name
	FROM tbl_ministry_member m
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	WHERE m.language_id = p_language_id
	AND m.ministry_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.organization_member_id,'#','O') w_id , m.organization_member_name w_name
	FROM tbl_organization_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_organization o ON o.organization_id = m.organization_id
	WHERE m.language_id = p_language_id
	AND m.organization_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_d`(p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_invitees
	SET is_delete = 1,
			user_id = p_user_id
	WHERE invitees_id = p_invitees_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.invitees_id, m.invitees_name, m.invitees_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, l.`language`, m.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",m.invitees_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.invitees_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_invitees m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 		
		WHERE m.language_id = p_language_id		
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name, invitees_organization, designation, phone, cell_phone, fax, email, address, comments, language_id, language, user_id, is_edit, is_delete FROM temp_all ORDER BY invitees_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_gid`(p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.invitees_id, m.invitees_name, m.invitees_organization, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, l.`language`, m.user_id
	FROM tbl_invitees m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.invitees_id = p_invitees_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_i`(p_invitees_id bigint , p_invitees_name varchar(150) , p_invitees_organization varchar(150) , p_designation varchar(100) , p_phone varchar(50) , p_cell_phone varchar(50) , p_fax varchar(50) , p_email varchar(150) , p_address varchar(250) , p_comments varchar(250) , p_language_id smallint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_invitees m WHERE m.invitees_id = p_invitees_id) THEN
		SET p_invitees_id = (SELECT IFNULL(MAX(invitees_id), 0) + 1 FROM tbl_invitees);

		INSERT INTO tbl_invitees(invitees_id, invitees_name, invitees_organization, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_invitees_id, p_invitees_name, p_invitees_organization, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_invitees
		SET invitees_name = p_invitees_name, 
				invitees_organization = p_invitees_organization,
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE invitees_id = p_invitees_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_invitees_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_invitees_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_invitees_token`(p_keyword varchar(20), p_language_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.invitees_id, m.invitees_name
	FROM tbl_invitees m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 	
	WHERE m.language_id = p_language_id	
	AND m.invitees_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_language_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_language_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_language_c`()
BEGIN
	#Routine body goes here...
	SELECT l.language_id, l.language 
	FROM tbl_language l
	ORDER BY l.language_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_d`(p_logistic_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_logistic_member
	SET is_delete = 1,
			user_id = p_user_id
	WHERE logistic_member_id = p_logistic_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.logistic_member_id, m.logistic_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.notification_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.notification_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_logistic_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT logistic_member_id, logistic_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY logistic_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_gid`(p_logistic_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.logistic_member_id, m.logistic_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_logistic_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.logistic_member_id = p_logistic_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_i`(p_logistic_member_id smallint, p_logistic_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_logistic_member m WHERE m.logistic_member_id = p_logistic_member_id) THEN
		SET p_logistic_member_id = (SELECT IFNULL(MAX(logistic_member_id), 0) + 1 FROM tbl_logistic_member);

		INSERT INTO tbl_logistic_member(logistic_member_id, logistic_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_logistic_member_id, p_logistic_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_logistic_member
		SET logistic_member_name = p_logistic_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE logistic_member_id = p_logistic_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_search`(p_logistic_member_name varchar(150), p_designation varchar(150), p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.logistic_member_id, m.logistic_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.logistic_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.logistic_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_logistic_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_logistic_member_name = '' OR m.logistic_member_name LIKE CONCAT('%',p_logistic_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT logistic_member_id, logistic_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY logistic_member_id DESC ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_logistic_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_logistic_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_logistic_member_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.logistic_member_id, m.logistic_member_name
	FROM tbl_logistic_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.logistic_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_d`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_committee
	WHERE metting_notice_id = p_metting_notice_id 
	AND member_id = p_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
		FROM tbl_meeting_notice_committee t
		INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
		INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT member_id, member_name FROM temp_all ORDER BY member_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_gid`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
	FROM tbl_meeting_notice_committee t
	INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.member_id = p_member_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_committee_i`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_committee c WHERE c.metting_notice_id = p_metting_notice_id AND c.member_id = p_member_id) THEN		
		INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_member_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_committee
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND member_id = p_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_d`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_inquiry_tag
	WHERE inquiry_id = p_inquiry_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, CONCAT(i.inquiry_no,':',i.inquiry_title) inquiry_no, i.inquiry_title, t.metting_notice_id
		FROM tbl_meeting_notice_inquiry_tag t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no FROM temp_all ORDER BY inquiry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_gid`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.metting_notice_id
	FROM tbl_meeting_notice_inquiry_tag t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.inquiry_id =  p_inquiry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_inquiry_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_inquiry_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_inquiry_tag_i`(p_metting_notice_id int, p_inquiry_id int , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_inquiry_tag c WHERE c.metting_notice_id = p_metting_notice_id AND c.inquiry_id = p_inquiry_id) THEN		
		INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_inquiry_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_inquiry_tag
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_d`(p_metting_notice_id int, p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_invitees
	WHERE invitees_id  = p_invitees_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name
		FROM tbl_meeting_notice_invitees t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0				
		INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id		
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name FROM temp_all ORDER BY invitees_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_gid`(p_invitees_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name, v.invitees_organization, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_meeting_notice_invitees t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.invitees_id = p_invitees_id
	AND l.language_id = m.language_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_invitees_i`(p_metting_notice_id int , p_invitees_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_invitees c WHERE c.invitees_id = p_invitees_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_invitees_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_invitees
		SET user_id = p_user_id
		WHERE invitees_id = p_invitees_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_d`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_logistic
	WHERE metting_notice_id = p_metting_notice_id 
	AND logistic_id = p_logistic_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
		FROM tbl_meeting_notice_logistic t
		INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.logistic_id AND i.is_delete = 0		
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT logistic_id, logistic_member_name FROM temp_all ORDER BY logistic_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_gid`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
	FROM tbl_meeting_notice_logistic t
	INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.p_logistic_id AND i.is_delete = 0	
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.logistic_id = p_logistic_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_logistic_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_logistic_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_logistic_i`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_logistic c WHERE c.metting_notice_id = p_metting_notice_id AND c.logistic_id = p_logistic_id) THEN		
		INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_logistic_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_logistic
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND logistic_id = p_logistic_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_adjourned`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_adjourned`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_adjourned`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		DECLARE v_metting_notice_id INT;	
		DECLARE v_length INT;
		DECLARE v_committee_id SMALLINT;
		DECLARE v_language_id SMALLINT;

		DECLARE v_sub_sitting_no INT;
		DECLARE v_sitting_no VARCHAR(50);

		SET v_sitting_no =  (SELECT SUBSTRING_INDEX(sitting_no, '/', 1) FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
		SET v_committee_id = (SELECT committee_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);
		SET v_language_id = (SELECT language_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id);

		SET v_length = LENGTH(v_sitting_no);

		SET v_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);
		SET v_sub_sitting_no = (SELECT COUNT(*) FROM tbl_meeting_notice_master WHERE LEFT(sitting_no,v_length) = v_sitting_no AND committee_id = v_committee_id AND language_id = v_language_id);

		#Set New Sitting Number
		SET v_sitting_no = CONCAT(v_sitting_no,'/',v_sub_sitting_no); 
			
		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, adjourned_date, is_revised, is_display, is_complete, is_delete, user_id)
		SELECT v_metting_notice_id, gov_no_pix, gov_no_postfix, v_sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, language_id, committee_id, 'ADJOURNED', 0, 0, 0, 1, NOW(), 0, is_display, is_complete, is_delete, p_user_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id;
		

		UPDATE tbl_meeting_notice_master
		SET is_display = 0
		WHERE metting_notice_id = p_metting_notice_id;



	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
	SELECT v_metting_notice_id, inquiry_id, is_delete, p_user_id FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;	

	#Inquires Tag Information


	#committee Information	

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
	SELECT v_metting_notice_id, member_id, is_delete, p_user_id FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;	

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
	SELECT v_metting_notice_id, witness_id, witness_type, is_delete, p_user_id FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;	

	#witness  Information


	#p_logistics Information
	
	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
	SELECT v_metting_notice_id, logistic_id, is_delete, p_user_id FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;	

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
	SELECT v_metting_notice_id, notification_id, is_delete, p_user_id FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;	

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
	SELECT v_metting_notice_id, invitees_id, is_delete, p_user_id FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;	

	#p_invitees Information






	#===========================Record of decision Part====================================================

	#tbl_record_of_decision_master 

	#IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_delete, user_id)
		SELECT v_metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_delete, p_user_id FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = v_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = v_metting_notice_id;

		#Record of decision Part
	#END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_approved`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_approved`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_approved`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_approved = 1, 
				approved_date = NOW(),	
				status = 'APPROVED',  
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_cancel`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_cancel`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_cancel`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_cancel = 1, 
				status = 'CANCEL', 
				cancel_date = NOW(),
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_meeting_notice_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.metting_notice_id, 
						t.gov_no_pix, 
						t.gov_no_postfix, 
						t.sitting_no, 
						DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, 
						t.bn_date, 
						t.time, 
						t.venue_id, 
						v.venue_name, 
						t.sub_committee_id, 
						s.sub_committee_name, 
						t.private_business_before, 
						t.public_business, 
						t.private_business_after, 
						t.language_id, 
						l.language, 
						t.committee_id, 
						c.committee_name, 
						t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0
		INNER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		WHERE t.language_id = p_language_id
		AND t.committee_id = p_committee_id
		AND t.is_delete = 0
		ORDER BY t.en_date DESC
	);
	SET @sql = CONCAT("SELECT metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, venue_name, sub_committee_id, sub_committee_name, private_business_before, public_business, private_business_after, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_gid`(p_metting_notice_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, t.gov_no_pix, t.gov_no_postfix, t.sitting_no, DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, t.bn_date, t.time, t.venue_id, v.venue_name, t.sub_committee_id, s.sub_committee_name, t.private_business_before, t.public_business, t.private_business_after, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id
	FROM tbl_meeting_notice_master t
	INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0	
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
	LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_i`(p_metting_notice_id int, p_gov_no_pix varchar(50), p_gov_no_postfix varchar(10) , p_sitting_no varchar(50), p_en_date varchar(10), p_bn_date varchar(250) , p_time varchar(50), p_venue_id smallint , p_sub_committee_id smallint, p_inquiries varchar(250), p_private_business_before longtext, p_public_business longtext, p_private_business_after longtext, p_committee varchar(250), p_witness varchar(250), p_logistics varchar(250), p_notification varchar(250), p_invitees varchar(250), p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	DECLARE v_sl_no INT;		

	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_master c WHERE c.metting_notice_id = p_metting_notice_id) THEN		

		SET p_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);

		IF(p_sub_committee_id = 0 OR p_sub_committee_id = '') THEN
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id);		
		ELSE
			SET v_sl_no = (SELECT IFNULL(COUNT(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master WHERE committee_id = p_committee_id AND language_id = p_language_id AND sub_committee_id = p_sub_committee_id);		
		END IF;

		#SET p_sitting_no = CONCAT(RIGHT(CONCAT('00', p_committee_id), 2), YEAR(NOW()), RIGHT(CONCAT('0000', v_sl_no), 4));

		SET p_sitting_no = CONCAT('',v_sl_no,'');

		IF(p_language_id = 2) THEN
			SET p_sitting_no = english_bangla_number(p_sitting_no);
		END IF;

		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, is_display, is_complete, is_delete, user_id)
		VALUES(p_metting_notice_id, p_gov_no_pix, p_gov_no_postfix, p_sitting_no, STR_TO_DATE(p_en_date,'%d-%m-%Y'), p_bn_date, p_time, p_venue_id, p_sub_committee_id, p_private_business_before, p_public_business, p_private_business_after, p_language_id, p_committee_id, 'NEW', 0, 0, 0, 0, 0, 1, 0, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_master
		SET gov_no_pix = p_gov_no_pix, 
				gov_no_postfix = p_gov_no_postfix, 
				sitting_no = p_sitting_no, 
				en_date = STR_TO_DATE(p_en_date,'%d-%m-%Y'), 
				bn_date = p_bn_date, 
				time = p_time, 
				venue_id = p_venue_id, 
				sub_committee_id = p_sub_committee_id, 
				private_business_before = p_private_business_before, 
				public_business = p_public_business, 
				private_business_after = p_private_business_after, 
				language_id = p_language_id, 
				committee_id = p_committee_id,  
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	END IF;


	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_inquiries != '' THEN		
		SET occurance = LENGTH(p_inquiries)-LENGTH(replace(p_inquiries,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_inquiries, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_inquiry_tag_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#Inquires Tag Information


	#committee Information

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness )-LENGTH(replace(p_witness ,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness , ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_meeting_notice_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#p_logistics Information

	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistics != '' THEN		
		SET occurance = LENGTH(p_logistics)-LENGTH(replace(p_logistics,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistics, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_meeting_notice_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information

	#SELECT p_metting_notice_id metting_notice_id;








	#Record of decision Part


	#tbl_record_of_decision_master 

	#IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_delete, user_id)
		VALUES(p_metting_notice_id, 0, '', '', '', p_language_id, p_committee_id , 0, 0, p_user_id);

		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = p_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = p_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = p_metting_notice_id;

		#Record of decision Part
	#END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_issued`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_issued`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_issued`(p_metting_notice_id int, p_user_id int)
BEGIN
	#Routine body goes here...
		UPDATE tbl_meeting_notice_master
		SET is_issued = 1, 
				status = 'ISSUED',  
				issued_date = NOW(),
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_revised`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_revised`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_revised`(p_metting_notice_id int, p_en_date varchar(10), p_bn_date varchar(250), p_time varchar(50), p_user_id int)
BEGIN
	#Routine body goes here...
		DECLARE v_metting_notice_id INT;		

		SET v_metting_notice_id = (SELECT IFNULL(MAX(metting_notice_id), 0) + 1 FROM tbl_meeting_notice_master);

		INSERT INTO tbl_meeting_notice_master(metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, language_id, committee_id, status, is_approved, is_issued, is_cancel, is_adjourned, is_revised, revised_date, is_display, is_complete, is_delete, user_id)
		SELECT v_metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, STR_TO_DATE(p_en_date,'%d-%m-%Y') , p_bn_date, p_time, venue_id, sub_committee_id, private_business_before, public_business, private_business_after, language_id, committee_id, 'REVISED', 0, 0, 0, 0, 1, NOW(), is_display, is_complete, is_delete, p_user_id FROM tbl_meeting_notice_master WHERE metting_notice_id = p_metting_notice_id;
		

		UPDATE tbl_meeting_notice_master
		SET is_display = 0
		WHERE metting_notice_id = p_metting_notice_id;



	#Inquires Tag Information

	DELETE FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_inquiry_tag(metting_notice_id, inquiry_id, is_delete, user_id)
	SELECT v_metting_notice_id, inquiry_id, is_delete, p_user_id FROM tbl_meeting_notice_inquiry_tag WHERE metting_notice_id = p_metting_notice_id;	

	#Inquires Tag Information


	#committee Information	

	DELETE FROM tbl_meeting_notice_committee WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_committee(metting_notice_id, member_id, is_delete, user_id)
	SELECT v_metting_notice_id, member_id, is_delete, p_user_id FROM tbl_meeting_notice_committee WHERE metting_notice_id = p_metting_notice_id;	

	#committee Information

	#witness  Information

	DELETE FROM tbl_meeting_notice_witness WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
	SELECT v_metting_notice_id, witness_id, witness_type, is_delete, p_user_id FROM tbl_meeting_notice_witness WHERE metting_notice_id = p_metting_notice_id;	

	#witness  Information


	#p_logistics Information
	
	DELETE FROM tbl_meeting_notice_logistic WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_logistic(metting_notice_id, logistic_id, is_delete, user_id)
	SELECT v_metting_notice_id, logistic_id, is_delete, p_user_id FROM tbl_meeting_notice_logistic WHERE metting_notice_id = p_metting_notice_id;	

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_meeting_notice_notification WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
	SELECT v_metting_notice_id, notification_id, is_delete, p_user_id FROM tbl_meeting_notice_notification WHERE metting_notice_id = p_metting_notice_id;	

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_meeting_notice_invitees WHERE metting_notice_id = v_metting_notice_id;
	INSERT INTO tbl_meeting_notice_invitees(metting_notice_id, invitees_id, is_delete, user_id)
	SELECT v_metting_notice_id, invitees_id, is_delete, p_user_id FROM tbl_meeting_notice_invitees WHERE metting_notice_id = p_metting_notice_id;	

	#p_invitees Information






	#===========================Record of decision Part====================================================

	#tbl_record_of_decision_master 

	#IF NOT EXISTS (SELECT * FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id) THEN
	
		DELETE FROM tbl_record_of_decision_master WHERE metting_notice_id = v_metting_notice_id;

		INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_delete, user_id)
		SELECT v_metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id, flag, is_delete, p_user_id FROM tbl_record_of_decision_master WHERE metting_notice_id = p_metting_notice_id;

		#tbl_record_of_decision_master 

		
		DELETE FROM tbl_record_of_decision_inquiry_tag WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_inquiry_tag (metting_notice_id, inquiry_id, status, is_delete, user_id)
		SELECT t.metting_notice_id, t.inquiry_id, '0', t.is_delete, t.user_id FROM tbl_meeting_notice_inquiry_tag t WHERE t.metting_notice_id = v_metting_notice_id;



		DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.member_id, t.is_delete, t.user_id FROM tbl_meeting_notice_committee t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_type, witness_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.witness_type, t.witness_id, t.is_delete, t.user_id FROM tbl_meeting_notice_witness t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.logistic_id, t.is_delete, t.user_id FROM tbl_meeting_notice_logistic t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.notification_id, t.is_delete, t.user_id FROM tbl_meeting_notice_notification t WHERE t.metting_notice_id = v_metting_notice_id;


		DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = v_metting_notice_id;
		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		SELECT t.metting_notice_id, t.invitees_id, t.is_delete, t.user_id FROM tbl_meeting_notice_invitees t WHERE t.metting_notice_id = v_metting_notice_id;

		#Record of decision Part
	#END IF;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_master_search`(p_start_date varchar(10), p_end_date varchar(10), p_ref_no varchar(100), p_sub_committee int, p_sitting_no varchar(100), p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
/*
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.gov_no_pix, t.gov_no_postfix, t.sitting_no, DATE_FORMAT(t.en_date,'%d-%m-%Y') en_date, t.bn_date, t.time, t.venue_id, v.venue_name, t.sub_committee_id, s.sub_committee_name, t.private_business_before, t.public_business, t.private_business_after, t.language_id, l.language, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete ,
          CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton'/>") is_approved,
          CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton'/>") is_issued,
          CONCAT("<input type='button' onClick='printid(",t.metting_notice_id, ");' name='basic' value='Print' class='gridbutton'/>") is_print,
          CONCAT("<input type='button' onClick='multimedia(",t.metting_notice_id, ");' name='basic' value='Multimedia' class='gridbutton'/>") is_multimedia,
          CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Rec. of Dec.' class='gridbutton'/>") is_record_of_dec
		FROM tbl_meeting_notice_master t
		INNER JOIN tbl_venue v ON v.venue_id = t.venue_id AND v.is_delete = 0
		LEFT OUTER JOIN tbl_sub_committee s ON s.sub_committee_id = t.sub_committee_id AND s.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0		
		WHERE t.language_id = p_language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, gov_no_pix, gov_no_postfix, sitting_no, en_date, bn_date, time, venue_id, venue_name, sub_committee_id, sub_committee_name, private_business_before, public_business, private_business_after, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete, is_approved, is_issued, is_print , is_multimedia, is_record_of_dec FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
*/
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	m.metting_notice_id, 
						m.sitting_no, 
						CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) ref_no, 
						DATE_FORMAT(m.en_date,'%d-%m-%Y') en_date, 
						m.bn_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						v.venue_name,
						m.status,
						CASE
						WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>")
						WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton' disabled style='background-color:gray;' />")
						END is_edit,
						CASE
						WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") 
						WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton' disabled style='background-color:gray;'/>") 
						END is_delete,
						CASE
						WHEN m.is_approved = 0 THEN CONCAT("<input type='button' onClick='approvedid(",t.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton'/>") 
						WHEN m.is_approved = 1 THEN CONCAT("<input type='button' onClick='approvedid(",t.metting_notice_id, ");' name='basic' value='Approved' class='gridbutton' disabled style='background-color:gray;' />")
						END is_approved,						
						CASE
						WHEN m.is_issued = 0 AND m.is_approved = 1 THEN CONCAT("<input type='button' onClick='issuedid(",t.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton'/>")
						WHEN m.is_issued = 0 AND m.is_approved = 0 THEN CONCAT("<input type='button' onClick='issuedid(",t.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton' disabled style='background-color:gray;'/>") 
						ELSE CONCAT("<input type='button' onClick='issuedid(",t.metting_notice_id, ");' name='basic' value='Issued' class='gridbutton' disabled style='background-color:gray;'/>") 
						END is_issued,
						CASE
						WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='cancelid(",t.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' disabled style='background-color:gray;'/>") 
						WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='cancelid(",t.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' />") 
						ELSE CONCAT("<input type='button' onClick='cancelid(",t.metting_notice_id, ");' name='basic' value='Cancel' class='gridbutton' disabled style='background-color:gray;'/>")
						END is_cancled,
						CASE
						WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='adjournedid(",t.metting_notice_id, ");' name='basic' value='Adjourne' class='gridbutton' disabled style='background-color:gray;'/>") 
						WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='adjournedid(",t.metting_notice_id, ");' name='basic' value='Adjourne' class='gridbutton'/>") 
						ELSE CONCAT("<input type='button' onClick='adjournedid(",t.metting_notice_id, ");' name='basic' value='Adjourne' class='gridbutton' disabled style='background-color:gray;'/>") 
						END is_adjourned,
						CASE
						WHEN m.is_issued = 0 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='revisedid(",t.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton' disabled style='background-color:gray;'/>") 
						WHEN m.is_issued = 1 AND m.is_cancel = 0 THEN CONCAT("<input type='button' onClick='revisedid(",t.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton'/>") 
						ELSE CONCAT("<input type='button' onClick='revisedid(",t.metting_notice_id, ");' name='basic' value='Revise' class='gridbutton' disabled style='background-color:gray;'/>")
						END is_revised,
						CASE
						WHEN m.is_issued = 0 THEN CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Issued Notice' class='gridbutton' disabled style='background-color:gray; width: 120px;' />") 
						WHEN m.is_issued = 1 THEN CONCAT("<input type='button' onClick='viewnoticeid(",t.metting_notice_id, ");' name='basic' value='View Issued Notice' class='gridbutton' style='width: 120px;'/>")
						END is_view_notice,
						CONCAT("<input type='button' onClick='printid(",t.metting_notice_id, ");' name='basic' value='Print' class='gridbutton'/>") is_print,

						CONCAT("<input type='button' onClick='multimedia(",t.metting_notice_id, ");' name='basic' value='Multimedia' class='gridbutton'/>") is_multimedia,
						CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Rec. of Dec.' class='gridbutton'/>") is_record_of_dec
		FROM tbl_meeting_notice_master m
		INNER JOIN tbl_meeting_notice_inquiry_tag t ON t.metting_notice_id = m.metting_notice_id AND t.is_delete = 0
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_venue v ON v.venue_id = m.venue_id AND v.is_delete = 0
		WHERE m.committee_id = p_committee_id
		AND m.language_id = p_language_id
		AND (p_sub_committee = '' OR p_sub_committee = 0 OR m.sub_committee_id = p_sub_committee)
		AND (p_ref_no = '' OR CONCAT(m.gov_no_pix, '.' ,m.gov_no_postfix) LIKE CONCAT('%',p_ref_no,'%'))
		AND (p_sitting_no = '' OR m.sitting_no LIKE CONCAT('%',p_sitting_no,'%'))
		AND (p_start_date='' OR p_end_date='' OR (m.en_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))
		AND m.is_delete = 0 
		AND m.is_display = 1
		ORDER BY m.en_date DESC
	);
	SET @sql = CONCAT("SELECT metting_notice_id, sitting_no, ref_no, en_date, bn_date, inquiry_no, inquiry_title, venue_name, status, is_edit, is_delete, is_approved, is_issued,  is_cancled, is_adjourned, is_revised, is_view_notice, is_print, is_multimedia, is_record_of_dec FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_d`(p_metting_notice_id int, p_notification_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_meeting_notice_notification
	WHERE notification_id  = p_notification_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name
		FROM tbl_meeting_notice_notification t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0						
		INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id		
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT notification_id, notification_member_name FROM temp_all ORDER BY notification_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_gid`(p_metting_notice_id int, p_notification_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_meeting_notice_notification t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.notification_id = p_notification_id
	AND l.language_id = m.language_id
	AND t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_notification_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_notification_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_notification_i`(p_metting_notice_id int , p_notification_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_notification c WHERE c.notification_id = p_notification_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_meeting_notice_notification(metting_notice_id, notification_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_notification_id, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_notification
		SET user_id = p_user_id
		WHERE notification_id = p_notification_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_d`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_meeting_notice_witness
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id = p_metting_notice_id 
	AND witness_id = p_witness_id
	AND witness_type = p_witness_type;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_gall`(p_metting_notice_id int, p_inquiry_id int)
BEGIN
	#Routine body goes here...	

	IF(p_metting_notice_id = 0) THEN
		
		DROP TEMPORARY TABLE IF EXISTS temp_mem;

		CREATE TEMPORARY TABLE temp_mem AS
		(
			SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
			FROM tbl_inquiry_witnesses t
			INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
			INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
			WHERE t.is_delete = 0		
			AND t.inquiry_id = p_inquiry_id
		);

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
		FROM tbl_inquiry_witnesses t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0
		INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
		WHERE t.is_delete = 0
		AND t.inquiry_id = p_inquiry_id;

		INSERT INTO temp_mem
		SELECT t.witness_id, 'T' witness_type, o.witness_name member_name
		FROM tbl_inquiry_other_witnessess t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id= t.inquiry_id AND i.is_delete = 0		
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = o.language_id
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0;
		
		SELECT CONCAT(t.witness_id, '#', t.witness_type) witness_id, t.member_name
		FROM temp_mem t
		ORDER BY t.witness_type, t.witness_id;

	ELSE
		SET @sql = NULL;	
		DROP TEMPORARY TABLE IF EXISTS temp_mem;

		CREATE TEMPORARY TABLE temp_mem AS
		(
			SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
			FROM tbl_meeting_notice_witness t
			INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
			INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
			WHERE t.is_delete = 0		
			AND t.metting_notice_id = p_metting_notice_id
		);

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
		FROM tbl_meeting_notice_witness t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
		WHERE t.is_delete = 0
		AND t.metting_notice_id = p_metting_notice_id;

		INSERT INTO temp_mem
		SELECT t.witness_id, t.witness_type, o.witness_name member_name
		FROM tbl_meeting_notice_witness t
		INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
		WHERE t.is_delete = 0
		AND t.metting_notice_id = p_metting_notice_id;


		#SELECT * FROM temp_mem;

		DROP TEMPORARY TABLE IF EXISTS temp_all;
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT t.metting_notice_id, i.sitting_no, i.en_date, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
			FROM tbl_meeting_notice_witness t
			INNER JOIN tbl_meeting_notice_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0		
			INNER JOIN temp_mem m ON m.witness_id = t.witness_id
			WHERE t.metting_notice_id = p_metting_notice_id
			AND t.is_delete = 0
		);
		SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
		PREPARE stmt FROM @sql;
		EXECUTE stmt;
		DEALLOCATE PREPARE stmt;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_gid`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.witness_id, t.witness_type, t.user_id
	FROM tbl_meeting_notice_witness t
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.witness_id = p_witness_id
	AND t.witness_type = p_witness_type
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_i`(p_metting_notice_id int, p_witness_type char(1), p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_meeting_notice_witness c WHERE c.metting_notice_id = p_metting_notice_id AND c.witness_id = p_witness_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_meeting_notice_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
		VALUES(p_metting_notice_id, p_witness_id, p_witness_type, 0, p_user_id);
	ELSE
		UPDATE tbl_meeting_notice_witness
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND witness_id = p_witness_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_meeting_notice_witness_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_meeting_notice_witness_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_meeting_notice_witness_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT CONCAT(m.ministry_member_id,'#','M') w_id, m.ministry_member_name w_name
	FROM tbl_ministry_member m
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	WHERE m.language_id = p_language_id
	AND m.ministry_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.organization_member_id,'#','O') w_id , m.organization_member_name w_name
	FROM tbl_organization_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	INNER JOIN tbl_organization o ON o.organization_id = m.organization_id
	WHERE m.language_id = p_language_id
	AND m.organization_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0
	UNION ALL
	SELECT CONCAT(m.witness_id,'#','T') w_id , m.witness_name w_name
	FROM tbl_other_witnessess m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.witness_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_menu_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_menu_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_menu_gall`(p_parent_id smallint, p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...
	DROP TEMPORARY TABLE IF EXISTS temp_all;	
	IF(p_language_id = 1) THEN	
		CREATE TEMPORARY TABLE temp_all AS
		(		
			SELECT m.menu_id, m.en_menu_name menu_name, m.menu_link  FROM tbl_menu m
			WHERE m.parent_id = 0		
		);
	ELSE
		CREATE TEMPORARY TABLE temp_all AS
		(
			SELECT m.menu_id, m.bn_menu_name menu_name, m.menu_link  FROM tbl_menu m
			WHERE m.parent_id = 0
		);
	END IF;

	IF(p_parent_id != 0) THEN
		DELETE FROM temp_all;
	END IF;

	IF(p_language_id = 1) THEN
		INSERT INTO temp_all
		SELECT m.menu_id, m.en_menu_name menu_name, m.menu_link  FROM tbl_menu m
		WHERE m.parent_id = p_parent_id
		AND m.menu_id IN (SELECT p.menu_id FROM tbl_group_menu_permission p INNER JOIN tbl_user_group_permission g ON g.group_id = p.group_id WHERE g.user_id = p_user_id)
		ORDER BY m.sl_no;
	ELSE
		INSERT INTO temp_all
		SELECT m.menu_id, m.bn_menu_name menu_name, m.menu_link  FROM tbl_menu m
		WHERE m.parent_id = p_parent_id
		AND m.menu_id IN (SELECT p.menu_id FROM tbl_group_menu_permission p INNER JOIN tbl_user_group_permission g ON g.group_id = p.group_id WHERE g.user_id = p_user_id)
		ORDER BY m.sl_no;
	END IF;
	SELECT menu_id, menu_name, menu_link FROM temp_all;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_id, m.ministry_name
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_d`(p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_ministry
	SET is_delete = 1
	WHERE ministry_id = p_ministry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_id, m.ministry_name, m.contact_person, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.ministry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_id, ministry_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_gid`(p_ministry_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_id, m.ministry_name, m.contact_person, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.ministry_id = p_ministry_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_i`(p_ministry_id smallint, p_ministry_name varchar(150), p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_ministry m WHERE m.ministry_id = p_ministry_id) THEN
		SET p_ministry_id = (SELECT IFNULL(MAX(ministry_id), 0) + 1 FROM tbl_ministry);

		INSERT INTO tbl_ministry(ministry_id, ministry_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_ministry_id, p_ministry_name, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_ministry
		SET ministry_name = p_ministry_name, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE ministry_id = p_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_d`(p_ministry_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_ministry_member
	SET is_delete = 1,
			user_id = p_user_id	
	WHERE ministry_member_id = p_ministry_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.ministry_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,",",m.ministry_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry_member m 
		INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_member_id, ministry_id, ministry_name, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_gid`(p_ministry_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language
	FROM tbl_ministry_member m 
	INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.ministry_member_id = p_ministry_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_i`(p_ministry_member_id int, p_ministry_id smallint, p_ministry_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_ministry_member m WHERE m.ministry_member_id = p_ministry_member_id) THEN
		SET p_ministry_member_id = (SELECT IFNULL(MAX(ministry_member_id), 0) + 1 FROM tbl_ministry_member);

		INSERT INTO tbl_ministry_member(ministry_member_id, ministry_id, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, is_delete, user_id)
		VALUES(p_ministry_member_id, p_ministry_id, p_ministry_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_ministry_member
		SET ministry_id = p_ministry_id, 
				ministry_member_name = p_ministry_member_name, 
				designation = p_designation, 				
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE ministry_member_id = p_ministry_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_member_search`(p_ministry_id smallint, p_ministry_member_name varchar(150), p_designation varchar(100),  p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.ministry_member_id, m.ministry_id, t.ministry_name, m.ministry_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid_member(",m.ministry_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.ministry_id,",",m.ministry_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_ministry_member m 
		INNER JOIN tbl_ministry t ON t.ministry_id = m.ministry_id
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_ministry_id = 0 OR m.ministry_id = p_ministry_id)
		AND (p_ministry_member_name = '' OR m.ministry_member_name LIKE CONCAT('%',p_ministry_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT ministry_member_id, ministry_id, ministry_name, ministry_member_name, designation, phone, cell_phone, fax, email, address, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY ministry_member_id DESC ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_ministry_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_ministry_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_ministry_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.ministry_id, m.ministry_name
	FROM tbl_ministry m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.ministry_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_month_info_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_month_info_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_month_info_gall`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 'January' id, 'January' month_name
	UNION ALL  
	SELECT 'February' id, 'February' month_name
	UNION ALL  
	SELECT 'March' id, 'March' month_name
	UNION ALL  
	SELECT 'April' id, 'April' month_name
	UNION ALL  
	SELECT 'May' id, 'May' month_name
	UNION ALL  
	SELECT 'June' id, 'June' month_name
	UNION ALL  
	SELECT 'July' id, 'July' month_name
	UNION ALL  
	SELECT 'August' id, 'August' month_name
	UNION ALL  
	SELECT 'September' id, 'September' month_name
	UNION ALL  
	SELECT 'October' id, 'October' month_name
	UNION ALL  
	SELECT 'November' id, 'November' month_name
	UNION ALL  
	SELECT 'December' id, 'December' month_name;                              
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_d`(p_notification_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_notification_member
	SET is_delete = 1,
			user_id = p_user_id
	WHERE notification_member_id = p_notification_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.notification_member_id, m.notification_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.notification_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.notification_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_notification_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY notification_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_gid`(p_notification_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT m.notification_member_id, m.notification_member_name, m.designation,  m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language
	FROM tbl_notification_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.notification_member_id = p_notification_member_id
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_i`(p_notification_member_id smallint, p_notification_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_notification_member m WHERE m.notification_member_id = p_notification_member_id) THEN
		SET p_notification_member_id = (SELECT IFNULL(MAX(notification_member_id), 0) + 1 FROM tbl_notification_member);

		INSERT INTO tbl_notification_member(notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_notification_member_id, p_notification_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_notification_member
		SET notification_member_name = p_notification_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE notification_member_id = p_notification_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_search`(p_notification_member_name varchar(150), p_designation varchar(150), p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.notification_member_id, m.notification_member_name, m.designation, m.phone, m.cell_phone, m.fax, m.email, m.address, m.comments, m.language_id, m.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",m.notification_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.notification_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_notification_member m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 
		WHERE m.language_id = p_language_id
		AND (p_notification_member_name = '' OR m.notification_member_name LIKE CONCAT('%',p_notification_member_name,'%'))
		AND (p_designation = '' OR m.designation LIKE CONCAT('%',p_designation,'%'))
		AND m.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT notification_member_id, notification_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY notification_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_notification_member_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_notification_member_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_notification_member_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.notification_member_id, m.notification_member_name
	FROM tbl_notification_member m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.notification_member_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_c`(p_language_id smallint, p_user_id int )
BEGIN
	#Routine body goes here...
	SELECT p.organization_id, p.organization_name
	FROM tbl_organization p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_d`(p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_organization
	SET is_delete = 1,
			user_id = p_user_id
	WHERE organization_id = p_organization_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.organization_id, p.organization_name, p.short_name, p.contact_person, p.designation, p.phone, p.cell_phone, p.fax, p.email, p.address, p.comments, p.language_id, p.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.organization_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.organization_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization p 
		INNER JOIN tbl_language l ON l.language_id = p.language_id 
		WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_id, organization_name, short_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_gid`(p_organization_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.organization_id, p.organization_name, p.short_name, p.contact_person, p.designation, p.phone, p.cell_phone, p.fax, p.email, p.address, p.comments, p.language_id, p.user_id, l.language
	FROM tbl_organization p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.organization_id = p_organization_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_i`(p_organization_id smallint, p_organization_name varchar(150), p_short_name varchar(5), p_contact_person varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_organization c WHERE c.organization_id = p_organization_id) THEN

		SET p_organization_id = (SELECT IFNULL(MAX(organization_id), 0) + 1 FROM tbl_organization);

		INSERT INTO tbl_organization(organization_id, organization_name, short_name, contact_person, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_organization_id, p_organization_name, p_short_name, p_contact_person, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_organization
		SET organization_name = p_organization_name, 
				short_name = p_short_name, 
				contact_person = p_contact_person, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE organization_id = p_organization_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_d`(p_organization_member_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_organization_member 
	SET is_delete = 1,
	user_id = p_user_id
	WHERE organization_member_id = p_organization_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.organization_member_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.organization_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization_member c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_member_id, organization_id, organization_name, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_gid`(p_organization_member_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
	FROM tbl_organization_member c 
	INNER JOIN tbl_language l ON l.language_id = c.language_id
	INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
	WHERE c.organization_member_id = p_organization_member_id		
	AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_i`(p_organization_member_id int, p_organization_id smallint, p_organization_member_name varchar(150), p_designation varchar(100), p_phone varchar(50), p_cell_phone varchar(50), p_fax varchar(50), p_email varchar(150), p_address varchar(250), p_comments varchar(250), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_organization_member c WHERE c.organization_member_id = p_organization_member_id) THEN

		SET p_organization_member_id = (SELECT IFNULL(MAX(organization_member_id), 0) + 1 FROM tbl_organization_member);

		INSERT INTO tbl_organization_member(organization_member_id, organization_id, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_organization_member_id, p_organization_id, p_organization_member_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_organization_member
		SET organization_id = p_organization_id, 
				organization_member_name = p_organization_member_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments, 
				language_id = p_language_id, 
				user_id = user_id
		WHERE organization_member_id = p_organization_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_member_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_member_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_member_search`(p_organization_id smallint, p_organization_member_name varchar(150), p_designation varchar(100),  p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.organization_member_id, c.organization_id, o.organization_name, c.organization_member_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid_member(",c.organization_member_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.organization_member_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_organization_member c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		INNER JOIN tbl_organization o ON o.organization_id = c.organization_id
		WHERE c.language_id = p_language_id						
		AND (p_organization_id = 0 OR c.organization_id = p_organization_id)
		AND (p_organization_member_name = '' OR c.organization_member_name LIKE CONCAT('%',p_organization_member_name,'%'))
		AND (p_designation = '' OR c.designation LIKE CONCAT('%',p_designation,'%'))
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT organization_member_id, organization_id, organization_name, organization_member_name, designation, phone, cell_phone, fax, email, address, comments, language_id, language, is_edit,is_delete FROM temp_all ORDER BY organization_member_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_organization_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_organization_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_organization_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.organization_id, m.organization_name
	FROM tbl_organization m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.organization_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_d`(p_others_org_ministry_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_others
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE others_org_ministry_id  = p_others_org_ministry_id ;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.others_org_ministry_id, c.others_org_ministry, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.others_org_ministry_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.others_org_ministry_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_others c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT others_org_ministry_id, others_org_ministry, language_id, language, is_edit, is_delete FROM temp_all ORDER BY others_org_ministry_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_gid`(p_others_org_ministry_id smallint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.others_org_ministry_id, c.others_org_ministry, c.language_id, c.user_id, l.language
		FROM tbl_others c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.others_org_ministry_id = p_others_org_ministry_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_i`(p_others_org_ministry_id bigint , p_others_org_ministry varchar (150) , p_language_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_others c WHERE c.others_org_ministry_id = p_others_org_ministry_id) THEN

		SET p_others_org_ministry_id = (SELECT IFNULL(MAX(others_org_ministry_id), 0) + 1 FROM tbl_others);

		INSERT INTO tbl_others(others_org_ministry_id, others_org_ministry, language_id, is_delete, user_id)
		VALUES(p_others_org_ministry_id, p_others_org_ministry, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_others
		SET others_org_ministry = p_others_org_ministry,
				language_id = p_language_id, 
				user_id = user_id
		WHERE others_org_ministry_id = p_others_org_ministry_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_others_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_others_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_others_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.others_org_ministry_id, m.others_org_ministry
	FROM tbl_others m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.others_org_ministry LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_d`(p_witness_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_other_witnessess
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE witness_id  = p_witness_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.witness_id, c.witness_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",c.witness_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",c.witness_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_other_witnessess c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id
		WHERE c.language_id = p_language_id		
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT witness_id, witness_name, designation, phone, cell_phone, fax, email, address, comments, language_id, user_id, language, is_edit, is_delete FROM temp_all ORDER BY witness_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_gid`(p_witness_id int, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.witness_id, c.witness_name, c.designation, c.phone, c.cell_phone, c.fax, c.email, c.address, c.comments, c.language_id, c.user_id, l.language
		FROM tbl_other_witnessess c 
		INNER JOIN tbl_language l ON l.language_id = c.language_id 
		WHERE c.witness_id = p_witness_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_i`(p_witness_id int , p_witness_name varchar (150) , p_designation varchar(100) , p_phone varchar(50) , p_cell_phone varchar(50) , p_fax varchar(50) , p_email varchar(150) , p_address varchar(250) , p_comments varchar(250) , p_language_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_other_witnessess c WHERE c.witness_id = p_witness_id) THEN

		SET p_witness_id = (SELECT IFNULL(MAX(witness_id), 0) + 1 FROM tbl_other_witnessess);

		INSERT INTO tbl_other_witnessess(witness_id, witness_name, designation, phone, cell_phone, fax, email, address, comments, language_id, is_delete, user_id)
		VALUES(p_witness_id, p_witness_name, p_designation, p_phone, p_cell_phone, p_fax, p_email, p_address, p_comments, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_other_witnessess
		SET witness_name = p_witness_name, 
				designation = p_designation, 
				phone = p_phone, 
				cell_phone = p_cell_phone, 
				fax = p_fax, 
				email = p_email, 
				address = p_address, 
				comments = p_comments,
				language_id = p_language_id, 
				user_id = user_id
		WHERE witness_id = p_witness_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_other_witnessess_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_other_witnessess_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_other_witnessess_token`(p_keyword varchar(20), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.witness_id, m.witness_name
	FROM tbl_other_witnessess m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 
	WHERE m.language_id = p_language_id
	AND m.witness_name LIKE CONCAT('%',p_keyword,'%')
	AND m.is_delete = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.parliament_id, p.parliament FROM tbl_parliament p
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.language_id = p_language_id
	AND p.is_delete = 0
	ORDER BY p.parliament_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_d`(p_parliament_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_parliament
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE parliament_id = p_parliament_id ;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	#DECLARE p_count int;
	SET @sql = NULL;

	#SET p_count = (SELECT COUNT(*) FROM tbl_parliament p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id);

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT CONCAT('<font style="font-family: "Siyam Rupali"">', p.parliament_id,"</font>") parliament_id, 
    case 
	     when p_language_id=2 then CONCAT("<font size='4'>", p.parliament,"</font>")
	     else p.parliament
    end as parliament
    , p.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.parliament_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.parliament_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_parliament p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, language_id, language, is_edit,is_delete FROM temp_all ORDER BY parliament_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_gid`(p_parliament_id smallint)
BEGIN
	#Routine body goes here...
	SELECT p.parliament_id, p.parliament, p.language_id, p.is_delete, l.language FROM tbl_parliament p
	INNER JOIN tbl_language l ON l.language_id = p.language_id
	WHERE p.parliament_id = p_parliament_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_parliament_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_parliament_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_parliament_i`(p_parliament_id smallint, p_parliament varchar(50), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_parliament p WHERE p.parliament_id = p_parliament_id) THEN

		SET p_parliament_id = (SELECT IFNULL(MAX(parliament_id), 0) + 1 FROM tbl_parliament);

		INSERT INTO tbl_parliament(parliament_id, parliament, language_id, is_delete, user_id)
		VALUES(p_parliament_id, p_parliament, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_parliament
		SET parliament = p_parliament, 
				language_id = p_language_id,
				user_id = p_user_id
				#is_delete = 0

		WHERE parliament_id = p_parliament_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_d`(p_rec_evidence_details_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_receive_evidence_d
	SET is_delete = 1,
			user_id = p_user_id
	WHERE rec_evidence_details_id  = p_rec_evidence_details_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_gall`(p_inquiry_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.inquiry_id, 
						DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
						t.rec_evidence_details_id, 
						t.evidence_type, 
						t.title, 
						t.submitted_by, 
						DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, 
						t.ref_no, 
						DATE_FORMAT(t.ref_date, '%d-%m-%Y') ref_date, 
						t.file_name,
						t.doc_type,
						t.create_date,
						t.user_id
		FROM tbl_receive_evidence_d t
		INNER JOIN tbl_receive_evidence_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
		WHERE t.inquiry_id = p_inquiry_id
		AND t.is_delete = 0
		/*
		SELECT t.doc_reg_id, DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, t.rec_evidence_details_id, t.evidence_type, t.title, t.submitted_by, DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, t.user_id
		FROM tbl_receive_evidence_d t
		INNER JOIN tbl_receive_evidence_master i ON i.doc_reg_id = t.doc_reg_id AND i.is_delete = 0		
		WHERE t.doc_reg_id = p_doc_reg_id
		AND t.is_delete = 0
		*/
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, rec_evidence_details_id, evidence_type, title, submitted_by, receive_date, ref_no, ref_date, file_name, doc_type, user_id FROM temp_all ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_gid`(p_rec_evidence_details_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT 	t.inquiry_id, 
					DATE_FORMAT(i.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
					t.rec_evidence_details_id, 
					t.evidence_type, 
					t.title, 
					t.submitted_by, 
					DATE_FORMAT(t.receive_date, '%d-%m-%Y') receive_date, 
					t.ref_no, 
					DATE_FORMAT(t.ref_date, '%d-%m-%Y') ref_date, 
					t.file_name,
					t.doc_type,
					t.create_date,
					t.user_id
	FROM tbl_receive_evidence_d t
	INNER JOIN tbl_receive_evidence_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0		
	WHERE t.rec_evidence_details_id = p_rec_evidence_details_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_d_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_d_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_d_i`(p_inquiry_id int , p_rec_evidence_details_id bigint , p_evidence_type varchar (100) , p_title longtext , p_submitted_by varchar (200) , p_receive_date varchar(10), p_ref_no varchar(150), p_ref_date varchar(10), p_doc_type varchar(50), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_name varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = 'C:/upload/';

	IF NOT EXISTS(SELECT * FROM tbl_receive_evidence_d c WHERE c.rec_evidence_details_id = p_rec_evidence_details_id) THEN		

		SET p_rec_evidence_details_id = (SELECT IFNULL(MAX(rec_evidence_details_id), 0) + 1 FROM tbl_receive_evidence_d);

		SET v_file_name = CONCAT(v_path, p_rec_evidence_details_id, '.', p_extension);

		INSERT INTO tbl_receive_evidence_d(inquiry_id, rec_evidence_details_id, evidence_type, title, submitted_by, receive_date, ref_no, ref_date, file_name, doc_type, create_date, is_delete, user_id)
		VALUES(p_inquiry_id, p_rec_evidence_details_id, p_evidence_type, p_title, p_submitted_by, STR_TO_DATE(p_receive_date,'%d-%m-%Y'), p_ref_no, STR_TO_DATE(p_ref_date, '%d-%m%Y'), v_file_name, p_doc_type, NOW(), 0, p_user_id);
	ELSE

		IF(TRIM(p_doc_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_name = CONCAT(v_path, p_rec_evidence_details_id, '.', p_extension);
	
			UPDATE tbl_receive_evidence_d
			SET inquiry_id = p_inquiry_id, 
					evidence_type = p_evidence_type, 
					title = p_title, 
					submitted_by = p_submitted_by, 
					receive_date = STR_TO_DATE(p_receive_date,'%d-%m-%Y'),
					ref_no = p_ref_no,
					ref_date = STR_TO_DATE(p_ref_date, '%d-%m%Y'),
					file_name = p_file_name, 
					doc_type = p_doc_type,
					user_id = p_user_id
			WHERE rec_evidence_details_id = p_rec_evidence_details_id;
		ELSE

			UPDATE tbl_receive_evidence_d
			SET inquiry_id = p_inquiry_id, 
					evidence_type = p_evidence_type, 
					title = p_title, 
					submitted_by = p_submitted_by, 
					receive_date = STR_TO_DATE(p_receive_date,'%d-%m-%Y'),
					ref_no = p_ref_no,
					ref_date = STR_TO_DATE(p_ref_date, '%d-%m%Y'),
					user_id = p_user_id
			WHERE rec_evidence_details_id = p_rec_evidence_details_id;
		END IF;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_d`(p_doc_reg_id int ,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_receive_evidence_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE doc_reg_id = p_doc_reg_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	t.inquiry_id, 
						DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
						t.user_id, 
						i.inquiry_no, 
						i.inquiry_title,
						DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date, 
						CONCAT("<input type='button' onClick='popupwinid(",t.inquiry_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",t.inquiry_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_receive_evidence_master t		
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.is_delete = 0
		AND i.language_id  = p_language_id 
		AND i.committee_id = p_committee_id
		ORDER BY t.receive_deadline_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, inquiry_no, inquiry_title, create_date, is_edit, is_delete FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_gid`(p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	
	SELECT 	i.inquiry_id, 
					DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 
					i.inquiry_no, 
					i.inquiry_title,
					DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date
	FROM tbl_inquiry_master i
	LEFT OUTER JOIN tbl_receive_evidence_master t ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0	
	WHERE i.inquiry_id = p_inquiry_id
	AND IFNULL(t.is_delete,0) = 0;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_i`(p_inquiry_id int , p_receive_deadline_date varchar(10), p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_receive_evidence_master c WHERE c.inquiry_id = p_inquiry_id) THEN				

		INSERT INTO tbl_receive_evidence_master(inquiry_id, receive_deadline_date, is_delete, user_id)
		VALUES(p_inquiry_id, STR_TO_DATE(p_receive_deadline_date,'%d-%m-%Y'), 0, p_user_id);
	ELSE
		UPDATE tbl_receive_evidence_master
		SET receive_deadline_date = STR_TO_DATE(p_receive_deadline_date,'%d-%m-%Y'), 
				user_id = p_user_id
		WHERE inquiry_id = p_inquiry_id;
	END IF;

	DELETE FROM tbl_receive_evidence_d WHERE inquiry_id = p_inquiry_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_receive_evidence_master_search`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_receive_evidence_master_search`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_receive_evidence_master_search`(p_inquiry_no varchar(50),  p_inquiry_title varchar(256), p_start_date varchar(10), p_end_date varchar(10), p_organization_name varchar(150), p_doc_reg_no varchar(150), p_deadline varchar(10), p_language_id smallint, p_committee_id smallint,  p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT 	i.inquiry_id, 
						DATE_FORMAT(t.receive_deadline_date,'%d-%m-%Y') receive_deadline_date, 						
						i.inquiry_no, 
						i.inquiry_title,
						DATE_FORMAT(i.create_date,'%d-%m-%Y') create_date, 
						CONCAT("<input type='button' onClick='popupwinid(",i.inquiry_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
						CONCAT("<input type='button' onClick='deleteid(",i.inquiry_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_inquiry_master i
		LEFT OUTER JOIN tbl_receive_evidence_master t ON i.inquiry_id = t.inquiry_id AND t.is_delete = 0
		LEFT OUTER JOIN tbl_language l ON l.language_id = i.language_id 
		LEFT OUTER JOIN tbl_committee c ON c.committee_id = i.committee_id AND c.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_organization io ON io.inquiry_id = i.inquiry_id AND io.is_delete = 0
		LEFT OUTER JOIN tbl_organization o ON o.organization_id = io.organization_id AND o.is_delete = 0
		LEFT OUTER JOIN tbl_inquiry_doc_tag dt ON dt.inquiry_id = i.inquiry_id AND dt.is_delete = 0
		LEFT OUTER JOIN tbl_document_registration_master d ON d.doc_reg_id = dt.doc_reg_id AND d.is_delete = 0
		WHERE i.language_id  = p_language_id 
		AND i.committee_id = p_committee_id
		AND (p_inquiry_no = '' OR i.inquiry_no LIKE CONCAT('%',p_inquiry_no,'%'))
		AND (p_inquiry_title = '' OR i.inquiry_title LIKE CONCAT('%',p_inquiry_title,'%'))	
		AND (p_organization_name = '' OR o.organization_name LIKE CONCAT('%', p_organization_name, '%'))
		AND (p_doc_reg_no = '' OR d.doc_reg_no LIKE CONCAT('%', p_doc_reg_no, '%'))
		AND (p_start_date = '' OR p_end_date = '' OR (i.create_date BETWEEN STR_TO_DATE(p_start_date,'%d-%m-%Y') AND STR_TO_DATE(p_end_date,'%d-%m-%Y') ))			
		AND (p_deadline = '' OR STR_TO_DATE(DATE_FORMAT(t.receive_deadline_date, '%d-%m-%Y'), '%d-%m-%Y') = STR_TO_DATE(p_deadline, '%d-%m-%Y'))
		AND i.is_delete = 0
		ORDER BY i.create_date DESC
	);
	SET @sql = CONCAT("SELECT inquiry_id, receive_deadline_date, inquiry_no, inquiry_title, create_date, is_edit, is_delete	FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_all_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_all_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_all_gall`(p_type varchar(20),  p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
CASE p_type
    WHEN 'cmember' THEN
			CALL tbl_record_of_decision_committee_gall(p_meeting_notice_id);
		
		WHEN 'logistic' THEN
			CALL tbl_record_of_decision_logistic_gall(p_meeting_notice_id);

		WHEN 'witness' THEN
			CALL tbl_record_of_decision_witness_gall(p_meeting_notice_id);

WHEN 'notification' THEN
			CALL tbl_record_of_decision_notification_gall(p_meeting_notice_id);

WHEN 'invitees' THEN
			CALL tbl_record_of_decision_invitees_gall(p_meeting_notice_id);

END CASE;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_all_token`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_all_token`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_all_token`(p_type varchar(20), p_keyword varchar(100), p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
	CASE p_type
			WHEN 'cmember' THEN
				SELECT mn.member_id id, cm.member_name name
				FROM tbl_meeting_notice_committee mn 
				INNER JOIN tbl_committee_member cm ON mn.member_id = cm.committee_member_id
				WHERE mn.metting_notice_id=p_meeting_notice_id AND cm.member_name LIKE CONCAT('%',p_keyword,'%');
			
			WHEN 'logistic' THEN
				SELECT mn.logistic_id id, lm.logistic_member_name name
				FROM tbl_meeting_notice_logistic mn
				INNER JOIN tbl_logistic_member lm ON lm.logistic_member_id = mn.logistic_id
				WHERE mn.metting_notice_id=p_meeting_notice_id AND lm.logistic_member_name LIKE CONCAT('%',p_keyword,'%');

			WHEN 'witness' THEN
			
				DROP TEMPORARY TABLE IF EXISTS temp_mem;
				CREATE TEMPORARY TABLE temp_mem AS
				(
					SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
					FROM tbl_meeting_notice_witness t				
					INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id
					WHERE t.is_delete = 0		
					AND t.witness_type = 'M'
					AND t.metting_notice_id = p_meeting_notice_id
				);

				INSERT INTO temp_mem
				SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
				FROM tbl_meeting_notice_witness t			
				INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id
				WHERE t.is_delete = 0
				AND t.witness_type = 'O'
				AND t.metting_notice_id = p_meeting_notice_id;

				INSERT INTO temp_mem
				SELECT t.witness_id, t.witness_type, o.witness_name member_name
				FROM tbl_meeting_notice_witness t			
				INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND o.is_delete = 0			
				WHERE t.metting_notice_id = p_meeting_notice_id
				AND t.witness_type = 'T'
				AND t.is_delete = 0;

				SELECT CONCAT(t.witness_id, '#', t.witness_type) id, t.member_name name
				FROM temp_mem t
				WHERE t.member_name LIKE CONCAT('%',p_keyword,'%');

	WHEN 'notification' THEN
				SELECT mn.notification_id id, lm.notification_member_name name
				FROM tbl_meeting_notice_notification mn
				INNER JOIN tbl_notification_member lm ON lm.notification_member_id = mn.notification_id
				WHERE mn.metting_notice_id = p_meeting_notice_id AND lm.notification_member_name LIKE CONCAT('%',p_keyword,'%');

	WHEN 'invitees' THEN
				SELECT mn.invitees_id id, lm.invitees_name name
				FROM tbl_meeting_notice_invitees mn
				INNER JOIN tbl_invitees lm ON lm.invitees_id = mn.invitees_id
				WHERE mn.metting_notice_id = p_meeting_notice_id AND lm.invitees_name LIKE CONCAT('%',p_keyword,'%');

	END CASE;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_chair_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_chair_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_chair_c`(p_meeting_notice_id int)
BEGIN
	#Routine body goes here...
SELECT mn.member_id id, cm.member_name name
FROM tbl_meeting_notice_committee mn 
INNER JOIN tbl_committee_member cm ON mn.member_id = cm.committee_member_id
WHERE mn.metting_notice_id=p_meeting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_d`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_committee
	WHERE metting_notice_id = p_metting_notice_id 
	AND member_id = p_member_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
		FROM tbl_record_of_decision_committee t
		INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
		INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT member_id, member_name FROM temp_all ORDER BY member_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_gid`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.member_id, i.member_name, t.user_id
	FROM tbl_record_of_decision_committee t
	INNER JOIN tbl_committee_member i ON i.committee_member_id = t.member_id AND i.is_delete = 0
	INNER JOIN tbl_committee c ON c.committee_id = i.committee_id
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.member_id = p_member_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_committee_i`(p_metting_notice_id int, p_member_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_committee c WHERE c.metting_notice_id = p_metting_notice_id AND c.member_id = p_member_id) THEN		
		INSERT INTO tbl_record_of_decision_committee(metting_notice_id, member_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_member_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_committee
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND member_id = p_member_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_d`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_inquiry_tag
	WHERE inquiry_id = p_inquiry_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, i.inquiry_no, t.metting_notice_id, 
		CASE 
				WHEN t.`status` = 0 THEN "On going"
				WHEN t.`status` = 1 THEN "Complete"
		END AS `status`,
		CASE 
			WHEN t.`status` = 0 THEN CONCAT("<input type='button' onClick='settled(",t.metting_notice_id,",", t.inquiry_id,");' name='basic' style='width:120px;background-color:green;' value='Click to Settle' class='gridbutton'/>") 
			WHEN t.`status` = 1 THEN CONCAT("<input type='button' onClick='settled(",t.metting_notice_id,",", t.inquiry_id,");' name='basic' style='width:120px;background-color:red;' value='Click to Unsettle' class='gridbutton'/>") 			
		END AS is_settle
		FROM tbl_record_of_decision_inquiry_tag t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no, status, is_settle FROM temp_all ORDER BY inquiry_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_gid`(p_metting_notice_id int, p_inquiry_id int,  p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.inquiry_id, i.inquiry_no, i.inquiry_title, t.metting_notice_id, t.`status`
	FROM tbl_record_of_decision_inquiry_tag t
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.inquiry_id =  p_inquiry_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_inquiry_tag_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_inquiry_tag_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_inquiry_tag_i`(p_metting_notice_id int, p_inquiry_id int , p_user_id int)
BEGIN
	#Routine body goes here...
# New Code By Pijush

	DECLARE p_status INT;
	SET p_status = (SELECT it.status FROM tbl_record_of_decision_inquiry_tag it WHERE it.metting_notice_id = p_metting_notice_id AND it.inquiry_id=p_inquiry_id);

	IF(p_status = 0) THEN
		UPDATE tbl_record_of_decision_inquiry_tag		
		SET user_id = p_user_id,
				status = 1
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	ELSE
		UPDATE tbl_record_of_decision_inquiry_tag		
		SET user_id = p_user_id,
				status = 0
		WHERE metting_notice_id = p_metting_notice_id 
		AND inquiry_id = p_inquiry_id;
	END IF;


# Old Code by taufiq
#	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_inquiry_tag c WHERE c.metting_notice_id = p_metting_notice_id AND c.inquiry_id = p_inquiry_id) THEN		
#		INSERT INTO tbl_record_of_decision_inquiry_tag(metting_notice_id, inquiry_id, status, is_delete, user_id)
#		VALUES(p_metting_notice_id, p_inquiry_id, p_status, 0, p_user_id);
#	ELSE
#		UPDATE tbl_record_of_decision_inquiry_tag		
#		SET user_id = p_user_id,
#				status = p_status
#		WHERE metting_notice_id = p_metting_notice_id 
#		AND inquiry_id = p_inquiry_id;
#	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_d`(p_metting_notice_id int, p_invitees_id bigint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_invitees
	WHERE invitees_id  = p_invitees_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name
		FROM tbl_record_of_decision_invitees t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0				
		INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id		
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT invitees_id, invitees_name FROM temp_all ORDER BY invitees_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_gid`(p_invitees_id bigint ,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.invitees_id, v.invitees_name, v.invitees_organization, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_record_of_decision_invitees t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_invitees v ON v.invitees_id = t.invitees_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.invitees_id = p_invitees_id
	AND l.language_id = m.language_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_invitees_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_invitees_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_invitees_i`(p_metting_notice_id int , p_invitees_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_invitees c WHERE c.invitees_id = p_invitees_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_record_of_decision_invitees(metting_notice_id, invitees_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_invitees_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_invitees
		SET user_id = p_user_id
		WHERE invitees_id = p_invitees_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_d`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_logistic
	WHERE metting_notice_id = p_metting_notice_id 
	AND logistic_id = p_logistic_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
		FROM tbl_record_of_decision_logistic t
		INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.logistic_id AND i.is_delete = 0		
		WHERE t.metting_notice_id = p_metting_notice_id 
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT logistic_id, logistic_member_name FROM temp_all ORDER BY logistic_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_gid`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.logistic_id, i.logistic_member_name, t.user_id
	FROM tbl_record_of_decision_logistic t
	INNER JOIN tbl_logistic_member i ON i.logistic_member_id = t.p_logistic_id AND i.is_delete = 0	
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.logistic_id = p_logistic_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_logistic_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_logistic_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_logistic_i`(p_metting_notice_id int, p_logistic_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_logistic c WHERE c.metting_notice_id = p_metting_notice_id AND c.logistic_id = p_logistic_id) THEN		
		INSERT INTO tbl_record_of_decision_logistic(metting_notice_id, logistic_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_logistic_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_logistic
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND logistic_id = p_logistic_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_d`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_record_of_decision_master
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id  = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_gall`(p_committee_id smallint, p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, t.chair_member_id, t.private_business, t.public_business, t.result_of_deliberation, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id, 
					CONCAT("<input type='button' onClick='popupwinid(",t.metting_notice_id, ");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",t.metting_notice_id, ");' name='basic' value='Delete' class='gridbutton'/>") is_delete 
		FROM tbl_record_of_decision_master t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0		
		INNER JOIN tbl_language l ON l.language_id = t.language_id
		INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
		WHERE t.language_id = p_language_id
		AND t.committee_id = p_committee_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, language, committee_id, committee_name, user_id, is_edit, is_delete FROM temp_all ORDER BY metting_notice_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_gid`(p_metting_notice_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, t.chair_member_id, t.private_business, t.public_business, t.result_of_deliberation, t.language_id, l.`language`, t.committee_id, c.committee_name, t.user_id
	FROM tbl_record_of_decision_master t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0		
	INNER JOIN tbl_language l ON l.language_id = t.language_id
	INNER JOIN tbl_committee c ON c.committee_id = t.committee_id AND c.is_delete = 0
	WHERE t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_master_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_master_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_master_i`(p_metting_notice_id int, p_chair_member_id smallint, p_committee varchar(100), p_witness varchar(100), p_logistic varchar(100), p_notification varchar(100), p_invitees varchar(100),  p_private_business longtext , p_public_business longtext, p_result_of_deliberation longtext , p_language_id smallint , p_committee_id smallint , p_user_id int)
BEGIN
	#Routine body goes here...

	DECLARE p_index INT;
	DECLARE occurance INT;
	DECLARE p_value INT;
	DECLARE p_value_text VARCHAR(100);

	#IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_master c WHERE c.metting_notice_id = p_metting_notice_id) THEN		
	#
	#	INSERT INTO tbl_record_of_decision_master(metting_notice_id, chair_member_id, private_business, public_business, result_of_deliberation, language_id, committee_id , is_delete, user_id)
	#	VALUES(p_metting_notice_id, p_chair_member_id, p_private_business, p_public_business, p_result_of_deliberation, p_language_id, p_committee_id , 0, p_user_id);
	#ELSE
		UPDATE tbl_record_of_decision_master
		SET chair_member_id = p_chair_member_id, 
				private_business = p_private_business, 
				public_business = p_public_business, 
				result_of_deliberation = p_result_of_deliberation, 
				language_id = p_language_id, 
				committee_id = p_committee_id, 
				flag = 1,
				user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id;
	#END IF;

	#committee Information

	DELETE FROM tbl_record_of_decision_committee WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_committee != '' THEN		
		SET occurance = LENGTH(p_committee)-LENGTH(replace(p_committee,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_committee, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_committee_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#committee Information

	#witness  Information

	DELETE FROM tbl_record_of_decision_witness WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_witness  != '' THEN		
		SET occurance = LENGTH(p_witness)-LENGTH(replace(p_witness,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value_text = SUBSTRING_INDEX(SUBSTRING_INDEX(p_witness, ',', p_index), ',', -1);
			SET p_value = CAST(SUBSTRING_INDEX(p_value_text, '#', 1) AS SIGNED);
			SET p_value_text = SUBSTRING_INDEX(p_value_text, '#', -1);
			CALL tbl_record_of_decision_witness_i(p_metting_notice_id, p_value_text, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#witness  Information


	#p_logistics Information

	DELETE FROM tbl_record_of_decision_logistic WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_logistic != '' THEN		
		SET occurance = LENGTH(p_logistic)-LENGTH(replace(p_logistic,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_logistic, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_logistic_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_logistics Information


	#p_notification Information

	DELETE FROM tbl_record_of_decision_notification WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_notification != '' THEN		
		SET occurance = LENGTH(p_notification)-LENGTH(replace(p_notification,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_notification, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_notification_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_notification Information


	#p_invitees Information

	DELETE FROM tbl_record_of_decision_invitees WHERE metting_notice_id = p_metting_notice_id;

	SET p_index = 1;
	IF p_invitees != '' THEN		
		SET occurance = LENGTH(p_invitees)-LENGTH(replace(p_invitees,',','')) + 1;		
		WHILE(p_index <= occurance) DO
			SET p_value = CAST(SUBSTRING_INDEX(SUBSTRING_INDEX(p_invitees, ',', p_index), ',', -1) AS SIGNED);
			CALL tbl_record_of_decision_invitees_i(p_metting_notice_id, p_value, p_user_id);
			SET p_index = p_index + 1;
		END WHILE;		
	END IF;

	#p_invitees Information



END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_d`(p_metting_notice_id int, p_notification_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_record_of_decision_notification
	WHERE notification_id  = p_notification_id
	AND metting_notice_id = p_metting_notice_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...		
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name
		FROM tbl_record_of_decision_notification t
		INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0						
		INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
		INNER JOIN tbl_language l ON l.language_id = v.language_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND l.language_id = m.language_id		
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT notification_id, notification_member_name FROM temp_all ORDER BY notification_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_gid`(p_metting_notice_id int, p_notification_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT t.metting_notice_id, m.gov_no_pix, t.notification_id, v.notification_member_name, v.designation, v.phone, v.cell_phone, v.fax, v.email, v.address, v.comments, v.language_id, l.`language`, t.user_id
	FROM tbl_record_of_decision_notification t
	INNER JOIN tbl_meeting_notice_master m ON m.metting_notice_id = t.metting_notice_id AND m.is_delete = 0			
	INNER JOIN tbl_notification_member v ON v.notification_member_id = t.notification_id AND v.is_delete = 0
	INNER JOIN tbl_language l ON l.language_id = v.language_id	
	WHERE t.notification_id = p_notification_id
	AND l.language_id = m.language_id
	AND t.metting_notice_id = p_metting_notice_id
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_notification_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_notification_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_notification_i`(p_metting_notice_id int , p_notification_id bigint , p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_notification c WHERE c.notification_id = p_notification_id AND c.metting_notice_id = p_metting_notice_id) THEN				

		INSERT INTO tbl_record_of_decision_notification(metting_notice_id, notification_id, is_delete, user_id)
		VALUES(p_metting_notice_id, p_notification_id, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_notification
		SET user_id = p_user_id
		WHERE notification_id = p_notification_id
		AND metting_notice_id = p_metting_notice_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_d`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_record_of_decision_witness
	SET is_delete = 1,
			user_id = p_user_id
	WHERE metting_notice_id = p_metting_notice_id 
	AND witness_id = p_witness_id
	AND witness_type = p_witness_type;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_gall`(p_metting_notice_id int)
BEGIN
	#Routine body goes here...	
	
	SET @sql = NULL;	
	DROP TEMPORARY TABLE IF EXISTS temp_mem;

	CREATE TEMPORARY TABLE temp_mem AS
	(
		SELECT t.witness_id, t.witness_type, m.ministry_member_name member_name
		FROM tbl_record_of_decision_witness t		
		INNER JOIN tbl_ministry_member m ON m.ministry_member_id = t.witness_id AND t.witness_type = 'M'
		WHERE t.is_delete = 0		
		AND t.metting_notice_id = p_metting_notice_id
	);

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.organization_member_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_organization_member o ON o.organization_member_id = t.witness_id AND t.witness_type = 'O'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_metting_notice_id;

	INSERT INTO temp_mem
	SELECT t.witness_id, t.witness_type, o.witness_name member_name
	FROM tbl_record_of_decision_witness t	
	INNER JOIN tbl_other_witnessess o ON o.witness_id = t.witness_id AND t.witness_type = 'T'
	WHERE t.is_delete = 0
	AND t.metting_notice_id = p_metting_notice_id;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.metting_notice_id, i.private_business, CONCAT(t.witness_id, '#', t.witness_type) witness_id, m.member_name
		FROM tbl_record_of_decision_witness t	
		INNER JOIN tbl_record_of_decision_master i ON i.metting_notice_id = t.metting_notice_id AND i.is_delete = 0
		INNER JOIN temp_mem m ON m.witness_id = t.witness_id
		WHERE t.metting_notice_id = p_metting_notice_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT witness_id, member_name FROM temp_all ORDER BY witness_id ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_gid`(p_metting_notice_id int, p_witness_id int, p_witness_type char(1), p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT t.metting_notice_id, t.witness_id, t.witness_type, t.user_id
	FROM tbl_record_of_decision_witness t
	WHERE t.metting_notice_id = p_metting_notice_id 
	AND t.witness_id = p_witness_id
	AND t.witness_type = p_witness_type
	AND t.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_record_of_decision_witness_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_record_of_decision_witness_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_record_of_decision_witness_i`(p_metting_notice_id int, p_witness_type char(1), p_witness_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_record_of_decision_witness c WHERE c.metting_notice_id = p_metting_notice_id AND c.witness_id = p_witness_id AND c.witness_type = p_witness_type) THEN		
		INSERT INTO tbl_record_of_decision_witness(metting_notice_id, witness_id, witness_type, is_delete, user_id)
		VALUES(p_metting_notice_id, p_witness_id, p_witness_type, 0, p_user_id);
	ELSE
		UPDATE tbl_record_of_decision_witness
		SET user_id = p_user_id
		WHERE metting_notice_id = p_metting_notice_id 
		AND witness_id = p_witness_id
		AND witness_type = p_witness_type;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_d`(p_request_evidence_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	UPDATE tbl_request_evidence
	SET is_delete = 1,
			user_id = p_user_id
	WHERE m.request_evidence_id = p_request_evidence_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT m.request_evidence_id, m.inquiry_id, i.inquiry_no, i.inquiry_title, m.metting_notice_id, DATE_FORMAT(m.request_date,'%d-%m-%Y') request_date, m.cover_letter, m.user_id,
					CONCAT("<input type='button' onClick='popupwinid(",m.request_evidence_id,");' name='basic' value='Edit' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",m.request_evidence_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete,
					CONCAT("<input type='button' onClick='printid(",m.request_evidence_id,");' name='basic' value='Print' class='gridbutton'/>") is_print
		FROM tbl_request_evidence m 
		INNER JOIN tbl_language l ON l.language_id = m.language_id 				
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
		WHERE m.language_id = p_language_id		
		AND m.committee_id = p_committee_id
		AND m.is_delete = 0		
		ORDER BY m.request_date DESC
	);
	SET @sql = CONCAT("SELECT request_evidence_id, inquiry_id, inquiry_no, inquiry_title, metting_notice_id, request_date, cover_letter, is_edit, is_delete, is_print FROM temp_all ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_gid`(p_request_evidence_id int, p_user_id int)
BEGIN
	#Routine body goes here...	
	SELECT m.request_evidence_id, m.inquiry_id, i.inquiry_no, i.inquiry_title, m.metting_notice_id, DATE_FORMAT(m.request_date,'%d-%m-%Y') request_date, m.cover_letter, m.user_id
	FROM tbl_request_evidence m 
	INNER JOIN tbl_language l ON l.language_id = m.language_id 			
	INNER JOIN tbl_inquiry_master i ON i.inquiry_id = m.inquiry_id AND i.is_delete = 0
	WHERE m.request_evidence_id = p_request_evidence_id			
	AND m.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_i`(p_request_evidence_id int, p_inquiry_id int, p_metting_notice_id int, p_request_date varchar(10), p_cover_letter longtext, p_committee_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_request_evidence m WHERE m.request_evidence_id = p_request_evidence_id) THEN
		SET p_request_evidence_id = (SELECT IFNULL(MAX(request_evidence_id), 0) + 1 FROM tbl_request_evidence);

		INSERT INTO tbl_request_evidence(request_evidence_id, inquiry_id, metting_notice_id, request_date, cover_letter, committee_id, language_id, is_delete, entry_date, user_id)
		VALUES(p_request_evidence_id, p_inquiry_id, p_metting_notice_id, STR_TO_DATE(p_request_date,'%d-%m-%Y'), p_cover_letter, p_committee_id, p_language_id, 0, NOW(), p_user_id);

	ELSE
		UPDATE tbl_request_evidence
		SET inquiry_id = p_inquiry_id, 
				metting_notice_id = p_metting_notice_id,				
				request_date = STR_TO_DATE(p_request_date,'%d-%m-%Y'), 
				cover_letter = p_cover_letter,
				committee_id = p_committee_id,
				language_id = p_language_id,
				user_id = p_user_id
		WHERE request_evidence_id = p_request_evidence_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_request_evidence_prepopulated`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_request_evidence_prepopulated`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_request_evidence_prepopulated`(p_request_evidence_id int)
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT t.inquiry_id, CONCAT(i.inquiry_no,':',i.inquiry_title) inquiry_no
		FROM tbl_request_evidence t
		INNER JOIN tbl_inquiry_master i ON i.inquiry_id = t.inquiry_id AND i.is_delete = 0
		WHERE t.request_evidence_id = p_request_evidence_id
		AND t.is_delete = 0
	);
	SET @sql = CONCAT("SELECT inquiry_id, inquiry_no FROM temp_all ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT s.session_id, s.`session` FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.language_id = p_language_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_c_by_parliament`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_c_by_parliament`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_c_by_parliament`(p_parliament_id smallint, p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT s.session_id, s.`session` FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.language_id = p_language_id
	AND s.parliament_id = p_parliament_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_d`(p_session_id smallint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_session
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE session_id = p_session_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.parliament_id, p.parliament, s.session_id, s.session, s.language_id, l.`language`,
					CONCAT("<input type='button' onClick='popupwinid(",s.session_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.session_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_session s 
		INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
		INNER JOIN tbl_language l ON s.language_id = l.language_id
		WHERE s.language_id = p_language_id
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, session_id, session, language_id, language, is_edit, is_delete FROM temp_all ORDER BY session_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;

END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_gid`(p_session_id smallint)
BEGIN
	#Routine body goes here...
	SELECT s.parliament_id, p.parliament, s.session_id, s.`session`, s.language_id, l.language FROM tbl_session s
	INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
	INNER JOIN tbl_language l ON s.language_id = l.language_id
	WHERE s.session_id = p_session_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_i`(p_parliament_id smallint, p_session_id smallint, p_session varchar(50), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
		IF NOT EXISTS(SELECT * FROM tbl_session s WHERE s.session_id = p_session_id) THEN

		SET p_session_id =	(SELECT IFNULL(MAX(session_id), 0) + 1 FROM tbl_session);

		INSERT INTO tbl_session(parliament_id, session_id, `session`, language_id, is_delete, user_id)
		VALUES(p_parliament_id, p_session_id, p_session, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_session
		SET parliament_id = p_parliament_id, 
				`session` = p_session,
				language_id = p_language_id,
				user_id  = p_user_id 
		WHERE session_id = p_session_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_session_par_id`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_session_par_id`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_session_par_id`(p_language_id smallint, p_parliament_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.parliament_id, p.parliament, s.session_id, s.`session`, s.language_id, l.`language`,
					CONCAT("<input type='button' onClick='popupwinid(",s.session_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.session_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_session s 
		INNER JOIN tbl_parliament p ON s.parliament_id = p.parliament_id
		INNER JOIN tbl_language l ON s.language_id = l.language_id
		WHERE s.language_id = p_language_id
		AND s.parliament_id = p_parliament_id
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT parliament_id, parliament, session_id, session, language_id, language, is_edit, is_delete FROM temp_all ORDER BY session_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_c`(p_language_id smallint, p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
		SELECT s.sub_committee_id, s.sub_committee_name
		FROM tbl_sub_committee s 
		INNER JOIN tbl_language l ON l.language_id = s.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
		WHERE s.language_id = p_language_id
		AND s.committee_id = p_committee_id 
		AND s.is_delete = 0		;
	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_d`(p_sub_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_sub_committee
	SET is_delete = 1,
	user_id = p_user_id
	WHERE sub_committee_id = p_sub_committee_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_gall`(p_language_id smallint, p_committee_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT s.sub_committee_id, s.committee_id, c.committee_name, s.sub_committee_name, s.user_id, s.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",s.sub_committee_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",s.sub_committee_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_sub_committee s 
		INNER JOIN tbl_language l ON l.language_id = s.language_id 
		INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
		WHERE s.language_id = p_language_id
		AND s.committee_id = p_committee_id 
		AND s.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT sub_committee_id, committee_id, committee_name, sub_committee_name, language_id, language, is_edit,is_delete FROM temp_all ORDER BY sub_committee_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_gid`(p_sub_committee_id smallint)
BEGIN
	#Routine body goes here...
	SELECT s.sub_committee_id, s.committee_id, c.committee_name, s.sub_committee_name, s.user_id, s.language_id, l.language
	FROM tbl_sub_committee s 
	INNER JOIN tbl_language l ON l.language_id = s.language_id 
	INNER JOIN tbl_committee c ON c.committee_id = s.committee_id
	WHERE s.sub_committee_id = p_sub_committee_id
	AND s.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_sub_committee_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_sub_committee_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_sub_committee_i`(p_sub_committee_id smallint, p_committee_id smallint, p_sub_committee_name varchar(150), p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_sub_committee c WHERE c.sub_committee_id = p_sub_committee_id) THEN

		SET p_sub_committee_id = (SELECT IFNULL(MAX(sub_committee_id), 0) + 1 FROM tbl_sub_committee);

		INSERT INTO tbl_sub_committee(sub_committee_id, committee_id, sub_committee_name, language_id, is_delete, user_id)
		VALUES(p_sub_committee_id, p_committee_id, p_sub_committee_name, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_sub_committee
		SET committee_id = p_committee_id, 
				sub_committee_name = p_sub_committee_name, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE sub_committee_id = p_sub_committee_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_d`(p_transcript_id bigint,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_transcript
	SET is_delete = 1,
	user_id = p_user_id	
	WHERE transcript_id = p_transcript_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_gall`(p_metting_notice_id int, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...	
	SET @sql = NULL;	

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT c.metting_notice_id, c.transcript_id, c.file_title, c.file_type, c.file_path, c.user_id, d.committee_name,
					CONCAT("<input type='button' onClick='play(\"",c.file_path,"\");' name='basic' value='Play' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button'  name='delete' onClick='deleteid(",c.transcript_id,",",c.metting_notice_id,");'  value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_transcript c 
		INNER JOIN tbl_meeting_notice_master m On m.metting_notice_id = c.metting_notice_id AND m.is_delete = 0
		INNER JOIN tbl_committee d ON d.committee_id = m.committee_id AND d.is_delete = 0
		WHERE c.metting_notice_id = p_metting_notice_id
		AND c.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT metting_notice_id, transcript_id, file_title, file_type, file_path, is_edit, is_delete FROM temp_all ORDER BY transcript_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_gid`(p_transcript_id bigint, p_user_id int)
BEGIN
		#Routine body goes here...
		SELECT c.metting_notice_id, c.transcript_id, c.file_title, c.file_type, c.file_path, c.user_id
		FROM tbl_transcript c 
		WHERE c.transcript_id = p_transcript_id
		AND c.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_transcript_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_transcript_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_transcript_i`(p_transcript_id bigint, p_metting_notice_id int, p_file_title varchar(200), p_file_type varchar (20), p_extension varchar(8), p_user_id int)
BEGIN
	#Routine body goes here...
	DECLARE v_file_path varchar(250);
	DECLARE v_path varchar(250);
	SET v_path = '../upload/';
	IF NOT EXISTS(SELECT * FROM tbl_transcript c WHERE c.transcript_id = p_transcript_id) THEN

		SET p_transcript_id = (SELECT IFNULL(MAX(transcript_id), 0) + 1 FROM tbl_transcript);

		SET v_file_path = CONCAT(v_path, p_transcript_id, '.', p_extension);

		INSERT INTO tbl_transcript(metting_notice_id, transcript_id, file_title, file_type, file_path, is_delete, user_id)
		VALUES(p_metting_notice_id, p_transcript_id, p_file_title, p_file_type, v_file_path, 0, p_user_id);
	ELSE
		IF(TRIM(p_file_type) != '' AND TRIM(p_extension) != '') THEN

			SET v_file_path = CONCAT(v_path, p_doc_reg_detail_id, '.', p_extension);

			UPDATE tbl_transcript
			SET metting_notice_id = p_metting_notice_id, 
					file_title = p_file_title, 
					file_path = v_file_path,
					file_type = p_file_type, 			
					user_id = p_user_id
			WHERE transcript_id = p_transcript_id;  
		ELSE      
			SET v_file_path = (SELECT file_path FROM tbl_transcript WHERE transcript_id = p_transcript_id);
			UPDATE tbl_transcript
			SET metting_notice_id = p_metting_notice_id, 
					file_title = p_file_title, 							
					user_id = p_user_id
			WHERE transcript_id = p_transcript_id;
		END IF;
	END IF;

	SELECT p_transcript_id transcript_id, v_file_path file_path;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_activation`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_activation`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_activation`(p_user_id int, p_is_active tinyint)
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET is_active = p_is_active,
			last_login = NOW()
	WHERE user_id = p_user_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_change_pwd`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_change_pwd`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_change_pwd`(p_user_id int, p_password varchar(128))
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET password = p_password,
			last_login = NOW()
	WHERE user_id = p_user_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_d`(p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_user_committee_permission
	WHERE committee_id = p_committee_id 
	AND user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_gall`(p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.committee_id, p.committee_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.committee_id, ",", p_user_id,",0);' value='Grant' name='basic' class='gridbutton'/>") is_edit
		FROM tbl_committee p 
		WHERE p.committee_id NOT IN (SELECT g.committee_id FROM tbl_user_committee_permission g WHERE g.user_id = p_user_id)		
		AND p.language_id = p_language_id
	);

	INSERT INTO temp_all
	SELECT p.committee_id, p.committee_name,
					CONCAT("<input type='button' onClick='popupwinid(", p.committee_id, ",", p_user_id,",1);' value='Revoke' name='basic' class='gridbuttonr' checked />") is_edit
	FROM tbl_committee p 
	WHERE p.committee_id IN (SELECT g.committee_id FROM tbl_user_committee_permission g WHERE g.user_id = p_user_id)
	AND p.language_id = p_language_id;

	SET @sql = CONCAT("SELECT committee_id, committee_name, is_edit FROM temp_all ORDER BY committee_name ");
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_gid`(p_user_id int, p_language_id smallint)
BEGIN
	#Routine body goes here...	
	SELECT p.committee_id, p.committee_name
	FROM tbl_user_committee_permission g 
	INNER JOIN tbl_committee p ON p.committee_id = g.committee_id 
	WHERE g.user_id = p_user_id
	AND p.language_id = p_language_id;	
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_committee_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_committee_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_committee_permission_i`(p_committee_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user_committee_permission c WHERE c.committee_id = p_committee_id AND c.user_id = p_user_id) THEN		
			INSERT INTO tbl_user_committee_permission(committee_id, user_id)
			VALUES(p_committee_id, p_user_id);			
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_gall`(p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;
	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.user_id, p.user_name, p.password, DATE_FORMAT(p.last_login,'%d-%m-%Y') last_login,
			CASE 
				WHEN p.is_active = 1 THEN CONCAT("<input type='button' onClick='activeid(",p.user_id,");' name='basic' value='Inactive' class='gridbutton'/>") 
				WHEN p.is_active = 0 THEN CONCAT("<input type='button' onClick='activeid(",p.user_id,");' name='basic' value='Active' class='gridbutton'/>") 
			END is_active,
					CONCAT("<input type='button' onClick='popupchangeid(",p.user_id,");' name='basic' value='Change Password' class='gridbutton' style='width: 130px;'/>") is_edit,
					CONCAT("<input type='button' onClick='popgroupid(",p.user_id,");' name='basic' value='Group Permission' class='gridbutton' style='width: 130px;'/>") is_delete,
					CONCAT("<input type='button' onClick='popcommitteeid(",p.user_id,");' name='basic' value='Committee Permission' class='gridbutton' style='width: 150px;'/>") is_committee
		FROM tbl_user p 	
	);
	SET @sql = CONCAT("SELECT user_id, user_name, last_login, is_active, is_edit, is_delete, is_committee FROM temp_all ORDER BY user_name ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gbyname`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gbyname`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_gbyname`(p_user_name varchar(128), p_password varchar(128))
BEGIN
	#Routine body goes here...
	UPDATE tbl_user
	SET last_login = NOW()
	WHERE user_name = p_user_name;

	SELECT u.user_id, u.user_name, u.password, u.last_login 
	FROM tbl_user u
	WHERE u.user_name = p_user_name
	AND u.password = p_password
	AND u.is_active = 1;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_gid`(p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.user_id, p.user_name, p.password, p.last_login, p.is_active 
	FROM tbl_user p
	WHERE p.user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_group_permission_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_group_permission_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_group_permission_d`(p_group_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	DELETE FROM tbl_user_group_permission
	WHERE group_id = p_group_id 
	AND user_id = p_user_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_group_permission_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_group_permission_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_group_permission_i`(p_group_id int, p_user_id int)
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user_group_permission c WHERE c.group_id = p_group_id AND c.user_id = p_user_id) THEN		
			INSERT INTO tbl_user_group_permission(group_id, user_id)
			VALUES(p_group_id, p_user_id);			
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_user_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_user_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_user_i`(p_user_id int, p_user_name varchar(128), p_password varchar(128))
BEGIN
	#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_user c WHERE c.user_id = p_user_id) THEN

		IF NOT EXISTS(SELECT * FROM tbl_user c WHERE c.user_name = p_user_name) THEN 
			SET p_user_id = (SELECT IFNULL(MAX(user_id), 0) + 1 FROM tbl_user);

			INSERT INTO tbl_user(user_id, user_name, password, last_login, is_active)
			VALUES(p_user_id, p_user_name, p_password, NOW(), 1);
		END IF;	
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_c`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_c`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_c`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.venue_id, p.venue_name
	FROM tbl_venue p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.language_id = p_language_id 
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_d`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_d`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_d`(p_venue_id int,  p_user_id int)
BEGIN
	#Routine body goes here...
	UPDATE tbl_venue
	SET is_delete = 1
	WHERE venue_id = p_venue_id;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_gall`(p_language_id smallint, p_user_id int, p_limit varchar(16))
BEGIN
	#Routine body goes here...
	SET @sql = NULL;

	DROP TEMPORARY TABLE IF EXISTS temp_all;
	CREATE TEMPORARY TABLE temp_all AS
	(
		SELECT p.venue_id, p.venue_name, p.language_id, l.language,
					CONCAT("<input type='button' onClick='popupwinid(",p.venue_id,");' name='basic' value='Change' class='gridbutton'/>") is_edit,
					CONCAT("<input type='button' onClick='deleteid(",p.venue_id,");' name='basic' value='Delete' class='gridbutton'/>") is_delete
		FROM tbl_venue p INNER JOIN tbl_language l ON l.language_id = p.language_id WHERE p.language_id = p_language_id
		AND p.is_delete = 0		
	);
	SET @sql = CONCAT("SELECT venue_id, venue_name, language_id, language, is_edit,is_delete FROM temp_all ORDER BY venue_id ", p_limit);
	PREPARE stmt FROM @sql;
	EXECUTE stmt;
	#select @sql;
# 
	DEALLOCATE PREPARE stmt;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_gid`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_gid`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_gid`(p_venue_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT p.venue_id, p.venue_name, p.language_id, l.language
	FROM tbl_venue p 
	INNER JOIN tbl_language l ON l.language_id = p.language_id 
	WHERE p.venue_id = p_venue_id
	AND p.is_delete = 0;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_venue_i`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_venue_i`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_venue_i`(p_venue_id smallint, p_venue_name varchar(200), p_language_id smallint, p_user_id int)
BEGIN
#Routine body goes here...
	IF NOT EXISTS(SELECT * FROM tbl_venue c WHERE c.venue_id = p_venue_id) THEN

		SET p_venue_id = (SELECT IFNULL(MAX(venue_id), 0) + 1 FROM tbl_venue);

		INSERT INTO tbl_venue(venue_id, venue_name, language_id, is_delete, user_id)
		VALUES(p_venue_id, p_venue_name, p_language_id, 0, p_user_id);

	ELSE
		UPDATE tbl_venue
		SET venue_name = p_venue_name, 
				language_id = p_language_id, 
				user_id = p_user_id
		WHERE venue_id = p_venue_id;
	END IF;
END
;;
DELIMITER ;

-- ----------------------------
-- Procedure structure for `tbl_year_info_gall`
-- ----------------------------
DROP PROCEDURE IF EXISTS `tbl_year_info_gall`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` PROCEDURE `tbl_year_info_gall`(p_language_id smallint, p_user_id int)
BEGIN
	#Routine body goes here...
	SELECT y.year_id, y.year_name FROM tbl_year_info y ORDER BY y.year_id ASC;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `bangla_english_number`
-- ----------------------------
DROP FUNCTION IF EXISTS `bangla_english_number`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `bangla_english_number`(p_bangla varchar(150)) RETURNS varchar(150) CHARSET utf8
BEGIN
	#Routine body goes here...
	declare ret_value varchar(150);
	declare len int;
	declare pos int;
  declare str varchar(6);
  set len= length(p_bangla);
  set pos=1;
	set ret_value='';
	while  len > 0 do 
     set  str = substr(p_bangla,pos,3);
		 case str
				when 'à§¦' THEN set ret_value= CONCAT(ret_value,'0');
				when 'à§§' THEN set ret_value= CONCAT(ret_value,'1');
				when 'à§¨' THEN set ret_value= CONCAT(ret_value,'2');

				when 'à§©' THEN set ret_value= CONCAT(ret_value,'3');
				when 'à§ª' THEN set ret_value= CONCAT(ret_value,'4');
				when 'à§«' THEN set ret_value= CONCAT(ret_value,'5');

				when 'à§¬' THEN set ret_value= CONCAT(ret_value,'6');
				when 'à§­' THEN set ret_value= CONCAT(ret_value,'7');
				when 'à§®' THEN set ret_value= CONCAT(ret_value,'8');
				when 'à§¯' THEN set ret_value= CONCAT(ret_value,'9');

     end case;
     #set ret_value= CONCAT(ret_value,str);
		 set pos=pos + 3;
		 set len=len - 6;
  end while;
	RETURN ret_value;
END
;;
DELIMITER ;

-- ----------------------------
-- Function structure for `english_bangla_number`
-- ----------------------------
DROP FUNCTION IF EXISTS `english_bangla_number`;
DELIMITER ;;
CREATE DEFINER=`sa`@`%` FUNCTION `english_bangla_number`(p_english varchar(150)) RETURNS varchar(150) CHARSET utf8
BEGIN
	#Routine body goes here...
	declare ret_value varchar(150);
	declare len int;
	declare pos int;
  declare str varchar(6);
  set len= length(p_english);
  set pos=1;
	set ret_value='';
	while  len > 0 do 
     set  str = substr(p_english,pos,1);
		 case str
				when '0' THEN set ret_value= CONCAT(ret_value,'à§¦');
				when '1' THEN set ret_value= CONCAT(ret_value,'à§§');
				when '2' THEN set ret_value= CONCAT(ret_value,'à§¨');

				when '3' THEN set ret_value= CONCAT(ret_value,'à§©');
				when '4' THEN set ret_value= CONCAT(ret_value,'à§ª');
				when '5' THEN set ret_value= CONCAT(ret_value,'à§«');

				when '6' THEN set ret_value= CONCAT(ret_value,'à§¬');
				when '7' THEN set ret_value= CONCAT(ret_value,'à§­');
				when '8' THEN set ret_value= CONCAT(ret_value,'à§®');
				when '9' THEN set ret_value= CONCAT(ret_value,'à§¯');

     end case;
     #set ret_value= CONCAT(ret_value,str);
		 set pos=pos + 1;
		 set len=len - 1;
  end while;
	RETURN ret_value;
END
;;
DELIMITER ;
