<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/record_decisions_info.php');
require_once('../model/meeting_notice_info.php');

$user_id = $_SESSION['user_id'];

if ($_GET['metting_notice_id'] != NULL) {
    $result = $meeting_notice_info->editrow(array($_GET['metting_notice_id'], $user_id));
}
if ($_GET['metting_notice_id'] != NULL) {
    $result1 = $record_decisions_info->editrow(array($_GET['metting_notice_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="css/grid.css" media="screen"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
        <link type="text/css" rel="stylesheet" href="css/bngrid.css" media="screen"/>
    <?php endif; ?>
    <link type='text/css' href='css/demo.css' rel='stylesheet' media='screen'/>
    <!--	<link type='text/css' href='css/menu.css' rel='stylesheet' media='screen' />-->
    <!-- Contact Form CSS files -->
    <style>
        tr:nth-child(odd) {
            background-color: #F2F1F0;
            color: #ffffff;
        }
    </style>
    <!--	<script type='text/javascript'>-->
    <!--			function settled(p, q)-->
    <!--			{			-->
    <!--				window.open("record_decisions_popup.php?metting_notice_id="+p+"&inquiry_id="+q,"_self");-->
    <!--			}-->
    <!--	</script>-->

    <script src="js/nic/nicEdit.js" type="text/javascript"></script>
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('area1');
            new nicEditor().panelInstance('area2');
            new nicEditor().panelInstance('area3');
            /*new nicEditor({fullPanel : true}).panelInstance('area2');
             new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
             new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
             new nicEditor({maxHeight : 100}).panelInstance('area5');*/
        });
    </script>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <style>
        #overlay_form {
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
        });
    </script>
    <!--Validation End-->
</head>
<body>
<?php
$xml = simplexml_load_file("xml/record_decisions_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
        $result_of_deliberation = $information->result_of_deliberation;
        $chair = $information->chair;
        $remarks = $information->remarks;
        $specialist_adviser = $information->specialist_adviser;
        $committee_officer = $information->committee_officer;
    }
}
?>
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<form id="formID" name="overlay_form" method="post" action="#">
<h2><?php echo $headingpopup; ?></h2>
</br>
<div>
    <input type="hidden" name="metting_notice_id"
           value="<?php echo isset($result->metting_notice_id) ? $result->metting_notice_id : ''; ?>"/>

    <div style="width:55%; float:left;">
        <div>
            <div style="width:32%; float:left;"><?php echo $ref_no; ?></div>
            <div style="width:35%; float:left;">
                <input id="name" type="text" name="p_gov_no_pix" style="width:95%;"
                       value="<?php echo $result->gov_no_pix . '/' . $result->gov_no_postfix ?>" disabled="disabled"/>
            </div>
            <div style="width:15%; float:left;"><?php echo $sitting_no; ?></div>
            <div style="width:10%; float:left;">
                <input id="name" type="text" name="sitting_no" style="width:70%;"
                       value="<?php echo isset($result->sitting_no) ? $result->sitting_no : ''; ?>"
                       disabled="disabled"/></div>
            <div class="divclear">
            </div>
        </div>
        <div>
            <div style="width:32%; float:left;"><?php echo $date; ?></div>
            <div style="width:20%; float:left;">
                <input style='width:70%' id="demo1" type="text" name="en_date"
                       value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>" disabled="disabled"/>
            </div>
            <div style="width:20%; float:left;">
                <input style='width:70%' id="demo1" type="text" name="en_date"
                       value="<?php echo isset($result->bn_date) ? $result->bn_date : ''; ?>" disabled="disabled"/>
            </div>
            <div style="width:10%; float:left;"><?php echo $time; ?></div>
            <div style="width:15%; float:left;">
                <input id="name" type="text" name="time" style="width:70%;"
                       value="<?php echo isset($result->time) ? $result->time : ''; ?>" disabled="disabled"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="width:32%; float:left;"><?php echo $venue; ?></div>
            <div style="width:60%; float:left;">
                <input id="name" type="text" name="time" style="width:70%;"
                       value="<?php echo isset($result->venue_name) ? $result->venue_name : ''; ?>"
                       disabled="disabled"/>

            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="width:32%; float:left;"><?php echo $chair; ?></div>
            <div style="width:60%; float:left;">
                <?php
                $id = $result1->chair_member_id;
                $metting_notice_id = $_GET['metting_notice_id'];
                echo $record_decisions_info->comboview_chairman($metting_notice_id, $id);
                ?>
            </div>
        </div>
        <div class="divclear"></div>
    </div>
</div>
<div style="width:35%; float:left;">
    <script>
        function settled(name, job) {
            //alert("Welcome " + name + ", the " + job);
            if (name == "" && job == "") {
                document.getElementById("txtHint").innerHTML = "";
                return;
            }
            if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
                xmlhttp = new XMLHttpRequest();
            }
            else {// code for IE6, IE5
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
                }
            }
            xmlhttp.open("GET", "record_of_decision_inquiry_data.php?metting_notice_id=" + name + "&inquiry_id=" + job, true);
            xmlhttp.send();
        }
    </script>
    <!--<input type="button" onclick="myFunction('Subrata','Software Engineer')" value="edit" />-->
    <div id="txtHint">
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        echo $record_decisions_info->gridview_record_inquiry($metting_notice_id);
        ?>
    </div>

</div>
<div class="divclear"></div>
</div>
<div>

<fieldset>
<legend>Attendence:</legend>
<div style="margin-top:5px;">
    <div style="float:left;width:18%;">
        <?php echo $commiitee_members; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" id="demo-input-facebook-theme2" name="committee"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_all("'cmember'", $metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("record_of_decision_data.php?type=cmember&meeting_notice_id=' . $metting_notice_id . '", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_all("'cmember'", $metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("record_of_decision_data.php?type=cmember&meeting_notice_id=' . $metting_notice_id . '", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme2").tokenInput("record_of_decision_data.php?type=cmember&meeting_notice_id=' . $metting_notice_id . '", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }
        ?>
    </div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:18%;margin-top:5px;">
        <?php echo $witness; ?>
    </div>
    <div style="float:left;width:80%;margin-top:5px;">
        <input type="text" id="demo-input-facebook-theme3" name="witness"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_all("'witness'", $metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("record_of_decision_data.php?type=witness&meeting_notice_id=' . $metting_notice_id . '", {';
                //echo'$("#demo-input-facebook-theme3").tokenInput("record_of_decision_data.php", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_all("'witness'", $metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("record_of_decision_data.php?type=witness&meeting_notice_id=' . $metting_notice_id . '", {';
//              		echo'$("#demo-input-facebook-theme3").tokenInput("record_of_decision_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme3").tokenInput("record_of_decision_data.php?type=witness&meeting_notice_id=' . $metting_notice_id . '", {';
//              echo'$("#demo-input-facebook-theme3").tokenInput("committee_member_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
</div>
<!-- SA-->

<div style="margin-top:5px;">
    <div style="float:left;width:18%;margin-top:5px;">
        <?php echo $specialist_adviser; ?>
    </div>
    <div style="float:left;width:80%;margin-top:5px;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme15"
               name="adviser"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_adviser($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_adviser($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>
<!--CO-->
<div style="margin-top:5px;">
    <div style="float:left;width:18%;">
        <?php echo $committee_officer; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme16"
               name="committee_officer"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_officer($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_officer($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>

<div style="margin-top:5px;display:none;">
    <div style="float:left;width:18%;">
        <?php echo $logistic_admin_ser; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" id="demo-input-facebook-theme4" name="logistic"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_all("'logistic'", $metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme4").tokenInput("record_of_decision_data.php?type=logistic&meeting_notice_id=' . $metting_notice_id . '", {';
//			   	 	echo'$("#demo-input-facebook-theme4").tokenInput("record_of_decision_data.php", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_all("'logistic'", $metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme4").tokenInput("record_of_decision_data.php?type=logistic&meeting_notice_id=' . $metting_notice_id . '", {';
                //echo'$("#demo-input-facebook-theme4").tokenInput("record_of_decision_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme4").tokenInput("record_of_decision_data.php?type=logistic&meeting_notice_id=' . $metting_notice_id . '", {';
            //echo'$("#demo-input-facebook-theme4").tokenInput("committee_member_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
</div>


<div style="margin-top:5px;display:none;">
    <div style="float:left;width:18%;">
        <?php echo $notification; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" id="demo-input-facebook-theme5" name="notification"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_all("'notification'", $metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("record_of_decision_data.php?type=notification&meeting_notice_id=' . $metting_notice_id . '", {';
                //echo'$("#demo-input-facebook-theme5").tokenInput("record_of_decision_data.php", {';
                echo 'prePopulate: ';
                echo $record_decisions_info->gridview_token_pree_popolated_all("'notification'", $metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("record_of_decision_data.php?type=notification&meeting_notice_id=' . $metting_notice_id . '", {';
                //echo'$("#demo-input-facebook-theme5").tokenInput("record_of_decision_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme5").tokenInput("record_of_decision_data.php?type=notification&meeting_notice_id=' . $metting_notice_id . '", {';
            //echo'$("#demo-input-facebook-theme5").tokenInput("committee_member_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
</div>


<div style="margin-top:5px;display:none;">
    <div style="float:left;width:18%;">
        <?php echo $invitees; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" id="demo-input-facebook-theme6" name="invitees"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($record_decisions_info->gridview_token_pree_popolated_all("'invitees'", $metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("record_of_decision_data.php?type=invitees&meeting_notice_id=' . $metting_notice_id . '", {';
                //echo'$("#demo-input-facebook-theme6").tokenInput("record_of_decision_data.php", {';
                echo 'prePopulate: ';

                echo $record_decisions_info->gridview_token_pree_popolated_all("'invitees'", $metting_notice_id);

                echo ',';
                echo 'theme: "facebook"';
                echo '});';
                echo '});';
                echo '</script>';

            } else {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme6").tokenInput("record_of_decision_data.php?type=invitees&meeting_notice_id=' . $metting_notice_id . '", {';
                //	echo'$("#demo-input-facebook-theme6").tokenInput("record_of_decision_data.php", {';
                echo 'theme: "facebook"';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme6").tokenInput("record_of_decision_data.php?type=invitees&meeting_notice_id=' . $metting_notice_id . '", {';
            //echo'$("#demo-input-facebook-theme6").tokenInput("committee_member_data.php", {';
            echo 'theme: "facebook"';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
</div>
<div class="divclear"></div>
</fieldset>
</div>
<div>
    <div style="float:left;width:18%;">
        <?php echo $private_business; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" id="area1"
                  name="private_business"><?php echo isset($result1->private_business) ? $result1->private_business : ''; ?></textarea>
    </div>
</div>
<div>
    <div style="float:left;width:18%;">
        <?php echo $public_business; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%;height:50px;" id="area2"
                  name="public_business"><?php echo isset($result1->public_business) ? $result1->public_business : ''; ?></textarea>
    </div>
</div>
<div>
    <div style="float:left;width:18%;">
        <?php echo $result_of_deliberation; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" id="area3"
                  name="result_of_deliberation"><?php echo isset($result1->result_of_deliberation) ? $result1->result_of_deliberation : ''; ?></textarea>
    </div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:18%;margin-top:5px;">
        <?php echo $remarks; ?>
    </div>
    <div style="float:left;width:80%;margin-top:5px;">
        <textarea style="width:100%; height:50px;" class='text-input bangla'
                  name="remarks"><?php echo isset($result1->remarks) ? $result1->remarks : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
<input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
<input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

<div>
    <div class="columnAbutton">
    </div>
    <div class="columnBbutton">
        &nbsp;&nbsp;
        <script>
            function myFunction() {
                window.close();
            }
        </script>
        <button onClick="myFunction()" class="popupbutton">Close</button>
        &nbsp;&nbsp;&nbsp;&nbsp;<input type="button" onclick='location.reload(false);' value="Refresh"
                                       class="popupbutton"/>
        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        <input id="send" name="btn_save_record" type="submit" value="Save" class="popupbutton"/>

    </div>
    <div class="divclear"></div>
</div>
<div class="divclear"></div>
</div>
</form>
</body>
</html>
