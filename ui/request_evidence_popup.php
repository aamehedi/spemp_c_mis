<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/request_evidence_info.php');
?>
<?php
$user_id = $_SESSION['user_id'];
if ($_GET['request_evidence_id'] != NULL) {
    $result = $request_evidence_info->editrow(array($_GET['request_evidence_id'], $user_id));
}
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
        <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <?php else: ?>
        <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="styles/jquery-ui-1.10.4.custom.min.css"/>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
    <?php
    $xml = simplexml_load_file("xml/request_evidance_popup.xml");
    foreach ($xml->information as $information) {
        if ($information->language_id == $_SESSION['language_id']) {
            $heading = $information->heading;
            $headingpopup = $information->headingpopup;
            $id = $information->id;
            $ministry_member_name = $information->ministry_member_name;
            $ministry_name = $information->ministry_name;
            $designation = $information->designation;
            $constituency = $information->constituency;
            $phone = $information->phone;
            $cell_phone = $information->cell_phone;
            $fax = $information->fax;
            $email = $information->email;
            $address = $information->address;
            $inquiry_no = $information->inquiry_no;
            $issue_date = $information->issue_date;
            $cover_letter = $information->cover_letter;
            $remarks = $information->remarks;
        }
    }
    ?>
    <!--validation start-->
    <link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="validation_form/template.css" type="text/css">
    <script src="validation_form/jquery-1.js" type="text/javascript"></script>
    <script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
    <script>
        jQuery(document).ready(function () {
            // binds form submission and fields to the validation engine
            jQuery("#formID").validationEngine();
            jQuery("#request_date").datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
                var weekend = date.getDay() == 5 || date.getDay() == 6;
                return [true, weekend ? 'myweekend' : ''];
            }});
        });
    </script>
    <!--Validation End-->
</head>
<body style="background-color:#FFFFFF;" onLoad="doOnLoad();">
<form id="formID" class="formular" name="overlay_form" method="post" action="#">
    <h2><?php echo $headingpopup; ?></h2>
    <br>

    <div>
        <!--Row1 -->
        <div>
            <div class="columnA"></div>
            <div class="columnB">
                <input type="hidden" name="request_evidence_id"
                       value="<?php echo isset($result->request_evidence_id) ? $result->request_evidence_id : '[Auto]'; ?>"/>

            </div>
            <div class="divclear"></div>
        </div>
        <!--Row2 -->

        <!--row3-->
        <div>
            <div class="columnA"><label><?php echo $inquiry_no; ?> *</label></div>
            <div class="columnB" style="width:50%;">
                <?php
                require_once('../model/create_inquiry_info.php');

                $language_id = $_SESSION['language_id'];
                $user_id = $_SESSION['user_id'];
                $committee_id = $_SESSION['committee_id'];
                $inquiry_id = isset($result->inquiry_id) ? $result->inquiry_id : '';
                $request_evidence_id = !empty($_GET['request_evidence_id']) ? $_GET['request_evidence_id'] : '';
                $create_inquiry_info->comboview_inquiry_for_request_evidence($inquiry_id, $language_id, $committee_id, $user_id, 0, $request_evidence_id);
                ?>
            </div>
            <input type="hidden" name="metting_notice_id" value="0"/>

            <div class="divclear"></div>
        </div>
        <!--row3 -->
        <div style="padding-top:2px;">
            <div class="columnA"><label><?php echo $issue_date; ?></label>
            </div>
            <div class="columnB">
                <div style="float:left;">
                    <input style='width:70%' id="request_date" type="text" class='validate[required] text-input bangla'
                           name="request_date"
                           value="<?php echo isset($result->request_date) ? $result->request_date : ''; ?>"/>
                </div>
                <div style="float:left;width:100px;padding-left:30px;">
                    &nbsp;
                </div>
                <div style="float:left;">
                    &nbsp;
                </div>
            </div>
            <div class="divclear"></div>
        </div>
        <!--row4-->
        <div>
            <div class="columnA"><label><?php echo $cover_letter; ?> *</label></div>
            <div class="columnB">
                <textarea style="width:500px; height:50px;" id="area1"
                          name="cover_letter"><?php echo isset($result->cover_letter) ? $result->cover_letter : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>

        <!--row4-->
        <div>
            <div class="columnA"><label><?php echo $remarks; ?> </label></div>
            <div class="columnB">
                <textarea style="width:500px; height:50px;"
                          name="remarks"><?php echo isset($result->remarks) ? $result->remarks : ''; ?></textarea>
            </div>
            <div class="divclear"></div>
        </div>
        </br>
        <input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
        <input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
        <input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

        <div>
            <div class="columnAbutton">
                <input id="send" name="btn_save_request" type="submit" value="Save" class="popupbutton"/>

            </div>
            <div class="columnBbutton">
                <script>
                    function myFunction() {
                        window.close();
                    }
                </script>
                &nbsp;&nbsp;
                <button onClick="myFunction()" class="popupbutton">close</button>
            </div>
            <div class="divclear"></div>
        </div>
        <div class="divclear"></div>
    </div>
</form>
</body>
</html>

