<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/user.php');
if ($_GET['user_id'] != NULL) {
    $result = $user->editrow(array($_GET['user_id']));
}
?>
<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <script language="javascript">
        function fncSubmit() {


            if (document.overlay_form.password.value == "") {
                alert('Please input Password');
                document.form1.txtPassword.focus();
                return false;
            }

            if (document.overlay_form.confirmpassword.value == "") {
                alert('Please input Confirm Password');
                document.form1.txtConPassword.focus();
                return false;
            }

            if (document.overlay_form.password.value != document.overlay_form.confirmpassword.value) {
                alert('Confirm Password Not Match');
                document.form1.txtConPassword.focus();
                return false;
            }
            document.form1.submit();
        }
    </script>
    <style>
        #overlay_form {
            position: absolute;
            /*border: 5px solid gray;*/
            padding: 0px;
        }

        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

        .close_box#close {
            background: fuchsia;
            color: #000;
            padding: 2px 5px;
            display: inline;
            position: absolute;
            right: 15px;
            border-radius: 3px;
            cursor: pointer;
        }

        .columnA {
            float: left;
            width: 150px;
        }

        .columnB {
            float: left;
        }

        .columnAbutton {
            float: left;
            width: 100px;
        }

        .columnBbutton {
            float: left;
        }

        .divclear {
            clear: both;
        }

        textarea {
            height: 60px;
            width: 227px;
        }
    </style>
</head>
<body>
<form id="overlay_form" name="overlay_form" method="post" action="#" OnSubmit="return fncSubmit();">
    <h2><?php echo 'Change User Password'; ?></h2>
    </br>
    <div>
        <div>
            <div class="columnA"><label><?php echo 'User Name' ?></label></div>
            <div class="columnB">
                <input id="user_name" type="text" name="user_name" value="<?php echo $result->user_name; ?>"
                       readonly="true"/>
                <input id="user_id" type="hidden" name="user_id" value="<?php echo $result->user_id; ?>"
                       readonly="true"/></div>
        </div>
        <div class="divclear"></div>
    </div>
    <div>
        <div class="columnA"><label><?php echo 'Password' ?></label></div>
        <div class="columnB">
            <input id="password" type="password" name="password"/></div>
        <div class="divclear"></div>
    </div>
    <div>
        <div class="columnA"><label><?php echo 'Confirm Password' ?></label></div>
        <div class="columnB">
            <input id="confirmpassword" type="password" name="confirm_password"/></div>
        <div class="divclear"></div>
    </div>
    <br/>

    <div>
        <div class="columnAbutton">
            <input id="send" name="btn_change" type="submit" value="Save" class="popupbutton"/>
        </div>
        <div class="columnBbutton">
            <script>
                function myFunction() {
                    window.close();
                }
            </script>

            &nbsp;&nbsp;&nbsp;&nbsp;
            <button onClick="myFunction()" class="popupbutton">Close</button>
        </div>
        <div class="divclear"></div>
    </div>
    <div class="divclear"></div>
    </div>
</form>
</body>
</html>
