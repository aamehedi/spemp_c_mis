<?php
session_start();
if ($_SESSION['user_id'] == NULL) {
    header('Location: sec_login.php');
    exit;
}
?>
<?php
require_once('../model/meeting_notice_info.php');
$user_id = $_SESSION['user_id'];
if ($_GET['metting_notice_id'] != NULL) {
    $result = $meeting_notice_info->editrow(array($_GET['metting_notice_id'], $user_id));
}
$language_id = $_SESSION['language_id'];
$committee_id = $_SESSION['committee_id'];
$result1 = $meeting_notice_info->govref(array($committee_id, $language_id));
?>
<!DOCTYPE html>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
<html>
<?php else: ?>
<html lang="bn">
<?php endif; ?>
<head>
<title></title>

<link rel="stylesheet" type="text/css" href="styles/jquery-ui-1.10.4.custom.min.css">
<link rel="stylesheet" type="text/css" href="styles/jquery.ui.timepicker.css">
<script src="js/nic/nicEdit.js" type="text/javascript"></script>
<script type="text/javascript">
    bkLib.onDomLoaded(function () {
        new nicEditor().panelInstance('area1');
        new nicEditor().panelInstance('area2');
        new nicEditor().panelInstance('area3');
        /*new nicEditor({fullPanel : true}).panelInstance('area2');
         new nicEditor({iconsPath : '../nicEditorIcons.gif'}).panelInstance('area3');
         new nicEditor({buttonList : ['fontSize','bold','italic','underline','strikeThrough','subscript','superscript','html','image']}).panelInstance('area4');
         new nicEditor({maxHeight : 100}).panelInstance('area5');*/
    });
</script>
<!-- datebangla-->
<script>
    function myFunction(str) {

        if (str == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "bndate.php?date=" + str, true);
        xmlhttp.send();
    }
</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<?php if (isset($_SESSION['language_id']) && $_SESSION['language_id'] === '1'): ?>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
<?php else: ?>
    <link href="Style/bnhome.css" rel="stylesheet" type="text/css"/>
<?php endif; ?>
<style>
    #overlay_form {
        /*border: 5px solid gray;*/
        padding: 0px;
    }

    .loader {
        display: block;
        border: 1px solid gray;
        width: 165px;
        text-align: center;
        padding: 6px;
        border-radius: 5px;
        text-decoration: none;
        /*margin: 0 auto;*/
    }

    .close_box#close {
        background: fuchsia;
        color: #000;
        padding: 2px 5px;
        display: inline;
        position: absolute;
        right: 15px;
        border-radius: 3px;
        cursor: pointer;
    }

    .columnA {
        float: left;
        width: 150px;
    }

    .columnB {
        float: left;

    }

    .columnAbutton {
        float: left;
        width: 100px;
    }

    .columnBbutton {
        float: left;
    }

    .divclear {
        clear: both;
    }

    textarea {
        height: 60px;
        width: 227px;
    }

    .validationRequired {
        display: inline !important;
        visibility: hidden;
        position: absolute;
        top: 0px;
        left: 0px;
        width: 85%;
    }
</style>
<!--validation start-->
<link rel="stylesheet" href="validation_form/validationEngine.css" type="text/css">
<link rel="stylesheet" href="validation_form/template.css" type="text/css">
<script src="validation_form/jquery-1.js" type="text/javascript"></script>
<script src="validation_form/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="validation_form/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" src="js/jquery-ui-1.10.4.custom.min.js"></script>
<script src="js/jquery.ui.timepicker.js"></script>

<script>

    jQuery(document).ready(function () {
        // binds form submission and fields to the validation engine
        jQuery("#formID").validationEngine();
        jQuery('#demo-input-facebook-theme5').addClass('validationRequired');
        jQuery('#en_date').datepicker({ dateFormat: "dd-mm-yy", firstDay: 0, beforeShowDay: function (date) {
            var weekend = date.getDay() == 5 || date.getDay() == 6;
            return [true, weekend ? 'myweekend' : ''];
        }});
        jQuery('#timepicker').timepicker({
            showPeriodLabels: false,
            onHourShow: startTimeOnHourShowCallback,
            // showOn: 'button'
            //  		button: '.timepicker_button_trigger'
            onMinuteShow: startTimeOnMinuteShowCallback
        });
        jQuery('#end_timepicker').timepicker({
            showPeriodLabels: false,
            onHourShow: endTimeOnHourShowCallback,
            // showOn: 'button'
            //  		button: '.timepicker_button_trigger'
            onMinuteShow: endTimeOnMinuteShowCallback
        });

        function startTimeOnHourShowCallback(hour) {

            var endTime = jQuery('#end_timepicker').val();
            // alert(endTime);
            if ((hour > 17) || (hour < 9)) {
                return false; // not valid
            }
            if (endTime != '') {
                endTime = endTime.split(':');
                if (hour > endTime[0]) {
                    return false;
                }
                ;
            }
            ;
            return true; // valid
        };

        function startTimeOnMinuteShowCallback(hour, minute) {
            var endTime = jQuery('#end_timepicker').val();
            if (endTime != '') {
                endTime = endTime.split(':');
                var endTimeInMinute = parseInt(endTime[0]) * 60 + parseInt(endTime[1]);
                var startTimeInMinute = parseInt(hour) * 60 + parseInt(minute);
                if (endTimeInMinute <= startTimeInMinute) {
                    return false;
                }
                ;
            }
            ;
            return true;
        };


        function endTimeOnHourShowCallback(hour) {

            var startTime = jQuery('#timepicker').val();

            if (startTime != '') {
                startTime = startTime.split(':');
                if (parseInt(startTime[1]) >= 55) {
                    if (hour <= startTime[0]) {
                        return false;
                    }
                    ;
                } else {
                    if (hour < startTime[0]) {
                        return false;
                    }
                    ;
                }
                ;
            }
            ;
            return true; // valid
        }

        function endTimeOnMinuteShowCallback(hour, minute) {
            var startTime = jQuery('#timepicker').val();
            if (startTime != '') {
                startTime = startTime.split(':');
                var startTimeInMinute = parseInt(startTime[0]) * 60 + parseInt(startTime[1]);
                var endTimeInMinute = parseInt(hour) * 60 + parseInt(minute);
                if (endTimeInMinute <= startTimeInMinute) {
                    return false;
                }
                ;
            }
            ;
            return true;
        };
    });

    function calculateDuration() {
        var startTime = jQuery('#timepicker').val();
        startTime = startTime.split(':');
        var startTimeInMinute = parseInt(startTime[0]) * 60 + parseInt(startTime[1]);
        var endTime = jQuery('#end_timepicker').val();
        var endTimeInMinute = startTimeInMinute + 30;
        if (endTime == '') {
            jQuery('#end_timepicker').val([Math.floor(endTimeInMinute / 60), endTimeInMinute % 60].join(':'));
        } else {
            endTime = endTime.split(':');
            endTimeInMinute = parseInt(endTime[0]) * 60 + parseInt(endTime[1]);
        }
        ;
        var diffInMinute = endTimeInMinute - startTimeInMinute;
        var hour = Math.floor(diffInMinute / 60);
        var minute = diffInMinute % 60;
        if (hour < 10) {
            hour = '0' + hour;
        }
        ;
        if (minute < 10) {
            minute = '0' + minute;
        }
        ;
        jQuery('#duration').val([hour, minute].join(':'));
    }

    function tokenInputRequired() {
        return 'abra ka dabara';
    }
</script>

<!--Validation End-->
</head>
<body>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.1/jquery.min.js"></script>-->
<script type="text/javascript" src="src/jquery.tokeninput.js"></script>
<link rel="stylesheet" href="styles/token-input.css" type="text/css"/>
<link rel="stylesheet" href="styles/token-input-facebook.css" type="text/css"/>
<?php
$xml = simplexml_load_file("xml/meeting_notice_popup.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $headingpopup = $information->headingpopup;
        $ref_no = $information->ref_no;
        $sitting_no = $information->sitting_no;
        $date = $information->date;
        $venue = $information->venue;
        $chair = $information->chair;
        $items = $information->items;
        $private_business = $information->private_business;
        $public_business = $information->public_business;
        $private_business = $information->private_business;
        $commiitee_members = $information->commiitee_members;
        $witness = $information->witness;
        $logistic_admin_ser = $information->logistic_admin_ser;
        $notification = $information->notification;
        $invitees = $information->invitees;
        $inquires = $information->inquires;
        $to = $information->to;
        $inquiry_no = $information->inquiry_no;
        $committee = $information->committee;
        $time = $information->time;
        $end_time = $information->end_time;
        $remarks = $information->remarks;
        $specialist_adviser = $information->specialist_adviser;
        $committee_officer = $information->committee_officer;

    }
}

?>
<form id="formID" class="formular" name="overlay_form" method="post" action="#">
<h2><?php echo $headingpopup; ?></h2>

<div>
    <input type="hidden" name="metting_notice_id"
           value="<?php echo isset($result->metting_notice_id) ? $result->metting_notice_id : ''; ?>"/>

    <div style="width:65%; float:left;">
        <div>
            <div style="width:23%; float:left;"><?php echo $ref_no; ?> *</div>
            <div style="width:37%; float:left;">
                <table style="width:100%;" cellspacing="0" cellpadding="0" border='0'>
                    <tr>
                        <td style="width:70%;">
                            <input id="name" type="text" name="p_gov_no_pix" style="width:95%;" class='validate[maxSize[50]] text-input bangla'
                                   value="<?php echo isset($result1->govt_ref_no) ? $result1->govt_ref_no : ''; ?>" />
                        </td>
                        <td style="width:30%;">
                            <input class='validate[maxSize[10]] text-input bangla' type="text"
                                   name="p_gov_no_postfix" style="width:95%;"
                                   value="<?php echo isset($result->gov_no_postfix) ? $result->gov_no_postfix : ''; ?>"/>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width:12%; float:left;"><?php echo $sitting_no; ?></div>
            <div style="width:18%; float:left;"><input id="name" readonly="true" type="text" class='bangla'
                                                       name="sitting_no" style="width:70%;"
                                                       value="<?php echo isset($result->sitting_no) ? $result->sitting_no : '[Auto]'; ?>"/>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="width:23%; float:left;"><?php echo $date; ?> *</div>
            <div style="width:25%; float:left;">
                <input style='width:70%' id="en_date" type="text" name="en_date" onchange="myFunction(this.value)"
                       value="<?php echo isset($result->en_date) ? $result->en_date : ''; ?>"/>
                <!-- <a href="javascript:NewCal('demo1','ddmmyyyy')"><img src="datepopup/cal.gif" width="16" height="16" border="0" alt="Pick a date"></a>-->
            </div>
            <div style="width:12%; float:left;">
                <div id="txtHint"><input id="name" type="text" class='validate[required,maxSize[80]] text-input bangla'
                                         name="bn_date"
                                         value="<?php echo isset($result->bn_date) ? $result->bn_date : ''; ?>"
                                         style="width:94%;" readonly="true"/></div>
            </div>
            <div style="width:12%; float:left;"><?php echo $time; ?> *</div>
            <!--
            // class='validate[timeFormat] text-input bangla'
            -->
            <div style="width:22%; float:left;"><input id="timepicker" id="name" type="text" name="time"
                                                       class='validate[required] text-input bangla' style="width:70%;"
                                                       readonly onchange="calculateDuration()"
                                                       value="<?php echo isset($result->time) ? $result->time : ''; ?>"/>
            </div>

            <!--
                              <div class="divclear"></div>
                              -->
        </div>

        <div style="margin-left: 60%; width: 74%;">
            <div style="width: 16%; float: left;clear: left;"><?php echo $end_time; ?> *</div>
            <div style="width: 30%; float:left;">
                <input id="end_timepicker" type="text" name="end_time" class='validate[required] text-input bangla'
                       style="width:70%;" readonly onchange="calculateDuration()"
                       value="<?php echo isset($result->end_time) ? $result->end_time : ''; ?>"/>
            </div>
            <div style="width: 18%; float: left;margin-left: 8%">Duration</div>
            <div style="width: 28%; float:left;">
                <input id="duration" type="text" name="duration" style="width:100%;" readonly
                       value="<?php echo isset($result->end_time) ? $result->end_time : ''; ?>"/>
            </div>
        </div>
    </div>
    <div style="width:35%; float:left; margin-top:-10px;">
        <div>
            <div style="width:25%; float:left;"><?php echo $venue; ?> *</div>
            <div style="width:75%; float:left;">
                <?php
                $id = isset($result->venue_id) ? $result->venue_id : '';
                $language_id = $_SESSION['language_id'];
                $user_id = $_SESSION['user_id'];
                echo '<select  name="venue_id" class="validate[required] text-input bangla">';
                echo $meeting_notice_info->comboview_venue($language_id, $user_id, $id);
                echo '</select>';
                ?>
            </div>
            <div class="divclear"></div>
        </div>
        <div>
            <div style="width:25%; float:left;"><?php echo $committee; ?></div>
            <div style="width:75%; float:left;">

                <?php
                $id = isset($result->sub_committee_id) ? $result->sub_committee_id : '';
                //echo $result->sub_committee_id;
                $language_id = $_SESSION['language_id'];
                $committee_id = $_SESSION['committee_id'];
                $user_id = $_SESSION['user_id'];
                if ($id != NULL) {
                    echo '<input type="hidden" value="' . $result->sub_committee_id . '" name="sub_committee_id"/>';
                    echo '<select  name="sub_committee_id" disabled="disabled">';
                } else {
                    echo '<select  name="sub_committee_id">';
                }
                echo $meeting_notice_info->comboview_committee($language_id, $committee_id, $user_id, $id);
                echo '</select>';
                ?>
            </div>
            <div class="divclear"></div>
        </div>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $inquires; ?> *
    </div>
    <div style="float:left;width:80%;position: relative;">
        <input type="text" class="validate[required] text-input bangla" id="demo-input-facebook-theme5"
               name="inqiuries"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_inquiry($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("inquiry_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_inquiry($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo ',';
                echo 'tokenLimit: 1';
                echo '});';
                echo '});';
                echo '</script>';

            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme5").tokenInput("inquiry_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo ',';
                echo 'tokenLimit: 1';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme5").tokenInput("inquiry_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo ',';
            echo 'tokenLimit: 1';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>

<div>
    <div><b><?php echo $items; ?></b></div>
    <div class="divclear"></div>
</div>

<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $private_business; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" class='validate[required] text-input bangla' id="area1"
                  name="private_business_before"><?php echo isset($result->private_business_before) ? $result->private_business_before : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:10px;">
    <div style="float:left;width:15%;">
        <?php echo $public_business; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" class='validate[required] text-input bangla' id="area2"
                  name="public_business"><?php echo isset($result->public_business) ? $result->public_business : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $private_business; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" class='validate[required] text-input bangla' id="area3"
                  name="private_business_after"><?php echo isset($result->private_business_after) ? $result->private_business_after : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $remarks; ?>
    </div>
    <div style="float:left;width:80%;">
        <textarea style="width:100%; height:50px;" class='text-input bangla'
                  name="remarks"><?php echo isset($result->remarks) ? $result->remarks : ''; ?></textarea>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $commiitee_members; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme2"
               name="committee"/>
        <?php
        $_SESSION['language_id'];
        $_SESSION['committee_id'];
        $metting_notice_id = $_GET['metting_notice_id'];
        $sub_committee_id = 0;
        $committee_id = $_SESSION['committee_id'];
        $language_id = $_SESSION['language_id'];

        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id) != ']') {

                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("committee_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';

            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("committee_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            if ($meeting_notice_info->gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("committee_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme2").tokenInput("committee_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $witness; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme3" name="witness"/>
        <?php
        // if (empty(NULL) === true) {
        // 	echo "Yes";
        // }
        // var_dump($meeting_notice_info->gridview_token_pree_popolated_witness($metting_notice_id));
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        // var_dump($meeting_notice_info->gridview_token_pree_popolated_witness($metting_notice_id));

        $witness = '';
        $witnesses = '';
        if (!empty($_SESSION['witness'])) {
            // var_dump($_SESSION['witness']);
            $witnesses = explode('****', $_SESSION['witness']);
            foreach ($witnesses as $key => $value) {
                if ($key == 0) {
                    $data = explode('^?^', $value);
                    $witness = '{id: "' . $data[0] . '#T", name: "' . $data[1] . '"}';
                } else {
                    $data = explode('^?^', $value);
                    $witness = $witness . '{id: "' . $data[0] . '#T", name: "' . $data[1] . '"}';
                    // $witness = $witness.',{name: "'.$value.'"}';
                }
            }
        }
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_witness($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_member_data.php", {';
                echo 'prePopulate: ';
                $prevWitnesses = $meeting_notice_info->gridview_token_pree_popolated_witness($metting_notice_id);
                $prevWitnesses = substr($prevWitnesses, 1);
                $prevWitnesses = substr($prevWitnesses, 0, -1);
                if (!empty($witness)) {
                    $witness = '[' . $prevWitnesses . ',' . $witness . ']';
                } else {
                    $witness = '[' . $prevWitnesses . ']';
                }
                echo $witness;
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme3").tokenInput("witness_member_data.php", {';
                if (!empty($witness)) {
                    $witness = '[' . $witness . ']';
                    echo 'prePopulate: ';
                    echo $witness;
                    echo ',';
                }
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme3").tokenInput("witness_member_data.php", {';
            if (!empty($witness)) {
                $witness = '[' . $witness . ']';
                echo 'prePopulate: ';
                echo $witness;
                echo ',';
            }
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('others_witness_popup.php?witness_id=0','','width=660,height=400,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=500,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $logistic_admin_ser; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme7"
               name="logistic"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_logistic($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme7").tokenInput("logistic_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_logistic($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme7").tokenInput("logistic_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            if ($meeting_notice_info->gridview_token_auto_popolated_logistic($language_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme7").tokenInput("logistic_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_auto_popolated_logistic($language_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme7").tokenInput("logistic_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $notification; ?>
    </div>

    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme11"
               name="notification"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];

        if ($meeting_notice_info->gridview_token_pree_popolated_notification($metting_notice_id) != ']') {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme11").tokenInput("notification_member_data.php", {';
            echo 'prePopulate: ';
            echo $meeting_notice_info->gridview_token_pree_popolated_notification($metting_notice_id);
            echo ',';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme11").tokenInput("notification_member_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        // if($metting_notice_id!=0)
        // {
        // 	if($meeting_notice_info->gridview_token_pree_popolated_notification($metting_notice_id)!=']')
        // 	{
        // 		echo '<script type="text/javascript">';
        //          		echo'$(document).ready(function() {';
        //    	 	echo'$("#demo-input-facebook-theme11").tokenInput("notification_member_data.php", {';
        // 		echo'prePopulate: ';
        // 	   	echo $meeting_notice_info->gridview_token_pree_popolated_notification($metting_notice_id);
        // 		echo',';
        // 		echo 'theme: "facebook"';
        // 		echo',';
        // 		echo 'preventDuplicates: true';
        //          		echo'});';
        //      			echo'});';
        //         			echo'</script>';
        // 	}
        // 	else
        // 	{
        // 		echo'<script type="text/javascript">';
        //            echo'$(document).ready(function() {';
        //            		echo'$("#demo-input-facebook-theme11").tokenInput("notification_member_data.php", {';
        //            		echo'theme: "facebook"';
        // 		echo',';
        // 		echo 'preventDuplicates: true';
        //   		echo'});';
        //            		echo'});';
        //   		echo'</script>';
        // 	}
        // }
        // else
        // {
        //   echo'<script type="text/javascript">';
        //            echo'$(document).ready(function() {';
        //            echo'$("#demo-input-facebook-theme11").tokenInput("notification_member_data.php", {';
        //            echo'theme: "facebook"';
        //   echo',';
        //   echo 'preventDuplicates: true';
        //   echo'});';
        //            echo'});';
        //   echo'</script>';
        // }

        ?>
    </div>
    <div style="float:left;width:5%;">
        <input type="button" name="file"
               onclick="javascript:void window.open('notification_member_popup.php?notification_member_id=0','','width=660,height=400,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=500,top=300');return false;"
               Value=" + "/>
    </div>
    <div class="divclear"></div>
</div>
<!-- SA-->

<div style="margin-top:5px;">
    <div style="float:left;width:15%;margin-top:5px;">
        <?php echo $specialist_adviser; ?>
    </div>
    <div style="float:left;width:80%;margin-top:5px;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme15"
               name="adviser"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_adviser($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_adviser($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme15").tokenInput("adviser_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>
<!--CO-->
<div style="margin-top:5px;">
    <div style="float:left;width:15%;">
        <?php echo $committee_officer; ?>
    </div>
    <div style="float:left;width:80%;">
        <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme16"
               name="committee_officer"/>
        <?php
        $metting_notice_id = $_GET['metting_notice_id'];
        $user_id = $_SESSION['user_id'];
        if ($metting_notice_id != 0) {
            if ($meeting_notice_info->gridview_token_pree_popolated_officer($metting_notice_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_pree_popolated_officer($metting_notice_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        } else {
            if ($meeting_notice_info->gridview_token_auto_popolated_officer($language_id, $committee_id) != ']') {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'prePopulate: ';
                echo $meeting_notice_info->gridview_token_auto_popolated_officer($language_id, $committee_id);
                echo ',';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            } else {
                echo '<script type="text/javascript">';
                echo '$(document).ready(function() {';
                echo '$("#demo-input-facebook-theme16").tokenInput("officer_member_data.php", {';
                echo 'theme: "facebook"';
                echo ',';
                echo 'preventDuplicates: true';
                echo '});';
                echo '});';
                echo '</script>';
            }
        }

        ?>
    </div>
    <div class="divclear"></div>
</div>


<div style="float:left;width:15%;display:none;">
    <?php echo $invitees; ?>
</div>
<div style="float:left;width:80%;display:none;">
    <input type="text" class='validate[required] text-input bangla' id="demo-input-facebook-theme10" name="invitees"/>
    <?php
    $metting_notice_id = $_GET['metting_notice_id'];
    $user_id = $_SESSION['user_id'];
    if ($metting_notice_id != 0) {
        if ($meeting_notice_info->gridview_token_pree_popolated_invitees($metting_notice_id) != ']') {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme10").tokenInput("invitees_data.php", {';
            echo 'prePopulate: ';
            echo $meeting_notice_info->gridview_token_pree_popolated_invitees($metting_notice_id);
            echo ',';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        } else {
            echo '<script type="text/javascript">';
            echo '$(document).ready(function() {';
            echo '$("#demo-input-facebook-theme10").tokenInput("invitees_data.php", {';
            echo 'theme: "facebook"';
            echo ',';
            echo 'preventDuplicates: true';
            echo '});';
            echo '});';
            echo '</script>';
        }
    } else {
        echo '<script type="text/javascript">';
        echo '$(document).ready(function() {';
        echo '$("#demo-input-facebook-theme10").tokenInput("invitees_data.php", {';
        echo 'theme: "facebook"';
        echo ',';
        echo 'preventDuplicates: true';
        echo '});';
        echo '});';
        echo '</script>';
    }

    ?>
</div>
<div style="float:left;width:5%; display:none;">
    <input type="button" name="file"
           onclick="javascript:void window.open('invites_popup.php?invitees_id=0','1376201640569','width=660,height=400,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=500,top=300');return false;"
           Value=" + "/>

    <div class="divclear"></div>
</div>
<div></div>
<input name="language_id" type="hidden" value="<?php echo $_SESSION['language_id']; ?>"/>
<input name="committee_id" type="hidden" value="<?php echo $_SESSION['committee_id']; ?>"/>
<input name="user_id" type="hidden" value="<?php echo $_SESSION['user_id']; ?>"/>

<div>
    <div style="float:left;width:25%;">
    </div>
    <div style="float:left;width:75%;">
        &nbsp;&nbsp;
        <script>
            function myFunction1() {
                window.close();
            }
        </script>
        <button onClick="myFunction1()" class="popupbutton">Close</button>
        &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;
        <input id="send" name="btn_save" type="submit" value="Save" class="popupbutton"/>
    </div>
</div>

</form>
</body>
</html>
