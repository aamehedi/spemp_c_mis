<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/session.css' rel='stylesheet' media='screen'/>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("session_popup.php?session_id=" + p, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:460px;dialogHeight:220px")
    }
    function popupwinis(p, q) {
        window.showModalDialog("session_popup.php?session_id=" + p + "&parliament_id=" + q, "", "dialogTop:325px;dialogLeft:445px;dialogWidth:460px;dialogHeight:220px")
    }
    function deleteid(p) {
        var r = confirm("Do You Want To Delete This Record Or Data?");
        //alert('Delete'+p);
        if (r == true) {
            window.location.href = "session_delete.php?session_id=" + p;
        }
        else {

        }
    }
</script>
<script>
    function show(str) {
        if (str == "") {
            document.getElementById("txtHint").innerHTML = "";
            return;
        }
        if (window.XMLHttpRequest) {// code for IE7+, Firefox, Chrome, Opera, Safari
            xmlhttp = new XMLHttpRequest();
        }
        else {// code for IE6, IE5
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange = function () {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET", "session_data.php?parliament_id=" + str, true);
        xmlhttp.send();
    }
</script>
<?php
require_once('../model/session_info.php');
?>
<?php
$xml = simplexml_load_file("xml/session.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {

        $heading = $information->heading;
        $id = $information->id;
        $session = $information->session;
        $parliament = $information->parliament;
    }
}

?>
<div id="head_info">
    <?php
    echo $heading;
    ?>
</div>
<div style="margin-top:3px;">
    <?php echo $parliament ?>
    <?php
    if (isset($_GET['parliament_id'])) {
        $id = $_GET['parliament_id'];
    }
    $language_id = $_SESSION['language_id'];
    $user_id = 1;
    echo $session_info->comboview($language_id, $user_id, $id);
    ?>

</div>

<div class="content" id="conteudo">
    <?php
    echo '<div id="txtHint">';
    if (isset($_GET['parliament_id'])) {
        $parliament_id = $_GET["parliament_id"];
        $language_id = $_SESSION['language_id'];
        echo '<input type="submit" onClick="popupwinis(0,' . $parliament_id . ');" name="basic" value="Add New" class="newbutton"/> ';
        echo '<br />';
        $user_id = 1;
        $user_name;
        $committe_id;
        $committe_name;
        $param = array($id, $session);
        echo $session_info->gridview($language_id, $parliament_id, $user_id, $param);
    }
    echo '</div>';
    ?>
    <?php

    ?>

</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>