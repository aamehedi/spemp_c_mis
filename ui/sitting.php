<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<?php require_once('../model/meeting_notice_info.php'); ?>


<?php
if (!empty($_GET['sittingNo'])) {

    // var_dump($meeting_notice_info->editrow(array($_GET['sittingNo'],'1')));
    $result = $meeting_notice_info->editrow(array($_GET['sittingNo'], '1'));
    $result1 = $meeting_notice_info->gridview_token_pree_popolated_inquiry($_GET['sittingNo']);
    $committeeReference = substr($result1, strpos($result1, 'name: "') + 7, strlen($result1) - (strpos($result1, 'name: "') + 10));
    $resultCommittee = $meeting_notice_info->raw_token_pree_popolated_committee($_GET['sittingNo'], $result->sub_committee_id, $_SESSION['committee_id'], $_SESSION['language_id']);
    $resultWitness = $meeting_notice_info->raw_token_pree_popolated_witness($_GET['sittingNo']);
    $resultLogistic = $meeting_notice_info->raw_token_pree_popolated_logistic($_GET['sittingNo']);
    $resultNotification = $meeting_notice_info->raw_token_pree_popolated_notification($_GET['sittingNo']);
    $resultAdviser = $meeting_notice_info->raw_token_pree_popolated_adviser($_GET['sittingNo']);
    $resultOfficer = $meeting_notice_info->raw_token_pree_popolated_officer($_GET['sittingNo']);

    $attributes = array();
    if ($_SESSION['language_id'] === '2') {
        $attributes = array('metting_notice_id' => 'Meeting Notice ID',
            'gov_no_pix' => 'সুত্র নং ',
            'sitting_no' => 'অধিবেশন নং ',
            'en_date' => 'তারিখ ',
            'time' => 'আরম্ভ সময় ',
            'venue_name' => 'স্থান ',
            'sub_committee_name' => 'সাব কমিটি ',
            'private_business_before' => 'সাধারণ আলোচ্য বিষয় ',
            'public_business' => 'প্রকাশ্য আলোচ্য বিষয় ',
            'private_business_after' => 'সাধারণ আলোচ্য বিষয় ',
            'remarks' => 'মন্তব্য সমূহ ',
            'end_time' => 'সমাপ্তি সময় ',
            'committee_reference' => 'কমিটি রেফারেন্স',
            'Committee Member(s)' => 'কমিটির সদস্য',
            'Witness' => 'সাক্ষী',
            'Logistic & Admin Ser' => 'লজিস্টিক ও অ্যাডমিন সার্ভিস',
            'Notification' => 'বিজ্ঞপ্তি',
            'Specialist Adviser' => 'বিশেষজ্ঞ উপদেষ্টা',
            'Committee Officer' => 'কমিটির কর্মকর্তা');
        ?>
        <style type="text/css">
            table.mGrid tr td {
                font-size: 20px !important;
            }
        </style>
    <?php


    } else {
        $attributes = array('metting_notice_id' => 'Meeting Notice ID',
            'gov_no_pix' => 'Ref No. ',
            'sitting_no' => 'Sitting No. ',
            'en_date' => 'Date ',
            'time' => 'Start Time ',
            'venue_name' => 'Venue ',
            'sub_committee_name' => 'Sub Committee ',
            'private_business_before' => 'Private Business ',
            'public_business' => 'Public Business ',
            'private_business_after' => 'Private Business ',
            'remarks' => 'Remarks ',
            'end_time' => 'End Time ',
            'committee_reference' => 'Committee Reference',
            'Committee Member(s)' => 'Committee Member(s)',
            'Witness' => 'Witness',
            'Logistic & Admin Ser' => 'Logistic & Admin Ser',
            'Notification' => 'Notification',
            'Specialist Adviser' => 'Specialist Adviser',
            'Committee Officer' => 'Committee Officer');

    }


//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////
//
//
//
//
//
//
//
//////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////

    ?>
    <style type="text/css">
        .back {
            -moz-box-shadow: inset 0px 1px 0px 0px #d197fe;
            -webkit-box-shadow: inset 0px 1px 0px 0px #d197fe;
            box-shadow: inset 0px 1px 0px 0px #d197fe;
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #a53df6), color-stop(1, #7c16cb));
            background: -moz-linear-gradient(center top, #a53df6 5%, #7c16cb 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#a53df6', endColorstr='#7c16cb');
            background-color: #a53df6;
            -webkit-border-top-left-radius: 9px;
            -moz-border-radius-topleft: 9px;
            border-top-left-radius: 9px;
            -webkit-border-top-right-radius: 9px;
            -moz-border-radius-topright: 9px;
            border-top-right-radius: 9px;
            -webkit-border-bottom-right-radius: 9px;
            -moz-border-radius-bottomright: 9px;
            border-bottom-right-radius: 9px;
            -webkit-border-bottom-left-radius: 9px;
            -moz-border-radius-bottomleft: 9px;
            border-bottom-left-radius: 9px;
            text-indent: 0;
            border: 1px solid #9c33ed;
            display: inline-block;
            color: #ffffff;
            font-family: Arial;
            font-size: 19px;
            font-weight: bold;
            font-style: normal;
            height: 35px;
            line-height: 35px;
            width: 113px;
            text-decoration: none;
            text-align: center;
            text-shadow: 1px 1px 0px #7d15cd;
            margin-top: 1%;

        }

        .back:hover {
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0.05, #7c16cb), color-stop(1, #a53df6));
            background: -moz-linear-gradient(center top, #7c16cb 5%, #a53df6 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#7c16cb', endColorstr='#a53df6');
            background-color: #7c16cb;
        }

        .back:active {
            position: relative;
            top: 1px;
        }

        .sittingNo {
            width: 40%;
            background-color: #7d15cd;
            color: #FFFFFF;
            height: 35px;
            float: left;
            margin-right: 48.9%;
            text-align: center;
            font-size: 1.5em;
            font-weight: bold;
            display: table;
            overflow: hidden;
            margin-top: 1%;
        }

        table {
            margin-top: 0px !important;
        }
    </style>
    <div class="sittingNo">
        <div style="display: table-cell; vertical-align: middle;">
            Sitting No. <?php echo $result->sitting_no; ?>
        </div>
    </div>
    <a href="index.php" class="back">BACK</a>
    <table class='mGrid'>
        <tr>
            <td><?php echo $attributes['gov_no_pix']; ?></td>
            <td><?php echo $result->gov_no_pix . ' ' . $result->gov_no_postfix; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['sitting_no']; ?></td>
            <td><?php echo $result->sitting_no; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['en_date']; ?></td>
            <td><?php echo $result->en_date . '  ' . $result->bn_date; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['time']; ?></td>
            <td><?php echo $result->time; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['end_time']; ?></td>
            <td><?php echo $result->end_time; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['venue_name']; ?></td>
            <td><?php echo $result->venue_name; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['sub_committee_name']; ?></td>
            <td><?php echo $result->sub_committee_name; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['committee_reference']; ?></td>
            <td><?php echo htmlspecialchars_decode($committeeReference); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['private_business_before']; ?></td>
            <td><?php echo $result->private_business_before; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['public_business']; ?></td>
            <td><?php echo $result->public_business; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['private_business_after']; ?></td>
            <td><?php echo $result->private_business_after; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['remarks']; ?></td>
            <td><?php echo $result->remarks; ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Committee Member(s)']; ?></td>
            <td><?php echo stripMysqliObject($resultCommittee); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Witness']; ?></td>
            <td><?php echo stripMysqliObject($resultWitness); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Logistic & Admin Ser']; ?></td>
            <td><?php echo stripMysqliObject($resultLogistic); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Notification']; ?></td>
            <td><?php echo stripMysqliObject($resultNotification); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Specialist Adviser']; ?></td>
            <td><?php echo stripMysqliObject($resultAdviser); ?></td>
        </tr>
        <tr>
            <td><?php echo $attributes['Committee Officer']; ?></td>
            <td><?php echo stripMysqliObject($resultOfficer); ?></td>
        </tr>


    </table>

<?php
} else {
    require_once '../dal/data_access.php';
    $data_access = new data_access();
    $result = $data_access->data_reader("tbl_meeting_notice_master_search_dashboard", array("'" . date('Y-m-d') . "'", "''", "''", 0, "''", "'" . $_SESSION['committee_id'] . "'", "'" . $_SESSION['language_id'] . "'", "'" . $_SESSION['user_id'] . "'", "''", "''"));
    ?>
    <style type="text/css">
        a {
            color: blue;
        }

        .header_contain > a {
            color: #FFFFFF;
        }

        td {
            text-align: center;
        }
    </style>
    <script type="text/javascript">
        function viewnoticeid(p) {
            //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
            window.showModalDialog("meeting_notice_popup_view_isssued.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
        function viewBriefingNote(p) {
            //window.open('create_inquiry_popup.php','1376201640569','width=900,height=600,toolbar=0,menubar=0,location=0,status=1,scrollbars=1,resizable=1,left=245,top=50');
            window.showModalDialog("briefing_note_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
            //return false;
        }
        function viewRecordOfDecision(p) {
            window.showModalDialog("record_decisions_popup.php?metting_notice_id=" + p, "", "dialogTop:center;dialogLeft:center;dialogWidth:1500px;dialogHeight:900px")
        }

    </script>
    <table class="mGrid" cellspacing="0">
        <tbody>
        <tr>
            <th>Sitting No.</th>
            <th>Date</th>
            <th>Venue</th>
            <th>Inquiries</th>
            <th>View Notice</th>
            <th>Briefing Note</th>
            <th>View Record of Decision</th>
        </tr>
        <?php
        while ($row = mysqli_fetch_assoc($result)) {
            ?>
            <tr>
                <td>
                    <a href="sitting.php?sittingNo=<?php echo $row['metting_notice_id']; ?>"><?php echo $row['sitting_no']; ?></a>
                </td>
                <td><?php echo ($_SESSION['language_id'] === '1') ? $row['en_date'] : $row['bn_date']; ?></td>
                <td><?php echo $row['venue_name']; ?></td>
                <td><?php echo $row['inquiry_no']; ?></td>
                <td><?php echo $row['is_view_notice']; ?></td>
                <td><?php echo $row['is_briefing_note']; ?></td>
                <td><?php echo $row['is_record_of_dec']; ?></td>

            </tr>
        <?php
        }
        ?>
        </tbody>
    </table>
<?php

}

function stripMysqliObject($mysqliObject)
{
    $first = true;
    $result = '';
    while ($row = mysqli_fetch_array($mysqliObject)) {
        if (!$first) {
            $result .= ', ';
        } else {
            $first = false;
        }
        $result .= htmlspecialchars_decode($row[1]);
    }
    return $result;
}