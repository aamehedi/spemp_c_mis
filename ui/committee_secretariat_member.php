<?php require_once('header.php'); ?>
<?php require_once('aside_left.php'); ?>
<?php require_once('menu.php'); ?>
<link type='text/css' href='css/ministry.css' rel='stylesheet' media='screen'/>
<?php
// mehedi
require_once('../model/committee_secretariat_member_info.php');
?>
<script type='text/javascript'>
    function popupwinid(p) {
        window.showModalDialog("committee_secretariat_member_popup.php?member_id=" + p, "", "dialogTop:200px;dialogLeft:300px;dialogWidth:850px;dialogHeight:450px");
    }
</script>
<?php
$xml = simplexml_load_file("xml/committee_secretariat_member.xml");
foreach ($xml->information as $information) {
    if ($information->language_id == $_SESSION['language_id']) {
        $heading = $information->heading;
        $member_name = $information->member_name;
        $designation = $information->designation;
        $committee_secretariat = $information->committee_secretariat;
    }
}
?>
<div id="head_info">
    <?php echo $heading; ?>
</div>
<input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
<div class="content" id="conteudo">
    <?php
    $language_id = $_SESSION['language_id'];
    $user_id = $_SESSION['user_id'];
    $param = array($member_name, $designation, $committee_secretariat);
    echo $committee_secretariat_member_info->gridview($language_id, $user_id, $param);
    ?>
</div>
</div>
<?php require_once('aside_right.php'); ?>
<?php require_once('footer.php'); ?>