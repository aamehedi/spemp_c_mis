<!DOCTYPE html>
<html>
<head>
    <title>SPO MIS</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <script type='text/javascript' src='js/jquery.js'></script>
    <script type='text/javascript' src='js/jquery.simplemodal.js'></script>
    <link href="Style/home.css" rel="stylesheet" type="text/css"/>
    <!--<link href="Style/Style.css" rel="stylesheet" type="text/css" />-->
    <link type='text/css' href='css/demo.css' rel='stylesheet' media='screen'/>
    <link type='text/css' href='css/menu.css' rel='stylesheet' media='screen'/>
    <!-- Contact Form CSS files -->
    <link type='text/css' href='css/basic.css' rel='stylesheet' media='screen'/>
    <link type='text/css' href='css/grid.css' rel='stylesheet' media='screen'/>
    <style>
        tr:nth-child(odd) {
            background-color: #F2F1F0;
            color: #ffffff;
        }
    </style>
    <style>
        .loader {
            display: block;
            border: 1px solid gray;
            width: 165px;
            text-align: center;
            padding: 6px;
            border-radius: 5px;
            text-decoration: none;
            /*margin: 0 auto;*/
        }

    </style>
    <script>
        function myFunctionEnglish() {
            document.getElementById('language').innerHTML = '<?php $language_id=1; echo $language_id;?>';
        }
    </script>
    <script>
        function myFunctionBangla() {
            document.getElementById('language').innerHTML = '<?php $language_id=2; echo $language_id;?>';
        }
    </script>
</head>
<body>
<div id="wrapper">
    <div class="header">
        <div class="header_text">Public Account Committee</div>
        <div class="header_font" id="boldStuff">
            <a onclick="myFunctionEnglish()"><img src="img/e_white.png"></a><a onclick="myFunctionBangla()"><img
                    src="img/b_grey.png"></a>
        </div>

        <div class="header_contain"><img src="img/m5.png" height="15" width="12">&nbsp;&nbsp;Admin &nbsp;&nbsp;&nbsp;&nbsp;Logout
        </div>
    </div>
    <div class="aside_left">

    </div>
    <div id="content_main">
        <div id="menu">
            <!--menu info head-->
            <ul id="nav" class="drop">
                <li><a href="#">System</a>
                    <ul>
                        <li><a href="#">Commitee</a></li>
                        <li><a href="#">Sub Commitee</a></li>
                        <li><a href="#">Parliament</a></li>
                        <li><a href="#">Ministry</a></li>
                        <li><a href="#">Organization</a></li>
                        <li><a href="#">Venue</a></li>
                    </ul>
                </li>

                <li><a href="#">Document Database</a>
                    <ul>
                        <li><a href="#">Document Registration</a></li>
                        <li><a href="#">Keys Area & Recommendation</a></li>
                    </ul>
                </li>
                <li><a href="#">Inquiry</a>
                    <ul>
                        <li><a href="#">Inquiry Scheduling</a></li>
                        <li><a href="#">Create Inquiry</a></li>
                        <li><a href="#">Receive Evidance</a></li>
                        <li><a href="#">Inquiry Report Creation</a></li>
                        <li><a href="#">Follow up Inquiry</a></li>
                    </ul>
                </li>
                <li><a href="#">Sitting </a>
                    <ul>
                        <li><a href="#">Meeting Notice</a></li>
                        <li><a href="#">Briefing Note</a></li>
                        <li><a href="#">Record & Decisions</a></li>
                        <li><a href="#">Multimedia & Verbatim Recording</a></li>
                    </ul>
                </li>
            </ul>

            <!--menu info foot-->
        </div>
        <script type='text/javascript'>
            function popupwinid(p) {
                $('<div></div>').load('parliament_info_ui.php?parliament_id=' + p).modal();
                return false;
            }
        </script>
        <?php
        require_once('../model/parliament_info.php');
        ?>

        <?php
        $str = "<b id='language'></b>";

        $xml = simplexml_load_file("xml/parliament.xml");
        foreach ($xml->information as $information) {
            $str;

            if ($information->language_id == isset($str)) {
                echo $str;
                $parliament_information = $information->parliament_information;
                $id = $information->id;
                $parliament = $information->parliament;
            }
        }
        ?>
        <div id="head_info">
            <?php
            echo $parliament_information;
            ?>
        </div>
        <div>
            <input type="button" onClick="popupwinid(0);" name="basic" value="Add New" class="newbutton"/>
        </div>

        <div class="content" id="conteudo">
            <?php
            $language_id = 1;
            $user_id = 1;
            //echo $user_id;
            $param = array($id, $parliament);
            echo $parliament_info->gridview($language_id, $user_id, $param);
            ?>
        </div>
    </div>
    <div class="aside_right">

    </div>
    <div class="footer">
    </div>
</div>
</body>
</html>

