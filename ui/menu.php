<div id="content_main">
    <div id="menu">
        <!--menu info head-->
        <ul id="nav" class="drop">
            <?php
            if ($_SESSION['is_parliament_member'] !== '0') {
                if ($_SESSION['language_id'] === '1') {
                    ?>

                    <!--menu info head-->

                    <li><a title="Sitting Info" target="_self" href="sitting.php">Sitting Info</a></li>
                    <li><a title="Ongong Inquiries" target="_self" href="ongoingInquiries.php">Ongong Inquiries</a></li>
                    <li><a title="Completed Inquiries" target="_self" href="completedInquiries.php">Completed
                            Inquiries</a></li>


                    <!--menu info foot-->

                <?php
                } else {
                    ?>
                    <!--menu info head-->
                    <li><a title="সভা তথ্য" target="_self" href="sitting.php">সভা তথ্য</a></li>
                    <li><a title="আসন্ন তদন্ত" target="_self" href="ongoingInquiries.php">আসন্ন তদন্ত</a></li>
                    <li><a title="নিষ্পন্ন তদন্ত" target="_self" href="completedInquiries.php">নিষ্পন্ন তদন্ত</a></li>


                    <!--menu info foot-->
                    <?php # code...
                }
            } else {
                if (empty($_SESSION['menu'])) {
                    require_once('../model/menu_info.php');
                    $menu_info = new menu_info;
                    $language_id = $_SESSION['language_id'];
                    $user_id = $_SESSION['user_id'];
                    $menu = $menu_info->get_menu_all($user_id, $language_id);
                    $_SESSION['menu'] = $menu;
                    echo $menu;
                } else {
                    echo $_SESSION['menu'];
                }

            }
            ?>
        </ul>
        <!--menu info foot-->
    </div>
	
