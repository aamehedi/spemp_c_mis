<?php

class data_access
{
    private $sever_name;
    private $db_name;
    private $user_name;
    private $password;

    public function __construct()
    {
        $this->sever_name = "192.168.3.20";
        $this->db_name = "dbmis";
        $this->user_name = "sa";
        $this->password = "Bipl1!23";
    }

    public function get_connection()
    {
        try {
            $mysqli = new mysqli($this->sever_name, $this->user_name, $this->password, $this->db_name);
            set_time_limit(30000);
            mysqli_set_charset($mysqli, 'utf8');
            return $mysqli;
        } catch (Exception $e) {
            throw new Exception('DB Erorr.' . $e->getMessage());
        }
    }

    public function data_reader($sp_name, $param)
    {
        return $this->_datareader($sp_name, $param);
    }

    public function execute_non_query($sp_name, $param)
    {
        return $this->_executenonquery($sp_name, $param);
    }

    private function _datareader($sp_name, $param)
    {
        try {
            $sql = $this->set_parameter($sp_name, $param);
//            var_dump($sql);
//            if($sp_name === 'tbl_document_registration_details_i'){
//                var_dump($sql);
//                die();
//            }
//			 die();

            $mysqli = $this->get_connection();
            // mysqli_set_charset($mysqli, 'utf8');
            $result = $mysqli->query($sql);
            // while ($row) {
            // 	# code...
            // }
            // var_dump($result);
            // die();
            mysqli_close($mysqli);
            return $result;
        } catch (Exception $e) {
            throw new Exception('DB Erorr.' . $e->getMessage());
        }
    }

    private function _executenonquery($sp_name, $param)
    {

        try {
            $sql = $this->set_parameter($sp_name, $param);
            $mysqli = $this->get_connection();
            $result = $mysqli->query($sql);
            mysqli_close($mysqli);
            return $result;
        } catch (Exception $e) {
            throw new Exception('DB Erorr.' . $e->getMessage());
        }
    }

    private function set_parameter($sp_name, $param)
    {
        //echo $param;
        $procedure = "call " . $sp_name . "(";
        if ($param != NULL) {
            $cntparam = count($param);
            for ($i = 0; $i < $cntparam; $i++) {
                //$i++;
                $procedure .= $param[$i] . ",";
            }
            $procedure = substr($procedure, 0, -1);
        }
        $procedure .= ")";
        return $procedure;
    }
}
?>