<?php
//ob_start();
require_once("../biz/session_info_biz.php");

class session_info
{

    function __construct()
    {
        if (isset($_POST['btn_save_session'])) {
            $this->build_param();
            echo("<script language=\"javascript\">");
            echo("opener.location.reload(true);");
            echo("window.close();");
            echo("</script>");
            //header('Location: session.php?parliament_id='.$_POST['parliament_id']);
            exit;
        }
        if (isset($_POST['btn_close'])) {

            //header("Location: ".$_SERVER['HTTP_SERVER'].$_POST['back']."");
            header('Location: session.php?parliament_id=' . $_POST['parliament_id']);
            exit;
        }

    }

    function build_param()
    {

        $session_info_biz = new session_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'-'.$value.'</br>';
                if (htmlspecialchars($key) == 'parliament_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'session_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'session') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $session_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $parliament_id, $user_id, $param)
    {
        $session_info_biz = new session_info_biz;
        return $session_info_biz->getall($language_id, $parliament_id, $user_id, $param);
    }

    function editrow($param)
    {
        $session_info_biz = new session_info_biz;
        return $session_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $session_info_biz = new session_info_biz;
        return $session_info_biz->delete($param);
    }

    function comboview($language_id, $user_id, $id)
    {
        $session_info_biz = new session_info_biz;
        return $session_info_biz->getcombo($language_id, $user_id, $id);
    }
}

$session_info = new session_info;
?>
