<?php
//ob_start();
require_once("../biz/commiittee_info_biz.php");

class commiittee_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $commiittee_info_biz = new commiittee_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'short_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'section') {
                    array_push($param, "'".htmlspecialchars($value)."'");
                } elseif (htmlspecialchars($key) == 'branch') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'contact_person') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $commiittee_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->delete($param);
    }

    function comboview_section($language_id, $user_id, $id)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getcombo_section($language_id, $user_id, $id);
    }

    function comboview_branch($language_id, $user_id, $id)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getcombo_branch($language_id, $user_id, $id);
    }
}

$commiittee_info = new commiittee_info;
?>
