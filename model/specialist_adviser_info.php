<?php
//ob_start();
require_once("../biz/specialist_adviser_biz.php");

class specialist_adviser_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $specialist_adviser_biz = new specialist_adviser_biz;
        try {

            $param = array();
            foreach ($_POST as $key => $value) {

                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'adviser_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'adviser_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'adviser_organization') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'constituency') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }

            if (isset($_POST['office'])) {
                array_push($param, "'" . htmlspecialchars($_POST['office']) . "'");
            }
            if (isset($_POST['is_active'])) {
                array_push($param, htmlspecialchars($_POST['is_active']));
            }

            return $specialist_adviser_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $specialist_adviser_biz = new specialist_adviser_biz;
        return $specialist_adviser_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $specialist_adviser_biz = new specialist_adviser_biz;
        return $specialist_adviser_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_info_biz = new ministry_info_biz;
        return $ministry_info_biz->delete($param);
    }

}

$specialist_adviser_info = new specialist_adviser_info;
?>
