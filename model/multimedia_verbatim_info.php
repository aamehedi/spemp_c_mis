<?php
//ob_start();
require_once("../biz/multimedia_verbatim_biz.php");

class multimedia_verbatim_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");

            }
            if (isset($_POST['upload'])) {
                $this->build_param();
            }
            if (isset($_POST['btn_delete'])) {
                $this->delete_param();
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function  delete_param()
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        try {
            $transcript_id = $_POST['transcript_id'];
            $user_id = $_POST['user_id'];
            echo $transcript_id . $user_id;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param()
    {

        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        try {
            $transcript_id = $_POST['transcript_id'];
            $metting_notice_id = $_POST['metting_notice_id'];
            $file_title = "'" . $_POST['file_title'] . "'";
            $file_name = $_FILES['file_name']['name'][0];
            $language_id = $_POST['language_id'];
            $user_id = $_POST['user_id'];
            //list($file, $ext) = explode('.',$file_name);
            $ext = pathinfo($_FILES['file_name']['name'][0], PATHINFO_EXTENSION);
            $file_type = "'" . $_FILES['file_name']['type'][0] . "'";
            $ext_t = "'" . $ext . "'";
            //echo $transcript_id.$metting_notice_id.$file_title.$file_type.$ext.$user_id;
            $param = array($transcript_id, $metting_notice_id, $file_title, $file_type, $ext_t, $user_id);
            $result = $multimedia_verbatim_biz->save($param);
            $tmpname = $_FILES['file_name']['tmp_name'][0];
            $fp = fopen($tmpname, 'r');
            $content = fread($fp, filesize($tmpname));
            $content = addslashes($content);
            fclose($fp);
            move_uploaded_file($tmpname, $result->file_path);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($metting_notice_id, $user_id)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->getall($metting_notice_id, $user_id);
    }

    function searchgridview($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->getone($param);
    }

    function rpt_multimedia_verbatim($param)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->rpt_multimedia_verbatim_getone($param);
    }

    function deleterow($param)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->delete($param);
    }

    function comboview($language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo($language_id, $user_id);
    }

    function comboview_venue($language_id, $user_id, $id)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->getcombo_venue($language_id, $user_id, $id);
    }

    function comboview_committee($language_id, $committee_id, $user_id, $id)
    {
        $multimedia_verbatim_biz = new multimedia_verbatim_biz;
        return $multimedia_verbatim_biz->getcombo_committee($language_id, $committee_id, $user_id, $id);
    }
}

$multimedia_verbatim_info = new multimedia_verbatim_info;
?>
