<?php
//ob_start();
require_once("../biz/sub_committee_member_biz.php");

class sub_committee_member_info
{

    function __construct()
    {
        if (isset($_POST['btn_save'])) {
            echo $_POST['btn_save'];
            $this->build_param();
            echo("<script language=\"javascript\">");
            echo("opener.location.reload(true);");
            echo("window.close();");
            echo("</script>");
        }
        if (isset($_POST['btn_close'])) {

            //header("Location: ".$_SERVER['HTTP_SERVER'].$_POST['back']."");
            header('Location: session.php?parliament_id=' . $_POST['parliament_id']);
            exit;
        }

    }

    function build_param()
    {

        $sub_committee_member_biz = new sub_committee_member_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo $key . '-' . $value . '</br>';
                if (htmlspecialchars($key) == 'sub_committee_member_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'sub_committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_member_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['is_active'])) {
                array_push($param, htmlspecialchars($_POST['is_active']));
            }
            return $sub_committee_member_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $committe_id, $user_id, $param)
    {
        $sub_committee_member_biz = new sub_committee_member_biz;
        return $sub_committee_member_biz->getall($language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $sub_committee_member_biz = new sub_committee_member_biz;
        return $sub_committee_member_biz->getone($param);
    }

    function deleterow($param)
    {
        $session_info_biz = new session_info_biz;
        return $session_info_biz->delete($param);
    }

    function comboview_sub_committee($language_id, $committee_id, $user_id, $id)
    {
        $sub_committee_member_biz = new sub_committee_member_biz;
        return $sub_committee_member_biz->getcombo_sub_committee($language_id, $committee_id, $user_id, $id);
    }

    function comboview_committee_member($committee_id, $language_id, $user_id, $id)
    {
        $sub_committee_member_biz = new sub_committee_member_biz;
        return $sub_committee_member_biz->getcombo_committee_member($committee_id, $language_id, $user_id, $id);
    }
}

$sub_committee_member_info = new sub_committee_member_info;
?>
