<?php
//ob_start();
require_once("../biz/venue_info_biz.php");

class venue_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $venue_info_biz = new venue_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $value;
                if (htmlspecialchars($key) == 'venue_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'venue_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $venue_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $venue_info_biz = new venue_info_biz;
        return $venue_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $venue_info_biz = new venue_info_biz;
        return $venue_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $venue_info_biz = new venue_info_biz;
        return $venue_info_biz->delete($param);
    }

}

$venue_info = new venue_info;
?>
