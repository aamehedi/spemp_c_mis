<?php
//ob_start();
//mehedi
require_once("../biz/committee_secretariat_member_biz.php");

class committee_secretariat_member_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                // if (!empty($_SESSION['parlimentMemberUserID'])) {
                // 	$_SESSION['parliamentMemberAdded'] = true;
                // }
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $committee_secretariat_member_biz = new committee_secretariat_member_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                // echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'member_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'member_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'active') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['user_id'])) {
                array_push($param, htmlspecialchars($_POST['user_id']));
            }
            if (isset($_POST['committee_secretariat_id'])) {
                array_push($param, htmlspecialchars($_POST['committee_secretariat_id']));
            }
            return $committee_secretariat_member_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $committee_secretariat_member_biz = new committee_secretariat_member_biz;
        return $committee_secretariat_member_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $committee_secretariat_member_biz = new committee_secretariat_member_biz;
        return $committee_secretariat_member_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_info_biz = new ministry_info_biz;
        return $ministry_info_biz->delete($param);
    }

    function getNotAddedUser($param)
    {
        $committee_secretariat_member_biz = new committee_secretariat_member_biz;
        return $committee_secretariat_member_biz->getNotAddedUser($param);
    }

    function comboview($language_id, $id)
    {
        $committee_secretariat_member_biz = new committee_secretariat_member_biz;
        return $committee_secretariat_member_biz->getcombo($language_id, $id);
    }
}

$committee_secretariat_member_info = new committee_secretariat_member_info;
?>
