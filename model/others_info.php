<?php
//ob_start();
require_once("../biz/others_info_biz.php");

class others_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $others_info_biz = new others_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                // if(htmlspecialchars($key)=='others_org_ministry_id')
                // {
                //     array_push($param, htmlspecialchars($value)== '' ? 0:htmlspecialchars($value));
                // }
                // elseif(htmlspecialchars($key)=='others_org_ministry')
                // {
                // 	array_push($param, "'".htmlspecialchars($value)."'");
                // }

                // elseif(htmlspecialchars($key)=='user_id')
                // {
                // 	array_push($param,htmlspecialchars($value));
                // }
                // elseif(htmlspecialchars($key)=='language_id')
                // {
                // 	array_push($param,htmlspecialchars($value));
                // }


                if (htmlspecialchars($key) == 'others_org_ministry_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'short_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'contact_person') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $others_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $others_info_biz = new others_info_biz;
        return $others_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $others_info_biz = new others_info_biz;
        return $others_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->delete($param);
    }

}

function comboview($language_id, $user_id)
{
    $commiittee_section_info_biz = new commiittee_section_info_biz;
    return $commiittee_section_info_biz->getcombo($language_id, $user_id);
}

$others_info = new others_info;
?>
