<?php
//ob_start();
require_once("../biz/commiittee_section_info_biz.php");

class commiittee_section_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $commiittee_section_info_biz = new commiittee_section_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo $key . '---' . $value . '<br>';
                if (htmlspecialchars($key) == 'section_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'section') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $commiittee_section_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $commiittee_info_biz = new commiittee_info_biz;
        return $commiittee_info_biz->delete($param);
    }

}

function comboview($language_id, $user_id)
{
    $commiittee_section_info_biz = new commiittee_section_info_biz;
    return $commiittee_section_info_biz->getcombo($language_id, $user_id);
}

$commiittee_section_info = new commiittee_section_info;
?>
