<?php
//ob_start();
require_once("../biz/notification_member_biz.php");

class notification_member_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {

            }
        }
    }

    function build_param()
    {

        $notification_member_info_biz = new notification_member_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'notification_member_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'notification_member_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'constituency') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $notification_member_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->getall($language_id, $user_id, $param);
    }

    function searchgridview($notification_member_name, $designation, $language_id, $user_id, $param)
    {
        $notification_member_info_biz = new notification_member_info_biz;
        return $notification_member_info_biz->getsearchall($notification_member_name, $designation, $language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $notification_member_info_biz = new notification_member_info_biz;
        return $notification_member_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $notification_member_info_biz = new notification_member_info_biz;
        return $notification_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->getcombo($language_id, $user_id);
    }
}

$notification_member_info = new notification_member_info;
?>
