<?php
//ob_start();
require_once("../biz/document_registration_biz.php");

class document_registration_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();

            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $document_registration_biz = new document_registration_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'doc_reg_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'doc_reg_no') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'doc_reg_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'doc_title_m') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'ref_no') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'ref_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['document_closed'])) {
                array_push($param, htmlspecialchars($_POST['document_closed']));
            } else {
                array_push($param, htmlspecialchars('0'));
            }
            $doc_reg_id = $document_registration_biz->save_master($param);

            if ($doc_reg_id != NULL) {
                $doc_reg_detail_id = $_POST['doc_reg_detail_id'];
                $doc_title = $_POST['doc_title'];
                var_dump($_FILES);
                foreach ($doc_title as $key => $v_doc_title) {
                    $t_doc_reg_detail_id = $doc_reg_detail_id[$key];
                    $t_doc_title = "'" . $v_doc_title . "'";
                    $fileName = $_FILES['file_name']['name'][$key];
                    $user_id = $_SESSION['user_id'];
                    if ($fileName != NULL) {
                        list($file, $ext) = explode('.', $fileName);
                        $t_ext = "'" . $ext . "'";
                        $filetype = "'" . $_FILES['file_name']['type'][$key] . "'";
                        $param_details = array($t_doc_reg_detail_id, $doc_reg_id, $t_doc_title, $filetype, $t_ext, $user_id);
//                      var_dump($filetype);
//                      var_dump($t_ext);
//                      die();
                        $result = $document_registration_biz->save_details_i($param_details);
                        $tmpName = $_FILES['file_name']['tmp_name'][$key];
                        $fp = fopen($tmpName, 'r');
                        $content = fread($fp, filesize($tmpName));
                        $content = addslashes($content);
                        fclose($fp);
                        move_uploaded_file($tmpName, $result->file_name);
                    } else {
                        $param_details = array($t_doc_reg_detail_id, $doc_reg_id, $t_doc_title, "''", "''", $user_id);
                        $result = $document_registration_biz->save_details_i($param_details);
                    }
                }

            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($doc_reg, $doc_reg_date_s, $doc_reg_date_end, $doc_t, $ref_no, $language_id, $committe_id, $user_id, $param)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getsearchall($doc_reg, $doc_reg_date_s, $doc_reg_date_end, $doc_t, $ref_no, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getone($param);
    }

    function editrow_details($param)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getone_details($param);
    }

    function deleterow($param)
    {
        $this->delete_param($param);
        echo("<script language=\"javascript\">");
        echo("window.close();");
        echo("</script>");
    }

    function delete_param($param)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->delete($param);

    }

    function comboview($language_id, $user_id, $id)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->getcombo($language_id, $user_id, $id);
    }
}

$document_registration_info = new document_registration_info;
?>
