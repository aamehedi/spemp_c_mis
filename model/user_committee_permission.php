<?php
//ob_start();
require_once("../biz/user_committee_permission_biz.php");

class user_committee_permission
{

    function __construct()
    {
    }

    function committee_permission($committee_id, $user_id, $status)
    {
        $v = '';
        $user_committee_permission_biz = new user_committee_permission_biz;
        if ($status == 0) {
            $v = $user_committee_permission_biz->save(array($committee_id, $user_id));
        } else if ($status == 1) {
            $v = $user_committee_permission_biz->delete(array($committee_id, $user_id));
        } else {
            $v = '';
        }
        //header('Location: http://www.example.com/');
    }

    function getgrid($user_id, $language_id)
    {
        $user_committee_permission_biz = new user_committee_permission_biz;
        return $user_committee_permission_biz->getall($user_id, $language_id);
    }

    function get_user_committee($user_id, $language_id)
    {
        $user_committee_permission_biz = new user_committee_permission_biz;
        return $user_committee_permission_biz->get_user_committee($user_id, $language_id);
    }

    function get_user_committee_count($user_id, $language_id)
    {
        $user_committee_permission_biz = new user_committee_permission_biz;
        return $user_committee_permission_biz->get_user_committee_count($user_id, $language_id);
    }
}

$user_committee_permission = new user_committee_permission;
?>