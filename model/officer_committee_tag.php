<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php
//ob_start();
require_once("../biz/officer_committee_tag_biz.php");

class officer_committee_tag
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param($officer_id, $committee_id, $type, $user_id)
    {
        $officer_committee_tag_biz = new officer_committee_tag_biz;
        try {
            return $officer_committee_tag_biz->save($officer_id, $committee_id, $type, $user_id);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($committee_id, $language_id, $user_id, $param)
    {
        $officer_committee_tag_biz = new officer_committee_tag_biz;
        return $officer_committee_tag_biz->getall($committee_id, $language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->delete($param);
    }

}

$officer_committee_tag = new officer_committee_tag;
?>
