<?php
//ob_start();
require_once("../biz/committee_member_biz.php");

class committee_member_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_save_n'])) {
                //echo $_POST['btn_save'];
                $this->build_param_n();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {

            }
        }
    }

    function build_param_n()
    {

        $committee_member_info_biz = new committee_member_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                //echo $key.'---'.$value.'<br>';

                if (htmlspecialchars($key) == 'member_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['member_no'])) {
                array_push($param, "'" . htmlspecialchars($_POST['member_no']) . "'");
            }
            var_dump($_POST['member_no']);
            if (isset($_POST['is_chairman'])) {
                array_push($param, htmlspecialchars($_POST['is_chairman']));
            } else {
                array_push($param, '0');
            }
            // var_dump($param);
            // die();
            return $committee_member_info_biz->save_n($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function build_param()
    {

        $committee_member_info_biz = new committee_member_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'committee_member_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'member_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $committee_member_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($committee_id, $language_id, $user_id, $param)
    {
        $committee_member_info_biz = new committee_member_info_biz;
        return $committee_member_info_biz->getall($committee_id, $language_id, $user_id, $param);
    }

    function searchgridview($organization_id, $organization_member_name, $designation, $language_id, $user_id, $param)
    {
        $committee_member_info_biz = new committee_member_info_biz;
        return $committee_member_info_biz->getsearchall($organization_id, $organization_member_name, $designation, $language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $committee_member_info_biz = new committee_member_info_biz;
        return $committee_member_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $committee_id, $member_id, $id)
    {
        $committee_member_info_biz = new committee_member_info_biz;
        return $committee_member_info_biz->getcombo($language_id, $committee_id, $member_id, $id);
    }
}

$committee_member_info = new committee_member_info;
?>
