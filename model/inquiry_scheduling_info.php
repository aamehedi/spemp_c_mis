<?php
//ob_start();
require_once("../biz/inquiry_scheduling_biz.php");

class inquiry_scheduling_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (isset($_POST['btn_save_inquery'])) {
                //echo $_POST['btn_save_inquery'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");

            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        try {

            $schedule_id = $_POST['schedule_id'];
            $inquiry_id = $_POST['inquiry_id'];
            $schedule_no = $_POST['schedule_no'];
            $schedule_date = $_POST['schedule_date'];
            $user_id = $_POST['user_id'];
            foreach ($schedule_no as $key => $v_schedule_no) {
                $v_schedule_date = "'" . $schedule_date[$key] . "'";
                //echo $v_schedule_no.$v_schedule_date.'<br>';
                $param = array($schedule_id, $inquiry_id, $v_schedule_no, $v_schedule_date, $user_id);
                $result = $inquiry_scheduling_biz->save($param);
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewdetails($inquiry_id, $user_id)
    {
        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        return $inquiry_scheduling_biz->getalldetails($inquiry_id, $user_id);
    }

    function gridviewdetails_issued($inquiry_id, $user_id)
    {
        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        return $inquiry_scheduling_biz->getalldetails_issued($inquiry_id, $user_id);
    }

    function searchgridview($inquiry_no, $inquiry_title, $schedule_month, $schedule_year, $doc_reg_no, $language_id, $committe_id, $user_id, $param)
    {
        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        return $inquiry_scheduling_biz->getsearchall($inquiry_no, $inquiry_title, $schedule_month, $schedule_year, $doc_reg_no, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        return $inquiry_scheduling_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id, $id)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->getcombo($language_id, $user_id, $id);
    }
}

$inquiry_scheduling_info = new inquiry_scheduling_info;
?>
