<?php
//ob_start();
require_once("../biz/record_decisions_biz.php");

class record_decisions_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save_record'])) {
                // echo $_POST['btn_save_record'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");

            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $record_decisions_biz = new record_decisions_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                // echo'<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                // echo $key.'---'.$value.'<br>';

                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'chair_member_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'witness') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'adviser') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'committee_officer') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'logistic') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'notification') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'invitees') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'private_business') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'public_business') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'result_of_deliberation') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $record_decisions_biz->save($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview_token_pree_popolated_adviser($metting_notice_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->get_gridview_token_pree_popolated_adviser($metting_notice_id);
    }

    function gridview_token_pree_popolated_officer($metting_notice_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->get_gridview_token_pree_popolated_officer($metting_notice_id);
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function record_decision_inquiry_save($metting_notice_id, $inquiry_id, $user_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->record_decision_inquiry_save_biz($metting_notice_id, $inquiry_id, $user_id);
    }

    function searchgridview($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->getone($param);
    }


    //gridview_token_committee_all
    function gridview_token_committee_all($type, $keyword, $meeting_notice_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->get_gridview_token_committee_all($type, $keyword, $meeting_notice_id);
    }

    //record of decision pre populated
    function gridview_token_pree_popolated_all($type, $metting_notice_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->get_gridview_token_pree_popolated_all($type, $metting_notice_id);
    }


    function comboview_chairman($metting_notice_id, $id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->getcombo_chairman($metting_notice_id, $id);
    }

    function comboview_venue($language_id, $user_id, $id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->getcombo_venue($language_id, $user_id, $id);
    }

    function comboview_committee($language_id, $committee_id, $user_id, $id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getcombo_committee($language_id, $committee_id, $user_id, $id);
    }

    function gridview_record_inquiry($metting_notice_id)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->getall_record_inquiry($metting_notice_id);
    }

    function rpt_record_of_decision_getone($param)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->rpt_record_of_decision_getone($param);
    }

    function record_issued($param)
    {
        $record_decisions_biz = new record_decisions_biz;
        return $record_decisions_biz->record_issued_value($param);
    }
}

$record_decisions_info = new record_decisions_info;
?>
