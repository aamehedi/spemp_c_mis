<?php
//ob_start();
require_once("../biz/receive_evidance_biz.php");

class receive_evidance_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {

                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $receive_evidance_biz = new receive_evidance_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'inquiry_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'receive_deadline_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            $receive_evidance_biz->save_master($param);
            //echo $inquiry_id;

            $inquiry_id = $_POST['inquiry_id'];
            $rec_evidence_details_id = $_POST['rec_evidence_details_id'];
            $evidence_type = $_POST['evidence_type'];
            $submitted_by = $_POST['submitted_by'];
            $title = $_POST['title'];
            $receive_date = $_POST['receive_date'];
            $ref_no = $_POST['ref_no'];
            $ref_date = $_POST['ref_date'];
            //echo $_FILES['file_name']['name'][0];
            //$ext = pathinfo($_FILES['file_name']['name'][0], PATHINFO_EXTENSION);
            //$file_type="'".$_FILES['file_name']['type'][0]."'";
            foreach ($rec_evidence_details_id as $key => $v_rec_evidence_details_id) {
                $t_evidence_type = "'" . $evidence_type[$key] . "'";
                $t_submitted_by = "'" . $submitted_by[$key] . "'";
                $t_title = "'" . $title[$key] . "'";
                $t_receive_date = "'" . $receive_date[$key] . "'";
                $t_ref_no = "'" . $ref_no[$key] . "'";
                $t_ref_date = "'" . $ref_date[$key] . "'";
                $file_type = "'" . $_FILES['file_name']['type'][$key] . "'";
                $ext = "'" . pathinfo($_FILES['file_name']['name'][$key], PATHINFO_EXTENSION) . "'";
                $user_id = $_SESSION['user_id'];
                //echo $inquiry_id.$v_rec_evidence_details_id.$t_evidence_type.$t_title.$t_submitted_by.$t_receive_date.$t_ref_no.$t_ref_date.$file_type.$ext.$user_id.'<br>';
                $param_details = array($inquiry_id, $v_rec_evidence_details_id, $t_evidence_type, $t_title, $t_submitted_by, $t_receive_date, $t_ref_no, $t_ref_date, $file_type, $ext, $user_id);
                $result = $receive_evidance_biz->save_details($param_details);
                echo $result->file_name;
                echo $ext;
                if ($ext != "''") {
                    $tmpname = $_FILES['file_name']['tmp_name'][$key];
                    $fp = fopen($tmpname, 'r');
                    $content = fread($fp, filesize($tmpname));
                    $content = addslashes($content);

                    move_uploaded_file($tmpname, $result->file_name);
                    fclose($fp);
                }
            }

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $receive_evidance_biz = new receive_evidance_biz;
        return $receive_evidance_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($inquiry_n, $inquiry_title, $start_date, $end_date, $organization_name, $doc_reg_no, $deadline, $language_id, $committe_id, $user_id, $param)
    {
        $receive_evidance_biz = new receive_evidance_biz;
        return $receive_evidance_biz->getsearchall1($inquiry_n, $inquiry_title, $start_date, $end_date, $organization_name, $doc_reg_no, $deadline, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $receive_evidance_biz = new receive_evidance_biz;
        return $receive_evidance_biz->getone($param);
    }

    function download($param)
    {
        $receive_evidance_biz = new receive_evidance_biz;
        return $receive_evidance_biz->downloadone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo($language_id, $user_id);
    }
}

$receive_evidance_info = new receive_evidance_info;
?>
