<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php
//ob_start();
require_once("../biz/parliament_info_biz.php");

class parliament_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {
        $parliament_info_biz = new parliament_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'-'.$value.'<br>';
                if (htmlspecialchars($key) == 'parliament_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'parliament') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $parliament_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->delete($param);
    }

}

$parliament_info = new parliament_info;
?>
