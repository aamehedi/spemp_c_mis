<?php
//ob_start();
require_once("../biz/sub_committee_info_biz.php");

class sub_committee
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save_commiitee'])) {
                // echo 'qqqqq';
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
                //header('Location: sub_committee.php?committee_id='.$_POST['committee_id']);
                exit;
            }
            if (isset($_POST['btn_close_commiitee'])) {
                header('Location: sub_committee.php?committee_id=' . $_POST['committee_id']);
                exit;
            }
        }
    }

    function build_param()
    {

        $sub_committee_info_biz = new sub_committee_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo $key . '-' . $value . '<br>';
                if (htmlspecialchars($key) == 'sub_committee_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));

                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'sub_committee_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['period_from'])) {
                array_push($param, "'" . htmlspecialchars($_POST['period_from']) . "'");
            }
            if (isset($_POST['period_to'])) {
                array_push($param, "'" . htmlspecialchars($_POST['period_to']) . "'");
            }
            if (isset($_POST['is_active'])) {
                array_push($param, '1');
            } else {
                array_push($param, '0');
            }
            return $sub_committee_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $committee_id, $user_id, $param)
    {
        $sub_committee_info_biz = new sub_committee_info_biz;
        return $sub_committee_info_biz->getall($language_id, $committee_id, $user_id, $param);
    }

    function editrow($param)
    {
        $sub_committee_info_biz = new sub_committee_info_biz;
        return $sub_committee_info_biz->getone($param);
    }

    function deleterow($param)
    {
        $sub_committee_info_biz = new sub_committee_info_biz;
        return $sub_committee_info_biz->delete($param);
    }

    function comboview($language_id, $user_id, $id)
    {
        $sub_committee_info_biz = new sub_committee_info_biz;
        return $sub_committee_info_biz->getcombo($language_id, $user_id, $id);
    }
}

$sub_committee = new sub_committee;
?>
