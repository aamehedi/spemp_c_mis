<?php
//ob_start();
require_once("../biz/user_group_permission_biz.php");

class user_group_permission
{

    function __construct()
    {
    }

    function group_permission($group_id, $user_id, $status)
    {
        $v = '';
        $user_group_permission_biz = new user_group_permission_biz;
        if ($status == 0) {
            $v = $user_group_permission_biz->save(array($group_id, $user_id));
        } else if ($status == 1) {
            $v = $user_group_permission_biz->delete(array($group_id, $user_id));
        } else {
            $v = '';
        }
        //header('Location: http://www.example.com/');
    }
}

$user_group_permission = new user_group_permission;
?>