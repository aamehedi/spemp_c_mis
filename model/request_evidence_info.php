<?php
//ob_start();
require_once("../biz/request_evidence_biz.php");

class request_evidence_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save_request'])) {
                //echo $_POST['btn_save_request'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $request_evidence_biz = new request_evidence_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key."-".$value."<br>";
                if (htmlspecialchars($key) == 'request_evidence_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'parliament_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'session_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'inquiry_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'request_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cover_letter') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $request_evidence_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $committee_id, $user_id, $param)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->getall($language_id, $committee_id, $user_id, $param);
    }

    function editrow($param)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->getone($param);
    }

    function deleterow($param)
    {
        $parliament_info_biz = new parliament_info_biz;
        return $parliament_info_biz->delete($param);
    }

    function issued($param)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->issued_value($param);
    }

    function comboview_parliament($language_id, $user_id, $id)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->getcombo_parliament($language_id, $user_id, $id);
    }

    function comboview_session($language_id, $user_id, $id)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->getcombo_session($language_id, $user_id, $id);
    }

    function comboview_session_parliament($parliament_id, $language_id, $user_id)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->getcombo_session_parliament($parliament_id, $language_id, $user_id);
    }

    function gridview_token_pree_popolated_inquiry($request_evidence_id)
    {
        $request_evidence_biz = new request_evidence_biz;
        return $request_evidence_biz->get_gridview_token_pree_popolated_inquiry($request_evidence_id);
    }
}

$request_evidence_info = new request_evidence_info;
?>
