<?php
////ob_start();
require_once("../biz/meeting_notice_biz.php");

class meeting_notice_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_approved'])) {
                $this->build_param_approved();
                exec('./wkhtmltopdf "http://' . $_SERVER['HTTP_HOST'] . '/mis/ui/rpt_meeting_notice.php?language_id=' . $_SESSION['language_id'] . '&meeting_notice_id=' . $_POST['metting_notice_id'] . '" ../pdf/meeting_notice/pdf_rpt_meeting_notice_' . $_POST['metting_notice_id'] . '.pdf', $output, $return);
                if ($return === 0) {
                    echo("<script language=\"javascript\">");
                    echo("opener.location.reload(true);");
                    echo("window.close();");
                    echo("</script>");
                }
            }
            if (isset($_POST['btn_issued'])) {
                $this->build_param_issued();
                exec('./wkhtmltopdf http://' . $_SERVER['HTTP_HOST'] . '/mis/ui/rpt_meeting_notice.php?language_id=' . $_SESSION['language_id'] . '&meeting_notice_id=' . $_POST['metting_notice_id'] . ' ../pdf/meeting_notice/pdf_rpt_meeting_notice_' . $_POST['metting_notice_id'] . '.pdf', $output, $return);
                if ($return === 0) {
                    echo("<script language=\"javascript\">");
                    echo("opener.location.reload(true);");
                    echo("window.close();");
                    echo("</script>");
                }
            }
            if (isset($_POST['btn_cancel'])) {
                echo $_POST['btn_cancel'];
                $this->build_param_cancel();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_revieced'])) {
                echo $_POST['btn_revieced'];
                $this->build_param_revieced();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_adjourned'])) {
                echo $_POST['btn_adjourned'];
                $this->build_param_adjourned();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_save'])) {
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");

            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param_approved()
    {

        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $meeting_notice_biz->approved($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param_issued()
    {

        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }

            return $meeting_notice_biz->issued($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param_cancel()
    {
        die('This is what you want');
        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $meeting_notice_biz->cancel($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param_revieced()
    {

        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                echo $key . '---' . $value . '<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'en_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'bn_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'time') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $meeting_notice_biz->revieced($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param_adjourned()
    {

        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $meeting_notice_biz->adjourned($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param()
    {

        $meeting_notice_biz = new meeting_notice_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo '<meta http-equiv="content-type" content="text/html; charset=UTF-8">';
                echo $key . '---' . $value . '<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'p_gov_no_pix') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'p_gov_no_postfix') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'sitting_no') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'en_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'bn_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'time') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'venue_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'sub_committee_id') {
                    if ($value == '')
                        array_push($param, htmlspecialchars(0));
                    else
                        array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'inqiuries') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'private_business_before') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'public_business') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'private_business_after') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'committee') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'witness') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'logistic') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'notification') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'invitees') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'doc_reg_no') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'ministry') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'organization') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'others') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'witness') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'other_witness') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'adviser') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'committee_officer') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'end_time') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                }
            }
            return $meeting_notice_biz->save($param);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param);
    }

    function grid_issued_view($metting_notice_id, $user_id, $param)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getsearchall_issuedview($metting_notice_id, $user_id, $param);
    }

    function editrow($param)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getone($param);
    }

    function govref($param)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->govrefb($param);
    }

    function rpt_meeting_notice($param)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->rpt_meeting_notice_getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo($language_id, $user_id);
    }

    function gridview_token_pree_popolated_inquiry($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_inquiry($metting_notice_id);
    }

    function getall_meeting_notice_by_inquiry_title($inquiryTitle)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getall_meeting_norice_inquiry_tag_by_inquiry_title($inquiryTitle);
    }

    function gridview_token_pree_popolated_inquiry_display($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_inquiry_display($metting_notice_id);
    }

    function gridview_token_inquiry($keyword, $language_id, $committee_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_inquiry($keyword, $language_id, $committee_id, $user_id);
    }

    function gridview_token_notification($keyword, $language_id, $committee_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_notification($keyword, $language_id, $committee_id, $user_id);
    }

    function gridview_token_invitees($keyword, $language_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_invitees($keyword, $language_id, $user_id);
    }

    function gridview_token_committee_member($keyword, $language_id, $committee_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_committee_member($keyword, $language_id, $committee_id, $user_id);
    }

    function gridview_token_witness_member($keyword, $language_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_witnes_member($keyword, $language_id, $user_id);
    }

    function gridview_token_logistic_member($keyword, $language_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_logistic_member($keyword, $language_id, $user_id);
    }

    function gridview_token_officer_member($keyword, $language_id, $committee_id, $user_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_officer_member($keyword, $language_id, $committee_id, $user_id);
    }

    function gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id);
    }

    function raw_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id);
    }

    function gridview_token_pree_popolated_committee_display($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_committee_display($metting_notice_id, $sub_committee_id, $committee_id, $language_id);
    }

    function gridview_token_pree_popolated_witness($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_witness($metting_notice_id);
    }

    function raw_token_pree_popolated_witness($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_witness($metting_notice_id);
    }

    function gridview_token_pree_popolated_logistic($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_logistic($metting_notice_id);
    }

    function raw_token_pree_popolated_logistic($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_logistic($metting_notice_id);
    }

    //Taufiq
    function gridview_token_auto_popolated_logistic($language_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_auto_popolated_logistic($language_id);
    }

    //Taufiq
    function gridview_token_pree_popolated_adviser($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_adviser($metting_notice_id);
    }

    function raw_token_pree_popolated_adviser($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_adviser($metting_notice_id);
    }

    function gridview_token_pree_popolated_officer($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_officer($metting_notice_id);
    }

    function raw_token_pree_popolated_officer($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_officer($metting_notice_id);
    }

    //Taufiq
    function gridview_token_auto_popolated_officer($language_id, $committee_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_auto_popolated_officer($language_id, $committee_id);
    }

    //Taufiq
    function gridview_token_pree_popolated_notification($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_notification($metting_notice_id);
    }

    function raw_token_pree_popolated_notification($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_raw_token_pree_popolated_notification($metting_notice_id);
    }

    function gridview_token_pree_popolated_invitees($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_invitees($metting_notice_id);
    }

    function comboview_venue($language_id, $user_id, $id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getcombo_venue($language_id, $user_id, $id);
    }

    function comboview_committee($language_id, $committee_id, $user_id, $id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->getcombo_committee($language_id, $committee_id, $user_id, $id);
    }
}

$meeting_notice_info = new meeting_notice_info;
?>
