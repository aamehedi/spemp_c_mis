<?php
//ob_start();
require_once("../biz/user_biz.php");

class user
{

    function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save_user'])) {
                $resultUserSave = mysqli_fetch_assoc($this->build_param());
                echo("<script>");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");


            }
            if (isset($_POST['btn_change'])) {
                $this->change_password();
                echo("<script>");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_group_change'])) {
                $this->update_group();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {
        $user_biz = new user_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                echo $key.'-'.$value.'<br>';
                if (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'password') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'is_parliament_member') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            if (isset($_POST['parliamentMember'])) {
                array_push($param, htmlspecialchars($_POST['parliamentMember']));
            }
            if (isset($_POST['group_id'])) {
                array_push($param, htmlspecialchars($_POST['group_id']));
            }
            return $user_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview()
    {
        $user_biz = new user_biz;
        return $user_biz->getall();
    }

    function change_password()
    {
        $user_biz = new user_biz;
        $pwd = '';
        $conpwd = '';
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $value;
                if (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'password') {
                    $pwd = htmlspecialchars($value);
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'confirm_password') {
                    $conpwd = htmlspecialchars($value);
                }

            }
            if ($pwd == $conpwd)
                return $user_biz->change_password($param);
        } catch (Exception $e) {
            return "Error: " . $e->getMessage();
        }
    }

    function update_group(){
        try{
            $user_id = $_POST['user_id'];
            $parliament_member_id = (!empty($_POST['parliament_member_id'])) ? $_POST['parliament_member_id'] : '';
            $group_id = $_POST['group_id'];

            $user_biz = new user_biz();
            return $user_biz->update_group(array($user_id, $parliament_member_id, $group_id));
        } catch (Exception $e){
            return "Error: ".$e->getMessage();
        }
    }

    function change_activation($param)
    {
        $user_biz = new user_biz;
        return $user_biz->change_activation($param);
    }

    function editrow($param)
    {
        $user_biz = new user_biz;
        return $user_biz->getone($param);
    }

    function checkuser($param)
    {
        $user_biz = new user_biz;
        $result = $user_biz->getbyname($param);
        if ($result->num_rows > 0) {
            $result = mysqli_fetch_object($result);
//            var_dump($result);
//            die();
            $_SESSION["user_name"] = $result->user_name;
            $_SESSION["user_id"] = $result->user_id;
            $_SESSION['is_parliament_member'] = $result->is_parliament_member;
            $_SESSION['group'] = $result->group_name;
            $_SESSION["language_id"] = 2;
            return true;
        }
        return false;
    }


}

$user = new user;
?>