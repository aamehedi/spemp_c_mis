<?php
//ob_start();
require_once("../biz/document_registration_key_biz.php");

class document_registration_info_key
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save_key'])) {
                //echo $_POST['btn_save_key'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {

            }
        }
    }

    function build_param()
    {
        $document_registration_key_biz = new document_registration_key_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'doc_reg_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'key_area') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'recommendations') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'result_forcasting') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $document_registration_key_biz->save_master($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($doc_reg_no, $doc_reg_date_start, $doc_reg_date_end, $doc_title, $language_id, $committe_id, $user_id, $param)
    {
        $document_registration_key_biz = new document_registration_key_biz;
        return $document_registration_key_biz->getsearchall($doc_reg_no, $doc_reg_date_start, $doc_reg_date_end, $doc_title, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $document_registration_key_biz = new document_registration_key_biz;
        return $document_registration_key_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id, $id)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->getcombo($language_id, $user_id, $id);
    }
}

$document_registration_info_key = new document_registration_info_key;
?>
