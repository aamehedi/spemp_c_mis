<?php
//ob_start();
require_once("../biz/create_inquiry_biz.php");
require_once("../biz/inquiry_scheduling_biz.php");

class create_inquiry_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save_inqueiry'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");

            }
            if (isset($_POST['btn_issued'])) {
                $result = $this->build_param_issued();
                exec('./wkhtmltopdf "http://' . $_SERVER['HTTP_HOST'] . '/mis/ui/create_inquiry_print.php?language_id=' . $_SESSION['language_id'] . '&inquiry_id=' . $_POST['inquery_id'] . '" ../pdf/create_inquiry_pdf/pdf_rpt_create_inquiry_' . $_POST['inquery_id'] . '.pdf', $output, $return);
                if ($return === 0) {
                    echo("<script language=\"javascript\">");
                    echo("opener.location.reload(true);");
                    echo("window.close();");
                    echo("</script>");
                }
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }


    function build_param_issued()
    {

        $create_inquiry_biz = new create_inquiry_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'inquery_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $create_inquiry_biz->save_issued($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function build_param()
    {

        $create_inquiry_biz = new create_inquiry_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'inquery_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'inquery_no') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'create_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'proposed_month') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'proposed_year') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'parliament_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'session_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'inquery_title') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'doc_reg_no') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'ministry') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'organization') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'others') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'witness') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'other_witness') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'adviser') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                }
            }
            $inquiry_id = $create_inquiry_biz->save_master($param);
            //echo $inquiry_id;
            $schedule_id = 0;
            $schedule_no = $_POST['schedule_no'];
            $schedule_date = $_POST['schedule_date'];
            $user_id = $_POST['user_id'];

            foreach ($schedule_no as $key => $v_schedule_no) {
                $v_schedule_date = "'" . $schedule_date[$key] . "'";
                //echo $v_schedule_no.$v_schedule_date.'<br>';
                $param = array($schedule_id, $inquiry_id, $v_schedule_no, $v_schedule_date, $user_id);
                $result = $create_inquiry_biz->save_details_date($param);
            }
            /*
            if($doc_reg_id!=NULL)
            {
                   $doc_reg_detail_id=$_POST['doc_reg_detail_id'];
                $doc_name=$_POST['doc_name'];
                $doc_type=$_POST['doc_type'];
                 foreach($doc_name as $key=>$v_doc_name) {
                    $t_doc_reg_detail_id = $doc_reg_detail_id[$key];
                    $t_doc_name="'".$v_doc_name."'";
                    $t_doc_type ="'".$doc_type[$key]."'";
                    $user_id=1;
                    $param_details=array($t_doc_reg_detail_id,$doc_reg_id,$t_doc_name,$t_doc_type,$user_id);
                    $document_registration_biz->save_details($param_details);
                  }
            }
            */
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewdetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($inquiry_no, $inquiry_title, $schedule_month, $schedule_year, $doc_reg_no, $language_id, $committe_id, $user_id, $param)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getsearchall($inquiry_no, $inquiry_title, $schedule_month, $schedule_year, $doc_reg_no, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getone($param);
    }

    function editreport($param)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getreport($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboview($language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo($language_id, $user_id);
    }

    function gridview_token($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token($keyword, $language_id, $user_id);
    }

    function gridview_token_document($keyword, $language_id, $committee_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_document($keyword, $language_id, $committee_id);
    }

    function gridview_token_pree_popolated_document($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_pree_popolated_document($inquiry_id);
    }

    function gridview_token_pree_popolated_ministry($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_ministry($inquiry_id);
    }

    function gridview_token_pree_popolated_organization($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_organization($inquiry_id);
    }

    function gridview_token_pree_popolated_adviser($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_adviser($inquiry_id);
    }

    function gridview_token_pree_popolated_others($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_others($inquiry_id);
    }

    function gridview_token_pree_popolated_witness($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_witness($inquiry_id);
    }

    function gridview_token_pree_popolated_others_witness($inquiry_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->gridview_token_pree_popolated_other_witness($inquiry_id);
    }

    function gridview_token_organization($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_organization($keyword, $language_id, $user_id);
    }

    function gridview_token_adviser($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_adviser($keyword, $language_id, $user_id);
    }

    function  gridview_token_witness($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_witness($keyword, $language_id, $user_id);
    }

    function  gridview_token_other_witness($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_other_witness($keyword, $language_id, $user_id);
    }

    function  gridview_token_others_data($keyword, $language_id, $user_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->get_gridview_token_other_data($keyword, $language_id, $user_id);
    }

    function comboview_year($language_id, $user_id, $id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo_year($language_id, $user_id, $id);
    }

    function comboview_month($language_id, $user_id, $id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo_month($language_id, $user_id, $id);
    }

    function comboview_inquiry($inquiry_id, $language_id, $committee_id, $user_id, $status)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo_inquiry($inquiry_id, $language_id, $committee_id, $user_id, $status);
    }

    function comboview_inquiry_for_request_evidence($inquiry_id, $language_id, $committee_id, $user_id, $status, $request_evidence_id)
    {
        $create_inquiry_biz = new create_inquiry_biz;
        return $create_inquiry_biz->getcombo_inquiry_for_request_evidence($inquiry_id, $language_id, $committee_id, $user_id, $status, $request_evidence_id);
    }

    function getcount($inquiry_id, $user_id)
    {
        $inquiry_scheduling_biz = new inquiry_scheduling_biz;
        return $inquiry_scheduling_biz->getcount($inquiry_id, $user_id);
    }
}
$create_inquiry_info = new create_inquiry_info;
?>
