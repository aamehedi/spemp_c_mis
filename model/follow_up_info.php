<?php
//ob_start();
require_once("../biz/follow_up_biz.php");

class follow_up_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {

            }
        }
    }

    function build_param()
    {

        $follow_up_biz = new follow_up_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'inquiry_report_followup_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                }
                if (htmlspecialchars($key) == 'inquiry_report_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'followup_no') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'followup_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'background') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            $inquiry_report_follow_id = $follow_up_biz->save_master($param);
            if ($inquiry_report_follow_id != NULL) {
                $action_taken = $_POST['action_taken'];
                $recommendation = $_POST['recommendation'];
                $comment = $_POST['comment'];
                $organization_response = $_POST['organization_response'];
                foreach ($recommendation as $key => $v_recommendation) {
                    $t_action_taken = "'" . $action_taken[$key] . "'";
                    $t_recommendation = "'" . $v_recommendation . "'";
                    $t_comment = "'" . $comment[$key] . "'";
                    $t_organization_response = "'" . $organization_response[$key] . "'";
                    $user_id = $_SESSION['user_id'];
                    $param_details = array($inquiry_report_follow_id, $t_recommendation, $t_organization_response, $t_action_taken, $t_comment, $user_id);
                    $follow_up_biz->save_details($param_details);
                }
            }

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($inquiry_report_followup_id, $inquiry_report_id, $user_id)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getalldetails($inquiry_report_followup_id, $inquiry_report_id, $user_id);
    }

    function searchgridview($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getsearchall($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param);
    }

    function rpt_follow_up_getall($param)
    {
        try {
            $follow_up_biz = new follow_up_biz;
            return $follow_up_biz->rpt_follow_up_getall($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function editrow($param)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboviewinquiry($inquiry_id, $language_id, $committee_id, $user_id)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getcombo($inquiry_id, 'tbl_inquiry_master_c', 'inquiry_id', $language_id, $committee_id, $user_id);
    }

    function comboviewparliament($parliament_id, $language_id, $committee_id, $user_id)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getcombo2($parliament_id, 'tbl_parliament_c', 'parliament_id', $language_id, $committee_id, $user_id);
    }

    function comboviewsession($session_id, $language_id, $committee_id, $user_id)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->getcombo2($session_id, 'tbl_session_c', 'session_id', $language_id, $committee_id, $user_id);
    }

    function issued($param)
    {
        $follow_up_biz = new follow_up_biz;
        return $follow_up_biz->issued_value($param);
    }

}

$follow_up_info = new follow_up_info;
?>
