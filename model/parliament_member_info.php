<?php
//ob_start();
require_once("../biz/parliament_member_biz.php");

class parliament_member_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                // if (!empty($_SESSION['parlimentMemberUserID'])) {
                // 	$_SESSION['parliamentMemberAdded'] = true;
                // }
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $parliament_member_biz = new parliament_member_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                // echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'member_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'member_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'constituency') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'current_address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'active') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }

            }
            array_push($param, htmlspecialchars($_POST['user_id']));
            if (isset($_POST['seat_no'])) {
                array_push($param, "'" . htmlspecialchars($_POST['seat_no']) . "'");
            }
            if (isset($_POST['permanent_address'])) {
                array_push($param, "'" . htmlspecialchars($_POST['permanent_address']) . "'");
            }
            if (isset($_POST['url'])) {
                array_push($param, "'" . htmlspecialchars($_POST['url']) . "'");
            }
            if (isset($_POST['sex'])) {
                array_push($param, "'" . htmlspecialchars($_POST['sex']) . "'");
            }
            if (isset($_POST['nid'])) {
                array_push($param, "'" . htmlspecialchars($_POST['nid']) . "'");
            }
            if (isset($_POST['election_date'])) {
                array_push($param, "'" . htmlspecialchars($_POST['election_date']) . "'");
            }
            if (isset($_POST['party_name'])) {
                array_push($param, "'" . htmlspecialchars($_POST['party_name']) . "'");
            }
            if (isset($_POST['dob'])) {
                array_push($param, "'" . htmlspecialchars($_POST['dob']) . "'");
            }
            if (isset($_POST['birth_place'])) {
                array_push($param, "'" . htmlspecialchars($_POST['birth_place']) . "'");
            }
            if (isset($_POST['fathers_name'])) {
                array_push($param, "'" . htmlspecialchars($_POST['fathers_name']) . "'");
            }
            if (isset($_POST['mothers_name'])) {
                array_push($param, "'" . htmlspecialchars($_POST['mothers_name']) . "'");
            }
            if (isset($_POST['marital_status'])) {
                array_push($param, "'" . htmlspecialchars($_POST['marital_status']) . "'");
            }

            return $parliament_member_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $parliament_member_biz = new parliament_member_biz;
        return $parliament_member_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {

        $parliament_member_biz = new parliament_member_biz;
        return $parliament_member_biz->getone($param);
    }

    function deleterow($param)
    {
        $ministry_info_biz = new ministry_info_biz;
        return $ministry_info_biz->delete($param);
    }

    function getNotAddedUser($param)
    {
        $parliament_member_biz = new parliament_member_biz;
        return $parliament_member_biz->getNotAddedUser($param);
    }


}

$parliament_member_info = new parliament_member_info;
?>
