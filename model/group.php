<?php
//ob_start();
require_once("../biz/group_biz.php");

class group
{

    function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                $this->build_param();
                echo("<script>");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {
        $group_biz = new group_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $value;
                if (htmlspecialchars($key) == 'group_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'group_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                }
            }
            return $group_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview()
    {
        $group_biz = new group_biz;
        return $group_biz->getall();
    }

    function gridviewpermission($user_id)
    {
        $group_biz = new group_biz;
        return $group_biz->getallpermission($user_id);
    }

    function editrow($param)
    {
        $group_biz = new group_biz;
        return $group_biz->getone($param);
    }

    function getGroup($id)
    {
        $group_biz = new group_biz;
        return $group_biz->getGroup($id);
    }

}

$group = new group;
?>