<?php
////ob_start();
require_once("../biz/inquiry_report_biz.php");

class inquiry_report_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $inquiry_report_biz = new inquiry_report_biz;
        try {
            $param = array();
            // var_dump($_POST);
            foreach ($_POST as $key => $value) {
                // echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'inquiry_report_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'report_no') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'report_date') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'inquiry_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                }
                if (htmlspecialchars($key) == 'title') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                }
//				elseif(htmlspecialchars($key)=='parliament_id')
//				{
//					array_push($param, htmlspecialchars($value)== '' ? 0 : htmlspecialchars($value));
//				}
//				elseif(htmlspecialchars($key)=='session_id')
//				{
//					array_push($param, htmlspecialchars($value)== '' ? 0 : htmlspecialchars($value));
//				}
                if (htmlspecialchars($key) == 'executive_summary') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'introduction') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'issues') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            // die();
            $inquiry_report_id = $inquiry_report_biz->save_master($param);
            // var_dump($inquiry_report_id);
            if ($inquiry_report_id != NULL) {
                $recom_id = $_POST['recom_id'];
                $recommendation = $_POST['recommendation'];
                $recom_no = $_POST['recom_no'];
                foreach ($recommendation as $key => $v_recommendation) {
                    $t_recom_id = $recom_id[$key];
                    $t_recommendation = "'" . $v_recommendation . "'";
                    $t_recom_no = "' '";
                    $user_id = $_SESSION['user_id'];
                    $param_details = array($inquiry_report_id, $t_recom_id, $t_recom_no, $t_recommendation, $user_id);
                    $inquiry_report_biz->save_details($param_details);
                }
            }

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($inquiry_report_id, $user_id)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getalldetails($inquiry_report_id, $user_id);
    }

    function searchgridview($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getsearchall($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param);
    }

    function editrow($param)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getone($param);
    }

    function rpt_inquiry_report_getall($param)
    {
        try {
            $inquiry_report_biz = new inquiry_report_biz;
            return $inquiry_report_biz->rpt_inquiry_report_getall($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function comboviewinquiry($inquiry_id, $language_id, $committee_id, $user_id)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getcombo($inquiry_id, 'tbl_inquiry_master_complete_c', 'inquiry_id', $language_id, $committee_id, $user_id);
    }

    function comboviewparliament($parliament_id, $language_id, $committee_id, $user_id)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getcombo2($parliament_id, 'tbl_parliament_c', 'parliament_id', $language_id, $committee_id, $user_id);
    }

    function comboviewsession($session_id, $language_id, $committee_id, $user_id)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->getcombo2($session_id, 'tbl_session_c', 'session_id', $language_id, $committee_id, $user_id);
    }

    function record_issued($param)
    {
        $inquiry_report_biz = new inquiry_report_biz;
        return $inquiry_report_biz->record_issued_value($param);
    }
}

$inquiry_report_info = new inquiry_report_info;
?>
