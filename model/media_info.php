<?php
//ob_start();
require_once("../biz/media_info_biz.php");

class media_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['btn_save'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $media_info_biz = new media_info_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {

                //echo $key.'---'.$value.'<br>';

                if (htmlspecialchars($key) == 'media_id') {
                    array_push($param, htmlspecialchars($value) == '[Auto]' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'media_name') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'media_type') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'contact_person') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'cell_phone') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'designation') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'fax') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'email') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'address') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'comments') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, htmlspecialchars($value));
                }
            }
            return $media_info_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridview($language_id, $user_id, $param)
    {
        $media_info_biz = new media_info_biz;
        return $media_info_biz->getall($language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $media_info_biz = new media_info_biz;
        return $media_info_biz->getone($param);
    }

    function comboview($id)
    {
        $media_info_biz = new media_info_biz;
        return $media_info_biz->getcombo($id);
    }

    function deleterow($param)
    {
        $ministry_info_biz = new ministry_info_biz;
        return $ministry_info_biz->delete($param);
    }

}

$media_info = new media_info;
?>
