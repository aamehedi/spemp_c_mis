<?php
//ob_start();
require_once("../biz/briefing_note_biz.php");

class briefing_note_info
{

    function __construct()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            if (isset($_POST['search_button'])) {
                echo $_POST['search_button'];
                //$this->build_param();
            }
            if (isset($_POST['btn_save_b'])) {
                //echo $_POST['btn_save'];
                $this->build_param();
                echo("<script language=\"javascript\">");
                echo("opener.location.reload(true);");
                echo("window.close();");
                echo("</script>");
            }
            if (isset($_POST['btn_close'])) {
                echo("<script language=\"javascript\">");
                echo("window.close();");
                echo("</script>");
            }
        }
    }

    function build_param()
    {

        $create_briefing_note_biz = new create_briefing_note_biz;
        try {
            $param = array();
            foreach ($_POST as $key => $value) {
                //echo $key.'---'.$value.'<br>';
                if (htmlspecialchars($key) == 'metting_notice_id') {
                    array_push($param, htmlspecialchars($value) == '' ? 0 : htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'introduction') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'key_issue') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'advisor_question') {
                    array_push($param, "'" . trim(htmlspecialchars($value)) . "'");
                } elseif (htmlspecialchars($key) == 'witness_question') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'written_evidence') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'remarks') {
                    array_push($param, "'" . htmlspecialchars($value) . "'");
                } elseif (htmlspecialchars($key) == 'user_id') {
                    array_push($param, htmlspecialchars($value));
                } elseif (htmlspecialchars($key) == 'language_id') {
                    array_push($param, trim(htmlspecialchars($value)));
                } elseif (htmlspecialchars($key) == 'committee_id') {
                    array_push($param, trim(htmlspecialchars($value)));
                }
            }
            //var_dump($param);
            return $create_briefing_note_biz->save($param);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function gridviewDetails($doc_reg_id, $user_id)
    {
        $document_registration_biz = new document_registration_biz;
        return $document_registration_biz->getalldetails($doc_reg_id, $user_id);
    }

    function searchgridview($start_date, $end_date, $ref_no, $sub_committe_id, $sitting_id, $committe_id, $language_id, $user_id, $param)
    {
        $create_briefing_note_biz = new create_briefing_note_biz;
        return $create_briefing_note_biz->getsearchall($start_date, $end_date, $ref_no, $sub_committe_id, $sitting_id, $committe_id, $language_id, $user_id, $param);
    }

    function editrow($param)
    {
        $create_briefing_note_biz = new create_briefing_note_biz;
        return $create_briefing_note_biz->getone($param);
    }

    function rpt_brifeing_note($param)
    {
        $create_briefing_note_biz = new create_briefing_note_biz;
        return $create_briefing_note_biz->rpt_brifeing_note_getone($param);
    }

    function deleterow($param)
    {
        $ministry_member_info_biz = new ministry_member_info_biz;
        return $ministry_member_info_biz->delete($param);
    }

    function issued($param)
    {
        $create_briefing_note_biz = new create_briefing_note_biz;
        return $create_briefing_note_biz->issued_value($param);
    }

    function comboview($language_id, $user_id)
    {
        $create_briefing_note_biz = new create_briefing_note_biz;
        return $create_briefing_note_biz->getcombo($language_id, $user_id);
    }

    function gridview_token_pree_popolated_inquiry($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_inquiry($metting_notice_id);
    }

    function gridview_token_pree_popolated_committee($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_committee($metting_notice_id);
    }

    function gridview_token_pree_popolated_witness($metting_notice_id)
    {
        $meeting_notice_biz = new meeting_notice_biz;
        return $meeting_notice_biz->get_gridview_token_pree_popolated_witness($metting_notice_id);
    }
}

$briefing_note_info = new briefing_note_info;
?>
