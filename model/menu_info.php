<?php
require_once('../biz/menu_biz.php');

class menu_info
{
    function __construct()
    {

    }


    function get_menu_all($user_id, $language_id)
    {
        $menu_biz = new menu_biz;
        return $menu_biz->get_menu_all($user_id, $language_id);
    }

    function __destruct()
    {

    }
}


?>