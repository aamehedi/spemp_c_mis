<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class multimedia_verbatim_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_transcript_i", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($metting_notice_id, $user_id)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_transcript_gall", array($metting_notice_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_transcript_gall", array($metting_notice_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array('File Name', ' ', ' ');
                $gridview->datafield = array('file_title', 'is_edit', 'is_delete');
                $gridview->data_bind($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_transcript_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function rpt_multimedia_verbatim_getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("rpt_multimedia_verbatim", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        try {
            $start_date_t = "'" . $start_date . "'";
            $end_date_t = "'" . $end_date . "'";
            $ref_no_t = "'" . $ref_no . "'";
            $sitting_no_t = "'" . $sitting_no . "'";
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array($param[0], $param[1], $param[2], $param[3], ' ');
                $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'venue_name', 'is_multimedia');
                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function delete($param)
    {
        try {
            $data_access = new data_access;
            $data_access->data_reader("tbl_transcript_d", $param);
            //$result = mysqli_fetch_object($result);
            return '';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_master_c", array($language_id, $user_id));
            echo '<select onchange="show(this.value)" name="doc_reg_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_inquiry($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_inquiry_tag_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_committee($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_committee_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_witness($metting_notice_id)
    {
        try {
            $inquiry_id = 1;
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_witness_gall", array($metting_notice_id, $inquiry_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_logistic($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_logistic_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_notification($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_notification_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_invitees($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_invitees_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_notification($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_notification_member_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_inquiry($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_inquiry_master_token", array("'" . $keyword . "'", $language_id, $committee_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_invitees($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_invitees_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_committee_member($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_committee_member_token", array("'" . $keyword . "'", $language_id, $committee_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_witnes_member($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_multimedia_verbatim_witness_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_logistic_member($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_logistic_member_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getalldetails($doc_reg_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_details_gall", array($doc_reg_id, $user_id));
            echo '<div>';
            $gridview->data_bind_details($resultset);
            echo '</div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_venue($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_venue_c", array($language_id, $user_id));
            echo '<select  name="venue_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_committee($language_id, $committee_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_sub_committee_c", array($language_id, $committee_id, $user_id));
            echo '<select  name="sub_committee_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
