﻿<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class meeting_notice_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_i", $param);
            if ($ifsuccess == true) {
                $_SESSION['witness_name'] = '';
                return "Successfully Saved!";
            } else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function approved($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_approved", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function issued($param)
    {
        try {

            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_issued", $param);
            return $ifsuccess;
//			if($ifsuccess == true){
//				return "Successfully Saved!";
//			}
//			else
//				return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function cancel($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_cancel", $param);
            if ($ifsuccess == true) {
                return "Successfully Saved!";
            } else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function revieced($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_revised", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function adjourned($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_meeting_notice_master_adjourned", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($language_id, $user_id, $param)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_ministry_member_gall", array($language_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_ministry_member_gall", array($language_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array('ID', 'Member Name', 'Ministry Name', 'Designation', ' ', ' ');
                $gridview->datafield = array('ministry_member_id', 'ministry_member_name', 'ministry_name', 'designation', 'is_edit', 'is_delete');
                $gridview->data_bind($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function getsearchall_issuedview($metting_notice_id, $user_id)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;

            $resultset = $data_access->data_reader("tbl_meeting_notice_master_view", array($metting_notice_id, $user_id));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array('Sitting No.', 'Date', 'Venue', 'Modify Date', ' ');
                $gridview->datafield = array('sitting_no', 'en_date', 'venue_name', 'modify_date', 'is_view_notice');
                $gridview->data_bind($resultset);
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_meeting_notice_master_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function govrefb($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_govt_ref_no_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function rpt_meeting_notice_getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("rpt_meeting_notice", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        try {
            $start_date_t = "'" . $start_date . "'";
            $end_date_t = "'" . $end_date . "'";
            $ref_no_t = "'" . $ref_no . "'";
            $sitting_no_t = "'" . $sitting_no . "'";
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit));
            // while ($rowMehedi = mysqli_fetch_assoc($resultset)) {
            // 	var_dump($rowMehedi['metting_notice_id']);
            // }
            // die();

            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit_value));
            if ($resultset) {

                $field_count = $resultset->field_count;

                $xml = simplexml_load_file("xml/meeting_notice_popup.xml");
                foreach ($xml->information as $information) {
                    if ($information->language_id == $_SESSION['language_id']) {

                        $ref_no = $information->ref_no;
                        $sitting_no = $information->sitting_no;
                        $date = $information->date;
                        $venue = $information->venue;
                    }
                }
                if ($_SESSION['language_id'] === '1') {
                    $status = 'Status';
                } else {
                    $status = 'অবস্থা';
                }
                if ($_SESSION['group'] === 'System Admin') {
                    $gridview->headertext = array($ref_no, $sitting_no, $date, $venue, $status, ' ', ' ');
                    $gridview->datafield = array('ref_no', 'sitting_no', 'en_date', 'venue_name', 'status', 'is_revoke', 'is_print');
                } elseif ($_SESSION['group'] === 'Approver') {
                    $gridview->headertext = array($ref_no, $sitting_no, $date, $venue, $status, ' ', ' ', ' ', ' ', ' ', ' ', ' ');
                    $gridview->datafield = array('ref_no', 'sitting_no', 'en_date', 'venue_name', 'status', 'is_approved', 'is_issued', 'is_cancled', 'is_adjourned', 'is_revised', 'is_view_notice', 'is_print');
                } elseif ($_SESSION['group'] === 'User') {
                    $gridview->headertext = array($ref_no, $sitting_no, $date, $venue, $status, ' ', ' ');
                    $gridview->datafield = array('ref_no', 'sitting_no', 'en_date', 'venue_name', 'status', 'is_edit', 'is_print');
                }
//                $gridview->headertext = array($ref_no, $sitting_no, $date, $venue, $status, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');

                //$gridview->headertext=array('Ref No','Sitting No','Date','Venue Name','Status',' ',' ',' ',' ',' ',' ',' ', ' ');
//                $gridview->datafield = array('ref_no', 'sitting_no', 'en_date', 'venue_name', 'status', 'is_edit', 'is_approved', 'is_issued', 'is_cancled', 'is_adjourned', 'is_revised', 'is_view_notice', 'is_print');
                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_ministry_member_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_master_c", array($language_id, $user_id));
            echo '<select onchange="show(this.value)" name="doc_reg_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_inquiry($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_inquiry_tag_gall", array($metting_notice_id));
            // var_dump(mysqli_fetch_object($resultset));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall_meeting_norice_inquiry_tag_by_inquiry_title($inquiryTitle)
    {
        try {
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_inquiry_tag_gall_by_inquiry_title", array("'" . $inquiryTitle . "'"));
            return $resultset;

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_inquiry_display($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_inquiry_tag_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated_display($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_committee_gall", array($metting_notice_id, $sub_committee_id, $committee_id, $language_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_committee($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_committee_gall", array($metting_notice_id, $sub_committee_id, $committee_id, $language_id));
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_committee_display($metting_notice_id, $sub_committee_id, $committee_id, $language_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_committee_gall", array($metting_notice_id, $sub_committee_id, $committee_id, $language_id));
            return $gridview->data_bind_taken_pree_popolated_display($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_witness($metting_notice_id)
    {
        try {
            $inquiry_id = 1;
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_witness_gall", array($metting_notice_id, $inquiry_id));
            // var_dump($gridview->data_bind_taken_pree_popolated($resultset));
            // die();
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_witness($metting_notice_id)
    {
        try {
            $inquiry_id = 1;
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_witness_gall", array($metting_notice_id, $inquiry_id));
            // var_dump($gridview->data_bind_taken_pree_popolated($resultset));
            // die();
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_logistic($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_logistic_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_logistic($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_logistic_gall", array($metting_notice_id));
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    //Taufiq
    function get_gridview_token_auto_popolated_logistic($language_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_logistic_member_gall", array($language_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    //Taufiq
    function get_gridview_token_pree_popolated_adviser($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_adviser_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_adviser($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_adviser_gall", array($metting_notice_id));
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_officer($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_officer_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_officer($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_officer_gall", array($metting_notice_id));
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    //taufiq
    function get_gridview_token_auto_popolated_officer($language_id, $committee_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_committee_officer_committee_tag_auto", array($language_id, $committee_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    //taufiq

    function get_gridview_token_pree_popolated_notification($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_notification_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_raw_token_pree_popolated_notification($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_notification_gall", array($metting_notice_id));
            return $resultset;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_invitees($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_invitees_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_notification($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_notification_member_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_inquiry($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_inquiry_master_token", array("'" . $keyword . "'", $language_id, $committee_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_invitees($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_invitees_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_committee_member($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_committee_member_token", array("'" . $keyword . "'", $language_id, $committee_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_witnes_member($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_meeting_notice_witness_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_logistic_member($keyword, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_logistic_member_token", array("'" . $keyword . "'", $language_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_officer_member($keyword, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_committee_officer_committee_tag_token", array("'" . $keyword . "'", $language_id, $committee_id, $user_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getalldetails($doc_reg_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_details_gall", array($doc_reg_id, $user_id));
            echo '<div>';
            $gridview->data_bind_details($resultset);
            echo '</div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_venue($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_venue_c", array($language_id, $user_id));

            $gridview->data_bind_combo_i($resultset, $id);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_committee($language_id, $committee_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_sub_committee_c", array($language_id, $committee_id, $user_id));

            $gridview->data_bind_combo_i($resultset, $id);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
