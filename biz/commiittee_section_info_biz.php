<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class commiittee_section_info_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_committee_section_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($language_id, $user_id, $param)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_committee_gall", array($language_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_committee_gall", array($language_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array('ID', 'Name', 'Short Name', ' ', ' ');
                $gridview->datafield = array('committee_id', 'committee_name', 'short_name', 'is_edit', 'is_delete');
                $gridview->data_bind($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_committee_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_committee_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_parliament_c", array($language_id, $user_id));
            $gridview->data_bind_combo($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
