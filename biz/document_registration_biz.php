<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class document_registration_biz extends gridview
{
    function __construct()
    {

    }

    function save_master($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_master_i", $param);
            $result = mysqli_fetch_object($result);
            return $result->doc_reg_id;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function save_details_i($param_details)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_details_i", $param_details);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function save_details_u($param_details)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_details_u", $param_details);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_master_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone_details($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_details_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getsearchall($doc_reg, $doc_reg_date_s, $doc_reg_date_end, $doc_t, $ref_no, $language_id, $committe_id, $user_id, $param)
    {
        try {
            $doc_reg_no_t = "'" . $doc_reg . "'";
            $doc_reg_date_start_t = "'" . $doc_reg_date_s . "'";
            $doc_reg_date_end_t = "'" . $doc_reg_date_end . "'";
            $doc_title_t = "'" . $doc_t . "'";
            $ref_no_t = "'" . $ref_no . "'";
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_document_registration_master_search", array($doc_reg_no_t, $doc_reg_date_start_t, $doc_reg_date_end_t, $doc_title_t, $ref_no_t, $language_id, $committe_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?ref_no=' . $ref_no . '&doc_reg_no=' . $doc_reg . '&doc_reg_date_start=' . $doc_reg_date_s . '&doc_reg_date_end=' . $doc_reg_date_end . '&doc_title=' . $doc_t . '&pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_document_registration_master_search", array($doc_reg_no_t, $doc_reg_date_start_t, $doc_reg_date_end_t, $doc_title_t, $ref_no_t, $language_id, $committe_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                //$gridview->headertext=array('ID', 'Reg No', 'Date','Titel',' ',' ');
                //$gridview->datafield=array('doc_reg_id', 'doc_reg_no', 'doc_reg_date','doc_title','is_edit','is_delete');
                $gridview->headertext = array($param[0], $param[1], $param[2], ' ');
                $gridview->datafield = array('doc_reg_no', 'doc_reg_date', 'doc_title', 'is_edit');
                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<br><br><b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_document_registration_details_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_ministry_c", array($language_id, $user_id));
            echo '<select onchange="show(this.value)" name="ministry_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getalldetails($doc_reg_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_details_gall", array($doc_reg_id, $user_id));
            echo '<div>';
            $gridview->data_bind_details($resultset);
            echo '</div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
