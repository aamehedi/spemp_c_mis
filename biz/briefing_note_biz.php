<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class create_briefing_note_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        // die("tbl_briefing_note_i called.");
        try {
            $data_access = new data_access;
            // var_dump($param);

            $ifsuccess = $data_access->execute_non_query("tbl_briefing_note_i", $param);
            // var_dump($ifsuccess);
            // die();
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getsearchall($start_date, $end_date, $ref_no, $sub_committe_id, $sitting_id, $committe_id, $language_id, $user_id, $param1)
    {
        try {
            $ref_no_t = "'" . $ref_no . "'";
            $start_date_t = "'" . $start_date . "'";
            $end_date_t = "'" . $end_date . "'";
            $sitting_id_t = "'" . $sitting_id . "'";
            $sub_committe_id_t = $sub_committe_id;
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "''";
            $resultset = $data_access->data_reader("tbl_briefing_note_search", array($start_date_t, $end_date_t, $ref_no_t, $sub_committe_id_t, $sitting_id_t, $committe_id, $language_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }
            $resultset = $data_access->data_reader("tbl_briefing_note_search", array($start_date_t, $end_date_t, $ref_no_t, $sub_committe_id_t, $sitting_id_t, $committe_id, $language_id, $user_id, $limit_value));
            //$resultset=$data_access->data_reader("tbl_briefing_note_search",array("''","''","''",1,"''",1,1,1,"''"));
            if ($resultset) {
                $field_count = $resultset->field_count;
                if ($_SESSION['group'] === 'System Admin') {
                    $gridview->headertext = array($param1[0], $param1[1], $param1[2], $param1[3], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'inquiry_title', 'is_revoke', 'is_delete');

                } elseif ($_SESSION['group'] === 'Approver') {
                    $gridview->headertext = array($param1[0], $param1[1], $param1[2], $param1[3], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'inquiry_title', 'is_issued', 'is_delete');

                } elseif ($_SESSION['group'] === 'User') {
                    $gridview->headertext = array($param1[0], $param1[1], $param1[2], $param1[3], '  ', ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'inquiry_title', 'is_edit', 'is_delete');

                }

                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_briefing_note_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_parliament_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function issued_value($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_briefing_note_issued", $param);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function rpt_brifeing_note_getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("rpt_brifeing_note", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
