<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class group_menu_permission_biz
{
    function __construct()
    {
    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_group_menu_permission_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_group_menu_permission_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($group_id)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $resultset = $data_access->data_reader("tbl_group_menu_permission_gall", array($group_id));
            // var_dump($resultset);
            $gridview->headertext = array('ID', 'English Menu Name', 'Bangla Menu Name', 'View', 'Insert', 'Edit', 'Delete', 'Print');
            $gridview->datafield = array('menu_id', 'en_menu_name', 'bn_menu_name', 'is_view', 'is_insert', 'is_edit', 'is_delete', 'is_print');
            $gridview->data_bind($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
