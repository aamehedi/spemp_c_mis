<?php
require_once('../dal/data_access.php');

class menu_biz
{
    function __construct()
    {

    }

    private function get_menu($parentmenuid, $user_id, $language_id)
    {
        $param = array($parentmenuid, $user_id, $language_id);
        $data_access = new data_access;
        $result = $data_access->data_reader("tbl_menu_gall", $param);
        return $result;
    }

    function get_menu_all($user_id, $language_id)
    {
        $str = '';
        $result_set1 = $this->get_menu(0, $user_id, $language_id);
        while ($r1 = mysqli_fetch_object($result_set1)) {
            if ($r1->menu_link == null)
                $str .= '<li><a href="#" >' . $r1->menu_name . '</a>';
            else
                $str .= "<li><a href=" . $r1->menu_link . " title=" . $r1->menu_name . " target='_self'>" . $r1->menu_name . "</a>";
            $result_set2 = $this->get_menu($r1->menu_id, $user_id, $language_id);
            if ($result_set2->num_rows > 0) {
                $str .= "<ul>";
                while ($r2 = mysqli_fetch_object($result_set2)) {
                    if ($r2->menu_link != '') {
                        $str .= "<li><a href=" . $r2->menu_link . " target='_self' title=" . $r2->menu_name . ">" . $r2->menu_name . "</a>";
                        // var_dump($r2->menu_name);
                        // if ($r2->menu_name === 'মিডিয়া') {

                        // 	die();
                        // }
                    } else
                        $str .= '<li><a href="#">' . $r2->menu_name . '</a>';

                    $result_set3 = $this->get_menu($r2->menu_id, $user_id, $language_id);
                    //print_r($result_set3);
                    if ($result_set3->num_rows > 0) {
                        $str .= "<ul>";
                        while ($r3 = mysqli_fetch_object($result_set3)) {
                            $str .= "<li><a href=" . $r3->menu_link . " target='_self' title=" . $r3->menu_name . ">" . $r3->menu_name . "</a></li>";
                        }
                        $str .= "</ul></li>";
                    }
                }
                $str .= "</ul></li>";
            }
        }
        //echo $str;
        return $str;
    }

    function __destruct()
    {

    }
}


?>