<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class user_committee_permission_biz
{
    function __construct()
    {
    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_user_committee_permission_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_user_committee_permission_d", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($user_id, $language_id)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $resultset = $data_access->data_reader("tbl_user_committee_permission_gall", array($user_id, $language_id));
            $gridview->headertext = array('Committee Name', 'Is Permitted',);
            $gridview->datafield = array('committee_name', 'is_edit');
            $gridview->data_bind($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_user_committee($user_id, $language_id)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_user_committee_permission_gid", array($user_id, $language_id));
            //$result = mysqli_fetch_object($result);
            //$nr=$resultset->num_rows;
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_user_committee_count($user_id, $language_id)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_user_committee_permission_gid", array($user_id, $language_id));
            $nr = $result->num_rows;
            return $nr;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
