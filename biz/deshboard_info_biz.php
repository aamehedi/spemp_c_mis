<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class deshboard_info_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {

        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_parliament_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($committee_id, $language_id, $param)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;

            $resultset = $data_access->data_reader("sp_upcoming_sittings", array($committee_id, $language_id));
            if ($resultset) {
                $gridview->headertext = array($param[0], $param[1], $param[2], $param[3]);
                $gridview->datafield = array('sitting_no', 'metting_date', 'venue_name', 'inquiry_no');
                $gridview->data_bind($resultset);
            } else
                echo '<div><b style="color:#61399D">No Record Found</b></div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_parliament_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_parliament_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function __destruct()
    {

    }
}

?>
