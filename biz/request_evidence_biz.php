<?php
// error_reporting(E_ERROR | E_PARSE);
require_once('../dal/data_access.php');
require_once('gridview.php');

class request_evidence_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_request_evidence_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($language_id, $committee_id, $user_id, $param)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_request_evidence_gall", array($language_id, $committee_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }
            $resultset = $data_access->data_reader("tbl_request_evidence_gall", array($language_id, $committee_id, $user_id, $limit_value));
            if ($resultset) {
                $xml = simplexml_load_file("../ui/xml/request_evidance_popup.xml");
                foreach ($xml->information as $information) {
                    if ($information->language_id == $_SESSION['language_id']) {
                        $issue_date = $information->issue_date;
                        $inquiry_title = $information->inquery_title;
                        $inquiry_no = $information->inquiry_no;
                    }
                }

                if ($_SESSION['group'] === 'System Admin') {
                    $gridview->headertext = array($inquiry_no, $inquiry_title, $issue_date, ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'inquiry_title', 'request_date', 'is_revoke', 'is_print');
                } elseif ($_SESSION['group'] === 'Approver') {
                    $gridview->headertext = array($param[0], $param[1], $param[2], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'inquiry_title', 'request_date', 'is_issued', 'is_print');
                } elseif ($_SESSION['group'] === 'User') {
                    $gridview->headertext = array($param[0], $param[1], $param[2], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'inquiry_title', 'request_date', 'is_edit', 'is_print');
                }

                //$gridview->headertext=array('Inquiry No','Inquiry Title','Request Date',' ', ' ',' ');

                $gridview->data_bind($resultset);

                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else
                echo '<div><b style="color:#61399D">No Record Found</b></div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_request_evidence_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_parliament_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function issued_value($param)
    {
        try {

            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_request_evidence_issued", $param);
//			$result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_parliament($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_parliament_c", array($language_id, $user_id));

            $gridview->data_bind_combo_i($resultset, $id);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_session($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_session_c", array($language_id, $user_id));

            $gridview->data_bind_combo_i($resultset, $id);

        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_session_parliament($parliament_id, $language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_session_c_by_parliament", array($parliament_id, $language_id, $user_id));
            echo '<select  name="session_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_inquiry($request_evidence_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_request_evidence_prepopulated", array($request_evidence_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
