<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class follow_up_biz extends gridview
{
    function __construct()
    {

    }

    function save_master($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_follow_up_master_i", $param);
            $result = mysqli_fetch_object($result);
            return $result->inquiry_report_followup_id;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function save_details($param_details)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_follow_up_recommendation_i", $param_details);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall($language_id, $user_id, $param)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("", array($language_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_inquiry_report_master_gall", array($language_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array('ID', 'Member Name', 'Ministry Name', 'Designation', 'Add Follow-up');
                $gridview->datafield = array('ministry_member_id', 'ministry_member_name', 'ministry_name', 'designation', 'add_new',);
                $gridview->data_bind($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_follow_up_master_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getsearchall($report_date_start, $report_date_end, $report_no, $inquiry_no, $parliament_id, $language_id, $committe_id, $user_id, $param)
    {
        try {
            $report_date_start_t = "'" . $report_date_start . "'";
            $report_date_end_t = "'" . $report_date_end . "'";
            $report_no_t = "'" . $report_no . "'";
            $parliament_id_t = "'" . $parliament_id . "'";
            $inquiry_no_t = "'" . $inquiry_no . "'";
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_follow_up_master_search", array($report_date_start_t, $report_date_end_t, $report_no_t, $parliament_id_t, $inquiry_no_t, $language_id, $committe_id, $user_id, $limit));
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_follow_up_master_search", array($report_date_start_t, $report_date_end_t, $report_no_t, $parliament_id_t, $inquiry_no_t, $language_id, $committe_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                $gridview->headertext = array($param[0], $param[1], $param[2], $param[3], ' ');
                $gridview->datafield = array('report_no', 'report_date', 'inquiry_no', 'add_new', 'v_text');

                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function rpt_follow_up_getall($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("rpt_follow_up", $param);
            //$result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_ministry_member_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($combo_id, $sp_name, $id, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader($sp_name, array($language_id, $committee_id, $user_id));
            echo '<select onchange="show(this.value)" name="' . $id . '">';
            $gridview->data_bind_combo_i($resultset, $combo_id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo2($combo_id, $sp_name, $id, $language_id, $committee_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader($sp_name, array($language_id, $user_id));
            echo '<select onchange="show(this.value)" name="' . $id . '">';
            $gridview->data_bind_combo_i($resultset, $combo_id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getalldetails($inquiry_report_followup_id, $inquiry_report_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_follow_up_recommendation_gall", array($inquiry_report_followup_id, $inquiry_report_id, $user_id));
            echo '<div>';
            //$gridview->data_bind_details_followup_recommendation($resultset);
            $i = 1;
            echo '<div>
					<div class="columnA" style="width:28%;"><label>Committee Recommendation</label></div>
					<div class="columnA" style="width:15%;"><label>Response of Organization</label></div>
					<div class="columnA" style="width:28%;"><label>Action Taken</label></div>
					<div class="columnA" style="width:28%;"><label>Comment</label></div>
					<div class="divclear"></div>
				</div>';
            while ($row = $resultset->fetch_row()) {
                echo '<div>
					<div class="columnA" style="width:28%;"><textarea id="txt' . $i++ . '" name="recommendation[]" style="font-size: 11px;width: 98%;">' . $row[2] . '</textarea></div>
					<div class="columnA" style="width:15%;"><select style="font-size: 11px;width: 98%;" name="organization_response[]"><option ';
                if ($row[3] == "Accept") echo 'Selected ';
                echo ' value="Accept">Accept</option><option ';
                if ($row[3] == "Not Accepted") echo 'Selected ';
                echo 'value="Not Accepted">Not Accepted</option><option ';
                if ($row[3] == "Partially Accepted") echo 'Selected ';
                echo 'value="Partially Accepted">Partially Accepted</option></select></div>
					<div class="columnA" style="width:28%;"><textarea name="action_taken[]" id="txt' . $i++ . '" style="font-size: 11px;width: 98%;">' . $row[4] . '</textarea></div>
					<div class="columnA" style="width:28%;"><textarea name="comment[]" id="txt' . $i++ . '" style="font-size: 11px;width: 98%;">' . $row[5] . '</textarea></div>
					<div class="divclear"></div>
				</div><br>';

            }
            echo '</div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
