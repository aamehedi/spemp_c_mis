<?php
require_once('../dal/data_access.php');
require_once('gridview.php');

class record_decisions_biz extends gridview
{
    function __construct()
    {

    }

    function save($param)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_record_of_decision_master_i", $param);
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function record_decision_inquiry_save_biz($metting_notice_id, $inquiry_id, $user_id)
    {
        try {
            $data_access = new data_access;
            $ifsuccess = $data_access->execute_non_query("tbl_record_of_decision_inquiry_tag_i", array($metting_notice_id, $inquiry_id, $user_id));
            if ($ifsuccess == true)
                return "Successfully Saved!";
            else
                return "Record can not Save!";
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_record_of_decision_master_gid", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getsearchall($start_date, $end_date, $ref_no, $id, $sitting_no, $language_id, $committe_id, $user_id, $param)
    {
        try {
            $start_date_t = "'" . $start_date . "'";
            $end_date_t = "'" . $end_date . "'";
            $ref_no_t = "'" . $ref_no . "'";
            $sitting_no_t = "'" . $sitting_no . "'";
            $data_access = new data_access;
            $gridview = new gridview;
            $limit = "' '";
            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit));
            // while ($rowTest = mysqli_fetch_assoc($resultset)) {
            // 	var_dump($rowTest);
            // }
            $nr = $resultset->num_rows;
            if (isset($_GET['pn'])) {
                $pn = preg_replace('#[^0-9]#i', '', $_GET['pn']);
            } else {
                $pn = 1;
            }
            $itemsPerPage = 10;
            $lastPage = ceil($nr / $itemsPerPage);
            if ($pn < 1) {
                $pn = 1;
            } else if ($pn > $lastPage) {
                $pn = $lastPage;
            }
            $centerPages = "";
            $sub1 = $pn - 1;
            $sub2 = $pn - 2;
            $add1 = $pn + 1;
            $add2 = $pn + 2;
            if ($pn == 1) {
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            } else if ($pn == $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
            } else if ($pn > 2 && $pn < ($lastPage - 1)) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub2 . '">' . $sub2 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add2 . '">' . $add2 . '</a> &nbsp;';
            } else if ($pn > 1 && $pn < $lastPage) {
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $sub1 . '">' . $sub1 . '</a> &nbsp;';
                $centerPages .= '&nbsp; <span class="pagNumActive">' . $pn . '</span> &nbsp;';
                $centerPages .= '&nbsp; <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $add1 . '">' . $add1 . '</a> &nbsp;';
            }
            $limit = ' LIMIT ' . ($pn - 1) * $itemsPerPage . ',' . $itemsPerPage;
            $limit_value = "'" . $limit . "'";
            //echo $limit_value;
            //$limit="' LIMIT 0,10'";
            $paginationDisplay = "";
            if ($lastPage != "1") {
                $paginationDisplay .= 'Page <strong>' . $pn . '</strong> of ' . $lastPage . '&nbsp;  &nbsp;  &nbsp; ';
                if ($pn != 1) {
                    $previous = $pn - 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $previous . '"> Back</a> ';
                }
                $paginationDisplay .= '<span class="paginationNumbers">' . $centerPages . '</span>';
                if ($pn != $lastPage) {
                    $nextPage = $pn + 1;
                    $paginationDisplay .= '&nbsp;  <a href="' . $_SERVER['PHP_SELF'] . '?pn=' . $nextPage . '"> Next</a> ';
                }
            }

            $resultset = $data_access->data_reader("tbl_meeting_notice_master_search", array($start_date_t, $end_date_t, $ref_no_t, $id, $sitting_no_t, $committe_id, $language_id, $user_id, $limit_value));
            if ($resultset) {
                $field_count = $resultset->field_count;
                if ($_SESSION['group'] === 'System Admin') {
                    $gridview->headertext = array($param[0], $param[1], $param[2], $param[3], ' ',  ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'venue_name', 'is_record_of_dec_revoke', 'is_print_rod');

                } elseif ($_SESSION['group'] === 'Approver') {
                    $gridview->headertext = array($param[0], $param[1], $param[2], $param[3], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'venue_name', 'is_record_of_dec_issue', 'is_print_rod');

                } elseif ($_SESSION['group'] === 'User') {
                    $gridview->headertext = array($param[0], $param[1], $param[2], $param[3], ' ', ' ');
                    $gridview->datafield = array('inquiry_no', 'sitting_no', 'en_date', 'venue_name', 'is_record_of_dec', 'is_print_rod');

                }

                $gridview->data_bind_document($resultset);
                echo '<div class="footergrid">&nbsp;&nbsp;&nbsp;&nbsp;' . $paginationDisplay . '</div>';
            } else {
                echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function delete($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_ministry_member_d", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo($language_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_master_c", array($language_id, $user_id));
            echo '<select onchange="show(this.value)" name="doc_reg_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_adviser($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_record_of_decision_adviser_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function get_gridview_token_pree_popolated_officer($metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_record_of_decision_officer_gall", array($metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    //get_gridview_token_pree_popolated_all
    function get_gridview_token_pree_popolated_all($type, $metting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_record_of_decision_all_gall", array($type, $metting_notice_id));
            return $gridview->data_bind_taken_pree_popolated($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    //get_gridview_token_committee_all
    function get_gridview_token_committee_all($type, $keyword, $meeting_notice_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_record_of_decision_all_token", array("'" . $type . "'", "'" . $keyword . "'", $meeting_notice_id));
            return $gridview->data_bind_taken($resultset);
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }


    function getalldetails($doc_reg_id, $user_id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_document_registration_details_gall", array($doc_reg_id, $user_id));
            echo '<div>';
            $gridview->data_bind_details($resultset);
            echo '</div>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_chairman($metting_notice_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_record_of_decision_chair_c", array($metting_notice_id));
            echo '<select  name="chair_member_id" style="width:70%;"  class="validate[required,maxSize[150]] text-input bangla">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_venue($language_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_venue_c", array($language_id, $user_id));
            echo '<select  name="venue_id" readonly="true" >';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getcombo_committee($language_id, $committee_id, $user_id, $id)
    {
        try {
            $gridview = new gridview;
            $data_access = new data_access;
            $resultset = $data_access->data_reader("tbl_sub_committee_c", array($language_id, $committee_id, $user_id));
            echo '<select  name="sub_committee_id">';
            $gridview->data_bind_combo_i($resultset, $id);
            echo '</select>';
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function getall_record_inquiry($metting_notice_id)
    {
        try {
            $data_access = new data_access;
            $gridview = new gridview;
            $resultset = $data_access->data_reader("tbl_record_of_decision_inquiry_tag_gall", array($metting_notice_id));
            // var_dump($resultset);
            if (!is_bool($resultset)) {
                $gridview->headertext = array('ID', 'Inquiry No:', 'Status', ' ');
                $gridview->datafield = array('inquiry_id', 'inquiry_no', 'status', 'is_settle');
                $gridview->data_bind_with_out_footer($resultset);
            } else {
                //echo '<b style="color:#61399D;">No Record Found</b>';
            }
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function rpt_record_of_decision_getone($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("rpt_record_of_decision", $param);
            $result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function record_issued_value($param)
    {
        try {
            $data_access = new data_access;
            $result = $data_access->data_reader("tbl_record_of_decision_master_issued", $param);
            //$result = mysqli_fetch_object($result);
            return $result;
        } catch (Exception $e) {
            return "Erorr: " . $e->getMessage();
        }
    }

    function __destruct()
    {

    }
}

?>
