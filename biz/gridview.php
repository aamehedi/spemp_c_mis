﻿<?php

class gridview
{
    function __construct()
    {

    }

    public function data_bind_with_out_footer($resultset)
    {

        echo '<table width="100%" border="0" class="mGrid">';
        echo '<tr>';
        for ($i = 0; $i < count($this->headertext); $i++) {
            echo '<th align="center">';
            echo $this->headertext[$i];
            echo '</th>';
        }
        echo '</tr>';
        while ($result = $resultset->fetch_object()) {
            echo '<tr>';
            for ($i = 0; $i < count($this->datafield); $i++) {
                echo '<td align="center">';
                $df = $this->datafield[$i];
                echo $result->$df;
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';

    }

    public function data_bind($resultset)
    {
        //if($resultset)
        //{
        echo '<table width="100%" border="0" class="mGrid">';
        echo '<tr>';
        for ($i = 0; $i < count($this->headertext); $i++) {
            echo '<th align="center">';
            echo $this->headertext[$i];
            echo '</th>';
        }
        echo '</tr>';
        while ($result = $resultset->fetch_object()) {
            echo '<tr>';
            for ($i = 0; $i < count($this->datafield); $i++) {
                echo '<td align="center">';
                $df = $this->datafield[$i];
                echo htmlspecialchars_decode($result->$df);
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
        //}
    }

    public function data_bind_member($resultset)
    {
        echo '<table width="100%" border="0" class="mGrid">';
        echo '<tr>';
        for ($i = 0; $i < count($this->headertext); $i++) {
            echo '<th align="center">';
            echo $this->headertext[$i];
            echo '</th>';
        }
        echo '</tr>';
        while ($result = $resultset->fetch_object()) {
            echo '<tr>';
            for ($i = 0; $i < count($this->datafield); $i++) {
                echo '<td align="center">';
                $df = $this->datafield[$i];
                if ($df === 'is_chairman') {
                    if ($_SESSION['language_id'] === '1') {
                        if ($result->$df === '1') {
                            echo 'Chairman';
                        } else {
                            echo 'Member';
                        }
                    } else {
                        if ($result->$df === '1') {
                            echo 'সভাপতি';
                        } else {
                            echo 'সদস্য';
                        }
                    }
                } else {
                    echo htmlspecialchars_decode($result->$df);
                }
                echo '</td>';
            }
            echo '</tr>';
        }
        echo '</table>';
    }

    public function data_bind_document($resultset)
    {

        echo '<table width="100%" border="0" class="mGrid">';
        echo '<tr>';
        for ($i = 0; $i < count($this->headertext); $i++) {
            echo '<th align="center">';
            echo $this->headertext[$i];
            echo '</th>';
        }
        echo '</tr>';
        while ($result = $resultset->fetch_object()) {
            echo '<tr>';
            for ($i = 0; $i < count($this->datafield); $i++) {
                if ($this->datafield[$i] == 'doc_title') {
                    echo '<td align="left">';
                    $df = $this->datafield[$i];
                    echo htmlspecialchars_decode($result->$df);
                    echo '</td>';
                } else {
                    echo '<td align="center">';
                    $df = $this->datafield[$i];
                    // if ($df == 'gov_no_pix') {
                    // 	var_dump($df);
                    // 	var_dump($result);
                    // }
                    echo htmlspecialchars_decode($result->$df);
                    echo '</td>';
                }
            }
            echo '</tr>';
        }
        echo '</table>';

    }

    public function data_bind_combo($resultset, $id)
    {
        echo '<select onchange="show(this.value)" name="id">';
        echo '<option  value="">--Select --</option>';
        while ($row = $resultset->fetch_row()) {
            if ($id == $row[0]) {
                echo '<option   value="' . $row[0] . '" class="option" selected >' . $row[1] . '</option>';
            } else {
                echo '<option   value="' . $row[0] . '" class="option" >' . $row[1] . '</option>';
            }
        }
        echo '</select>';
    }

    public function data_bind_taken($resultset)
    {
        $SearchStr = '[';

        while ($row = $resultset->fetch_row()) {
//            var_dump($row);
//           $row[1] = str_replace(' ','_',$row[1]);
//           $row[1] = preg_replace('/[^A-Za-z0-9<>_=;\/&\-]/','',$row[1]);
            // $SearchStr = $SearchStr.'{"name":"'.strip_tags(html_entity_decode(str_replace('_',' ',$row[1]))).'","id":"'.$row[0].'"},';
            $SearchStr = $SearchStr . '{"name":"' . strip_tags(html_entity_decode($row[1])) . '","id":"' . $row[0] . '"},';
// //		     {"name":"'.$name.'","id":"1"}
// //		   echo '[{"name":"'.$row[0].'","id":"'.$row[0].'"}' ; 
// 	       //echo'<option   value="'.$row[0].'" class="option" selected >'.$row[1].'</option>';		 
        }
        $len = strlen($SearchStr);
        $SearchStr = substr($SearchStr, 0, $len - 1);
        $SearchStr = $SearchStr . ']';
        return $SearchStr;
    }


    public function data_bind_taken_pree_popolated($resultset)
    {
        $PreLoad = '[';
        while ($row = $resultset->fetch_row()) {
            $PreLoad = $PreLoad . '{id: "' . $row[0] . '", name: "' . $row[1] . '"},';
//		     {"name":"'.$name.'","id":"1"}
//		   echo '[{"name":"'.$row[0].'","id":"'.$row[0].'"}' ; 
            //echo'<option   value="'.$row[0].'" class="option" selected >'.$row[1].'</option>';
        }
        $len = strlen($PreLoad);
        $PreLoad = substr($PreLoad, 0, $len - 1);
        $PreLoad = $PreLoad . ']';
        return $PreLoad;
    }

    public function data_bind_taken_pree_popolated_display($resultset)
    {
        while ($row = $resultset->fetch_row()) {
            echo $row[1] . ",";
        }

    }


    public function data_bind_combo_i($resultset, $id)
    {
        echo '<option  selected="selected"  value="" >--Select --</option>';
        while ($row = $resultset->fetch_row()) {
            if ($id == $row[0]) {
                echo '<option   value="' . $row[0] . '" class="option" selected="selected">' . $row[1] . '</option>';
            } else {
                echo '<option   value="' . $row[0] . '" class="option" >' . $row[1] . '</option>';
            }
        }
    }

    public function data_bind_combo_v($resultset, $id)
    {
        echo '<option  selected="selected"  value="" class="option">--Select --</option>';
        while ($row = $resultset->fetch_row()) {
            if ($id == $row[1]) {
                echo '<option   value="' . $row[1] . '" class="option" selected="selected">' . $row[1] . '</option>';
            } else {
                echo '<option   value="' . $row[1] . '" class="option" >' . $row[1] . '</option>';
            }
        }
    }

    public function data_bind_details_recommendation($resultset)
    {
        while ($row = $resultset->fetch_row()) {
            $token = explode(' ', $row[2]);
            echo "<div id='" . $row[1] . "'><div class='columnA'><input name='recom_id[]' type='hidden' value='0' /><label id='label" . $row[1] . "'>" . $row[2] . "</label></div><div class='columnB' style='width:70%'><input type='hidden' name='recom_no[]'/><textarea name='recommendation[]' id='txt" . $token[1] . "' style='font-size: 11px;width: 78%;'>" . $row[3] . "</textarea><input type='button' name='recommendation[]' value='Delete' onclick='myFunction(" . $row[1] . ");'  class='popupbutton simplemodal-wrap'/></div>&nbsp;&nbsp;<div class='divclear'></div><br></div>";

        }
    }

    public function data_bind_details_followup_recommendation($resultset)
    {
        $i = 1;
        echo '<div>
					<div class="columnA" style="width:28%;"><label>Committee<br style="line-height:1px;">Recommendation</label></div>
					<div class="columnA" style="width:15%;"><label>Response of Organization</label></div>
					<div class="columnA" style="width:28%;"><label>Action Taken</label></div>
					<div class="columnA" style="width:28%;"><label>Comment</label></div>
					<div class="divclear"></div>
				</div>';
        while ($row = $resultset->fetch_row()) {
            echo '<div>
					<div class="columnA" style="width:28%;"><textarea id="txt' . $i++ . '" name="recommendation[]" style="font-size: 11px;width: 98%;">' . $row[2] . '</textarea></div>
					<div class="columnA" style="width:15%;"><select style="font-size: 11px;width: 98%;" name="organization_response[]"><option ';
            if ($row[3] == "Accept") echo 'Selected ';
            echo ' value="Accept">Accept</option><option ';
            if ($row[3] == "Not Accepted") echo 'Selected ';
            echo 'value="Not Accepted">Not Accepted</option><option ';
            if ($row[3] == "Partially Accepted") echo 'Selected ';
            echo 'value="Partially Accepted">Partially Accepted</option></select></div>
					<div class="columnA" style="width:28%;"><textarea name="action_taken[]" id="txt' . $i++ . '" style="font-size: 11px;width: 98%;">' . $row[4] . '</textarea></div>
					<div class="columnA" style="width:28%;"><textarea name="comment[]" id="txt' . $i++ . '" style="font-size: 11px;width: 98%;">' . $row[5] . '</textarea></div>
					<div class="divclear"></div>
				</div><br>';

        }
    }

    public function data_bind_details($resultset)
    {
        while ($row = $resultset->fetch_row()) {
            echo '<div id="' . $row[0] . '"><input name="doc_reg_detail_id[]" type="hidden" value="' . $row[0] . '" /><div style="float:left;width:30%;"><input type="text" name="doc_title[]" value="' . $row[2] . '"></div><div class="myfileupload-buttonbar" style="visibility: hidden !important;"><label class="myui-button">Add Files<input type="file" name="file_name[]" id="file"/></label></div><div style="float:left;width:15%;">' . $row[8] . '</div>
<div style="float:left;width:15%;">' . $row[7] . '</div><div style="clear:both"></div></div>';
            //<div class="myfileupload-buttonbar"><label class="myui-button">Add Files<input type="file" name="file_name[]" id="file"/></label></div>
        }
    }

    public function data_bind_details_schedule($resultset)
    {
        while ($row = $resultset->fetch_row()) {
            echo "<div style='float:left;padding-left:2px;'><input type='hidden' name='schedule_no[]' value='" . $row[0] . "' /><input type='text' name='schedule_date[]'   value='" . $row[1] . "' id='" . $row[0] . "' onclick='datepicker(" . $row[0] . ");'/></div>";
        }
    }

    public function data_bind_details_schedule_issued($resultset)
    {
        while ($row = $resultset->fetch_row()) {
            echo "<div style='float:left;padding-left:2px;'><input type='hidden' name='schedule_no[]' value='" . $row[0] . "' disabled/><input type='text' name='schedule_date[]'   value='" . $row[1] . "' disabled/></div>";
        }
    }

    public function data_bind_details_evidance($resultset)
    {
        $countEvidance = 0;
        if (!empty($resultset)) {
            while ($row = $resultset->fetch_row()) {
                $countEvidance++;
                echo "<div id='" . $row[2] . "'><input name='rec_evidence_details_id[]' type='hidden' value='" . $row[2] . "' /><div style='float:left;width:11%;'><select name='evidence_type[]'><option value='WrittenEvidence'>Written Evidence</option><option value='Multimedia'>Multimedia</option></select></div><div style='float:left;width:11%;'><input id='name' type='text' style='width:90%;' name='submitted_by[]' class='validate[required] text-input bangla' value='" . $row[5] . "'/></div><div style='float:left;width:11%;'><input  type='text' style='width:90%;' name='title[]' class='validate[required] text-input bangla' value='" . $row[4] . "' /></div><div style='float:left;width:11%;'><input  type='text' style='width:90%;' name='receive_date[]' id='evidanceCalendar" . $countEvidance . "' class='validate[required] text-input bangla' value='" . $row[6] . "'/></div><div style='float:left;width:11%;'><input  type='text' style='width:90%;' name='ref_no[]'  value='" . $row[7] . "'/></div><div style='float:left;width:11%;'><input  type='text' style='width:90%;' id='referenceCalendar" . $countEvidance . "' name='ref_date[]'  value='" . $row[8] . "'/></div><div class='myfileupload-buttonbar'><label class='myui-button'>Add Files<input type='file' name='file_name[]' id='file' /></label></div><div style='float:left;width:11%;'>&nbsp;</div><div style='float:left;width:11%;'><input type='button' class='gridbutton' value='View' onclick='download(" . $row[2] . ");'/></div><div class='divclear'></div></div>";
//                <div style='float:left;width:12%;'><input  type='file' name='file_name[]'/></div>

            }
        }
    }

    function __destruct()
    {

    }
}

?>